#pragma OPENCL EXTENSION cl_khr_fp64 : enable    //requires checking that the graphics card can support double calcs

typedef struct tag_ray_iterator2
{
    double r;
    double z;
    double zeta;
    double xi;
	double q;
    double p;
	int terminated;
} RayIterator2;


typedef struct tag_point_info
{
    double ds;
    double c;
    double dcdz;
    double d2cdz2;
} PointInfo;

RayIterator2 rayf3(RayIterator2 x0, double c, double dcdz, double d2cdz2) {
    double c2 = c*c; //cHere*cHere;
    double c2inv = 1/c2;
    double cnn = c2*(d2cdz2*x0.xi*x0.xi); 

	RayIterator2 dx;
    dx.r = c*x0.xi; //dr/ds := c*xi //eq 3.23
    dx.z = c*x0.zeta; //dz/ds := c*zeta //eq 3.24
    dx.zeta = -dcdz*c2inv;   //dzeta/ds := -1/c^2*(dc/dz) //eq 3.24
    dx.q = c*x0.p; //dq/ds := c*p //eq 3.58
    dx.p = -cnn*c2inv*x0.q; //dp/ds := -cnn/c2*q //eq 3.58
	
	//NB xi doesn't change if dc/dr = 0
	return dx;
}

RayIterator2 addIterator(RayIterator2 x, RayIterator2 dx, double ds) {
	RayIterator2 z;
    z.r = x.r + dx.r * ds;
	z.z = x.z + dx.z * ds;
	z.zeta = x.zeta + dx.zeta * ds;
	z.xi = x.xi; //NB const
	z.q = x.q + dx.q * ds;
	z.p = x.p + dx.p * ds;
	return z;
}

void rayIterator2MultiplyBy(RayIterator2 x, double ds) {
	x.r *= ds;
	x.z *= ds;
	x.zeta *= ds;
	x.q *= ds;
	x.p *= ds;
}

RayIterator2 rk2StepMCK2(RayIterator2 x0, double ds, double c, double dcdz, double d2cdz2) {
    RayIterator2 k1 = rayf3(x0 ,c,dcdz,d2cdz2);
	RayIterator2 tmp = addIterator(x0,k1,ds/2);
    RayIterator2 k2 = rayf3(tmp,c,dcdz,d2cdz2);
	RayIterator2 x1 = addIterator(x0,k2,ds);
	
	x1.r = 4561.0;
	return x1;
}

RayIterator2 rk4StepMCK2(RayIterator2 x0, double ds, double c, double dcdz, double d2cdz2) {
	RayIterator2 k1 = rayf3(x0 ,c,dcdz,d2cdz2);
	rayIterator2MultiplyBy(k1,ds);
	RayIterator2 tmp1 = addIterator(x0,k1,1/2);	
	
	RayIterator2 k2 = rayf3(tmp1,c,dcdz,d2cdz2);
	rayIterator2MultiplyBy(k2,ds);
	RayIterator2 tmp2 = addIterator(x0,k2,1/2);
	
	RayIterator2 k3 = rayf3(tmp2,c,dcdz,d2cdz2);
	rayIterator2MultiplyBy(k3,ds);
	RayIterator2 tmp3 = addIterator(x0,k3,1);
	
	RayIterator2 k4 = rayf3(tmp3,c,dcdz,d2cdz2);    
	
	RayIterator2 x1;
	x1.r = x0.r + 0.16666667*k1.r + 0.33333333*k2.r + 0.33333333*k3.r + 0.16666667*k4.r*ds;
	x1.z = x0.z + 0.16666667*k1.z + 0.33333333*k2.z + 0.33333333*k3.z + 0.16666667*k4.z*ds;
	x1.zeta = x0.zeta + 0.16666667*k1.zeta + 0.33333333*k2.zeta + 0.33333333*k3.zeta + 0.16666667*k4.zeta*ds;
	x1.xi = x0.xi; //NB const	
	x1.q = x0.q + 0.16666667*k1.q + 0.33333333*k2.q + 0.33333333*k3.q + 0.16666667*k4.q*ds;
	x1.p = x0.p + 0.16666667*k1.p + 0.33333333*k2.p + 0.33333333*k3.p + 0.16666667*k4.p*ds;
	return x1;
}

kernel void takeStepConstSSP(global RayIterator2 *rays, global PointInfo *inInfo) {
    unsigned int iTheta = get_global_id(0);
	//TODO get c, dcdz etc and iz
	if (!rays[iTheta].terminated) {
		rays[iTheta] = rk2StepMCK2(rays[iTheta],inInfo[iTheta].ds,inInfo[iTheta].c,inInfo[iTheta].dcdz,inInfo[iTheta].d2cdz2);		
	}
}	

kernel void takeStepNotConstSSP(global RayIterator2* inRays, global RayIterator2 *outRays, global PointInfo *inInfo) {
    unsigned int iTheta = get_global_id(0);
	if (!inRays[iTheta].terminated) {
		outRays[iTheta] = rk4StepMCK2(inRays[iTheta],inInfo[iTheta].ds,inInfo[iTheta].c,inInfo[iTheta].dcdz,inInfo[iTheta].d2cdz2);
	}
}


