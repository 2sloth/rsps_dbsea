#pragma OPENCL EXTENSION cl_khr_fp64 : enable    //requires checking that the graphics card can support double calcs
//#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable

//void atomic_add_global(volatile global double *source, const double operand) {
//    union {
//        unsigned long intVal;
//        double floatVal;
//    } newVal;
//    union {
//        unsigned long intVal;
//        double floatVal;
//    } prevVal;
//
//    do {
//        prevVal.floatVal = *source;
//        newVal.floatVal = prevVal.floatVal + operand;
//    } while (atomic_cmpxchg((volatile global unsigned long *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
//}

double myAbs(double d) {
	if (d < 0) {
		return -d;
	} else {
		return d;
	}
}

double myMin(double a, double b) {
	if (a < b) {
		return a;
	} else {
		return b;
	}
}

double myMax(double a, double b) {
	if (a > b) {
		return a;
	} else {
		return b;
	}
}

typedef struct tag_my_complex
{
    double r;
    double i;
} MyComplex;

MyComplex makeComplex(double r, double i) {
	MyComplex q;
	q.r = r;
	q.i = i;
	return q;
}

double modulus(MyComplex z) {
    return sqrt(z.r*z.r + z.i*z.i);
}

MyComplex negativeComplex(MyComplex z) {
	return makeComplex(-z.r,-z.i);
}

MyComplex addComplex(MyComplex a, MyComplex b) {
	return makeComplex(a.r + b.r,a.i + b.i);;
}

double complexArgument(MyComplex z) {
	return atan2(z.i,z.r);
}

MyComplex mulComplexByI(MyComplex z) {
	return makeComplex(-z.i,z.r);
}

MyComplex multByDouble(MyComplex z, double d) {
	return makeComplex(z.r*d,z.i*d);
}

MyComplex mulComplex(MyComplex a, MyComplex b) {
	return makeComplex(a.r * b.r - a.i * b.i, a.r * b.i + a.i * b.r);
}

MyComplex divComplex(MyComplex a, MyComplex b) {
    double descrim = b.r * b.r + b.i * b.i;
    if (descrim != 0) {
        return makeComplex((a.r*b.r+a.i*b.i)/descrim, (a.i*b.r-a.r*b.i)/descrim);
    } else {
		return makeComplex(0,0);
	}
}

MyComplex complexSQRT(MyComplex z) {
    if ((z.r != 0) || (z.i != 0)) {
        double temp1 = sqrt(0.5*(myAbs(z.r) + modulus(z)));
        double temp2 = z.i/(2*temp1);
        if (z.r >= 0) {
            return makeComplex(temp1,temp2);
        } else if (z.i < 0) {
            return makeComplex(-temp2,-temp1);
        } else {
            return makeComplex(temp2,temp1);
        }
    } else {
        return makeComplex(0,0);
	}
}

MyComplex complexLN(MyComplex z) {
	double temp = z.r * z.r + z.i * z.i;
	if (temp == 0) {
		return makeComplex(NAN,NAN);
	} else {
		return makeComplex(log(temp),complexArgument(z));
	}
}

MyComplex complexARGCOSH(MyComplex z) {
	MyComplex t = mulComplex(z,z);
	t.r = 1.0 - t.r;
	t.i = -t.r;
	t = complexSQRT(t);
	t = mulComplexByI(t);
	t = addComplex(z,t);
	t = complexLN(t);
	return negativeComplex(t);
}

MyComplex complexARCCOS(MyComplex z) {
	MyComplex q = complexARGCOSH(z);
	return mulComplexByI(q); 
}

MyComplex complexSIN(MyComplex z) {
	return makeComplex(sin(z.r)*cosh(z.i),cos(z.r)*sinh(z.i));
}

MyComplex complexEXP(MyComplex z) {
    double temp = exp(z.r); // for Speed UP
    return makeComplex(temp*cos(z.i), temp*sin(z.i));
}

//-----------------------------------------

typedef struct tag_my_seafloorReflectionLayer
{
	double zb; //valid at this depth
    double rho;
	double c;
	double h;
	double attenuation;
    double k;
    MyComplex phi;
	MyComplex theta;
	MyComplex Z;
} SeafloorReflectionLayer;

typedef struct tag_in_struct
{
    //int iTheta;
    int nf;
    int nz;
    int nr;
	int nzb; //n points in z bottom profile
    int constSSPInt;
	int maxBottomReflections;
	int outputArrivalsInt;
	//int lenPField;
    double srcZ;
    double c0;
    double ds;
	double rmax;
	double deltaR;
	double cb;
    double delTheta;
    double deltaZ;
	double rho0;
	double rhob;
} InStruct;

typedef struct tag_ray_iterator
{
    double r;
    double z;
    double zeta;
    double q;
    double p;
} RayIterator;

#define RETURN_N __RETURN_N__

typedef struct tag_out_struct
{
    int rayReturn;
	int stepCount;
	//double x;
	//double y;
	double theta0;
	double xs[RETURN_N];
	double ys[RETURN_N];
	double zetas[RETURN_N];
	double qs[RETURN_N];
	double ps[RETURN_N];
} OutStruct;

//------------------------------------------------------------


int mySign(double a) {
	return a > 0;
}

int getIZ(double z, int nz) {
	int iz = round(z);
	if (iz < 0) {
		iz = 0;
	} else if (iz > nz - 1) {
		iz = nz - 1;
	}
	return iz;
}

void rayIteratorMultiplyBy(RayIterator x, double ds) {
	z.r *= d;
	z.z *= d;
	z.zeta *= d;
	z.q *= d;
	z.p *= d;
}

RayIterator rayf2(RayIterator x0, double c, double dcdz, double d2cdz2, double xi) {
    //int iz = round(x0.z);
    //if (iz < 0) {
    //    iz = 0;
    //} else if (iz > nz - 1) {
    //    iz = nz - 1;
    //}
    //double cHere = c[iz];
    double c2 = c*c; //cHere*cHere;
    double c2inv = 1/c2;
    double cnn = c2*(d2cdz2*xi*xi); 

	RayIterator dx;
    dx.r = cHere*xi; //dr/ds := c*xi //eq 3.23
    dx.z = cHere*x0.zeta; //dz/ds := c*zeta //eq 3.24
    dx.zeta = -dcdz*c2inv;   //dzeta/ds := -1/c^2*(dc/dz) //eq 3.24
    dx.q = cHere*x0.p; //dq/ds := c*p //eq 3.58
    dx.p = -cnn*c2inv*x0.q; //dp/ds := -cnn/c2*q //eq 3.58
	
	//NB xi doesn't change if dc/dr = 0
	return dx;
}

RayIterator addIterator(RayIterator x, RayIterator dx, double ds) {
	RayIterator z;
    z.r = x.r + dx.r * ds;
	z.z = x.z + dx.z * ds;
	z.zeta = x.zeta + dx.zeta * ds;
	z.q = x.q + dx.q * ds;
	z.p = x.p + dx.p * ds;
	return z;
}

RayIterator rk2StepMCK(RayIterator x0, double ds, double xi, double c, double dcdz, double d2cdz2) {
    RayIterator k1 = rayf2(x0 ,c,dcdz,d2cdz2,xi);
	RayIterator tmp = addIterator(x0,k1,ds/2);

    RayIterator k2 = rayf2(tmp,c,dcdz,d2cdz2,xi);
	RayIterator x1 = addIterator(x0,k2,ds);
	return x1;
}

RayIterator rk4StepMCK(RayIterator x0, double ds, double xi, double c, double dcdz, double d2cdz2) {
	RayIterator k1 = rayf2(x0 ,c,dcdz,d2cdz2,xi);
	rayIterator2MultiplyBy(k1,ds);
	RayIterator tmp1 = addIterator(x0,k1,1/2);	

	RayIterator k2 = rayf2(tmp1,c,dcdz,d2cdz2,xi);
    rayIterator2MultiplyBy(k2,ds);
	RayIterator tmp2 = addIterator(x0,k2,1/2);
	
	RayIterator k3 = rayf2(tmp2,c,dcdz,d2cdz2,xi);
    rayIterator2MultiplyBy(k3,ds);
	RayIterator tmp3 = addIterator(x0,k3,1);

	RayIterator k4 = rayf2(tmp3,c,dcdz,d2cdz2,xi);    

	RayIterator x1;
	x1.r = x0.r + 0.16666667*k1.r + 0.33333333*k2.r + 0.33333333*k3.r + 0.16666667*k4.r*ds;
	x1.z = x0.z + 0.16666667*k1.z + 0.33333333*k2.z + 0.33333333*k3.z + 0.16666667*k4.z*ds;
	x1.zeta = x0.zeta + 0.16666667*k1.zeta + 0.33333333*k2.zeta + 0.33333333*k3.zeta + 0.16666667*k4.zeta*ds;
	x1.q = x0.q + 0.16666667*k1.q + 0.33333333*k2.q + 0.33333333*k3.q + 0.16666667*k4.q*ds;
	x1.p = x0.p + 0.16666667*k1.p + 0.33333333*k2.p + 0.33333333*k3.p + 0.16666667*k4.p*ds;
	return x1;
}

int rayTerminateFromHitSurface(RayIterator xnext, RayIterator xprev, int nf, double *reflectionAmplitudePhaseReal, double *reflectionAmplitudePhaseImag, double *c, int nz, int inAir, double xi) {
	int b;
    if (xnext.z < 0) {
        //ray hit the surface
        b = inAir;
        inAir = 1;

        double stepdz = xnext.z - xprev.z;
        double stepdr = xnext.r - xprev.r;
        double inTheta = atan2(stepdz,stepdr);
        double outTheta = -inTheta; //eq 3.173
        double cHere = c[getIZ(round(xprev.z), nz)]; 
        xi = cos(outTheta)/cHere;
        xnext.r = xprev.r - xprev.r / stepdz * stepdr;
        xnext.z = 0.0;
        xnext.zeta = sin(outTheta) / cHere; 

        for (int fi = 0; fi < nf; fi++) {
            reflectionAmplitudePhaseReal[fi] *= -reflectionAmplitudePhaseReal[fi]; //phase change on reflection
            reflectionAmplitudePhaseImag[fi] *= -reflectionAmplitudePhaseImag[fi]; 
        }
    } else {
        inAir = 0;
        b = 0;
    }
	
	return b;
}


void getdepth(double *r_zb, double *zb, int nr, double r, double z, double ang, double m, double c) {
	int len = nr - 1;
	int i;
	if (r <= 0.0) {
		i = 0;
		z = zb[i];
		ang = 0.0;
		m = 0.0;
	} else if (r >= r_zb[len]) {
		i = len;
		z = zb[i];
		ang = 0.0;
		m = 0.0;
	} else {
		i = ceil(r / (r_zb[1] - r_zb[0]));

		if ((i == 0) || (i >= len)) {
			z = zb[i];
			ang = 0;
			m = 0;
		} else {
			i = i - 1; //get the point before the interp point
			z = zb[i] + (zb[i + 1] - zb[i]) * (r - r_zb[i]) / (r_zb[i + 1] - r_zb[i]);
			double thisdz = zb[i + 1] - zb[i];
			double thisdr = r_zb[i + 1] - r_zb[i];
			ang = atan2(thisdz,thisdr);
			m = thisdz/thisdr; //y := mx + c
		}
	}
	c = zb[i] - m*r_zb[i]; //y := mx + c
}


int rayTerminateFromHitBottom(RayIterator xnext, RayIterator xprev, int nf, int maxBottomReflections, double *reflectionAmplitudePhaseReal, double *reflectionAmplitudePhaseImag, double c0, double cb, double rho0, double rhob, double *f, double *c, int nz, double *r_zb, double *zb, int nr, int inSeafloor, double xi, double thisZb, int nReflections) {
													//const pSeafloor : pSeafloorScheme;
    //get bottom info at this point
	double botTheta, mbot, cbot;
    getdepth(r_zb,zb,xnext.r,nr,thisZb,botTheta,mbot,cbot);

	int b;
    if (thisZb <= 0) {
        b = 1; //stop ray at 0 depth
    } else if (xnext.z > thisZb) { //todo interpolate zb for range
        //{hit the bottom}
        b = inSeafloor;
        inSeafloor = 1;
        nReflections += 1;

        //kill ray if it has had more than the max number of reflections
        if (nReflections >= maxBottomReflections) {
            b = 1;
		}

        double stepdz = xnext.z - xprev.z;
        double stepdr = xnext.r - xprev.r;       
		double inTheta = atan2(stepdz,stepdr);
        double cHere = c[getIZ(round(xprev.z), nz)]; //todo average for this ray
        double theta1 = inTheta - botTheta;
        double outTheta = -(inTheta - 2*botTheta);
        double mwat = stepdz/stepdr;
        double cwat = xprev.z - mwat*xprev.r;

        xi = cos(outTheta)/cHere;
        xnext.r = (cwat - cbot)/(mbot - mwat);
        xnext.z = mwat*xnext.r + cwat;
        xnext.zeta = sin(outTheta)/cHere; //zeta
		
		int useOnlyFirstLayer = 1;
		if (useOnlyFirstLayer) {
			double cmp1 = rho0*c0*sin(theta1); //only real
			MyComplex theta2c = complexARCCOS(makeComplex(cb/c0*cos(theta1),0)); //input range can be outside 0 to 1, so we need complex
			MyComplex cmp2 = multByDouble(complexSIN(theta2c),rhob*cb);
			for (int fi = 0; fi < nf; fi++) {
				MyComplex q = makeComplex(reflectionAmplitudePhaseReal[fi],reflectionAmplitudePhaseImag[fi]);
				MyComplex q1 = makeComplex(cmp2.r - cmp1,cmp2.i);
				MyComplex q2 = makeComplex(cmp2.r + cmp1,cmp2.i);
				q = mulComplex(q, divComplex(q1,q2));
				reflectionAmplitudePhaseReal[fi] = q.r;
				reflectionAmplitudePhaseImag[fi] = q.i;	
			}
		} else {		
			//layers = TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(pSeafloor^,thisZb);
			//for (int fi = 0; fi < nf; fi++) {
			//	double rCoeffr, rCoeffi; 
		    //	TMultiLayerReflectionCoefficient.calcR(f[fi], theta1, layers, rCoeffr, rCoeffi); //TODO
			//  double tmp = reflectionAmplitudePhaseReal[fi]; 
			// reflectionAmplitudePhaseReal[fi] = reflectionAmplitudePhaseReal[fi] * rCoeffr - reflectionAmplitudePhaseImag[fi] * rCoeffi;
			//	reflectionAmplitudePhaseImag[fi] = tmp                             * rCoeffi + reflectionAmplitudePhaseImag[fi] * rCoeffr;
			//}
		}
    } else {
        inSeafloor = 0;
        b = 0;
    }
	return b;
}

void calculateRayParameters(double *c, int nz, double *f, RayIterator xprev, RayIterator xnext, int nf, double delTheta, double thisZb, double c0, double cosTheta0, double *rayPhase, double *rayWidth, double *ABeam, int *bPastRayWidthXOver, double delay) {
    int iz1 = getIZ(round(xnext.z), nz);
    int iz2 = getIZ(round(xprev.z), nz);
    double cHere = (c[iz1] + c[iz2])/2; //average c for this step
    double stepDist = hypot(xnext.r - xprev.r, xnext.z - xprev.z);
    delay += stepDist / cHere; //keep track of time delay, for arrivals calc

    for (int fi = 0; fi < nf; fi++) {
        rayPhase[fi] += (2 * M_PI * f[fi])/cHere*stepDist;

        if (sign(xnext.q) != sign(xprev.q)) {
            rayPhase[fi] += M_PI;  //phase change at caustics
		}

        rayWidth[fi] = myAbs(xnext.q*delTheta);

        if (!bPastRayWidthXOver[fi]) {
            if (rayWidth[fi] > M_PI*cHere/f[fi]) { 
                bPastRayWidthXOver[fi] = 1;
			}
		}

        if (bPastRayWidthXOver[fi]) {
			double d = M_PI*cHere/f[fi];
			if (d > rayWidth[fi]) {
				rayWidth[fi] = d;
			}
		}

        ABeam[fi] = 1/pow(2*M_PI,0.25)*sqrt(delTheta/xnext.r*cHere/c0*2*cosTheta0/rayWidth[fi]);
		if (thisZb < rayWidth[fi]) {
			rayWidth[fi] = thisZb; //reduce ray width when the channel is shallow. but keep intensity as it would have been
		}
    }
}

double sqLineMagnitude(double dx, double dy) {
    return dx * dx + dy * dy;
}

double distPointToLineSegment(double px, double py, double x1, double y1, double x2, double y2) {
	double EPS    = 0.000001;         // smallest positive value: less than that to be considered zero
    double EPSEPS = EPS * EPS;
	double dx, dy;
	dx = x2 - x1;
	dy = y2 - y1;
	double sqLineMag = dx * dx + dy * dy;//sqLineMagnitude(x2 - x1, y2 - y1);
	if (sqLineMag < EPSEPS) {
		return -1.0;
	}

	double u = ( (px - x1)*(x2 - x1) + (py - y1)*(y2 - y1) ) / sqLineMag;

	double r;
	if ((u < EPS) || (u > 1)) {
		//  Closest point does not fall within the line segment,
		//    take the shorter distance to an endpoint
		dx = px - x1;
		dy = py - y1;
		double ix = dx * dx + dy * dy;//sqLineMagnitude(px - x1, py - y1);
		dx = px - x2;
		dy = py - y2;
		double iy = dx * dx + dy * dy;//sqLineMagnitude(px - x2, py - y2);
		if (ix < iy) {
			r = ix;
		} else {
			r = iy;
		}
	} else {
		//  Intersecting point is on the line, use the formula
		double ix = x1 + u * (x2 - x1);
		double iy = y1 + u * (y2 - y1);
		dx = px - ix;
		dy = py - iy;
		r = dx * dx + dy * dy; //sqlineMagnitude(px - ix, py - iy);
	} //  else NOT (u < EPS) or (u > 1)

	// finally convert to actual distance not its square
	return sqrt(r);
}

//#define A(x,y) a[x*width + y]

void updateFieldOrArrivalsFromRay(RayIterator xprev, RayIterator xnext, double deltaR, double deltaZ, double delay, double *rayWidth, double *rayPhase, double *ABeam, int nf, int nz, int nr, int outputArrivals, double * reflectionAmplitudePhaseReal, double * reflectionAmplitudePhaseImag, MyComplex *pField) { //arrivals : TArrivalsField; 
	int nznr = nz * nr;
	int lenPField = nznr * nf;
    double startR = xprev.r;
    double endR = xnext.r;
    //get the approx start and end range indices (much faster than looping over all range indices)
    int irStart = floor(startR / deltaR) - 1;
	if (irStart < 0) {
		irStart = 0;
	}
    int irEnd = ceil(endR / deltaR) + 1;
	if (irEnd > nr - 1) {
		irEnd = nr - 1;
	}
    for (int ir = irStart; ir <= irEnd; ir++) {
		double thisR = ir * deltaR;
        if ((thisR >= startR) || (thisR < endR)) {
            for (int fi = 0; fi < nf; fi++) {
                //{check all the z points that are within the range of the raywidth}
				double xmin = myMin(xprev.z,xnext.z);
				int iz1 = floor((xmin - 2*rayWidth[fi])/deltaZ);
				if (iz1 < 0) {
					iz1 = 0;
				}
				double xmax = myMax(xprev.z,xnext.z);
				int iz2 = ceil((xmax + 2*rayWidth[fi])/deltaZ);
				if (iz2 > nz - 1) {
					iz2 = nz - 1;
				}
                for (int iz = iz1; iz <= iz2; iz++) {
                    double distToRay = distPointToLineSegment(thisR, iz * deltaZ, startR, xprev.z, endR, xnext.z); //takes account of line segment endpoints

                    if (distToRay < 2*rayWidth[fi]) { //2*waywidth gets us to 2 StdDev, which is around -30 dB
                        double magnitude = exp(-pow(distToRay/rayWidth[fi],2)); //gaussian -- we can speed this line up

						if (outputArrivals) {
							//TODO
						} else {
							//{add the contribution from this ray at this point to the output field}
							MyComplex q1 = makeComplex(reflectionAmplitudePhaseReal[fi],reflectionAmplitudePhaseImag[fi]);
							multByDouble(q1,ABeam[fi]*magnitude);
							MyComplex q3 = makeComplex(0,rayPhase[fi]);
							MyComplex q2 = complexEXP(q3);
							MyComplex q = mulComplex(q1,q2);
							
							int pFieldInd = fi * nznr  +  iz * nr  +  ir;
							if (pFieldInd < lenPField) {
								pField[pFieldInd].r += q.r;
								pField[pFieldInd].i += q.i;
								//atomic_add_global(&pField[pFieldInd].r, q.r);
								//atomic_add_global(&pField[pFieldInd].i, q.i);
							}
						}
                    } //distToRay <
                } //iz
            } //fi
        } //ir in range
    } //ir
}


#define MAXF __MAXF__
#define MAXR __MAXR__
#define MAXZ __MAXZ__

kernel void runRay(global constant InStruct* inStruct, global OutStruct* outStruct, constant double* theta, constant double * f, constant double* inr_zb, constant double* inzb, constant float* inDirectivity, constant double* inc, constant double* indcdz, constant double* ind2cdz2, global MyComplex *pField) {
    unsigned int iTheta = get_global_id(0);

    private int nf = inStruct[0].nf;
    private int nz = inStruct[0].nz;
	private int nr = inStruct[0].nr;
	private int nzb = inStruct[0].nzb;
	private int constSSP = inStruct[0].constSSPInt;
	private int maxBottomReflections = inStruct[0].maxBottomReflections;
	private int outputArrivals = inStruct[0].outputArrivalsInt;
    private int nReflections, inAir, inSeafloor;
	int nznr = nz * nr;
	int lenPField = nf * nznr;

    double c0 = inStruct[0].c0;
    double ds = inStruct[0].ds;
	double rmax = inStruct[0].rmax;
	double deltaR = inStruct[0].deltaR;
	double cb = inStruct[0].cb;
	double delTheta = inStruct[0].delTheta;
	double deltaZ = inStruct[0].deltaZ;
	double rho0 = inStruct[0].rho0;
	double rhob = inStruct[0].rhob;
	double theta0 = theta[iTheta];
    double delay, xi, cosTheta0;
	//double tmp, tmp1;

	double r_zb[MAXR], zb[MAXR];
	for (int i = 0; i < nzb; i++) {
		r_zb[i] = inr_zb[i];
		zb[i] = inzb[i];
	}
		
    double c[MAXZ], dcdz[MAXZ], d2cdz2[MAXZ]; 
    for (int i = 0; i < nz; i++) {
        c[i] = inc[i];
        dcdz[i] = indcdz[i];
        d2cdz2[i] = ind2cdz2[i];
    }
	
	double ABeam[MAXF], rayWidth[MAXF], reflectionAmplitudePhaseReal[MAXF], reflectionAmplitudePhaseImag[MAXF], rayPhase[MAXF];
	int bPastRayWidthXOver[MAXF];
    for (int fi = 0; fi < nf; fi++) {
        reflectionAmplitudePhaseReal[fi] = pow(10, -0.05 * inDirectivity[fi]); //amplitude from the directivity: NB -ve
        reflectionAmplitudePhaseImag[fi] = 0.0; 
        rayPhase[fi] = 0.0;
        rayWidth[fi] = 0.0;
		ABeam[fi] = 0.0;
		bPastRayWidthXOver[fi] = 0;
    }
    delay = 0.0; //keeping track of time taken, for arrivals calc
    
	////{ray initial conditions: r,z,zeta,q,p //xi only changes at reflections so we don't integrate it}
    RayIterator xprev;
    RayIterator xnext;
    xnext.r = 0.0;
    xnext.z = inStruct[0].srcZ;
    xnext.zeta = sin(theta0) / c0; //eq 3.28
    xnext.q = 0.0;
    xnext.p = 1.0 / c0;
    
    cosTheta0 = cos(theta0);
    xi = cosTheta0 / c0; //eq 3.27
    inSeafloor = 0;
    inAir = 0;
    nReflections = 0;
    
    //output ints
    outStruct[iTheta].rayReturn = 0;
	
    // start loop
	//int stepCount = 0;
	outStruct[iTheta].stepCount = 0;
	if (outStruct[iTheta].stepCount < RETURN_N) {
		outStruct[iTheta].theta0 = theta[iTheta];
		outStruct[iTheta].xs[outStruct[iTheta].stepCount] = xnext.r;
		outStruct[iTheta].ys[outStruct[iTheta].stepCount] = xnext.z;
		outStruct[iTheta].zetas[outStruct[iTheta].stepCount] = xnext.zeta;
		outStruct[iTheta].qs[outStruct[iTheta].stepCount] = xnext.q;
		outStruct[iTheta].ps[outStruct[iTheta].stepCount] = xnext.p;
	}
	int bRayTerminate = 0;
    while (!bRayTerminate) {
        //for (int i = 0; i < 5; i++) {
        //    xprev[i] = xnext[i];
        //}
		xprev.r = xnext.r;
		xprev.z = xnext.z;
		xprev.zeta = xnext.zeta;
		xprev.q = xnext.q;
		xprev.p = xnext.p;
		
        // {take a step, via 2nd or 4th order runge kutta (depending on whether the SSP is const or not) // TODO smaller stepsize accuracy checking/rk-fehlberg}
		int iz = round(x0.z);
		if (iz < 0) {
			iz = 0;
		} else if (iz > nz - 1) {
			iz = nz - 1;
		}
        if (constSSP) {
            xnext = rk2StepMCK(xprev,ds,xi,c[iz],dcdz[iz],d2cdz2[iz]);
        } else {
            xnext = rk4StepMCK(xprev,ds,xi,c[iz],dcdz[iz],d2cdz2[iz]);
        }	
		
		if (xnext.z < 0) {
			//ray hit the surface
			bRayTerminate = inAir;
			if (bRayTerminate) {
				outStruct[iTheta].rayReturn = 1;
			}
			inAir = 1;

			double stepdz = xnext.z - xprev.z;
			double stepdr = xnext.r - xprev.r;
			double inTheta = atan2(stepdz,stepdr);
			double outTheta = -inTheta;
			double cHere = c[getIZ(round(xprev.z), nz)]; 
			xi = cos(outTheta)/cHere;
			xnext.r = xprev.r - (xprev.z / stepdz * stepdr);
			xnext.z = 0.0;
			xnext.zeta = sin(outTheta) / cHere;    //zeta

			for (int fi = 0; fi < nf; fi++) {
				reflectionAmplitudePhaseReal[fi] *= -reflectionAmplitudePhaseReal[fi]; //phase change on reflection
				reflectionAmplitudePhaseImag[fi] *= -reflectionAmplitudePhaseImag[fi]; 
			}
		} else {
			inAir = 0;
		}

		double thisZb, botTheta, mbot, cbot;
		getdepth(r_zb,zb,xnext.r,nr,thisZb,botTheta,mbot,cbot);
		if (thisZb <= 0) {
			bRayTerminate = 1; //stop ray at 0 depth
			outStruct[iTheta].rayReturn = 2;
		} else if (xnext.z > thisZb) { //todo interpolate zb for range
			//{hit the bottom}
			bRayTerminate = inSeafloor;
			if (bRayTerminate) {
				outStruct[iTheta].rayReturn = 3;
			}
			inSeafloor = 1;
			nReflections += 1;

			//kill ray if it has had more than the max number of reflections
			if (nReflections >= maxBottomReflections) {
				bRayTerminate = 1;
				outStruct[iTheta].rayReturn = 4;
			}

			double stepdz = xnext.z - xprev.z;
			double stepdr = xnext.r - xprev.r;
			double inTheta = atan2(stepdz,stepdr);
			double cHere = c[getIZ(round(xprev.z), nz)]; //todo average for this ray
			double theta1 = inTheta - botTheta;
			double outTheta = -(inTheta - 2*botTheta);
			double mwat = stepdz/stepdr;
			double cwat = xprev.z - mwat*xprev.r;

			xi = cos(outTheta)/cHere;
			xnext.r = (cwat - cbot)/(mbot - mwat);
			xnext.z = mwat*xnext.r + cwat;
			xnext.zeta = sin(outTheta)/cHere; //zeta
			
			int useOnlyFirstLayer = 1;
			if (useOnlyFirstLayer) {
				double cmp1 = rho0 * c0 * sin(theta1); //only real
				MyComplex theta2c = complexARCCOS(makeComplex(cos(theta1)*cb/c0,0)); //input range can be outside 0 to 1, so we need complex
				MyComplex tmpc2 = complexSIN(theta2c);
				MyComplex cmp2 = multByDouble(tmpc2,rhob*cb);
				for (int fi = 0; fi < nf; fi++) {
					MyComplex q = makeComplex(reflectionAmplitudePhaseReal[fi],reflectionAmplitudePhaseImag[fi]);
					MyComplex q1 = makeComplex(cmp2.r - cmp1,cmp2.i);
					MyComplex q2 = makeComplex(cmp2.r + cmp1,cmp2.i);
					MyComplex q3 = divComplex(q1,q2);
					q = mulComplex(q, q3);
					reflectionAmplitudePhaseReal[fi] = q.r;
					reflectionAmplitudePhaseReal[fi] = q.i;	
				}
			} else {		
				//layers = TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(pSeafloor^,thisZb);
				//for (int fi = 0; fi < nf; fi++) {
				//	double rCoeffr, rCoeffi; 
				//	TMultiLayerReflectionCoefficient.calcR(f[fi], theta1, layers, rCoeffr, rCoeffi); //TODO
				// reflectionAmplitudePhaseReal[fi] = reflectionAmplitudePhaseReal[fi] * rCoeffr - reflectionAmplitudePhaseImag[fi] * rCoeffi;
				//	reflectionAmplitudePhaseReal[fi] = reflectionAmplitudePhaseReal[fi] * rCoeffi + reflectionAmplitudePhaseImag[fi] * rCoeffr;
				//}
			}
		} else {
			inSeafloor = 0;
		}

		if ((xnext.r < 0) || (xnext.r > rmax)) { // {range checks - range < 0 means the ray exited the bounding box to the left}
            bRayTerminate = 1;
			outStruct[iTheta].rayReturn = 5;
		}

		
		
		if (!bRayTerminate) {
			//calculateRayParameters(c, nz, f, xprev, xnext, nf, delTheta, thisZb, c0, cosTheta0, rayPhase, rayWidth, ABeam, bPastRayWidthXOver, delay);
			int iz1 = getIZ(round(xnext.z), nz);
			int iz2 = getIZ(round(xprev.z), nz);
			double cHere = (c[iz1] + c[iz2])/2; //average c for this step
			double stepDist = hypot(xnext.r - xprev.r, xnext.z - xprev.z);
			delay += stepDist / cHere; //keep track of time delay, for arrivals calc

			for (int fi = 0; fi < nf; fi++) {
				rayPhase[fi] += (2 * M_PI * f[fi] / cHere)*stepDist;

				if (sign(xnext.q) != sign(xprev.q)) {
					rayPhase[fi] += M_PI;  //phase change at caustics
				}
 
				rayWidth[fi] = myAbs(xnext.q*delTheta); //eq 3.74

				if (!bPastRayWidthXOver[fi]) {
					if (rayWidth[fi] > M_PI*cHere/f[fi]) { 
						bPastRayWidthXOver[fi] = 1;
					}
				}

				if (bPastRayWidthXOver[fi]) {
					double d = M_PI*cHere/f[fi];
					if (d > rayWidth[fi]) {
						rayWidth[fi] = d;
					}
				}

				ABeam[fi] = 1/pow(2*M_PI,0.25)*sqrt(delTheta/xnext.r*cHere/c0*2*cosTheta0/rayWidth[fi]); //eq 3.76
				if (thisZb < rayWidth[fi]) {
					rayWidth[fi] = thisZb; //reduce ray width when the channel is shallow. but keep intensity as it would have been
				}
			} //fi
			
			//updateFieldOrArrivalsFromRay(xprev, xnext, deltaR, deltaZ, delay, rayWidth, rayPhase, ABeam, nf, nz, nr, outputArrivals, reflectionAmplitudePhaseReal,reflectionAmplitudePhaseImag);    
			double startR = xprev.r;
			double endR = xnext.r;
			//get the approx start and end range indices (much faster than looping over all range indices)
			int irStart = floor(startR / deltaR) - 1;
			if (irStart < 0) {
				irStart = 0;
			}
			int irEnd = ceil(endR / deltaR) + 1;
			if (irEnd > nr - 1) {
				irEnd = nr - 1;
			}
			for (int ir = irStart; ir <= irEnd; ir++) {
				double thisR = ir * deltaR;
				
				if ((thisR >= startR) || (thisR < endR)) {
					for (int fi = 0; fi < nf; fi++) {
						//{check all the z points that are within the range of the raywidth}
						double xmin = myMin(xprev.z,xnext.z);
						int iz1 = floor((xmin - 2*rayWidth[fi])/deltaZ);
						if (iz1 < 0) {
							iz1 = 0;
						}
						double xmax = myMax(xprev.z,xnext.z);
						int iz2 = ceil((xmax + 2*rayWidth[fi])/deltaZ);
						if (iz2 > nz - 1) {
							iz2 = nz - 1;
						}
						for (int iz = iz1; iz <= iz2; iz++) {
							double distToRay = distPointToLineSegment(thisR, iz * deltaZ, startR, xprev.z, endR, xnext.z); //takes account of line segment endpoints

							if (distToRay < 2*rayWidth[fi]) { //2*waywidth gets us to 2 StdDev, which is around -30 dB
								double magnitude = exp(-pow(distToRay/rayWidth[fi],2)); //gaussian -- we can speed this line up  //eq 3.75

								if (outputArrivals) {
									//TODO
								} else {
									//{add the contribution from this ray at this point to the output field} //eq 3.72
									MyComplex q1 = makeComplex(reflectionAmplitudePhaseReal[fi],reflectionAmplitudePhaseImag[fi]);
									multByDouble(q1,ABeam[fi]*magnitude);
									MyComplex q3 = makeComplex(0,rayPhase[fi]);
									MyComplex q2 = complexEXP(q3);
									MyComplex q = mulComplex(q1,q2);
									//pFieldi[fi,iz,ir] += mulComplex(q1,q2);
									
									int pFieldInd = fi * nznr;
									pFieldInd += iz * nr;
									pFieldInd += ir;
									if (pFieldInd < lenPField) {
										pField[pFieldInd].r += q.r;
										pField[pFieldInd].i += q.i;
										//atomic_add_global(&pField[pFieldInd].r, q.r);
										//atomic_add_global(&pField[pFieldInd].i, q.i);
									}
									//pFieldi[0] = 654.9;//q.i;
									//pFieldi[fi,iz,ir] += complexEXP(makeComplex(0,rayPhase[fi])) *
									//                                                   makecomplex(reflectionAmplitudePhaseReal[fi],reflectionAmplitudePhaseImag[fi]) *
									//                                                    (ABeam[fi]*magnitude); //includes accumulated reflection amplitude/phase
								}
							} //distToRay <
						} //iz
					} //fi
				} //ir in range
			} //ir									
		}
		
		//pField[0].r = 321.0;
		//pField[0].i = 456.0;
		//pField[1].r = 999.0;
		//pField[1].i = 888.0;
		
		//if (iTheta < 10) {
		//	pField[iTheta].r = iTheta;
		//}
				
		//outStruct[iTheta].x = xnext.r;
		//outStruct[iTheta].y = xnext.z;
		
		outStruct[iTheta].stepCount += 1;
		
		if (outStruct[iTheta].stepCount < RETURN_N) {
			outStruct[iTheta].xs[outStruct[iTheta].stepCount] = xnext.r;
			outStruct[iTheta].ys[outStruct[iTheta].stepCount] = xnext.z;
			outStruct[iTheta].zetas[outStruct[iTheta].stepCount] = xnext.zeta;
			outStruct[iTheta].qs[outStruct[iTheta].stepCount] = xnext.q;
			outStruct[iTheta].ps[outStruct[iTheta].stepCount] = xnext.p;
		}
		
		if (outStruct[iTheta].stepCount > 30) {
			bRayTerminate = 1;
			outStruct[iTheta].rayReturn = 6;
		}
    }	
}

