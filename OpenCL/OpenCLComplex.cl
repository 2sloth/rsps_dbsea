#pragma OPENCL EXTENSION cl_khr_fp64 : enable    //requires checking that the graphics card can support double calcs

typedef struct tag_my_complex
{
    double r;
    double i;
} MyComplex;

MyComplex makeComplex(double r, double i) {
	MyComplex q;
	q.r = r;
	q.i = i;
	return q;
}

double modulus(MyComplex z) {
    return sqrt(z.r*z.r + z.i*z.i);
}

MyComplex negativeComplex(MyComplex z) {
	return makeComplex(-z.r,-z.i);
}

MyComplex addComplex(MyComplex a, MyComplex b) {
	return makeComplex(a.r + b.r,a.i + b.i);;
}

double complexArgument(MyComplex z) {
	return atan2(z.i,z.r);
}

MyComplex mulComplexByI(MyComplex z) {
	return makeComplex(-z.i,z.r);
}

MyComplex multByDouble(MyComplex z, double d) {
	return makeComplex(z.r*d,z.i*d);
}

MyComplex mulComplex(MyComplex a, MyComplex b) {
	return makeComplex(a.r * b.r - a.i * b.i, a.r * b.i + a.i * b.r);
}

MyComplex divComplex(MyComplex a, MyComplex b) {
    double descrim = b.r * b.r + b.i * b.i;
    if (descrim != 0) {
        return makeComplex((a.r*b.r+a.i*b.i)/descrim, (a.i*b.r-a.r*b.i)/descrim);
    } else {
		return makeComplex(0,0);
	}
}

MyComplex complexSQRT(MyComplex z) {
    if ((z.r != 0) || (z.i != 0)) {
        double temp1 = sqrt(0.5*(abs(z.r) + modulus(z)));
        double temp2 = z.i/(2*temp1);
        if (z.r >= 0) {
            return makeComplex(temp1,temp2);
        } else if (z.i < 0) {
            return makeComplex(-temp2,-temp1);
        } else {
            return makeComplex(temp2,temp1);
        }
    } else {
        return makeComplex(0,0);
	}
}

MyComplex complexLN(MyComplex z) {
	double temp = z.r * z.r + z.i * z.i;
	if (temp == 0) {
		return makeComplex(NAN,NAN);
	} else {
		return makeComplex(log(temp),complexArgument(z));
	}
}

MyComplex complexARGCOSH(MyComplex z) {
	MyComplex t = mulComplex(z,z);
	t.r = 1.0 - t.r;
	t.i = -t.r;
	t = complexSQRT(t);
	t = mulComplexByI(t);
	t = addComplex(z,t);
	t = complexLN(t);
	return negativeComplex(t);
}

MyComplex complexARCCOS(MyComplex z) {
	MyComplex q = complexARGCOSH(z);
	return mulComplexByI(q); 
}

MyComplex complexSIN(MyComplex z) {
	return makeComplex(sin(z.r)*cosh(z.i),cos(z.r)*sinh(z.i));
}

MyComplex complexEXP(MyComplex z) {
    double temp = exp(z.r); // for Speed UP
    return makeComplex(temp*cos(z.i), temp*sin(z.i));
end;

}
