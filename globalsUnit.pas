unit globalsUnit;

interface

uses
    syncommons,
    system.sysutils, system.types, system.classes, system.threading,
    vcl.stdctrls, vcl.forms,
    Winapi.Windows, winapi.mmsystem,
    generics.collections,
    basetypes,
    {$IFNDEF HASP_DIAGNOSTIC}
    SQLiteTable3,
    databaseUpdater,
    eventLogging,
    {$ENDIF}
    {material,}
    dbseaconstants;

type
    TLicenseDBCheckStatus = (kNoInternet,kInProgress,kFinished);

var

  runningMode                               : TRunningMode;
  formatSettings                            : TFormatSettings;
  inputDisabled                             : boolean;
  cLastlblSolveExtraInfoLastUpdated         : cardinal;
  cLastlblSliceNumberLastUpdated            : cardinal;
  cLastlblEstimatedSolveTimeLastUpdated     : cardinal;
  haveAskedAssociateUWA                     : boolean;
  lastAction                                : TLastAction;
  appPath                                   : string;
  logsPath                                  : string;
  tempPath                                  : string;
  defaultSavePath                           : string;
  currentProjectName                        : String;
  currentSaveFile                           : TFilename;
  titleDebugString                          : string;
  slDBpath                                  : string;
  userSlDBpath                              : string;
  GLNoDraw                                  : boolean;
  licenseDBCheckStatus                      : TLicenseDBCheckStatus;

  {$IFNDEF HASP_DIAGNOSTIC}
  MDASldb                                   : TSQLiteDatabase;
  userSldb                                  : TSQLiteDatabase;
  {$ENDIF}

  bMultiCorePE: boolean;
  bMultiCoreModes: boolean;
  bMultiCoreRay: boolean;
  bUseGPU: boolean;
  bPESS: boolean;

  //pool: TThreadPool;
  //pools: TObjectList<TThreadPool>;
  //solveMessages: TMemo;

  procedure globalsCreate(const resourceFileUnbundle: TResourceUnbundleFunction);
  procedure globalsDestroy;
  //procedure cleanupOldPools();
  //procedure addAndInitPool();
  //procedure initPool();

implementation

uses helperFunctions;

{$IFDEF HASP_DIAGNOSTIC}
  procedure globalsCreate(const resourceFileUnbundle: TResourceUnbundleFunction); begin end;
  procedure globalsDestroy; begin end;
{$ELSE}
procedure globalsCreate(const resourceFileUnbundle: TResourceUnbundleFunction);
var
    log: ISynLog;
begin
    log := TSynLogLevs.enter(nil, 'globalsCreate');

    try
        {create the database connection. the database file must exist, otherwise we will run into errors with database queries.}
        slDBPath := TempPath + mdaDBFile;
        {check if the database file is there}
        if not fileExists(slDBPath) then
            resourceFileUnbundle(TempPath, mdaDBFile, 'acoustLibDB');
        if not fileExists(slDBPath) then
        begin
            eventlog.log('Error: Database file not found ' + slDBPath + '. Attempt clearing the temporary directory and restarting the program.');
            raise Exception.Create('Error: Database file not found ' + slDBPath + '. Attempt clearing the temporary directory and restarting the program.');
        end;
        MDASldb := TSQLiteDatabase.Create(slDBPath);

        {create the user database connection. the database file must exist, otherwise we will run into errors with database queries.}
        userSlDBPath := TempPath + userDBFile;
        {check if the database file is there}
        if not fileExists(userSlDBPath) then
            resourceFileUnbundle(TempPath, userDBFile, 'userEmptyDB');
        if not fileExists(userSlDBPath) then
        begin
            eventlog.log('Error: User database file not found ' + userSlDBPath + '. Unable to create database file.');
            raise Exception.Create('Error: User database file not found ' + userSlDBPath + '. Unable to create database file.');
        end;
        userSldb := TSQLiteDatabase.Create(userSlDBPath);

        TDBUpdater.checkForMetaTableInDB(MDASldb,eventlog);
        TDBUpdater.checkForMetaTableInDB(userSldb,eventlog);
        TDBUpdater.checkForCrestFactorInEquiqmentTable(MDASldb,eventlog);
        TDBUpdater.checkForCrestFactorInEquiqmentTable(userSldb,eventlog);

        //TDBUpdater.updateNOAACurves(MDASldb, eventLog);

    except on e: Exception do
    begin
        eventLog.logException(log, e.toString);
        raise e;
    end;
    end;
end;

procedure globalsDestroy;
begin
    MDASldb.Free;
    userSldb.Free;
end;
{$ENDIF}

//procedure cleanupOldPools();
//begin
//    ITask(TTask.create(procedure begin
//        while pools.count > 1 do
//            pools.delete(0);
//    end)).start();
//end;
//
//procedure addAndInitPool();
//begin
//    pool := TThreadPool.create;
//    //pool.SetMaxWorkerThreads(cpucount);
//    pools.add(pool);
//    initpool();
//end;
//
//procedure initPool();
//begin
//    TParallel.for(0,0,procedure(i: integer)
//    var
//        a: integer;
//    begin
//        a := i;
//        thelper.RemoveWarning(a);
//    end,pool);
//end;

initialization
    formatSettings := TFormatSettings.Create(LOCALE_USER_DEFAULT);
    formatSettings.DecimalSeparator := '.'; // make sure we are using . and not ,
    //pools := TObjectList<TThreadPool>.create(true);
    //pool := TThreadPool.create;
    //pools.add(pool);


    {$IFDEF DEBUG}
    bUseGPU := false;
    bPESS := false;
    {$ELSE}
    bUseGPU := false;
    bPESS := false;
    {$ENDIF}

    bMultiCorePE := true;
    bMultiCoreModes := true;
    bMultiCoreRay := true;

    haveAskedAssociateUWA := false;
    lastAction := TLastAction.kNull;
    currentSaveFile := '';
    currentProjectName := '(untitled project)';

    titleDebugString := '';
    {$IFDEF DEBUG}
        titleDebugString := ': Debug mode';
    {$ENDIF}
    {$IFNDEF NON_MDA}
        titleDebugString := ': dBSea/Marshall Day Acoustics version, for dBSea/Marshall Day Acoustics internal use only';
    {$ENDIF}

    // TODO make sure to update the resource file whenever you make changes to solvers, DB etc
    AppPath := ExtractFilePath(Application.ExeName);

    cLastlblSolveExtraInfoLastUpdated := 0;
    cLastlblSliceNumberLastUpdated := 0;
    cLastlblEstimatedSolveTimeLastUpdated := 0;

finalization
    //cleanupOldPools;
    //pools.free;

end.
