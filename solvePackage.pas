unit solvePackage;

interface

uses
    classes, system.Types, basetypes, mComplex, seafloorScheme, stdCtrls,
    generics.collections;

type
    TSolvePackage = class(TObject)
        fi,n,startfi,dmax,maxModes,movingPosIndex : integer;
        f : double;
        angle : double;
        x,y,z : double;
        rmax, dr, zmax : double;
        solver : TCalculationMethod;
        zProfile, cProfile : TMatrix;
        modalStarterZ : TDoubleDynArray;
        modalStarterPsi : TComplexVect;
        seafloorScheme : TSeafloorScheme;
        cancel : pBoolean;
        pcLastlblSolveExtraInfoLastUpdated : pCardinal;
        plblSolveExtraInfo : TLabel;

        useLowFreqSolverText : boolean;
        coherent : boolean;
        assignedToThread : boolean;
        isSolved : boolean;
    end;

    TSolvePackageList = TObjectList<TSolvePackage>;

implementation

end.
