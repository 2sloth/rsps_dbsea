unit filterbank;

{$B-}

interface

uses
    syncommons,
    system.sysutils, system.classes, system.math, system.StrUtils,
    vcl.dialogs,
    DSP, basetypes, spectrum,
    mcklib, types, eventlogging, FilterDef;

type

    TFilterbank = class(TObject)
    private
    var
        f3o: TDoubleDynArray;
        fo: TDoubleDynArray;
        thirdOctBandFilters: array of TFilterDef;
        octBandFilters: array of TFilterDef;

        function thirdOctaveBand(const f0: double; const x: TDoubleDynArray): TDoubleDynArray;
        function octaveBand(const f0: double; const x: TDoubleDynArray): TDoubleDynArray;
    public
    var
        fs: double;

        constructor Create(const afs: double; const aEventlog: TEventLog = nil);
        destructor Destroy; override;

        function band(const bw: TBandwidth; const f0: double; const x: TDoubleDynArray): TDoubleDynArray;
        function bandLevel(const bw: TBandwidth; const f0: double; const x: TDoubleDynArray): double;
    end;

implementation

constructor TFilterbank.Create(const afs: double; const aEventlog: TEventLog = nil);
var
    i: integer;
begin
    inherited create;

    fs := afs;

    setlength(fo,TSpectrum.getLength(bwOctave));
    for i := 0 to length(fo) - 1 do
        fo[i] := TSpectrum.getFbyInd(i,bwOctave,aEventlog);

    setlength(f3o,TSpectrum.getLength(bwThirdOctave));
    for i := 0 to length(f3o) - 1 do
        f3o[i] := TSpectrum.getFbyInd(i,bwThirdOctave,aEventlog);

    setlength(octBandFilters,length(fo));
    for i := 0 to length(octBandFilters) - 1 do
        octBandFilters[i] := TDSP.octbandFilter(fs,fo[i]);

    setlength(thirdOctBandFilters,length(f3o));
    for i := 0 to length(thirdOctBandFilters) - 1 do
        thirdOctBandFilters[i] := TDSP.thirdOctbandFilter(fs,f3o[i]);
end;

destructor TFilterbank.Destroy;
var
    i: integer;
begin
    for i := 0 to length(octBandFilters) - 1 do
        octBandFilters[i].free;
    for i := 0 to length(thirdOctBandFilters) - 1 do
        thirdOctBandFilters[i].free;

    inherited;
end;

function TFilterbank.thirdOctaveBand(const f0: double; const x: TDoubleDynArray): TDoubleDynArray;
var
    i: integer;
begin
    for i := 0 to length(f3o) - 1 do
        if (abs(f0 - f3o[i]) < 0.01) and assigned(thirdOctBandFilters[i]) then
            exit(TDSP.applyFilter3Times(thirdOctBandFilters[i],x));

    setlength(result, length(x));
    result.setAllVal(0);
end;

function TFilterbank.octaveBand(const f0: double; const x: TDoubleDynArray): TDoubleDynArray;
var
    i: integer;
begin
    for i := 0 to length(fo) - 1 do
        if (abs(f0 - self.fo[i]) < 0.01) and assigned(octBandFilters[i]) then
            exit(TDSP.applyFilter3Times(octBandFilters[i],x));

    setlength(result, length(x));
    result.setAllVal(0);
end;

function TFilterbank.band(const bw: TBandwidth; const f0: double; const x: TDoubleDynArray): TDoubleDynArray;
begin
    case bw of
        bwOctave: result := octaveBand(f0,x);
        bwThirdOctave: result := thirdOctaveBand(f0,x);
    end;
end;

function TFilterbank.bandLevel(const bw: TBandwidth; const f0: double; const x: TDoubleDynArray): double;
var
    rms: double;
begin
    rms := TDSP.rms(band(bw,f0,x));
    if rms = 0 then
        result := nan
    else
        result := 20*log10(rms);
end;

end.
