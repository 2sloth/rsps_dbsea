//summary of findings on object code:
//delphi pre XE2 wants obj code in omf32 format. gcc produces COFF
//bcc32 produces omf32 files from c code, however it can't handle variable array sizes (introduced in C99)
//coff2omf doesn't seem to work, and neither does objconv
// btw use {$L 'file.obj'} to link in delphi.
//otherwise we can make a dll with gcc and call it from delphi.
//also the compiler will be unhappy with most c obj code, due to missing _strlen(), _malloc() etc
//
//function _malloc(Size: Cardinal): Pointer; cdecl;
// begin
  // GetMem(Result, Size);
// end;

// function _strlen(const Str: PChar): Cardinal; cdecl;
// begin
  // Result := StrLen(Str);
// end;

// function _strcspn(s1, s2: PChar): Cardinal; cdecl;
// var
  // SrchS2: PChar;
// begin
  // Result := 0;
  // while S1^ <> #0 do
  // begin
    // SrchS2 := S2;
    // while SrchS2^ <> #0 do
    // begin
      // if S1^ = SrchS2^ then
        // Exit;
      // Inc(SrchS2);
    // end;
    // Inc(S1);
    // Inc(Result);
  // end;
// end;

// function _strchr(const S: PChar; C: Integer): PChar; cdecl;
// begin
  // Result := StrScan(S, Chr(C));
// end;

// function _strncmp(S1, S2: PChar; MaxLen: Cardinal): Integer; cdecl;
// begin
  // Result := StrLComp(S1, S2, MaxLen);
// end;


//NB when compiled with gcc on 32 bit, int is 32 bits, ie 4 bytes, [-2147483647,+2147483647] 

#include "kiss_fft.h"

kiss_fft_cfg cfg;
int nfft;

void FFTInit(int aNfft)
{   
    nfft = aNfft;
    cfg = kiss_fft_alloc( nfft ,0 ,NULL,NULL );
}

void FFTFree() {
    kiss_fft_cleanup();
    free(cfg);
}

void FFTRealInput(int *reIn, int *reOut, int *imOut) 
{
    kiss_fft_cpx cx_in [nfft];
    kiss_fft_cpx cx_out [nfft];
    
    int i;
    for (i = 0; i < nfft; i++) {
        cx_in[i].r = reIn[i];
        cx_in[i].i = 0;
    }

    kiss_fft(cfg, cx_in, cx_out);

    for (i = 0; i < nfft; i++) {
        reOut[i] = cx_out[i].r;
        imOut[i] = cx_out[i].i;
    }
}

void FFT(int *reIn, int *imIn, int *reOut, int *imOut) 
{
    kiss_fft_cpx cx_in [nfft];
    kiss_fft_cpx cx_out [nfft];
    
    int i;
    for (i = 0; i < nfft; i++) {
        cx_in[i].r = reIn[i];
        cx_in[i].i = imIn[i];
    }

    kiss_fft(cfg, cx_in, cx_out);

    for (i = 0; i < nfft; i++) {
        reOut[i] = cx_out[i].r;
        imOut[i] = cx_out[i].i;
    }
}

void iFFT(int *reIn, int *imIn, int *reOut, int *imOut) 
{
    kiss_fft_cpx cx_in [nfft];
    kiss_fft_cpx cx_out [nfft];
    
    int i;
    for (i = 0; i < nfft; i++) {
        cx_in[i].r = reIn[i];
        cx_in[i].i = -imIn[i]; //complex conjugate
    }

    kiss_fft(cfg, cx_in, cx_out);

    for (i = 0; i < nfft; i++) {
        reOut[i] = cx_out[i].r / nfft; //scale
        imOut[i] = cx_out[i].i / nfft; //scale
    }
}