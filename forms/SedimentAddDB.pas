unit SedimentAddDB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Math, StrUtils, dBSeaOptions;

type
  TSedimentAddForm = class(TForm)
    lblC: TLabel;
    lblDensity: TLabel;
    lblAttenuation: TLabel;
    lblName: TLabel;
    lblComments: TLabel;
    EditC: TEdit;
    EditRho: TEdit;
    EditAttenuation: TEdit;
    EditName: TEdit;
    memoComments: TMemo;
    btOK: TButton;
    btCancel: TButton;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  SedimentAddForm: TSedimentAddForm;

implementation

{$R *.dfm}

uses Main;

procedure TSedimentAddForm.FormCreate(Sender: TObject);
begin
    self.memoComments.Text := '';

    if UWAOpts.dispfeet then
    begin
        lblC.Caption := 'c (ft/s)';
        lblDensity.Caption := 'Density (lb/ft�)';
    end
    else
    begin
        lblC.Caption := 'c (m/s)';
        lblDensity.Caption := 'Density (kg/m�)';
    end;

end;

end.
