unit GeoOverlaysFormUnit;

interface

uses
    System.SysUtils, System.Variants, System.Classes, system.uitypes,
    system.Generics.collections,
  Winapi.Windows, Winapi.Messages, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  geoOverlay, Vcl.StdCtrls, helperFunctions, myColor, options;

type
  TGeoOverlaysForm = class(TForm)
    lbOverlays: TListBox;
    btAdd: TButton;
    btRemove: TButton;
    btOK: TButton;
    odShp: TOpenDialog;
    lbName: TLabel;
    edName: TEdit;
    cbVisible: TCheckBox;
    btColour: TButton;
    lbZPosition: TLabel;
    edZPosition: TEdit;
    lbSourceFileTitle: TLabel;
    lbSourceFile: TLabel;
    cbFillVisible: TCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure btRemoveClick(Sender: TObject);
    procedure btAddClick(Sender: TObject);
    procedure btColourClick(Sender: TObject);
    procedure lbOverlaysClick(Sender: TObject);
    procedure edNameChange(Sender: TObject);
    procedure cbVisibleClick(Sender: TObject);
    procedure edZPositionChange(Sender: TObject);
    procedure cbFillVisibleClick(Sender: TObject);
  private
    updateFromCode: boolean;
    procedure editBoxesInit;
    function  overlay: TGeoOverlay;
  public
    overlays: TGeoOverlays;
  end;

var
  GeoOverlaysForm: TGeoOverlaysForm;

implementation

{$R *.dfm}

uses problemContainer;

procedure TGeoOverlaysForm.btAddClick(Sender: TObject);
var
    o: TGeoOverlay;
    filename: string;
begin
    if not odShp.Execute then exit;

    for filename in odShp.Files do
    begin
        try
            o := TGeoOverlay.overlayFromShapeFile(fileName);

            if o.isProbablyLatLng and
                (mrYes = MessageDlg('Is the shapefile in degrees latitude/longitude?',
                mtConfirmation,[mbYes,mbNo],0)) then
                o.convertFromLatLng;

            o.z := overlays.count + 1;
            o.removeEastingNorthing(container.geoSetup.Easting, container.geoSetup.Northing);
            o.color := TColor.random;
            overlays.add(o);

        except on e: exception do
            showmessage('Unable to open ' + filename + ': ' + e.Message);
        end;
    end;

    editBoxesInit;
end;

procedure TGeoOverlaysForm.btColourClick(Sender: TObject);
begin
    if overlay = nil then exit;

    with TOptionsForm.setColor(overlay.Color) do
    begin
        if not HasValue then exit;
        overlay.Color := val;
    end;
end;

procedure TGeoOverlaysForm.btRemoveClick(Sender: TObject);
begin
    if (lbOverlays.ItemIndex < 0) or (lbOverlays.ItemIndex >= overlays.Count) then exit;
    overlays.Delete(lbOverlays.ItemIndex);
    editBoxesInit;
end;

procedure TGeoOverlaysForm.cbFillVisibleClick(Sender: TObject);
begin
    if overlay = nil then exit;
    overlay.fillVisible := cbFillVisible.Checked;
end;

procedure TGeoOverlaysForm.cbVisibleClick(Sender: TObject);
begin
    if overlay = nil then exit;
    overlay.visible := cbVisible.Checked;
end;

procedure TGeoOverlaysForm.editBoxesInit;
var
    p: TPair<integer, TGeoOverlay>;
    ind: integer;
begin
    updateFromCode := true;
    try
        ind := lbOverlays.ItemIndex;

        lbOverlays.Clear;
        for p in THelper.iterate<TGeoOverlay>(overlays) do
            lbOverlays.Items.add(inttostr(p.Key) + ': ' + p.Value.name  + '  (' + p.Value.sourceFile + ')');

        if (ind >= 0) and (ind < overlays.count) then
            lbOverlays.ItemIndex := ind
        else if overlays.count > 0 then
            lbOverlays.ItemIndex := 0;

        if overlay = nil then
        begin
            edName.Text := '';
            edZPosition.Text := '';
            lbSourceFile.Caption := '';
            exit;
        end;

        edName.Text := overlay.name;
        cbVisible.Checked := overlay.visible;
        cbFillVisible.Checked := overlay.fillVisible;
        edZPosition.Text := floattostrf(overlay.z, ffFixed, 14,2);
        lbSourceFile.Caption := overlay.sourceFile;

    finally
        updateFromCode := false;
    end;
end;

procedure TGeoOverlaysForm.edNameChange(Sender: TObject);
begin
    if overlay = nil then exit;
    overlay.name := edName.Text;
    editBoxesInit;
end;

procedure TGeoOverlaysForm.edZPositionChange(Sender: TObject);
begin
    if (overlay = nil) or not isFloat(edZPosition.Text) then exit;
    overlay.z := tofl(edZPosition.Text);
end;

procedure TGeoOverlaysForm.FormCreate(Sender: TObject);
begin
    updateFromCode := false;
end;

procedure TGeoOverlaysForm.FormShow(Sender: TObject);
begin
    editBoxesInit;
end;

procedure TGeoOverlaysForm.lbOverlaysClick(Sender: TObject);
begin
    // todo
    editBoxesInit;
end;

function TGeoOverlaysForm.overlay: TGeoOverlay;
begin
    if overlays.count = 1 then exit(overlays.first);
    if (lbOverlays.ItemIndex < 0) or (lbOverlays.ItemIndex >= overlays.count)  then exit(nil);
    result := overlays[lbOverlays.ItemIndex];
end;

end.
