object directivityInputForm: TdirectivityInputForm
  Left = 0
  Top = 0
  Caption = 'Input source directivity data'
  ClientHeight = 741
  ClientWidth = 827
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  DesignSize = (
    827
    741)
  PixelsPerInch = 96
  TextHeight = 13
  object lbnAngle: TLabel
    Left = 16
    Top = 26
    Width = 84
    Height = 13
    Caption = 'Number of angles'
  end
  object lbLevsInDB: TLabel
    Left = 244
    Top = 26
    Width = 472
    Height = 13
    Caption = 
      'Attenuation in dB (use positive values for sound reduction). -90' +
      ' = up, 0 = horizontal, +90 = down'
  end
  object sgLevs: TStringGrid
    Left = 16
    Top = 51
    Width = 793
    Height = 637
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultColWidth = 70
    TabOrder = 0
    OnDrawCell = sgLevsDrawCell
    OnMouseDown = sgLevsMouseDown
    OnSelectCell = sgLevsSelectCell
  end
  object btCancel: TButton
    Left = 734
    Top = 703
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object btOK: TButton
    Left = 653
    Top = 703
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = btOKClick
  end
  object ednAngle: TEdit
    Left = 121
    Top = 24
    Width = 73
    Height = 21
    TabOrder = 3
    Text = '19'
    OnChange = ednAngleChange
  end
  object btClear: TButton
    Left = 16
    Top = 703
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Clear'
    Default = True
    TabOrder = 4
    OnClick = btClearClick
  end
  object odDirectivity: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'txt file|*.txt|All files|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Choose input file'
    Left = 592
    Top = 712
  end
end
