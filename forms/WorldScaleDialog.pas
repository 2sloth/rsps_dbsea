unit WorldScaleDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, math, StrUtils, helperFunctions, dBSeaOptions, Vcl.ExtCtrls;

type
  TFrmWorldScale = class(TForm)
    btOK: TButton;
    btCancel: TButton;
    edScale: TEdit;
    lblScale: TLabel;
    rbDistBetweenPoints: TRadioButton;
    rbXandY: TRadioButton;
    lblAx: TLabel;
    lblAy: TLabel;
    lblBx: TLabel;
    lblBy: TLabel;
    edAx: TEdit;
    edBy: TEdit;
    edAy: TEdit;
    edBx: TEdit;
    pnPointA: TPanel;
    lbPointATitle: TLabel;
    pnPointB: TPanel;
    lbPointBTitle: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure EditBoxesInit;

    procedure edScaleChange(Sender: TObject);
    procedure rbDistBetweenPointsClick(Sender: TObject);
    procedure rbXandYClick(Sender: TObject);
  private
  public
    procedure setLabelsForSettingStyle(const settingStyle: Integer);
  end;

var
  frmWorldScale: TfrmWorldScale;

implementation

{$R *.dfm}

uses main;

procedure TFrmWorldScale.edScaleChange(Sender: TObject);
begin
//    {conversion feet to metres is handled here}
//    if isFloat(edScale.Text) then
//        if tofl(edScale.Text) > 0 then
//            worldScalePointsDist := f2m(tofl(edScale.Text), UWAOpts.dispFeet);
end;

procedure TFrmWorldScale.FormCreate(Sender: TObject);
begin
    EditBoxesInit;
end;

procedure TFrmWorldScale.setLabelsForSettingStyle(const settingStyle:Integer);
begin
    if settingStyle <> 3 then
    begin
        self.lbPointATitle.Caption := 'Point A   bottom left';
        case settingStyle of
        0 : self.lbPointBTitle.Caption := 'Point B   bottom right';
        1 : self.lbPointBTitle.Caption := 'Point B   top left';
        2 : self.lbPointBTitle.Caption := 'Point B   top right';
        end;
    end;
end;

procedure TFrmWorldScale.EditBoxesInit;
var
    sUnits : string;
begin
    sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');
    lblScale.Caption := 'Distance ' + sUnits;
    lblAx.Caption := 'Easting ' + sUnits;
    lblAy.Caption := 'Northing ' + sUnits;
    lblBx.Caption := 'Easting ' + sUnits;
    lblBy.Caption := 'Northing ' + sUnits;

    lblScale.Enabled   := rbDistBetweenPoints.Checked;
    edScale.Enabled    := rbDistBetweenPoints.Checked;

    lbPointATitle.Enabled := rbXandY.Checked;
    lblAx.Enabled := rbXandY.Checked;
    lblAy.Enabled := rbXandY.Checked;
    lblBx.Enabled := rbXandY.Checked;
    lblBy.Enabled := rbXandY.Checked;

    lbPointBTitle.Enabled := rbXandY.Checked;
    edAx.Enabled := rbXandY.Checked;
    edAy.Enabled := rbXandY.Checked;
    edBx.Enabled := rbXandY.Checked;
    edBy.Enabled := rbXandY.Checked;
end;

procedure TFrmWorldScale.rbDistBetweenPointsClick(Sender: TObject);
begin
    rbXandY.Checked    := false;
    EditBoxesInit;
end;

procedure TFrmWorldScale.rbXandYClick(Sender: TObject);
begin
    rbDistBetweenPoints.Checked    := false;
    EditBoxesInit;
end;

end.
