object scenarioDiffFrm: TscenarioDiffFrm
  Left = 0
  Top = 0
  Caption = 'Scenario diff tool'
  ClientHeight = 730
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    402
    730)
  PixelsPerInch = 96
  TextHeight = 13
  object pbLevels: TPaintBox
    Left = 24
    Top = 264
    Width = 320
    Height = 320
    Anchors = [akLeft, akTop, akRight]
  end
  object lbMin: TLabel
    Left = 310
    Top = 599
    Width = 3
    Height = 13
  end
  object lbMax: TLabel
    Left = 310
    Top = 636
    Width = 3
    Height = 13
  end
  object lbAllMin: TLabel
    Left = 24
    Top = 672
    Width = 3
    Height = 13
  end
  object lbAllMax: TLabel
    Left = 270
    Top = 672
    Width = 3
    Height = 13
  end
  object lbScenariosCannotBePaired: TLabel
    Left = 24
    Top = 235
    Width = 193
    Height = 13
    Caption = 'The selected scenarios cannot be paired'
    Visible = False
  end
  object btOk: TButton
    Left = 221
    Top = 697
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    ExplicitLeft = 213
    ExplicitTop = 707
  end
  object lbScenario1: TListBox
    Left = 0
    Top = 0
    Width = 193
    Height = 229
    Style = lbOwnerDrawFixed
    ItemHeight = 13
    TabOrder = 1
    OnClick = lbScenarioClick
    OnDrawItem = lbScenario1DrawItem
  end
  object lbScenario2: TListBox
    Tag = 1
    Left = 199
    Top = 0
    Width = 205
    Height = 229
    Style = lbOwnerDrawFixed
    Anchors = [akLeft, akTop, akRight]
    ItemHeight = 13
    TabOrder = 2
    OnClick = lbScenarioClick
    OnDrawItem = lbScenario1DrawItem
    ExplicitWidth = 197
  end
  object btCancel: TButton
    Left = 302
    Top = 697
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    Default = True
    ModalResult = 2
    TabOrder = 3
    ExplicitLeft = 294
    ExplicitTop = 707
  end
  object sbMin: TScrollBar
    Left = 24
    Top = 595
    Width = 277
    Height = 17
    PageSize = 0
    TabOrder = 4
    OnChange = sbMinChange
  end
  object sbMax: TScrollBar
    Left = 24
    Top = 636
    Width = 277
    Height = 17
    PageSize = 0
    TabOrder = 5
    OnChange = sbMaxChange
  end
end
