unit directivityInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, helperFunctions, soundSource, scenario, spectrum,
  directivity, dBSeaColours, basetypes, mcklib, math, types, StringGridHelper;

type
  TdirectivityInputForm = class(TForm)
    sgLevs: TStringGrid;
    btCancel: TButton;
    btOK: TButton;
    ednAngle: TEdit;
    lbnAngle: TLabel;
    lbLevsInDB: TLabel;
    btClear: TButton;
    odDirectivity: TOpenDialog;
    procedure FormShow(Sender: TObject);

    procedure btOKClick(Sender: TObject);

    procedure ednAngleChange(Sender: TObject);
    procedure sgLevsSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure btClearClick(Sender: TObject);
    procedure sgLevsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure sgLevsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

  private
    fsrc : TSource;

    function  nRows : integer;
    procedure updateStringGrid;
    procedure setSource(aSrc: TSource);
    procedure entriesFromStringForFreqIndex(const s : string; const fi : integer);
    procedure readFromFileWithFreqIndex(const fi : integer);
  public
    defaultOpenDir : string;
    property src : TSource read fSrc write setSource;
  end;

var
  directivityInputForm: TdirectivityInputForm;

implementation

{$R *.dfm}

procedure TdirectivityInputForm.btClearClick(Sender: TObject);
var
    i, j : integer;
begin
    for i := 1 to sgLevs.ColCount - 1 do
        for j := 2 to sgLevs.RowCount - 1 do
            sgLevs.Cells[i,j] := '0';
end;

procedure TdirectivityInputForm.btOKClick(Sender: TObject);
var
    i,j : integer;
    directivity : TDirectivity;
    levelsOK : boolean;
begin
    levelsOK := true;
    for i := 0 to sgLevs.ColCount - 2 do
        for j := 0 to nRows - 1 do
            if not isfloat(sgLevs.Cells[i + 1,j + 2]) then
            begin
                levelsOK := false;
                break;
            end;
    if not levelsOK then
        showmessage('Entered data could not be interpreted, check data entry')
    else
    begin
        src.directivities.Clear;
        for i := 0 to sgLevs.ColCount - 2 do
        begin
            src.directivities.Add(TDirectivity.Create);
            directivity := src.directivities.Last;
            directivity.f := TSpectrum.getFbyInd(i + src.delegate.getStartfi, src.delegate.getBandwidth);
            setlength(directivity.directivity,nRows);
            for j := 0 to nRows - 1 do
                directivity.directivity[j] := tofl(sgLevs.Cells[i + 1,j + 2]);
        end;
    end;
end;

procedure TdirectivityInputForm.readFromFileWithFreqIndex(const fi : integer);
begin
    if odDirectivity.Execute then
        entriesFromStringForFreqIndex(THelper.readfiletostring(odDirectivity.filename),fi);
end;

procedure TdirectivityInputForm.entriesFromStringForFreqIndex(const s : string; const fi : integer);
var
    delimitedString : String;
    m : TMatrix;
    i : integer;
    v : TDoubleDynArray;
begin
    delimitedString  := StringReplace(s, #13#10, ';', [rfReplaceAll, rfIgnoreCase]); //windows style line breaks
    while delimitedString[length(delimitedString)] = ';' do
    begin
        if length(delimitedString) = 0 then
            break;
        setlength(delimitedString, length(delimitedString) - 1);
    end;
    m.fromDelimitedString(delimitedString,';',true);
    setlength(v,length(m[0]));
    for i := 0 to length(m[0]) - 1 do
        v[i] := m[0,i];

    if length(v) = 0 then
    begin
        showmessage('Unable to parse input, length = 0');
        exit;
    end;
    for i := 0 to length(v) - 1 do
        if isnan(v[i]) then
        begin
            showmessage('Unable to parse input at line ' + inttostr(i + 1));
            exit;
        end;
    if length(v) <> (nRows) then
    begin
        showmessage('Length of input file is ' + inttostr(length(v)) + ' entries, need ' +
                     inttostr(nRows) + ' entries');
        exit;
    end;
    if (fi < 0) or (fi + src.delegate.getstartfi > src.delegate.getEndfi) then
    begin
        exit;
    end;

    //copy values over to the stringgrid
    for i := 0 to length(v) - 1 do
        sgLevs.Cells[fi + 1,i + 2] := tostr(v[i]);
end;

procedure TdirectivityInputForm.ednAngleChange(Sender: TObject);
begin
    updateStringGrid;
end;

procedure TdirectivityInputForm.FormShow(Sender: TObject);
begin
    updateStringGrid;
end;

procedure TdirectivityInputForm.sgLevsDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
    angle : double;
begin
    with TStringGrid(Sender) do
    begin
        if (aRow = 1) and (ACol > 0) then
        begin
            Canvas.Font.Color := TDBSeaColours.midBlue;

            {$IFDEF VER280}
            if DefaultDrawing then
                Rect.Left := Rect.Left - 4;
            Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+5, Cells[ACol, ARow]);
            {$ELSE}
            Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
            {$ENDIF}
        end;

        {angles that are outside the current ray range}
        if (ARow >= 2) and (ACol > 0) then
        begin
            angle := -90 + 180 * (aRow - 1) / nRows;
            if (angle < src.delegate.getRayTracingStartAngle) or
               (angle > src.delegate.getRayTracingEndAngle  ) then
            begin
                Canvas.Font.Color := TDBSeaColours.greyText;

                {$IFDEF VER280}
                if DefaultDrawing then
                    Rect.Left := Rect.Left - 4;
                Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+5, Cells[ACol, ARow]);
                {$ELSE}
                Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
                {$ENDIF}
            end;
        end;
    end;
end;


procedure TdirectivityInputForm.sgLevsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    aCol, aRow: Integer;
begin
    sgLevs.MouseToCell(X, Y, aCol, aRow);
    if (button = mbLeft) and (arow = 1) and (acol > 0) then
        readFromFileWithFreqIndex(acol - 1);
end;

procedure TdirectivityInputForm.sgLevsSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
    {disallow editing on certain fields.}
    if (Arow <= 1) or (ACol = 0) then
        sgLevs.Options := sgLevs.Options - [goEditing] + [goTabs]
    else
        sgLevs.Options := sgLevs.Options + [goEditing, goTabs, goAlwaysShowEditor];
end;

function  TdirectivityInputForm.nRows : integer;
begin
    result := sgLevs.RowCount - 2;
end;

procedure TdirectivityInputForm.updateStringGrid;
var
    rows : integer;
    i,j : integer;
    directivity : TDirectivity;
begin
    sgLevs.clear;
    sgLevs.Cells[0,1] := 'Angles (deg)';
    if isInt(ednAngle.Text) then
    begin
        rows := toInt(ednAngle.Text);
        sgLevs.RowCount := rows + 2;
        sgLevs.ColCount := src.delegate.getlenfi + 1;
        if rows = 1 then
            sgLevs.Cells[0,2] := '0'
        else
            for j := 0 to rows - 1 do
                sgLevs.Cells[0,j + 2] := tostr(round1(-90 + 180 * j / (rows - 1)));
    //            sgLevs.Cells[0,j + 1] := tostr(round1(TScenario(src.parent).rayTracingStartAngle +
    //                                           (TScenario(src.parent).rayTracingEndAngle - TScenario(src.parent).rayTracingStartAngle) *
    //                                           j / (rows - 1)));

        for i := 0 to sgLevs.ColCount - 2 do
        begin
            sgLevs.Cells[i + 1,0] := TSpectrum.getFStringbyInd(i + src.delegate.getStartfi, src.delegate.getBandwidth);
            sgLevs.Cells[i + 1,1] := 'File';
            for j := 0 to rows - 1 do
                sgLevs.Cells[i + 1,j + 2] := '0';

            for directivity in src.directivities do
                if directivity.f = TSpectrum.getFbyInd(i + src.delegate.getStartfi, src.delegate.getBandwidth) then
                    if Length(directivity.directivity) = rows then
                        for j := 0 to rows - 1 do
                            sgLevs.Cells[i + 1,j + 2] := tostr(directivity.directivity[j]);
        end;
    end;
end;

procedure TdirectivityInputForm.setSource(aSrc: TSource);
begin
    fSrc := aSrc;
    if src.directivities.Count > 0 then
        ednAngle.Text := tostr(length(src.directivities.First.directivity));
end;

end.
