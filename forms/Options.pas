unit Options;

{$B-}

interface

uses
    syncommons,
    System.SysUtils, system.math, system.strutils,
    winapi.Windows, winapi.Messages,
  vcl.Graphics, vcl.Forms,
  vcl.Dialogs, vcl.StdCtrls, vcl.ExtCtrls, vcl.ComCtrls,
  spectrum, helperFunctions, baseTypes,
  levelConverter, myregistry, problemContainer,
  scenario, dBSeaOptions, globalsunit, Vcl.Controls, System.Classes, Vcl.Menus,
  ProcessorAffinity, SmartPointer, nullable, eventLogging;

type
  TOptionsForm = class(TForm)
    editDefaultSave: TEdit;
    editTempDir: TEdit;
    btBrowseDefaultSaveDir: TButton;
    btBrowseTempDir: TButton;
    lblDefaultSaveDir: TLabel;
    lblTempDir: TLabel;
    editProjectName: TEdit;
    lblProjectName: TLabel;
    btOK: TButton;
    btCancel: TButton;
    rgFeetMeters: TRadioGroup;
    pcOptionsTabs: TPageControl;
    tsGraphics: TTabSheet;
    lblDepthScaling: TLabel;
    btBackgroundFlatColor: TButton;
    btLandColorPick: TButton;
    btWaterColorPick: TButton;
    sbZFact: TScrollBar;
    cbSmoothColorMap: TCheckBox;
    sbResultsAlpha: TScrollBar;
    lblResultsAlpha: TLabel;
    tsMisc: TTabSheet;
    cbShowLandInXSec: TCheckBox;
    sbZLandFact: TScrollBar;
    lblLandScaling: TLabel;
    cbResultsTris: TCheckBox;
    rgLevelsDisplay: TRadioGroup;
    lbLayerNumber: TLabel;
    lbDepth: TLabel;
    cbHideProbesOnExport: TCheckBox;
    sbSourceSize: TScrollBar;
    sbProbeSize: TScrollBar;
    lbProbeSize: TLabel;
    lbSourceSize: TLabel;
    edBellhopNRays: TEdit;
    lblBellhopNRays: TLabel;
    cbKrakenCoupledModes: TCheckBox;
    cbFullXSecDepth: TCheckBox;
    editBathymetryFile: TEdit;
    lblBathymetryfile: TLabel;
    cbXSecTruncateAtWaterSurface: TCheckBox;
    cbRayTracingCoherent: TCheckBox;
    lblMaxGLBathyPixels: TLabel;
    edGLMaxBathyPixels: TEdit;
    tsExport: TTabSheet;
    imLogo: TImage;
    btBrowseLogo: TButton;
    edLogoImage: TEdit;
    lblImageToAttach: TLabel;
    lblExportText: TLabel;
    cbNaNSeafloorLevels: TCheckBox;
    cbAutosaveAfterSolve: TCheckBox;
    edRadialSmoothing: TEdit;
    lblRadialSmoothing: TLabel;
    udRadialSmoothing: TUpDown;
    mmExportText: TMemo;
    btDisplayIndUp: TButton;
    btDisplayIndDn: TButton;
    cbMakeTLMonotonic: TCheckBox;
    sbMovingSrcLinewidth: TScrollBar;
    lblMovingSrcWidth: TLabel;
    lblDBSeaModesMaxModes: TLabel;
    edDBSeaModesMaxModes: TEdit;
    btUseTopoLandColors: TButton;
    tsAdvanced: TTabSheet;
    lblDBSeaRayMaxBottomReflections: TLabel;
    edDBSeaRayMaxBottomReflections: TEdit;
    tsGraphicsSizes: TTabSheet;
    lbExclusionZoneRadialSmoothing: TLabel;
    edExclusionZoneRadialSmoothing: TEdit;
    udExclusionZoneRadialSmoothing: TUpDown;
    btAssociateUWA: TButton;
    lbRayTracingAngles: TLabel;
    edRayStartAngle: TEdit;
    edRayEndAngle: TEdit;
    lbAngleTo: TLabel;
    rgLevelType: TRadioGroup;
    lbAssessmentPeriod: TLabel;
    edAssessmentPeriod: TEdit;
    btSetAssessmentPeriodTo: TButton;
    rgExportDelimiter: TRadioGroup;
    tsLevels: TTabSheet;
    cbCalculateAttenuationAtEachStep: TCheckBox;
    sbResultsContoursDarkening: TScrollBar;
    lbResultsContoursDarkening: TLabel;
    lbResultsContoursWidth: TLabel;
    sbResultsContoursWidth: TScrollBar;
    sbPEzOversampling: TScrollBar;
    lbPEzOversampling: TLabel;
    lbPErOversampling: TLabel;
    sbPErOversampling: TScrollBar;
    tsMultiCore: TTabSheet;
    cbMulticorePE: TCheckBox;
    cbMulticoreModes: TCheckBox;
    cbMulticoreRay: TCheckBox;
    puAssessmentPeriod: TPopupMenu;
    mi1Hour: TMenuItem;
    mi24Hours: TMenuItem;
    lbPETitle: TLabel;
    lbDBSeaRayTitle: TLabel;
    lbdBSeaModes: TLabel;
    lbFreqOversampling: TLabel;
    edFreqOversampling: TEdit;
    udFrequencyOversampling: TUpDown;
    lbAngleHint: TLabel;
    mi1Second: TMenuItem;
    odLogo: TOpenDialog;
    edInitialStepSize: TEdit;
    lbInitialStepSize: TLabel;
    sbNumberOfProcessors: TScrollBar;
    lbNumberOfProcessors: TLabel;
    udDBSeaMaxBottomReflections: TUpDown;
    btNODATAColor: TButton;
    cbPEFFT: TCheckBox;
    edNumberPadeTerms: TEdit;
    lbNumberPadeTerms: TLabel;
    udNumberPadeTerms: TUpDown;
    cbStopMarchingSolutionAtLand: TCheckBox;
    lbAssessmentPeriodMovingSourceHint: TLabel;
    cbLiveUpdate1: TCheckBox;
    cbLiveUpdate2: TCheckBox;
    lbModesZOversampling: TLabel;
    sbModeszOversampling: TScrollBar;
    lbModesROversampling: TLabel;
    sbModesROversampling: TScrollBar;
    mmFreqDomainPeakLevelWarning: TMemo;
    rgRKMethod: TRadioGroup;
    pcSolverSpecific: TPageControl;
    tsParabolicEquation: TTabSheet;
    tsModal: TTabSheet;
    tsRayTracer: TTabSheet;
    lbNumberPadeTermsHorizontal: TLabel;
    edNumberPadeTermsHorizontal: TEdit;
    udNumberPadeTermsHorizontal: TUpDown;
    lblBellhopNRaysAzimuth: TLabel;
    edBellhopNRaysAzimuth: TEdit;
    lbPEPhiOversampling: TLabel;
    sbPEPhiOversampling: TScrollBar;
    cbExclusionZoneStopAtLand: TCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure editBoxesInit;

    procedure btOKClick(Sender: TObject);

    procedure cbSmoothColorMapClick(Sender: TObject);
    procedure cbShowLandInXSecClick(Sender: TObject);
    procedure cbResultsTrisClick(Sender: TObject);
    procedure cbInterpolateLevelsClick(Sender: TObject);
    procedure cbHideProbesOnExportClick(Sender: TObject);
    procedure cbKrakenCoupledModesClick(Sender: TObject);
    procedure cbSaveResultsToFileClick(Sender: TObject);
    procedure cbFullXSecDepthClick(Sender: TObject);
    procedure cbXSecTruncateAtWaterSurfaceClick(Sender: TObject);
    procedure cbRayTracingCoherentClick(Sender: TObject);
    procedure cbNaNSeafloorLevelsClick(Sender: TObject);
    procedure cbAutosaveAfterSolveClick(Sender: TObject);
    procedure cbMakeTLMonotonicClick(Sender: TObject);

    procedure rgLevelsDisplayClick(Sender: TObject);
    procedure rgFeetMetersClick(Sender: TObject);

    procedure sbResultsAlphaChange(Sender: TObject);
    procedure sbZLandFactChange(Sender: TObject);
    procedure sbMovingSrcLinewidthChange(Sender: TObject);
    procedure sbSourceSizeChange(Sender: TObject);
    procedure sbProbeSizeChange(Sender: TObject);
    procedure sbZFactChange(Sender: TObject);

    procedure mmExportTextChange(Sender: TObject);

    procedure editProjectNameChange(Sender: TObject);
    procedure editProjectNameEnter(Sender: TObject);

    procedure btBrowseDefaultSaveDirClick(Sender: TObject);
    procedure btBrowseTempDirClick(Sender: TObject);
    procedure btWaterColorPickClick(Sender: TObject);
    procedure btLandColorPickClick(Sender: TObject);
    procedure btBackgroundFlatColorClick(Sender: TObject);
    procedure zIndButtonsClick(const dir : integer);
    procedure btBrowseLogoClick(Sender: TObject);
    procedure btDisplayIndUpClick(Sender: TObject);
    procedure btDisplayIndDnClick(Sender: TObject);
    procedure btUseTopoLandColorsClick(Sender: TObject);
    procedure btSetCurrentSolversToDefaultClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btAssociateUWAClick(Sender: TObject);
    procedure rgLevelTypeClick(Sender: TObject);
    procedure bt1HourClick(Sender: TObject);
    procedure bt24HourClick(Sender: TObject);
    procedure rgExportDelimiterClick(Sender: TObject);
    procedure cbCalculateAttenuationAtEachStepClick(Sender: TObject);
    procedure sbResultsContoursDarkeningChange(Sender: TObject);
    procedure sbResultsContoursWidthChange(Sender: TObject);
    procedure sbPEzOversamplingChange(Sender: TObject);
    procedure sbPErOversamplingChange(Sender: TObject);
    procedure btSetAssessmentPeriodToClick(Sender: TObject);
    procedure mi1SecondClick(Sender: TObject);
    procedure sbNumberOfProcessorsChange(Sender: TObject);
    procedure btNODATAColorClick(Sender: TObject);
    procedure cbPEFFTClick(Sender: TObject);
    procedure cbStopMarchingSolutionAtLandClick(Sender: TObject);
    procedure sbModesROversamplingChange(Sender: TObject);
    procedure sbModeszOversamplingChange(Sender: TObject);
    procedure rgRKMethodClick(Sender: TObject);
    procedure sbPEPhiOversamplingChange(Sender: TObject);
    procedure cbExclusionZoneStopAtLandClick(Sender: TObject);
  private
    updateFromCode: boolean;

    procedure updateOversamplingLabels;
    procedure updateNumberOfProcessorsText;
    procedure clearLogoImage;
  public
    const
    iGraphicsSizesTab = 1;
    iAdvancedSolverTab = 2;
    var
    //savePath : string;
    tempPath: string;
    projName: string;
    bLogoImageLoaded: boolean;
    iLogoImageHeight: integer;
    bShouldUpdateResults: boolean;

    procedure loadLogoImage;
    class function  setColor(const inColor: integer = - 1): Nullable<TColor>;
  end;

var
    OptionsForm : TOptionsForm;

implementation

{$R *.dfm}

uses Main, GLWindow, EditProbeFrame, EditResultsFrame, editwaterframe,
    EditSetupFrame, editfishframe, EditSourceFrame, EditSedimentFrame;

procedure TOptionsForm.FormCreate(Sender: TObject);
begin
    updateFromCode := true;

    pcOptionsTabs.ActivePageIndex := 0;
    pcSolverSpecific.ActivePageIndex := 0;
    bLogoImageLoaded := false;
    iLogoImageHeight := 0;

    {$IF not Defined(DEBUG)}
        cbRayTracingCoherent.Visible := false;
        cbKrakenCoupledModes.Visible := false;
        cbKrakenCoupledModes.Visible := false;
        cbPEFFT.Visible := false;

        lbNumberPadeTermsHorizontal.Visible := false;
        edNumberPadeTermsHorizontal.Visible := false;
        udNumberPadeTermsHorizontal.Visible := false;
        lbPEPhiOversampling.Visible := false;
        sbPEPhiOversampling.Visible := false;
        lblBellhopNRaysAzimuth.Visible := false;
        edBellhopNRaysAzimuth.Visible := false;
    {$ENDIF}
//        lbNumberPadeTermsHorizontal.Visible := True;
//        edNumberPadeTermsHorizontal.Visible := True;
//        udNumberPadeTermsHorizontal.Visible := True;
//        lbPEPhiOversampling.Visible := True;
//        sbPEPhiOversampling.Visible := True;
//        lblBellhopNRaysAzimuth.Visible := True;
//        edBellhopNRaysAzimuth.Visible := True;

    sbNumberOfProcessors.Max := CPUCount;
    sbNumberOfProcessors.Enabled := TProcessorAffinity.CPUCount > 1;
    bShouldUpdateResults := false;
    updateFromCode := false;
end;

procedure TOptionsForm.FormDestroy(Sender: TObject);
begin
    //
end;

procedure TOptionsForm.FormShow(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'FormShow');

    updateFromCode := true;
    EditBoxesInit;
    updateFromCode := false;
end;

procedure TOptionsForm.mmExportTextChange(Sender: TObject);
begin
    UWAOpts.ExportFreeText := mmExportText.Text;
end;

procedure TOptionsForm.editBoxesInit;
var
    sUnits : String;
begin
    updateFromCode := true;

    //bw := container.scenario.bandwidth;

    //editDefaultSave.Text    := savePath;
    editTempDir.Text        := tempPath;
    editProjectName.Text    := projName;
    editBathymetryFile.Text := container.bathymetry.BathymetryFile; //not editable

    lbInitialStepSize.Caption := 'Initial step size ' + ifthen(UWAOpts.dispFeet, '(ft)', '(m)');
    edInitialStepSize.Text := tostr(m2f(container.scenario.rayTracingInitialStepsize, UWAOpts.dispFeet));
    udRadialSmoothing.Position := container.scenario.iSmoothTLRadial;
    udExclusionZoneRadialSmoothing.Position := container.scenario.iSmoothDistToExlusionZone;
    udFrequencyOversampling.Position := container.scenario.nOversampleFreqsPerBand;
    edBellhopNRays.Text := tostr(container.scenario.BellhopNRays);
    edBellhopNRaysAzimuth.Text := tostr(container.scenario.bellhopNRaysAzimuth);
    edDBSeaModesMaxModes.Text := tostr(container.scenario.DBSeaModesMaxModes);
    edGLMaxBathyPixels.Text := tostr(UWAOpts.MaxGLBathyPixels);
    udNumberPadeTerms.position := container.scenario.nPadeTerms;
    udNumberPadeTermsHorizontal.Position := container.scenario.nPadeTermsHorizontal;

    udDBSeaMaxBottomReflections.Position := container.scenario.dBSeaRayMaxBottomReflections;
    //edMaxCores.Text := tostr(UWAOpts.maxProcessorsForSolve);
    edRayStartAngle.Text := tostr(container.scenario.rayTracingStartAngle);
    edRayEndAngle.Text := tostr(container.scenario.rayTracingEndAngle);
    //lbMaxCores.Caption := 'Maximum number of cores for solve (' + tostr(CPUCount) + ' available)';

    {set feet or meters}
    if UWAOpts.DispFeet then
        rgFeetMeters.ItemIndex := 1
    else
        rgFeetMeters.ItemIndex := 0;

    sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');

    if TLevelConverter.isTimeSeries(container.scenario.levelType) then
    begin
        rgLevelType.Items.Clear;
        rgLevelType.Items.Add('Sound exposure level    dB SELcum @ T0 = 1s');
        rgLevelType.Items.Add('Peak sound pressure level    dBpeak');
        rgLevelType.Items.Add('Peak to peak sound pressure level    dB SPLpkpk');
    end
    else
    begin
        rgLevelType.Items.Clear;
        rgLevelType.Items.Add('Sound pressure level    dB SPL');
        rgLevelType.Items.Add('Sound exposure level    dB SELcum @ T0 = 1s');
        rgLevelType.Items.Add('Peak sound pressure level    dBpeak');
    end;
    mmFreqDomainPeakLevelWarning.visible := container.scenario.levelType = kSPLCrestFactor;

    case container.scenario.levelType of
        kSPL        : rgLevelType.ItemIndex := 0;
        kSELfreq    : rgLevelType.ItemIndex := 1;
        kSPLCrestFactor      : rgLevelType.ItemIndex := 2;
        kSELtime    : rgLevelType.ItemIndex := 0;
        kSPLpk      : rgLevelType.ItemIndex := 1;
        kSPLpkpk    : rgLevelType.ItemIndex := 2;
    end;

    case UWAOpts.exportDelimiter of
        ',' : rgExportDelimiter.ItemIndex := 0;
        ';' : rgExportDelimiter.ItemIndex := 1;
        #09 : rgExportDelimiter.ItemIndex := 2;
        #32 : rgExportDelimiter.ItemIndex := 3;
    end;

    edAssessmentPeriod.Text := tostr(container.scenario.assessmentPeriod);
    edAssessmentPeriod.Enabled := not container.scenario.anySourceMoving;
    btSetAssessmentPeriodTo.Enabled := edAssessmentPeriod.Enabled;
    lbAssessmentPeriodMovingSourceHint.Visible := not edAssessmentPeriod.Enabled;

    //lblClipBelow.Caption := 'Clip everything below ' + sUnits;

    case UWAOpts.LevelsDisplay of
        ldMax           : rgLevelsDisplay.ItemIndex := 0;
        ldSingleLayer   : rgLevelsDisplay.ItemIndex := 1;
        ldAllLayers     : rgLevelsDisplay.ItemIndex := 2;
    end;
    if UWAOpts.LevelsDisplay = ldMax then
    begin
        btDisplayIndUp.Enabled      := false;
        btDisplayIndDn.Enabled      := false;
        lbDepth.Enabled             := false;
        lbLayerNumber.Enabled       := false;
    end
    else
    begin
        btDisplayIndUp.Enabled      := true;
        btDisplayIndDn.Enabled      := true;
        lbDepth.Enabled             := true;
        lbLayerNumber.Enabled       := true;
        lbLayerNumber.Caption       := 'Display solution depth # ' + inttostr(UWAOpts.DisplayZInd + 1);
        {get the current z display index so we can say what depth is being displayed}
        lbDepth.Caption             := ('Depth ' + container.geoSetup.makeZString(container.scenario.depths(UWAOpts.DisplayZInd)) + ' ' + sUnits);
    end;

    cbSmoothcolorMap.Checked        := UWAOpts.ResultsColormapSmooth;
    cbShowLandInXSec.Checked        := UWAOpts.GLShowLandinXSec;
    cbResultsTris.Checked           := UWAOpts.ResultsTris;
    cbHideProbesOnExport.Checked    := UWAOpts.bHideProbesOnExport;
    cbKrakenCoupledModes.Checked    := container.scenario.NormalModesCoupledModes;
    cbRayTracingCoherent.Checked       := container.scenario.RayTracingCoherent;
    cbCalculateAttenuationAtEachStep.Checked := container.scenario.rayTracingCalculateAttenuationAtEachStep;
    cbFullXSecDepth.checked         := UWAOpts.FullXSecDepth;
    cbXSecTruncateAtWaterSurface.Checked := UWAOpts.XSecTruncateAtWaterSurface;
    cbNaNSeafloorLevels.Checked     := container.scenario.NaNSeafloorLevels;
    cbAutosaveAfterSolve.Checked    := UWAOpts.AutosaveAfterSolve;
    cbMakeTLMonotonic.Checked       := container.scenario.MakeTLMonotonic;
    cbPEFFT.Checked                 := bPESS;
    cbStopMarchingSolutionAtLand.Checked := container.scenario.stopMarchingSolverAtLand;
    cbExclusionZoneStopAtLand.Checked := UWAOpts.exclusionZoneStopAtLand;

    //cbMulticoreAll.checked := bMultiCorePE and bMultiCoreModes and bMultiCoreRay;
    cbMulticorePE.Checked := bMultiCorePE;
    cbMulticoreModes.Checked := bMultiCoreModes;
    cbMulticoreRay.Checked := bMultiCoreRay;

    sbZFact.Position        := saferound(-UWAOpts.ZFactor / UWAOpts.ZFactMax * sbZFact.Max);
    sbZLandFact.Position    := saferound(-UWAOpts.ZLandFactor / UWAOpts.ZFactMax * sbZLandFact.Max);
    sbResultsAlpha.Position := saferound(UWAOpts.ResultsAlpha*sbResultsAlpha.Max);
    sbResultsContoursDarkening.Position := safeRound((1.0 - UWAOpts.resultsContoursDarkening)*sbResultsContoursDarkening.max);
    sbResultsContoursWidth.Position := UWAOpts.resultsContoursWidth;
    sbSourceSize.Position   := saferound(UWAOpts.sourceSize / UWAOpts.MaxSourceSize * sbSourceSize.Max);
    sbProbeSize.Position    := saferound(UWAOpts.ProbeSize / UWAOpts.MaxProbeSize * sbProbeSize.Max);
    sbMovingSrcLinewidth.Position := UWAOpts.SourceLineWidth;
    sbPEzOversampling.Position := saferound( container.scenario.dBSeaPEZOversampling / TScenario.dBSeaPEzOversamplingMax * self.sbPEzOversampling.Max);
    sbPErOversampling.Position := saferound( container.scenario.dBSeaPEROversampling / TScenario.dBSeaPErOversamplingMax * self.sbPErOversampling.Max);
    sbPEPhiOversampling.Position := saferound( container.scenario.dBSeaPEPhiOversampling);
    sbModeszOversampling.Position := saferound(container.scenario.dBSeaModeszOversampling * 10);
    sbModesrOversampling.Position := saferound(container.scenario.dBSeaModesrOversampling * 10);
    updateOversamplingLabels;
    case container.scenario.raySolverIterationMethod of
    kConstSSP: rgRKMethod.ItemIndex := 0;
    kRK2: rgRKMethod.ItemIndex := 1;
    kRK4: rgRKMethod.ItemIndex := 2;
    end;

    mmExportText.Text := UWAOpts.ExportFreeText;
    LoadLogoImage;

    sbNumberOfProcessors.Position := TProcessorAffinity.ProcessorsInUse;
    updateNumberOfProcessorsText;

    updateFromCode := false;
end;

procedure TOptionsForm.updateNumberOfProcessorsText;
begin
    lbNumberOfProcessors.Caption := 'Number of processors to use: ' +
        IntToStr(sbNumberOfProcessors.Position) +
        ' of ' + IntToStr(TProcessorAffinity.CPUCount);
end;

procedure TOptionsForm.LoadLogoImage;
var
    bmp : ISmart<TBitmap>;
    //jpg : TJpegImage;
    //png : TPNGImage;
    pic : ISmart<TPicture>;
    sImgType : string;
begin
    {draw the current export logo to the options form, if none exists then draw it in gray}
    bLogoImageLoaded := false;
    edLogoImage.Text := UWAOpts.ExportLogoFile;
    if not fileExists(UWAOpts.ExportLogoFile) then
    begin
        clearLogoImage;
        exit;
    end;

    sImgType := THelper.getImageType(UWAOpts.ExportLogoFile);

    if not ((sImgType = 'JPG') or
        (sImgType = 'PNG') or
        (sImgType = 'GIF') or
        (sImgType = 'BMP')) then
    begin
        clearLogoImage;
        exit;
    end;

    pic := TSmart<TPicture>.create(TPicture.Create);
    bmp := TSmart<TBitmap>.create(TBitmap.create);
    pic.LoadFromFile(UWAOpts.ExportLogoFile);    //NB uses file extension to determine the file type
    {if we make the bmp dims square, at the larger dimension of the pic,
    then when we stretchdraw, the stretching will be equal in both directions so
    the aspect ratio is preserved}
    bmp.Width := larger(pic.Width, pic.Height);
    bmp.Height := larger(pic.Width, pic.Height);
    {iLogoImageHeight is the number of vertical pixels acutally taken up by the scaled image}
    iLogoImageHeight := safeceil(pic.Height*imLogo.Width/bmp.Height);
    bmp.Canvas.Draw(0, 0, pic.Graphic);
    {set stretch mode for the destination - coloroncolor or halftone, experiment to
    find which works better}
    SetStretchBltMode(imLogo.Canvas.Handle,
                    HALFTONE);
    {stretchblit from our source - much better than canvas.stretchdraw}
    StretchBlt(imLogo.Canvas.Handle,
                    0,
                    0,
                    imLogo.Width,
                    imLogo.Height,
                    bmp.Canvas.Handle,
                    0,
                    0,
                    bmp.Width,
                    bmp.Height,
                    SRCCOPY);
    {invalidate the image to force it to redraw}
    imLogo.Invalidate;
    //imLogo.Canvas.stretchdraw(imLogo.Canvas.ClipRect,bmp);
    bLogoImageLoaded := true;
end;

procedure TOptionsForm.clearLogoImage;
begin
    imLogo.Canvas.Brush.Color := clSilver;
    imLogo.Canvas.Rectangle(imLogo.Canvas.ClipRect);
    bLogoImageLoaded := false;
end;

procedure TOptionsForm.rgExportDelimiterClick(Sender: TObject);
begin
    case rgExportDelimiter.ItemIndex of
        0 : UWAOpts.exportDelimiter := ',';
        1 : UWAOpts.exportDelimiter := ';';
        2 : UWAOpts.exportDelimiter := #09;
        3 : UWAOpts.exportDelimiter := #32;
    end;
end;

procedure TOptionsForm.rgFeetMetersClick(Sender: TObject);
begin
    case rgFeetMeters.ItemIndex of
        0 : UWAOpts.dispFeet := false;
        1 : UWAOpts.dispFeet := true;
    end;
    EditBoxesInit;

    {update the relevant charts and pages. update the charts as
    they are not necessarily updated when we show the parent tab.}
    framWater.updateSSPChart;
    framWater.updateAttenuationChart;
    framProbe.updateProbeChartAndTable;

    Mainform.activeFrame.didChangeUnits;
end;

procedure TOptionsForm.rgLevelsDisplayClick(Sender: TObject);
var
    tmpDisplay : TLevelsDisplay;
begin
    case rgLevelsDisplay.ItemIndex of
        0 : tmpDisplay := ldMax;
        1 : tmpDisplay := ldSingleLayer;
        2 : tmpDisplay := ldAllLayers;
    else
        tmpDisplay := ldMax;
    end;
    if tmpDisplay <> UWAOpts.LevelsDisplay then
    begin
        UWAOpts.setLevelsDisplay(tmpDisplay, container.scenario.dMax); {setLevelsDisplay also updates the cmax cmin and levelcontours}

        {if we happen to be looking at the results page, then update the results colorbar}
        if Mainform.mainPageControl.ActivePageIndex = ord(TMyPages.Results) then
            framResults.updateAllGraphics;

        {if we happen to be looking at the probe page, then update the edit boxes}
        if Mainform.mainPageControl.ActivePageIndex = ord(TMyPages.Probes) then
            framProbe.EditBoxesInit;

        if cbLiveUpdate1.Checked and assigned(GLForm) then
        begin
            GLForm.setRedrawResults;
            GLForm.forcedrawnow;
        end
        else
            bShouldUpdateResults := true;
    end;

    EditBoxesInit;
end;


procedure TOptionsForm.rgLevelTypeClick(Sender: TObject);
var
    oldLevelType: TLevelType;
begin
    if updateFromCode then exit;

    oldLevelType := container.scenario.levelType;
    if TLevelConverter.isTimeSeries(container.scenario.levelType) then
    begin
        case rgLevelType.ItemIndex of
            0 : container.scenario.setLevelType(kSELtime);
            1 : container.scenario.setLevelType(kSPLpk);
            2 : container.scenario.setLevelType(kSPLpkpk);
        end;
    end
    else
    begin
        case rgLevelType.ItemIndex of
            0 : container.scenario.setLevelType(kSPL);
            1 : container.scenario.setLevelType(kSELfreq);
            2 : container.scenario.setLevelType(kSPLCrestFactor);
        end;
    end;
    mmFreqDomainPeakLevelWarning.visible := container.scenario.levelType = kSPLCrestFactor;

    if oldLevelType <> container.scenario.levelType then
    begin
        framFish.didSetLevelType(container.scenario.levelType);
        framResults.updateAllGraphics();
        bShouldUpdateResults := true;
    end;
end;

procedure TOptionsForm.rgRKMethodClick(Sender: TObject);
begin
    case rgRKMethod.ItemIndex of
    0: container.scenario.raySolverIterationMethod := kConstSSP;
    1: container.scenario.raySolverIterationMethod := kRK2;
    2: container.scenario.raySolverIterationMethod := kRK4;
    end;
end;

procedure TOptionsForm.sbModeszOversamplingChange(Sender: TObject);
begin
    container.scenario.dBSeaModeszOversampling := sbModeszOversampling.Position / 10;
    updateOversamplingLabels;
end;

procedure TOptionsForm.sbModesROversamplingChange(Sender: TObject);
begin
    container.scenario.dBSeaModesrOversampling := sbModesROversampling.Position / 10;
    updateOversamplingLabels;
end;

procedure TOptionsForm.sbMovingSrcLinewidthChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.SourceLineWidth := sbMovingSrcLinewidth.Position;
        EditBoxesInit;
        if assigned(GLForm) then
            GLForm.ForceDrawnow;
    end;
end;

procedure TOptionsForm.sbNumberOfProcessorsChange(Sender: TObject);
begin
    updateNumberOfProcessorsText;
end;

procedure TOptionsForm.sbPEPhiOversamplingChange(Sender: TObject);
begin
    container.scenario.dBSeaPEPhiOversampling := self.sbPEPhiOversampling.Position;
    updateOversamplingLabels;
end;

procedure TOptionsForm.sbPErOversamplingChange(Sender: TObject);
begin
    container.scenario.dBSeaPErOversampling := self.sbPErOversampling.Position / self.sbPErOversampling.Max * TScenario.dBSeaPErOversamplingMax;
    updateOversamplingLabels;
end;

procedure TOptionsForm.sbPEzOversamplingChange(Sender: TObject);
begin
    container.scenario.dBSeaPEzOversampling := self.sbPEzOversampling.Position / self.sbPEzOversampling.Max * TScenario.dBSeaPEzOversamplingMax;
    updateOversamplingLabels;
end;

procedure TOptionsForm.updateOversamplingLabels;
begin
    lbPEzOversampling.Caption := 'Depth oversampling: ' + tostr(container.scenario.dBSeaPEzOversampling);
    lbPErOversampling.Caption := 'Range oversampling: ' + tostr(container.scenario.dBSeaPErOversampling);
    lbPEPhiOversampling.Caption := '3D: angle oversampling: ' + tostr(container.scenario.dBSeaPEPhiOversampling);
    lbModesZOversampling.Caption := 'Depth oversampling: ' + tostr(container.scenario.dBSeaModeszOversampling);
    lbModesROversampling.Caption := 'Range oversampling: ' + tostr(container.scenario.dBSeaModesrOversampling);
end;

procedure TOptionsForm.sbProbeSizeChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.ProbeSize := sbProbeSize.Position/sbProbeSize.Max*UWAOpts.MaxProbeSize;
        EditBoxesInit;
        if assigned(GLForm) then
            GLForm.ForceDrawnow;
    end;
end;

procedure TOptionsForm.sbResultsAlphaChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.resultsAlpha := sbResultsAlpha.Position/sbResultsAlpha.Max;
        UWAOpts.resultsContoursAlpha := sbResultsAlpha.Position/sbResultsAlpha.Max;
        EditBoxesInit;
        if cbLiveUpdate2.Checked and assigned(GLForm) then
        begin
            GLForm.setRedrawResults;
            GLForm.forcedrawnow;
        end
        else
            bShouldUpdateResults := true;
    end;
end;

procedure TOptionsForm.sbResultsContoursDarkeningChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.resultsContoursDarkening := 1.0 - (self.sbResultsContoursDarkening.Position / self.sbResultsContoursDarkening.Max);
        if cbLiveUpdate2.Checked and assigned(GLForm) then
        begin
            GLForm.setRedrawResults;
            GLForm.forceDrawNow;
        end
        else
            bShouldUpdateResults := true;
    end;
end;

procedure TOptionsForm.sbResultsContoursWidthChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.resultsContoursWidth := self.sbResultsContoursWidth.Position;
        if cbLiveUpdate2.Checked and assigned(GLForm) then
        begin
            glform.setRedrawResults;
            glform.forceDrawNow;
        end
        else
            bShouldUpdateResults := true;
    end;
end;

procedure TOptionsForm.sbSourceSizeChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.SourceSize := sbSourceSize.Position/sbSourceSize.Max*UWAOpts.MaxSourceSize;
        EditBoxesInit;
        if assigned(GLForm) then
            GLForm.ForceDrawnow;
    end;
end;

procedure TOptionsForm.sbZFactChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.zFactor := -sbZFact.Position * UWAOpts.ZFactMax / sbZFact.Max;
        if assigned(GLForm) then
        begin
            GLForm.setRedrawBathymetry();

            if cbLiveUpdate2.Checked and container.scenario.resultsExist then
                GLForm.setRedrawResults()
            else
                bShouldUpdateResults := true;

            GLForm.forcedrawnow;
        end;
    end;
end;

procedure TOptionsForm.sbZLandFactChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        UWAOpts.zLandFactor := -sbZLandFact.Position * UWAOpts.ZFactMax / sbZLandFact.Max;
        if assigned(GLForm) then
        begin
            GLForm.setRedrawBathymetry();
            GLForm.forcedrawnow;
        end;
    end;
end;

procedure TOptionsForm.editProjectNameChange(Sender: TObject);
begin
    if not updateFromCode then
        projName := editProjectName.Text;
end;

procedure TOptionsForm.editProjectNameEnter(Sender: TObject);
begin
    if ansicompareText(editProjectName.Text,'(untitled project)') = 0 then
        editProjectName.Text := ''
    else
        editProjectName.SelectAll;
end;

procedure TOptionsForm.btBrowseDefaultSaveDirClick(Sender: TObject);
//var
//    openDialog : TFileOpenDialog;
begin
//    openDialog := TFileOpenDialog.Create(self);
//    try
//        openDialog.Options := [fdoPickFolders];
//        openDialog.DefaultFolder := savePath;
//        if openDialog.Execute then
//            savePath := includeTrailingPathDelimiter(openDialog.FileName);
//    finally
//        openDialog.Free;
//    end;
//    EditBoxesInit;
end;

procedure TOptionsForm.btBrowseLogoClick(Sender: TObject);
begin
    if odLogo.Execute then
        UWAOpts.ExportLogoFile := odLogo.FileName;
    EditBoxesInit;
end;

procedure TOptionsForm.btBrowseTempDirClick(Sender: TObject);
var
    openDialog: ISmart<TFileOpenDialog>;
begin
    openDialog := TSmart<TFileOpenDialog>.create(TFileOpenDialog.Create(self));
    openDialog.Options := [fdoPickFolders];
    openDialog.DefaultFolder := tempPath;
    if openDialog.Execute then
        tempPath := includeTrailingPathDelimiter(openDialog.FileName);

    EditBoxesInit;
end;

procedure TOptionsForm.btCancelClick(Sender: TObject);
begin
    //
end;

procedure TOptionsForm.btDisplayIndDnClick(Sender: TObject);
begin
    zIndButtonsClick(-1);
end;

procedure TOptionsForm.btDisplayIndUpClick(Sender: TObject);
begin
    zIndButtonsClick(1);
end;

procedure TOptionsForm.zIndButtonsClick(const dir : integer);
begin
    if (UWAOpts.DisplayZInd + dir) < 0 then exit;
    if (UWAOpts.DisplayZInd + dir) >= container.scenario.dMax then exit;

    UWAOpts.setDisplayZInd(UWAOpts.DisplayZInd + dir, container.scenario.dMax);
    EditBoxesInit;
    if cbLiveUpdate1.Checked and assigned(GLForm) then
    begin
        GLForm.setRedrawResults;
        GLForm.forcedrawnow;
    end
    else
        bShouldUpdateResults := true;
end;


procedure TOptionsForm.mi1SecondClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '1';
end;

procedure TOptionsForm.bt1HourClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '3600';
end;

procedure TOptionsForm.bt24HourClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '86400';
end;

procedure TOptionsForm.btAssociateUWAClick(Sender: TObject);
begin
    reg.associateUWAFiles;
end;

procedure TOptionsForm.btBackgroundFlatColorClick(Sender: TObject);
begin
    with setColor(UWAOpts.backgroundColor) do
    begin
        if not HasValue then exit;
        UWAOpts.backgroundColor := val;
        GLForm.force2drawnow;
    end;
end;

class function TOptionsForm.setColor(const inColor: integer = - 1): Nullable<TColor>;
var
    ColorDialog: ISmart<TColorDialog>;
begin
    ColorDialog := TSmart<TColorDialog>.create(TColorDialog.create(application));
    ColorDialog.CustomColors.CommaText := UWAOpts.CustomColors;
    if inColor >= 0 then
        ColorDialog.color := inColor;

    if not ColorDialog.execute then exit;
    result := ColorDialog.color;
    UWAOpts.CustomColors := ColorDialog.CustomColors.CommaText;
end;


procedure TOptionsForm.btLandColorPickClick(Sender: TObject);
begin
    with setColor(UWAOpts.landColor) do
    begin
        if not HasValue then exit;
        UWAOpts.landColor := val;
        GLForm.setRedrawBathymetry;
        GLForm.force2drawnow;
    end;
end;


procedure TOptionsForm.btNODATAColorClick(Sender: TObject);
begin
    with setColor(UWAOpts.NaNColor) do
    begin
        if not HasValue then exit;
        UWAOpts.NaNColor := val;
        GLForm.setRedrawBathymetry;
        GLForm.force2drawnow;
    end;
end;

procedure TOptionsForm.btOKClick(Sender: TObject);
begin
    if isInt(edFreqOversampling.Text) then
        container.scenario.nOversampleFreqsPerBand := max(toint(edFreqOversampling.Text), 1);

    if isInt(self.edBellhopNRays.Text) then
        container.scenario.BellhopNRays := max(toint(self.edBellhopNRays.Text),0);

    if isInt(self.edBellhopNRaysAzimuth.Text) then
        container.scenario.bellhopNRaysAzimuth := max(toint(self.edBellhopNRaysAzimuth.Text),0);

    if isInt(self.edDBSeaModesMaxModes.Text) then
        container.scenario.DBSeaModesMaxModes := max(toint(self.edDBSeaModesMaxModes.Text),0);

    if isInt(edDBSeaRayMaxBottomReflections.Text) then
        container.scenario.DBSeaRayMaxBottomReflections := ifthen(toint(edDBSeaRayMaxBottomReflections.Text) > 0, toint(edDBSeaRayMaxBottomReflections.Text), 1000);

    if isint(edNumberPadeTerms.Text) then
        container.scenario.nPadeTerms := toint(edNumberPadeTerms.Text);
    if isInt(edNumberPadeTermsHorizontal.Text) then
        container.scenario.nPadeTermsHorizontal := toint(edNumberPadeTermsHorizontal.Text);

    if isInt(edGLMaxBathyPixels.Text) and
        (toint(edGLMaxBathyPixels.text) > 3) and
        UWAOpts.setMaxGLBathyPixels(toint(edGLMaxBathyPixels.text), container.bathymetry.ZDepthsLen) then
        GLForm.setRedraw();

    if isInt(edRadialSmoothing.Text) and container.scenario.setSmoothTLRadial(toint(edRadialSmoothing.Text)) then
    begin
        GLForm.setRedrawResults();
        //GLForm.forceDrawNow();
    end;

    if isInt(edExclusionZoneRadialSmoothing.Text) then
    begin
        container.scenario.iSmoothDistToExlusionZone := toint(edExclusionZoneRadialSmoothing.Text);
        if UWAOpts.levelLimits.showExclusionZone then
            GLForm.setRedrawResults();
    end;

    if isFloat(edInitialStepSize.Text) then
        container.scenario.rayTracingInitialStepsize := f2m(tofl(edInitialStepSize.Text),UWAOpts.dispFeet);

    //if isInt(edMaxCores.Text) then
    //    UWAOpts.maxProcessorsForSolve := toint(edMaxCores.Text);

    if isFloat(edRayStartAngle.Text) then
        container.scenario.rayTracingStartAngle := min(90,max(-90,tofl(edRayStartAngle.Text)));
    if isFloat(edRayEndAngle.Text) then
        container.scenario.rayTracingEndAngle := min(90,max(-90,tofl(edRayEndAngle.Text)));
    if container.scenario.rayTracingEndAngle < container.scenario.rayTracingStartAngle then
        container.scenario.rayTracingEndAngle := container.scenario.rayTracingStartAngle;

    bMultiCorePE := cbMulticorePE.Checked;
    bMultiCoreModes := cbMulticoreModes.Checked;
    bMultiCoreRay := cbMulticoreRay.Checked;

    if  not container.scenario.anySourceMoving and isfloat(edAssessmentPeriod.Text) and (tofl(edAssessmentPeriod.Text) > 0) then
    begin
        container.scenario.assessmentPeriod := tofl(edAssessmentPeriod.Text);
        framResults.updateAllGraphics();
        glForm.setRedrawResults();
    end;

    if sbNumberOfProcessors.Position <> TProcessorAffinity.ProcessorsInUse then
        TProcessorAffinity.setNCPUs(sbNumberOfProcessors.Position);

    UWAOpts.ExportFreeText := mmExportText.Text;
    GLForm.drawNow;
end;

procedure TOptionsForm.btSetAssessmentPeriodToClick(Sender: TObject);
var
    pnt: TPoint;
begin
    if GetCursorPos(pnt) then
        TButton(sender).PopupMenu.Popup(pnt.X, pnt.Y);
end;

procedure TOptionsForm.btSetCurrentSolversToDefaultClick(Sender: TObject);
begin
    //if assigned(prob) then
    //    prob.setCurrentSolversDefault;
end;

procedure TOptionsForm.btUseTopoLandColorsClick(Sender: TObject);
begin
    UWAOpts.GLUseTopoLandColors := true;
    GLForm.forceDrawNow;
end;

procedure TOptionsForm.btWaterColorPickClick(Sender: TObject);
begin
    with setColor(UWAOpts.waterColor) do
    begin
        if not HasValue then exit;
        UWAOpts.waterColor := val;
        GLForm.setRedrawBathymetry;
        GLForm.force2drawnow;
    end;
end;

procedure TOptionsForm.cbAutosaveAfterSolveClick(Sender: TObject);
begin
    UWAOpts.AutosaveAfterSolve := cbAutosaveAfterSolve.Checked;
end;

procedure TOptionsForm.cbCalculateAttenuationAtEachStepClick(Sender: TObject);
begin
    container.scenario.rayTracingCalculateAttenuationAtEachStep := cbCalculateAttenuationAtEachStep.Checked;
end;

procedure TOptionsForm.cbExclusionZoneStopAtLandClick(Sender: TObject);
begin
    UWAOpts.exclusionZoneStopAtLand := cbExclusionZoneStopAtLand.Checked;
end;

procedure TOptionsForm.cbRayTracingCoherentClick(Sender: TObject);
begin
    container.scenario.RayTracingCoherent := cbRayTracingCoherent.Checked;
end;

procedure TOptionsForm.cbFullXSecDepthClick(Sender: TObject);
begin
    UWAOpts.fullXSecDepth := cbFullXSecDepth.checked;

    if assigned(GLForm) and GLForm.bDrawCrossSection then
        GLForm.ForceDrawNow();
end;

procedure TOptionsForm.cbHideProbesOnExportClick(Sender: TObject);
begin
    UWAOpts.bHideProbesOnExport := cbHideProbesOnExport.Checked;
end;

procedure TOptionsForm.cbInterpolateLevelsClick(Sender: TObject);
begin
    //prob.InterpolateSoundLevs := cbInterpolateLevels.Checked;
end;

procedure TOptionsForm.cbKrakenCoupledModesClick(Sender: TObject);
begin
    container.scenario.NormalModesCoupledModes := cbKrakenCoupledModes.Checked;
end;

procedure TOptionsForm.cbMakeTLMonotonicClick(Sender: TObject);
begin
    container.scenario.setMakeTLMonotonic(cbMakeTLMonotonic.Checked);
end;

procedure TOptionsForm.cbNaNSeafloorLevelsClick(Sender: TObject);
begin
    container.scenario.NaNSeafloorLevels := cbNaNSeafloorLevels.Checked;
end;

procedure TOptionsForm.cbPEFFTClick(Sender: TObject);
begin
    bPESS := cbPEFFT.Checked;
end;

procedure TOptionsForm.cbResultsTrisClick(Sender: TObject);
begin
    UWAOpts.resultsTris := cbResultsTris.Checked;
    if cbLiveUpdate1.Checked and assigned(GLForm) then
    begin
        GLForm.setRedrawResults();
        GLForm.forcedrawnow;
    end
    else
        bShouldUpdateResults := true;
end;

procedure TOptionsForm.cbSaveResultsToFileClick(Sender: TObject);
begin
    //UWAOpts.SaveLevelsToFile := cbSaveResultsToFile.Checked;
end;

procedure TOptionsForm.cbShowLandInXSecClick(Sender: TObject);
begin
    UWAOpts.GLShowLandinXSec := cbShowLandInXSec.Checked;
    if assigned(GLForm) then
        GLForm.forcedrawnow;
end;

procedure TOptionsForm.cbSmoothColorMapClick(Sender: TObject);
begin
    UWAOpts.resultsColormapSmooth := cbSmoothColorMap.Checked;

    {update and redraw the results colorbar}
    if mainform.mainPageControl.ActivePageIndex = ord(TMyPages.Results) then
        framResults.updateAllGraphics();

   if cbLiveUpdate1.Checked and assigned(GLForm) then
    begin
        GLForm.setRedrawResults();
        GLForm.forceDrawNow();
    end
    else
        bShouldUpdateResults := true;

    mainform.UpdateVisibilityMenuItems(UWAOpts.ComponentVisibility);//so that the menu item is checked or not
    EditBoxesInit;
end;

procedure TOptionsForm.cbStopMarchingSolutionAtLandClick(Sender: TObject);
begin
    container.scenario.stopMarchingSolverAtLand := cbStopMarchingSolutionAtLand.Checked;
end;

procedure TOptionsForm.cbXSecTruncateAtWaterSurfaceClick(Sender: TObject);
begin
    UWAOpts.xSecTruncateAtWaterSurface := cbXSecTruncateAtWaterSurface.Checked;
    if assigned(GLForm) and GLForm.bDrawCrossSection then
        GLForm.ForceDrawNow();
end;

end.
