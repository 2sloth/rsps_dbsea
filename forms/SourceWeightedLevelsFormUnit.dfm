object SourceWeightedLevelsForm: TSourceWeightedLevelsForm
  Left = 0
  Top = 0
  Caption = 'Weighted source levels'
  ClientHeight = 670
  ClientWidth = 749
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbTitle: TLabel
    Left = 20
    Top = 10
    Width = 326
    Height = 13
    Caption = 
      'Current source levels weighted by marine weightings from databas' +
      'e'
  end
  object btOK: TButton
    Left = 609
    Top = 629
    Width = 122
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object sg: TStringGrid
    Left = 20
    Top = 40
    Width = 707
    Height = 569
    TabOrder = 1
  end
  object btCopyToClipboard: TButton
    Left = 473
    Top = 629
    Width = 122
    Height = 25
    Caption = 'Copy to clipboard'
    TabOrder = 2
    OnClick = btCopyToClipboardClick
  end
end
