object SeabedMapFrm: TSeabedMapFrm
  Left = 0
  Top = 0
  Caption = 'Seabed map'
  ClientHeight = 789
  ClientWidth = 981
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    981
    789)
  PixelsPerInch = 96
  TextHeight = 13
  object imMap: TImage
    Left = 248
    Top = 16
    Width = 700
    Height = 700
    Anchors = [akLeft, akTop, akRight, akBottom]
    OnClick = imMapClick
    OnMouseDown = imMapMouseDown
    OnMouseMove = imMapMouseMove
    OnMouseUp = imMapMouseUp
  end
  object lbInfo: TLabel
    Left = 248
    Top = 752
    Width = 28
    Height = 13
    Anchors = [akLeft, akTop, akBottom]
    Caption = 'lbInfo'
  end
  object pnTools: TPanel
    Left = 24
    Top = 16
    Width = 201
    Height = 281
    BevelKind = bkFlat
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object sbRect: TSpeedButton
      Left = 16
      Top = 19
      Width = 160
      Height = 22
      GroupIndex = 1
      Down = True
      Caption = 'Rectangle selection'
      OnClick = sbRectClick
    end
    object sbEllipse: TSpeedButton
      Left = 16
      Top = 47
      Width = 160
      Height = 22
      GroupIndex = 1
      Caption = 'Ellipse selection'
      OnClick = sbEllipseClick
    end
    object sbFreehand: TSpeedButton
      Left = 16
      Top = 75
      Width = 160
      Height = 22
      GroupIndex = 1
      Caption = 'Freehand selection'
      OnClick = sbFreehandClick
    end
    object sbBrush: TSpeedButton
      Left = 16
      Top = 168
      Width = 160
      Height = 22
      GroupIndex = 1
      Caption = 'Circular brush'
      OnClick = sbBrushClick
    end
    object btUndo: TButton
      Left = 16
      Top = 247
      Width = 160
      Height = 25
      Caption = 'Undo'
      TabOrder = 0
      OnClick = btUndoClick
    end
    object btSet: TButton
      Left = 16
      Top = 109
      Width = 160
      Height = 25
      Caption = 'Fill selection'
      TabOrder = 1
      OnClick = btSetClick
    end
    object tbBrushSize: TTrackBar
      Left = 16
      Top = 196
      Width = 160
      Height = 45
      Max = 40
      Min = 1
      Frequency = 5
      Position = 10
      TabOrder = 2
    end
  end
  object btOK: TButton
    Left = 680
    Top = 747
    Width = 124
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btCancel: TButton
    Left = 824
    Top = 747
    Width = 124
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object sgPalette: TStringGrid
    Left = 24
    Top = 320
    Width = 201
    Height = 396
    Anchors = [akLeft, akTop, akBottom]
    ColCount = 3
    DefaultColWidth = 40
    FixedCols = 0
    FixedRows = 0
    TabOrder = 3
    OnDrawCell = sgPaletteDrawCell
    OnMouseDown = sgPaletteMouseDown
    ColWidths = (
      40
      40
      40)
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 32
    Top = 736
  end
end
