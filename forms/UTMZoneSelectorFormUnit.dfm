object UTMZoneSelectorForm: TUTMZoneSelectorForm
  Left = 0
  Top = 0
  Caption = 'UTM zone'
  ClientHeight = 123
  ClientWidth = 249
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbZone: TLabel
    Left = 24
    Top = 35
    Width = 47
    Height = 13
    Caption = 'UTM zone'
  end
  object edZone: TEdit
    Left = 88
    Top = 32
    Width = 135
    Height = 21
    TabOrder = 0
    Text = 'edZone'
  end
  object btOK: TButton
    Left = 67
    Top = 80
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btOKClick
  end
  object btCancel: TButton
    Left = 148
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
