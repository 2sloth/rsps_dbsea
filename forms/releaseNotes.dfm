object releaseNotesForm: TreleaseNotesForm
  Left = 0
  Top = 0
  Caption = 'Release notes'
  ClientHeight = 715
  ClientWidth = 931
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    931
    715)
  PixelsPerInch = 96
  TextHeight = 13
  object mmNotes: TMemo
    Left = 0
    Top = 0
    Width = 931
    Height = 673
    Align = alTop
    Lines.Strings = (
      'mmNotes')
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btOK: TButton
    Left = 844
    Top = 679
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
end
