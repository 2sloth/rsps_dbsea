object SedimentAddForm: TSedimentAddForm
  Left = 0
  Top = 0
  Caption = 'Insert data into user database'
  ClientHeight = 399
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblC: TLabel
    Left = 32
    Top = 64
    Width = 33
    Height = 13
    Caption = 'c (m/s)'
  end
  object lblDensity: TLabel
    Left = 32
    Top = 104
    Width = 75
    Height = 13
    Caption = 'Density (kg/m'#179')'
  end
  object lblAttenuation: TLabel
    Left = 32
    Top = 144
    Width = 140
    Height = 13
    Caption = 'Attenuation (dB/wavelength)'
  end
  object lblName: TLabel
    Left = 32
    Top = 27
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblComments: TLabel
    Left = 32
    Top = 199
    Width = 50
    Height = 13
    Caption = 'Comments'
  end
  object EditC: TEdit
    Left = 200
    Top = 61
    Width = 200
    Height = 21
    TabOrder = 0
    Text = '0.0'
  end
  object EditRho: TEdit
    Left = 200
    Top = 101
    Width = 200
    Height = 21
    TabOrder = 1
    Text = '0.0'
  end
  object EditAttenuation: TEdit
    Left = 200
    Top = 141
    Width = 200
    Height = 21
    TabOrder = 2
    Text = '0.0'
  end
  object EditName: TEdit
    Left = 200
    Top = 24
    Width = 200
    Height = 21
    TabOrder = 3
  end
  object memoComments: TMemo
    Left = 32
    Top = 226
    Width = 368
    Height = 88
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object btOK: TButton
    Left = 231
    Top = 354
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object btCancel: TButton
    Left = 325
    Top = 354
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
end
