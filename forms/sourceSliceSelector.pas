unit sourceSliceSelector;

interface

uses
  winapi.Windows, winapi.Messages, system.SysUtils,
  system.Variants, system.Classes, vcl.Graphics, vcl.Controls, vcl.Forms,
  generics.collections,
  vcl.Dialogs, vcl.extctrls, vcl.stdctrls, vcl.comctrls, DBSeaColours, helperfunctions, system.math,
  vcltee.Series, vcltee.Chart, soundSource, spectrum, basetypes, mcklib,
  dBSeaconstants, system.strutils, system.types, VCLTee.TeEngine, VclTee.TeeGDIPlus,
  VCLTee.TeeProcs, levelConverter, dBSeaOptions, problemContainer,
  SmartPointer, solversGeneralFunctions;

type

    TDistLev = record
        dist,lev: double;

        function isOK: boolean;
    end;
    TWrappedDistLevArray = class(TObject)
    public
        ang: double;
        slice: integer;
        arr: TArray<TDistLev>;
    end;
    TWrappedDistLevArrayList = TObjectList<TWrappedDistLevArray>;

  TsourceSliceSelectorForm = class(TForm)
    pbSlice: TPaintBox;
    edSlice: TEdit;
    btGoToCrossSection: TButton;
    btDone: TButton;
    udSlice: TUpDown;
    lbAngle: TLabel;
    chLevs: TChart;
    srLevs: TFastLineSeries;
    cbOnlyThisSource: TCheckBox;
    edPosition: TEdit;
    udPosition: TUpDown;
    lbPosition: TLabel;
    lbSlice: TLabel;
    sr10Log: TFastLineSeries;
    sr20Log: TFastLineSeries;
    lbDashedLinesHint: TLabel;
    btSaveCurrent: TButton;
    btClearSaved: TButton;
    btShowAllSlices: TButton;
    btToClipboard: TButton;
    btAllToClipboard: TButton;
    cbLogDistance: TCheckBox;
    srBathy: TFastLineSeries;
    cbShowdepth: TCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure edSliceChange(Sender: TObject);
    procedure pbSlicePaint(Sender: TObject);
    procedure pbSliceMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbOnlyThisSourceClick(Sender: TObject);
    procedure edPositionChange(Sender: TObject);
    procedure btSaveCurrentClick(Sender: TObject);
    procedure btClearSavedClick(Sender: TObject);
    procedure btShowAllSlicesClick(Sender: TObject);
    procedure btToClipboardClick(Sender: TObject);
    procedure btAllToClipboardClick(Sender: TObject);
    procedure cbLogDistanceClick(Sender: TObject);
    procedure cbShowdepthClick(Sender: TObject);
  private
    spect: TSpectrum; //used in calcs
    saved: TWrappedDistLevArrayList;

    procedure updateChart;
    function  levForI(const i: integer; const ang: double): double;
    function  getDistLevArray(const n: integer = -1): TArray<TDistLev>;
    function  getAngleRadians(const n: integer): double;
    function  addSlice(const sliceInd: integer; const thisSlice: TArray<TDistLev>): string;
  public
    src: TSource;
    iPosition: integer;
  end;

var
  sourceSliceSelectorForm: TsourceSliceSelectorForm;


implementation

{$R *.dfm}

//uses scenario;

procedure TsourceSliceSelectorForm.FormCreate(Sender: TObject);
begin
    iPosition := 0;
    saved := TWrappedDistLevArrayList.create(true);
    spect := TSpectrum.create;
end;

procedure TsourceSliceSelectorForm.FormDestroy(Sender: TObject);
begin
    saved.free;
    spect.free;

    inherited;
end;

procedure TsourceSliceSelectorForm.FormShow(Sender: TObject);
begin
    pbSlice.Repaint;
    edPosition.Enabled := src.isMovingSource;
    udPosition.Enabled := src.isMovingSource;
    srLevs.SeriesColor := TDBSeaColours.midBlue;
    sr10Log.SeriesColor := TDBSeaColours.lightBlue;
    sr20Log.SeriesColor := TDBSeaColours.lightBlue;
end;

procedure TsourceSliceSelectorForm.pbSliceMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if not assigned(src) then exit;

    udSlice.Position := saferound(arctan2((pbSlice.Height div 2 + 1) - Y,X - (pbSlice.Width div 2 + 1)) / 2.0 / pi * src.nSlices);
    pbSlice.Repaint;
end;

procedure TsourceSliceSelectorForm.pbSlicePaint(Sender: TObject);
const
    iStroke: integer = 30;
    iArrowLen: integer = 30;
    iSmallTickLen: integer = 3;
    iMedTickLen: integer = 8;
    iLongTickLen: integer = 10;
var
    i, h, w: integer;
    cx, cy: integer;
    ang: double;
begin
    if not assigned(src) then exit;

    while udSlice.Position >= src.nSlices do
        udSlice.Position := udSlice.Position - src.nSlices;
    while udSlice.Position < 0 do
        udSlice.Position := udSlice.Position + src.nSlices;

    h := pbSlice.Height;
    w := pbSlice.Width;
    cx := w div 2 + 1;
    cy := h div 2 + 1;

    pbSlice.Canvas.Brush.Color := self.Color;// TDBSeaColours.directionFill;
    pbSlice.Canvas.FillRect(pbSlice.Canvas.ClipRect);

    pbSlice.Canvas.Pen.Color := clBlack;
    pbSlice.Canvas.Pen.Width := 1;
    {small 15 degree strokes}
    for i := 0 to 23 do
    begin
        pbSlice.Canvas.MoveTo(saferound(cx + iStroke*cos(i*pi/12)),
                                    saferound(cy + iStroke*sin(i*pi/12)));
        pbSlice.Canvas.LineTo(saferound(cx + (iStroke + iSmallTickLen)*cos(i*pi/12)),
                                    saferound(cy + (iStroke + iSmallTickLen)*sin(i*pi/12)));
    end;

    {med 45 degree strokes}
    for i := 0 to 7 do
    begin
        pbSlice.Canvas.MoveTo(saferound(cx + iStroke*cos(i*pi/4)),
                                    saferound(cy + iStroke*sin(i*pi/4)));
        pbSlice.Canvas.LineTo(saferound(cx + (iStroke + iMedTickLen)*cos(i*pi/4)),
                                    saferound(cy + (iStroke + iMedTickLen)*sin(i*pi/4)));
    end;

    {long 90 degree strokes}
    for i := 0 to 3 do
    begin
        pbSlice.Canvas.MoveTo(saferound(cx + iStroke*cos(i*pi/2)),
                                    saferound(cy + iStroke*sin(i*pi/2)));
        pbSlice.Canvas.LineTo(saferound(cx + (iStroke + iLongTickLen)*cos(i*pi/2)),
                                    saferound(cy + (iStroke + iLongTickLen)*sin(i*pi/2)));
    end;

    ang := self.getAngleRadians(udSlice.Position);

    {draw arrow shaft}
    pbSlice.Canvas.Pen.Color := TDBSeaColours.directionArrow;
    pbSlice.Canvas.Pen.Width := 3;
    pbSlice.Canvas.MoveTo(cx,cy);
    pbSlice.Canvas.LineTo(cx + saferound(cos(ang)*iArrowLen),cy - saferound(sin(ang)*iArrowLen));

    {update the text in the box, but not if we are editing the angles right now}

    lbAngle.caption := tostr(round1(angleRadToCompassAngleDeg(ang)));
    if lbAngle.caption = '360' then
        lbAngle.caption := '0';
    lbAngle.caption := lbAngle.caption + ' ' + #176; //degree symbol

    updateChart;
end;

procedure TsourceSliceSelectorForm.btAllToClipboardClick(Sender: TObject);
var
    thisSlice: TArray<TDistLev>;
    i, n: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    thisSlice := self.getDistLevArray();

    sb.Append('Distance ' + ifthen(UWAOpts.dispFeet,'(ft)','(m)'));
    for I := 0 to src.iMax - 1 do
        sb.append(#09 + floattostrf(m2f(src.ranges(i),UWAOpts.DispFeet),fffixed,14,1));

    for n := 0 to src.nSlices - 1 do
        sb.append(addSlice(n, self.getDistLevArray(n)));

    THelper.setClipboard(sb.ToString);
end;

procedure TsourceSliceSelectorForm.btClearSavedClick(Sender: TObject);
begin
    saved.clear;
    updateChart;
end;

procedure TsourceSliceSelectorForm.btSaveCurrentClick(Sender: TObject);
var
    wrapped: TWrappedDistLevArray;
begin
    wrapped := TWrappedDistLevArray.create();
    wrapped.slice := udSlice.Position;
    wrapped.arr := self.getDistLevArray();
    saved.add(wrapped);
end;

procedure TsourceSliceSelectorForm.btShowAllSlicesClick(Sender: TObject);
var
    n,i: integer;
    sr: TFastLineSeries;
    thisSlice: TArray<TDistLev>;
begin
    for n := 0 to src.nSlices - 1 do
    begin
        sr := TFastLineSeries.create(chLevs);
        chLevs.AddSeries(sr);

        thisSlice := self.getDistLevArray(n);
        for i := 0 to min(length(thisSlice), src.iMax) - 1 do
            if thisSlice[i].isOK then
                sr.AddXY(m2f(src.ranges(i),UWAOpts.DispFeet),thisSlice[i].lev);
    end;
end;

function TsourceSliceSelectorForm.addSlice(const sliceInd: integer; const thisSlice: TArray<TDistLev>): string;
var
    i: integer;
begin
    result := #13#10 + 'Slice ' + inttostr(sliceInd) + ': ' + FloatToStrF(angleRadToCompassAngleDeg(self.getAngleRadians(sliceInd)),ffFixed,14,1) + #176;
    for i := 0 to min(length(thisSlice), src.iMax) - 1 do
        if thisSlice[i].isOK then
            result := result + #09 + FloatToStrF(thisSlice[i].lev, ffFixed, 14,1)
        else
            result := result + #09 + '-';
end;

procedure TsourceSliceSelectorForm.btToClipboardClick(Sender: TObject);
var
    thisSlice: TArray<TDistLev>;
    i: integer;
    sb: ISmart<TStringBuilder>;
    wrapped: TWrappedDistLevArray;
    zprofile: TRangeValArray;
    r,z: double;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    thisSlice := self.getDistLevArray();

    sb.Append('Distance ' + ifthen(UWAOpts.dispFeet,'(ft)','(m)'));
    for I := 0 to src.iMax - 1 do
        sb.append(#09 + floattostrf(m2f(src.ranges(i),UWAOpts.DispFeet),fffixed,14,1));

    sb.append(addSlice(udSlice.position, thisSlice));

    //add line with bathymetry
    if saved.Count = 0 then
    begin
        zprofile := src.bathyDelegate.getBathymetryVector2(src.pos.X, src.pos.Y, self.getAngleRadians(udSlice.Position), src.rmax, src.iExtendPastEdge);

        sb.append(#13#10 + 'Bathymetry/land height ' + ifthen(UWAOpts.dispFeet,'(ft)','(m)'));
        for i := 0 to min(length(thisSlice), src.iMax) - 1 do
        begin
            r := src.ranges(i);
            if r > zprofile[length(zprofile) - 1].range then
                sb.append(#09 + '-')
            else
            begin
                z := TSolversGeneralFunctions.interpolationFunPE(zprofile,r);
                if isnan(z) then
                    sb.append(#09 + '-')
                else
                    sb.append(#09 + floattostrf(m2f(-z,UWAOpts.DispFeet),fffixed,14,1));
            end;
        end;
    end;

    for wrapped in saved do
        sb.Append(addSlice(wrapped.slice, wrapped.arr));

    THelper.setClipboard(sb.ToString);
end;

procedure TsourceSliceSelectorForm.cbLogDistanceClick(Sender: TObject);
begin
    chLevs.BottomAxis.Logarithmic := cbLogDistance.Checked;
    //chLevs.BottomAxis.Minimum := ifthen(chLevs.BottomAxis.Logarithmic, src.ranges(1), src.ranges(0));
end;

procedure TsourceSliceSelectorForm.cbOnlyThisSourceClick(Sender: TObject);
begin
    updateChart;
end;

procedure TsourceSliceSelectorForm.cbShowdepthClick(Sender: TObject);
begin
    updateChart;
end;

procedure TsourceSliceSelectorForm.edPositionChange(Sender: TObject);
begin
    if isint(edPosition.Text) and (toint(edPosition.Text) < src.getMovingPosMax) then
    begin
        iPosition := toint(edPosition.Text);
        updateChart;
    end;
end;

procedure TsourceSliceSelectorForm.edSliceChange(Sender: TObject);
begin
    pbSlice.Repaint;
end;

function TsourceSliceSelectorForm.getAngleRadians(const n: integer): double;
begin
    result := angleWrap(2.0 * n / src.nSlices * pi);
end;

function TsourceSliceSelectorForm.getDistLevArray(const n: integer = -1): TArray<TDistLev>;
var
    i: integer;
    ang: double;
begin
    if not assigned(src) then exit;
    if not src.srcSolved then exit;
    if src.isMovingSource and container.scenario.movingSourcesUseGriddedTL and not src.griddedTL.bSizeIsAtPlannedSize then exit;

    ang := self.getAngleRadians(ifthen(n < 0,udSlice.Position,n));
    setlength(result, src.iMax);
    for i := 1 to src.iMax - 1 do
    begin
        result[i].dist := src.ranges(i);
        result[i].lev := levForI(i, ang);
        if result[i].isOK then
            result[i].lev := container.scenario.convertLevel(result[i].lev); //no crest factor here as we got the lev from theRA
    end;
end;

procedure TsourceSliceSelectorForm.updateChart;
var
    i: integer;
    mn,mx: TFpoint;
    lev0, z: double;
    sr: TFastLineSeries;
    wrapped: TWrappedDistLevArray;
    zprofile: TRangeValArray;
    thisSlice: TArray<TDistLev>;
    dl: TDistLev;
begin
    if not assigned(src) then exit;

    srBathy.Clear;

    //add line with bathymetry
    chLevs.RightAxis.Visible := cbShowdepth.Checked;
    if cbShowdepth.Checked then
    begin
        zprofile := src.bathyDelegate.getBathymetryVector2(src.pos.X, src.pos.Y, self.getAngleRadians(udSlice.Position), src.rmax, src.iExtendPastEdge);
        for i := 1 to src.iMax - 1 do
            if src.ranges(i) <= zprofile[length(zprofile) - 1].range then
            begin
                z := TSolversGeneralFunctions.interpolationFunPE(zprofile,src.ranges(i));
                if not isnan(z) then
                    srBathy.AddXY(m2f(src.ranges(i),UWAOpts.DispFeet),m2f(-z,UWAOpts.DispFeet));
            end;
    end;

    if not src.srcSolved then exit;
    if src.isMovingSource and container.scenario.movingSourcesUseGriddedTL and not src.griddedTL.bSizeIsAtPlannedSize then exit;

    mn.x := nan;
    mn.y := nan;
    mx.x := nan;
    mx.y := nan;

    srLevs.Clear;
    sr10Log.Clear;
    sr20Log.Clear;
    while chLevs.Seriescount > 4 do
        chLevs.SeriesList[chLevs.SeriesCount - 1].Free;

    lev0 := TLevelConverter.convertSourceLevel(src.srcLevel,
                kSL,
                src.displayLevelType,
                container.scenario.assessmentPeriod,
                src.srcSpectrumCrestFactor);

    chLevs.LeftAxis.Title.Caption := 'Level (dB' + TLevelConverter.levelTypeName(container.scenario.levelType) + ')';
    chLevs.BottomAxis.Title.Caption := ifthen(UWAOpts.DispFeet,'Distance from source (ft)','Distance from source (m)');

    thisSlice := self.getDistLevArray();
    for i := 1 to src.iMax - 1 do
        if thisSlice[i].isOK then
        begin
            srLevs.AddXY(m2f(thisSlice[i].dist,UWAOpts.DispFeet),thisSlice[i].lev);

            if (i > 0) and (not src.isTimeSeries) then
            begin
                sr10Log.AddXY(m2f(thisSlice[i].dist,UWAOpts.DispFeet),lev0 - 10*log10(thisSlice[i].dist));
                sr20Log.AddXY(m2f(thisSlice[i].dist,UWAOpts.DispFeet),lev0 - 20*log10(thisSlice[i].dist));
            end;

            //get the limts of the graph
            lesserGreater(mn.X,mx.X,m2f(thisSlice[i].dist,UWAOpts.DispFeet));
            lesserGreater(mn.Y,mx.Y,thisSlice[i].lev);
        end;

    for wrapped in saved do
    begin
        sr := TFastLineSeries.create(chLevs);
        chLevs.AddSeries(sr);
        for dl in wrapped.arr do
            if dl.isOK then
                sr.AddXY(m2f(dl.dist,UWAOpts.DispFeet),dl.lev);
    end;

    chLevs.Visible := not ( isnan(mn.x) or isnan(mn.y) or isnan(mx.x) or isnan(mx.y) );
    chLevs.BottomAxis.Logarithmic := cbLogDistance.Checked;
    //chLevs.BottomAxis.Minimum := ifthen(chLevs.BottomAxis.Logarithmic, src.ranges(1), src.ranges(0));
end;

function TsourceSliceSelectorForm.levForI(const i: integer; const ang: double): double;
var
    fi,d: integer;
    depthLevs: TDoubleDynArray;
begin
    if cbOnlyThisSource.Checked then
    begin
        //get levels from only this source, and we have to do all the processing here as it doesn't exist in TSource
        setlength(depthLevs,container.scenario.dmax);
        spect.clear;
        for fi := 0 to container.scenario.lenfi - 1 do
        begin
            for d := 0 to container.scenario.dMax - 1 do
            begin
                depthLevs[d] := src.getLevelAtPos(src.pos.X + src.ranges(i) * cos(ang),
                                                  src.pos.Y + src.ranges(i) * sin(ang),
                                                  d,
                                                  -1,
                                                  fi + container.scenario.startfi,
                                                  container.scenario.Bandwidth);
            end; //d
            spect.setAmpbyInd(fi + container.scenario.startfi,depthLevs.max,container.scenario.Bandwidth);
        end; //fi
        result := spect.Level;
    end
    else
    begin
        //getting total levels from theRA
        result := container.scenario.theRA.getLevAtXY(src.pos.X + src.ranges(i) * cos(ang),
                                                   src.pos.Y + src.ranges(i) * sin(ang),
                                                   ldMax,0,true);
    end;
end;

{ TDistLev }

function TDistLev.isOK: boolean;
begin
    result := (not isnan(lev)) and (lev > lowLev);
end;

end.
