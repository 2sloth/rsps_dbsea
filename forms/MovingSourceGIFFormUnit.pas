unit MovingSourceGIFFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes,
  system.generics.collections,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, vcl.imaging.gifimg, Vcl.ComCtrls,
  problemcontainer, SmartPointer, AviWriter_2;

type

    TIntBMPFunc = procedure(const p: integer; const bmp: TBitmap) of object;

  TMovingSourceGIFForm = class(TForm)
    btGenerate: TButton;
    btDone: TButton;
    im: TImage;
    lbInvalid: TLabel;
    pb: TProgressBar;
    btCancel: TButton;
    btSaveToGIF: TButton;
    sdGIF: TSaveDialog;
    edFrameSpeed: TEdit;
    lbFrameDelay: TLabel;
    btSaveToAVI: TButton;
    sdAVI: TSaveDialog;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure btGenerateClick(Sender: TObject);
    procedure btDoneClick(Sender: TObject);
    procedure btSaveToGIFClick(Sender: TObject);
    procedure btSaveToAVIClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    cancel: boolean;
    gif,gifForImg: TGIFImage;
    avi: TAviWriter_2;
    bmps: TObjectList<TBitmap>;

    function frameDelay: integer;
  public
    bmpFunc: TIntBMPFunc;
  end;

var
  MovingSourceGIFForm: TMovingSourceGIFForm;

implementation

{$R *.dfm}

procedure TMovingSourceGIFForm.btDoneClick(Sender: TObject);
begin
    self.cancel := true;
    btGenerate.Enabled := true;
    pb.Visible := false;
    btCancel.Visible := false;
end;

procedure TMovingSourceGIFForm.btGenerateClick(Sender: TObject);
var
    bmp, bmpForIm: TBitmap;
    p: integer;
    gifControl: ISmart<TGIFGraphicControlExtension>;
begin
    if not container.scenario.anySourceMoving then exit;

    btGenerate.Enabled := false;
    btSaveToGIF.enabled := false;
    btSaveToAVI.Enabled := false;
    pb.Visible := true;
    pb.Max := container.scenario.maxSourceMovingPos;
    pb.Position := 0;
    btCancel.Visible := true;
    Application.ProcessMessages;

    BorderStyle := bsSingle;
    cancel := false;
    bmp := nil;
    bmpForIm := nil;
    try
        bmps.clear;

        //gif.AnimationSpeed := 25;
        gif.Clear;
        gif.Images.Clear;
        gifForImg.clear;
        gifForImg.Images.clear;
        gifForImg.SetSize(im.Width, im.Height);
        gifForImg.AnimationSpeed := 100;

        bmpForIm := TBitmap.create();
        bmpForIm.PixelFormat := pf24bit;
        bmpForIm.SetSize(im.Width, im.Height);

        for p := 0 to container.scenario.maxSourceMovingPos - 1 do
        begin
            if cancel then exit;

            try
                bmp := TBitmap.create;
                bmp.PixelFormat := pf24bit;
                bmpFunc(p, bmp);
            except
                continue;
            end;

            if cancel then exit;
            if p = 0 then
                gif.SetSize(bmp.Width, bmp.Height);

            bmps.add(bmp);

            gifControl := TSmart<TGIFGraphicControlExtension>.create(TGIFGraphicControlExtension.Create(gif.Add(bmp)));
            gifControl.Delay := self.frameDelay;

            bmpForIm.Canvas.StretchDraw(im.ClientRect, bmp);
            gifForImg.add(bmpForIm);

            self.im.Picture.assign(bmpForIm);
            pb.StepIt;
            application.ProcessMessages;
        end;

        if cancel then exit;
        TGIFAppExtNSLoop.Create(gifForImg.Images.Frames[0]).Loops := 0;  //loop continuously
        im.Picture.Assign(gifForImg);
        (im.Picture.Graphic as TGIFImage).AnimateLoop := glContinously;
        (im.Picture.Graphic as TGIFImage).Animate := True;

    finally
        bmpForIm.free;

        btGenerate.Enabled := true;
        btSaveToGIF.enabled := true;
        btSaveToAVI.Enabled := true;
        pb.Visible := false;
        btCancel.visible := false;
        self.BorderStyle := bsSizeable;
    end;
end;

function TMovingSourceGIFForm.frameDelay: integer;
begin
    try
        result := StrToInt(edFrameSpeed.Text);
    except on e: exception do
        result := 100;
    end;
end;

procedure TMovingSourceGIFForm.btSaveToGIFClick(Sender: TObject);
begin
    if (gif.Images.Count > 0) and sdGIF.Execute then
        gif.SaveToFile(sdGIF.FileName);
end;

procedure TMovingSourceGIFForm.btSaveToAVIClick(Sender: TObject);
var
    bmp: TBitmap;
begin
    if bmps.Count <= 0 then
    begin
        ShowMessage('No frames exist');
        exit;
    end;

    if not sdAVI.Execute then exit;

    avi.filename := sdAVI.FileName;
    if avi.FileName = '' then
    begin
        ShowMessage('Choose an output file name');
        exit;
    end;

//    if FileExists(avi.FileName) then
//        if MessageDlg('File exists, overwrite?', mtWarning, [mbYes, mbNo], 0) = mrNo then
//            exit;

    bmp := bmps.first;
    avi.SetCompression('IYUV'); //todo check if this exists on other computers
    avi.Width := bmp.Width;
    avi.Height := bmp.Height;
    avi.FrameTime := self.frameDelay;
    avi.WavFileName := '';
    avi.Stretch := true;
    avi.OnTheFlyCompression := true;
    avi.InitVideo(bmp);

    for bmp in bmps do
        avi.AddFrame(bmp);

    avi.FinalizeVideo;
    avi.WriteAvi;
end;

procedure TMovingSourceGIFForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    cancel := true;
end;

procedure TMovingSourceGIFForm.FormCreate(Sender: TObject);
begin
    inherited;

    gif := TGIFImage.create;
    gifForImg := TGIFImage.create;
    avi := TAviWriter_2.create(self);
    bmps := TObjectList<TBitmap>.create;
end;

procedure TMovingSourceGIFForm.FormDestroy(Sender: TObject);
begin
    gif.free;
    gifForImg.Free;
    avi.free;
    bmps.free;

    inherited;
end;

procedure TMovingSourceGIFForm.FormResize(Sender: TObject);
begin
    //can't use this, as form is resized on change of borderstyle
    //im.Picture.Assign(nil);
    //cancel := true;
end;

procedure TMovingSourceGIFForm.FormShow(Sender: TObject);
begin
    btGenerate.Enabled := container.scenario.resultsExist
        and (container.scenario.numberOfMovingSources = 1)
        and not container.scenario.movingSourcesUseGriddedTL;
    lbInvalid.Visible := not btGenerate.Enabled;
    pb.Visible := false;
    btCancel.Visible := false;
    im.Picture := nil; //clear previous gif
end;

end.
