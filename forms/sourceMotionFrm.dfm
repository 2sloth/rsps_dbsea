object SourceMotionForm: TSourceMotionForm
  Left = 0
  Top = 0
  Caption = 'Set source motion'
  ClientHeight = 614
  ClientWidth = 879
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sgMotion: TStringGrid
    Left = 0
    Top = 41
    Width = 879
    Height = 513
    Align = alClient
    ColCount = 8
    DefaultColWidth = 100
    TabOrder = 0
    OnDrawCell = sgMotionDrawCell
    OnMouseDown = sgMotionMouseDown
    OnSelectCell = sgMotionSelectCell
    OnSetEditText = sgMotionSetEditText
  end
  object pbBottom: TPanel
    Left = 0
    Top = 554
    Width = 879
    Height = 60
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      879
      60)
    object lblProblemValues: TLabel
      Left = 46
      Top = 6
      Width = 330
      Height = 17
      Anchors = [akLeft, akBottom]
      Caption = 'Caution : values shown in red will cause solution errors'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -14
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object lbTotalPositions: TLabel
      Left = 46
      Top = 28
      Width = 9
      Height = 13
      Caption = '   '
    end
    object btCancel: TButton
      Left = 790
      Top = 25
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object btImportFromFile: TButton
      Left = 377
      Top = 25
      Width = 169
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Import positions from file'
      TabOrder = 1
      Visible = False
      OnClick = btImportFromFileClick
    end
    object btOK: TButton
      Left = 709
      Top = 25
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 2
    end
    object btImportGPX: TButton
      Left = 552
      Top = 25
      Width = 143
      Height = 25
      Caption = 'Import GPX track'
      TabOrder = 3
      Visible = False
      OnClick = btImportGPXClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 879
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object rbSpeedms: TRadioButton
      Left = 329
      Top = 13
      Width = 113
      Height = 17
      Caption = 'Set speed (m/s)'
      TabOrder = 0
      OnClick = rbTimeClick
    end
    object rbTime: TRadioButton
      Left = 487
      Top = 13
      Width = 113
      Height = 17
      Caption = 'Set time (s)'
      TabOrder = 1
      OnClick = rbTimeClick
    end
    object rbSpeedkmh: TRadioButton
      Left = 46
      Top = 13
      Width = 113
      Height = 17
      Caption = 'Set speed (km/h)'
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = rbTimeClick
    end
    object rbSpeedKnot: TRadioButton
      Left = 189
      Top = 13
      Width = 113
      Height = 17
      Caption = 'Set speed (knot)'
      TabOrder = 3
      OnClick = rbTimeClick
    end
  end
  object odGPXTrack: TOpenDialog
    DefaultExt = 'gpx'
    Filter = 'Text files|*.txt|All files|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Choose input file'
    Left = 432
    Top = 312
  end
end
