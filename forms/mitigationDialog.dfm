object MitigationForm: TMitigationForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Mitigation'
  ClientHeight = 216
  ClientWidth = 361
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 164
    Height = 13
    Caption = 'Mitigation options: choose method'
  end
  object Label2: TLabel
    Left = 321
    Top = 67
    Width = 12
    Height = 13
    Caption = 'dB'
  end
  object edMitigation: TEdit
    Left = 239
    Top = 64
    Width = 57
    Height = 21
    TabOrder = 0
    Text = '5'
    OnChange = edMitigationChange
  end
  object udMitigation: TUpDown
    Left = 296
    Top = 64
    Width = 17
    Height = 21
    Max = 1000
    Position = 5
    TabOrder = 1
    Thousands = False
    OnClick = udMitigationClick
  end
  object btmitigationValue: TButton
    Left = 32
    Top = 62
    Width = 173
    Height = 25
    Caption = 'Insert constant value'
    Default = True
    ModalResult = 6
    TabOrder = 2
  end
  object btmitigationLibrary: TButton
    Left = 32
    Top = 112
    Width = 173
    Height = 25
    Caption = 'Lookup data from library'
    ModalResult = 7
    TabOrder = 3
  end
  object btmitigationCancel: TButton
    Left = 239
    Top = 168
    Width = 74
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
