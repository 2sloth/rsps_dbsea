object ImageOverlayAnchorForm: TImageOverlayAnchorForm
  Left = 0
  Top = 0
  Caption = 'Image overlay'
  ClientHeight = 810
  ClientWidth = 755
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    755
    810)
  PixelsPerInch = 96
  TextHeight = 13
  object im: TImage
    Left = 24
    Top = 16
    Width = 700
    Height = 700
    Anchors = [akLeft, akTop, akRight, akBottom]
    OnMouseDown = imMouseDown
  end
  object lbx1: TLabel
    Left = 24
    Top = 751
    Width = 53
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'West x (m)'
    ExplicitTop = 732
  end
  object lbx2: TLabel
    Left = 24
    Top = 778
    Width = 49
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'East x (m)'
    ExplicitTop = 759
  end
  object lby1: TLabel
    Left = 190
    Top = 751
    Width = 55
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'North y (m)'
    ExplicitTop = 732
  end
  object lby2: TLabel
    Left = 190
    Top = 780
    Width = 56
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'South y (m)'
    ExplicitTop = 761
  end
  object btLoad: TButton
    Left = 572
    Top = 746
    Width = 156
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Load image'
    TabOrder = 0
    OnClick = btLoadClick
  end
  object btOK: TButton
    Left = 572
    Top = 777
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 1
    OnClick = btOKClick
  end
  object btCancel: TButton
    Left = 653
    Top = 777
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object edWest: TEdit
    Left = 95
    Top = 748
    Width = 73
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 3
    OnChange = edXYChange
  end
  object edSouth: TEdit
    Left = 260
    Top = 775
    Width = 73
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 6
    OnChange = edXYChange
  end
  object edEast: TEdit
    Left = 95
    Top = 775
    Width = 73
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 4
    OnChange = edXYChange
  end
  object edNorth: TEdit
    Left = 260
    Top = 748
    Width = 73
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 5
    OnChange = edXYChange
  end
  object btClearImage: TButton
    Left = 404
    Top = 746
    Width = 156
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Clear image'
    TabOrder = 7
    OnClick = btClearImageClick
  end
  object btResetScale: TButton
    Left = 404
    Top = 777
    Width = 156
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Reset scale'
    TabOrder = 8
    OnClick = btResetScaleClick
  end
  object cbAspectRatioLock: TCheckBox
    Left = 24
    Top = 722
    Width = 457
    Height = 17
    Caption = 'Lock aspect ratio on map drag'
    TabOrder = 9
    OnClick = cbAspectRatioLockClick
  end
  object odImage: TOpenDialog
    Filter = 'JPG files|*.jpg'
    FilterIndex = 0
    Left = 392
    Top = 368
  end
  object tmAnimation: TTimer
    Enabled = False
    Interval = 400
    OnTimer = tmAnimationTimer
    Left = 304
    Top = 312
  end
end
