{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit sourceSpectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Vcl.Buttons,
  Dialogs, Grids, ExtCtrls, Chart, StdCtrls, SQLiteTable3,
  jpeg, MCKLib, Series, ComCtrls, math, Clipbrd, DBSeaColours, baseTypes,
  spectrum, helperFunctions, soundSource, equipReturn, sourceDBData,
  levelConverter, types, VCLTee.TeEngine, system.UITypes, VclTee.TeeGDIPlus,
  VCLTee.TeeProcs, problemContainer, Vcl.Menus, StringGridHelper,
  SourceWeightedLevelsFormUnit;

type
  TsourceSpectrumForm = class(TForm)
    chSrcSpect: TChart;
    sgEquip: TStringGrid;
    btSrcSpectOK: TButton;
    btSrcSpectCancel: TButton;
    srSrcSpect: TBarSeries;
    lbTotLevel: TLabel;
    btAddMitigation: TButton;
    btAdd1: TButton;
    btAdd2: TButton;
    btAdd3: TButton;
    btAdd4: TButton;
    btRemove1: TButton;
    btRemove2: TButton;
    btRemove3: TButton;
    btRemove4: TButton;
    btRemoveMitigation: TButton;
    Series2: TFastLineSeries;
    Series3: TFastLineSeries;
    Series4: TFastLineSeries;
    Series5: TFastLineSeries;
    Series6: TFastLineSeries;
    btDataToClipboard: TButton;
    bt200dBAll: TButton;
    edAssessmentPeriod: TEdit;
    rgLevelType: TRadioGroup;
    lbAssessmentPeriod: TLabel;
    btSetAssessmentPeriodTo: TButton;
    lbCrestFactor: TLabel;
    edCrestFactor: TEdit;
    puAssessmentPeriod: TPopupMenu;
    mi1Hour: TMenuItem;
    mi24Hours: TMenuItem;
    pnAddButtons: TPanel;
    pnRemoveButtons: TPanel;
    mi1Second: TMenuItem;
    btPlus: TSpeedButton;
    btMinus: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    btWeightedLevels: TButton;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure setSrc(aSrc: TSource);

    procedure btDataToClipboardClick(Sender: TObject);
    procedure sgEquipClick(Sender: TObject);
    procedure btAddEquipClick(Sender: TObject);
    procedure btRmEquipClick(Sender: TObject);

    procedure sgEquipSetEditText(Sender: TObject; ACol, ARow: Integer; const Value: string);
    procedure sgEquipSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgEquipDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);

    procedure bt200dBAllClick(Sender: TObject);
    procedure btSrcSpectOKClick(Sender: TObject);
    procedure edAssessmentPeriodChange(Sender: TObject);
    procedure rgLevelTypeClick(Sender: TObject);
    procedure bt1HourClick(Sender: TObject);
    procedure bt24HourClick(Sender: TObject);
    procedure btSetAssessmentPeriodToClick(Sender: TObject);
    procedure mi1SecondClick(Sender: TObject);
    procedure btPlusClick(Sender: TObject);
    procedure btMinusClick(Sender: TObject);
    procedure btWeightedLevelsClick(Sender: TObject);

  private
  const
    iNameCol        : integer = 0;
    iCountCol       : integer = 1;
    iDutyCol        : integer = 2;
    iLevCol         : integer = 3;
    //iCrestFactorCol : integer = 4;
    fiOffsetCol     : integer = 4;

    iEquipRows      : integer = 4;
    iMitigationRow  : integer = 5;
    iTotalsRow      : integer = 6;
  var
    src             : TSource; //maintain a reference to the source that this form is for
    DBData          : TSourceDBData;
    //levType         : TLevelType;

    function  sumLevelsByFreq(const fi : integer; const aBW : TBandwidth) : double;
    procedure updateStringGridAndGraph;
    procedure updateTotalLevel;
    procedure updateGraph;
    procedure updateStringGrid;
    procedure updateDBDataFromStringGrid;
    function  anyRowActive : boolean;
  public
    function  displayLevelType : TSourceLevelType;

    class function getFiOffset : integer; //make it a class method so that soundSource can access this without creating the form
  end;

  var
    sourceSpectrumForm: TsourceSpectrumForm;

implementation

{$R *.dfm}

uses Main, equipDBFrm, mitigationDialog;

procedure TsourceSpectrumForm.FormCreate(Sender: TObject);
begin
    sgEquip.ColCount := container.scenario.lenfi + fiOffsetCol;

    //levType := kSPL;
    DBData := TSourceDBData.Create;

    bt200dBAll.Visible := {$IFDEF DEBUG}true; {$ELSE} false; {$ENDIF}

    {$IF defined(DEBUG) or defined(VER2)}
        btWeightedLevels.Visible := true;
    {$ELSE}
        btWeightedLevels.Visible := false;
    {$ENDIF}
end;


procedure TsourceSpectrumForm.FormDestroy(Sender: TObject);
begin
    DBData.Free;
end;

procedure TsourceSpectrumForm.setSrc(aSrc : TSource);
begin
    self.Caption := 'Edit source spectrum: ' + aSrc.Name;

    self.src := aSrc;
    //levType := src.srcSpectrumLevelType;
    if not isnan(src.displayAssessmentPeriod) then
        self.edAssessmentPeriod.Text := tostr(src.displayAssessmentPeriod);

    DBData.cloneFrom(src.srcDBData);
    //for i := 0 to length(src.equipmentReturns) - 1 do
    //    equipmentReturns[i].cloneFrom(src.equipmentReturns[i]);
end;

procedure TsourceSpectrumForm.FormShow(Sender: TObject);
begin
    updateStringGridAndGraph;
end;

procedure TsourceSpectrumForm.sgEquipClick(Sender: TObject);
begin
    //
end;

procedure TsourceSpectrumForm.sgEquipSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
    {disallow editing on certain fields.}
    if (Arow = iTotalsRow) or
       (ACol = iLevCol) or
       ((ARow = iMitigationRow) and ((ACol = iCountCol) or (ACol = iDutyCol))) then
        sgEquip.Options := sgEquip.Options - [goEditing] + [goTabs]
    else
        sgEquip.Options := sgEquip.Options + [goEditing, goTabs, goAlwaysShowEditor];
end;

procedure TsourceSpectrumForm.sgEquipSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
    {checking on cell input to number cells - is it a number?
    (NB no negatives can be typed in, only pasted in.)}
    if not ((ACol = iNameCol) ) then
    begin
        if isFloat(Value) then
        begin
            if sgEquip.Cells[iCountCol,Arow] = '' then
                sgEquip.Cells[iCountCol,Arow] := '1';
            if sgEquip.Cells[iDutyCol,Arow] = '' then
                sgEquip.Cells[iDutyCol,Arow] := '100.0';

            updateDBDataFromStringGrid;
            //don't update stringgrid, as that will clear it
            updateTotalLevel;
            updateGraph;
        end
        else
            sgEquip.Cells[ACol,ARow] := '';  //clear cell if not a number inserted
    end;
end;


procedure TsourceSpectrumForm.btRmEquipClick(Sender: TObject);
var
    rowID : integer;
begin
    rowID := TButton(sender).tag;
    if rowID = iMitigationRow then
    begin
        sgEquip.Cells[iNameCol,rowID] := 'Mitigation'; {put back mitigation text if deleted}
        DBData.equipmentReturns[rowID - 1].name := 'Mitigation';
    end;

    DBData.equipmentReturns[rowID - 1].clear;

    updateStringGridAndGraph;
end;

procedure TsourceSpectrumForm.btSetAssessmentPeriodToClick(Sender: TObject);
var
    pnt: TPoint;
begin
    if GetCursorPos(pnt) then
        TButton(sender).PopupMenu.Popup(pnt.X, pnt.Y);
end;

procedure TsourceSpectrumForm.btSrcSpectOKClick(Sender: TObject);
begin
    if not assigned(src) then exit;

    if isfloat(edCrestFactor.Text) then
        src.srcSpectrumCrestFactor := tofl(edCrestFactor.Text);

    if isfloat(edAssessmentPeriod.Text) then
        src.displayAssessmentPeriod := tofl(edAssessmentPeriod.Text);

    updateDBDataFromStringGrid;
    DBData.updateTotalLev;
    src.srcDBData.cloneFrom(DBData);
    src.delegate.clearLevelsCache;
    //src.srcSpectrumLevelType := levType;
end;

procedure TsourceSpectrumForm.btWeightedLevelsClick(Sender: TObject);
begin
    updateDBDataFromStringGrid;
    DBData.updateTotalLev;
    SourceWeightedLevelsForm.sourceSpectrum.cloneFrom(DBData.totalEquipReturn.spectrum);
    SourceWeightedLevelsForm.startfi := src.delegate.getStartfi;
    SourceWeightedLevelsForm.lenfi := src.delegate.getLenfi;
    SourceWeightedLevelsForm.bw := src.delegate.getBandwidth;
    SourceWeightedLevelsForm.ShowModal;
end;

procedure TsourceSpectrumForm.edAssessmentPeriodChange(Sender: TObject);
begin
    if isFloat(edAssessmentPeriod.Text) and (tofl(edAssessmentPeriod.Text) > 0) then
        updateStringGridAndGraph;
end;

function  TsourceSpectrumForm.sumLevelsByFreq(const fi : integer; const aBW : TBandwidth) : double;
var
    iRow : integer;
    thisLev : double;
begin
    result := NaN;

    {sum over all the equipmentReturns. also multiply by the count for each piece of equip (=log10(count))
    NB it has to be the string values in the cells, as we can be picking up what the user typed}
    for iRow := 1 to iEquipRows do
    begin
        thisLev := DBData.equipmentReturns[iRow - 1].getAmpCountDutyByInd(fi,aBW);

        //add lev to total for this freq
        if not isnan(thisLev) then
            if isNaN(result) then
                result := thisLev
            else
                result := dbAdd(result,thisLev);
    end;

    //mitigation doesnt use count or duty
    thisLev := DBData.mitigationEquipReturn.spectrum.getMitigationAmpByInd(fi,aBW);
    if not isnan(thisLev) then
        result := result + thisLev;
end;

procedure TsourceSpectrumForm.mi1SecondClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '1';
end;

procedure TsourceSpectrumForm.bt1HourClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '3600';
end;

procedure TsourceSpectrumForm.bt24HourClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '86400';
end;

procedure TsourceSpectrumForm.bt200dBAllClick(Sender: TObject);
var
    i : integer;
    rowID : integer;
begin
    {this is just here to speed up development testing. not shown in the Release build}
    rowID := 1;
    for i := 0 to container.scenario.lenfi - 1 do
        sgEquip.Cells[fiOffsetCol + i,rowID] := '200';
    sgEquip.Cells[iCountCol,rowID] := '1';
    sgEquip.Cells[iDutyCol,rowID] := '100';

    updateStringGridAndGraph;
end;

procedure TsourceSpectrumForm.btAddEquipClick(Sender: TObject);
var
    fi                   : integer;
    rowID               : integer;
    modalreturn         : integer;
begin
    rowID := TButton(sender).tag;

    {first check if it is mitigation, in which case show the mitigationDialog. then, if we
    need to keep going to the fequipdb form then do so, otherwise exit}
    if rowID = iMitigationRow then
    begin
        modalreturn := mitigationForm.ShowModal;
        if modalreturn = mrYes then
        begin
            {inserting constant value, as given in the form.}
            DBData.mitigationEquipReturn.duty := 1;
            DBData.mitigationEquipReturn.count := 1;
            for fi := 0 to TSpectrum.maxLength - 1 do
                DBData.mitigationEquipReturn.spectrum.setMitigationAmpbyInd(fi,
                                                                            -mitigationForm.mitigation,
                                                                            bwThirdOctave);

            sgEquip.Cells[iNameCol,rowID] := 'Constant';
            //sgEquip.Cells[iCategoryCol,rowID] := 'Mitigation';
            sgEquip.Cells[iCountCol,rowID] := 'Mitigation';
            sgEquip.Cells[iDutyCol,rowID] := '';
            sgEquip.Cells[iLevCol,rowID] := '';
            //sgEquip.Cells[iCrestFactorCol,rowID] := '';
            DBData.updateTotalLev;
            updateStringGridAndGraph;
        end;
        if modalreturn <> mrNO then
            Exit; {2 of the 3 mr returns cause us to exit and go no further, only for mrNO do we now want to carry on and show the DB form}
    end;

    {if a particular piece of equip has been selected before for this row,
    then send this variable so we can reselect it}
    equipDBForm.equipReturn.cloneFrom(DBData.equipmentReturns[rowID - 1]);
    equipDBForm.LevType := src.displayLevelType;
    if not isnan(src.displayAssessmentPeriod) then
        equipDBForm.edAssessmentPeriod.Text := tostr(src.displayAssessmentPeriod);

    {if the mitigation line was clicked, let the equipDBForm know by
    setting the mitigation boolean}
    equipDBForm.wantMitigation := rowID = iMitigationRow;

    if equipDBForm.ShowModal = mrOK then
    begin
        DBData.equipmentReturns[rowID - 1].cloneFrom(equipDBForm.equipReturn);
        src.displayLevelType := equipDBForm.levType;
        if isfloat(equipDBForm.edAssessmentPeriod.Text) then
        begin
            src.displayAssessmentPeriod := tofl(equipDBForm.edAssessmentPeriod.Text);
            self.edAssessmentPeriod.Text := equipDBForm.edAssessmentPeriod.Text;
        end;

        DBData.updateTotalLev;
        updateStringGridAndGraph;

        //TODO change button to say 'change equip' and then back to 'add equip' if equip is removed
    end;
end;

procedure TsourceSpectrumForm.updateStringGridAndGraph;
begin
    if not assigned(self.src) then exit;

    updateStringGrid;
    updateTotalLevel;
    updateGraph;

    case src.displayLevelType of
        kSL: rgLevelType.ItemIndex := 0;
        kSWL: rgLevelType.ItemIndex := 1;
        kSELfreqSrc: rgLevelType.ItemIndex := 2;
    end;

    if isnan(src.srcSpectrumCrestFactor) then
        edCrestFactor.Text := '12'
    else
        edCrestFactor.Text := tostr(src.srcSpectrumCrestFactor);
end;

procedure TsourceSpectrumForm.updateTotalLevel;
//this method updates the level boxes, and also gets and new data the user has typed into the cells
//and saves it into self.equipReturns
var
    fi, startfi,endfi,iRow : integer;
    totTotLev : double;
    d : double;
    bw     : TBandwidth;
begin
    if not assigned(self.src) then exit;

    bw := container.scenario.bandwidth;
    startfi := container.scenario.startfi;
    endfi := container.scenario.endfi;

    sgEquip.cells[iNameCol,iTotalsRow] := 'Total';

    //get total levels for each freq
    for fi := startfi to endfi do
    begin
        d := sumLevelsByFreq(fi,bw);
        sgEquip.cells[fi - startfi + fiOffsetCol,iTotalsRow] := tostrWithTestAndRound1(TLevelConverter.convertSourceLevel(d, kSL, self.src.displayLevelType, tryTofl(edAssessmentPeriod.Text)));
    end;

    {get the total level for each individual equipment row}
    for iRow := 1 to iEquipRows do
    begin
        d := DBData.equipmentReturns[iRow - 1].level(startfi,endfi,bw);
        sgEquip.Cells[iLevCol,iRow] := tostrWithTestAndRound1(TLevelConverter.convertSourceLevel(d, kSL, src.displayLevelType, tryTofl(edAssessmentPeriod.Text)));
    end;

    //get total level between startfi - endfi
    totTotLev := DBData.totalEquipReturn.spectrum.Level(startfi,endfi,bw);
    if (not anyRowActive) or isNaN(totTotLev) then
    begin
        sgEquip.Cells[iLevCol,iTotalsRow] := '';
        lbTotLevel.Caption := '';
    end
    else
    begin
        sgEquip.Cells[iLevCol,iTotalsRow] := tostrWithTestAndRound1( TLevelConverter.convertSourceLevel(totTotLev, kSL, src.displayLevelType, tryTofl(edAssessmentPeriod.Text)) );
        lbTotLevel.Caption := 'Total :   ' + tostrWithTestAndRound1( TLevelConverter.convertSourceLevel(totTotLev, kSL, src.displayLevelType, tryTofl(edAssessmentPeriod.Text)) ) + ' dB';
    end;
end;

procedure TsourceSpectrumForm.updateGraph;
const
    belowMin : double = 30;
var
    fi, startfi, lenfi  : integer;
    bw                  : TBandwidth;
    maxLev              : double;
    minLev              : double;
    levels              : TDoubleDynArray;
    fStr                : String;
begin
    if not assigned(self.src) then exit;

    lenfi := container.scenario.lenfi;
    bw := container.scenario.bandwidth;
    startfi := container.scenario.startfi;

    srSrcSpect.Clear;
    srSrcSpect.ColorEachPoint := true;
    maxLev := nan;
    minLev := nan;
    chSrcSpect.LeftAxis.Automatic := true;
    {plot the total level}
    setlength(levels,lenfi);
    for fi := 0 to lenfi - 1 do
    begin
        levels[fi] := DBData.totalEquipReturn.spectrum.getAmpbyInd(fi + startfi,bw);
        levels[fi] := TLevelConverter.convertSourceLevel(levels[fi], kSL, src.displayLevelType, tryTofl(edAssessmentPeriod.Text));
        if not isNaN(levels[fi]) then
            lesserGreater( minLev, maxLev, levels[fi] );
    end;
    for fi := 0 to lenfi - 1 do
    begin
        {bar charts can't handle NaN, unlike line charts. so escape NaNs but setting a 0.
        Note that we can't just skip that index, as otherwise the chart will make
        a wide bar that keeps going till we hit the next data value.}
        fStr := TSpectrum.getFStringbyInd(fi + startfi,bw);
        if isNaN(levels[fi]) then
            srSrcSpect.AddXY(fi,0,fStr,TDBSeaColours.darkBlue) //ColorWithLevel(Lev,minLev,maxLev))
        else
            srSrcSpect.AddXY(fi,levels[fi],fStr,TDBSeaColours.darkBlue); //ColorWithLevel(Lev,minLev,maxLev));
    end;

    {setting the vertical axis}
    if not(isnan(maxLev) or isnan(minLev)) then
    begin
        chSrcSpect.LeftAxis.Automatic := false;
        chSrcSpect.LeftAxis.SetMinMax(floorx(minLev - belowMin,10),ceilx(maxLev,10));
    end;


        //    {plot the levels for each of the 5 equip components}
//    for Li := 1 to 5 do
//        if rowActive[Li] then
//        begin
//            offset := -0.6;   //this is some weird behaviour of the charting thing
//            chrtSrcSpect.Series[Li].Visible := true;
//            chrtSrcSpect.Series[Li].clear;
//            chrtSrcSpect.Series[Li].AddXY(0,0);
//            for fi := 0 to len - 1 do
//                if sgEquip.Cells[fi+fiOffset,Li] <> '' then
//                begin
//                    chrtSrcSpect.Series[Li].AddXY(fi+offset,tofl(sgEquip.Cells[fi+fiOffset,Li]),prob.analysisSpectrum.getFStringbyInd(fi));
//                    chrtSrcSpect.Series[Li].AddXY(fi+offset+1,tofl(sgEquip.Cells[fi+fiOffset,Li]),prob.analysisSpectrum.getFStringbyInd(fi));
//                end;
//            chrtSrcSpect.Series[Li].AddXY(fi+offset+1,0);
//        end
//        else
//            chrtSrcSpect.Series[Li].Visible := false; //turn it off if this line is not used
end;

procedure TSourceSpectrumForm.updateStringGrid;
var
    i, fi   : integer;
    bw      : TBandwidth;
    d       : double;
begin
    sgEquip.clear;

    //header row
    bw := container.scenario.bandwidth;
    sgEquip.colCount := container.scenario.lenfi + fiOffsetCol;
    sgEquip.Cells[iNameCol,0] := 'Name';
    sgEquip.ColWidths[iNameCol] := 100;   //make the 'name' col wider
    sgEquip.Cells[iCountCol,0] := 'Count';
    sgEquip.Cells[iDutyCol,0] := 'Duty%';
    sgEquip.Cells[iLevCol,0] := 'Level';
    //sgEquip.Cells[iCrestFactorCol,0] := 'Crest factor (dB)';


    //mitigation in name column
    sgEquip.Cells[iNameCol,iMitigationRow] := 'Mitigation';

    //frequency header row
    for fi := 0 to container.scenario.lenfi - 1 do
        sgEquip.cells[fi+fiOffsetCol,0] := TSpectrum.getFStringbyInd(fi + container.scenario.startfi,bw);

    //
    for i := 0 to (iEquipRows + 1) - 1 do //don't do totals row
    begin
        if DBData.isActive(i) then //.equipmentReturns[i].isActive then
        begin
            if i < iEquipRows then
            begin
                //we only fill these cells for equipment - mitigation row ignores duty and count
                sgEquip.Cells[iNameCol,i + 1]   := DBData.equipmentReturns[i].name;
                sgEquip.Cells[iCountCol,i + 1]  := tostr(DBData.equipmentReturns[i].count);
                sgEquip.Cells[iDutyCol,i + 1]   := tostrWithTestAndRound1(DBData.equipmentReturns[i].duty * 100.0);
                d := DBData.equipmentReturns[i].spectrum.level(container.scenario.startfi,
                                                               container.scenario.endfi,
                                                               container.scenario.Bandwidth);
                sgEquip.Cells[iLevCol,i + 1]    := tostrWithTestAndRound1( TLevelConverter.convertSourceLevel(d, kSL, src.displayLevelType, tryTofl(edAssessmentPeriod.Text)) );
                //sgEquip.Cells[iCrestFactorCol,i + 1]    := tostr(DBData.equipmentReturns[i].crestFactordB);
            end;

            for fi := 0 to container.scenario.lenfi do
            begin
                if DBData.equipmentReturns[i] = DBData.mitigationEquipReturn then
                begin
                    d := DBData.equipmentReturns[i].spectrum.getMitigationAmpbyInd(fi + container.scenario.startfi, container.scenario.bandwidth);
                    sgEquip.Cells[fi + fiOffsetCol,i + 1] := tostrWithTestAndRound1( -d ) //no conversion when it is mitigation
                end
                else
                begin
                    d := DBData.equipmentReturns[i].spectrum.getAmpbyInd(fi + container.scenario.startfi, container.scenario.bandwidth);
                    sgEquip.Cells[fi + fiOffsetCol,i + 1] := tostrWithTestAndRound1( TLevelConverter.convertSourceLevel(d, kSL, src.displayLevelType, tryTofl(edAssessmentPeriod.Text)) );
                end;
            end;
        end;
    end;
end;

procedure TsourceSpectrumForm.updateDBDataFromStringGrid;
var
    fi, startfi,iRow,iSgCol : integer;
    bw : TBandwidth;
    d : double;
begin
    bw := container.scenario.bandwidth;
    startfi := container.scenario.startfi;

    //store the name, count and duty for each equipment row
    for iRow := 1 to iEquipRows do
    begin
        DBData.equipmentReturns[iRow - 1].name := sgEquip.cells[iNameCol,iRow];
        if isInt(sgEquip.cells[iCountCol,iRow]) then
            DBData.equipmentReturns[iRow - 1].count := toint(sgEquip.cells[iCountCol,iRow])
        else
            DBData.equipmentReturns[iRow - 1].count := 1;
        if isFloat(sgEquip.cells[iDutyCol ,iRow]) then
            DBData.equipmentReturns[iRow - 1].duty := tofl(sgEquip.cells[iDutyCol,iRow]) / 100.0 //do conversion from percent here
        else
            DBData.equipmentReturns[iRow - 1].duty := 1;
    end;

    for fi := 0 to container.scenario.lenfi - 1 do
    begin
        iSgCol := fi + fiOffsetCol; //the column of the stringgrid for this freq

        //get the vales in the cells for each equipment
        for iRow := 1 to iEquipRows do
        begin
            //convert level to kSL and store in equipReturn
            d := tryTofl(sgEquip.cells[iSgCol,iRow]);
            DBData.equipmentReturns[iRow - 1].spectrum.setAmpByInd(fi + startfi,
                                                                   TLevelConverter.convertSourceLevel(d, src.displayLevelType, kSL, tryTofl(edAssessmentPeriod.Text)),
                                                                   bw);
        end; //rows

        {the mitigation row}
        //NB no level type conversion
        DBData.mitigationEquipReturn.spectrum.setMitigationAmpByInd(fi + startfi,
                                                                    -tryTofl(sgEquip.cells[iSgCol,iMitigationRow]), {nb negative}
                                                                    bw);
    end; //fi

    DBData.updateTotalLev;
end;

function  TsourceSpectrumForm.anyRowActive : boolean;
{check all equip rows, see if any are active}
var
    iRow : integer;
begin
    result := false;
    for iRow := 1 to iEquipRows do
        if DBData.equipmentReturns[iRow - 1].isActive then
            result := true;
end;

procedure TsourceSpectrumForm.sgEquipDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
    {change the font/colour for certain cells in the grid}
    with TStringGrid(Sender) do
    begin
        {header row with background}
        if (ARow = 0) then
            Canvas.Brush.Color := TDBSeaColours.headerRow;

        {mitigation row in mid blue}
        if (ARow = iMitigationRow) then
            Canvas.Font.Color := TDBSeaColours.midBlue;

        {levels column with background (but not header row)}
        if (ACol = iLevCol) and (ARow > 0) then
            Canvas.Brush.Color := TDBSeaColours.levelsColumn;

        {'total' row in bold}
        if (ARow = iTotalsRow) then
            Canvas.Font.Style := Canvas.Font.Style + [fsBold];

        {$IFDEF VER280}
        if DefaultDrawing then
            Rect.Left := Rect.Left - 4;
        Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+3, Cells[ACol, ARow]);
        {$ELSE}
        Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
        {$ENDIF}
    end;
end;

procedure TsourceSpectrumForm.btDataToClipboardClick(Sender: TObject);
begin
    Clipboard.AsText := sgEquip.myToString;
end;


procedure TsourceSpectrumForm.btMinusClick(Sender: TObject);
var
    rowID: integer;
    col: integer;
begin
    rowID := TButton(sender).tag;

    for col := fiOffsetCol to fiOffsetCol + container.scenario.lenfi - 1 do
        if isFloat(sgEquip.Cells[col,rowID]) then
            sgEquip.Cells[col,rowID] := floattostr(tofl(sgEquip.Cells[col,rowID]) - 1.0);

    updateDBDataFromStringGrid;
    //don't update stringgrid, as that will clear it
    updateTotalLevel;
    updateGraph;
end;

procedure TsourceSpectrumForm.btPlusClick(Sender: TObject);
var
    rowID: integer;
    col: integer;
begin
    rowID := TButton(sender).tag;

    for col := fiOffsetCol to fiOffsetCol + container.scenario.lenfi - 1 do
        if isFloat(sgEquip.Cells[col,rowID]) then
            sgEquip.Cells[col,rowID] := floattostr(tofl(sgEquip.Cells[col,rowID]) + 1.0);

    updateDBDataFromStringGrid;
    //don't update stringgrid, as that will clear it
    updateTotalLevel;
    updateGraph;
end;

class function TsourceSpectrumForm.getFiOffset : integer;
begin
    result := self.fiOffsetCol;
end;

function  TsourceSpectrumForm.displayLevelType : TSourceLevelType;
begin
    if assigned(src) then
        result := src.displayLevelType
    else
        result := kSL;
end;

procedure TsourceSpectrumForm.rgLevelTypeClick(Sender: TObject);
begin
    case rgLevelType.ItemIndex of
        0 : src.displayLevelType := kSL;
        1 : src.displayLevelType := kSWL;
        2 : src.displayLevelType := kSELfreqSrc;
    end;
    updateStringGridAndGraph;
end;

end.
