unit releaseNotes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  helperfunctions, dbseaconstants;

type
  TReleaseNotesForm = class(TForm)
    mmNotes: TMemo;
    btOK: TButton;
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
    ReleaseNotesForm: TReleaseNotesForm;

implementation

{$R *.dfm}

procedure TReleaseNotesForm.FormShow(Sender: TObject);
begin
    mmNotes.Text := THelper.readFileToString(ReleaseNotesFile);
end;

end.
