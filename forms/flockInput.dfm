object flockInputForm: TflockInputForm
  Left = 0
  Top = 0
  Caption = 'Flock input parameters'
  ClientHeight = 374
  ClientWidth = 433
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    433
    374)
  PixelsPerInch = 96
  TextHeight = 13
  object lbNAnimats: TLabel
    Left = 60
    Top = 48
    Width = 90
    Height = 13
    Caption = 'Number of animats'
  end
  object lbSteps: TLabel
    Left = 60
    Top = 160
    Width = 122
    Height = 13
    Caption = 'Total assessment time (s)'
  end
  object lbStepTime: TLabel
    Left = 60
    Top = 120
    Width = 61
    Height = 13
    Caption = 'Step time (s)'
  end
  object btOK: TButton
    Left = 190
    Top = 324
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    TabOrder = 0
    OnClick = btOKClick
    ExplicitTop = 376
  end
  object btCancel: TButton
    Left = 286
    Top = 324
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    ExplicitTop = 376
  end
  object edNAnimats: TEdit
    Left = 240
    Top = 45
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '300'
  end
  object edThreshold: TEdit
    Left = 240
    Top = 81
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '150.0'
  end
  object edTotalTime: TEdit
    Left = 240
    Top = 157
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '600.0'
  end
  object edGLUpdate: TEdit
    Left = 240
    Top = 193
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '60'
  end
  object edStepTime: TEdit
    Left = 240
    Top = 117
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '1.0'
  end
  object cbGLUpdate: TCheckBox
    Left = 46
    Top = 193
    Width = 169
    Height = 17
    Caption = 'Update graphics each n steps'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object cbThreshold: TCheckBox
    Left = 46
    Top = 83
    Width = 169
    Height = 17
    Caption = 'Threshold level (dB)'
    Checked = True
    State = cbChecked
    TabOrder = 8
  end
  object cbParallel: TCheckBox
    Left = 46
    Top = 232
    Width = 97
    Height = 17
    Caption = 'Run in parallel'
    Checked = True
    State = cbChecked
    TabOrder = 9
  end
  object cbSimpleFleeing: TCheckBox
    Left = 46
    Top = 272
    Width = 136
    Height = 17
    Caption = 'Simple fleeing model'
    Checked = True
    State = cbChecked
    TabOrder = 10
  end
end
