object BatchForm: TBatchForm
  Left = 0
  Top = 0
  Caption = 'Batch solve'
  ClientHeight = 461
  ClientWidth = 620
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    620
    461)
  PixelsPerInch = 96
  TextHeight = 13
  object lbProgress: TLabel
    Left = 19
    Top = 399
    Width = 30
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '          '
  end
  object btSolve: TButton
    Left = 437
    Top = 420
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Solve list'
    TabOrder = 0
    OnClick = btSolveClick
    ExplicitLeft = 304
    ExplicitTop = 341
  end
  object btDone: TButton
    Left = 525
    Top = 420
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Done'
    ModalResult = 1
    TabOrder = 1
    ExplicitLeft = 392
    ExplicitTop = 341
  end
  object btClear: TButton
    Left = 351
    Top = 420
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Clear list'
    TabOrder = 2
    OnClick = btClearClick
    ExplicitLeft = 218
    ExplicitTop = 341
  end
  object btOpen: TButton
    Left = 208
    Top = 420
    Width = 129
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Add files to list'
    TabOrder = 3
    OnClick = btOpenClick
  end
  object mmFiles: TMemo
    Left = 19
    Top = 16
    Width = 581
    Height = 377
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 4
  end
  object odBatch: TOpenDialog
    DefaultExt = 'uwa'
    Filter = 'UWA files|*.uwa|All files|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 56
    Top = 64
  end
end
