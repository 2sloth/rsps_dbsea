object sourceSpectrumForm: TsourceSpectrumForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Edit source spectrum'
  ClientHeight = 589
  ClientWidth = 1048
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    1048
    589)
  PixelsPerInch = 96
  TextHeight = 13
  object lbTotLevel: TLabel
    Left = 24
    Top = 202
    Width = 66
    Height = 16
    Anchors = [akLeft, akBottom]
    Caption = 'Total level'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbAssessmentPeriod: TLabel
    Left = 750
    Top = 377
    Width = 158
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Assessment period for source (s)'
  end
  object lbCrestFactor: TLabel
    Left = 771
    Top = 445
    Width = 81
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Crest factor (dB)'
  end
  object btPlus: TSpeedButton
    Tag = 1
    Left = 710
    Top = 30
    Width = 21
    Height = 21
    Caption = '+'
    OnClick = btPlusClick
  end
  object btMinus: TSpeedButton
    Tag = 1
    Left = 730
    Top = 30
    Width = 21
    Height = 21
    Caption = '-'
    OnClick = btMinusClick
  end
  object SpeedButton1: TSpeedButton
    Tag = 2
    Left = 710
    Top = 51
    Width = 21
    Height = 21
    Caption = '+'
    OnClick = btPlusClick
  end
  object SpeedButton2: TSpeedButton
    Tag = 2
    Left = 730
    Top = 51
    Width = 21
    Height = 21
    Caption = '-'
    OnClick = btMinusClick
  end
  object SpeedButton3: TSpeedButton
    Tag = 3
    Left = 710
    Top = 72
    Width = 21
    Height = 21
    Caption = '+'
    OnClick = btPlusClick
  end
  object SpeedButton4: TSpeedButton
    Tag = 3
    Left = 730
    Top = 72
    Width = 21
    Height = 21
    Caption = '-'
    OnClick = btMinusClick
  end
  object SpeedButton5: TSpeedButton
    Tag = 4
    Left = 710
    Top = 93
    Width = 21
    Height = 21
    Caption = '+'
    OnClick = btPlusClick
  end
  object SpeedButton6: TSpeedButton
    Tag = 4
    Left = 730
    Top = 93
    Width = 21
    Height = 21
    Caption = '-'
    OnClick = btMinusClick
  end
  object SpeedButton7: TSpeedButton
    Tag = 5
    Left = 710
    Top = 114
    Width = 21
    Height = 21
    Caption = '+'
    OnClick = btPlusClick
  end
  object SpeedButton8: TSpeedButton
    Tag = 5
    Left = 730
    Top = 114
    Width = 21
    Height = 21
    Caption = '-'
    OnClick = btMinusClick
  end
  object chSrcSpect: TChart
    Left = 8
    Top = 255
    Width = 697
    Height = 310
    Legend.Visible = False
    MarginBottom = 0
    MarginTop = 0
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Grid.Visible = False
    BottomAxis.Title.Caption = 'f (Hz)'
    DepthAxis.Automatic = False
    DepthAxis.AutomaticMaximum = False
    DepthAxis.AutomaticMinimum = False
    DepthAxis.Maximum = 0.500000000000000000
    DepthAxis.Minimum = -0.500000000000000000
    DepthTopAxis.Automatic = False
    DepthTopAxis.AutomaticMaximum = False
    DepthTopAxis.AutomaticMinimum = False
    DepthTopAxis.Maximum = 0.500000000000000000
    DepthTopAxis.Minimum = -0.500000000000000000
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Grid.Visible = False
    LeftAxis.Title.Caption = 'Level (dB)'
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    View3D = False
    Zoom.Allow = False
    BevelOuter = bvNone
    TabOrder = 0
    Anchors = [akLeft, akRight, akBottom]
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object srSrcSpect: TBarSeries
      Marks.Visible = False
      Marks.Callout.Length = 8
      SeriesColor = 9199360
      BarWidthPercent = 100
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Bar'
      YValues.Order = loNone
    end
    object Series2: TFastLineSeries
      LinePen.Color = clGreen
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series3: TFastLineSeries
      LinePen.Color = clYellow
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series4: TFastLineSeries
      LinePen.Color = clBlue
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series5: TFastLineSeries
      LinePen.Color = clWhite
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series6: TFastLineSeries
      LinePen.Color = clGray
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object sgEquip: TStringGrid
    Left = 8
    Top = 8
    Width = 697
    Height = 177
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 30
    DefaultColWidth = 40
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 7
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 1
    OnClick = sgEquipClick
    OnDrawCell = sgEquipDrawCell
    OnSelectCell = sgEquipSelectCell
    OnSetEditText = sgEquipSetEditText
  end
  object btSrcSpectOK: TButton
    Left = 823
    Top = 540
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = btSrcSpectOKClick
  end
  object btSrcSpectCancel: TButton
    Left = 929
    Top = 540
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object btDataToClipboard: TButton
    Left = 602
    Top = 199
    Width = 103
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Copy to clipboard'
    TabOrder = 4
    OnClick = btDataToClipboardClick
  end
  object bt200dBAll: TButton
    Left = 719
    Top = 540
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '200 dB all'
    TabOrder = 5
    Visible = False
    OnClick = bt200dBAllClick
  end
  object edAssessmentPeriod: TEdit
    Left = 929
    Top = 374
    Width = 100
    Height = 21
    Anchors = [akRight, akBottom]
    TabOrder = 6
    Text = '3600'
    OnChange = edAssessmentPeriodChange
  end
  object rgLevelType: TRadioGroup
    Left = 719
    Top = 255
    Width = 310
    Height = 101
    Anchors = [akRight, akBottom]
    Caption = 'Level type'
    ItemIndex = 0
    Items.Strings = (
      'Source level    dB SL @ 1m'
      'Source power level    dB SWL Lw'
      'Sound exposure level    dB SEL @ T0 = 1s')
    TabOrder = 7
    OnClick = rgLevelTypeClick
  end
  object btSetAssessmentPeriodTo: TButton
    Left = 929
    Top = 398
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Set to...'
    PopupMenu = puAssessmentPeriod
    TabOrder = 8
    OnClick = btSetAssessmentPeriodToClick
  end
  object edCrestFactor: TEdit
    Left = 929
    Top = 442
    Width = 100
    Height = 21
    Anchors = [akRight, akBottom]
    TabOrder = 9
    Text = '0'
    TextHint = 'Enter crest factor (Peak > RMS)'
  end
  object pnAddButtons: TPanel
    Left = 750
    Top = 30
    Width = 145
    Height = 109
    Anchors = [akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 10
    object btAdd1: TButton
      Tag = 1
      Left = 0
      Top = 0
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Add equipment'
      TabOrder = 0
      OnClick = btAddEquipClick
    end
    object btAdd2: TButton
      Tag = 2
      Left = 0
      Top = 21
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Add equipment'
      TabOrder = 1
      OnClick = btAddEquipClick
    end
    object btAdd3: TButton
      Tag = 3
      Left = 0
      Top = 42
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Add equipment'
      TabOrder = 2
      OnClick = btAddEquipClick
    end
    object btAdd4: TButton
      Tag = 4
      Left = 0
      Top = 63
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Add equipment'
      TabOrder = 3
      OnClick = btAddEquipClick
    end
    object btAddMitigation: TButton
      Tag = 5
      Left = 0
      Top = 84
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Add mitigation'
      TabOrder = 4
      OnClick = btAddEquipClick
    end
  end
  object pnRemoveButtons: TPanel
    Left = 895
    Top = 30
    Width = 145
    Height = 109
    Anchors = [akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 11
    object btRemove1: TButton
      Tag = 1
      Left = 0
      Top = 0
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Remove equipment'
      TabOrder = 0
      OnClick = btRmEquipClick
    end
    object btRemove2: TButton
      Tag = 2
      Left = 0
      Top = 21
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Remove equipment'
      TabOrder = 1
      OnClick = btRmEquipClick
    end
    object btRemove3: TButton
      Tag = 4
      Left = 0
      Top = 63
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Remove equipment'
      TabOrder = 2
      OnClick = btRmEquipClick
    end
    object btRemove4: TButton
      Tag = 3
      Left = 0
      Top = 42
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Remove equipment'
      TabOrder = 3
      OnClick = btRmEquipClick
    end
    object btRemoveMitigation: TButton
      Tag = 5
      Left = 0
      Top = 84
      Width = 145
      Height = 21
      Align = alTop
      Caption = 'Remove mitigation'
      TabOrder = 4
      OnClick = btRmEquipClick
    end
  end
  object btWeightedLevels: TButton
    Left = 929
    Top = 484
    Width = 100
    Height = 25
    Caption = 'Weighted levels'
    TabOrder = 12
    OnClick = btWeightedLevelsClick
  end
  object puAssessmentPeriod: TPopupMenu
    Left = 888
    Top = 200
    object mi1Second: TMenuItem
      Caption = '1 second'
      OnClick = mi1SecondClick
    end
    object mi1Hour: TMenuItem
      Caption = '1 hour'
      OnClick = bt1HourClick
    end
    object mi24Hours: TMenuItem
      Caption = '24 hours'
      OnClick = bt24HourClick
    end
  end
end
