unit scenarioSummary;

interface

uses
  winapi.Windows, winapi.Messages,
  system.SysUtils, system.Variants, system.Classes,
  vcl.Graphics, vcl.Controls, vcl.Forms,
  vcl.clipbrd,
  vcl.Dialogs, vcl.Grids, vcl.StdCtrls,
  StringGridHelper;

type
  TfrmScenarioSummary = class(TForm)
    btOK: TButton;
    sgSummary: TStringGrid;
    btToclipboard: TButton;
    procedure btToclipboardClick(Sender: TObject);
  private
  public
  end;

var
  frmScenarioSummary: TfrmScenarioSummary;

implementation

{$R *.dfm}

procedure TfrmScenarioSummary.btToclipboardClick(Sender: TObject);
begin
    Clipboard.AsText := sgSummary.myToString;
end;

end.
