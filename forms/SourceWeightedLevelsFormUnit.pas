unit SourceWeightedLevelsFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  system.math,
  system.generics.collections,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.StdCtrls,
  soundSource, StringGridHelper, SQLiteTable3, globalsUnit, dbSeaConstants,
  fishCurve, SmartPointer, helperFunctions, spectrum, baseTypes;

type
  TSourceWeightedLevelsForm = class(TForm)
    btOK: TButton;
    sg: TStringGrid;
    btCopyToClipboard: TButton;
    lbTitle: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure btCopyToClipboardClick(Sender: TObject);
  private
    curves: TObjectList<TFishCurve>;
  public
    sourceSpectrum: TSpectrum;
    startfi, lenfi: integer;
    bw: TBandwidth;
  end;

var
  SourceWeightedLevelsForm: TSourceWeightedLevelsForm;

implementation

{$R *.dfm}

procedure TSourceWeightedLevelsForm.btCopyToClipboardClick(Sender: TObject);
begin
    THelper.setClipboard(sg.myToString);
end;

procedure TSourceWeightedLevelsForm.FormCreate(Sender: TObject);
begin
    if not MDASldb.TableExists(fishTblName) then
        raise Exception.Create(rsErrMDANoTable + fishTblName);
    if not UserSldb.TableExists(fishTblName) then
        raise Exception.Create(rsUserNoTable + fishTblName);

    curves := TObjectList<TFishCurve>.create(true);
    sourceSpectrum := TSpectrum.create;
end;

procedure TSourceWeightedLevelsForm.FormDestroy(Sender: TObject);
begin
    curves.free;
    sourceSpectrum.free;
end;

procedure TSourceWeightedLevelsForm.FormShow(Sender: TObject);
var
    p: TPair<integer, TFishCurve>;
    i: integer;
    d: double;
    sltbMDA, sltbUser: ISmart<TSQLiteTable>;
begin
    curves.clear;
    sltbMDA := TSmart<TSQLiteTable>.create(MDASldb.GetTable(ansistring('SELECT * FROM ' + fishTblName + ' ORDER BY ID')));
    sltbUser := TSmart<TSQLiteTable>.create(userSlDb.GetTable(ansistring('SELECT * FROM ' + fishTblName + ' ORDER BY ID')));
    TFishCurve.addEntries(sltbMDA, curves);
    TFishCurve.addEntries(sltbUser, curves);

    self.sg.clear;

    self.sg.RowCount := curves.Count + 1;
    self.sg.ColCount := lenfi + 1;

    for I := 0 to lenfi - 1 do
        sg.Cells[i + 1, 0] := TSpectrum.getFStringbyInd(startfi + i, bw);

    for p in THelper.iterate<TFishCurve>(curves) do
    begin
        sg.cells[0,p.Key + 1] := p.Value.name.Replace(#13#10, '').replace(#10, '');
        for I := 0 to lenfi - 1 do
        begin
            d := sourceSpectrum.getAmpbyInd(i + startFi, bw) - p.Value.spectrum.getMitigationAmpbyInd(i + startFi, bw);
            if isnan(d) then
                sg.Cells[i + 1, p.Key + 1] := '-'
            else
                sg.Cells[i + 1, p.Key + 1] := FloatToStrF(d, ffFixed, 6, 1);
        end;
    end;
end;

end.
