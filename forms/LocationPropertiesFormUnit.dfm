object LocationPropertiesForm: TLocationPropertiesForm
  Left = 0
  Top = 0
  Caption = 'Properties map'
  ClientHeight = 670
  ClientWidth = 982
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnHide = FormHide
  OnShow = FormShow
  DesignSize = (
    982
    670)
  PixelsPerInch = 96
  TextHeight = 13
  object im: TImage
    Left = 16
    Top = 16
    Width = 598
    Height = 600
    Anchors = [akLeft, akTop, akRight, akBottom]
    OnMouseDown = imMouseDown
    OnMouseMove = imMouseMove
    ExplicitWidth = 600
  end
  object lbMousePos: TLabel
    Left = 16
    Top = 622
    Width = 56
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'lbMousePos'
  end
  object lbEasting: TLabel
    Left = 636
    Top = 413
    Width = 35
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Easting'
  end
  object lbNorthing: TLabel
    Left = 636
    Top = 443
    Width = 41
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Northing'
  end
  object lbName: TLabel
    Left = 636
    Top = 210
    Width = 27
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Name'
  end
  object lbComments: TLabel
    Left = 636
    Top = 269
    Width = 50
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Comments'
  end
  object btCancel: TButton
    Left = 899
    Top = 637
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
  end
  object btOK: TButton
    Left = 811
    Top = 637
    Width = 82
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btOKClick
  end
  object btAdd: TButton
    Left = 636
    Top = 167
    Width = 100
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Add'
    TabOrder = 2
    OnClick = btAddClick
  end
  object btDuplicate: TButton
    Left = 757
    Top = 167
    Width = 100
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Duplicate'
    TabOrder = 3
    OnClick = btDuplicateClick
  end
  object btRemove: TButton
    Left = 871
    Top = 167
    Width = 100
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Remove'
    TabOrder = 4
    OnClick = btRemoveClick
  end
  object edName: TEdit
    Left = 636
    Top = 229
    Width = 329
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 5
    Text = 'edName'
    OnChange = edNameChange
  end
  object mmComments: TMemo
    Left = 636
    Top = 288
    Width = 329
    Height = 73
    Anchors = [akTop, akRight]
    Lines.Strings = (
      'mmComments')
    TabOrder = 6
    OnChange = mmCommentsChange
  end
  object edEasting: TEdit
    Left = 728
    Top = 410
    Width = 122
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 7
    Text = 'edEasting'
    OnChange = edEastingChange
  end
  object edNorthing: TEdit
    Left = 728
    Top = 440
    Width = 122
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 8
    Text = 'edNorthing'
    OnChange = edNorthingChange
  end
  object lbItems: TListBox
    Left = 636
    Top = 16
    Width = 333
    Height = 139
    Style = lbOwnerDrawFixed
    Anchors = [akTop, akRight]
    ItemHeight = 13
    TabOrder = 9
    OnClick = lbItemsClick
    OnDrawItem = lbItemsDrawItem
    OnKeyUp = lbItemsKeyUp
  end
  object pb: TProgressBar
    Left = 227
    Top = 288
    Width = 150
    Height = 17
    Step = 1
    TabOrder = 10
  end
  object btShowInfluence: TButton
    Left = 486
    Top = 637
    Width = 128
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Show influence'
    TabOrder = 11
    OnClick = btShowInfluenceClick
  end
  object btColour: TButton
    Left = 636
    Top = 374
    Width = 100
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Colour'
    TabOrder = 12
    OnClick = btColourClick
  end
  object btJSONImport: TButton
    Left = 622
    Top = 637
    Width = 185
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Import JSON'
    TabOrder = 13
    OnClick = btJSONImportClick
  end
  object odJSON: TOpenDialog
    Left = 488
    Top = 344
  end
  object tmAnimation: TTimer
    Enabled = False
    Interval = 100
    OnTimer = tmAnimationTimer
    Left = 536
    Top = 392
  end
end
