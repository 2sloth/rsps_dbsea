unit flockInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  helperFunctions;

type
  TflockInputForm = class(TForm)
    btOK: TButton;
    btCancel: TButton;
    lbNAnimats: TLabel;
    lbSteps: TLabel;
    edNAnimats: TEdit;
    edThreshold: TEdit;
    edTotalTime: TEdit;
    edGLUpdate: TEdit;
    lbStepTime: TLabel;
    edStepTime: TEdit;
    cbGLUpdate: TCheckBox;
    cbThreshold: TCheckBox;
    cbParallel: TCheckBox;
    cbSimpleFleeing: TCheckBox;
    procedure btOKClick(Sender: TObject);
  private
  public
  end;

var
  flockInputForm: TflockInputForm;

implementation

{$R *.dfm}

procedure TflockInputForm.btOKClick(Sender: TObject);
begin
    if not (isint(edNAnimats.Text) and
            isfloat(edThreshold.Text) and
            isfloat(edTotalTime.Text) and
            isint(edGLUpdate.Text) and
            isfloat(edStepTime.Text)) then
    begin
       showmessage('Unable to parse values, check entries and retry');
       exit;
    end;

    ModalResult := mrOK;
end;

end.
