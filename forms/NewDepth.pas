unit NewDepth;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, helperFunctions;

type
  TNewDepthForm = class(TForm)
    btOK: TButton;
    btCancel: TButton;
    edDepth: TEdit;
    Label1: TLabel;
    procedure btOKClick(Sender: TObject);
  private
  public
  end;

var
  NewDepthForm: TNewDepthForm;

implementation

{$R *.dfm}

procedure TNewDepthForm.btOKClick(Sender: TObject);
begin
    if (not isFloat(edDepth.Text)) or (tofl(edDepth.Text) <= 0) then
        showmessage('Enter a valid positive value to proceed')
    else
        self.ModalResult := mrOK;
end;

end.
