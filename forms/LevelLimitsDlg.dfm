object LevelLimitsDialog: TLevelLimitsDialog
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Set level limits'
  ClientHeight = 378
  ClientWidth = 1045
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblMaxLevel: TLabel
    Left = 24
    Top = 49
    Width = 68
    Height = 13
    Caption = 'Max level (dB)'
  end
  object lnlMinLevel: TLabel
    Left = 24
    Top = 99
    Width = 64
    Height = 13
    Caption = 'Min level (dB)'
  end
  object edMax: TEdit
    Left = 122
    Top = 46
    Width = 70
    Height = 21
    NumbersOnly = True
    TabOrder = 0
    Text = '7'
    OnChange = levelsChanged
  end
  object edMin: TEdit
    Left = 122
    Top = 96
    Width = 70
    Height = 21
    NumbersOnly = True
    TabOrder = 1
    Text = '8'
    OnChange = levelsChanged
  end
  object btOK: TButton
    Left = 800
    Top = 329
    Width = 100
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = btOKClick
  end
  object btCancel: TButton
    Left = 917
    Top = 329
    Width = 100
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object udMax: TUpDown
    Left = 192
    Top = 45
    Width = 17
    Height = 23
    Min = -1000
    Max = 1000
    TabOrder = 4
    Thousands = False
    OnClick = udMaxClick
  end
  object udMin: TUpDown
    Left = 192
    Top = 95
    Width = 17
    Height = 23
    Min = -1000
    Max = 1000
    TabOrder = 6
    Thousands = False
    OnClick = udMinClick
  end
  object btReset: TButton
    Left = 24
    Top = 329
    Width = 100
    Height = 25
    Caption = 'Reset'
    TabOrder = 5
    OnClick = btResetClick
  end
  object chLevelHistogram: TChart
    Left = 552
    Top = 46
    Width = 465
    Height = 255
    Legend.Visible = False
    MarginBottom = 0
    MarginTop = 0
    Title.Font.Color = clBlack
    Title.Text.Strings = (
      'Global levels histogram')
    BottomAxis.Title.Caption = 'Level (dB)'
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Title.Caption = 'Count'
    View3D = False
    Zoom.Allow = False
    BevelOuter = bvNone
    TabOrder = 7
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object lbGatheringLevels: TLabel
      Left = 160
      Top = 121
      Width = 192
      Height = 13
      Caption = 'Gathering levels for histogram display...'
    end
    object pbGatheringLevels: TProgressBar
      Left = 176
      Top = 160
      Width = 150
      Height = 17
      TabOrder = 0
    end
    object srActiveHistogram: TBarSeries
      Marks.Visible = False
      BarWidthPercent = 100
      MultiBar = mbNone
      Shadow.Color = 8618883
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Bar'
      YValues.Order = loNone
    end
    object srInactiveHistogram: TBarSeries
      Marks.Visible = False
      SeriesColor = clSilver
      BarWidthPercent = 100
      MultiBar = mbNone
      Shadow.Color = 8750469
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Bar'
      YValues.Order = loNone
    end
  end
  object rgBandSpacing: TRadioGroup
    Left = 24
    Top = 167
    Width = 185
    Height = 137
    Caption = 'Spacing between bands'
    Items.Strings = (
      '1 dB'
      '3 dB'
      '5 dB'
      '10 dB'
      '20 dB')
    TabOrder = 8
    OnClick = rgBandSpacingClick
  end
  object pnExclusionZone: TPanel
    Left = 224
    Top = 46
    Width = 313
    Height = 256
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 9
    object pbExclusionZoneColor: TPaintBox
      Left = 216
      Top = 201
      Width = 81
      Height = 25
    end
    object lbExclusionZone: TLabel
      Left = 16
      Top = 87
      Width = 118
      Height = 13
      Caption = 'Exclusion zone level (dB)'
    end
    object cbExclusionZone: TCheckBox
      Left = 16
      Top = 29
      Width = 206
      Height = 17
      Caption = 'Show exclusion zone'
      TabOrder = 0
      OnClick = cbExclusionZoneClick
    end
    object btSetExclusionZoneColor: TButton
      Left = 16
      Top = 201
      Width = 185
      Height = 25
      Caption = 'Exclusion zone colour'
      TabOrder = 1
      OnClick = btSetExclusionZoneColorClick
    end
    object edExclusionZone: TEdit
      Left = 216
      Top = 84
      Width = 81
      Height = 21
      TabOrder = 2
      Text = '5'
      OnChange = edExclusionZoneChange
    end
    object cbThresholds: TComboBox
      Left = 16
      Top = 142
      Width = 281
      Height = 21
      DropDownCount = 100
      TabOrder = 3
      Text = 'cbThresholds'
      OnChange = cbThresholdsChange
    end
  end
  object ttPaintboxDraw: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ttPaintboxDrawTimer
    Left = 544
    Top = 320
  end
end
