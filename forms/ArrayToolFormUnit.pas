unit ArrayToolFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, system.Math,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  dbseacolours, helperfunctions,
  probe, soundSource, pos3, scenario;

type
  TArrayToolForm = class(TForm)
    rgObjectType: TRadioGroup;
    btOK: TButton;
    btCancel: TButton;
    edSpacing: TEdit;
    cbSourceObject: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    pbDirection: TPaintBox;
    edDirection: TEdit;
    lblDirection: TLabel;
    edNumberToCreate: TEdit;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);

    procedure updatePBDirection;
    procedure pbDirectionPaint(Sender: TObject);
    procedure pbDirectionMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edDirectionEnter(Sender: TObject);
    procedure edDirectionExit(Sender: TObject);
    procedure rgObjectTypeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btOKClick(Sender: TObject);
  private
    bAngleEditing : boolean;

    procedure ProbeSelectBoxInit;
    procedure SrcSelectBoxInit;
  public
    direction : double;
  end;

var
  ArrayToolForm: TArrayToolForm;

implementation

{$R *.dfm}

uses main, problemContainer;

procedure TArrayToolForm.btOKClick(Sender: TObject);
var
    i: integer;
    ang: double;
    spacing: double;
    src, newSrc: TSource;
    prb, newPrb: TProbe;
begin
    if (not isInt(edNumberToCreate.Text)) or (not isFloat(edSpacing.Text)) or (not isFloat(edDirection.Text)) then exit;

    ang := -(tofl(edDirection.Text) / 180 * PI - pi/2);
    spacing := tofl(edSpacing.Text);

    case self.rgObjectType.ItemIndex of
    0: begin
        src := container.scenario.sources[self.cbSourceObject.ItemIndex];
        for I := 1 to StrToInt(edNumberToCreate.Text) do
        begin
            newSrc := container.scenario.addSource();
            newSrc.cloneFrom(src);
            newSrc.pos.setPos(src.pos.x + i * spacing * cos(ang), src.pos.y + i * spacing * sin(ang), src.pos.z);
            newSrc.name := newSrc.name + ' ' + IntToStr(i);
        end;
    end;
    1: begin
        prb := container.scenario.theRA.probes[self.cbSourceObject.ItemIndex];
        for I := 1 to StrToInt(edNumberToCreate.Text) do
        begin
            newPrb := container.scenario.theRA.addProbe();
            newPrb.cloneFrom(prb);
            newPrb.pos.setPos(prb.pos.x + i * spacing * cos(ang), prb.pos.y + i * spacing * sin(ang), prb.pos.z);
            newPrb.name := newPrb.name + ' ' + IntToStr(i);
        end;
    end;
    end;
end;

procedure TArrayToolForm.edDirectionEnter(Sender: TObject);
begin
    bAngleEditing := true;
end;

procedure TArrayToolForm.edDirectionExit(Sender: TObject);
begin
    bAngleEditing := false;
end;

procedure TArrayToolForm.FormCreate(Sender: TObject);
begin
    bAngleEditing := false;
    direction := 0;
end;


procedure TArrayToolForm.FormShow(Sender: TObject);
begin
    self.rgObjectTypeClick(self);
end;

procedure TArrayToolForm.pbDirectionMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
    roundToDeg : double = 15;
var
    h,w : integer;
    cx,cy : integer;
    //ang : double;
    radDir : double;
    degDir : double;
begin
    h := pbDirection.Height;
    w := pbDirection.Width;
    cx := w div 2 + 1;
    cy := h div 2 + 1;

    radDir := arctan2(Y - cy,X - cx);
    {convert to degrees to round to 15 degrees, then convert back to radians to store it}
    degDir := safetrunc(roundx(radDir*180/pi,roundToDeg)); //get the angle rounded to 15 degrees
    radDir := degDir*pi/180;
    Direction := radDir;
    bAngleEditing := false;
    mainform.ActiveControl := nil;
    updatePBDirection;
end;

procedure TArrayToolForm.pbDirectionPaint(Sender: TObject);
begin
    self.updatePBDirection;
end;

procedure TArrayToolForm.rgObjectTypeClick(Sender: TObject);
begin
    case self.rgObjectType.ItemIndex of
    0: self.SrcSelectBoxInit;
    1: self.ProbeSelectBoxInit;
    end;
end;

procedure TArrayToolForm.ProbeSelectBoxInit;
var
    prb : TProbe;
begin
    if not assigned(container) then exit;

    self.cbSourceObject.items.clear();
    self.cbSourceObject.DoubleBuffered := true;
    self.cbSourceObject.AutoComplete := true;

    if container.scenario.theRA.probes.Count < 1 then
    begin
        self.cbSourceObject.ItemIndex := -1;
        self.cbSourceObject.Text := '';
    end
    else
    begin
        for prb in container.scenario.theRA.probes do
            self.cbSourceObject.AddItem(prb.Name, nil);
        self.cbSourceObject.ItemIndex := 0;
    end;
end;

procedure TArrayToolForm.SrcSelectBoxInit;
var
    src : TSource;
begin
    self.cbSourceObject.items.clear;
    self.cbSourceObject.DoubleBuffered := true;
    self.cbSourceObject.AutoComplete := true;
    if container.scenario.sources.Count < 1 then
    begin
        self.cbSourceObject.ItemIndex := -1;
        self.cbSourceObject.Text := '';
    end
    else
    begin
        for src in container.scenario.sources do
            self.cbSourceObject.AddItem(src.Name, nil);
        self.cbSourceObject.ItemIndex := 0;
    end;
end;

procedure TArrayToolForm.updatePBDirection;
const
    iStroke : integer = 30;
    iArrowLen : integer = 30;
    iArrowHead : integer = 5;
    iSmallTickLen : integer = 3;
    iMedTickLen : integer = 8;
    iLongTickLen : integer = 10;
var
    i,h,w : integer;
    cx,cy : integer;
    ang : double;
    //iStroke,iArrowLen,iArrowHead : integer;
begin
    ang := Direction;

    h := pbDirection.Height;
    w := pbDirection.Width;
    cx := w div 2 + 1;
    cy := h div 2 + 1;

    pbDirection.Canvas.Brush.Color := TDBSeaColours.directionFill;
    pbDirection.Canvas.FillRect(pbDirection.Canvas.ClipRect);

    pbDirection.Canvas.Pen.Color := clBlack;
    pbDirection.Canvas.Pen.Width := 1;
    {small 15 degree strokes}
    for i := 0 to 23 do
    begin
        pbDirection.Canvas.MoveTo(saferound(cx + iStroke*cos(i*pi/12)),
                                    saferound(cy + iStroke*sin(i*pi/12)));
        pbDirection.Canvas.LineTo(saferound(cx + (iStroke + iSmallTickLen)*cos(i*pi/12)),
                                    saferound(cy + (iStroke + iSmallTickLen)*sin(i*pi/12)));
    end;

    {med 45 degree strokes}
    for i := 0 to 7 do
    begin
        pbDirection.Canvas.MoveTo(saferound(cx + iStroke*cos(i*pi/4)),
                                    saferound(cy + iStroke*sin(i*pi/4)));
        pbDirection.Canvas.LineTo(saferound(cx + (iStroke + iMedTickLen)*cos(i*pi/4)),
                                    saferound(cy + (iStroke + iMedTickLen)*sin(i*pi/4)));
    end;

    {long 90 degree strokes}
    for i := 0 to 3 do
    begin
        pbDirection.Canvas.MoveTo(saferound(cx + iStroke*cos(i*pi/2)),
                                    saferound(cy + iStroke*sin(i*pi/2)));
        pbDirection.Canvas.LineTo(saferound(cx + (iStroke + iLongTickLen)*cos(i*pi/2)),
                                    saferound(cy + (iStroke + iLongTickLen)*sin(i*pi/2)));
    end;

    {draw arrow shaft}
    pbDirection.Canvas.Pen.Color := TDBSeaColours.directionArrow;
    pbDirection.Canvas.Pen.Width := 3;
    pbDirection.Canvas.MoveTo(cx,cy);
    pbDirection.Canvas.LineTo(cx + saferound(cos(ang)*iArrowLen),cy + saferound(sin(ang)*iArrowLen));

    {draw arrow head}
    pbDirection.Canvas.MoveTo(cx + saferound(cos(ang)*iArrowLen),cy + saferound(sin(ang)*iArrowLen));
    pbDirection.Canvas.LineTo(cx + saferound(cos(ang)*iArrowLen + cos(ang + 3*pi/4)*iArrowHead),
        cy + saferound(sin(ang)*iArrowLen + sin(ang + 3*pi/4)*iArrowHead));
    pbDirection.Canvas.MoveTo(cx + saferound(cos(ang)*iArrowLen),cy + saferound(sin(ang)*iArrowLen));
    pbDirection.Canvas.LineTo(cx + saferound(cos(ang)*iArrowLen + cos(ang + 5*pi/4)*iArrowHead),
        cy + saferound(sin(ang)*iArrowLen + sin(ang + 5*pi/4)*iArrowHead));

    {update the text in the box, but not if we are editing the angles right now}
    if not bAngleEditing then
    begin
        //lblDirection.Caption := 'Direction: ' + tostr(saferound(180*angleWrap(ang + pi/2)/pi)) + #176;
        edDirection.Text := tostr(saferound(180*angleWrap(pi/2 + ang)/pi));
        if edDirection.Text = '360' then
            edDirection.Text := '0';
    end;
end;


end.

