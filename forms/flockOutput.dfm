object flockOutputForm: TflockOutputForm
  Left = 0
  Top = 0
  Caption = 'Flock output'
  ClientHeight = 811
  ClientWidth = 984
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter3: TSplitter
    Left = 209
    Top = 0
    Height = 811
    OnPaint = Splitter2Paint
    ExplicitLeft = 208
    ExplicitTop = 328
    ExplicitHeight = 100
  end
  object pbLeft: TPanel
    Left = 0
    Top = 0
    Width = 209
    Height = 811
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object rgLevelType: TRadioGroup
      Left = 0
      Top = 0
      Width = 209
      Height = 81
      Align = alTop
      Caption = 'Level type'
      ItemIndex = 0
      Items.Strings = (
        'SEL'
        'SPLpeak')
      TabOrder = 0
      OnClick = rgLevelTypeClick
    end
    object rgBinSpacing: TRadioGroup
      Left = 0
      Top = 81
      Width = 209
      Height = 162
      Align = alTop
      Caption = 'Level bin spacing'
      ItemIndex = 0
      Items.Strings = (
        '1'
        '3'
        '5'
        '10'
        '20')
      TabOrder = 1
      OnClick = rgBinSpacingClick
    end
    object clSelect: TCheckListBox
      Left = 0
      Top = 260
      Width = 209
      Height = 551
      OnClickCheck = clSelectClickCheck
      Align = alClient
      ItemHeight = 13
      TabOrder = 2
    end
    object cbShowAll: TCheckBox
      Left = 0
      Top = 243
      Width = 209
      Height = 17
      Align = alTop
      Caption = 'Show all'
      TabOrder = 3
      OnClick = cbShowAllClick
    end
  end
  object pnRight: TPanel
    Left = 212
    Top = 0
    Width = 772
    Height = 811
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 0
      Top = 393
      Width = 772
      Height = 3
      Cursor = crVSplit
      Align = alTop
      OnPaint = Splitter2Paint
      ExplicitWidth = 418
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 772
      Height = 393
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 369
        Top = 0
        Height = 393
        OnPaint = Splitter2Paint
        ExplicitLeft = 584
        ExplicitTop = 240
        ExplicitHeight = 100
      end
      object chLevsHistogram: TChart
        Left = 0
        Top = 0
        Width = 369
        Height = 393
        Legend.Visible = False
        MarginBottom = 0
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Title.Caption = 'Level (dB)'
        LeftAxis.Title.Caption = 'Count'
        View3D = False
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object srLevsHistogram: TBarSeries
          Marks.Visible = False
          BarWidthPercent = 100
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
        end
      end
      object chLevsHistory: TChart
        Left = 372
        Top = 0
        Width = 400
        Height = 393
        Legend.Visible = False
        MarginBottom = 0
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Grid.Visible = False
        BottomAxis.Title.Caption = 'Time (s)'
        LeftAxis.Title.Caption = 'Level (dB)'
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object srLevsHistory: TPointSeries
          ClickableLine = False
          Pointer.HorizSize = 1
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 1
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object chSelectedAnimats: TChart
      Left = 0
      Top = 396
      Width = 772
      Height = 374
      Legend.Visible = False
      MarginBottom = 0
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      BottomAxis.Title.Caption = 'Time (s)'
      LeftAxis.Title.Caption = 'Level (dB)'
      View3D = False
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      DefaultCanvas = 'TGDIPlusCanvas'
      ColorPaletteIndex = 13
    end
    object pnButtons: TPanel
      Left = 0
      Top = 770
      Width = 772
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object btExport: TButton
        Tag = 1
        Left = 512
        Top = 6
        Width = 130
        Height = 25
        Caption = 'Export all to file'
        TabOrder = 0
        OnClick = btExportClick
      end
      object btExportSelectedToFile: TButton
        Left = 376
        Top = 6
        Width = 130
        Height = 25
        Caption = 'Export selected to file'
        TabOrder = 1
        OnClick = btExportClick
      end
      object btOK: TButton
        Left = 648
        Top = 6
        Width = 100
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 2
      end
    end
  end
  object sdExportFlock: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Text files|*.txt|All files|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Choose output data file'
    Left = 184
    Top = 48
  end
end
