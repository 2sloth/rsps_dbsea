object crossSecNameChangeFrm: TcrossSecNameChangeFrm
  Left = 0
  Top = 0
  Caption = 'Enter new name'
  ClientHeight = 112
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbName: TLabel
    Left = 28
    Top = 20
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object edName: TEdit
    Left = 76
    Top = 17
    Width = 264
    Height = 21
    TabOrder = 0
  end
  object btOK: TButton
    Left = 184
    Top = 64
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btCancel: TButton
    Left = 265
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
