object FishAddToDB: TFishAddToDB
  Left = 0
  Top = 0
  Caption = 'Insert data into user database'
  ClientHeight = 323
  ClientWidth = 984
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    984
    323)
  PixelsPerInch = 96
  TextHeight = 13
  object lblName: TLabel
    Left = 40
    Top = 125
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblComments: TLabel
    Left = 40
    Top = 159
    Width = 50
    Height = 13
    Caption = 'Comments'
  end
  object lblCategory: TLabel
    Left = 40
    Top = 287
    Width = 45
    Height = 13
    Caption = 'Category'
  end
  object sgWeighting: TStringGrid
    Left = 40
    Top = 27
    Width = 921
    Height = 78
    Anchors = [akLeft, akTop, akRight]
    RowCount = 2
    TabOrder = 0
    OnSelectCell = sgWeightingSelectCell
  end
  object btOK: TButton
    Left = 792
    Top = 276
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    ExplicitTop = 327
  end
  object btCancel: TButton
    Left = 886
    Top = 276
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    ExplicitTop = 327
  end
  object EditName: TEdit
    Left = 150
    Top = 122
    Width = 363
    Height = 21
    TabOrder = 3
  end
  object memoComments: TMemo
    Left = 40
    Top = 178
    Width = 473
    Height = 97
    Lines.Strings = (
      'memoComments')
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object EditCategory: TEdit
    Left = 150
    Top = 284
    Width = 363
    Height = 21
    TabOrder = 5
    Text = 'Other'
  end
end
