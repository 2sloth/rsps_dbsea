unit UTMConversionForm;

interface

uses
  System.SysUtils, System.Variants, System.Classes, system.StrUtils, system.math,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VCL.clipbrd,
  Winapi.Windows, Winapi.Messages, Vcl.StdCtrls, Vcl.Grids,
  stringGridHelper, helperFunctions, basetypes, latlong2UTM, dbseaoptions,
  Vcl.ExtCtrls;

type
  TUTMConversionFrm = class(TForm)
    sgLatLng: TStringGrid;
    sgEastingNorthing: TStringGrid;
    btOK: TButton;
    lbRefZone: TLabel;
    lbHint: TLabel;
    btFromClipboard: TButton;
    btToClipboard: TButton;
    lbArrows: TLabel;
    rgFormat: TRadioGroup;
    rbValidRow: TRadioButton;
    rbUserSetZone: TRadioButton;
    edUserSetZone: TEdit;

    procedure FormShow(Sender: TObject);

    procedure btToEastingNorthingClick(Sender: TObject);
    procedure sgLatLngSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgLatLngKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btToClipboardClick(Sender: TObject);
    procedure btFromClipboardClick(Sender: TObject);
    procedure sgLatLngKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rgFormatClick(Sender: TObject);
    procedure rbValidRowClick(Sender: TObject);
    procedure rbUserSetZoneClick(Sender: TObject);
    procedure edUserSetZoneChange(Sender: TObject);
    procedure btOKClick(Sender: TObject);
  private
  const
    decimalDegreesWidth = 524;
    degreesMinutesSecondsWidth = 844;

    function  anyRowValid(): boolean;
    function  rowValid(const row: integer): boolean;
    function  getFirstValidRow(): integer;
    function  getLatLong(const row: integer = -1): TEastNorth;
    function  getUTMZString(): string;
    function  getRefZone(): integer;
    procedure editBoxesInit();
  public
  end;

var
  UTMConversionFrm: TUTMConversionFrm;

implementation

{$R *.dfm}

function  TUTMConversionFrm.getRefZone(): integer;
var
    s: string;
begin
    if rbUserSetZone.Checked and TLatLong2UTM.isValidUTMZone(trim(edUserSetZone.Text)) then
    begin
        s := trim(edUserSetZone.Text)[1];
        if trim(edUserSetZone.Text).length = 3 then
            s := s + trim(edUserSetZone.Text)[2];
        if isint(s) then
            exit(strtoint(s));
    end;

    if not self.anyRowValid() then
        exit(-1);

    result := TLatLong2UTM.getLongZone(self.getLatLong.longitude);
end;

function  TUTMConversionFrm.getFirstValidRow(): integer;
begin
    result := 1;
    while not rowValid(result) do
    begin
        inc(result);
        if result = sgLatLng.RowCount then
            exit();
    end;
end;

function  TUTMConversionFrm.rowValid(const row: integer): boolean;
begin
    if row < 1 then exit(false);

    if rgFormat.ItemIndex = 1 then
        result := (isFloat(sgLatLng.cells[0,row]) and
                   isFloat(sgLatLng.cells[1,row]) and
                   isFloat(sgLatLng.cells[2,row]) and
                   isFloat(sgLatLng.cells[3,row]) and
                   isFloat(sgLatLng.cells[4,row]) and
                   isFloat(sgLatLng.cells[5,row]))
    else
        result := (isFloat(sgLatLng.cells[0,row]) and isFloat(sgLatLng.cells[1,row]));
end;

function  TUTMConversionFrm.getLatLong(const row: integer = -1): TEastNorth;
var
    aRow: integer;
    //latLng: TEastNorth;
begin
    if row <> -1 then
        aRow := row
    else
        aRow := self.getFirstValidRow;

    if not rowValid(aRow) then
        exit();

    if rgFormat.ItemIndex = 1 then
    begin
        result.latitude := tofl(sgLatLng.cells[0,aRow])
                         + tofl(sgLatLng.cells[1,aRow]) / 60
                         + tofl(sgLatLng.cells[2,aRow]) / 3600;
        result.longitude := tofl(sgLatLng.cells[3,aRow])
                          + tofl(sgLatLng.cells[4,aRow]) / 60
                          + tofl(sgLatLng.cells[5,aRow]) / 3600;
    end
    else
    begin
        result.latitude := tofl(sgLatLng.cells[0,aRow]);
        result.longitude := tofl(sgLatLng.cells[1,aRow]);
    end;
end;

procedure TUTMConversionFrm.sgLatLngKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (shift = [ssCtrl]) and (key = ord('V')) then
        sgLatLng.Cells[sgLatLng.col,sgLatLng.Row] := Clipboard.AsText;
end;

procedure TUTMConversionFrm.sgLatLngKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    self.btToEastingNorthingClick(sender);
    editBoxesInit;
end;

procedure TUTMConversionFrm.sgLatLngSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
    if not (sender is TStringGrid) then exit;

    if arow <> 0 then
        TStringGrid(sender).Options := TStringGrid(sender).Options + [goEditing];
end;

function TUTMConversionFrm.anyRowValid: boolean;
begin
    result := self.rowValid(self.getFirstValidRow);
end;

procedure TUTMConversionFrm.btFromClipboardClick(Sender: TObject);
    function formattingOK(const dM : TMatrix) : boolean;
    var
        i : integer;
    begin
        {check for correct number of columns}
        if length(dM) = 0 then
        begin
            showmessage('Unable to parse input data');
            Exit(false);
        end;

        if length(dM[0]) <> 2 then
        begin
            showmessage('Wrong number of input data columns');
            Exit(false);
        end;

        {check for correct formatting and range check of each field}
        for i := 0 to length(dM) - 1 do
            if isnan(dM[i,0]) or isnan(dM[i,1]) then
            begin
                showmessage('Unable to parse input at line ' + tostr(i));
                exit(false);
            end;

        result := true;
    end;
var
    dM: TMatrix;
    i: integer;
begin
    dm.fromClipboard;

    if not formattingOK(dM) then exit;

    {data is correctly formatted, we can put use it}
    for i := 0 to min(sgLatLng.RowCount - 1,length(dM)) - 1 do
    begin
        sgLatLng.Cells[0,i + 1] := tostr(dm[i,0]);
        sgLatLng.Cells[1,i + 1] := tostr(dm[i,1]);
    end;

    self.editBoxesInit;
    self.btToEastingNorthingClick(sender);
end;

procedure TUTMConversionFrm.btOKClick(Sender: TObject);
begin
    self.Hide;
end;

procedure TUTMConversionFrm.btToClipboardClick(Sender: TObject);
var
    zone: string;
begin
    if rbUserSetZone.Checked and TLatLong2UTM.isValidUTMZone(trim(edUserSetZone.Text)) then
        zone := UpperCase(trim(edUserSetZone.Text))
    else
        zone := self.getUTMZString;

    Clipboard.AsText := 'UTM zone ' + zone + #13#10 + sgEastingNorthing.myToString;
end;

procedure TUTMConversionFrm.btToEastingNorthingClick(Sender: TObject);
var
    row: integer;
    en: TEastNorth;
begin
    if not anyRowValid then exit;
    if self.getRefZone() = -1 then exit;

    for row := 1 to sgLatLng.RowCount - 1 do
    begin
        if rowValid(row) then
        begin
            en := TLatLong2UTM.latlong2UTM(self.getLatLong(row),self.getRefZone());
            sgEastingNorthing.Cells[0,row] := tostrWithTestAndRound1(m2f(en.easting , UWAOpts.dispFeet));
            sgEastingNorthing.Cells[1,row] := tostrWithTestAndRound1(m2f(en.northing, UWAOpts.dispFeet));
        end
        else
        begin
            sgEastingNorthing.Cells[0,row] := '';
            sgEastingNorthing.Cells[1,row] := '';
        end;
    end;
end;

procedure TUTMConversionFrm.FormShow(Sender: TObject);
begin
    sgLatLng.clear;
    sgEastingNorthing.clear;

    sgEastingNorthing.Cells[0,0] := 'Easting';
    sgEastingNorthing.Cells[1,0] := 'Northing';

    editBoxesInit;
end;

procedure TUTMConversionFrm.editBoxesInit();
begin
    if rgFormat.ItemIndex = 1 then
    begin
        self.Width := degreesMinutesSecondsWidth;
        sgLatLng.ColCount := 6;
        sgLatLng.Cells[0,0] := 'Latitiude � N';
        sgLatLng.Cells[1,0] := 'Minutes';
        sgLatLng.Cells[2,0] := 'Seconds';
        sgLatLng.Cells[3,0] := 'Longitude � E';
        sgLatLng.Cells[4,0] := 'Minutes';
        sgLatLng.Cells[5,0] := 'Seconds';
    end
    else
    begin
        self.Width := decimalDegreesWidth;
        sgLatLng.ColCount := 2;
        sgLatLng.Cells[0,0] := 'Latitiude � N';
        sgLatLng.Cells[1,0] := 'Longitude � E';
    end;

    if rbUserSetZone.Checked then
    begin
        if TLatLong2UTM.isValidUTMZone(trim(edUserSetZone.Text)) then
            self.lbRefZone.Caption := 'Reference zone: ' + UpperCase(trim(edUserSetZone.Text))
        else if self.getRefZone = -1 then
            self.lbRefZone.Caption := 'Invalid zone'
        else
            self.lbRefZone.Caption := 'Invalid zone. Using reference zone: ' + self.getUTMZString();
    end
    else if self.getRefZone = -1 then
        self.lbRefZone.Caption := ''
    else
        self.lbRefZone.Caption := 'Reference zone: ' + self.getUTMZString();
end;

procedure TUTMConversionFrm.edUserSetZoneChange(Sender: TObject);
begin
    editBoxesInit;
    btToEastingNorthingClick(nil);
end;

function TUTMConversionFrm.getUTMZString(): string;
begin
    if not anyRowValid then
        exit('?');

    result := TLatLong2UTM.getUTMString(self.getLatLong);
end;

procedure TUTMConversionFrm.rbUserSetZoneClick(Sender: TObject);
begin
    rbValidRow.Checked := false;
    editBoxesInit;
    btToEastingNorthingClick(nil);
end;

procedure TUTMConversionFrm.rbValidRowClick(Sender: TObject);
begin
    rbUserSetZone.Checked := false;
    editBoxesInit;
    btToEastingNorthingClick(nil);
end;

procedure TUTMConversionFrm.rgFormatClick(Sender: TObject);
begin
    sgLatLng.clear;
    editBoxesInit;
end;

end.
