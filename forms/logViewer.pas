unit logViewer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, helperfunctions, eventLogging;

type
  TEventLogViewerForm = class(TForm)
    mmLog: TMemo;
    btOK: TButton;
    btSaveLog: TButton;
    sdSaveLog: TSaveDialog;
    procedure btSaveLogClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
    procedure setText(const str : TStrings);
  end;

var
    eventLogViewerForm: TEventLogViewerForm;

implementation

{$R *.dfm}

procedure TeventLogViewerForm.btSaveLogClick(Sender: TObject);
begin
    if not sdSaveLog.Execute then exit;

    if FileExists(sdSaveLog.filename) and THelper.IsFileInUse(sdSaveLog.filename) then
    begin
        showmessage('File is in use by another process, operation cancelled');
        Exit;
    end;

    THelper.WriteStringToFile(sdSaveLog.filename,self.mmLog.Text);
end;

procedure TEventLogViewerForm.FormShow(Sender: TObject);
begin
    self.setText(eventLog);
end;

procedure TeventLogViewerForm.setText(const str : TStrings);
var
    s : String;
begin
    self.mmlog.lines.Clear;
    for s in str do
        self.mmLog.Lines.Add(s);
end;

end.
