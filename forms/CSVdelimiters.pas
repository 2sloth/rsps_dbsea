{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit CSVdelimiters;

interface

uses
    system.types, system.SysUtils, system.Variants, system.Classes, system.strutils,
  winapi.Windows, winapi.Messages,
  vcl.Graphics, vcl.Controls, vcl.Forms,
  vcl.Dialogs, vcl.StdCtrls, vcl.ExtCtrls,
  baseTypes, helperFunctions, mcklib,
   SmartPointer;

type
  TCSVDelimitersForm = class(TForm)
    rgDecimalSeparator: TRadioGroup;
    rgFieldDelimiter: TRadioGroup;
    btCSVOK: TButton;
    mmSampleText: TMemo;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    //procedure FormShow(Sender: TObject);
    procedure rgDecimalSeparatorClick(Sender: TObject);
    procedure rgFieldDelimiterClick(Sender: TObject);

    function  setFileName(const fileName : TFileName) : boolean;

  private
    { Private declarations }
    function  numberOfNonzeroEntries(const intArray : TIntegerDynArray) : integer;
  public
    { Public declarations }
    decimalPoint    : char;
    fieldDelimiter      : char;
  end;

var
  CSVDelimitersForm: TCSVDelimitersForm;

implementation

{$R *.dfm}

procedure TCSVDelimitersForm.FormCreate(Sender: TObject);
begin
    EnableMenuItem(GetSystemMenu(handle,False),SC_CLOSE,MF_BYCOMMAND or MF_GRAYED); //disable the window close button

    decimalPoint := formatSettings.decimalSeparator;  //from sysutils, determined by locale
end;


function TCSVDelimitersForm.setFileName(const fileName: TFileName): boolean;
const
    icolons: integer = 0;
    icommas: integer = 1;
    itabs: integer = 2;
    ispaces: integer = 3;
var
    i: integer;
    s: String;
    FileStream: ISmart<TFileStream>;
    StreamReader: ISmart<TStreamReader>;
    intarr: TIntegerDynArray;
    k: integer;
    localFormatSettings: TFormatSettings;
begin
    //returns whether this form needs to be shown, ie if it can't determine the correct delimiters
    //result := true;

    self.rgDecimalSeparatorClick(nil);
    self.rgFieldDelimiterClick(nil);

    setlength(intarr,4);
    for i := 0 to length(intarr) - 1 do
        intarr[i] := 0;
    {read a few lines from the file into the memo box so we can see how it looks.
    //NB TStreamReader and TFileStream are a good alternative to TStringlist
    //when we don't need to read the whole file, they work a lot faster}
    FileStream := TSmart<TFileStream>.create(TFileStream.Create(fileName,fmOpenRead));
    StreamReader := TSmart<TStreamReader>.create(TStreamReader.Create(FileStream));
    FileStream.Seek(0,soBeginning);
    s := ansileftstr(StreamReader.ReadLine,5);
    if ansicomparetext(s,'ncols') = 0 then
    begin
        result := false; //esri file, no need to try parse here
    end
    else
    begin
        FileStream.Seek(0,soBeginning);
        for i := 0 to 15 do  //just read a few lines in
            if not StreamReader.EndOfStream then
            begin
                {truncate the line as it could potentially be several thousand charachters long}
                s := ansileftstr(StreamReader.ReadLine,100);
                {keep a running count of each type of delimiter, and see which is greater - this is probably the correct one}
                intarr[icolons]  := intarr[icolons] + SubStringOccurences(';',s);
                intarr[icommas]  := intarr[icommas] + SubStringOccurences(',',s);
                intarr[itabs  ]  := intarr[itabs  ] + SubStringOccurences(#09,s);
                intarr[ispaces]  := intarr[ispaces] + SubStringOccurences(' ',s);
                mmSampleText.lines[i] := s;
            end;

        k := intarr.findExact(intarr.max);

        localFormatSettings := TFormatSettings.Create(LOCALE_USER_DEFAULT);
        if localFormatSettings.decimalSeparator = ',' then
        begin
            //for locales with comma as decimal separator, if there are multiple delimeters and one of those is comma, it is probably not that
            if (numberOfNonzeroEntries(intArr) > 1) and (intarr[icommas] > 0) then
            begin
                decimalPoint := ',';
                intarr[icommas] := 0;
                k := intarr.findExact(intarr.max);
            end;
        end;

        //if there is more than 1 delimiter and one of those is space, it is probably not space
        if (numberOfNonzeroEntries(intArr) > 1) and (intarr[ispaces] > 0) then
        begin
            intarr[ispaces] := 0;
            k := intarr.findExact(intarr.max);
        end;

        //if there is more than 1 delimiter and one of those is space, it is probably not space
        if (numberOfNonzeroEntries(intArr) > 1) and (intarr[itabs] > 0) then
        begin
            intarr[itabs] := 0;
            k := intarr.findExact(intarr.max);
        end;

        //return true if the form needs to show, because it can't determine what the delimiters are
        result := numberOfNonzeroEntries(intArr) <> 1;

        rgFieldDelimiter.ItemIndex := k;
        rgFieldDelimiterClick(nil);
    end;
end;


procedure TCSVDelimitersForm.rgDecimalSeparatorClick(Sender: TObject);
begin
    case rgDecimalSeparator.ItemIndex of
        0 : decimalPoint := '.';
        1 : decimalPoint := ',';
    end;
end;

procedure TCSVDelimitersForm.rgFieldDelimiterClick(Sender: TObject);
begin
    case rgFieldDelimiter.ItemIndex of
        0 : fieldDelimiter := ';';
        1 : fieldDelimiter := ',';
        2 : fieldDelimiter := #09;
        3 : fieldDelimiter := ' ';
    end;
end;

function TCSVDelimitersForm.numberOfNonzeroEntries(const intArray : TIntegerDynArray) : integer;
var
    i : integer;
begin
    result := 0;
    for i := 0 to length(intArray) - 1 do
        if intarray[i] <> 0 then
            inc(result);
end;

end.
