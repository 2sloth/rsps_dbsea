object sourceTimeSeriesForm: TsourceTimeSeriesForm
  Left = 0
  Top = 0
  Caption = 'Set source time series data'
  ClientHeight = 749
  ClientWidth = 1109
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    1109
    749)
  PixelsPerInch = 96
  TextHeight = 13
  object lbFS: TLabel
    Left = 330
    Top = 27
    Width = 80
    Height = 13
    Caption = 'Sample rate (Hz)'
  end
  object lbSEL: TLabel
    Left = 28
    Top = 605
    Width = 24
    Height = 13
    Caption = 'aaaa'
  end
  object lbSPLpk: TLabel
    Left = 268
    Top = 605
    Width = 24
    Height = 13
    Caption = 'aaaa'
  end
  object lbSplpkpk: TLabel
    Left = 496
    Top = 605
    Width = 24
    Height = 13
    Caption = 'aaaa'
  end
  object lbCount: TLabel
    Left = 580
    Top = 27
    Width = 390
    Height = 13
    Caption = 
      'Count (per assessment period for static source or per position f' +
      'or moving source)'
  end
  object lbScaleBy: TLabel
    Left = 20
    Top = 66
    Width = 85
    Height = 13
    Caption = 'Scale pressure by'
  end
  object chTimeSeries: TChart
    Left = 20
    Top = 92
    Width = 1061
    Height = 507
    Legend.Visible = False
    MarginBottom = 0
    MarginTop = 0
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Title.Caption = 't (s)'
    LeftAxis.Title.Caption = 'Pressure (Pa)'
    View3D = False
    Zoom.Allow = False
    BevelOuter = bvNone
    TabOrder = 0
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object srTimeSeries: TLineSeries
      SeriesColor = 10391090
      Brush.BackColor = clDefault
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object srDownsampled: TLineSeries
      SeriesColor = 4470529
      Brush.BackColor = clDefault
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object srUpsampleAgain: TLineSeries
      SeriesColor = 8388863
      Brush.BackColor = clDefault
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object edFS: TEdit
    Left = 424
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '44100'
    OnChange = edFSChange
  end
  object btOpen: TButton
    Left = 80
    Top = 22
    Width = 101
    Height = 25
    Caption = 'Open text file'
    TabOrder = 2
    OnClick = btOpenTextClick
  end
  object btOK: TButton
    Left = 925
    Top = 696
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 3
    OnClick = btOKClick
  end
  object btCancel: TButton
    Left = 1006
    Top = 696
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object sgLevels: TStringGrid
    Left = 20
    Top = 634
    Width = 881
    Height = 87
    Anchors = [akLeft, akRight, akBottom]
    RowCount = 3
    TabOrder = 5
    OnDrawCell = sgLevelsDrawCell
    OnSelectCell = sgLevelsSelectCell
  end
  object edCount: TEdit
    Left = 983
    Top = 24
    Width = 66
    Height = 21
    TabOrder = 6
    Text = '1'
    OnChange = edCountChange
  end
  object btOpenWAV: TButton
    Left = 204
    Top = 22
    Width = 101
    Height = 25
    Caption = 'Open WAV file'
    TabOrder = 7
    OnClick = btOpenWAVClick
  end
  object btScale: TButton
    Left = 268
    Top = 61
    Width = 85
    Height = 25
    Caption = 'Scale'
    TabOrder = 8
    OnClick = btScaleClick
  end
  object edScale: TEdit
    Left = 124
    Top = 63
    Width = 121
    Height = 21
    TabOrder = 9
    Text = '1.0'
  end
  object odTimeSeries: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'txt file|*.txt|All files|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Choose input file'
    Left = 648
    Top = 648
  end
  object odWAVFile: TOpenDialog
    DefaultExt = 'wav'
    Filter = 'wav file|*.wav|All files|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Choose input file'
    Left = 712
    Top = 648
  end
end
