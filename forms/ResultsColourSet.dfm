object ResultsColoursFrm: TResultsColoursFrm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Set Result Colours'
  ClientHeight = 420
  ClientWidth = 259
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    259
    420)
  PixelsPerInch = 96
  TextHeight = 13
  object sgColors: TStringGrid
    Left = 0
    Top = 0
    Width = 259
    Height = 316
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 3
    DefaultColWidth = 80
    FixedCols = 0
    TabOrder = 0
    OnDrawCell = sgColorsDrawCell
    OnMouseDown = sgColorsMouseDown
    ExplicitWidth = 249
  end
  object btRsltClrOK: TButton
    Left = 168
    Top = 339
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btRsltClrOKClick
    ExplicitLeft = 158
  end
  object btRsltClrCancel: TButton
    Left = 168
    Top = 379
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    ExplicitLeft = 158
  end
  object btReset: TButton
    Left = 20
    Top = 339
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Default set'
    TabOrder = 3
    OnClick = btResetClick
  end
  object btAlternateColors: TButton
    Left = 20
    Top = 379
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Alternate set'
    TabOrder = 4
    OnClick = btAlternateColorsClick
  end
end
