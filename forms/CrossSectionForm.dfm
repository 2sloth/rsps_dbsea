object editCrossSectionForm: TeditCrossSectionForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Setting cross section/transect'
  ClientHeight = 257
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    468
    257)
  PixelsPerInch = 96
  TextHeight = 13
  object btCancel: TButton
    Left = 359
    Top = 204
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
  end
  object pnPointA: TPanel
    Left = 24
    Top = 24
    Width = 409
    Height = 71
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 1
    object lblAx: TLabel
      Left = 15
      Top = 42
      Width = 35
      Height = 13
      Caption = 'Easting'
    end
    object lblAy: TLabel
      Left = 230
      Top = 42
      Width = 41
      Height = 13
      Caption = 'Northing'
    end
    object lbPointATitle: TLabel
      Left = 15
      Top = 15
      Width = 24
      Height = 13
      Caption = 'Start'
    end
    object edAx: TEdit
      Left = 87
      Top = 42
      Width = 96
      Height = 21
      TabOrder = 0
      Text = '0'
      OnMouseDown = edMouseDown
    end
    object edAy: TEdit
      Left = 297
      Top = 42
      Width = 96
      Height = 21
      TabOrder = 1
      Text = '0'
      OnMouseDown = edMouseDown
    end
  end
  object pnPointB: TPanel
    Left = 24
    Top = 109
    Width = 409
    Height = 70
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 2
    object lbPointBTitle: TLabel
      Left = 15
      Top = 14
      Width = 18
      Height = 13
      Caption = 'End'
    end
    object lblBx: TLabel
      Left = 16
      Top = 41
      Width = 35
      Height = 13
      Caption = 'Easting'
    end
    object lblBy: TLabel
      Left = 232
      Top = 41
      Width = 41
      Height = 13
      Caption = 'Northing'
    end
    object edBx: TEdit
      Left = 88
      Top = 41
      Width = 96
      Height = 21
      TabOrder = 0
      Text = '0'
      OnMouseDown = edMouseDown
    end
    object edBy: TEdit
      Left = 298
      Top = 41
      Width = 96
      Height = 21
      TabOrder = 1
      Text = '0'
      OnMouseDown = edMouseDown
    end
  end
  object btOK: TButton
    Left = 266
    Top = 204
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    TabOrder = 3
    OnClick = btOKClick
  end
end
