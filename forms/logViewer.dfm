object eventLogViewerForm: TeventLogViewerForm
  Left = 0
  Top = 0
  Caption = 'dBSea event log'
  ClientHeight = 751
  ClientWidth = 913
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    913
    751)
  PixelsPerInch = 96
  TextHeight = 13
  object mmLog: TMemo
    Left = 0
    Top = 0
    Width = 913
    Height = 681
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'mmLog')
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btOK: TButton
    Left = 768
    Top = 705
    Width = 121
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btSaveLog: TButton
    Left = 608
    Top = 705
    Width = 121
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Save log to file'
    TabOrder = 2
    OnClick = btSaveLogClick
  end
  object sdSaveLog: TSaveDialog
    DefaultExt = 's'
    Filter = 'Text files|*.txt|CSV files|*.csv|All files|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Choose output file'
    Left = 632
    Top = 320
  end
end
