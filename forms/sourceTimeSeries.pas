unit sourceTimeSeries;

interface

uses
    syncommons,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Series, ExtCtrls, Chart, Grids,
  timeseries, mcklib, helperFunctions, spectrum, math, filterbank,
  basetypes, equipReturn, levelConverter, VCLTee.TeEngine, VclTee.TeeGDIPlus,
  VCLTee.TeeProcs, eventLogging, problemContainer, dBSeaColours,
  SmartPointer, wavReader;

type
  TsourceTimeSeriesForm = class(TForm)
    chTimeSeries: TChart;
    srTimeSeries: TLineSeries;
    edFS: TEdit;
    btOpen: TButton;
    btOK: TButton;
    btCancel: TButton;
    lbFS: TLabel;
    lbSEL: TLabel;
    lbSPLpk: TLabel;
    lbSplpkpk: TLabel;
    sgLevels: TStringGrid;
    edCount: TEdit;
    lbCount: TLabel;
    odTimeSeries: TOpenDialog;
    srDownsampled: TLineSeries;
    srUpsampleAgain: TLineSeries;
    btOpenWAV: TButton;
    odWAVFile: TOpenDialog;
    btScale: TButton;
    edScale: TEdit;
    lbScaleBy: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure btOpenTextClick(Sender: TObject);
    procedure edFSChange(Sender: TObject);
    procedure edCountChange(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure sgLevelsSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgLevelsDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btSetSampleRateClick(Sender: TObject);
    procedure btOpenWAVClick(Sender: TObject);
    procedure btScaleClick(Sender: TObject);
  private
    procedure updateGraphAndTable;
    //function  highestFreq(): double;
  public
    timeSeries: TTimeSeries;
    mitigation: TEquipReturn;
  end;

var
  sourceTimeSeriesForm: TSourceTimeSeriesForm;

implementation

uses main;

{$B-}
{$R *.dfm}

procedure TsourceTimeSeriesForm.FormCreate(Sender: TObject);
begin
    timeseries := TTimeseries.Create(44100,0);
    mitigation := TEquipReturn.create();

    {$IF not (defined(DEBUG) or defined(VER2))}
    btOpenWAV.Visible := false;
    {$ENDIF}
end;

procedure TsourceTimeSeriesForm.FormDestroy(Sender: TObject);
begin
    timeseries.Free;
    mitigation.Free;
end;

procedure TsourceTimeSeriesForm.FormShow(Sender: TObject);
begin
    if isnan(timeSeries.fs) or (timeSeries.fs = 0) then
    begin
        eventLog.log('Timeseries fs = nan or 0, set to 44100');
        timeSeries.fs := 44100;
    end;

    edFS.Text := tostr(timeSeries.fs);

    if isnan(timeSeries.count) then
    begin
        eventLog.log('Timeseries count = nan, set to 1');
        timeSeries.count := 1;
    end;
    edCount.Text := tostr(timeSeries.count);

    updateGraphAndTable;
end;

procedure TsourceTimeSeriesForm.sgLevelsDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
    f: double;
begin
    with TStringGrid(Sender) do
        if (ARow > 0) and (ACol > 0) then
        begin
            f := TSpectrum.getFbyInd(acol - 1 + container.scenario.startfi,container.scenario.bandwidth,nil);
            if f > timeSeries.fs / 2 then
            begin
                Canvas.Brush.Color := TdBSeaColours.greyText;
                {$IFDEF VER280}
                if DefaultDrawing then
                    Rect.Left := Rect.Left - 4;
                Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+5, Cells[ACol, ARow]);
                {$ELSE}
                Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
                {$ENDIF}
                Canvas.FrameRect(Rect);
            end;
        end;
end;

procedure TsourceTimeSeriesForm.sgLevelsSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
    if (Arow < 2) or (ACol = 0) then
        sgLevels.Options := sgLevels.Options - [goEditing] + [goTabs]
    else
        sgLevels.Options := sgLevels.Options + [goEditing, goTabs, goAlwaysShowEditor];
end;

//function  TSourceTimeSeriesForm.highestFreq: double;
//var
//    bw: TBandwidth;
//    fi, startfi, lenfi: integer;
//    multipleSolveFrequencies: double;
//begin
//    result := nan;
//    //setup for multi freq solve
//    for fi := 0 to lenfi - 1 do
//    begin
//        multipleSolveFrequencies := TSpectrum.getFbyInd(fi + startfi, bw);
//        solveThisFreq[fi] := self.isTimeSeries or not isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw));
//        //don't solve bands above the nyquist
//        if self.isTimeSeries and (multipleSolveFrequencies[fi] > self.timeSeries.fs / (2 * sqrt(2))) then
//            solveThisFreq[fi] := false;
//
//        tempOversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(multipleSolveFrequencies[fi], bw, nFreqsPerBand);
//
//        if solveThisFreq[fi] then
//            result := tempOversampleFreqs[nFreqsPerBand - 1];
//    end;
//end;

procedure TSourceTimeSeriesForm.updateGraphAndTable;
var
    i, fi: Integer;
    lev: single;
    bw: TBandwidth;
    filterbank: ISmart<TFilterbank>;
    startfi, lenfi: Integer;
    sCountText: String;
    //downsampled: TTimeSeries;
    //upsampled: TTimeSeries;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'updateGraphAndTable');

    try
        if isnan(timeSeries.fs) or (timeSeries.fs <= 0) then
        begin
            eventLog.log('Timeseries fs = nan or 0, set to 44100');
            timeSeries.fs := 44100;
        end;

        srTimeSeries.Clear;
        for I := 0 to length(timeSeries.samples) - 1 do
            srTimeSeries.AddXY(i / timeseries.fs, timeseries.samples[i]);

        srUpsampleAgain.Clear;
        srDownsampled.clear;
//        downsampled := self.timeSeries.downsampleTo(500 * 5);//self.highestFreq * 5);
//        upsampled := TTimeSeries.create(1,1);
//        upsampled.cloneFrom(downsampled);
//        for i := 0 to 3 do
//            upsampled.upsample1Octave;
//        try
//            for I := 0 to length(downsampled.samples) - 1 do
//                srDownsampled.AddXY(i / downsampled.fs, downsampled.samples[i] + 20000);
//            for I := 0 to length(upsampled.samples) - 1 do
//                srUpsampleAgain.AddXY(i / upsampled.fs, upsampled.samples[i] + 40000);
//        finally
//            downsampled.free;
//        end;

        if isnan(timeSeries.count) or (timeSeries.count = 0) or (timeSeries.count = 1) then
        begin
            eventLog.log('Timeseries count = nan or 0');
            sCountText := ''
        end
        else
            sCountText := ' + ' + tostr(round1(dB10(timeSeries.count)));

        lev := timeSeries.SEL;
        if isnan(lev) then
            lbSEL    .Caption := 'SEL -'
        else
            lbSEL    .Caption := 'SEL = ' + tostr(round1(lev)) + sCountText + ' dB re 1 uPa';

        lev := timeSeries.SPLpeak;
        if isnan(lev) then
            lbSplpk  .Caption := 'SPLpeak -'
        else
            lbSplpk  .Caption := 'SPLpeak = ' + tostr(round1(lev)) + ' dB re 1 uPa';

        lev := timeSeries.SPLpeaktopeak;
        if isnan(lev) then
            lbSplpkpk.Caption := 'SPLpeak to peak -'
        else
            lbSplpkpk.Caption := 'SPLpeak to peak = ' + tostr(round1(lev)) + ' dB re 1 uPa';

        sgLevels.ColCount := container.scenario.lenfi + 1;
        bw := container.scenario.bandwidth;
        startfi := container.scenario.startfi;
        lenfi := container.scenario.lenfi;
        try
            filterbank := TSmart<TFilterbank>.create(TFilterbank.Create(timeseries.fs));
        except on e: Exception do
            eventLog.logException(log, 'Error creating filterbank: ' + e.ToString);
        end;

        sgLevels.Cells[0,0] := 'f (Hz)';
        sgLevels.Cells[0,1] := 'Level';
        sgLevels.Cells[0,2] := 'Mitigation';
        for fi := 0 to lenfi - 1 do
        begin
            sgLevels.Cells[fi + 1,0] := TSpectrum.getFStringbyInd(fi + startfi,bw,eventlog);
            lev := TTimeSeries.SEL(filterbank.band(bw,
                                                   TSpectrum.getFbyInd(fi + startfi,bw),
                                                   timeSeries.samples),
                                   timeseries.fs);   //includes conversion to re 1uPa

            if isnan(lev) then
                sgLevels.Cells[fi + 1,1] := '-'
            else
                sgLevels.Cells[fi + 1,1] := tostr(round1(lev));

            //mitigation
            lev := mitigation.spectrum.getAmpbyInd(fi + container.scenario.startfi, container.scenario.bandwidth);
            sgLevels.Cells[fi + 1,2] := tostrWithTestAndRound1( lev );
        end;
    except on e: Exception do
    begin
        eventLog.logException(log, e.ToString);
        showmessage('There was an error while filtering the time series');
    end;
    end;
end;

procedure TsourceTimeSeriesForm.btOpenTextClick(Sender: TObject);
const
    fadeInLength = 0; //was this there because of FFT or filters?
var
    s: string;
begin
    if not odTimeSeries.Execute then exit;

    s := THelper.readfiletostring(odTimeSeries.filename);
    timeSeries.readSamplesFromString(s,fadeInLength);
    updateGraphAndTable;
end;

procedure TsourceTimeSeriesForm.btOpenWAVClick(Sender: TObject);
begin
    if not odWAVFile.Execute then exit;

    wr.readWAVInfo(odWAVFile.FileName);
    wr.readAllWAVSamples(odWAVFile.FileName, true, 0);
    edFS.Text := inttostr(wr.fs);
    timeSeries.setWithWavReader(wr);
    timeSeries.samples.normalise;
    updateGraphAndTable;
    showmessage('Normalised data imported. Scale the pressure to the correct level');
end;

procedure TsourceTimeSeriesForm.btScaleClick(Sender: TObject);
begin
    if not isFloat(edScale.Text) then exit;

    timeSeries.samples.multiplyBy(tofl(edScale.Text));
    updateGraphAndTable;
end;

procedure TsourceTimeSeriesForm.btSetSampleRateClick(Sender: TObject);
begin
    self.updateGraphAndTable;
end;

procedure TsourceTimeSeriesForm.btOKClick(Sender: TObject);
var
    fi: integer;
begin
    for fi := 0 to container.scenario.lenfi - 1 do
        if isfloat(sgLevels.Cells[fi + 1,2]) then
            mitigation.spectrum.setMitigationAmpbyInd( fi + container.scenario.startfi,
                                                      -tofl(sgLevels.Cells[fi + 1,2]), //nb negative
                                                       container.scenario.bandwidth);

    modalResult := mrOK;
end;


procedure TsourceTimeSeriesForm.edCountChange(Sender: TObject);
begin
    if isFloat(edCount.Text) then
    begin
        timeSeries.count := tofl(edCount.Text);
        updateGraphAndTable;
    end;
end;

procedure TsourceTimeSeriesForm.edFSChange(Sender: TObject);
begin
    if not isFloat(edFS.Text) then exit;

    timeSeries.fs := tofl(edFS.Text);
    self.updateGraphAndTable;
end;


end.
