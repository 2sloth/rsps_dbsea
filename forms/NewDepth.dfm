object NewDepthForm: TNewDepthForm
  Left = 0
  Top = 0
  Caption = 'New project'
  ClientHeight = 139
  ClientWidth = 316
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    316
    139)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 43
    Width = 106
    Height = 13
    Caption = 'Bathymetry depth (m)'
  end
  object btOK: TButton
    Left = 129
    Top = 96
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 0
    OnClick = btOKClick
    ExplicitLeft = 448
    ExplicitTop = 256
  end
  object btCancel: TButton
    Left = 217
    Top = 96
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    ExplicitLeft = 536
    ExplicitTop = 256
  end
  object edDepth: TEdit
    Left = 160
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '100'
  end
end
