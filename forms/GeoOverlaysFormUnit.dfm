object GeoOverlaysForm: TGeoOverlaysForm
  Left = 0
  Top = 0
  Caption = 'Shape overlays'
  ClientHeight = 471
  ClientWidth = 453
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    453
    471)
  PixelsPerInch = 96
  TextHeight = 13
  object lbName: TLabel
    Left = 32
    Top = 332
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lbZPosition: TLabel
    Left = 32
    Top = 396
    Width = 63
    Height = 13
    Caption = 'Elevation (m)'
  end
  object lbSourceFileTitle: TLabel
    Left = 32
    Top = 364
    Width = 50
    Height = 13
    Caption = 'Source file'
  end
  object lbSourceFile: TLabel
    Left = 136
    Top = 364
    Width = 65
    Height = 13
    Caption = 'Z position (m)'
  end
  object lbOverlays: TListBox
    Left = 32
    Top = 24
    Width = 379
    Height = 241
    Anchors = [akLeft, akTop, akRight]
    ItemHeight = 13
    TabOrder = 0
    OnClick = lbOverlaysClick
  end
  object btAdd: TButton
    Left = 32
    Top = 280
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 1
    OnClick = btAddClick
  end
  object btRemove: TButton
    Left = 136
    Top = 280
    Width = 75
    Height = 25
    Caption = 'Remove'
    TabOrder = 2
    OnClick = btRemoveClick
  end
  object btOK: TButton
    Left = 336
    Top = 421
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Done'
    ModalResult = 1
    TabOrder = 3
  end
  object edName: TEdit
    Left = 136
    Top = 329
    Width = 179
    Height = 21
    TabOrder = 4
    Text = 'edName'
    OnChange = edNameChange
  end
  object cbVisible: TCheckBox
    Left = 32
    Top = 434
    Width = 65
    Height = 17
    Caption = 'Visible'
    TabOrder = 5
    OnClick = cbVisibleClick
  end
  object btColour: TButton
    Left = 240
    Top = 280
    Width = 75
    Height = 25
    Caption = 'Colour'
    TabOrder = 6
    OnClick = btColourClick
  end
  object edZPosition: TEdit
    Left = 136
    Top = 393
    Width = 179
    Height = 21
    TabOrder = 7
    Text = 'Edit1'
    OnChange = edZPositionChange
  end
  object cbFillVisible: TCheckBox
    Left = 103
    Top = 434
    Width = 65
    Height = 17
    Caption = 'Draw fill'
    TabOrder = 8
    OnClick = cbFillVisibleClick
  end
  object odShp: TOpenDialog
    DefaultExt = 'shp'
    Filter = 'SHP file|*.shp|All files|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 352
    Top = 48
  end
end
