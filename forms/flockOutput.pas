unit flockOutput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, flock, helperFunctions, math, dBSeaconstants,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.Series, Vcl.ExtCtrls,
  VCLTee.TeeProcs, VCLTee.Chart, system.Types, HistogramCalc, mcklib,
  dBSeaColours, baseTypes, Vcl.CheckLst, generics.collections, animat;

type
  TExportChecksFunc = reference to function(): boolean;

  TFlockOutputForm = class(TForm)
    btOK: TButton;
    chLevsHistogram: TChart;
    srLevsHistogram: TBarSeries;
    rgLevelType: TRadioGroup;
    chLevsHistory: TChart;
    srLevsHistory: TPointSeries;
    rgBinSpacing: TRadioGroup;
    btExport: TButton;
    clSelect: TCheckListBox;
    chSelectedAnimats: TChart;
    btExportSelectedToFile: TButton;
    pbLeft: TPanel;
    pnRight: TPanel;
    Panel3: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    cbShowAll: TCheckBox;
    pnButtons: TPanel;
    sdExportFlock: TSaveDialog;
    procedure FormShow(Sender: TObject);

    procedure rgLevelTypeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rgBinSpacingClick(Sender: TObject);
    procedure btExportClick(Sender: TObject);
    procedure clSelectClickCheck(Sender: TObject);
    procedure Splitter2Paint(Sender: TObject);
    procedure cbShowAllClick(Sender: TObject);
  private
    fflock : TFlock;

    procedure setFlock(const aflock : TFlock);
    procedure updateGraph;
    procedure updatChLevsHistory(const binSpacing: integer);
    procedure updateSelectedGraph;
  public
    outputPerNSteps : integer;
    stepTime : double;
    exportChecksFunc : TExportChecksFunc;
    defaultSavePath : TFilename;

    property flock : TFlock read fflock write setFlock;
  end;

var
    flockOutputForm: TFlockOutputForm;

implementation

{$B-}
{$R *.dfm}


procedure TflockOutputForm.btExportClick(Sender: TObject);
var
    i : integer;
    ba : TBooleanDynArray;
begin
    if not self.exportChecksFunc() then exit;

    sdExportFlock.Title := 'Choose output data file';
    if directoryExists(self.defaultSavePath) then
        sdExportFlock.InitialDir := self.defaultSavePath
    else
        sdExportFlock.InitialDir := THelper.GetMyDocumentsPath();
    if sdExportFlock.Execute then
    begin
        if FileExists(sdExportFlock.filename) and THelper.IsFileInUse(sdExportFlock.filename) then
        begin
            showmessage('File is in use by another process, operation cancelled');
            Exit;
        end;

        setlength(ba,self.clSelect.count);
        for i := 0 to self.clSelect.Count - 1 do
            ba[i] := self.clSelect.Selected[i];
        flock.writeToFile(sdExportFlock.FileName, (sender as TButton).tag = 1, ba);
    end;
end;

procedure TflockOutputForm.cbShowAllClick(Sender: TObject);
var
    i : integer;
begin
    for i := 0 to clSelect.Count - 1 do
        clSelect.Checked[i] := cbShowAll.Checked;
    self.updateSelectedGraph;
end;

procedure TflockOutputForm.clSelectClickCheck(Sender: TObject);
begin
    self.updateSelectedGraph();
end;

procedure TflockOutputForm.FormCreate(Sender: TObject);
begin
    outputPerNSteps := 60;
    stepTime := 1.0;
    //edThreshold.Text := '200';
end;

procedure TflockOutputForm.FormShow(Sender: TObject);
var
    animat : TAnimat;
    lev : single;
begin
    flock.sortAnimatsBySEL;
    flock.animats.Reverse;

    clSelect.clear;
    for animat in flock.animats do
    begin
        case rgLevelType.ItemIndex of
        1 : lev := animat.peakLevel;
        else lev := animat.SEL;
        end;
        if not (isnan(lev) or isnan(lev) or (lev < lowLev)) then
            clSelect.AddItem(tostr(clSelect.Count + 1) + ': ' + tostrWithTestAndRound1(lev) + ' dB',animat);
    end;

    self.updateGraph;
end;

procedure TflockOutputForm.rgBinSpacingClick(Sender: TObject);
begin
    self.updateGraph;
end;

procedure TflockOutputForm.rgLevelTypeClick(Sender: TObject);
begin
    self.updateGraph;
end;

procedure TflockOutputForm.setFlock(const aflock : TFlock);
begin
    self.fflock := aflock;
end;

procedure TflockOutputForm.Splitter2Paint(Sender: TObject);
begin
    with sender as TSplitter do
    begin
        Canvas.Brush.Color := clGray;
        Canvas.FillRect(ClientRect);
    end;
end;

procedure TflockOutputForm.updateGraph;
var
    i : integer;
    binCenters : TDoubleDynArray;
    binCounts : TIntegerDynArray;
    binSpacing : integer;
begin

    case rgBinSpacing.ItemIndex of
    0: binSpacing := 1;
    1: binSpacing := 3;
    2: binSpacing := 5;
    3: binSpacing := 10;
    4: binSpacing := 20;
    else binSpacing := 1;
    end;

    srLevsHistogram.Clear;
    srLevsHistogram.ColorEachPoint := true;
    case rgLevelType.ItemIndex of
    1: TMyHistogram.calcHistogramBins(flock.animatPeakArray.toDoubleArray,binSpacing,binCenters,binCounts);
    else TMyHistogram.calcHistogramBins(flock.animatSELArray.toDoubleArray,binSpacing,binCenters,binCounts);
    end;

    if length(binCenters) > 0 then
        for i := 0 to length(binCenters) - 1 do
            srLevsHistogram.AddXY(bincenters[i],bincounts[i], '', TdBSeaColours.darkBlue);

    updatChLevsHistory(binSpacing);
end;

procedure TflockOutputForm.updatChLevsHistory(const binSpacing: integer);
var
    i,j : integer;
    levsHistory : TSingleMatrix;
    maxCount : integer;
    binCenters : TDoubleDynArray;
    binCounts : TIntegerDynArray;
begin
    srLevsHistory.clear;
    srLevsHistory.Pointer.SizeFloat := 5;
    srLevsHistory.ColorEachPoint := true;
    levsHistory := flock.animatLevsHistory(outputPerNSteps);
    levsHistory := levsHistory.swapAxes;
    if length(levsHistory) > 0 then
    begin
        maxCount := 0;
        for i := 0 to length(levsHistory) - 1 do
        begin
            TMyHistogram.calcHistogramBins(levsHistory[i].toDoubleArray,binSpacing,binCenters,binCounts);
            maxCount := max(maxCount,bincounts.max);
        end;

        for i := 0 to length(levsHistory) - 1 do
        begin
            TMyHistogram.calcHistogramBins(levsHistory[i].toDoubleArray,binSpacing,binCenters,binCounts);
            for j := 0 to length(bincenters) - 1 do
                srLevsHistory.AddXY(steptime* i * outputPerNSteps, bincenters[j], '', TDBSeaColours.white(bincounts[j] / maxCount));
        end;
    end;
end;

procedure TflockOutputForm.updateSelectedGraph;
var
    i,j : integer;
    animat : TAnimat;
    lineSeries : TLineSeries;
begin
    chSelectedAnimats.serieslist.clear;
    if self.clSelect.Count > 0 then
    begin
        for I := 0 to self.clSelect.Count - 1 do
            if self.clSelect.Checked[i] and (self.clSelect.Items.Objects[i] is TAnimat) then
            begin
                animat := TAnimat(self.clSelect.Items.Objects[i]);
                lineSeries := TLineSeries.Create(chSelectedAnimats);
                for j := 0 to animat.levelHistory.Count - 1 do
                    lineSeries.AddXY(j * stepTime, animat.levelHistory[j]);
                chSelectedAnimats.AddSeries(lineSeries);
            end;

        //line series showing threshold
        lineSeries := TLineSeries.Create(chSelectedAnimats);
        //lineSeries.Style := dashed
        lineSeries.AddXY(0,flock.animats[0].thresholdLevel);
        lineSeries.AddXY((flock.animats[0].levelHistory.count - 1) * stepTime,flock.animats[0].thresholdLevel);
        chSelectedAnimats.AddSeries(lineSeries);
    end;
end;

end.
