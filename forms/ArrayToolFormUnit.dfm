object ArrayToolForm: TArrayToolForm
  Left = 0
  Top = 0
  Caption = 'Array tool'
  ClientHeight = 418
  ClientWidth = 487
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 139
    Width = 58
    Height = 13
    Caption = 'Copied from'
  end
  object Label2: TLabel
    Left = 34
    Top = 179
    Width = 56
    Height = 13
    Caption = 'Spacing (m)'
  end
  object pbDirection: TPaintBox
    Left = 370
    Top = 216
    Width = 79
    Height = 79
    OnMouseDown = pbDirectionMouseDown
    OnPaint = pbDirectionPaint
  end
  object lblDirection: TLabel
    Left = 32
    Top = 219
    Width = 58
    Height = 13
    Caption = 'Direction ('#176')'
  end
  object Label3: TLabel
    Left = 34
    Top = 309
    Width = 132
    Height = 13
    Caption = 'Additional objects to create'
  end
  object rgObjectType: TRadioGroup
    Left = 32
    Top = 32
    Width = 169
    Height = 81
    Caption = 'Create objects'
    ItemIndex = 0
    Items.Strings = (
      'Sources'
      'Probes')
    TabOrder = 0
    OnClick = rgObjectTypeClick
  end
  object btOK: TButton
    Left = 270
    Top = 360
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btOKClick
  end
  object btCancel: TButton
    Left = 366
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object edSpacing: TEdit
    Left = 200
    Top = 176
    Width = 145
    Height = 21
    TabOrder = 3
    Text = '1000'
  end
  object cbSourceObject: TComboBox
    Left = 200
    Top = 136
    Width = 145
    Height = 21
    DropDownCount = 100
    TabOrder = 4
    Text = 'cbSourceObject'
  end
  object edDirection: TEdit
    Left = 200
    Top = 216
    Width = 145
    Height = 21
    TabOrder = 5
    Text = '0'
    OnEnter = edDirectionEnter
    OnExit = edDirectionExit
  end
  object edNumberToCreate: TEdit
    Left = 200
    Top = 306
    Width = 145
    Height = 21
    TabOrder = 6
    Text = '1'
  end
end
