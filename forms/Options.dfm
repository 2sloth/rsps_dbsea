object OptionsForm: TOptionsForm
  Left = 0
  Top = 0
  Caption = 'Preferences'
  ClientHeight = 721
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    784
    721)
  PixelsPerInch = 96
  TextHeight = 13
  object btOK: TButton
    Left = 591
    Top = 688
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = btOKClick
  end
  object btCancel: TButton
    Left = 687
    Top = 688
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    OnClick = btCancelClick
  end
  object pcOptionsTabs: TPageControl
    Left = 0
    Top = 0
    Width = 784
    Height = 676
    ActivePage = tsAdvanced
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    object tsGraphics: TTabSheet
      Caption = 'Graphics'
      ImageIndex = 2
      object lbLayerNumber: TLabel
        Left = 370
        Top = 297
        Width = 116
        Height = 13
        Caption = 'Display solution depth #'
      end
      object lbDepth: TLabel
        Left = 370
        Top = 316
        Width = 29
        Height = 13
        Caption = 'Depth'
      end
      object lblMaxGLBathyPixels: TLabel
        Left = 38
        Top = 61
        Width = 185
        Height = 13
        Caption = 'Max bathymetry graphics data (pixels)'
      end
      object btBackgroundFlatColor: TButton
        Left = 316
        Top = 434
        Width = 133
        Height = 25
        Caption = 'Background colour'
        TabOrder = 0
        OnClick = btBackgroundFlatColorClick
      end
      object btLandColorPick: TButton
        Left = 177
        Top = 434
        Width = 133
        Height = 25
        Caption = 'Land colour'
        TabOrder = 1
        OnClick = btLandColorPickClick
      end
      object btWaterColorPick: TButton
        Left = 38
        Top = 434
        Width = 133
        Height = 25
        Caption = 'Bathymetry colour'
        TabOrder = 2
        OnClick = btWaterColorPickClick
      end
      object cbSmoothColorMap: TCheckBox
        Left = 40
        Top = 136
        Width = 145
        Height = 17
        Caption = 'Smooth result colours'
        TabOrder = 3
        OnClick = cbSmoothColorMapClick
      end
      object cbShowLandInXSec: TCheckBox
        Left = 257
        Top = 200
        Width = 183
        Height = 17
        Caption = 'Show land in cross-section view'
        TabOrder = 4
        OnClick = cbShowLandInXSecClick
      end
      object cbResultsTris: TCheckBox
        Left = 39
        Top = 167
        Width = 202
        Height = 17
        Caption = 'Draw levels with triangle algorithm'
        TabOrder = 5
        OnClick = cbResultsTrisClick
      end
      object rgLevelsDisplay: TRadioGroup
        Left = 34
        Top = 281
        Width = 276
        Height = 112
        Caption = 'Levels display'
        ItemIndex = 0
        Items.Strings = (
          'Max from all depths projected to surface'
          'Single depth'
          'All depths')
        TabOrder = 6
        OnClick = rgLevelsDisplayClick
      end
      object cbHideProbesOnExport: TCheckBox
        Left = 39
        Top = 200
        Width = 171
        Height = 17
        Caption = 'Hide probes on image export'
        TabOrder = 7
        OnClick = cbHideProbesOnExportClick
      end
      object cbFullXSecDepth: TCheckBox
        Left = 257
        Top = 136
        Width = 224
        Height = 17
        Caption = 'Show full water depth in cross-section'
        TabOrder = 8
        OnClick = cbFullXSecDepthClick
      end
      object cbXSecTruncateAtWaterSurface: TCheckBox
        Left = 257
        Top = 169
        Width = 180
        Height = 17
        Caption = 'Truncate land in cross-section'
        TabOrder = 9
        OnClick = cbXSecTruncateAtWaterSurfaceClick
      end
      object edGLMaxBathyPixels: TEdit
        Left = 39
        Top = 80
        Width = 53
        Height = 21
        NumbersOnly = True
        TabOrder = 10
        Text = '1'
      end
      object btDisplayIndUp: TButton
        Left = 537
        Top = 321
        Width = 20
        Height = 20
        Caption = '+'
        TabOrder = 11
        OnClick = btDisplayIndUpClick
      end
      object btDisplayIndDn: TButton
        Left = 537
        Top = 295
        Width = 20
        Height = 20
        Caption = '-'
        TabOrder = 12
        OnClick = btDisplayIndDnClick
      end
      object btUseTopoLandColors: TButton
        Left = 177
        Top = 465
        Width = 133
        Height = 25
        Caption = 'Default land colours'
        TabOrder = 13
        OnClick = btUseTopoLandColorsClick
      end
      object btNODATAColor: TButton
        Left = 38
        Top = 465
        Width = 133
        Height = 25
        Caption = 'Not surveyed colour'
        TabOrder = 14
        OnClick = btNODATAColorClick
      end
      object cbLiveUpdate1: TCheckBox
        Left = 40
        Top = 19
        Width = 249
        Height = 17
        Caption = 'Immediately update results graphics'
        TabOrder = 15
      end
    end
    object tsGraphicsSizes: TTabSheet
      Caption = 'Graphics sizes'
      ImageIndex = 6
      DesignSize = (
        776
        648)
      object lbSourceSize: TLabel
        Left = 38
        Top = 187
        Width = 54
        Height = 13
        Caption = 'Source size'
      end
      object lbProbeSize: TLabel
        Left = 38
        Top = 241
        Width = 49
        Height = 13
        Caption = 'Probe size'
      end
      object lblLandScaling: TLabel
        Left = 38
        Top = 117
        Width = 91
        Height = 13
        Caption = 'Land height scaling'
      end
      object lblDepthScaling: TLabel
        Left = 38
        Top = 54
        Width = 64
        Height = 13
        Caption = 'Depth scaling'
      end
      object lblResultsAlpha: TLabel
        Left = 38
        Top = 415
        Width = 73
        Height = 13
        Caption = 'Results opacity'
      end
      object lblMovingSrcWidth: TLabel
        Left = 38
        Top = 299
        Width = 117
        Height = 13
        Caption = 'Moving source line width'
      end
      object lbResultsContoursDarkening: TLabel
        Left = 38
        Top = 471
        Width = 130
        Height = 13
        Caption = 'Results contours darkening'
      end
      object lbResultsContoursWidth: TLabel
        Left = 38
        Top = 355
        Width = 128
        Height = 13
        Caption = 'Results contours line width'
      end
      object sbSourceSize: TScrollBar
        Left = 38
        Top = 206
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 30
        PageSize = 0
        TabOrder = 0
        OnChange = sbSourceSizeChange
      end
      object sbZLandFact: TScrollBar
        Left = 38
        Top = 139
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 30
        Max = 1000
        PageSize = 0
        TabOrder = 1
        OnChange = sbZLandFactChange
      end
      object sbResultsAlpha: TScrollBar
        Left = 38
        Top = 434
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 10
        PageSize = 0
        Position = 100
        SmallChange = 10
        TabOrder = 2
        OnChange = sbResultsAlphaChange
      end
      object sbMovingSrcLinewidth: TScrollBar
        Left = 38
        Top = 318
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 30
        Max = 8
        Min = 1
        PageSize = 0
        Position = 1
        TabOrder = 3
        OnChange = sbMovingSrcLinewidthChange
      end
      object sbProbeSize: TScrollBar
        Left = 38
        Top = 260
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 30
        PageSize = 0
        TabOrder = 4
        OnChange = sbProbeSizeChange
      end
      object sbZFact: TScrollBar
        Left = 38
        Top = 77
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 30
        Max = 1000
        PageSize = 0
        Position = 30
        TabOrder = 5
        OnChange = sbZFactChange
      end
      object sbResultsContoursDarkening: TScrollBar
        Left = 38
        Top = 490
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 30
        PageSize = 0
        Position = 1
        TabOrder = 6
        OnChange = sbResultsContoursDarkeningChange
      end
      object sbResultsContoursWidth: TScrollBar
        Left = 38
        Top = 374
        Width = 706
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        LargeChange = 30
        Max = 8
        Min = 1
        PageSize = 0
        Position = 1
        TabOrder = 7
        OnChange = sbResultsContoursWidthChange
      end
      object cbLiveUpdate2: TCheckBox
        Left = 39
        Top = 19
        Width = 274
        Height = 17
        Caption = 'Immediately update results graphics'
        TabOrder = 8
      end
    end
    object tsAdvanced: TTabSheet
      Caption = 'Advanced'
      ImageIndex = 5
      object lblRadialSmoothing: TLabel
        Left = 32
        Top = 121
        Width = 113
        Height = 13
        Caption = 'Radial smoothing factor'
      end
      object lbExclusionZoneRadialSmoothing: TLabel
        Left = 32
        Top = 153
        Width = 183
        Height = 13
        Caption = 'Exclusion zone radial smoothing factor'
      end
      object lbFreqOversampling: TLabel
        Left = 32
        Top = 89
        Width = 279
        Height = 13
        Caption = 'Frequency oversampling (number of solve freqs per band)'
      end
      object udRadialSmoothing: TUpDown
        Left = 399
        Top = 118
        Width = 17
        Height = 21
        Associate = edRadialSmoothing
        Max = 10000
        Position = 1
        TabOrder = 3
        Thousands = False
      end
      object cbMakeTLMonotonic: TCheckBox
        Left = 32
        Top = 59
        Width = 264
        Height = 17
        TabStop = False
        Caption = 'Levels must decrease with distance from source'
        TabOrder = 4
        OnClick = cbMakeTLMonotonicClick
      end
      object edRadialSmoothing: TEdit
        Left = 350
        Top = 118
        Width = 49
        Height = 21
        TabOrder = 1
        Text = '1'
      end
      object cbNaNSeafloorLevels: TCheckBox
        Left = 32
        Top = 36
        Width = 185
        Height = 17
        TabStop = False
        Caption = 'Remove levels below seafloor'
        TabOrder = 5
        OnClick = cbNaNSeafloorLevelsClick
      end
      object cbAutosaveAfterSolve: TCheckBox
        Left = 32
        Top = 13
        Width = 216
        Height = 17
        TabStop = False
        Caption = 'Autosave current project after solve'
        TabOrder = 6
        OnClick = cbAutosaveAfterSolveClick
      end
      object edExclusionZoneRadialSmoothing: TEdit
        Left = 350
        Top = 150
        Width = 49
        Height = 21
        TabOrder = 2
        Text = '1'
      end
      object udExclusionZoneRadialSmoothing: TUpDown
        Left = 399
        Top = 150
        Width = 17
        Height = 21
        Associate = edExclusionZoneRadialSmoothing
        Max = 10000
        Position = 1
        TabOrder = 7
        Thousands = False
      end
      object edFreqOversampling: TEdit
        Left = 350
        Top = 86
        Width = 49
        Height = 21
        TabOrder = 0
        Text = '1'
      end
      object udFrequencyOversampling: TUpDown
        Left = 399
        Top = 86
        Width = 17
        Height = 21
        Associate = edFreqOversampling
        Max = 10000
        Position = 1
        TabOrder = 8
        Thousands = False
      end
      object cbStopMarchingSolutionAtLand: TCheckBox
        Left = 350
        Top = 13
        Width = 290
        Height = 17
        TabStop = False
        Caption = 'Stop marching solution upon reaching land (2D only)'
        TabOrder = 9
        OnClick = cbStopMarchingSolutionAtLandClick
      end
      object pcSolverSpecific: TPageControl
        Left = 21
        Top = 186
        Width = 737
        Height = 447
        ActivePage = tsParabolicEquation
        TabOrder = 10
        object tsParabolicEquation: TTabSheet
          Caption = 'Parabolic equation'
          object lbNumberPadeTerms: TLabel
            Left = 11
            Top = 178
            Width = 277
            Height = 13
            Caption = 'Number of Pad'#233' terms (use 0 for Greene'#39's approximation)'
          end
          object lbPErOversampling: TLabel
            Left = 11
            Top = 103
            Width = 97
            Height = 13
            Caption = 'Range oversampling'
          end
          object lbPETitle: TLabel
            Left = 11
            Top = 16
            Width = 42
            Height = 13
            Caption = 'dBSeaPE'
          end
          object lbPEzOversampling: TLabel
            Left = 11
            Top = 64
            Width = 95
            Height = 13
            Caption = 'Depth oversampling'
          end
          object lbNumberPadeTermsHorizontal: TLabel
            Left = 11
            Top = 209
            Width = 232
            Height = 13
            Caption = '3D: Number of Pad'#233' terms in horizontal direction'
          end
          object lbPEPhiOversampling: TLabel
            Left = 11
            Top = 143
            Width = 112
            Height = 13
            Caption = '3D: angle oversampling'
          end
          object cbPEFFT: TCheckBox
            Left = 11
            Top = 251
            Width = 101
            Height = 18
            TabStop = False
            Caption = 'Split-step version'
            TabOrder = 0
            OnClick = cbPEFFTClick
          end
          object edNumberPadeTerms: TEdit
            Left = 313
            Top = 177
            Width = 97
            Height = 21
            TabOrder = 3
            Text = '0'
          end
          object sbPErOversampling: TScrollBar
            Left = 158
            Top = 99
            Width = 121
            Height = 18
            Max = 1000
            PageSize = 0
            Position = 1
            TabOrder = 1
            OnChange = sbPErOversamplingChange
          end
          object sbPEzOversampling: TScrollBar
            Left = 158
            Top = 60
            Width = 121
            Height = 18
            Max = 1000
            PageSize = 0
            Position = 1
            TabOrder = 2
            OnChange = sbPEzOversamplingChange
          end
          object udNumberPadeTerms: TUpDown
            Left = 410
            Top = 177
            Width = 16
            Height = 21
            Associate = edNumberPadeTerms
            Max = 100000
            TabOrder = 5
          end
          object edNumberPadeTermsHorizontal: TEdit
            Left = 313
            Top = 208
            Width = 97
            Height = 21
            TabOrder = 4
            Text = '0'
          end
          object udNumberPadeTermsHorizontal: TUpDown
            Left = 410
            Top = 208
            Width = 16
            Height = 21
            Associate = edNumberPadeTermsHorizontal
            Max = 100000
            TabOrder = 6
          end
          object sbPEPhiOversampling: TScrollBar
            Left = 158
            Top = 139
            Width = 121
            Height = 18
            Max = 1000
            Min = 1
            PageSize = 0
            Position = 1
            TabOrder = 7
            OnChange = sbPEPhiOversamplingChange
          end
        end
        object tsModal: TTabSheet
          Caption = 'Normal modes'
          ImageIndex = 1
          DesignSize = (
            729
            419)
          object lbdBSeaModes: TLabel
            Left = 11
            Top = 4
            Width = 61
            Height = 13
            Anchors = [akLeft, akBottom]
            Caption = 'dBSeaModes'
            ExplicitTop = 16
          end
          object lblDBSeaModesMaxModes: TLabel
            Left = 11
            Top = 120
            Width = 197
            Height = 13
            Anchors = [akLeft, akBottom]
            Caption = 'Max number of modes (use 0 for default)'
            ExplicitTop = 132
          end
          object lbModesROversampling: TLabel
            Left = 11
            Top = 90
            Width = 97
            Height = 13
            Caption = 'Range oversampling'
          end
          object lbModesZOversampling: TLabel
            Left = 11
            Top = 53
            Width = 95
            Height = 13
            Caption = 'Depth oversampling'
          end
          object cbKrakenCoupledModes: TCheckBox
            Left = 16
            Top = 162
            Width = 174
            Height = 17
            Anchors = [akLeft, akBottom]
            Caption = 'Coupled solve'
            TabOrder = 0
            Visible = False
            OnClick = cbKrakenCoupledModesClick
          end
          object edDBSeaModesMaxModes: TEdit
            Left = 239
            Top = 119
            Width = 97
            Height = 21
            Anchors = [akLeft, akBottom]
            NumbersOnly = True
            TabOrder = 3
            Text = '0'
          end
          object sbModesROversampling: TScrollBar
            Left = 158
            Top = 86
            Width = 121
            Height = 17
            Min = 10
            PageSize = 0
            Position = 10
            TabOrder = 1
            OnChange = sbModesROversamplingChange
          end
          object sbModeszOversampling: TScrollBar
            Left = 158
            Top = 49
            Width = 121
            Height = 17
            Min = 10
            PageSize = 0
            Position = 10
            TabOrder = 2
            OnChange = sbModeszOversamplingChange
          end
        end
        object tsRayTracer: TTabSheet
          Caption = 'Ray tracer'
          ImageIndex = 2
          object lbAngleHint: TLabel
            Left = 11
            Top = 201
            Width = 109
            Height = 13
            Caption = '-90 = up, +90 = down'
          end
          object lbAngleTo: TLabel
            Left = 366
            Top = 187
            Width = 10
            Height = 13
            Caption = 'to'
          end
          object lbDBSeaRayTitle: TLabel
            Left = 11
            Top = 16
            Width = 49
            Height = 13
            Caption = 'dBSeaRay'
          end
          object lbInitialStepSize: TLabel
            Left = 11
            Top = 226
            Width = 90
            Height = 13
            Caption = 'Initial step size (m)'
          end
          object lblBellhopNRays: TLabel
            Left = 11
            Top = 116
            Width = 187
            Height = 13
            Caption = 'Number of rays per slice (0 for default)'
          end
          object lblDBSeaRayMaxBottomReflections: TLabel
            Left = 11
            Top = 147
            Width = 192
            Height = 13
            Caption = 'Max number of reflections from seafloor'
          end
          object lbRayTracingAngles: TLabel
            Left = 11
            Top = 182
            Width = 141
            Height = 13
            Caption = 'Initial angles range (degrees)'
          end
          object lblBellhopNRaysAzimuth: TLabel
            Left = 360
            Top = 116
            Width = 261
            Height = 13
            Caption = '3D: Number of ray divisions horizontally (0 for default)'
          end
          object cbCalculateAttenuationAtEachStep: TCheckBox
            Left = 11
            Top = 82
            Width = 336
            Height = 17
            TabStop = False
            Caption = 'Calculate volume attenuation at each step'
            TabOrder = 6
            OnClick = cbCalculateAttenuationAtEachStepClick
          end
          object cbRayTracingCoherent: TCheckBox
            Left = 11
            Top = 51
            Width = 162
            Height = 17
            TabStop = False
            Caption = 'Coherent rays'
            TabOrder = 7
            OnClick = cbRayTracingCoherentClick
          end
          object edBellhopNRays: TEdit
            Left = 241
            Top = 113
            Width = 97
            Height = 21
            NumbersOnly = True
            TabOrder = 0
            Text = '0'
          end
          object edDBSeaRayMaxBottomReflections: TEdit
            Left = 241
            Top = 148
            Width = 97
            Height = 21
            NumbersOnly = True
            TabOrder = 2
            Text = '6'
          end
          object edInitialStepSize: TEdit
            Left = 239
            Top = 226
            Width = 97
            Height = 21
            TabOrder = 5
            Text = '10'
          end
          object edRayEndAngle: TEdit
            Left = 402
            Top = 183
            Width = 97
            Height = 21
            TabOrder = 4
            Text = '0'
          end
          object edRayStartAngle: TEdit
            Left = 241
            Top = 183
            Width = 97
            Height = 21
            TabOrder = 3
            Text = '0'
          end
          object rgRKMethod: TRadioGroup
            Left = 11
            Top = 280
            Width = 238
            Height = 105
            Caption = 'Runge-Kutta Method'
            ItemIndex = 0
            Items.Strings = (
              'Choose based on SSP'
              '2nd order'
              '4th order')
            TabOrder = 8
            OnClick = rgRKMethodClick
          end
          object udDBSeaMaxBottomReflections: TUpDown
            Left = 338
            Top = 148
            Width = 17
            Height = 21
            Associate = edDBSeaRayMaxBottomReflections
            Min = 1
            Max = 1000000
            Position = 6
            TabOrder = 9
            Thousands = False
          end
          object edBellhopNRaysAzimuth: TEdit
            Left = 632
            Top = 113
            Width = 86
            Height = 21
            NumbersOnly = True
            TabOrder = 1
            Text = '0'
          end
        end
      end
      object cbExclusionZoneStopAtLand: TCheckBox
        Left = 350
        Top = 36
        Width = 290
        Height = 17
        TabStop = False
        Caption = 'Stop exclusion zone upon reaching land (2D only)'
        TabOrder = 11
        OnClick = cbExclusionZoneStopAtLandClick
      end
    end
    object tsMultiCore: TTabSheet
      Caption = 'Multi core'
      ImageIndex = 7
      object lbNumberOfProcessors: TLabel
        Left = 80
        Top = 204
        Width = 138
        Height = 13
        Caption = 'Number of processors to use'
      end
      object cbMulticorePE: TCheckBox
        Left = 80
        Top = 64
        Width = 200
        Height = 17
        Caption = 'Use multicore for dBSeaPE'
        TabOrder = 0
      end
      object cbMulticoreModes: TCheckBox
        Left = 80
        Top = 97
        Width = 200
        Height = 17
        Caption = 'Use multicore for dBSeaModes'
        TabOrder = 1
      end
      object cbMulticoreRay: TCheckBox
        Left = 80
        Top = 129
        Width = 200
        Height = 17
        Caption = 'Use multicore for dBSeaRay'
        TabOrder = 2
      end
      object sbNumberOfProcessors: TScrollBar
        Left = 80
        Top = 232
        Width = 169
        Height = 17
        Min = 1
        PageSize = 0
        Position = 1
        TabOrder = 3
        OnChange = sbNumberOfProcessorsChange
      end
    end
    object tsMisc: TTabSheet
      Caption = 'Misc.'
      ImageIndex = 3
      object lblProjectName: TLabel
        Left = 38
        Top = 29
        Width = 103
        Height = 13
        Caption = 'Current project name'
      end
      object lblTempDir: TLabel
        Left = 40
        Top = 334
        Width = 138
        Height = 13
        Caption = 'Temporary working directory'
      end
      object lblDefaultSaveDir: TLabel
        Left = 40
        Top = 261
        Width = 101
        Height = 13
        Caption = 'Default save location'
        Visible = False
      end
      object lblBathymetryfile: TLabel
        Left = 40
        Top = 397
        Width = 113
        Height = 13
        Caption = 'Current bathymetry file'
      end
      object rgFeetMeters: TRadioGroup
        Left = 38
        Top = 99
        Width = 185
        Height = 62
        Caption = 'Units'
        ItemIndex = 0
        Items.Strings = (
          'Metric'
          'Imperial')
        TabOrder = 0
        OnClick = rgFeetMetersClick
      end
      object editProjectName: TEdit
        Left = 38
        Top = 56
        Width = 449
        Height = 21
        TabOrder = 1
        Text = 'editProjectName'
        OnChange = editProjectNameChange
        OnEnter = editProjectNameEnter
      end
      object btAssociateUWA: TButton
        Left = 38
        Top = 200
        Width = 185
        Height = 25
        Caption = 'Open .uwa files with dBSea'
        TabOrder = 2
        OnClick = btAssociateUWAClick
      end
      object btBrowseDefaultSaveDir: TButton
        Left = 495
        Top = 278
        Width = 75
        Height = 25
        Caption = 'Browse'
        TabOrder = 3
        Visible = False
        OnClick = btBrowseDefaultSaveDirClick
      end
      object editTempDir: TEdit
        Left = 40
        Top = 350
        Width = 449
        Height = 21
        TabOrder = 4
        Text = 'editTempDir'
      end
      object editBathymetryFile: TEdit
        Left = 40
        Top = 416
        Width = 449
        Height = 21
        ReadOnly = True
        TabOrder = 5
        Text = ' '
      end
      object btBrowseTempDir: TButton
        Left = 495
        Top = 348
        Width = 75
        Height = 25
        Caption = 'Browse'
        TabOrder = 6
        OnClick = btBrowseTempDirClick
      end
      object editDefaultSave: TEdit
        Left = 40
        Top = 280
        Width = 449
        Height = 21
        TabOrder = 7
        Text = 'editDefaultSave'
        Visible = False
      end
    end
    object tsExport: TTabSheet
      Caption = 'Exporting'
      ImageIndex = 4
      object lblImageToAttach: TLabel
        Left = 16
        Top = 37
        Width = 149
        Height = 13
        Caption = 'Logo image to include in export'
      end
      object imLogo: TImage
        Left = 455
        Top = 37
        Width = 250
        Height = 250
      end
      object lblExportText: TLabel
        Left = 16
        Top = 126
        Width = 117
        Height = 13
        Caption = 'Text to include in export'
      end
      object btBrowseLogo: TButton
        Left = 350
        Top = 83
        Width = 75
        Height = 25
        Caption = 'Browse'
        TabOrder = 0
        OnClick = btBrowseLogoClick
      end
      object edLogoImage: TEdit
        Left = 16
        Top = 56
        Width = 409
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object mmExportText: TMemo
        Left = 16
        Top = 156
        Width = 280
        Height = 181
        TabOrder = 2
        OnChange = mmExportTextChange
      end
      object rgExportDelimiter: TRadioGroup
        Left = 16
        Top = 362
        Width = 280
        Height = 145
        Caption = 'Delimiter to use in CSV export'
        Color = clWhite
        Items.Strings = (
          ',  comma (ASCII 44)'
          ';  semicolon (ASCII 59)'
          'tab (ASCII 09)'
          'space (ASCII 32)')
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        OnClick = rgExportDelimiterClick
      end
    end
    object tsLevels: TTabSheet
      Caption = 'Sound levels display'
      ImageIndex = 6
      object lbAssessmentPeriod: TLabel
        Left = 38
        Top = 299
        Width = 158
        Height = 13
        Caption = 'Assessment period for results (s)'
      end
      object lbAssessmentPeriodMovingSourceHint: TLabel
        Left = 38
        Top = 272
        Width = 513
        Height = 13
        Caption = 
          'For scenarios with moving sources, the assessment period is equa' +
          'l to the time of the longest source motion'
      end
      object rgLevelType: TRadioGroup
        Left = 38
        Top = 61
        Width = 371
        Height = 111
        Caption = 'Level type for display'
        ItemIndex = 0
        Items.Strings = (
          'Sound pressure level    dB SPL'
          'Sound exposure level    dB SELcum @ T0 = 1s'
          'Peak sound pressure level    dBpeak')
        TabOrder = 0
        OnClick = rgLevelTypeClick
      end
      object edAssessmentPeriod: TEdit
        Left = 223
        Top = 296
        Width = 121
        Height = 21
        TabOrder = 1
        Text = '3600'
      end
      object btSetAssessmentPeriodTo: TButton
        Left = 374
        Top = 294
        Width = 75
        Height = 25
        Caption = 'Set to...'
        PopupMenu = puAssessmentPeriod
        TabOrder = 2
        OnClick = btSetAssessmentPeriodToClick
      end
      object mmFreqDomainPeakLevelWarning: TMemo
        Left = 38
        Top = 178
        Width = 569
        Height = 63
        BorderStyle = bsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          
            'Peak sound power level for frequency domain (NOAA non-impulsive)' +
            ' calculations is strongly discouraged. '
          
            'Using this option implies that you have a full understanding of ' +
            'how source crest factor, count and duty '
          
            'are applied, and how results are calculated in the case of multi' +
            'ple or moving sources. '
          
            'No thresholds are given for this calculation type in NOAA guidan' +
            'ce as of July 2016')
        ParentFont = False
        TabOrder = 3
      end
    end
  end
  object puAssessmentPeriod: TPopupMenu
    Left = 48
    Top = 688
    object mi1Second: TMenuItem
      Caption = '1 second'
      OnClick = mi1SecondClick
    end
    object mi1Hour: TMenuItem
      Caption = '1 hour'
      OnClick = bt1HourClick
    end
    object mi24Hours: TMenuItem
      Caption = '24 hours'
      OnClick = bt24HourClick
    end
  end
  object odLogo: TOpenDialog
    Filter = 'Image Files|*.jpg;*.png;*.bmp;*.gif|All files|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Choose image'
    Left = 104
    Top = 688
  end
end
