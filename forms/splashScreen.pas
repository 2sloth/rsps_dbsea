unit splashScreen;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls;

type
  TSplashScreenForm = class(TForm)
    imStandard: TImage;
    imLite: TImage;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  theSplashScreen: TSplashScreenForm;

implementation

{$R *.dfm}



procedure TSplashScreenForm.FormCreate(Sender: TObject);
begin
    {$IFDEF LITE}
    imLITE.Visible := true;
    {$ELSE}
    imLITE.Visible := false;
    {$ENDIF}
    imStandard.Visible := not imLite.Visible;
end;

end.
