object CSVDelimitersForm: TCSVDelimitersForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Read CSV File'
  ClientHeight = 435
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    464
    435)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 141
    Width = 99
    Height = 13
    Caption = 'Sample text from file'
  end
  object rgDecimalSeparator: TRadioGroup
    Left = 16
    Top = 24
    Width = 185
    Height = 65
    Caption = 'Choose decimal separator'
    Enabled = False
    ItemIndex = 0
    Items.Strings = (
      '.'
      ',')
    TabOrder = 0
    OnClick = rgDecimalSeparatorClick
  end
  object rgFieldDelimiter: TRadioGroup
    Left = 232
    Top = 24
    Width = 185
    Height = 113
    Caption = 'Choose field delimiter'
    ItemIndex = 0
    Items.Strings = (
      ';'
      ','
      'Tab'
      'Space')
    TabOrder = 1
    OnClick = rgFieldDelimiterClick
  end
  object btCSVOK: TButton
    Left = 366
    Top = 391
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object mmSampleText: TMemo
    Left = 16
    Top = 168
    Width = 425
    Height = 209
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    ScrollBars = ssVertical
    TabOrder = 3
  end
end
