unit FishAddDBFrm;

interface

uses
  winapi.Windows, winapi.Messages, system.SysUtils, system.Variants,
  system.Classes, vcl.Graphics, vcl.Controls, vcl.Forms,
  vcl.Dialogs, baseTypes, vcl.StdCtrls, vcl.Grids, spectrum, helperfunctions,
  system.StrUtils, StringGridHelper;

type
  TFishAddToDB = class(TForm)
    sgWeighting: TStringGrid;
    btOK: TButton;
    btCancel: TButton;
    lblName: TLabel;
    EditName: TEdit;
    lblComments: TLabel;
    memoComments: TMemo;
    lblCategory: TLabel;
    EditCategory: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure sgWeightingSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
  public
    spectrum : TSpectrum;
    crestFactor : double;

    procedure init;
    procedure setupForFish;
    procedure setupForSource(const mitigation: boolean);
  end;

var
  FishAddToDB: TFishAddToDB;

implementation

{$R *.dfm}

procedure TFishAddToDB.FormCreate(Sender: TObject);
begin
    if self.Width > Screen.Width then
        self.Width := screen.Width - 10;
    sgWeighting.Width := self.ClientWidth - sgWeighting.Left - 25;
    spectrum := TSpectrum.Create;
    init;
end;

procedure TFishAddToDB.init;
var
    i : integer;
begin
    sgWeighting.clear;
    sgWeighting.RowCount := 2;
    sgWeighting.ColCount := TSpectrum.getLength(bwThirdOctave) + 1;
    sgWeighting.Cells[0,0] := 'Frequency';
    sgWeighting.Cells[0,1] := 'Weighting';
    for I := 0 to TSpectrum.getLength(bwThirdOctave) - 1 do
        sgWeighting.Cells[i + 1,0] := TSpectrum.getFStringbyInd(i,bwThirdOctave);

    crestFactor := 12;
    EditName.Text := '';
    memoComments.Text := '';
end;


procedure TFishAddToDB.FormDestroy(Sender: TObject);
begin
    spectrum.Free;
end;

procedure TFishAddToDB.sgWeightingSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
    {disallow editing on certain fields.}
    if (Arow = 0) or (ACol = 0) then
        sgWeighting.Options := sgWeighting.Options - [goEditing] + [goTabs]
    else
        sgWeighting.Options := sgWeighting.Options + [goEditing, goTabs, goAlwaysShowEditor];
end;

procedure TFishAddToDB.setupForFish;
begin
    init;
    self.lblCategory.Enabled := false;
    self.EditCategory.Text := 'Fish';
    self.EditCategory.Visible := false;
    self.lblCategory.Visible := false;
end;

procedure TFishAddToDB.setupForSource(const mitigation: boolean);
begin
    init;
    self.lblCategory.Enabled := true;
    self.EditCategory.Text := 'Other';
    self.EditCategory.Visible := true;
    self.lblCategory.Visible := true;
end;


end.
