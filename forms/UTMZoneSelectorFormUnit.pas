unit UTMZoneSelectorFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  ImportedLocationUnit, latlong2UTM;

type
  TUTMZoneSelectorForm = class(TForm)
    edZone: TEdit;
    btOK: TButton;
    btCancel: TButton;
    lbZone: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btOKClick(Sender: TObject);
  private
  public
  end;

var
  UTMZoneSelectorForm: TUTMZoneSelectorForm;

implementation

{$R *.dfm}

procedure TUTMZoneSelectorForm.btOKClick(Sender: TObject);
begin
    importedLocation.setZoneWith(edZone.Text);
end;

procedure TUTMZoneSelectorForm.FormShow(Sender: TObject);
begin
    if importedLocation.hasUTMData then
        edZone.Text := inttostr( importedLocation.utmLongZone) + UpperCase(importedLocation.utmLatZone)
    else
        edZone.Text := '';
end;

end.
