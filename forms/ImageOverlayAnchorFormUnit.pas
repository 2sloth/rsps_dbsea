﻿unit ImageOverlayAnchorFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  system.math, system.UITypes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  imageOverlay, helperFunctions, SmartPointer;

type
    TImageAnchorState = (kSetP1, kSetP2);

  TImageOverlayAnchorForm = class(TForm)
    im: TImage;
    btLoad: TButton;
    btOK: TButton;
    btCancel: TButton;
    edWest: TEdit;
    edSouth: TEdit;
    edEast: TEdit;
    edNorth: TEdit;
    lbx1: TLabel;
    lbx2: TLabel;
    lby1: TLabel;
    lby2: TLabel;
    odImage: TOpenDialog;
    tmAnimation: TTimer;
    btClearImage: TButton;
    btResetScale: TButton;
    cbAspectRatioLock: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure btLoadClick(Sender: TObject);
    procedure tmAnimationTimer(Sender: TObject);
    procedure imMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure edXYChange(Sender: TObject);
    procedure btClearImageClick(Sender: TObject);
    procedure btResetScaleClick(Sender: TObject);
    procedure cbAspectRatioLockClick(Sender: TObject);
    procedure btOKClick(Sender: TObject);
  private
  const
    radius = 5;
  var
    updateFromCode: boolean;
    state: TImageAnchorState;
    selectedRadius: integer;
    procedure updateImage;
    //procedure plotPoint(const im: TImage; const x,y: integer; const color: integer; const selected: boolean);
    procedure editBoxesInit;
  public
    image: TImageOverlay;
  end;

var
  ImageOverlayAnchorForm: TImageOverlayAnchorForm;

implementation

{$R *.dfm}

uses problemContainer;

procedure TImageOverlayAnchorForm.btClearImageClick(Sender: TObject);
begin
    self.image.clearImage;
    self.updateImage;
end;

procedure TImageOverlayAnchorForm.btLoadClick(Sender: TObject);
begin
    if not odImage.Execute then exit;

    self.image.setImageHasWorldFile(odImage.FileName);
    self.editBoxesInit;
    self.updateImage;
end;

procedure TImageOverlayAnchorForm.btOKClick(Sender: TObject);
begin
    if isFloat(edWest.Text) and isFloat(edEast.Text)
        and (tofl(edEast.Text) <= tofl(edWest.Text)) then
    begin
        if (mrYes = MessageDlg('The eastern and western edges are reversed. Do you want to proceed?', mtConfirmation,[mbYes,mbNo],0)) then
        self.ModalResult := mrOk
    end
    else if isFloat(edNorth.Text) and isFloat(edSouth.Text)
        and (tofl(edNorth.Text) <= tofl(edSouth.Text)) then
    begin
        if (mrYes = MessageDlg('The northern and southern edges are reversed. Do you want to proceed?', mtConfirmation,[mbYes,mbNo],0)) then
        self.ModalResult := mrOk;
    end
    else
        self.ModalResult := mrOk;
end;

procedure TImageOverlayAnchorForm.btResetScaleClick(Sender: TObject);
begin
    self.image.resetScale;
end;

procedure TImageOverlayAnchorForm.cbAspectRatioLockClick(Sender: TObject);
begin
    self.image.lockAspectRatio := self.cbAspectRatioLock.Checked;
end;

procedure TImageOverlayAnchorForm.edXYChange(Sender: TObject);
begin
    if updateFromCode then exit;

    if isFloat(edWest.Text) then
        self.image.anchorWorldX1 := tofl(edWest.Text) - container.geoSetup.Easting;

    if isFloat(edSouth.Text) then
        self.image.anchorWorldY1 := tofl(edSouth.Text) - container.geoSetup.Northing;

    if isFloat(edEast.Text) then
        self.image.anchorWorldX2 := tofl(edEast.Text) - container.geoSetup.Easting;

    if isFloat(edNorth.Text) then
        self.image.anchorWorldY2 := tofl(edNorth.Text) - container.geoSetup.Northing;
end;

procedure TImageOverlayAnchorForm.FormCreate(Sender: TObject);
begin
    inherited;

    self.image := TImageOverlay.create(container);
end;

procedure TImageOverlayAnchorForm.FormDestroy(Sender: TObject);
begin
    self.image.Free;

    inherited;
end;

procedure TImageOverlayAnchorForm.editBoxesInit;
begin
    updateFromCode := true;

    self.edWest.Text := safefloattostrf(self.image.anchorWorldX1 + container.geoSetup.Easting, ffFixed, 14, 0, '');
    self.edSouth.Text := safefloattostrf(self.image.anchorWorldY1 + container.geoSetup.Northing, ffFixed, 14, 0, '');
    self.edEast.Text := safefloattostrf(self.image.anchorWorldX2 + container.geoSetup.Easting, ffFixed, 14, 0, '');
    self.edNorth.Text := safefloattostrf(self.image.anchorWorldY2 + container.geoSetup.Northing, ffFixed, 14, 0, '');
    self.cbAspectRatioLock.Checked := self.image.lockAspectRatio;

    updateFromCode := false;
end;

procedure TImageOverlayAnchorForm.FormShow(Sender: TObject);
begin
    updateFromCode := true;
    self.state := kSetP1;

    editBoxesInit;
    selectedRadius := 8;
    self.updateImage;

    updateFromCode := false;
end;

procedure TImageOverlayAnchorForm.imMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
//var
//    xx,yy: double;
//    largerDim: integer;
begin
//    if (length(self.image.image) = 0) or (not self.image.width.HasValue or not self.image.height.HasValue) then exit;
//
//    largerDim := max( self.image.width.val, self.image.height.val);
//    xx := x / self.im.Width  * largerDim / self.image.width.val;
//    yy := y / self.im.height * largerDim / self.image.height.val;
//
//    if (xx < 0) or (xx > 1) or (yy < 0) or (yy > 1) then
//    begin
//        showmessage('Cannot set point outside image bounds');
//        exit;
//    end;
//
//    case self.state of
//        kSetP1: begin
//            self.image.anchorImageX1 := xx;
//            self.image.anchorImageY1 := yy;
//            self.state := kSetP2;
//        end;
//        kSetP2: begin
//            self.image.anchorImageX2 := xx;
//            self.image.anchorImageY2 := yy;
//            self.state := kSetP1;
//        end;
//    end;
//
//    self.updateImage;
end;

procedure TImageOverlayAnchorForm.tmAnimationTimer(Sender: TObject);
begin
    if updateFromCode then exit;

    inc(selectedRadius);
    if selectedRadius >= 14 then
        selectedRadius := 6;

    self.updateImage;
end;

//procedure TImageOverlayAnchorForm.plotPoint(const im: TImage; const x,y: integer; const color: integer; const selected: boolean);
//begin
//    im.Canvas.Pen.Color := color;
//    im.Canvas.Pen.Width := 1;
//    im.Canvas.Pen.Style := psSolid;
//
//    im.Canvas.Brush.Style := bsSolid;
//    im.Canvas.Brush.Color := color;
//    im.Canvas.Ellipse(x - radius,y - radius,x + radius,y + radius);
//
//    if selected then
//    begin
//        im.Canvas.Brush.Style := bsClear;
//        im.Canvas.Ellipse(x - selectedRadius,y - selectedRadius,x + selectedRadius,y + selectedRadius);
//    end;
//end;

procedure TImageOverlayAnchorForm.updateImage;
var
    bmp: TBitmap;
    r: TRect;
    largerDim: integer;
begin
    if length(self.image.image) = 0 then
    begin
        im.Canvas.Brush.Color := clBtnFace;
        im.Canvas.Rectangle(0,0,im.Width,im.Height);
        exit;
    end;

    bmp := self.image.toBMP;
    try
        r.left := 0;
        r.top := 0;
        largerDim := max(bmp.width, bmp.height);
        r.right := round(bmp.width / largerDim * im.Width);
        r.bottom := round(bmp.height / largerDim * im.Height);
        im.Canvas.stretchDraw(r, bmp);

//        if self.image.p1ImageValid then
//        begin
//            x := round(image.anchorImageX1 * r.right);
//            y := round(image.anchorImageY1 * r.bottom);
//            self.plotPoint(self.im, x,y, clRed, self.state = kSetP1);
//        end;
//
//        if self.image.p2ImageValid then
//        begin
//            x := round(image.anchorImageX2 * r.right);
//            y := round(image.anchorImageY2 * r.bottom);
//            self.plotPoint(self.im, x,y, clPurple, self.state = kSetP2);
//        end;
    finally
        bmp.free;
    end;
end;

end.
