{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit equipDBFrm;

interface

uses
    syncommons,
    system.SysUtils, system.Variants, system.Classes, system.math, system.UITypes,
  winapi.Windows, winapi.Messages,
  vcl.Graphics, vcl.Controls, vcl.Forms,
  vcl.ComCtrls, vcl.StdCtrls, vcl.Grids, vcl.ExtCtrls,
  vcl.Dialogs, Vcl.Menus,
  vcl.imaging.jpeg,
  VCLTee.Series, VCLTee.Chart, VCLTee.TeEngine, VclTee.TeeGDIPlus, VCLTee.TeeProcs,
  SQLiteTable3, sourceSpectFrm, baseTypes, ImgList,
  FishAddDBFrm, StrUtils, dBSeaColours, spectrum,
  helperFunctions, dBSeaconstants, equipReturn, levelConverter, types,
  eventLogging, globalsUnit, problemContainer,
  PasswordChecking, HASPSecurity, StringGridHelper, SmartPointer;

type

  TEquipDBForm = class(TForm)
    btEquipOK: TButton;
    btEquipCancel: TButton;
    tvEquip: TTreeView;
    EquipImages: TImageList;
    grdSelectedEquip: TStringGrid;
    chEquipSelect: TChart;
    srEquipSelect: TBarSeries;
    mmComments: TMemo;
    lblComments: TLabel;
    cbMDADB: TCheckBox;
    cbUserDB: TCheckBox;
    edSearch: TEdit;
    lblSearch: TLabel;
    imDBEntryImage: TImage;
    btInsertIntoDB: TButton;
    lblDataSource: TLabel;
    btRemoveFromDB: TButton;
    btEditDB: TButton;
    gbEquipment: TGroupBox;
    rgLevelType: TRadioGroup;
    edAssessmentPeriod: TEdit;
    lbAssessmentPeriod: TLabel;
    puAssessmentPeriod: TPopupMenu;
    mi1Hour: TMenuItem;
    mi24Hours: TMenuItem;
    btSetAssessmentPeriodTo: TButton;
    mi1Second: TMenuItem;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure tvEquipChange(Sender: TObject; Node: TTreeNode);
    procedure tvEquipCreateNodeClass(Sender: TCustomTreeView; var NodeClass: TTreeNodeClass);

    procedure edSearchChange(Sender: TObject);
    procedure grdSelectedEquipDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);

    procedure cbMDADBClick(Sender: TObject);
    procedure cbUserDBClick(Sender: TObject);
    procedure btInsertIntoDBClick(Sender: TObject);
    procedure btRemoveFromDBClick(Sender: TObject);
    procedure btEditDBClick(Sender: TObject);
    procedure rgLevelTypeClick(Sender: TObject);
    procedure edAssessmentPeriodChange(Sender: TObject);
    procedure bt24HourClick(Sender: TObject);
    procedure bt1HourClick(Sender: TObject);
    procedure btSetAssessmentPeriodToClick(Sender: TObject);
    procedure mi1SecondClick(Sender: TObject);

  private
    procedure TreeviewInit(const srch : String);
    procedure SQLInsertFromFishAdd();
    procedure updateChartAndTable;
  public
    wantMitigation  : boolean;
    equipReturn     : TEquipReturn;
    levType         : TSourceLevelType;
  const
    iNameCol        : integer = 0;
    iCategoryCol    : integer = 1;
    iLevCol         : integer = 2;
    fioffsetCol     : integer = 3;
  end;

  //subclass TTreeNode so we can add some more info to it, relevant to the DB entries
  TMyTreeNode = class(TTreeNode)
  public
    //anything that will be passed back to parent forms and sources is in equipReturn
    equipReturn : TEquipReturn;

    //anything else we get from the DB
    Bandwidth   : TBandWidth; //not passed back, as equipReturn.spectrum is third octave internally regardless of DB bandwidth
    Category2,Comments : string;
    Level,RefDistance,RefSourceDepth,DepthUnderRefSource,baseFreq : double;
    ThirdOctaveLevelsSerial,OctaveLevelsSerial : string;

    constructor Create(AOwner: TTreeNodes); override;
    destructor Destroy; override;

    procedure spectrumFromSerialisedLevels;
  end;

var
    EquipDBForm: TEquipDBForm;

implementation

{$R *.dfm}
    {$B-}

//{$DEFINE SHOW_IMAGE}

uses main;

procedure TEquipDBForm.FormCreate(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'FormCreate');
    try
        inherited;

        wantMitigation := false;

        levType := kSL;

        equipReturn := TEquipReturn.Create;

        if not MDASldb.TableExists(equipTblName) then
            raise Exception.Create('Error: Default database contains no table ' + equipTblName);
        if not UserSldb.TableExists(equipTblName) then
            raise Exception.Create('Error: User database contains no table ' + equipTblName);

        grdSelectedEquip.ColWidths[0] := 100;   //make that 'name' col wider

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;


procedure TEquipDBForm.FormDestroy(Sender: TObject);
begin
    equipReturn.Free;
end;

procedure TEquipDBForm.FormShow(Sender: TObject);
var
    node   : TMyTreeNode;
begin
    TreeviewInit(edSearch.Text);

    {go through the tree and see if any node id matches the previously set id
    if so, expand the tree to there}
    node := TMyTreeNode(tvEquip.Items.GetFirstNode);

    {NB: equipID = -1 means not set}
    if equipReturn.equipID >= 0 then
    begin
        while (node <> nil) and not ((node.equipReturn.equipID = equipReturn.equipID) and (node.equipReturn.DataSource = equipReturn.dataSource)) do
            node := TMyTreeNode(node.GetNext);

        {if we found the correct node, we stopped at it and N <> nil}
        if (node <> nil) and (node.Parent <> nil) then
        begin
            node.Expand(false);   //expand(false) just expands this current node
            tvEquip.Select(node);
            tvEquip.HideSelection := false;
        end;
    end;

    case levType of
        kSL: rgLevelType.ItemIndex := 0;
        kSWL: rgLevelType.ItemIndex := 1;
        kSELfreqSrc: rgLevelType.ItemIndex := 2;
    end;

    self.btInsertIntoDB.Caption := ifthen(self.wantMitigation, 'New database mitigation', 'New database equipment');
    self.btEditDB.Caption := ifthen(self.wantMitigation, 'Edit current database mitigation', 'Edit current database equipment');
    self.btRemoveFromDB.Caption := ifthen(self.wantMitigation, 'Remove current database mitigation', 'Remove current database equipment');

    {$IFDEF SHOW_IMAGE}
    imDBEntryImage.Visible := true;
    {$ELSE}
    imDBEntryImage.Visible := false;
    {$ENDIF}
end;

procedure TEquipDBForm.grdSelectedEquipDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
    {change the font/colour for certain cells in the grid}
    with TStringGrid(Sender) do
    begin
        {header row with background}
        if (ARow = 0) then
            Canvas.Brush.Color := TDBSeaColours.headerRow;

        {levels column with background (but not header row)}
        if (ACol = iLevCol) and (ARow > 0) then
            Canvas.Brush.Color := TDBSeaColours.levelsColumn;

        {$IFDEF VER280}
        if DefaultDrawing then
            Rect.Left := Rect.Left - 4;
        Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+5, Cells[ACol, ARow]);
        {$ELSE}
        Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
        {$ENDIF}
    end;
end;

procedure TEquipDBForm.rgLevelTypeClick(Sender: TObject);
begin
    case rgLevelType.ItemIndex of
        0 : levType := kSL;
        1 : levType := kSWL;
        2 : levType := kSELfreqSrc;
    end;
    updateChartAndTable;
end;

procedure TEquipDBForm.mi1SecondClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '1';
end;

procedure TEquipDBForm.bt1HourClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '3600';
end;

procedure TEquipDBForm.bt24HourClick(Sender: TObject);
begin
    edAssessmentPeriod.Text := '86400';
end;

procedure TEquipDBForm.btEditDBClick(Sender: TObject);
var
    sWeighting   : string;
    sSQL        : String;
    len, fi     : integer;
    node        : TMyTreeNode;
    sl          : ISmart<TStringlist>;
    baseFreq    : double;
    tableName   : string;
begin
    sl := TSmart<TStringlist>.create(TStringList.Create());
    node := TMyTreeNode(tvEquip.Selected);
    if node <> nil then
    begin
        if node.Bandwidth.oct then
            baseFreq := BaseFreqOctave
        else
            baseFreq := BaseFreqThirdOctave;

        len := TSpectrum.getLength(node.Bandwidth);

        fishAddToDB.setupForSource(self.wantMitigation);
        fishAddToDB.EditName.Text := node.equipReturn.Name;
        fishAddToDB.EditCategory.Text := node.equipReturn.category;
        fishAddToDB.memoComments.Text := node.Comments;
        //fishAddToDB.edCrestFactor.Text := tostr(node.equipReturn.crestFactordB);

        {get the serialised levels out of the DB and insert them into the stringlist for reading}
        if node.Bandwidth.oct then
            sl.CommaText := node.OctaveLevelsSerial //sltb.FieldAsString(sltb.FieldIndex[levelsLabel]);
        else
            sl.CommaText := node.ThirdOctaveLevelsSerial;
        if (node.baseFreq <> baseFreq) then
        begin
            showmessage('DB error: Unexpected base frequency');
            eventLog.log('DB error: Unexpected base frequency');
            Exit;
        end;
        if (sl.Count <> len) then
        begin
            showmessage('DB error: Expected ' + tostr(len) + ' comma delimited level values, received ' + tostr(sl.Count));
            eventLog.log('DB error: Expected ' + tostr(len) + ' comma delimited level values, received ' + tostr(sl.Count));
            Exit;
        end;
        for fi := 0 to len - 1 do
            fishAddToDB.sgWeighting.Cells[fi + 1,1] := ifthen(isFloat(sl[fi]),sl[fi],'');

        if fishAddToDB.ShowModal = mrOK then
        begin
            {get user data back from the form and do an sql update}
            sWeighting := '';
            for fi := 0 to len - 1 do
                sWeighting := sWeighting + ifthen(isFloat(fishAddToDB.sgWeighting.Cells[fi + 1,1]),forcedotseparator(fishAddToDB.sgWeighting.Cells[fi + 1,1]) + ',','NaN,');
            sWeighting := leftStr(sWeighting,length(sWeighting) - 1); //strip out last comma

            if self.wantMitigation then
                tableName := 'mitigationtable'
            else
                tableName := equipTblName;

            {create the sql string, and execute. we are adding the user data into the user database. note that some fields
            are numeric.}
            userSldb.BeginTransaction;
            sSQL := 'UPDATE ' + tableName + ' SET Name="' + fishAddToDB.EditName.Text +'",';
            sSQL := sSQL + 'Category1="' + fishAddToDB.EditCategory.Text + '",';
            sSQL := sSQL + 'Comments="' + fishAddToDB.memoComments.Text + '",';
            sSQL := sSQL + 'ThirdOctaveLevelsSerial="' + sWeighting + '",';
            sSQL := sSQL + 'crestFactorDB=' + forcedotseparator(tostr(fishAddToDB.crestFactor)) + ' ';
            sSQL := sSQL + ' WHERE ID=' + tostr(node.equipReturn.equipID) + ';';
            userSldb.ExecSQLUnicode(sSQL);
            userSldb.Commit;

            {clear search text and update the tree}
            edSearch.Text := '';
            mmComments.Text := '';
            TreeviewInit(edSearch.Text);
        end;
    end;
end;

procedure TEquipDBForm.btInsertIntoDBClick(Sender: TObject);
begin
    fishAddToDB.setupForSource(self.wantMitigation);
    if FishAddToDB.ShowModal = mrOK then
    begin
        SQLInsertFromFishAdd();

        {clear search text and update the tree}
        edSearch.Text := '';
        TreeviewInit(edSearch.Text);
    end;
end;

procedure TEquipDBForm.SQLInsertFromFishAdd();
var
    sWeighting  : string;
    i           : integer;
    sSQL        : String;
    iNewID      : integer;
    node        : TMyTreeNode;
    category    : string;
    tableName   : string;
begin
    {get user data back from the form and do an sql insert}
    sWeighting := '';
    for I := 0 to FishAddToDB.sgWeighting.colCount - 2 do
        sWeighting := sWeighting + ifthen(isFloat(FishAddToDB.sgWeighting.Cells[i + 1,1]),
                               forceDotSeparator(FishAddToDB.sgWeighting.Cells[i + 1,1]) + ',','NaN,');
    sWeighting := leftStr(sWeighting,length(sWeighting) - 1); //strip out last comma

    {get current table IDs so we can add an ID integer. The ID will be higher than all the other IDs}
    iNewID := 1;
    node := TMyTreeNode(tvEquip.Items[0]);
    while node <> nil do
    begin
        if node.Parent <> Nil then
            if node.equipReturn.DataSource = TDataSource.dsUser then
                if node.equipReturn.equipID >= iNewID then
                    iNewID := node.equipReturn.equipID + 1;
        node := TMyTreeNode(node.GetNext);
    end;

    if self.wantMitigation then
    begin
        category := 'Mitigation';
        tableName := 'mitigationtable';
    end
    else
    begin
        category := FishAddToDB.editCategory.text;
        tableName := equipTblName;
    end;

    {create the sql string, and execute. we are adding the user data into the user database. note that some fields
    are numeric.}
    userSldb.BeginTransaction; //begin a transaction
    //NB this inserts the data as 'User', not 'MDA'
    sSQL := 'INSERT INTO ' + tableName + ' (ID,Datasource,Name,Category1,';
    sSQL := sSQL + 'Category2,Bandwidth,Comments,RefDistance,ThirdOctaveLevelsSerial,baseFreq,crestFactorDB';
    sSQL := sSQL + ') VALUES (' + tostr(iNewID) + ',"' + sUserDB + '","' + FishAddToDB.EditName.Text + '",';
    sSQL := sSQL + '"' + category + '","","ThirdOctave"';
    sSQL := sSQL + ', "' + FishAddToDB.memoComments.Text + '",1,"' + sWeighting + '"';
    sSQL := sSQL + ',' + forceDotSeparator( tostr(BaseFreqThirdOctave)) + ',' + forceDotSeparator(tostr(FishAddToDB.crestFactor)) + ');';
    userSldb.ExecSQLUnicode(sSQL);     //do the insert
    userSldb.Commit;   //end the transaction
end;

procedure TEquipDBForm.btRemoveFromDBClick(Sender: TObject);
var
    buttonSelected : Integer;
    sSQL        : String;
    node        : TMyTreeNode;
    tableName   : string;
begin
    node := TMyTreeNode(tvEquip.Selected);

    if node = nil then Exit;

    if node.equipReturn.DataSource = TDataSource.dsUser then
    begin
        {ask the user if they really want to delete the selected item}
        buttonSelected := MessageDlg('Really delete item ' + node.Text + '?',
                                     mtConfirmation,
                                     mbOKCancel,
                                     0);

        {if they do, then do the SQL Delete}
        if buttonSelected = mrOK then
        begin
            if self.wantMitigation then
                tableName := 'mitigationtable'
            else
                tableName := equipTblName;

            userSldb.BeginTransaction; //begin a transaction
            sSQL := 'DELETE FROM ' + tableName + ' WHERE ID=' + tostr(node.equipReturn.equipID) + ';';
            userSldb.ExecSQLUnicode(sSQL);     //do the delete
            userSldb.Commit;   //end the transaction

            {update the treeview}
            edSearch.Text := '';
            mmComments.Text := '';
            TreeviewInit(edSearch.Text);
        end;
    end;
end;

procedure TEquipDBForm.btSetAssessmentPeriodToClick(Sender: TObject);
var
    pnt: TPoint;
begin
    if GetCursorPos(pnt) then
        TButton(sender).PopupMenu.Popup(pnt.X, pnt.Y);
end;

procedure TEquipDBForm.cbMDADBClick(Sender: TObject);
begin
    TreeviewInit(edSearch.Text);
end;

procedure TEquipDBForm.cbUserDBClick(Sender: TObject);
begin
    TreeviewInit(edSearch.Text);
end;

procedure TEquipDBForm.edAssessmentPeriodChange(Sender: TObject);
begin
    if isFloat(edAssessmentPeriod.Text) then
        updateChartAndTable;
end;

procedure TEquipDBForm.edSearchChange(Sender: TObject);
begin
    TreeviewInit(edSearch.Text);
end;

procedure TEquipDBForm.TreeviewInit(const srch : String);
    type
        TTreeNodeArray = array of TTreeNode;
    const
        iMitigationNode     : integer = 0;
        iPilingNode         : integer = 0;
        iDredgingNode       : integer = 1;
        iVesselNode         : integer = 2;
        iTurbineNode        : integer = 3;
        iOtherNode          : integer = 4;

    function decryptEntry(const securityIndex: integer; const encrypted: string): String;
    begin
        if securityIndex < 0 then
            exit(encrypted)
        else
        begin
            case securityIndex of
            0: result := TPasswordChecking.DecodePWDEx(encrypted, DBSecurityString0);
            1: result := TPasswordChecking.DecodePWDEx(encrypted, DBSecurityString1);
            2: result := TPasswordChecking.DecodePWDEx(encrypted, DBSecurityString2);
            3: result := TPasswordChecking.DecodePWDEx(encrypted, DBSecurityString3);
            else result := '';
            end;
        end;
    end;

    procedure removeNodeImages();
    var
        node: TTreeNode;
    begin
        Node := tvEquip.Items[0];
        while Node <> nil do
        begin
            if Node.Parent <> Nil then
            begin
                node.ImageIndex := -1;
                node.SelectedIndex := -1;
            end;
            Node := Node.GetNext;
        end;
    end;

    procedure addEntries(const sltb: TSQLiteTable; const treenodes: TTreeNodeArray);
    var
        node            : TTreeNode;
        myNode          : TMyTreeNode;
        sDatasource     : string;
        aName           : string;
        securityIndex   : integer;
    begin
        sltb.MoveFirst;
        while not sltb.EOF do
        begin
            if sltb.hasField('Security') then
                securityIndex := sltb.FieldAsInteger(sltb.FieldIndex['Security'])
            else
                securityIndex := -1;
            if not theHASPSecurity.allowDBSecurityIndex(securityIndex) then continue;

            aName := sltb.FieldAsString(sltb.FieldIndex['Name']);
            if wantMitigation then
            begin
                {if mitigation, then there is only 1 top node}
                node := tvEquip.Items.AddChild(treeNodes[iMitigationNode],aName);
            end
            else
            begin
                //for each row in our database, add a child to the appropriate tree branch
                case THelper.caseOfText(sltb.FieldAsString(sltb.FieldIndex['Category1']),['Piling','Dredging','Vessel','Turbine']) of
                    0 : node := tvEquip.Items.AddChild(treeNodes[iPilingNode],aName);
                    1 : node := tvEquip.Items.AddChild(treeNodes[iDredgingNode],aName);
                    2 : node := tvEquip.Items.AddChild(treeNodes[iVesselNode],aName);
                    3 : node := tvEquip.Items.AddChild(treeNodes[iTurbineNode],aName);
                else
                    node := tvEquip.Items.AddChild(treeNodes[iOtherNode],aName);  //fallthrough
                end;
            end;

            myNode := TMyTreeNode(node);
            //write all the data from the DB to the TMyTreeNode
            myNode.equipReturn.Name      := aName;
            myNode.equipReturn.equipID := sltb.FieldAsInteger(sltb.FieldIndex['ID']);
            myNode.equipReturn.isMitigation := wantMitigation;
            sDatasource := sltb.FieldAsString(sltb.FieldIndex['DataSource']);
            {set the data source, so we know which database to look in for the graph data. nb this could
            go wrong if any of the entries in the db are mislabelled, however they should always be MDA
            for the default db, and User for the user db}
            if ansicomparetext(sDatasource,sUserDB) = 0 then
                myNode.equipReturn.DataSource := TDataSource.dsUser
            else
                myNode.equipReturn.DataSource := TDataSource.dsMDA;
            myNode.equipReturn.category := sltb.FieldAsString(sltb.FieldIndex['Category1']);
            myNode.Category2 := sltb.FieldAsString(sltb.FieldIndex['Category2']);
            myNode.Comments  := sltb.FieldAsString(sltb.FieldIndex['Comments']);
            //decrypt the spectra if necessary
            myNode.ThirdOctaveLevelsSerial := decryptEntry(securityIndex, sltb.FieldAsString(sltb.FieldIndex['ThirdOctaveLevelsSerial']));
            myNode.OctaveLevelsSerial := decryptEntry(securityIndex, sltb.FieldAsString(sltb.FieldIndex['OctaveLevelsSerial']));
            if not TSpectrum.bandwidthFromString(sltb.FieldAsString(sltb.FieldIndex['Bandwidth']), myNode.bandWidth) then
                showmessage('Bandwidth in database must be ''' + TSpectrum.bandwidthString(bwOctave) + ''' or ''' +
                            TSpectrum.bandwidthString(bwThirdOctave) + ''', check database entries');
            myNode.spectrumFromSerialisedLevels;
            //myNode.equipReturn.crestFactordB := sltb.FieldAsDouble(sltb.FieldIndex['crestFactorDB']);
            //if isnan(myNode.equipReturn.crestFactordB) then
            //    myNode.equipReturn.crestFactordB := 12;

            if sltb.FieldIsNull(sltb.FieldIndex['Level']) then
                myNode.Level := NaN
            else
                myNode.Level := sltb.FieldAsDouble(sltb.FieldIndex['Level']);

            if sltb.FieldIsNull(sltb.FieldIndex['RefDistance']) then
                myNode.RefDistance := NaN
            else
                myNode.RefDistance := sltb.FieldAsDouble(sltb.FieldIndex['RefDistance']);

            if sltb.FieldIsNull(sltb.FieldIndex['RefSourceDepth']) then
                myNode.RefSourceDepth := NaN
            else
                myNode.RefSourceDepth := sltb.FieldAsDouble(sltb.FieldIndex['RefSourceDepth']);

            if sltb.FieldIsNull(sltb.FieldIndex['DepthUnderRefSource']) then
                myNode.DepthUnderRefSource := NaN
            else
                myNode.DepthUnderRefSource := sltb.FieldAsDouble(sltb.FieldIndex['DepthUnderRefSource']);

            if sltb.FieldIsNull(sltb.FieldIndex['baseFreq']) then
                myNode.baseFreq := NaN
            else
                myNode.baseFreq := sltb.FieldAsDouble(sltb.FieldIndex['baseFreq']);

            sltb.Next;
        end;
    end;
var
    treeNodes                       : TTreeNodeArray;
    datasourceKey                   : string;
    sltbMDA, sltbUser               : ISmart<TSQLiteTable>;
    sSQL                            : string;
    thisTableName : string;
begin
    {if wantMitigation, then search the mitigation table. otherwise look in the equip table}
    thisTableName := ifthen(wantMitigation,mitigationTblName,equipTblName);

    if not MDASldb.TableExists(thisTableName) then
        raise Exception.Create('Error: Database contains no table ' + thisTableName);

    grdSelectedEquip.Cells[iNameCol,0] := 'Name';
    grdSelectedEquip.Cells[iCategoryCol,0] := 'Category';
    grdSelectedEquip.Cells[iLevCol,0] := 'Level';
    //grdSelectedEquip.Cells[iCrestFactorCol,0] := 'Crest factor (dB)';

    tvEquip.Items.Clear; // remove any existing nodes
    {create root nodes, and tell the nodes which images to look up
    imageindex is the unselected image, selectedindex is the image when selected}
    //TODO get some real images for these
    if wantMitigation then
    begin
        setlength(treeNodes,1);
        treeNodes[iMitigationNode]              := tvEquip.Items.Add(nil, 'Mitigation');
        treeNodes[iMitigationNode].ImageIndex   := 0;
        treeNodes[iMitigationNode].SelectedIndex:= 0;
    end
    else
    begin
        setlength(treeNodes,5); //remember to resize this if you add more nodes!
        treeNodes[iPilingNode]                  := tvEquip.Items.Add(nil, 'Piling'); { Add a root node }
        treeNodes[iPilingNode].ImageIndex       := 0;
        treeNodes[iPilingNode].SelectedIndex    := 0;
        treeNodes[iDredgingNode]                := tvEquip.Items.Add(nil, 'Dredging');
        treeNodes[iDredgingNode].ImageIndex     := 1;
        treeNodes[iDredgingNode].SelectedIndex  := 1;
        treeNodes[iVesselNode]                  := tvEquip.Items.Add(nil, 'Vessels');
        treeNodes[iVesselNode].ImageIndex       := 2;
        treeNodes[iVesselNode].SelectedIndex    := 2;
        treeNodes[iTurbineNode]                 := tvEquip.Items.Add(nil, 'Turbines');
        treeNodes[iTurbineNode].ImageIndex      := 3;
        treeNodes[iTurbineNode].SelectedIndex   := 3;
        treeNodes[iOtherNode]                   := tvEquip.Items.Add(nil, 'Other');
        treeNodes[iOtherNode].ImageIndex        := 4;
        treeNodes[iOtherNode].SelectedIndex     := 4;
    end;

    {create sql search terms, based on which 'libraries' we are searching}
    datasourceKey := '';
    if cbMDADB.Checked then
        datasourceKey := ifthen(cbUserDB.Checked,'''MDA'', ''' + sUserDB + '''','''MDA''')
    else
        datasourceKey := ifthen(cbUserDB.Checked,'''' + sUserDB + '''','');

    {search for entries, in the MDA and/or User databases, matching the search string
    (matched like %search%, so it can be anywhere in the entry}
    sSQL := 'SELECT * FROM ' + thisTableName + ' WHERE Datasource IN (' + datasourcekey + ') AND Name LIKE ''%' + srch + '%'' ORDER BY ID;';

    sltbMDA := TSmart<TSQLiteTable>.create(MDASldb.GetTable(ansistring(sSQL)));
    sltbUser := TSmart<TSQLiteTable>.create(userSlDb.GetTable(ansistring(sSQL)));
    addEntries(sltbMDA, treeNodes);
    addEntries(sltbUser, treeNodes);

    removeNodeImages();

    {once we start typing something, we should expand all the tree nodes. do the same
    for wantMitigation, as there will only be 1 topnode}
    if (srch <> '') or wantMitigation then
        tvEquip.FullExpand;

    {disable the remove equip button, as nothing is selected}
    btRemoveFromDB.Enabled := false;
    btEditDB.Enabled := false;
end;

procedure TEquipDBForm.updateChartAndTable;
const
    belowMin : double = 30;
    belowMinMitigation : double = 5;
var
    lenfi, fi, startfi,endfi   : integer;
    fStr     : String;
    probBW          : TBandwidth;
    Lev             : double;
    maxLev : double;
    minLev : double;
    levels     : TDoubleDynArray;
begin
    probBW := container.scenario.Bandwidth;
    lenfi := container.scenario.lenfi;
    startfi := container.scenario.startfi;
    endfi := container.scenario.endfi;

    grdSelectedEquip.clear;
    grdSelectedEquip.ColCount := lenfi + fioffsetCol;
    grdSelectedEquip.Cells[iNameCol,1] := equipReturn.Name; //sltb.FieldAsString(sltb.FieldIndex['Name']);
    grdSelectedEquip.ColWidths[iNameCol] := 100;   //make that 'name' col wider
    grdSelectedEquip.Cells[iCategoryCol,1] := equipReturn.category; //sltb.FieldAsString(sltb.FieldIndex['Category1']);
    srEquipSelect.Clear;
    srEquipSelect.ColorEachPoint := true;
    maxLev := nan;
    minLev := nan;
    chEquipSelect.LeftAxis.Automatic := true;

    //gather all the levels to plot in an array
    setlength(levels,lenfi);
    for fi := 0 to lenfi - 1 do
    begin
        {if wantmitigation, then changing between bandwidths is a little different.
        Levels should not be summed into bands using the normal formula.
        also, there is no sense in showing the overall level.
        the formula is:
        - octave to 1/3 oct: just copy the value to all 3 bands
        - 1/3 oct to oct : attenuation = 10*log10(3/sum(1/10^(0.1*band[i]))), sum over 3 bands}
        if wantmitigation then
            levels[fi] := equipReturn.spectrum.getMitigationAmpbyInd(fi + startfi,probBW)
        else {not wantmitigation}
        begin
            levels[fi] := equipReturn.spectrum.getAmpbyInd(fi + startfi,probBW);
            //do conversion to appropriate levelType
            levels[fi] := TLevelConverter.convertSourceLevel(levels[fi], kSL, levType, tryTofl(edAssessmentPeriod.Text) );
        end;

        if not isNaN(levels[fi]) then
            lesserGreater( minLev, maxLev, levels[fi] );
    end;

    //do plotting and fill table with string values
    for fi := 0 to lenfi - 1 do
    begin
        Lev := levels[fi];

        fStr := TSpectrum.getFStringbyInd(fi + startfi,probBW);
        if isNaN(Lev) then
        begin
            grdSelectedEquip.Cells[fi + fioffsetCol,1] := '-';
            srEquipSelect.AddXY(fi,0,fStr,TDBSeaColours.darkBlue) //ColorWithLevel(Lev,minLev,maxLev))
        end
        else
        begin
            grdSelectedEquip.Cells[fi + fioffsetCol,1] := tostr(saferound(Lev));  //set table values
            srEquipSelect.AddXY(fi,Lev,fStr,TDBSeaColours.darkBlue); //ColorWithLevel(Lev,minLev,maxLev));
        end;

        grdSelectedEquip.Cells[fi + fioffsetCol,0] := fStr + 'Hz';
    end;
    {setting the vertical axis}
    if not(isnan(maxLev) or isnan(minLev)) then
    begin
        chEquipSelect.LeftAxis.Automatic := false;
        if wantMitigation then
            chEquipSelect.LeftAxis.SetMinMax(0, ceilx(maxLev,10))
        else
            chEquipSelect.LeftAxis.SetMinMax(floorx(minLev - belowMin,10),ceilx(maxLev,10));
    end;

    //the overall level text
    if not wantmitigation then
    begin
        if isNaN(equipReturn.spectrum.Level(startfi,endfi,probBW)) then
            grdSelectedEquip.Cells[iLevCol,1] := '-'
        else
            grdSelectedEquip.Cells[iLevCol,1] := tostrWithTestAndRound1(TLevelConverter.convertSourceLevel(equipReturn.spectrum.Level(startfi,endfi,probBW),
                                                                                                           kSL,
                                                                                                           levType,
                                                                                                           tryTofl(edAssessmentPeriod.Text)));
    end;

    //grdSelectedEquip.Cells[iCrestFactorCol,1] := tostr(equipReturn.crestFactordB);

    lblDataSource.Caption := ifthen(equipReturn.DataSource = TDataSource.dsUser,'Data source: user defined',
                                                                    'Data source: default library');
    btRemoveFromDB.Enabled := equipReturn.DataSource = TDataSource.dsUser;
    btEditDB.Enabled := equipReturn.DataSource = TDataSource.dsUser;
end;

procedure TEquipDBForm.tvEquipChange(Sender: TObject; Node: TTreeNode);
var
    currentCount : integer;
    currentDuty : double;
begin
    if Node.Parent = Nil then Exit;

    btEquipOK.Enabled := true; //allow us to mrOK back to the sourcespect form

    //copy the equip data from the node, but preserve the current count and duty
    currentCount := equipReturn.count;
    currentDuty  := equipReturn.duty;
    equipReturn.cloneFrom(TMyTreeNode(node).equipReturn);  //copies spectrum, name, id, datasource
    equipReturn.count := currentCount;
    equipReturn.duty  := currentDuty;

    //if the equipreturn has been used before, it may have different count and duty so we want to keep those
    if not equipReturn.isActive then
    begin
        equipReturn.count := 1;
        equipReturn.duty := 1.0;
    end;

    updateChartAndTable;

    mmComments.Text := TMyTreeNode(node).Comments;

    {$IFDEF SHOW_IMAGE}
//        {code to show an image that is included in the DB. so far only jpg support.}
//        if sltb.FieldIsNull(sltb.FieldIndex['Picture']) then
//            image1.Picture := nil
//        else
//        begin
//            Stream := TMemoryStream.Create;
//            Stream := sltb.FieldAsBlob(sltb.FieldIndex['Picture']);
//            stream.seek(0,sofromBeginning);
//            //ashow(stream.Size);
//            try
//                jpeg := TJpegImage.Create;
//                jpeg.LoadFromStream(stream);
//                scale := larger(jpeg.Width/image1.Width,jpeg.Height/image1.Height);
//                jpeg.scale := jsFullSize;
//                if scale > 2 then
//                    jpeg.scale := jsHalf;
//                if scale > 4 then
//                    jpeg.scale := jsQuarter;
//                if scale > 8 then
//                    jpeg.scale := jsEighth;
//                image1.Picture.Bitmap.Assign(jpeg);//  LoadFromStream(stream);
//            finally
//                jpeg.free;
//            end;
//            stream.Free;
//        end;
    {$ENDIF}
end;

procedure TEquipDBForm.tvEquipCreateNodeClass(Sender: TCustomTreeView; var NodeClass: TTreeNodeClass);
begin
    {replace the createnodeclass method of the treeview because we are going to be
    using my modified treenodes which contain an integer ID field as well}
    NodeClass := TMyTreeNode;
end;


constructor TMyTreeNode.Create(AOwner: TTreeNodes);
begin
    inherited Create(AOwner);

    equipReturn := TEquipReturn.Create;
    bandwidth := bwThirdOctave;
end;

destructor TMyTreeNode.Destroy;
begin
    equipReturn.Free;

    inherited;
end;

procedure TMyTreeNode.spectrumFromSerialisedLevels;
var
    levels : TSingleDynArray;
    fi : integer;
begin
    {get the serialised levels out of the DB and insert them into the stringlist for reading}
    if bandwidth.oct then
        levels.deserialise(OctaveLevelsSerial)
    else
        levels.deserialise(ThirdOctaveLevelsSerial);

    if (length(levels) <> TSpectrum.getLength(bandwidth)) then
    begin
        showmessage('DB error: Expected ' + tostr(TSpectrum.getLength(bandwidth)) + ' comma delimited level values, received ' + tostr(length(levels)));
        eventLog.log('DB error: Expected ' + tostr(TSpectrum.getLength(bandwidth)) + ' comma delimited level values, received ' + tostr(length(levels)));
        Exit;
    end;

    //set the returned spectrum values in the equipreturn
    {anything in the stringlist entry apart from a valid number is treated as NaN. However
    it ;s recommended to use 'NaN' in the DB, to make this explicit}
    equipReturn.spectrum.clear;
    for fi := 0 to length(levels) - 1 do
        if not isnan(levels[fi]) then
            if equipReturn.isMitigation then
                equipReturn.spectrum.setMitigationAmpbyInd(fi,levels[fi],bandwidth)
            else
                equipReturn.spectrum.setAmpbyInd(fi,levels[fi],bandwidth);
end;



end.

