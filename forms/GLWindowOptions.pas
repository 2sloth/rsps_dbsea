unit GLWindowOptions;

interface

uses basetypes, nullable;

type

  TGLWindowState = record
    openGLReady                 : Boolean;
    mouse                       : TMouseInfo;

    bBathymetryListReady        : boolean; //does the bathymetry display list need to be generated or is it ready
    bResultsListReady           : boolean;
    bLastGLWasXSec              : boolean;
    bDrawNowNeeded              : boolean;
    bFirstRun                   : boolean;
    canShowProgressForm         : boolean;
    canHideProgressForm         : boolean;
    imageOverlaySelectedCorner  : nullable<TRectControl>;
  end;

  TGLWindowOptions = record
    showSourceProbeText         : boolean;
    fShowSourceThreshold        : boolean;
    bSettingWorldScale          : boolean;
    bSettingRuler               : boolean;
    bSettingWorldScaleCancelled : boolean;
    bSettingImageOverlay        : boolean;
  end;


implementation

end.
