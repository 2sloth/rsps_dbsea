unit sourceMotionFrm;

interface

uses
    system.DateUtils,
  winapi.Windows, winapi.Messages, system.SysUtils, system.Variants, system.Classes,
  vcl.Graphics, vcl.Controls, vcl.Forms,
  vcl.Dialogs, baseTypes, MCKLib, vcl.Grids, Generics.Defaults,
  Generics.Collections, vcl.StdCtrls, system.Math, helperFunctions, srcPoint,
  soundSource, dbseacolours, problemContainer,
  globalsUnit, Vcl.ExtCtrls, dBSeaOptions, gpxparser,
  latlong2UTM, StringGridHelper, SmartPointer;

type
  TSourceMotionForm = class(TForm)
    sgMotion: TStringGrid;
    btOK: TButton;
    btCancel: TButton;
    lblProblemValues: TLabel;
    lbTotalPositions: TLabel;
    btImportFromFile: TButton;
    pbBottom: TPanel;
    Panel1: TPanel;
    rbSpeedms: TRadioButton;
    rbTime: TRadioButton;
    rbSpeedkmh: TRadioButton;
    rbSpeedKnot: TRadioButton;
    btImportGPX: TButton;
    odGPXTrack: TOpenDialog;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EditBoxesInit;

    procedure setSrc(aSrc : TSource);

    procedure sgMotionSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgMotionSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure sgMotionDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgMotionMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btImportFromFileClick(Sender: TObject);
    procedure rbTimeClick(Sender: TObject);
    procedure btImportGPXClick(Sender: TObject);
  private
    const
    iIndCol         : integer = 0;
    ixCol           : integer = 1;
    iyCol           : integer = 2;
    izCol           : integer = 3;
    idTCol          : integer = 4;
    idivsCol        : integer = 5;
    iAddCol         : integer = 6;
    iRemoveCol      : integer = 7;
  var
    gridRedMask : TBoolMatrix;
  public
    Points : TSrcPointList;
  end;

var
  sourceMotionForm: TSourceMotionForm;

implementation

{$R *.dfm}

uses Main, GLWindow;

procedure TSourceMotionForm.FormCreate(Sender: TObject);
begin
    Points := TSrcPointList.Create(true);
    setlength(gridRedMask,sgMotion.ColCount,sgMotion.RowCount);
    gridRedMask.setAllVal(false);

    {$IFDEF DEBUG}
        self.btImportFromFile.Visible := true;
        self.btImportGPX.Visible := true;
    {$ENDIF}
end;

procedure TSourceMotionForm.FormDestroy(Sender: TObject);
begin
    Points.free;
end;

procedure TSourceMotionForm.FormShow(Sender: TObject);
begin
    EditBoxesInit;
end;

procedure TSourceMotionForm.rbTimeClick(Sender: TObject);
begin
    self.EditBoxesInit;
end;

procedure TSourceMotionForm.setSrc(aSrc: TSource);
var
    i : integer;
begin
    Points.Clear;

    {if we are in this form, then we have a moving source. however it may have no motion points set yet.
    Add all points in the source to our local list of points.}
    for i := 0 to aSrc.MovingPos.Count - 1 do
    begin
        Points.Add(TSrcPoint.Create);
        Points.Last.cloneFrom(aSrc.MovingPos[i]);
    end;

    {if there are no points here, then we need to create the first point, from the current source position}
    if Points.Count = 0 then
    begin
        Points.Add(TSrcPoint.Create);
        Points[0].X := aSrc.Pos.X;
        Points[0].Y := aSrc.Pos.Y;
        Points[0].Z := aSrc.Pos.Z;
        Points[0].dT := 0;
        Points[0].divs := 0;
    end;
end;

procedure TSourceMotionForm.sgMotionDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
    with TStringGrid(Sender) do
    begin
        if gridRedMask[aCol,aRow] then
            Canvas.Font.Color := clRed;

        if (acol = 0) or (arow = 0) then
            canvas.Brush.Color := TDBSeaColours.headerRow;

        {$IFDEF VER280}
        if DefaultDrawing then
            Rect.Left := Rect.Left - 4;
        Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+5, Cells[ACol, ARow]);
        {$ELSE}
        Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
        {$ENDIF}
    end;
end;

procedure TSourceMotionForm.sgMotionMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    aCol, aRow: Integer;
    newTsp,tsp0,tsp2 : TSrcPoint;
begin
    if (button = mbLeft) then
    begin
        sgMotion.MouseToCell(X, Y, aCol, aRow);
        if (arow = 0) or (acol = 0) then exit;

        {add a new point}
        if acol = iAddCol then
        begin
            tsp0 := Points[arow - 1];
            if arow = Points.Count then
            begin
                {insert a new point : copy values from previous point and offset}
                Points.Insert(arow,TSrcPoint.Create);
                newTsp := Points[arow];
                newTsp.X := tsp0.X + 100;
                newTsp.Y := tsp0.Y;
                newTsp.Z := tsp0.Z;
                newTsp.dT := 50;
                newTsp.divs := 1;
            end
            else
            begin
                {insert a new point : interpolate values from previous and next point}
                tsp2 := Points[arow];
                Points.Insert(arow,TSrcPoint.Create);
                newTsp := Points[arow];
                newTsp.X := (tsp0.X + tsp2.X)/2;
                newTsp.Y := (tsp0.Y + tsp2.Y)/2;
                newTsp.Z := (tsp0.Z + tsp2.Z)/2;
                newTsp.dT := tsp2.dT / 2;
                tsp2.dT := tsp2.dT / 2;
                newTsp.divs := tsp2.divs div 2;
                if newTsp.divs < 1 then
                    newTsp.divs := 1;
                tsp2.divs := tsp2.divs div 2;
                if tsp2.divs < 1 then
                    tsp2.divs := 1;
            end;
        end;

        {remove existing point}
        if acol = iRemoveCol then
        begin
            if arow = 1 then exit;
            Points.Delete(arow - 1);
        end;

        EditBoxesInit;
    end;
end;

procedure TSourceMotionForm.sgMotionSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
    if (ACol = iAddCol) or (ACol = iRemoveCol) then
        sgMotion.Options := sgMotion.Options - [goEditing] + [goTabs]
    else
        sgMotion.Options := sgMotion.Options + [goEditing, goTabs, goAlwaysShowEditor];
end;

procedure TSourceMotionForm.sgMotionSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
var
    distance: double;
begin
    {checking on cell input to number cells - is it a number?
    (NB no negatives can be typed in, only pasted in.)}
    if arow = 0 then exit;

    if isFloat(Value) then
    begin
        if acol = ixcol then
            Points[arow - 1].X := container.geoSetup.getXFromString(Value);
        if acol = iycol then
            Points[arow - 1].Y := container.geoSetup.getYFromString(Value);
        if acol = izcol then
            Points[arow - 1].Z := container.geoSetup.getZFromString(Value);
        if (acol = idTcol) and (arow > 1) and (tofl(value) > 0) then
        begin
            distance := Points[arow - 1].asFPoint3.dist(Points[arow - 2].asFPoint3);

            if rbTime.Checked then
                Points[arow - 1].dT := tofl(Value)  {seconds}
            else if distance = 0 then
            begin end
            else if rbSpeedms.Checked then
                Points[arow - 1].dT := distance / f2m(tofl(Value), UWAOpts.dispFeet) {m/s or ft/s}
            else if rbSpeedKnot.Checked then
                Points[arow - 1].dT := distance / (tofl(Value) / 3.6 / 0.621371 * 1.150779) {knots}
            else if UWAOpts.dispFeet then
                Points[arow - 1].dT := distance / (tofl(Value) / 3.6 / 0.621371) {miles per hour}
            else
                Points[arow - 1].dT := distance / (tofl(Value) / 3.6); {km per hour}
        end;
        if (acol = idivsCol) and (arow > 1) and isInt(value) then
            Points[arow - 1].divs := toint(Value);
    end;
end;

procedure TSourceMotionForm.btImportFromFileClick(Sender: TObject);
begin
     if odGPXTrack.Execute then
    begin
        //TODO
    end;
end;

procedure TSourceMotionForm.btImportGPXClick(Sender: TObject);
var
    tracks: ISmart<TTrackList>;
    track: TTrack;
    latlng: TLatLng;
    prevTime: TDateTime;
    tmp: TEastNorth;
begin
    odGPXTrack.Filter := 'GPX file|*.gpx|All files|*.*';
    if odGPXTrack.Execute then
    begin
        tracks := TSmart<TTrackList>.create(TGPXParser.parse(odGPXTrack.FileName));
        if tracks.Count = 0 then exit;

        sgMotion.clear;
        self.Points.Clear;
        prevTime := tracks.First.points.First.time;
        for track in tracks do
            for latlng in track.points do
            begin
                points.add(TSrcPoint.Create);
                tmp := TLatLong2UTM.latlong2UTM(latlng.toTEastNorth);
                points.Last.X := tmp.easting;
                points.Last.y := tmp.northing;
                points.Last.z := 2;
                points.Last.dT := SecondsBetween(latlng.time, prevTime);
                points.Last.divs := 1;
                prevTime := latlng.time;
            end;
        EditBoxesInit;
    end;
end;

procedure TSourceMotionForm.EditBoxesInit;
var
    tsp: TSrcPoint;
    i: integer;
    bShowWarningMessage: boolean;
    pointCount: integer;
    timeCount: double;
    distance: double;
    thisStepDistance: double;
begin
    if UWAOpts.dispFeet then
    begin
        rbSpeedms.Caption := 'Set speed (ft/s)';
        rbSpeedkmh.Caption := 'Set speed (mph)';
    end
    else
    begin
        rbSpeedms.Caption := 'Set speed (m/s)';
        rbSpeedkmh.Caption := 'Set speed (km/h)';
    end;

    sgMotion.RowCount := Points.Count + 1;
    sgMotion.ColCount := iRemoveCol + 1;
    sgMotion.clear;

    setlength(gridRedMask,sgMotion.ColCount,sgMotion.RowCount);
    gridRedMask.setAllVal(false);
    bShowWarningMessage := false;

    pointCount := 1;
    timeCount := 0;
    distance := 0;

    sgMotion.Cells[iIndCol   ,0] := '';
    sgMotion.Cells[ixCol     ,0] := 'x';
    sgMotion.Cells[iyCol     ,0] := 'y';
    sgMotion.Cells[izCol     ,0] := 'z';
    if rbTime.Checked then
        sgMotion.Cells[idTCol    ,0] := 'Time (s)'
    else if rbSpeedKnot.Checked then
        sgMotion.Cells[idTCol    ,0] := 'Speed (knot)'
    else if rbSpeedms.Checked then
    begin
        if UWAOpts.dispFeet then
            sgMotion.Cells[idTCol    ,0] := 'Speed (ft/s)'
        else
            sgMotion.Cells[idTCol    ,0] := 'Speed (m/s)'
    end
    else
    begin
        if UWAOpts.dispFeet then
            sgMotion.Cells[idTCol    ,0] := 'Speed (mph)'
        else
            sgMotion.Cells[idTCol    ,0] := 'Speed (km/h)';
    end;

    sgMotion.Cells[idivsCol  ,0] := 'Points';
    sgMotion.Cells[iAddCol   ,0] := '';
    sgMotion.Cells[iRemoveCol,0] := '';
    for I := 0 to Points.Count - 1 do
    begin
        tsp := Points[i];
        sgMotion.Cells[iIndCol   ,i + 1] := tostr(i + 1);
        sgMotion.Cells[ixCol     ,i + 1] := container.geoSetup.makeXString(tsp.X);
        sgMotion.Cells[iyCol     ,i + 1] := container.geoSetup.makeYString(tsp.Y);
        sgMotion.Cells[izCol     ,i + 1] := container.geoSetup.makeZString(tsp.Z);
        sgMotion.Cells[iAddCol   ,i + 1] := 'Add';

        if i > 0 then
        begin
            pointCount := pointCount + tsp.divs;
            timeCount := timeCount + tsp.dT;
            thisStepDistance := tsp.asFPoint3.dist(Points[i-1].asFPoint3);
            distance := distance + thisStepDistance;

            if rbTime.Checked then
                sgMotion.Cells[idTCol,i + 1] := tostrWithTestAndRound1(tsp.dT)  {seconds}
            else if tsp.dt = 0 then
                sgMotion.Cells[idTCol,i + 1] := '-'
            else if rbSpeedms.Checked then
                sgMotion.Cells[idTCol,i + 1] := tostrWithTestAndRound1( m2f(thisStepDistance, UWAOpts.dispFeet) / tsp.dT ) {m/s or ft/s}
            else if rbSpeedKnot.Checked then
                sgMotion.Cells[idTCol,i + 1] := tostrWithTestAndRound1( 0.621371 * 3.6 / 1.150779 * thisStepDistance / tsp.dT ) {knots}
            else if UWAOpts.dispFeet then
                sgMotion.Cells[idTCol,i + 1] := tostrWithTestAndRound1( 0.621371 * 3.6 * thisStepDistance / tsp.dT ) {Miles per hour}
            else
                sgMotion.Cells[idTCol,i + 1] := tostrWithTestAndRound1( 3.6 * thisStepDistance / tsp.dT ); {km/h}

            sgMotion.Cells[idivsCol,i + 1] := tostr(tsp.divs);

            if (tsp.dT = 0) or (tsp.divs = 0) then
            begin
                gridRedMask[idivsCol,i + 1] := true;
                bShowWarningMessage := true;
            end;

            sgMotion.Cells[iRemoveCol,i + 1] := 'Remove';
        end;
    end;

    self.lbTotalPositions.Caption := 'Total calculation positions: ' + tostrWithTestAndRound1(pointCount) +
                                     ', total time: ' + tostrWithTestAndRound1(timeCount) +
                                     ', total distance: ' + tostrWithTestAndRound1(saferound(distance));
    lblProblemValues.Visible := bShowWarningMessage;
end;

end.
