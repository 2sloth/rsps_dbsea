unit LocationPropertiesFormUnit;

interface

uses
    syncommons,
    system.StrUtils,
  Winapi.Windows, Winapi.Messages, winapi.mmsystem,
  System.SysUtils, System.Variants, System.Classes, system.Generics.collections,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  OtlSync, otlTaskcontrol, otlTask, LocationProperties, problemContainer,
  eventLogging, helperFunctions, baseTypes, dBSeaColours, Vcl.ComCtrls,
  options, OtlParallel;

type
  TLocationPropertiesForm = class(TForm)
    im: TImage;
    btCancel: TButton;
    btOK: TButton;
    btAdd: TButton;
    btDuplicate: TButton;
    btRemove: TButton;
    edName: TEdit;
    mmComments: TMemo;
    edEasting: TEdit;
    edNorthing: TEdit;
    lbItems: TListBox;
    lbMousePos: TLabel;
    lbEasting: TLabel;
    lbNorthing: TLabel;
    pb: TProgressBar;
    btShowInfluence: TButton;
    btColour: TButton;
    lbName: TLabel;
    lbComments: TLabel;
    btJSONImport: TButton;
    odJSON: TOpenDialog;
    tmAnimation: TTimer;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);

    procedure btAddClick(Sender: TObject);
    procedure btRemoveClick(Sender: TObject);
    procedure btDuplicateClick(Sender: TObject);
    procedure imMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure edNameChange(Sender: TObject);
    procedure mmCommentsChange(Sender: TObject);
    procedure lbItemsClick(Sender: TObject);
    procedure lbItemsKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edEastingChange(Sender: TObject);
    procedure edNorthingChange(Sender: TObject);
    procedure imMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure lbItemsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure btShowInfluenceClick(Sender: TObject);
    procedure btColourClick(Sender: TObject);
    procedure btJSONImportClick(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure tmAnimationTimer(Sender: TObject);
  private
  const
    MouseMoveTimeout = 50;
  var
    selectedRadius: integer;
    showInfluence: boolean;
    lastMoustUpdate: cardinal;
    updateFromCode: boolean;

    procedure editBoxesInit;
    procedure updateMousePos(X, Y: Integer);
    procedure lbSelectionChange;
    procedure updateOverlay;
    function  getWorldXY(x, y: integer): TFPoint;
    procedure setShowInfluence(const val: boolean);
  public
    bmp: TBitmap;
    bmpInfluence: TBitmap;
    //lp: TLocationProperties;
    propsMap: TLocationPropertiesList;
  end;

var
  LocationPropertiesForm: TLocationPropertiesForm;

implementation

{$R *.dfm}

procedure TLocationPropertiesForm.FormCreate(Sender: TObject);
begin
    self.propsMap := TLocationPropertiesList.Create(true);
    bmp := TBitmap.create;
    bmpInfluence := TBitmap.create;
    setShowInfluence(false);
    selectedRadius := 8;
end;

procedure TLocationPropertiesForm.FormDestroy(Sender: TObject);
begin
    bmp.free;
    bmpInfluence.Free;
    self.propsMap.free;
end;

procedure TLocationPropertiesForm.FormHide(Sender: TObject);
begin
    tmAnimation.Enabled := false;
end;

procedure TLocationPropertiesForm.FormShow(Sender: TObject);
var
    log: ISynLog;
begin
    updateFromCode := true;
    try
        log := TSynLogLevs.Enter(self, 'FormShow');

        self.propsMap.cloneFrom(container.scenario.propsMap, container.bathymetry.zMax);
        if self.propsMap.count = 0 then
        begin
            showmessage('Error: no properties found in list');
            eventLog.log('No properties found in properties map list');
            self.ModalResult := mrCancel;
        end;

        lastMoustUpdate := timegettime;
        tmAnimation.Enabled := true;
        setShowInfluence(false);
        pb.Visible := false;
        lbMousePos.Caption := '';
        editBoxesInit;
    finally
        updateFromCode := false;
    end;
end;

procedure TLocationPropertiesForm.imMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    p: TFPoint;
begin
    updateFromCode := true;
    try
        p := getWorldXY(x,y);
        propsMap.selected.easting := p.x;
        propsMap.selected.northing := p.y;
        setShowInfluence(false);
        editBoxesInit();
    finally
        updateFromCode := false;
    end;
end;

procedure TLocationPropertiesForm.setShowInfluence(const val: boolean);
begin
    self.showInfluence := val;
    self.btShowInfluence.Caption := ifthen(self.showInfluence, 'Hide influence', 'Show influence');
end;

procedure TLocationPropertiesForm.imMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    if updateFromCode then exit;
    if timeGetTime - lastMoustUpdate < self.MouseMoveTimeout then exit;

    updateMousePos(x,y);
    lastMoustUpdate := timeGetTime;
end;

procedure TLocationPropertiesForm.mmCommentsChange(Sender: TObject);
begin
    propsMap.selected.comments := mmComments.Text;
end;

procedure TLocationPropertiesForm.tmAnimationTimer(Sender: TObject);
begin
    if updateFromCode then exit;

    inc(selectedRadius);
    if selectedRadius >= 14 then
        selectedRadius := 6;

    updateOverlay;
end;

function TLocationPropertiesForm.getWorldXY(x,y: integer): TFPoint;
begin
    result.x := x                   / im.Width  * container.bathymetry.xAspectRatio * container.geoSetup.worldScale;
    result.y := (im.Height - 1 - y) / im.Height * container.bathymetry.yAspectRatio * container.geoSetup.worldScale;
end;

procedure TLocationPropertiesForm.updateMousePos(X, Y: Integer);
var
    p: TFPoint;
begin
    p := getWorldXY(x,y);
    lbMousePos.Caption := 'Mouse: ' + floattostrf(p.x + container.geoSetup.Easting,ffFixed,14,1) + ' east, ' +
        floattostrf(p.y + container.geoSetup.Northing,ffFixed,14,1) + ' north';
end;

procedure TLocationPropertiesForm.btAddClick(Sender: TObject);
var
    lp: TLocationProperties;
begin
    lp := TLocationProperties.create(container.bathymetry.zMax);
    propsMap.add(lp);
    propsMap.selected := lp;
    editBoxesInit;
end;

procedure TLocationPropertiesForm.btColourClick(Sender: TObject);
begin
    with TOptionsForm.setColor(propsMap.selected.color) do
    begin
        if not HasValue then exit;
        propsMap.selected.color := val;
        editBoxesInit;
    end;
end;

procedure TLocationPropertiesForm.btDuplicateClick(Sender: TObject);
var
    newLP: TLocationProperties;
begin
    newLP := TLocationProperties.Create(container.bathymetry.zMax);
    newLP.cloneFrom(propsMap.selected);
    newLP.color := TDBSeaColours.randomSaturatedColor;
    propsMap.add(newLP);
    propsMap.selected := newLP;
    editBoxesInit;
end;

procedure TLocationPropertiesForm.btJSONImportClick(Sender: TObject);
var
    lpl: TLocationPropertiesList;
begin
    if not odJSON.Execute then exit;

    try
        lpl := TLocationPropertiesList.importFromJSON(thelper.readFileToString(odJSON.FileName), container.bathymetry.zMax);
        if not assigned(lpl) then
            raise EJSONParsing.Create('Location properties list was not created');

        self.propsMap := lpl;

        setShowInfluence(false);
        editBoxesInit;
    except on e: exception do
    begin
        showmessage('Error parsing JSON: ' + e.Message);
    end;
    end;
end;

procedure TLocationPropertiesForm.btOKClick(Sender: TObject);
begin
    container.scenario.propsMap.cloneFrom(self.propsMap, container.bathymetry.zMax);
end;

procedure TLocationPropertiesForm.btRemoveClick(Sender: TObject);
var
    lp: TLocationProperties;
begin
    if propsMap.count <= 1 then
    begin
        showmessage('There must be at least one item in the list');
        exit;
    end;

    if not assigned(propsMap.selected) then exit;

    lp := propsMap.selected;
    propsMap.Extract(lp);
    lp.free;
    propsMap.selected := propsMap.First;
    editBoxesInit;
end;

procedure TLocationPropertiesForm.btShowInfluenceClick(Sender: TObject);
var
    c: TColor;
    i,j: integer;
    p: TFPoint;
begin
    if showInfluence then
    begin
        setShowInfluence(false);
        exit;
    end;

    pb.BringToFront;
    pb.Visible := true;
    pb.Position := 0;
    pb.Max := im.Width;

    bmpInfluence.SetSize(im.Width, im.Height);
    if propsMap.count > 1 then
        for i := 0 to im.Width - 1 do
        begin
            for j := 0 to im.Height - 1 do
            begin
                p := getWorldXY(i,j);
                c := propsMap.interpColor(p.x, p.y);
                bmpInfluence.Canvas.Pixels[i,j] := TDBSeaColours.colorMix(im.Canvas.Pixels[i,j],c,0.5);
                //im.Canvas.Pixels[i,j] := TDBSeaColours.colorMix(im.Canvas.Pixels[i,j],c,0.3);
            end;
            if i mod 20 = 0 then
            begin
                pb.Position := i;
                Application.ProcessMessages;
            end;
        end;

    setShowInfluence(true);
    pb.Visible := false;
end;

procedure TLocationPropertiesForm.edEastingChange(Sender: TObject);
begin
    if updateFromCode or not isFloat(edEasting.Text) then exit;

    propsMap.selected.easting := container.geoSetup.getXfromString(edEasting.Text);
    setShowInfluence(false);
    updateOverlay;
end;

procedure TLocationPropertiesForm.edNorthingChange(Sender: TObject);
begin
    if updateFromCode or not isFloat(edNorthing.Text) then exit;

    propsMap.selected.northing := container.geoSetup.getXfromString(edNorthing.Text);
    setShowInfluence(false);
    updateOverlay;
end;

procedure TLocationPropertiesForm.editBoxesInit;
var
    p: TPair<integer,TLocationProperties>;
begin
    updateFromCode := true;
    try
        if lbItems.Count = propsMap.Count then
        begin
            for p in propsMap.enumerate do
                lbItems.Items[p.Key] := inttostr(1 + p.key) + ': ' + p.Value.name;
        end
        else
        begin
            lbItems.clear;
            for p in propsMap.enumerate do
                lbItems.Items.add(inttostr(1 + p.key) + ': ' + p.Value.name);
        end;

        if lbItems.ItemIndex <> propsMap.IndexOf(propsMap.selected) then
            lbItems.ItemIndex := propsMap.IndexOf(propsMap.selected);

        self.edName.Text := propsMap.selected.name;
        self.mmComments.Text := propsMap.selected.comments;
        self.edEasting.Text := container.geoSetup.makeXString(propsMap.selected.easting);
        self.edNorthing.Text := container.geoSetup.makeYString(propsMap.selected.northing);

        btShowInfluence.Enabled := propsMap.Count > 1;

        updateOverlay;

    finally
        updateFromCode := false;
    end;
end;

procedure TLocationPropertiesForm.edNameChange(Sender: TObject);
begin
    propsMap.selected.name := edName.Text;
end;

procedure TLocationPropertiesForm.lbItemsClick(Sender: TObject);
begin
    lbSelectionChange;
end;

procedure TLocationPropertiesForm.lbItemsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
    ListBox: TListBox;
    Canvas: TCanvas;
    aLP: TLocationProperties;
begin
    ListBox := Control as TListBox;
    Canvas := ListBox.Canvas;

    // clear the destination rectangle
    Canvas.Brush.Color := clWhite;
    canvas.pen.color := clWhite;
    Canvas.FillRect(Rect);

    //focus rect
    if index = lbItems.ItemIndex then
    begin
        Canvas.Brush.color := clBlue;
        Canvas.Pen.color := clBlue;
        Canvas.FillRect(Rect);
    end;

    // draw the text
    Canvas.TextOut(Rect.Left, Rect.Top, ListBox.Items[Index]);

    aLP := self.propsMap[index];
    canvas.brush.color := aLP.color;
    canvas.pen.color := clWhite;
    Canvas.Pen.Width := 2;
    canvas.ellipse(rect.width - 15,
        rect.top  + 0,
        rect.width - 5,
        rect.top + 10);
end;

procedure TLocationPropertiesForm.lbItemsKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    lbSelectionChange;
end;

procedure TLocationPropertiesForm.lbSelectionChange;
begin
    if updateFromCode then exit;
    if lbItems.ItemIndex = propsMap.IndexOf(propsMap.selected) then exit;

    propsMap.selected := propsMap[lbItems.ItemIndex];
    editBoxesInit;
end;

procedure TLocationPropertiesForm.updateOverlay;
const
    radius = 5;
var
    aLP: TLocationProperties;
    x,y: integer;
begin
    if (im.ClientWidth <> im.Picture.Width) or (im.ClientHeight <> im.Picture.Height) then
        im.Picture := nil;
    if showInfluence then
        im.Canvas.StretchDraw(im.ClientRect, self.bmpInfluence)
    else
        im.Canvas.StretchDraw(im.ClientRect, self.bmp);

    for aLP in propsMap do
    begin
        x := round(aLP.easting  * (im.Width  / container.bathymetry.xAspectRatio / container.geoSetup.worldScale));
        y := round(aLP.northing * (im.Height / container.bathymetry.yAspectRatio / container.geoSetup.worldScale));
        y := (im.Height - 1 - y);
        im.Canvas.Pen.Color := aLP.color;
        im.Canvas.Pen.Width := 1;
        im.Canvas.Pen.Style := psSolid;

        im.Canvas.Brush.Style := bsSolid;
        im.Canvas.Brush.Color := aLP.color;
        im.Canvas.Ellipse(x - radius,y - radius,x + radius,y + radius);

        if aLP = propsMap.selected then
        begin
            im.Canvas.Brush.Style := bsClear;
            im.Canvas.Ellipse(x - selectedRadius,y - selectedRadius,x + selectedRadius,y + selectedRadius);
        end;
    end;
end;

end.
