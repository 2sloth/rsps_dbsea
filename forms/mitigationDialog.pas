{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit mitigationDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, helperFunctions;

type
  TMitigationForm = class(TForm)
    edMitigation: TEdit;
    udMitigation: TUpDown;
    btmitigationValue: TButton;
    btmitigationLibrary: TButton;
    btmitigationCancel: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edMitigationChange(Sender: TObject);
    procedure udMitigationClick(Sender: TObject; Button: TUDBtnType);
  private
    updateFromCode      : boolean;
  public
    mitigation : double;
  end;

var
    mitigationForm: TMitigationForm;

implementation

{$R *.dfm}

procedure TMitigationForm.edMitigationChange(Sender: TObject);
begin
    if not updateFromCode then
    begin
        if isFloat(edMitigation.Text) then
            mitigation := tofl(edMitigation.Text)
        else
            edMitigation.Text := tostr(mitigation);
    end;
end;

procedure TMitigationForm.FormCreate(Sender: TObject);
begin
    updateFromCode := true;
    mitigation := 5;
end;

procedure TMitigationForm.FormShow(Sender: TObject);
begin
    //edMitigation      := Tfloatedit.create(self,edMitigation);
    edMitigation.Text := tostr(mitigation);
    updateFromCode := false;
end;

procedure TMitigationForm.udMitigationClick(Sender: TObject; Button: TUDBtnType);
begin
    if Button = btNext then
        mitigation := mitigation + 1
    else
        mitigation := mitigation - 1;
    edMitigation.Text := tostr(mitigation);
end;

end.
