unit scenarioDiffForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, scenarioDiff, StdCtrls, ExtCtrls, scenario, math, helperfunctions,
  system.strutils, dbseaoptions, problemContainer;

type
  TscenarioDiffFrm = class(TForm)
    pbLevels: TPaintBox;
    btOk: TButton;
    lbScenario1: TListBox;
    lbScenario2: TListBox;
    btCancel: TButton;
    sbMin: TScrollBar;
    sbMax: TScrollBar;
    lbMin: TLabel;
    lbMax: TLabel;
    lbAllMin: TLabel;
    lbAllMax: TLabel;
    lbScenariosCannotBePaired: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure lbScenarioClick(Sender: TObject);
    procedure lbScenario1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure sbMinChange(Sender: TObject);
    procedure sbMaxChange(Sender: TObject);
  private
    var
        mxd, mnd : double;
        procedure updateImage;
        procedure updateMinMax;
        procedure updateAll;
  public
      var
        diff : TScenarioDiff;
  public

  end;

var
  scenarioDiffFrm: TscenarioDiffFrm;

implementation

{$R *.dfm}

{$B-}

uses main;

procedure TscenarioDiffFrm.FormCreate(Sender: TObject);
begin
    diff := TScenarioDiff.Create;
    mxd := 0;
    mnd := 0;
end;

procedure TscenarioDiffFrm.FormDestroy(Sender: TObject);
begin
    diff.free;
end;

procedure TscenarioDiffFrm.FormShow(Sender: TObject);
var
    scen : TScenario;
begin
    if not assigned(diff) then exit;

    for scen in container.scenarios do
    begin
        lbScenario1.AddItem(scen.name,scen);
        lbScenario2.AddItem(scen.name,scen);
    end;

    if container.scenarios.Count >= 2 then
    begin
        diff.s1 := container.scenario;
        lbScenario1.ItemIndex := container.currentScenario;
        if container.currentScenario = 1 then
        begin
            diff.s2 := container.scenarios[0];
            lbScenario2.ItemIndex := 0;
        end
        else
        begin
            diff.s2 := container.scenarios[1];
            lbScenario2.ItemIndex := 1;
        end;
        updateAll;
    end;
end;

procedure TscenarioDiffFrm.lbScenarioClick(Sender: TObject);
begin
    if not assigned(diff) then exit;

    case TListBox(sender).tag of
        0 : diff.s1 := TScenario(lbScenario1.Items.Objects[lbScenario1.ItemIndex]);
        1 : diff.s2 := TScenario(lbScenario2.Items.Objects[lbScenario2.ItemIndex]);
    else
        //
    end;

    lbScenariosCannotBePaired.Visible := not diff.scenariosCanBePaired;

    updateAll;
end;

procedure TscenarioDiffFrm.lbScenario1DrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
    if TScenario(TListBox(Control).Items.Objects[Index]).resultsExist then
        TListBox(Control).Canvas.Font.Color := clBlack
    else
        TListBox(Control).Canvas.Font.Color := clGray;
    TListBox(Control).Canvas.TextOut( Rect.Left, Rect.Top, TListBox(Control).Items[Index] );
end;

procedure TscenarioDiffFrm.sbMaxChange(Sender: TObject);
begin
    if sbMin.Position > sbMax.Position then
        sbMin.Position := sbMax.Position;
    updateImage;
end;

procedure TscenarioDiffFrm.sbMinChange(Sender: TObject);
begin
    if sbMin.Position > sbMax.Position then
        sbMax.Position := sbMin.Position;
    updateImage;
end;

procedure TscenarioDiffFrm.updateImage;
var
    i,j, ii, jj : integer;
    mn, mx : double;
    n : integer;
begin
    if not diff.isValid then exit;
    if length(diff.diff) = 0 then exit;

    if isnan(mxd) or isnan(mnd) then exit;
    if (mxd = 0) and (mnd = 0) then exit; //todo error messages here

    mn := sbMin.Position;
    mx := sbMax.Position;

    lbMin.Caption := tostr(sbMin.Position) + ' dB';
    lbMax.Caption := tostr(sbMax.Position) + ' dB';

    for i := 0 to length(diff.diff) - 1 do
        for j := 0 to length(diff.diff[0]) - 1 do
        begin
            if (mn = mx) or isnan(diff.diff[i,j]) then
                n := 0
            else
                n := saferound((diff.diff[i,j] - mn) / (mx - mn) * 255);

            if n < 0 then n := 0;
            if n > 255 then n := 255;

            ii := saferound(i * pbLevels.Width  / length(diff.diff));
            jj := saferound((pbLevels.Height - 1) - j * pbLevels.Height / length(diff.diff[0]));
            pbLevels.Canvas.Brush.Color := RGB(n,n,n);
            pbLevels.Canvas.Pen.Color := RGB(n,n,n);
            pbLevels.Canvas.Rectangle(ii,
                                      jj,
                                      ii + saferound(pbLevels.Width  / length(diff.diff)),
                                      jj + saferound(pbLevels.Height / length(diff.diff[0])));
        end;
end;

procedure TscenarioDiffFrm.updateMinMax;
var
    i,j : integer;
begin
    if not diff.isValid then exit;
    if length(diff.diff) = 0 then exit;

    mxd := nan;
    mnd := nan;
    for i := 0 to length(diff.diff) - 1 do
        for j := 0 to length(diff.diff[0]) - 1 do
            lesserGreater(mnd,mxd,diff.diff[i,j]);

    if isnan(mnd) or  isNan(mxd) then exit;

    sbMin.Min := safefloor(mnd);
    sbMin.Max := safeceil(mxd);
    sbMin.Position := safefloor(mnd);
    sbMax.Min := safefloor(mnd);
    sbMax.Max := safeceil(mxd);
    sbMax.Position := safeceil(mxd);

    lbMin   .Caption := tostr(sbMin.Position) + ' dB';
    lbMax   .Caption := tostr(sbMax.Position) + ' dB';
    lbAllMin.Caption := tostr(sbMin.Position) + ' dB';
    lbAllMax.Caption := tostr(sbMax.Position) + ' dB';
end;

procedure  TscenarioDiffFrm.updateAll;
begin
    diff.updateDiff(UWAOpts.levelsDisplay);
    updateMinMax;
    updateImage;
end;

end.
