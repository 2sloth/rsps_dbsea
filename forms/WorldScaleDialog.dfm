object frmWorldScale: TfrmWorldScale
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Setting world scale'
  ClientHeight = 341
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblScale: TLabel
    Left = 208
    Top = 59
    Width = 60
    Height = 13
    Caption = 'Distance (m)'
  end
  object btCancel: TButton
    Left = 526
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object edScale: TEdit
    Left = 303
    Top = 56
    Width = 96
    Height = 21
    TabOrder = 0
    Text = '0'
    OnChange = edScaleChange
  end
  object rbDistBetweenPoints: TRadioButton
    Left = 24
    Top = 58
    Width = 153
    Height = 17
    Caption = 'Distance between points'
    Checked = True
    TabOrder = 1
    TabStop = True
    OnClick = rbDistBetweenPointsClick
  end
  object rbXandY: TRadioButton
    Left = 24
    Top = 152
    Width = 137
    Height = 17
    Caption = 'Locations of two points'
    TabOrder = 2
    OnClick = rbXandYClick
  end
  object pnPointA: TPanel
    Left = 192
    Top = 120
    Width = 409
    Height = 71
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 4
    object lblAx: TLabel
      Left = 15
      Top = 42
      Width = 35
      Height = 13
      Caption = 'Easting'
    end
    object lblAy: TLabel
      Left = 230
      Top = 42
      Width = 41
      Height = 13
      Caption = 'Northing'
    end
    object lbPointATitle: TLabel
      Left = 15
      Top = 15
      Width = 34
      Height = 13
      Caption = 'Point A'
    end
    object edAx: TEdit
      Left = 87
      Top = 42
      Width = 96
      Height = 21
      TabOrder = 0
      Text = '0'
    end
    object edAy: TEdit
      Left = 297
      Top = 42
      Width = 96
      Height = 21
      TabOrder = 1
      Text = '0'
    end
  end
  object pnPointB: TPanel
    Left = 192
    Top = 197
    Width = 409
    Height = 70
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 5
    object lbPointBTitle: TLabel
      Left = 15
      Top = 14
      Width = 33
      Height = 13
      Caption = 'Point B'
    end
    object lblBx: TLabel
      Left = 16
      Top = 41
      Width = 35
      Height = 13
      Caption = 'Easting'
    end
    object lblBy: TLabel
      Left = 232
      Top = 41
      Width = 41
      Height = 13
      Caption = 'Northing'
    end
    object edBx: TEdit
      Left = 88
      Top = 41
      Width = 96
      Height = 21
      TabOrder = 0
      Text = '0'
    end
    object edBy: TEdit
      Left = 298
      Top = 41
      Width = 96
      Height = 21
      TabOrder = 1
      Text = '0'
    end
  end
  object btOK: TButton
    Left = 433
    Top = 288
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 6
  end
end
