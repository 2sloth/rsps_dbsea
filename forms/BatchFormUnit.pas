unit BatchFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  StringsHelper;

type
  TBatchForm = class(TForm)
    btSolve: TButton;
    btDone: TButton;
    btClear: TButton;
    btOpen: TButton;
    odBatch: TOpenDialog;
    mmFiles: TMemo;
    lbProgress: TLabel;

    procedure btSolveClick(Sender: TObject);
    procedure btClearClick(Sender: TObject);
    procedure btOpenClick(Sender: TObject);
  private
  public
  end;

var
  BatchForm: TBatchForm;

implementation

{$R *.dfm}

uses problemContainer;

procedure TBatchForm.btClearClick(Sender: TObject);
begin
    mmFiles.Lines.Clear;
end;

procedure TBatchForm.btOpenClick(Sender: TObject);
var
    filename: TFilename;
begin
    if not odBatch.Execute then exit;

    for filename in odBatch.Files do
        if (filename <> '') and not mmFiles.Lines.contains(filename) then
            mmFiles.Lines.Add(filename);
end;

procedure TBatchForm.btSolveClick(Sender: TObject);
var
    i: integer;
begin
    if mmFiles.Lines.Count = 0 then
    begin
        showmessage('Add files to the solve list');
        exit;
    end;

    for i := 0 to mmFiles.Lines.count - 1 do
    begin
        if mmFiles.Lines[i] = '' then continue;

        lbProgress.Caption := 'Solving ' + inttostr(i + 1) + ' of ' + inttostr(mmFiles.Lines.count) + ': ' + mmFiles.Lines[i];
        Application.ProcessMessages;

        try
            //todo container.batchSolve(mmFiles.Lines[i]);
        except on e: exception do
        end;
    end;

    lbProgress.Caption := 'Solve finished';
end;

end.
