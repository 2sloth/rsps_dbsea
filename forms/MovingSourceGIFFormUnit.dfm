object MovingSourceGIFForm: TMovingSourceGIFForm
  Left = 0
  Top = 0
  Caption = 'Animated results'
  ClientHeight = 562
  ClientWidth = 760
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  DesignSize = (
    760
    562)
  PixelsPerInch = 96
  TextHeight = 13
  object im: TImage
    Left = 24
    Top = 24
    Width = 720
    Height = 481
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object lbInvalid: TLabel
    Left = 120
    Top = 256
    Width = 502
    Height = 13
    Caption = 
      'This function can only be used with one moving source, where res' +
      'ults are stored for every solve position'
  end
  object lbFrameDelay: TLabel
    Left = 262
    Top = 525
    Width = 83
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Frame delay (ms)'
  end
  object btGenerate: TButton
    Left = 426
    Top = 520
    Width = 75
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = 'Generate'
    TabOrder = 0
    OnClick = btGenerateClick
  end
  object btDone: TButton
    Left = 669
    Top = 520
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Done'
    ModalResult = 2
    TabOrder = 1
    OnClick = btDoneClick
  end
  object pb: TProgressBar
    Left = 24
    Top = 520
    Width = 137
    Height = 17
    Anchors = [akRight, akBottom]
    Step = 1
    TabOrder = 2
  end
  object btCancel: TButton
    Left = 167
    Top = 520
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    TabOrder = 3
    OnClick = btDoneClick
  end
  object btSaveToGIF: TButton
    Left = 507
    Top = 520
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Save to GIF'
    TabOrder = 4
    OnClick = btSaveToGIFClick
  end
  object edFrameSpeed: TEdit
    Left = 351
    Top = 522
    Width = 58
    Height = 21
    Anchors = [akRight, akBottom]
    TabOrder = 5
    Text = '100'
  end
  object btSaveToAVI: TButton
    Left = 588
    Top = 520
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Save to AVI'
    TabOrder = 6
    OnClick = btSaveToAVIClick
  end
  object sdGIF: TSaveDialog
    DefaultExt = 'gif'
    Filter = 'GIF images|*.gif|All files|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 48
    Top = 40
  end
  object sdAVI: TSaveDialog
    DefaultExt = 'avi'
    Filter = 'AVI files|*.avi|All files|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 88
    Top = 48
  end
end
