unit CrossSectionForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, math, StrUtils, helperFunctions, dBSeaOptions, Vcl.ExtCtrls,
  problemContainer, CrossSecEndpoint, crossSec, latLongEntryFrm;

type
  TEditCrossSectionForm = class(TForm)
    btOK: TButton;
    btCancel: TButton;
    lblAx: TLabel;
    lblAy: TLabel;
    lblBx: TLabel;
    lblBy: TLabel;
    edAx: TEdit;
    edBy: TEdit;
    edAy: TEdit;
    edBx: TEdit;
    pnPointA: TPanel;
    lbPointATitle: TLabel;
    pnPointB: TPanel;
    lbPointBTitle: TLabel;
    procedure FormShow(Sender: TObject);
    procedure EditBoxesInit;
    procedure btOKClick(Sender: TObject);
    procedure edMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
  public
  end;

var
  editCrossSectionForm: TEditCrossSectionForm;

implementation

{$R *.dfm}

procedure TEditCrossSectionForm.FormShow(Sender: TObject);
begin
    EditBoxesInit;
end;

procedure TEditCrossSectionForm.btOKClick(Sender: TObject);
begin
    if not (isfloat(edax.text) and isfloat(eday.text) and
        isfloat(edbx.text) and isfloat(edby.text)) then
    begin
        showmessage('Could not parse values, check entered data');
        exit;
    end;

    container.crossSec.endpoints[0].pos.x := container.geoSetup.getXfromString(edax.text);
    container.crossSec.endpoints[0].pos.y := container.geoSetup.getYfromString(eday.text);
    container.crossSec.endpoints[1].pos.x := container.geoSetup.getXfromString(edbx.text);
    container.crossSec.endpoints[1].pos.y := container.geoSetup.getYfromString(edby.text);

    self.ModalResult := mrOK;
end;

procedure TEditCrossSectionForm.edMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if (Button = mbRight) and (sender is TEdit) then
    begin
        if (TEdit(sender) = edAx) or (TEdit(sender) = edAy) then
            latLongEntryForm.setEditsAndShowModal(edAx,edAy)
        else if (TEdit(sender) = edBx) or (TEdit(sender) = edBy) then
            latLongEntryForm.setEditsAndShowModal(edBx,edBy);
    end;
end;

procedure TeditCrossSectionForm.EditBoxesInit;
var
    sUnits : string;
begin
    sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');
    lblAx.Caption := 'Easting ' + sUnits;
    lblAy.Caption := 'Northing ' + sUnits;
    lblBx.Caption := 'Easting ' + sUnits;
    lblBy.Caption := 'Northing ' + sUnits;

    edAx.Text := container.geoSetup.makeXString( container.crossSec.endpoints[0].Pos.x );
    edAy.Text := container.geoSetup.makeYString( container.crossSec.endpoints[0].Pos.y );
    edBx.Text := container.geoSetup.makeXString( container.crossSec.endpoints[1].Pos.x );
    edBy.Text := container.geoSetup.makeYString( container.crossSec.endpoints[1].Pos.y );
end;

end.
