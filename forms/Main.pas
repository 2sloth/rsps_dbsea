{ ******************************************************* }
{                                                         }
{ dBSea Underwater Acoustics                              }
{                                                         }
{ Copyright (c) 2013 2014 2015 dBSea Ltd                  }
{                                                         }
{ ******************************************************* }

{ notes so far

  Angles are in radians
  lengths in metres
  density in kg/m^3 internally, but when it gets passed to the solvers it is in rho/rho_water
  speed in m/s
  internally, no northing or easting are used. Northing and Easting only shown on display

  angle of (x=1 y=0) = 0 , increasing going anticlockwise
  so x = cos(angle) and y = sin(angle)

  z (depth) is positive for sources, receivers etc. so when we draw to opengl, these become -ve

  generally
  z : depth (m)
  d : depth index
  r : range (m)
  i : range or x index
  j : y index

  SSP = CProfile = sound speed profile (with depth)

  note on NaN :
  I am trying to propagate NaN through the code as much as possible, and then deal
  with it finally at the last step (ie plotting or putting text onscreen).
  Sending a line chart a NaN is ok, it will just ignore, however a bar chart
  won't handle it (and will not plot). So send a 0 instead - nb we can't just
  skip that particular index, as the previous bar will be made wider until
  we hit the next data value.

  about the database:
  we are using an sqlite database, in the file acoustLib.db, and a user database
  with the same format C:/users/<username>/AppData/Local/dBSea Ltd/userLib.db.
  this file currently exists in the same directory as the executable, so needs to be
  written at installation. if we want to pass out updates to the database, it needs
  to be able to be written in an administrator controlled directory.
  the mainform makes a connection to the database (and
  kills it on shutdown) and the other pages can all use that main connection.
  User additions to the DB go into the userLib, and both databases are queried
  when we run a query.
  A major problem with this sqlite3.dll is that text fields are not unicode,
  only ansichar so we can't store any umlauts, accents etc for those not
  working in english. I would like to find another way of accessing the db that
  allows unicode strings.

  note on colormaps :
  I'm using an array of float, with values in the range 0 to 1. This
  is what openGL expects, and as this is where we want things to run
  quickly, that means a minimum of processing needs to be done during
  opengL render. At other (non time-critical) times however, it is necessary to convert to
  the more delphi standard TColor RGB(byte,byte,byte) type thing.

  also using the american spelling of color often, as that is the delphi standard.
  I haven't been too consistent about this though.

  Use the sizeconstraints->MinHeight for each frame to set a minimum height, otherwise
  the bottom of the frame can be cut off

  The bathymetry colormap is 2048 long. So, it is possible with very deep water and very shallow
  on the same map, that shallow areas will get coloured the land colour.

  RA, ReceiverAreas: Originally, it was possible to create multiple RAs covering different
  areas. I have changed this now so that there is only ever 1 RA. It still sits in
  a TObjectList, as the code works at least, and it is possible that at some stage
  we will want to get that functionality back. The single RA is at prob.ReceiverAreas[0].
  Also there are quite a few variables in the RA that make no sense, considering there
  aren't multiple RAs any more, I will leave them in place for the moment.

  note on solving and supersampling: not implemented yet, but probably a good way of going
  about the solution would be to supersample the source solver, and then at the RA
  level, get the minimum TL for each point from the nearby supersampled points. ie we are
  normally having a spacing that is greater (or much greater) than the wavelength, and
  so there is, even in the analytical solution, a short range variation of around 10 - 15 dB
  when we are talking about low freqs. (higher freq I think the solvers still try to give
  an exact solution - eg the ray tracer, which is obviously not possible in the fine detail.)
  (see the Buckingham paper showing the analytical wedge solution). It would make sense to
  sample more densely (true subwavelength supersampling wont be possible) and then
  choose the _minimum_ TL. Would obviously slow down the solution a lot.

  for each of the frames, we override the default constructor with
  constructor Create(AOwner: TComponent); so we can then do all our initialisation stuff then.
  otherwise we have to do it in frameshow, which gets called repeatedly. this causes
  problems with initialising the selectboxes etc.

  The levels that are plotted are weighted levels, ie they take the current
  weighting spectrum into account.

  ifthen() evaluates both arguments, so is best only to use it for things like alternative hardcoded strings etc.
  also don't use it with ifthen(isnumber(foo),tofloat(foo),bar) as that will also crash. also this makes
  ifthen slightly slower.

  1 <> nan = true
  not(1 = nan) = true
  nan <> nan = false (!)
}

unit Main;

//moving this here because it kept getting removed from the project file
{$R dBSea_Resource_manualbuild.RES} //contains resources, eg sqlite3.dll


interface

uses
    syncommons,
    winapi.Windows, winapi.Messages, winapi.ShellApi, winapi.RichEdit,
    //winapi.ActiveX,
    system.SysUtils, system.Classes, system.types, system.Typinfo,
    system.Threading, System.StrUtils, System.Actions, system.UITypes,
    system.math, system.Diagnostics,
    vcl.Graphics, vcl.Forms, vcl.Controls, vcl.Menus, vcl.imaging.gifimg,
    vcl.StdCtrls, vcl.Dialogs, vcl.Buttons,
    vcl.ExtCtrls, vcl.ComCtrls, vcl.StdActns,
    vcl.ActnList, vcl.ToolWin, vcl.ImgList, Vcl.OleCtrls,
    vcl.HTMLHelpViewer, vcl.Themes, vcl.clipbrd, vcl.AppEvnts,
    Generics.Collections,
    baseTypes,
    editSetupFrame,
    EditSourceFrame, GLWindow, equipDBFrm,
    sourceSpectFrm, EditWaterFrame, editResultsFrame,
    editProbeFrame, editSolveFrame,
    editSedimentFrame, editFishFrame, editScenarioFrame,
    editFrequencyFrame, ResultsColourSet, Options,
    SQLiteTable3, HASPSecurity,
    PasswordChecking, dBSeaColours, helperFunctions, srcPoint,
    crossSecEndpoint, probe, soundSource, problemContainer, dBSeaOptions,
    scenario, dBSeaconstants, crossSecNameChange,
    eventlogging, logViewer, releaseNotes, fileSaveOpen,
    myRegistry, databaseUpdater, waterProps, mcklib,
    webGetHTTP, arraytoolFormUnit, globalsUnit, material,
    thresholdLevels, MyFrameInterface,
    solveReturn, crossSectionForm,
    mycompression, synlz, synlzo, MaintenanceStringParser,
    MSHTML, cloudSolve, UTMConversionForm, system.json,
    newDepth, IdComponent, IdBaseComponent, IdTCPConnection,
    IdTCPClient, IdHTTP, soundSpeedProfile, OtlTaskControl,
    mmatrix, mcomplex, rayVisibilityFrm, rayPlotting,
    SmartPointer, NOAACurves, MovingSourceGIFFormUnit, WMI_Info,
    SlackIntegration, IdIOHandler, IdIOHandlerSocket,
    IdIOHandlerStack, IdSSL, IdSSLOpenSSL {$IF CompilerVersion >= 29},System.ImageList {$IFEND};

type

    {$SCOPEDENUMS ON}
    TMyMenus = (FileMenu,EditMenu,ViewMenu,ToolsMenu,HelpMenu);
    {$SCOPEDENUMS OFF}

    TMainForm = class(TForm)
    mnuMainMenu: TMainMenu;
    File1: TMenuItem;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    Help1: TMenuItem;
    N1: TMenuItem;
    FileExitItem: TMenuItem;
    HelpAboutItem: TMenuItem;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    alstActionList: TActionList;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    FileNew1: TAction;
    FileSave1: TAction;
    FileExit1: TAction;
    FileOpen1: TAction;
    FileSaveAs1: TAction;
    WindowCascade1: TWindowCascade;
    WindowTileHorizontal1: TWindowTileHorizontal;
    WindowArrangeAll1: TWindowArrange;
    WindowMinimizeAll1: TWindowMinimizeAll;
    HelpAbout1: TAction;
    FileClose1: TWindowClose;
    WindowTileVertical1: TWindowTileVertical;
    tbPagesBar: TToolBar;
    ToolButtonOpen: TToolButton;
    ToolButtonSave: TToolButton;
    ToolButtonNew: TToolButton;
    toolbarImageList: TImageList;
    mainInfoPanel: TPanel;
    miAxes: TMenuItem;
    mainPageControl: TPageControl;
    Tools1: TMenuItem;
    Options1: TMenuItem;
    tbbtSolve: TToolButton;
    tbbtResetView: TToolButton;
    tbSetup: TToolButton;
    tbSources: TToolButton;
    tbWater: TToolButton;
    tbSeafloor: TToolButton;
    tbFish: TToolButton;
    tbProbe: TToolButton;
    tbResults: TToolButton;
    menuImageList: TImageList;
    HelpHelpItem: TMenuItem;
    frameScrollBox: TScrollBox;
    miLoadbathymetry: TMenuItem;
    apevntsAppEvents: TApplicationEvents;
    miContours: TMenuItem;
    miExportImage: TMenuItem;
    miExportBMP: TMenuItem;
    miExportJPG: TMenuItem;
    miExportPNG: TMenuItem;
    N4: TMenuItem;
    tbXSec: TToolButton;
    miExportLevels: TMenuItem;
    miExportXSecLevels: TMenuItem;
    miExportXSecLevelsAndSpectra: TMenuItem;
    N2: TMenuItem;
    miExportLevelsAndSpectra: TMenuItem;
    tbPreferences: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    miRecentFilesList: TMenuItem;
    Edit1: TMenuItem;
    Cut1: TMenuItem;
    Paste1: TMenuItem;
    Copy1: TMenuItem;
    actUndo: TAction;
    Undo1: TMenuItem;
    actRedo: TAction;
    N5: TMenuItem;
    miExportPDF: TMenuItem;
    N6: TMenuItem;
    miReverseHeightsAndDepths: TMenuItem;
    N7: TMenuItem;
    FileImportLicense: TMenuItem;
    N8: TMenuItem;
    meLaunchCSVEditor: TMenuItem;
    tbRuler: TToolButton;
    tbScenario: TToolButton;
    tbSolver: TToolButton;
    miRecallXSec: TMenuItem;
    miSaveXSec: TMenuItem;
    miNameXSec: TMenuItem;
    miExportESRISingleLayerLevs: TMenuItem;
    miExportESRIBathy: TMenuItem;
    meViewLog: TMenuItem;
    ttLicenseCheck: TTimer;
    meReleaseNotes: TMenuItem;
    N9: TMenuItem;
    miExportESRIAllLayerLevs: TMenuItem;
    miCheckForUpdates: TMenuItem;
    pnGrab: TPanel;
    sbStatusBar: TStatusBar;
    Export1: TMenuItem;
    miCreateArray: TMenuItem;
    miStripResultsFromFile: TMenuItem;
    N3: TMenuItem;
    miExportExclusionZone: TMenuItem;
    miScaleBar: TMenuItem;
    miGrid: TMenuItem;
    odBathymetry: TOpenDialog;
    odOpenUWA: TOpenDialog;
    sdSaveUWA: TSaveDialog;
    sdExportLevels: TSaveDialog;
    sdExportBathy: TSaveDialog;
    miExportGIF: TMenuItem;
    miExportMaxLevels: TMenuItem;
    miEditCrossSection: TMenuItem;
    miLatLngConversion: TMenuItem;
    miNewWithDepth: TMenuItem;
    N10: TMenuItem;
    miCanSelectSources: TMenuItem;
    miCanSelectProbes: TMenuItem;
    acShowRayVisibility: TAction;
    miRays: TMenuItem;
    miGIFTool: TMenuItem;
    miExportExclusionZoneShapefile: TMenuItem;
    sdExportShapefile: TSaveDialog;
    miMakeC2v: TMenuItem;
    sdC2V: TSaveDialog;
    miExportShapefileAllLayerLevs: TMenuItem;

        procedure FormCreate(Sender: TObject);
        procedure FormClose(Sender: TObject; var Action: TCloseAction);
        procedure FormDestroy(Sender: TObject);
        procedure FormResize(Sender: TObject);
        procedure FormShow(Sender: TObject);

        procedure FileNew1Execute(Sender: TObject);
        procedure HelpAbout1Execute(Sender: TObject);
        procedure FileExit1Execute(Sender: TObject);
        procedure Options1Click(Sender: TObject);
        procedure FileSaveItemClick(Sender: TObject);
        procedure FileSaveAs1Execute(Sender: TObject);
        procedure FileOpenItemClick(Sender: TObject);
        procedure VisibilityClick(Sender: TObject);
        procedure tbbtSolveClick(Sender: TObject);
        procedure tbbtResetViewClick(Sender: TObject);
        procedure ToolButtonPagesClick(Sender: TObject);
        procedure HelpHelpItemClick(Sender: TObject);
        procedure miLoadbathymetryClick(Sender: TObject);
        procedure miExportImageClick(Sender: TObject);
        procedure RecentFilesEntryOnclick(Sender: TObject);
        procedure FileImportLicenseClick(Sender: TObject);

        procedure apevntsAppEventsMessage(var Msg: tagMSG; var Handled: Boolean);
        procedure tbXSecClick(Sender: TObject);
        procedure miExportLevelsClick(Sender: TObject);
        procedure miExportESRIBathyClick(Sender: TObject);
        procedure tbPreferencesClick(Sender: TObject);
        procedure tbRulerClick(Sender: TObject);
        procedure actUndoExecute(Sender: TObject);
        procedure actRedoExecute(Sender: TObject);
        procedure miReverseHeightsAndDepthsClick(Sender: TObject);
        procedure meLaunchCSVEditorClick(Sender: TObject);
        procedure meViewLogClick(Sender: TObject);
        procedure ttLicenseCheckTimer(Sender: TObject);
        procedure meReleaseNotesClick(Sender: TObject);
        procedure miCheckForUpdatesClick(Sender: TObject);
        procedure pnGrabMouseEnter(Sender: TObject);
        procedure pnGrabMouseLeave(Sender: TObject);
        procedure pnGrabMouseUp(Sender: TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: Integer);
        procedure miCreateArrayClick(Sender: TObject);
        procedure miStripResultsFromFileClick(Sender: TObject);
        procedure miExportExclusionZoneClick(Sender: TObject);
        procedure miImportMaintenanceFile(Sender: TObject);
    procedure miEditCrossSectionClick(Sender: TObject);
    procedure miLatLngConversionClick(Sender: TObject);
    procedure miNewWithDepthClick(Sender: TObject);
    procedure miCanSelectSourcesClick(Sender: TObject);
    procedure miCanSelectProbesClick(Sender: TObject);
    procedure acShowRayVisibilityExecute(Sender: TObject);
    procedure miGIFToolClick(Sender: TObject);
    procedure miExportExclusionZoneShapefileClick(Sender: TObject);
    procedure miMakeC2vClick(Sender: TObject);
    private
    const
        iRecentFilesMenu        = 2;
        maxFrameHeight          = 920;
        maxFrameWidth           = 630;
    var
        slRecentFiles          : ISmart<TStringList>;
        updateTaskControl, queryDBTaskControl: IOmniTaskControl;

        procedure createFrames;
        procedure showmessageIfNotEmpty(const s: string);
        procedure RecentFilesListUpdate;
        procedure saveXSecOnclick(Sender: TObject);
        procedure recallXSecOnclick(Sender: TObject);
        procedure openFile(const openFileName: TFilename);
        procedure nameXSecOnclick(Sender: TObject);

        procedure clearProjectName();
        //class function  resourceFileUnbundle(const targetDir, filename, resID: string): Boolean;
        procedure updateScrollBox();
        procedure readReg;
    public
    var
        frames: IMyFrameList;

        procedure initXSecSaveRecallMenus;
        procedure RecentFilesListAdd(const aName: String);
        procedure updateVisibilityMenuItems(const componentVisibility: TBooleanDynArray);
        procedure showOptionsForm(const pageIndex: integer = -1);
        procedure updateSelectBoxesAndChartsAfterScenarioChange;
        procedure updateMainCaption;
        function  DBVersion(const bd: TDataSource): integer;
        function  activeFrame: IMyFrame;
        procedure didChangeScenario;
    end;

type
    {$SCOPEDENUMS ON}
    TMyPages = (Setup,Source,Water,Sediment,Fish,Solve,Probes,Results,Scenarios,Frequency);
    {$SCOPEDENUMS OFF}
var
    Mainform: TMainForm;

implementation

{$R *.dfm}

uses about;

// procedure TMainForm.SetAsActivePage(tabSheet: TTabSheet) ;
// begin
// if tabSheet.PageControl.ActivePage <> tabSheet then
// begin
// If THackPageControl(tabSheet.PageControl).CanChange then
// begin
// tabSheet.PageControl.ActivePage := tabSheet;
// THackPageControl(tabSheet.PageControl).Change;
// end
// else Abort;
// end;
// end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
    mr: integer;
begin
    GLNoDraw := true;

    if not trim(TFileSaveOpen.serialise(container,uwaopts)).Equals(trim(container.serialisedOnInit)) then
    begin
        mr := MessageDlg('Save changes to project?', mtConfirmation,[mbYes,mbNo,mbCancel],0);
        if (mrYes = mr) then
        begin
            if CurrentSaveFile = '' then
                FileSaveAs1Execute(nil)
            else
                showmessageIfNotEmpty( TFileSaveOpen.FileSaveWithFilename(CurrentSaveFile,container,UWAOpts,true) );
        end
        else if mr = mrCancel then
        begin
            action := caNone;
            exit;
        end;
    end;

    if assigned(queryDBTaskControl) then
    begin
        queryDBTaskControl.Terminate(1);
        queryDBTaskControl := nil;
    end;

    if assigned(updateTaskControl) then
    begin
        updateTaskControl.Terminate(1);
        updateTaskControl := nil;
    end;

    TSource.killExternalSolvers(); //kill any active solvers which may still be running if the program crashed
    FreeAndNil(GLForm);
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
    log: ISynLog;
    i: integer;
begin
    try
        //Application.OnException := eventLog.onApplicationException;

        licenseDBCheckStatus := kFinished;
        queryDBTaskControl := nil;
        //start checking the license server for license updates
        //queryDBTaskControl := TMaintenanceStringParser.queryDB(procedure (const task: IOmniTaskControl) begin
        //    queryDBTaskControl := nil;
        //end);

        eventlog.log(log, 'Set thread pool');
        //NB default maxWorkerThreads = 200, ie many small threads. Our solve threads are heavy cpu users, so limiting the number seems to help prevent crashes on solve
        TThreadpool(nil).Default.SetMaxWorkerThreads(2*TThread.ProcessorCount);

        inputDisabled := true;
        GLNoDraw := true;
        //isFloat('0.0',true); //initialise these functions with the local decimal separator
        //tofl('0.0',true);

        //eventLog := TEventLog.create();
        //eventLog.log('dBSea start');

        self.Color := TdBSeaColours.FormBackground;
        self.Font.Color := TdBSeaColours.Text;
        {other screens at different sizes may have different default theme font sizes, we override this. however, this
        doesn't work if the user has set text size = 125% or similar}
        self.Font.Size := 8;

        ShowScrollBar(Handle, SB_BOTH, false);

        self.updateMainCaption();

        eventlog.log(log, 'Check and create directories');
        {check for and create if necessary, a dBSea folder in the local temp dir area
         note LocalAppDataPath should be backwards compatible with XP}
        TempPath := IncludeTrailingPathDelimiter(THelper.getLocalAppDataPath);
        TempPath := TempPath + IncludeTrailingPathDelimiter(sTempPathLastComponent);
        if not directoryexists(TempPath) then
        begin
            eventLog.log('Create temp directory ' + TempPath);
            if not CreateDir(TempPath) then
            begin
                ShowMessage('Temp directory add failed with error: '+ toStr(GetLastError));
                eventLog.log('Temp directory add failed with error: '+ toStr(GetLastError));
            end;
        end;

        logsPath := TempPath + IncludeTrailingPathDelimiter('Logs');
        if not DirectoryExists(logsPath) then
        begin
            eventLog.log('Create logs directory ' + logsPath);
            if not CreateDir(logsPath) then
            begin
                ShowMessage('Logs directory create at ' + logsPath + ' failed. Error logging will not function and may cause further unhandled errors. Error: '+ toStr(GetLastError));
                eventLog.log('Logs directory create at ' + logsPath + ' failed. Error logging will not function and may cause further unhandled errors. Error: '+ toStr(GetLastError));
            end;
        end;
        TSynLogLevs.Family.Level := LOG_VERBOSE;
        TSynLogLevs.Family.LevelStackTrace := LOG_STACKTRACE;
        //TSynLogLevs.Family.PerThreadLog := true;
        //TSynLogLevs.Family.HighResolutionTimeStamp := true;
        TSynLogLevs.Family.AutoFlushTimeOut := 20;
        TSynLogLevs.Family.OnArchive := EventArchiveSynLZ;
        //TSynLogLevs.Family.OnArchive := EventArchiveZip;
        TSynLogLevs.Family.ArchiveAfterDays := 7;
        TSynLogLevs.Family.DestinationPath := logsPath;
        TSynLogLevs.Family.ArchivePath := logsPath;
        TSynLogLevs.Family.StackTraceLevel := 255;
        //TSynLogLevs.Family.IncludeComputerNameInFileName := false;

        log := TSynLogLevs.Enter(self, 'FormCreate');
        log.log(sllInfo, rawutf8('dBSea version: ' + TEventLog.completeVersionString()));
        eventLog.log(log, TOSVersion.ToString);
        eventLog.log(log, 'Graphics cards: ' + TWMI_Info.info);

        eventLog.log(log, inttostr(Screen.MonitorCount) + ' monitors');

        for I := 0 to screen.MonitorCount - 1 do
            eventLog.log(log, 'Monitor ' + inttostr(screen.Monitors[i].MonitorNum)
                + ': ' + inttostr(screen.Monitors[i].Width) + 'w '
                + inttostr(screen.Monitors[i].Height) + 'h '
                + 'primary: ' + ifthen(screen.Monitors[i].Primary,'y','n'));

        DefaultSavePath := THelper.GetMyDocumentsPath();

        globalsCreate(THelper.resourceFileUnbundle);

        //link to help file. nb uses HTMLHelpViewer
        eventlog.log(log, 'Link to help file');
        Application.HelpFile := ExtractFilePath(Application.ExeName) + dBSeaconstants.helpFile;

        //init the recent files list
        eventlog.log(log, 'Update recent files');
        self.slRecentFiles := TSmart<TStringlist>.create(TStringList.Create());
        self.RecentFilesListUpdate();

        eventLog.log(log, 'Check or write default bathymetry file');
        //write the default bathy data to the temp dir if it doesn't exist (as reading default bathy files from admin dirs is not easy)
        if not fileExists(TempPath + defaultBathymetryFile) then
            THelper.resourceFileUnbundle(TempPath, defaultBathymetryFile,'sampleBathymetry');
        if not fileExists(TempPath + defaultBathymetryFile) then
        begin
            eventlog.log('Error: Unable to create default bathymetry file ' + TempPath + defaultBathymetryFile);
            raise Exception.Create('Error: Unable to create default bathymetry file ' + TempPath + defaultBathymetryFile);
        end;
        TSource.killExternalSolvers(); //kill any active solvers which may still be running if the program crashed

        eventLog.log(log, 'Create problem container');
        container := TProblemContainer.Create();
        container.InitSampleProblem(nil, nil);

        eventLog.log(log, 'Read registry');
        self.readReg;

        eventLog.log(log, 'Create frames');
        self.frames := IMyFrameList.create;
        self.createFrames();
        eventLog.log(log, 'Set active frame');
        self.mainPageControl.ActivePageIndex := ord(TMyPages.Setup); {set the setup page as the currently active tab}

    //    //try open any file that might have been fed to the command line, or by double clicking on a .uwa file
    //    if paramcount > 0 then
    //    begin
    //        //showmessage(tostr(paramCount));
    //        //for i := 0 to paramCount do
    //        //    showmessage(paramstr(i));
    //
    //        openFile(paramstr(1));
    //    end;

        {$IFNDEF DEBUG}
        miRays.Visible := false;
        {$ENDIF}

        {$IF not (defined(VER2) or defined(DEBUG))}
        miGIFTool.Visible := false;
        miExportExclusionZoneShapefile.Visible := false;
        miExportShapefileAllLayerLevs.Visible := false;
        {$ENDIF}

        GLNoDraw := false;
        inputDisabled := false;
    except on e: Exception do
        eventlog.logException(log, e.tostring);
    end;
end;

procedure TMainform.readReg;
var
    dispFeet, resultsColormapSmooth: boolean;
    resultsAlpha: double;
    backgroundColor: TColor;
begin
    reg.readRegistry(self.RecentFilesListAdd, dispFeet, resultsColormapSmooth, resultsAlpha, backgroundColor); {read graphics and other options from reg}
    UWAOpts.dispFeet := dispFeet;
    UWAOpts.resultsColormapSmooth := resultsColormapSmooth;
    UWAOpts.resultsAlpha := resultsAlpha;
    UWAOpts.backgroundColor := backgroundColor;
end;

procedure TMainForm.createFrames;
    procedure myFrameSetup(const frame: TFrame; const iFrame: IMyFrame; const tabSheet: TTabSheet);
    begin
        frames.add(iFrame);
        frame.Align := alClient;
        frame.parent := tabSheet;
        tabSheet.PageControl := mainPageControl;
        tabSheet.TabVisible := false;
    end;
var
    ts: TTabSheet;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'createFrames');
    try
        {creating separate tabs in the tabbed PageControl - getting the forms from defined form files}
        ts := TTabSheet.Create(mainPageControl);
        framSetup := TSetupFrame.Create(ts);
        myFrameSetup(framSetup, framSetup, ts);
        ts.OnShow := framSetup.frameshow;
        ts.OnHide := framSetup.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framSource := TEditSourceFrame.Create(ts);
        myFrameSetup(framSource, framSource, ts);
        ts.OnShow := framSource.frameshow;
        ts.OnHide := framSource.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framWater := TEditWaterFrame.Create(ts);
        myFrameSetup(framWater, framWater, ts);
        ts.OnShow := framWater.frameshow;
        ts.OnHide := framWater.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framSediment := TEditSedimentFrame.Create(ts);
        myFrameSetup(framSediment, framSediment, ts);
        ts.OnShow := framSediment.frameshow;
        ts.OnHide := framSediment.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framFish := TEditFishFrame.Create(ts);
        myFrameSetup(framFish, framFish, ts);
        ts.OnShow := framFish.frameshow;
        ts.OnHide := framFish.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framSolve := TEditSolveFrame.Create(ts);
        myFrameSetup(framSolve, framSolve, ts);
        ts.OnShow := framSolve.frameshow;
        ts.OnHide := framSolve.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framProbe := TEditProbeFrm.Create(ts);
        myFrameSetup(framProbe, framProbe, ts);
        ts.OnShow := framProbe.frameshow;
        ts.OnHide := framProbe.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framResults := TEditResultsFrame.Create(ts);
        myFrameSetup(framResults, framResults, ts);
        ts.OnShow := framResults.frameshow;
        ts.OnHide := framResults.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framScenario := TScenarioFrame.Create(ts);
        myFrameSetup(framScenario, framScenario, ts);
        ts.OnShow := framScenario.frameshow;
        ts.OnHide := framScenario.framehide;

        ts := TTabSheet.Create(mainPageControl);
        framFrequency := TFrequencyFrame.Create(ts);
        myFrameSetup(framFrequency, framFrequency, ts);
        ts.OnShow := framFrequency.frameshow;
        ts.OnHide := nil;
    except on e: Exception do
        eventLog.logException(log, e.tosTring);
    end;
end;

function TMainForm.DBVersion(const bd: TDataSource): integer;
var
    sltb: ISmart<TSQLiteTable>;
begin
    if bd = TDataSource.dsMDA then
    begin
        try
            sltb := TSmart<TSQLiteTable>.create(MDASldb.GetTable(ansistring('SELECT * FROM ' + metaTblName)))
        except
            exit(-1);
        end;
    end
    else
    begin
        try
            sltb := TSmart<TSQLiteTable>.create(userSldb.GetTable(ansistring('SELECT * FROM ' + metaTblName)))
        except
            exit(-1);
        end;
    end;

    result := sltb.FieldAsInteger(sltb.FieldIndex['databaseVersion']);
end;

procedure TMainForm.acShowRayVisibilityExecute(Sender: TObject);
begin
    {$IFDEF DEBUG}
    RayVisibilityForm.startAngle := container.scenario.rayTracingStartAngle;
    RayVisibilityForm.endAngle := container.scenario.rayTracingEndAngle;
    RayVisibilityForm.ShowModal;
    GLForm.forceDrawNow;
    {$ENDIF}
end;

function TMainForm.activeFrame: IMyFrame;
begin
    case TMyPages(self.mainPageControl.ActivePageIndex) of
    TMyPages.Setup: result := framSetup;
    TMyPages.Source: result := framSource;
    TMyPages.Water: result := framWater;
    TMyPages.Sediment: result := framSediment;
    TMyPages.Fish: result := framFish;
    TMyPages.Solve: result := framSolve;
    TMyPages.Probes: result := framProbe;
    TMyPages.Results: result := framResults;
    TMyPages.Scenarios: result := framScenario;
    TMyPages.Frequency: result := framResults;
    else result := framSetup;
    end;
end;

procedure TMainform.didChangeScenario;
var
    frame: IMyFrame;
begin
    updateMainCaption;
    for frame in frames do
        frame.didChangeScenario;
end;

procedure TMainForm.actRedoExecute(Sender: TObject);
begin
//    if Self.ActiveControl is TCustomEdit then
//        TCustomEdit(Self.ActiveControl).Perform(EM_REDO, 0, 0);
end;

procedure TMainForm.actUndoExecute(Sender: TObject);
begin
    {certain components do not implement undo automatically. For descendants of
    TCustomedit (memo, richedit etc) we need to fire the undo message especially}
    if Self.ActiveControl is TCustomEdit then
    begin
        TCustomEdit(Self.ActiveControl).Perform(EM_UNDO, 0, 0);
        Exit();
    end;

    {undo source and probe moves}
    case lastAction of
        TLastAction.kNull: ;
        TLastAction.kMoveSource: if checkrange(framSource.cbSource.ItemIndex,0,container.scenario.sources.Count - 1) then
                if container.scenario.Sources[framSource.cbSource.ItemIndex].UndoMove() then
                    if assigned(GLForm) then
                        GLForm.forceDrawNow();
        TLastAction.kMoveSourceMovingPos: ;
        TLastAction.kMoveProbe: if checkrange(framProbe.cbProbe.ItemIndex,0,container.scenario.theRA.probes.Count - 1) then
                if container.scenario.theRA.probes[framProbe.cbProbe.ItemIndex].UndoMove() then
                    if assigned(GLForm) then
                        GLForm.forceDrawNow();
        TLastAction.kMoveCSEP: ;
    end;
end;



procedure TMainForm.apevntsAppEventsMessage(var Msg: tagMSG; var Handled: Boolean);
var
    Pt : TPoint;
    C  : TWinControl;
    //con: TControl;
begin
    if Msg.message = RegisterWindowMessage('TaskbarButtonCreated') then
    begin
        // InitializeTaskbarAPI;
    end;

    if Msg.message = WM_KEYDOWN then
    begin
        if inputDisabled then
            handled := true;
    end;


    {catch mouse wheel and send it to the item currently under the mouse cursor.
    NB this doesn't do exactly what we want, as it doesn't do scrolling at the moment,
    and it also doesn't let you scroll a selectbox if it is focused. but it does send
    GL the middle mouse button appropriately}
    if Msg.message = WM_MOUSEWHEEL then
    begin
        if inputDisabled then
            handled := true
        else
        begin
            Pt.X := Word(Msg.lParam);
            Pt.Y := HiWord(Msg.lParam);
            C := FindVCLWindow(Pt);
            if C = nil then
                Handled := True
            else
                if C.Handle <> Msg.hwnd then
                begin
                    Handled := True;
                    SendMessage(C.Handle, WM_MOUSEWHEEL, Msg.wParam, Msg.lParam);
                end;
        end;
    end;

    if Msg.message = WM_RBUTTONDOWN then
    begin
        //this disables mouse rightclick input
        if inputDisabled then
            handled := true;
    end;

    if Msg.message = WM_LBUTTONDOWN then
    begin
        //this disables mouse leftclick input
        if inputDisabled then
            handled := true;


//        Pt.X := Word(Msg.lParam);
//        Pt.Y := HiWord(Msg.lParam);
//        con := FindDragTarget(pt,false);
//        if con <> nil then
//            if con is TComboBox then
//            begin
//                if not TComboBox(con).DroppedDown then
//                begin
//                    TComboBox(con).DroppedDown := true;
//                    handled := true;
//                end;
//            end;
    end;

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
    eventLog.log('dBSea shutdown');
    // wglMakeCurrent(Canvas.Handle, 0); //make main window context current
    // //wglMakeCurrent( NULL, NULL ); //example of making no opengl context active
    // wglDeleteContext(GLContext);     //release openGL render context
    // ReleaseDC( handle, glDC );       //release openGL device context

    reg.writeRegistry(slRecentFiles, UWAOpts.dispFeet, UWAOpts.resultsColormapSmooth, UWAOpts.resultsAlpha, UWAOpts.backgroundColor); //write graphics options etc to registry

    //eventLog.Free();
    globalsDestroy;

    container.Free;
    //UWAOpts.Free;

    //controls already destroyed by the time the frames are destroyed, so need to clean up here
    TThresholdLevels.clearThresholdsBoxObjects(framSource.cbThresholds);
    framWater.clearSSPObjects;
    framWater.clearWaterPropsObjects;
    framSediment.clearSedimentsBoxObjects;
    framFish.clearFishCurves;

    frames.Clear;
    frames.free;
end;

procedure TMainForm.updateMainCaption;
var
    scenarioName: String;
    runningModeString: string;
    dBSeaVersion: string;
begin

    case runningMode of
        rmDemo: RunningModeString := '   DEMO MODE  ';
        rmUnlicensed: RunningModeString := '   UNLICENSED MODE  ';
        rmTrial: RunningModeString := '   TRIAL MODE  ';
    else
        RunningModeString := '';
    end;

    if not assigned(container) then
        scenarioName := ''
    else if container.scenarios.Count < 2 then
        scenarioName := '' //only 1 scenario, no need to show the name
    else
        scenarioName := '    Scenario: ' + container.scenario.Name;

    dBSeaVersion := majorVersion(application.ExeName);
    self.caption := currentProjectName + ' - ' + sProgramName + ' ' + dBSeaVersion +
                    {$IFDEF CPUX64}
                    ' 64 bit ' +
                    {$ENDIF}
                    titleDebugString + RunningModeString + scenarioName;
end;

procedure TMainForm.HelpAbout1Execute(Sender: TObject);
begin
    if assigned(AboutBox) and assigned(theHASPSecurity) then
    begin
        theHASPSecurity.updateAboutBoxCaptions(AboutBox.lblSerialNo, AboutBox.lblComments, AboutBox.lblExpiryDate);
        //AboutBox.lbBuild.Caption := 'Build: ' + fileVersion(application.ExeName);
        //AboutBox.lblVersion.Caption := 'Version ' + majorVersion(application.ExeName);
        AboutBox.ShowModal();
    end;
end;

procedure TMainForm.HelpHelpItemClick(Sender: TObject);
begin
    HtmlHelp(0, Application.HelpFile, HH_DISPLAY_TOC, 0);
end;

procedure TMainForm.miLatLngConversionClick(Sender: TObject);
begin
    UTMConversionFrm.Show;
end;

procedure TMainForm.miLoadbathymetryClick(Sender: TObject);
begin
    if not assigned(container) then Exit();
    if container.isSolving then Exit();

    if runningMode = rmDemo then
    begin
        showmessage('Demo mode. Bathymetry import is disabled');
        Exit();
    end;

    if not odBathymetry.Execute then exit;

    if not FileExists(odBathymetry.filename) then
    begin
        Showmessage('File does not exist');
        Exit();
    end;

    if THelper.IsFileInUse(odBathymetry.filename) then
    begin
        Showmessage('File could not be opened as it is in use by another process');
        Exit();
    end;

    {read bathymetry from the file}
    eventLog.log('Load bathymetry from file ' + odBathymetry.filename);
    mainPageControl.ActivePageIndex := ord(TMyPages.Solve);
    container.bathymetryFromFile(odBathymetry.filename,framsolve.lbSolving,framSolve.pbSolveProgress);
    mainPageControl.ActivePageIndex := ord(TMyPages.Setup);

    clearProjectName(); //could use the name of the imported file

    if assigned(framSetup) then
        framSetup.EditBoxesInit();

    if assigned(framWater) then
    begin
        framWater.SSPSelectBoxChange(nil);
        framWater.updateSSPChart;
    end;

    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := false;
        GLForm.invalidateBathyTexture();
        GLForm.setRedrawBathymetry();
        GLForm.force2drawNow();
    end;
end;

procedure TMainForm.miMakeC2vClick(Sender: TObject);
var
    s: string;
begin
    s := theHASPSecurity.c2v;

    if s = '' then
    begin
        showmessage('Unable to get key info');
        exit;
    end;

    if sdC2V.Execute then
        THelper.writeStringToFile(sdC2V.FileName, s);
end;

procedure TMainForm.miNewWithDepthClick(Sender: TObject);
begin
    if container.isSolving then Exit();

    if NewDepthForm.ShowModal <> mrOK then exit;

    GLNoDraw := true;
    ToolButtonNew.Enabled := false;
    framWater.chartsShouldUpdate := false;
    try
        clearProjectName();
        container.clearAndMakeNew(tofl(NewDepthForm.edDepth.Text));
        updateSelectBoxesAndChartsAfterScenarioChange();
        updateMainCaption();

        mainPageControl.ActivePageIndex := ord(TMyPages.Setup);
        framSetup.EditBoxesInit();
        RecentFilesListUpdate();
    finally
        framWater.chartsShouldUpdate := false;
        ToolButtonNew.Enabled := true;
        GLNoDraw := false;
    end;

    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := false; //drawing fresh prob in xsec causes fail
        GLForm.invalidateBathyTexture;
        GLForm.setRedrawBathymetry();
        GLForm.Force2drawNow();
    end;
end;

procedure TMainForm.meLaunchCSVEditorClick(Sender: TObject);
var
    filename: TFilename;
begin
    //launch the CSV Editor. Executables will be in same dir after install
    eventLog.log('Launch CSV Editor');
    filename := includeTrailingPathDelimiter(AppPath) + CSVEditorEXEFile;
    ShellExecute(handle,'open',PChar(filename), '','',SW_SHOWNORMAL);
end;

procedure TMainForm.meReleaseNotesClick(Sender: TObject);
begin
    ReleaseNotesForm.ShowModal();
end;

procedure TMainForm.meViewLogClick(Sender: TObject);
begin
    eventLogViewerForm.ShowModal();
end;

procedure TMainForm.miCanSelectProbesClick(Sender: TObject);
begin
    miCanSelectProbes.Checked := not miCanSelectProbes.Checked;
    UWAOpts.canSelectProbes := miCanSelectProbes.Checked;
end;

procedure TMainForm.miCanSelectSourcesClick(Sender: TObject);
begin
    miCanSelectSources.Checked := not miCanSelectSources.Checked;
    UWAOpts.canSelectSources := miCanSelectSources.Checked;
end;

procedure TMainForm.miCheckForUpdatesClick(Sender: TObject);
begin
    updateTaskControl := nil;
    updateTaskControl := TWebGetHTTP.checkForNewVersionInBackground(true, procedure (const task: IOmniTaskControl) begin
        updateTaskControl := nil;
    end);
end;

procedure TMainForm.miCreateArrayClick(Sender: TObject);
begin
    if arrayToolForm.ShowModal = mrOk then
    begin
        GLForm.forceDrawNow;
        framSource.cbSourceInit;
        framProbe.cbProbeInit;
    end;
end;

procedure TMainForm.miExportImageClick(Sender: TObject);
var
    bProbeVisible: boolean;
begin
    {we may want to remove the probes from the view. store the current probe visibility}
    bProbeVisible := UWAOpts.ComponentVisibility[ord(veProbes)];

    {if needed, hide the probe and redraw GL}
    if bProbeVisible and UWAOpts.bHideProbesOnExport then
    begin
        UWAOpts.setVisibility(ord(veProbes),false); //includes drawnow
        GLForm.didChangeVisibility(ord(veProbes));
    end;

    {flush any pending GL stuff}
    GLForm.Invalidate();
    {go to export routine}
    GLForm.ExportCurrentScene(TMenuItem(sender).tag);

    {put visibility back the way it was}
    UWAOpts.setVisibility(ord(veProbes),bProbeVisible); //includes drawnow
    GLForm.didChangeVisibility(ord(veProbes));
end;

procedure TMainForm.miReverseHeightsAndDepthsClick(Sender: TObject);
begin
    if not assigned(container) then Exit();
    if container.isSolving then Exit();


    if not (container.bathymetry.ZDepths.min < 0) then
    begin
        showmessage('Cannot swap heights and depths for this dataset');
        Exit();
    end;

    container.bathymetry.reverseDepths;
    container.bathymetry.updateZMax();
    //container.bathymetry.updateGDepthsOld;

    if assigned(GLForm) then
    begin
        GLForm.setRedrawBathymetry();
        GLForm.invalidateBathyTexture();
        GLForm.ForceDrawNow();
    end;
end;

procedure TMainForm.miStripResultsFromFileClick(Sender: TObject);
begin
    if not directoryExists(DefaultSavePath) then
        DefaultSavePath := THelper.GetMyDocumentsPath;
    odOpenUWA.InitialDir := DefaultSavePath;
    if odOpenUWA.Execute() then
    begin
        sdSaveUWA.InitialDir := ExtractFilePath(odOpenUWA.FileName);
        if sdSaveUWA.Execute then
            TFileSaveOpen.FileStripResults(odOpenUWA.FileName,sdSaveUWA.filename);
    end;
end;

procedure TMainForm.RecentFilesListUpdate;
var
    RecentFileMenu: TMenuItem;
    SubItem: TMenuItem;
    i: integer;
    theFileName: TFilename;
begin
    if not assigned(mnuMainMenu) then Exit();

    RecentFileMenu := mnuMainMenu.Items[ord(TMyMenus.FileMenu)][iRecentFilesMenu];
    RecentFileMenu.Clear();
    for I := 0 to slRecentFiles.Count - 1 do
        if i < iRecentFilesMaxLength then
        begin
            theFileName := slRecentFiles[i];
            SubItem := TMenuItem.Create(self);
            SubItem.Caption := theFileName;
            SubItem.Tag := i;
            SubItem.OnClick := RecentFilesEntryOnclick;
            {if on a local drive, check if the file exists. on remote drives
            don't do this, as searching on a network might take a long time}
            if GetDriveType(PChar(ExtractFileDrive(theFileName))) = DRIVE_FIXED then
                SubItem.Enabled := fileExists(theFileName)
            else
                SubItem.Enabled := true;
            RecentFileMenu.Add(SubItem);
        end;
end;

procedure TMainForm.initXSecSaveRecallMenus;
var
    parentMenu: TMenuItem;
    SubItem: TMenuItem;
    i: integer;
begin
    if not assigned(mnuMainMenu) then Exit;
    if not assigned(container) then exit;

    parentMenu := miSaveXSec;
    parentMenu.Clear;
    for I := 0 to length(container.savedCrossSecs) - 1 do
    begin
        SubItem := TMenuItem.Create(self);
        SubItem.Caption := tostr(i + 1) + ' ' + container.savedCrossSecs[i].Name;
        SubItem.Tag := i;
        SubItem.OnClick := saveXSecOnclick;
        parentMenu.Add(SubItem);
    end;

    parentMenu := miRecallXSec;
    parentMenu.Clear;
    for I := 0 to length(container.savedCrossSecs) - 1 do
    begin
        SubItem := TMenuItem.Create(self);
        SubItem.Caption := tostr(i + 1) + ' ' + container.savedCrossSecs[i].Name;
        SubItem.Tag := i;
        SubItem.OnClick := recallXSecOnclick;
        SubItem.Enabled := container.savedCrossSecs[i].crossSecReady;
        parentMenu.Add(SubItem);
    end;

    parentMenu := miNameXSec;
    parentMenu.Clear;
    for I := 0 to length(container.savedCrossSecs) - 1 do
    begin
        SubItem := TMenuItem.Create(self);
        SubItem.Caption := tostr(i + 1) + ' ' + container.savedCrossSecs[i].Name;
        SubItem.Tag := i;
        SubItem.OnClick := nameXSecOnclick;
        parentMenu.Add(SubItem);
    end;
end;

procedure TMainForm.saveXSecOnclick(Sender: TObject);
begin
    container.saveXSec(TMenuItem(sender).Tag);
    initXSecSaveRecallMenus();
end;

procedure TMainForm.recallXSecOnclick(Sender: TObject);
begin
    container.recallXSec(TMenuItem(sender).Tag);
    initXSecSaveRecallMenus();
    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := true;
        GLForm.force2DrawNow();
    end;
end;

procedure TMainForm.nameXSecOnclick(Sender: TObject);
begin
    if (TMenuItem(sender).Tag < 0) or (TMenuItem(sender).Tag >= length(container.savedCrossSecs)) then exit();

    crossSecNameChangeFrm.edName.Text := container.savedCrossSecs[TMenuItem(sender).Tag].Name;

    if crossSecNameChangeFrm.ShowModal = mrOK then
    begin
        container.nameXSec(TMenuItem(sender).Tag,crossSecNameChangeFrm.edName.Text);
        initXSecSaveRecallMenus();
    end;
end;

procedure TMainForm.RecentFilesEntryOnclick(Sender: TObject);
begin
    if container.isSolving then Exit();

    if sender is TMenuItem then
        openFile(slRecentFiles[TMenuItem(Sender).tag]);
end;

procedure TMainForm.RecentFilesListAdd(const aName: String);
var
    i: integer;
begin
    {check if the given filename is already in the list, if so, delete that entry.
    NB delete changes stringlist length, hence the repeated range checking}
    for i := slRecentFiles.Count - 1 downto 0 do
        if ansicomparetext(aName,slRecentFiles[i]) = 0 then
            slRecentFiles.Delete(i);

    {insert filename at the start of the list}
    slRecentFiles.Insert(0,aName);

    {if the list is too long, trim the highest entries}
    while slRecentFiles.Count > iRecentFilesMaxLength do
        slRecentFiles.Delete(slRecentFiles.Count - 1);
    RecentFilesListUpdate();
end;

procedure TMainForm.Options1Click(Sender: TObject);
begin
    ShowOptionsForm();
end;

procedure TMainForm.pnGrabMouseEnter(Sender: TObject);
begin
    screen.Cursor := crHSplit;
end;

procedure TMainForm.pnGrabMouseLeave(Sender: TObject);
begin
    screen.Cursor := crDefault;
end;

procedure TMainForm.pnGrabMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    self.mainInfoPanel.Left := min(self.ClientWidth - 50,max(50,self.mainInfoPanel.Left + x));
    self.mainInfoPanel.Width := self.ClientWidth - self.mainInfoPanel.Left;
    GLForm.Width := self.mainInfoPanel.left;
end;

procedure TMainForm.ShowOptionsForm(const pageIndex: integer = -1);
begin
    optionsForm.tempPath := TempPath;
    optionsForm.projName := CurrentProjectName;
    optionsForm.LoadLogoImage();
    OptionsForm.bShouldUpdateResults := false;

    if pageIndex >= 0 then
        optionsForm.pcOptionsTabs.ActivePageIndex := pageIndex;
    if optionsForm.showmodal = mrOK then
    begin
        TempPath := optionsForm.tempPath;
        CurrentProjectName := optionsForm.projName;
        updateMainCaption();

        if OptionsForm.bShouldUpdateResults then
        begin
            GLForm.setRedrawResults;
            GLForm.forceDrawNow;
        end;
    end;
end;

procedure TMainForm.FileExit1Execute(Sender: TObject);
begin
    GLNoDraw := true;
    Close();
end;

procedure TMainForm.FileImportLicenseClick(Sender: TObject);
begin
    theHASPSecurity.ImportLicenseFile();
    updateMainCaption();
end;

procedure TMainForm.FormResize(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'FormResize');
    {resize the info panel and gl window to cope with the new form size}

    mainInfoPanel.Height := self.ClientHeight - tbPagesBar.Height - sbStatusBar.Height;
    mainInfoPanel.Top := tbPagesBar.Height - 2;
    mainInfoPanel.Width := safetrunc(self.ClientWidth * 0.4);
    if mainInfoPanel.Width > (maxFrameWidth + 40) then
        mainInfoPanel.Width := maxFrameWidth + 40;
    mainInfoPanel.Left := self.ClientWidth - mainInfoPanel.Width;

    updateScrollBox();
    ShowScrollBar(Handle, SB_BOTH, false);

    if assigned(GLForm) then
    //if assigned(GLForm) and not exiting then // exiting is there because there was an annoying bug on closing the mainform to do with resize being called after GLWin was destroyed
    begin
        GLForm.Left      := 0;
        GLForm.Width     := mainInfoPanel.Left - GLForm.Left;
        GLForm.Top       := 0;
        GLForm.Height    := ClientHeight - sbStatusBar.Height - tbPagesBar.Height - GLForm.Top;
        //GLWin.resize(self);
    end;

    eventLog.log(log, 'Form: ' + inttostr(self.left) + 'L '
        + inttostr(self.Top) + 'T '
        + inttostr(self.Width) + 'W '
        + inttostr(self.Height) + 'H ');
end;

{$DEFINE SKIPHASPCHECK}
procedure TMainForm.FormShow(Sender: TObject);
var
{$IF Defined(DEBUG) and Defined(SKIPHASPCHECK)}{$ELSE}
    haspReturnMessages: String;

{$ENDIF}
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'FormShow');
    try

    //    try
    //        raise Exception.Create('Error Message fdas fdsaf das ');
    //    except on e: Exception do
    //    begin
    //        eventLog.log(log, 'ST: ' + e.StackTrace);
    //        eventLog.logException(log, e.ToString);
    //    end;
    //    end;

        {$IFDEF DEBUG}
            {GPU Ray testing}
            //bUseGPU := true;
            //bMultiCoreRay := false;
            //container.scenario.solver := cmdBSeaRay;
            //container.scenario.setSourcesNSlices(1);

            //TCloudSolve.post('http://ec2-52-90-170-75.compute-1.amazonaws.com:8080', THelper.readFileToString('C:\Users\martin\Documents\node\ram.in'));
            //TCloudSolve.serverAvailable(sSolveServer);
        {$ENDIF}

        {$IF Defined(DEBUG) and Defined(SKIPHASPCHECK)}
            runningMode := rmNormalKey;
        {$ELSEIF Defined(LITE)}
            runningMode := rmLite;
        {$ELSE}
            runningMode := rmDemo;
            try
                theHASPSecurity.HASPFormOnShowChecks(nil, nil, nil, haspReturnMessages);
            except on e: exception do
                eventLog.log('Exception on HASPFormOnShowChecks ' + e.ToString, log);
            end;
            showmessageIfNotEmpty(haspReturnMessages);
        {$ENDIF}

        if runningMode = rmDemo then
        begin
            self.FileSaveItem.Enabled := false;
            self.FileSaveAsItem.Enabled := false;
            self.FileOpenItem.Enabled := false;
            self.miLoadbathymetry.Enabled := false;
            showmessage('Trial license not found or trial period finished, running dBSea demo version. ' +
                        'Bathymetry import, exporting, and file opening and saving are disabled');
        end;

        eventLog.log(log, 'Check parameter string');
        //try open any file that might have been fed to the command line, or by double clicking on a .uwa file
        if paramcount > 0 then
        begin
            //showmessage(tostr(paramCount));
            //for i := 0 to paramCount do
            //    showmessage(paramstr(i));

            openFile(paramstr(1));
        end;

        eventLog.log(log, 'Remove solver entries');
        framFrequency.removeSolverEntries;
        updateMainCaption;

        if runningMode = rmBlocked then
        begin
            eventLog.log(log, 'BLOCKED, terminating');
            showmessage('Error 116');
            application.Terminate();
        end;

        eventLog.log(log, 'Check file associations');
        reg.checkUWAFileAssociation();

        eventLog.log(log, 'Init menus');
        UpdateVisibilityMenuItems(UWAOpts.ComponentVisibility);
        initXSecSaveRecallMenus();

        miCanSelectSources.Checked := UWAOpts.canSelectSources;
        miCanSelectProbes.Checked := UWAOpts.canSelectProbes;

        eventLog.log(log, 'Check for new version');
        updateTaskControl := nil;
        updateTaskControl := TWebGetHTTP.checkForNewVersionInBackground(false, procedure (const task: IOmniTaskControl) begin
            updateTaskControl := nil;
        end);

        eventLog.log(log, 'Update NOAA curves');
        if TDBUpdater.updateNOAACurves(MDASldb, eventLog) then
            framFish.weightingSelectBoxInit();

         container.serialisedOnInit := TFileSaveOpen.serialise(container,uwaopts);

    except on e: Exception do
        eventlog.logException(log, e.toString);
    end;
end;

procedure TMainForm.FileNew1Execute(Sender: TObject);
begin
    if container.isSolving then Exit();

    GLNoDraw := true;
    ToolButtonNew.Enabled := false;
    framWater.chartsShouldUpdate := false;
    try
        clearProjectName();
        container.clearAndMakeNew();
        container.serialisedOnInit := TFileSaveOpen.serialise(container, UWAOpts);
        updateSelectBoxesAndChartsAfterScenarioChange();
        updateMainCaption();

        mainPageControl.ActivePageIndex := ord(TMyPages.Setup);
        framSetup.EditBoxesInit();
        RecentFilesListUpdate();
    finally
        framWater.chartsShouldUpdate := false;
        ToolButtonNew.Enabled := true;
        GLNoDraw := false;
    end;

    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := false; //drawing fresh prob in xsec causes fail
        GLForm.invalidateBathyTexture;
        GLForm.setRedrawBathymetry();
        GLForm.Force2drawNow();
    end;
end;

procedure TMainForm.clearProjectName;
begin
    currentProjectName := '(untitled project)';
    currentSaveFile := '';
    updateMainCaption();
end;

procedure TMainForm.FileSaveAs1Execute(Sender: TObject);
begin
    if container.isSolving then Exit();

    if runningMode = rmDemo then
    begin
        showmessage('Demo version: saving is disabled');
        Exit;
    end;

    if runningMode = rmTrial then
    begin
        //showmessage('Trial version: saving is disabled');
        //Exit;
    end;

    {$IFNDEF DEBUG}
    if (runningMode = rmNormalKey) and not theHASPSecurity.HASPFormWhileRunningChecks then
    begin
        showmessage('Valid HASP key not found, saving disabled');
        exit();
    end;
    {$ENDIF}

    if not directoryExists(DefaultSavePath) then
        DefaultSavePath := THelper.GetMyDocumentsPath;
    sdSaveUWA.InitialDir := defaultSavePath;
    if not sdSaveUWA.Execute then exit;

    if not THelper.IsValidFileName(ExtractFileName (sdSaveUWA.filename)) then
    begin
        Showmessage('File name was not valid (may contain system reserved characters)');
        Exit;
    end;

    defaultSavePath := ExtractFilePath(sdSaveUWA.filename);
    currentProjectName := sdSaveUWA.filename;
    updateMainCaption();
    {add this filename to the recent files list, and update the menu}
    RecentFilesListAdd(sdSaveUWA.filename);

    currentSaveFile := sdSaveUWA.filename;
    showmessageIfNotEmpty( TFileSaveOpen.FileSaveWithFilename(CurrentSaveFile,container,UWAOpts,true) );
end;

procedure TMainForm.FileSaveItemClick(Sender: TObject);
begin
    if container.isSolving then Exit;

    {check if there is currently a named save file. If so, then just save over that
    filename. If not, we need to open the 'save as' dialog}
    if CurrentSaveFile = '' then
        FileSaveAs1Execute(nil)
    else
        showmessageIfNotEmpty( TFileSaveOpen.FileSaveWithFilename(CurrentSaveFile,container,UWAOpts,true) );
end;

procedure TMainForm.FileOpenItemClick(Sender: TObject);
begin
    if container.isSolving then Exit;

    if runningMode = rmDemo then
    begin
        showmessage('dBSea demo mode. Opening files is disabled');
        exit;
    end;

    if not directoryExists(DefaultSavePath) then
        DefaultSavePath := THelper.GetMyDocumentsPath;
    odOpenUWA.InitialDir := DefaultSavePath;
    if not odOpenUWA.Execute() then Exit();

    openFile(odOpenUWA.FileName);
end;

procedure TMainForm.openFile(const openFileName: TFilename);
begin
    showmessageIfNotEmpty(TFileSaveOpen.FileOpenWithFilename(openFileName,container,UWAOpts,true));
    reg.writeRegistry(slRecentFiles, UWAOpts.dispFeet, UWAOpts.resultsColormapSmooth, UWAOpts.resultsAlpha, UWAOpts.backgroundColor);
end;

procedure TMainForm.updateSelectBoxesAndChartsAfterScenarioChange;
begin
    {need to update the select boxes on the source and probes pages, so they take account of
    how many sources/probes we have now. If not count > 0, then this will cause a problem.
    we should have some way of checking for this, and if so then creating at least 1 source/probe}
    if assigned(framSource) then
    begin
        framSource.cbSourceInit();
        if framSource.cbSource.Items.Count > 0 then
            framSource.cbSource.ItemIndex := 0;
    end;
    if assigned(framProbe) then
    begin
        framProbe.cbProbeInit();
        if framProbe.cbProbe.Items.Count > 0 then
            framProbe.cbProbe.ItemIndex := 0;
    end;
    if assigned(framWater) then
    begin
        framWater.frameshow(nil);
        framWater.updateSSPChart();
        framWater.updateAttenuationChart();
        framWater.updatePBDirection();
    end;

    miCanSelectSources.Checked := UWAOpts.canSelectSources;
    miCanSelectProbes.Checked := UWAOpts.canSelectProbes;
end;

procedure TMainForm.UpdateVisibilityMenuItems(const componentVisibility: TBooleanDynArray);
const
    //TODO keep this list updated if you add new items
    {list of the names of the menu items. the order must match the order in TVisibilityEnum}
    VisibilityItems: array [0..11] of string = ('miAxes','miWater','miBathymetry','miSources','miProbes','miReceiverAreas',
                                                'miSoundLevel','miContours','miEndpoints','miScaleBar','miGrid','miRays');
var
    foundItem: TMenuItem;
    i: integer;
begin
    if not assigned(mnuMainMenu) then Exit;

    if not (length(VisibilityItems) - 1) = integer(high(TVisibilityEnum)) then
    begin
        raise Exception.Create('Incorrect length of component visibility array');
        Exit;
    end;

    {make sure that the checkmarks on the visibility menu items match the current state of those items}
    for i := 0 to length(VisibilityItems) - 1 do
    begin
        {if a listed item is not in the menu, then FindMenuItem returns nil}
        foundItem := THelper.findMenuItem(mnuMainMenu.Items[ord(TMyMenus.ViewMenu)],VisibilityItems[i]);
        if assigned(foundItem) then
            foundItem.checked := componentVisibility[i];
    end;
end;

procedure TMainForm.VisibilityClick(Sender: TObject);
var
    ind: integer;
begin
    ind := TMenuItem(Sender).tag;

    UWAOpts.setVisibility(ind,not UWAOpts.componentVisibility[ind]);
    UpdateVisibilityMenuItems(UWAOpts.componentVisibility);

    if assigned(GLForm) then
    begin
        GLForm.didChangeVisibility(ind);
        GLForm.forceDrawNow();
    end;
end;

procedure TMainForm.tbbtResetViewClick(Sender: TObject);
begin
    tbbtResetView.Enabled := false;
    try
        if assigned(GLForm) then
        begin
            GLForm.bDrawCrossSection := false;
            GLForm.resetView();
            GLForm.force2DrawNow();
        end;
    finally
        tbbtResetView.Enabled := true;
    end;
end;

procedure TMainForm.tbbtSolveClick(Sender: TObject);
var
    solveReturn: ISmart<TSolveReturn>;
    Log: ISynLog;
begin
    if container.isSolving then
    begin
        mainPageControl.ActivePageIndex := ord(TMyPages.Solve); // go to solve info page
        Exit();
    end;

    Log := TSynLogLevs.Enter(self,'tbbtSolveClick');
    solveReturn := TSmart<TSolveReturn>.create(TSolveReturn.create);
    framSource.clearSourceThreshold();

    GLNoDraw := true;
    sliceRayss.clear;
    mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Solve);
    framSolve.initSolveUI(container.scenario);
    framSolve.SolveTimeEstimator.SolveTimerStart;
    try
        {----------------SOLVE----------------}
        container.SolveNow(framSolve.mmSolverErrors,framSolve.lbScenarioSolveInfo,framsolve.SolveTimeEstimator,
                           framSolve.lbSolving,framSolve.lbSolveExtraInfo,framSolve.lbSliceNumber,framSolve.lbSolveSourcePosition,
                           framSolve.stepProgressBars,
                           framSolve.setProgressBars,
                           framSolve.mmSolverMessages,
                           framSolve.UpdateRemainingSolveTime,
                           solveReturn);
        {-------------------------------------}

        framResults.mmSolverErrors.Visible := length(framSolve.mmSolverErrors.Text) <> 0;
        framResults.mmSolverErrors.Text := framSolve.mmSolverErrors.Text;
    finally
        GLNoDraw := false;
        framSolve.endSolveUI;
    end;

    if container.cancelSolve.b then
        log.log(sllInfo,'Solve cancelled by user');

    if solveReturn.success then
    begin
        if UWAOpts.AutosaveAfterSolve and (CurrentSaveFile <> '') and not container.cancelSolve.b then
            showmessageIfNotEmpty( TFileSaveOpen.fileSaveWithFilename(CurrentSaveFile,container,UWAOpts,true) );

        mainPageControl.ActivePageIndex := ord(TMyPages.Results);
        if not container.cancelSolve.b then
        begin
            log.Log(sllInfo,'Solved, call redraw GL');
            GLForm.setRedrawResults;
        end;
        GLForm.force2DrawNow();
    end
    else if solveReturn.hasError(kNoScenariosEnabled) then
        mainPageControl.ActivePageIndex := ord(TMyPages.Scenarios);
end;

procedure TMainForm.tbPreferencesClick(Sender: TObject);
begin
    tbPreferences.Enabled := false;
    try
        Options1Click(nil);
    finally
        tbPreferences.Enabled := true;
    end;
end;

procedure TMainForm.tbRulerClick(Sender: TObject);
begin
    tbRuler.Enabled := false;
    try
        if assigned(GLForm) then
            GLForm.SetRuler();
    finally
        tbRuler.Enabled := true;
    end;
end;

procedure TMainForm.tbXSecClick(Sender: TObject);
var
    Log: ISynLog;
    shouldRedrawGL: boolean;
begin
    Log := TSynLogLevs.Enter(self,'tbXSecClick');

    tbXSec.Enabled := false;
    try
        {check if the cross section is ready to show. if not, we need to do setup
        before showing it.}
        if not container.CrossSecReady(shouldRedrawGL) then
        begin
            if shouldRedrawGL and Assigned(GLForm) then
                GLForm.setRedraw;
            framSetup.btSetupCrossSecClick(self);
        end
        else
        begin
            if assigned(UWAOpts) then
                UWAOpts.setVisibility(ord(veEndpoints),false); {we no longer need to show the endpoints}

            if assigned(GLForm) then
            begin
                GLForm.bDrawCrossSection := true;
                GLForm.didChangeVisibility(ord(veEndpoints));
                GLForm.forceDrawNow();
            end;
        end;
    finally
        tbXSec.Enabled := true;
    end;
end;

procedure TMainForm.ToolButtonPagesClick(Sender: TObject);
begin
    self.mainPageControl.ActivePageIndex := TToolButton(Sender).tag;
    updateScrollBox();
end;

procedure TMainForm.ttLicenseCheckTimer(Sender: TObject);
begin
    {$IF Defined(LITE)}
    exit;
    {$ENDIF}

    {$IFNDEF DEBUG}
    if (runningMode = rmNormalKey) and (not theHASPSecurity.HASPFormWhileRunningChecks) then
    begin
        showmessage('Valid HASP key not found, setting trial mode');
        runningMode := rmTrial;
        self.ttLicenseCheck.Interval := 1000 * 60; // 1 minute
    end
    else if (runningMode = rmTrial) and theHASPSecurity.HASPFormWhileRunningChecks then
    begin
        showmessage('HASP key found, setting normal key mode');
        runningMode := rmNormalKey;
        self.ttLicenseCheck.Interval := 1000 * 180; // 3 minutes
    end;
    {$ENDIF}
end;

procedure TMainform.updateScrollBox;
begin
    //frameScrollBox.Width := mainInfoPanel.Width;
    //mainPageControl.Height := maxFrameHeight;
    //mainPageControl.Width := maxFrameWidth;
    frameScrollBox.VertScrollBar.Range := maxFrameHeight;
    frameScrollBox.VertScrollBar.Position := 0;
    frameScrollBox.HorzScrollBar.Range := maxFrameWidth;
    frameScrollBox.HorzScrollBar.Position := 0;
end;

//class function TMainForm.resourceFileUnbundle(const targetDir, filename, resID: string): Boolean;
//var
//    rStream: TResourceStream;
//    fStream: TFileStream;
//    fname: string;
//    theDir: string;
//begin
//    theDir := IncludeTrailingPathDelimiter(targetDir);
//    result := false;
//    eventLog.log('Unbundle resource file ' + theDir + filename);
//
//    if FileExists(theDir + filename) then Exit;
//
//    fname := theDir + filename;
//    fStream := nil;
//    rStream := TResourceStream.Create(hInstance, resID, RT_RCDATA);
//    try
//        fStream := TFileStream.Create(fname, fmCreate);
//        fStream.CopyFrom(rStream, 0);
//        result := true;
//    finally
//        rStream.Free();
//        fStream.Free();
//    end;
//end;

procedure TMainForm.miExportLevelsClick(Sender: TObject);
    function generateFilenameFunc(const baseFilename: TFilename; const depthInd: integer): TFileName;
    begin
        result := baseFilename + tostr(depthInd + 1) + '.asc';
    end;
    function atLeast1FileExistsOrIsInUse(): boolean;
    var
        d: integer;
        saveFilename: string;
    begin
        for d := 0 to container.scenario.dMax - 1 do
        begin
            savefilename := generateFilenameFunc(sdExportLevels.filename,d);
            if FileExists(savefilename) or
              (FileExists(savefilename) and THelper.IsFileInUse(savefilename)) then
                exit(true);
        end;
        result := false;
    end;
var
    shouldRedrawGL: boolean;
    d: integer;
begin
    if not theHASPSecurity.exportChecks then
        exit
    else if not container.scenario.resultsExist() then
    begin
        showmessage('No results to write');
        Exit;
    end
    else if ((TMenuItem(Sender).tag = 2) or (TMenuItem(Sender).tag = 3)) and (not container.CrossSecReady(shouldRedrawGL)) then
    begin
        showmessage('Set up cross section before exporting');
        framSetup.btSetupCrossSecClick(self);
        Exit;
    end
    else if ((TMenuItem(Sender).tag = 4) or (TMenuItem(Sender).tag = 5)) and (UWAOpts.levelsDisplay = ldAllLayers) then
    begin
        ShowMessage('Choose single layer or max levels display options before exporting layer');
        exit;
    end;

    if (TMenuItem(Sender).tag = 4) then
    begin
        sdExportLevels.Title := 'Choose output file';
        sdExportLevels.DefaultExt := 'asc';
    end
    else if (TMenuItem(Sender).tag = 6) then
    begin
        showmessage('Choose base_name for output files. One file is created for each z depth point, named <base_name>1.asc, <base_name>2.asc...');
        sdExportLevels.Title := 'Choose base for output files';
        sdExportLevels.DefaultExt := '';
    end
    else if (TMenuItem(Sender).tag = 7) then
    begin
        sdExportLevels.Title := 'Choose output file';
        sdExportLevels.DefaultExt := 'shp';
        sdExportLevels.Filter := 'Shapefiles|*.shp|All files|*.*';
        sdExportLevels.FilterIndex := 0;
    end
    else
    begin
        sdExportLevels.Title := 'Choose output file';
        sdExportLevels.DefaultExt := 'txt';
    end;

    if not directoryExists(DefaultSavePath) then
        DefaultSavePath := THelper.GetMyDocumentsPath;
    sdExportLevels.InitialDir := DefaultSavePath;

    if not sdExportLevels.Execute then exit;

    if (TMenuItem(Sender).tag = 6) then
    begin
        if atLeast1FileExistsOrIsInUse then
        begin
            ShowMessage('At least one of the output files exists or is in use by another process');
            exit;
        end;
    end
    else if FileExists(sdExportLevels.FileName) and THelper.IsFileInUse(sdExportLevels.FileName) then
    begin
        showmessage('File is in use by another process');
        Exit;
    end;

    DefaultSavePath := ExtractFilePath(sdExportLevels.FileName);

    case TMenuItem(Sender).tag of
        0: container.scenario.writeLevelsToFile(sdExportLevels.FileName,UWAOpts.exportDelimiter,false);
        1: container.scenario.writeLevelsToFile(sdExportLevels.FileName,UWAOpts.exportDelimiter,true);
        2: container.scenario.writeXSecLevelstoFile(sdExportLevels.FileName,UWAOpts.exportDelimiter,false);
        3: container.scenario.writeXSecLevelstoFile(sdExportLevels.FileName,UWAOpts.exportDelimiter,true);
        4: container.scenario.writeLevelsToESRIFile(sdExportLevels.FileName,UWAOpts.displayZInd,UWAOpts.levelsDisplay,true);
        5: container.scenario.writeMaxLevelsToFile(sdExportLevels.FileName,UWAOpts.exportDelimiter);
        6: for d := 0 to container.scenario.dMax - 1 do
            container.scenario.writeLevelsToESRIFile(generateFilenameFunc(sdExportLevels.filename,d),d,ldSingleLayer,d = 0);
        7: container.scenario.writeContoursToShapefile(sdExportLevels.filename);
    end;
end;

procedure TMainForm.miGIFToolClick(Sender: TObject);
begin
    MovingSourceGIFForm.bmpFunc := glform.BMPForMovingPosInd;
    MovingSourceGIFForm.ShowModal;

    GLForm.resultsMovingPosIndForGIFTool := -1;
    GLForm.setRedrawResults;
    GLForm.force2DrawNow;
end;

procedure TMainForm.miImportMaintenanceFile(Sender: TObject);
begin
    //

end;

procedure TMainForm.miEditCrossSectionClick(Sender: TObject);
begin
    if (editCrossSectionForm.ShowModal = mrOK) and assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := true;
        GLForm.didChangeVisibility(ord(veEndpoints));
        GLForm.forceDrawNow();
    end;
end;

procedure TMainForm.miExportESRIBathyClick(Sender: TObject);
begin
    //NB allow exporting of bathy regardless of running mode

    sdExportBathy.Title := 'Choose output data file';
    sdExportBathy.DefaultExt := 'asc';

    if not directoryExists(DefaultSavePath) then
        DefaultSavePath := THelper.GetMyDocumentsPath();
    sdExportBathy.InitialDir := DefaultSavePath;
    if not sdExportBathy.Execute() then exit;

    if FileExists(sdExportBathy.filename) and THelper.IsFileInUse(sdExportBathy.filename) then
    begin
        showmessage('File is in use by another process, operation cancelled');
        Exit;
    end;

    DefaultSavePath := ExtractFilePath(sdExportBathy.filename);
    container.writeBathyToESRIFile(sdExportBathy.filename)
end;

procedure TMainForm.miExportExclusionZoneClick(Sender: TObject);
begin
    if not container.scenario.resultsExist then exit;
    if isnan(UWAOpts.levelLimits.exclusionZoneLevel) then
    begin
        showmessage('No exclusion zone threshold is set');
        Exit;
    end;
    if not theHASPSecurity.exportChecks then exit;

    if not directoryExists(DefaultSavePath) then
        DefaultSavePath := THelper.GetMyDocumentsPath();
    sdExportLevels.InitialDir := DefaultSavePath;
    sdExportLevels.DefaultExt := 'asc';
    if not sdExportLevels.Execute() then exit;

    if FileExists(sdExportLevels.filename) and THelper.IsFileInUse(sdExportLevels.filename) then
    begin
        showmessage('File is in use by another process, operation cancelled');
        Exit;
    end;

    DefaultSavePath := ExtractFilePath(sdExportLevels.filename);
    container.scenario.writeExclusionZoneToESRIFile(sdExportLevels.filename);
end;

procedure TMainForm.miExportExclusionZoneShapefileClick(Sender: TObject);
begin
    if not container.scenario.resultsExist then exit;
    if isnan(UWAOpts.levelLimits.exclusionZoneLevel) then
    begin
        showmessage('No exclusion zone threshold is set');
        Exit;
    end;
    {$IFNDEF DEBUG}
    if not theHASPSecurity.exportChecks then exit;
    {$ENDIF}

    if not sdExportShapefile.Execute then exit;
    if sdExportShapefile.FileName = '' then exit;

    if FileExists(sdExportShapefile.filename) and THelper.IsFileInUse(sdExportShapefile.filename) then
    begin
        showmessage('File is in use by another process, operation cancelled');
        Exit;
    end;

    if framSource.sourceForThreshold = nil then
    begin
        showmessage('No source selected');
        exit;
    end;

    if framSource.sourceForThreshold.isMovingSource then
    begin
        showmessage('This function is not currently implemented for moving sources');
        exit;
    end;

    container.scenario.writeExclusionZoneToShapefile(sdExportShapefile.FileName, framSource.sourceForThreshold);
end;

procedure TMainform.showmessageIfNotEmpty(const s: string);
begin
    if not s.IsEmpty then
        showmessage(s);
end;

end.

