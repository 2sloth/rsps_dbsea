unit LatLongEntryFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  dbseaoptions, latlong2utm, baseTypes, helperFunctions;

type
  TLatLongEntryForm = class(TForm)
    btOK: TButton;
    btCancel: TButton;
    edLat: TEdit;
    edLong: TEdit;
    lbLongitude: TLabel;
    lbLatitude: TLabel;
    lbUTMZone: TLabel;
    edUTMZone: TEdit;
    procedure btOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edLongKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edUTMZoneKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
  public
    edEasting, edNorthing: TEdit;
    function  setEditsAndShowModal(const aEdEasting, aEdNorthing: TEdit): TModalResult;
  end;

var
  LatLongEntryForm: TLatLongEntryForm;

implementation

{$R *.dfm}

procedure TLatLongEntryForm.btOKClick(Sender: TObject);
var
    latlng, en: TEastNorth;
begin
    if not (assigned(edEasting) and Assigned(edNorthing)) then exit;
    if not (isFloat(edLat.Text) and isFloat(edLong.Text)) then
    begin
        showmessage('Could not parse text entries');
        exit;
    end;

    latlng.latitude := tofl(edLat.Text);
    latlng.longitude := tofl(edLong.Text);
    if isInt(edUTMZone.Text) then
        UWAOpts.UTMZone := toint(edUTMZone.Text)
    else
        UWAOpts.UTMZone := TLatLong2UTM.getLongZone(latlng.longitude);
    en := TLatLong2UTM.latlong2UTM(latlng, UWAOpts.UTMZone);
    edEasting.Text := tostrWithTestAndRound1(en.easting);
    edNorthing.Text := tostrWithTestAndRound1(en.northing);
end;

procedure TLatLongEntryForm.edLongKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (UWAOpts.UTMZone = -1) and isFloat(edLong.Text) then
        edUTMZone.Text := tostr(TLatLong2UTM.getLongZone(tofl(edLong.Text)));
end;

procedure TLatLongEntryForm.edUTMZoneKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if isint(edUTMZone.Text) then
        UWAOpts.UTMZone := toint(edUTMZone.Text);
end;

procedure TLatLongEntryForm.FormShow(Sender: TObject);
begin
    if UWAOpts.UTMZone = -1 then
        edUTMZone.Text := '?'
    else
        edUTMZone.Text := tostr(UWAOpts.UTMZone);
end;

function TLatLongEntryForm.setEditsAndShowModal(const aEdEasting, aEdNorthing: TEdit): TModalResult;
begin
    self.edEasting := aEdEasting;
    self.edNorthing := aEdNorthing;
    result := self.ShowModal;
end;

end.
