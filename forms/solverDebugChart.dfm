object solverDebugForm: TsolverDebugForm
  Left = 0
  Top = 0
  Caption = 'Source time series convolution'
  ClientHeight = 810
  ClientWidth = 1049
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  DesignSize = (
    1049
    810)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 1049
    Height = 13
    Align = alTop
    Caption = 'Label1'
    ExplicitWidth = 31
  end
  object chart1: TChart
    Left = 0
    Top = 19
    Width = 521
    Height = 313
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    View3D = False
    Zoom.Allow = False
    BevelOuter = bvNone
    TabOrder = 0
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object sr1: TFastLineSeries
      LinePen.Color = clRed
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Chart2: TChart
    Left = 524
    Top = 19
    Width = 525
    Height = 313
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    View3D = False
    Zoom.Allow = False
    BevelOuter = bvNone
    TabOrder = 1
    Anchors = [akLeft, akTop, akRight]
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object sr2: TFastLineSeries
      LinePen.Color = clRed
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Chart3: TChart
    Left = 0
    Top = 338
    Width = 1049
    Height = 472
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    DepthAxis.Automatic = False
    DepthAxis.AutomaticMaximum = False
    DepthAxis.AutomaticMinimum = False
    DepthAxis.Maximum = 0.590000000000000100
    DepthAxis.Minimum = -0.409999999999999900
    DepthTopAxis.Automatic = False
    DepthTopAxis.AutomaticMaximum = False
    DepthTopAxis.AutomaticMinimum = False
    DepthTopAxis.Maximum = 0.590000000000000100
    DepthTopAxis.Minimum = -0.409999999999999900
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    View3D = False
    Zoom.Allow = False
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object sr3: TFastLineSeries
      LinePen.Color = clRed
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
end
