object FormExportTitleBlock: TFormExportTitleBlock
  Left = 0
  Top = 0
  Caption = 'Export title block'
  ClientHeight = 110
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblProjName: TLabel
    Left = 16
    Top = 8
    Width = 63
    Height = 13
    Caption = 'Project name'
  end
  object lblSolver: TLabel
    Left = 16
    Top = 27
    Width = 30
    Height = 13
    Caption = 'Solver'
    Visible = False
  end
  object lblWeightingName: TLabel
    Left = 16
    Top = 65
    Width = 77
    Height = 13
    Caption = 'Weighting name'
  end
  object lblFrequency: TLabel
    Left = 16
    Top = 46
    Width = 51
    Height = 13
    Caption = 'Frequency'
  end
  object lblWeightingSpect: TLabel
    Left = 16
    Top = 84
    Width = 95
    Height = 13
    Caption = 'Weighting spectrum'
    Visible = False
  end
end
