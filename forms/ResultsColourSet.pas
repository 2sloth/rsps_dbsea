{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit ResultsColourSet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, Generics.Defaults,
    Generics.Collections, UxTheme, Math, dBSeaColours, helperFunctions,
    system.Types, mycolor, dBSeaOptions, problemContainer, SmartPointer;

type
  TResultsColoursFrm = class(TForm)
    sgColors: TStringGrid;
    btRsltClrOK: TButton;
    btRsltClrCancel: TButton;
    btReset: TButton;
    btAlternateColors: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure UpdateStringGrid;
    procedure sgColorsDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btResetClick(Sender: TObject);
    procedure btRsltClrOKClick(Sender: TObject);
    procedure btAlternateColorsClick(Sender: TObject);
    procedure sgColorsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
//    procedure evenSpacingDivsClick(Sender: TObject);

  private
    { Private declarations }
  const
    //iPositionCol        : integer = 0;
    iLevelCol           : integer = 0;
    iColourCol          : integer = 1;
    iVisibleCol         : integer = 2;
    //iAddRowCol          : integer = 4;
    //iRemoveRowCol       : integer = 5;
  public
    tmpColorList        : TMyColorList;
    bResetColorList     : boolean;
  end;

var
    ResultsColoursFrm: TResultsColoursFrm;

implementation

{$R *.dfm}

uses main;

procedure TResultsColoursFrm.FormCreate(Sender: TObject);
begin
    tmpColorList := TMyColorList.Create(true);
    bResetColorList := false;
end;

procedure TResultsColoursFrm.FormDestroy(Sender: TObject);
begin
    tmpColorList.Clear;
    tmpColorList.Free;
end;

procedure TResultsColoursFrm.FormShow(Sender: TObject);
begin
    UpdateStringGrid;
end;

procedure TResultsColoursFrm.UpdateStringGrid;
var
    i, len   : integer;
    mx, mn : double;
begin
    {completely redraw the stringgrid}
    len := tmpColorList.Count;
    //setlength(UWAOpts.LevelContours,len - 1);//there is 1 less contour than colors

    sgColors.RowCount := len + 1;
    //sgColors.Cells[iPositionCol,0] := 'Position';
    sgColors.Cells[iLevelCol,0] := 'Level (dB)';
    sgColors.Cells[iColourCol,0] := 'Colour';
    sgColors.Cells[iVisibleCol,0] := 'Visible';

    mx := container.scenario.convertLevel( UWAOpts.levelLimits.Max );
    mn := container.scenario.convertLevel( UWAOpts.levelLimits.Min );

    {fill the table with levels, colours and so on}
    for i := 0 to len - 1 do
    begin
        {creating of contour levels}
        if container.scenario.resultsExist then
            sgColors.Cells[iLevelCol,i+1] := tostr(round1(mx - (mx - mn)*(i)/len)) //NB doesn't reach mn
        else
            sgColors.Cells[iLevelCol,i+1] := '';

        {the setting of the colour cells is handled by the sgColorsDrawCell event}
    end;

    sgColors.Invalidate; //forces redrawing of the grid
end;

procedure TResultsColoursFrm.btAlternateColorsClick(Sender: TObject);
begin
    TDBSeaColours.resetResultsColorList(tmpColorList,
                                        saferound((UWAOpts.levelLimits.Max - UWAOpts.levelLimits.Min)/UWAOpts.levelLimits.LevelSpacing,1),1);

    UpdateStringGrid;
    {bResetColorList gets used by the calling form, so we know whether to put UWAOPts.bUserSetColors or not}
    bResetColorList := true;
end;

procedure TResultsColoursFrm.btResetClick(Sender: TObject);
begin
    {get back the original list of colors (from when the program starts)}
    TDBSeaColours.resetResultsColorList(tmpColorList,
                                        saferound((UWAOpts.levelLimits.Max - UWAOpts.levelLimits.Min)/UWAOpts.levelLimits.LevelSpacing,1),0);

    UpdateStringGrid;
    {bResetColorList gets used by the calling form, so we know whether to put UWAOPts.bUserSetColors or not}
    bResetColorList := true;
end;

procedure TResultsColoursFrm.btRsltClrOKClick(Sender: TObject);
begin
    {do nothing here, putting the colours back into the UWAOpts list is done by the parent form}
end;

procedure TResultsColoursFrm.sgColorsDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
const
    PADDING = 4;
    Checked: array[1..7] of boolean = (false, true, false, true, true, true, true);
var
    myCol   : TMyColorRec;
    h       : HTHEME;
    s       : TSize;
    r       : TRect;
begin
    {this is where we set the colours for the colour cells. getting the colours
    from UWAOpts.resultsColors}
    with (Sender as TStringGrid) do
    begin
        { Don't change color for header row}
        if (ARow = 0) then
            Canvas.Brush.Color := clBtnFace
        else
        begin
            if ACol = iColourCol then
            begin
                myCol := tmpColorList[arow - 1].asTMyColorRec();
                Canvas.Brush.Color := myCol.Color;

                {$IFDEF VER280}
                if DefaultDrawing then
                    Rect.Left := Rect.Left - 4;
                Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+5, Cells[ACol, ARow]);
                {$ELSE}
                Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
                {$ENDIF}
                Canvas.FrameRect(Rect);
            end;
        end;
    end;

    {drawing checkboxes}
    if (ACol = iVisibleCol) and (ARow >= 1) then
    begin
        FillRect(sgColors.Canvas.Handle, Rect, GetStockObject(WHITE_BRUSH));
        s.cx := GetSystemMetrics(SM_CXMENUCHECK);
        s.cy := GetSystemMetrics(SM_CYMENUCHECK);
        if UseThemes then
        begin
            h := OpenThemeData(sgColors.Handle, 'BUTTON');
            if h <> 0 then
            try
                GetThemePartSize(h,sgColors.Canvas.Handle,BP_CHECKBOX,
                    CBS_CHECKEDNORMAL,nil,TS_DRAW,s);

                r.Top := Rect.Top + (Rect.Bottom - Rect.Top - s.cy) div 2;
                r.Bottom := r.Top + s.cy;
                r.Left := Rect.Left + PADDING;
                r.Right := r.Left + s.cx;

                myCol := tmpColorList[arow - 1].asTMyColorRec();
                DrawThemeBackground(h,sgColors.Canvas.Handle,BP_CHECKBOX,
                    IfThen(myCol.Visible, CBS_CHECKEDNORMAL, CBS_UNCHECKEDNORMAL),r,nil);
            finally
                CloseThemeData(h);
            end;
        end
        else
        begin
            r.Top := Rect.Top + (Rect.Bottom - Rect.Top - s.cy) div 2;
            r.Bottom := r.Top + s.cy;
            r.Left := Rect.Left + PADDING;
            r.Right := r.Left + s.cx;
            DrawFrameControl(sgColors.Canvas.Handle,r,
                DFC_BUTTON,IfThen(Checked[ARow], DFCS_CHECKED, DFCS_BUTTONCHECK));
        end;
        r := Classes.Rect(r.Right + PADDING, Rect.Top, Rect.Right, Rect.Bottom);
        DrawText(sgColors.Canvas.Handle,sgColors.Cells[ACol, ARow],
            length(sgColors.Cells[ACol, ARow]),r,
            DT_SINGLELINE or DT_VCENTER or DT_LEFT or DT_END_ELLIPSIS);
    end;
end;

procedure TResultsColoursFrm.sgColorsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    aCol, aRow: Integer;
    ColorDialog: ISmart<TColorDialog>;
begin
    if (button = mbLeft) then
    begin
        sgColors.MouseToCell(X, Y, aCol, aRow);
        if arow = 0 then exit;
        if arow > tmpColorList.Count then exit;

        if acol = iColourCol then
        begin
            ColorDialog := TSmart<TColorDialog>.create(TColorDialog.Create(Self));
            Colordialog.CustomColors.CommaText := UWAOpts.customColors;
            if colordialog.execute then    //change colour of selected row
                tmpColorList[arow - 1].Color := colordialog.Color;
            UWAOpts.customColors := colordialog.CustomColors.CommaText;
        end;

        if acol = iVisibleCol then
            tmpColorList[arow - 1].Visible := not(tmpColorList[arow - 1].Visible);

        UpdateStringGrid;
    end;
end;

end.
