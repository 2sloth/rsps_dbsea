object sourceSliceSelectorForm: TsourceSliceSelectorForm
  Left = 206
  Top = 56
  Caption = 'Choose source position and radial slice'
  ClientHeight = 591
  ClientWidth = 984
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    984
    591)
  PixelsPerInch = 96
  TextHeight = 13
  object pbSlice: TPaintBox
    Left = 36
    Top = 144
    Width = 109
    Height = 109
    OnMouseDown = pbSliceMouseDown
    OnPaint = pbSlicePaint
  end
  object lbAngle: TLabel
    Left = 36
    Top = 276
    Width = 27
    Height = 13
    Caption = '0 deg'
  end
  object lbPosition: TLabel
    Left = 32
    Top = 51
    Width = 37
    Height = 13
    Caption = 'Position'
    Visible = False
  end
  object lbSlice: TLabel
    Left = 32
    Top = 87
    Width = 21
    Height = 13
    Caption = 'Slice'
  end
  object lbDashedLinesHint: TLabel
    Left = 252
    Top = 5
    Width = 208
    Height = 13
    Caption = 'Dashed lines show 10 log and 20 log curves'
  end
  object edSlice: TEdit
    Left = 96
    Top = 84
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '0'
    OnChange = edSliceChange
  end
  object btGoToCrossSection: TButton
    Left = 537
    Top = 512
    Width = 237
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'To cross section in main window'
    ModalResult = 1
    TabOrder = 1
  end
  object btDone: TButton
    Left = 882
    Top = 543
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Done'
    ModalResult = 2
    TabOrder = 2
  end
  object udSlice: TUpDown
    Left = 217
    Top = 84
    Width = 17
    Height = 21
    Associate = edSlice
    Min = -1000000
    Max = 100000
    TabOrder = 3
    Thousands = False
  end
  object chLevs: TChart
    Left = 252
    Top = 32
    Width = 705
    Height = 433
    Legend.Visible = False
    MarginBottom = 0
    MarginTop = 0
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Title.Caption = 'Distance from source (m)'
    LeftAxis.Title.Caption = 'Level (dB)'
    RightAxis.Title.Caption = 'Depth'
    View3D = False
    Zoom.Allow = False
    BevelOuter = bvNone
    TabOrder = 4
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object srLevs: TFastLineSeries
      SeriesColor = 10391090
      LinePen.Color = 10391090
      LinePen.Width = 2
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object sr10Log: TFastLineSeries
      SeriesColor = 10391090
      LinePen.Color = 10391090
      LinePen.Style = psDash
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object sr20Log: TFastLineSeries
      SeriesColor = 10391090
      LinePen.Color = 10391090
      LinePen.Style = psDash
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object srBathy: TFastLineSeries
      SeriesColor = 4289191
      VertAxis = aRightAxis
      LinePen.Color = 4289191
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object cbOnlyThisSource: TCheckBox
    Left = 252
    Top = 480
    Width = 197
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Only show levels from this source'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = cbOnlyThisSourceClick
  end
  object edPosition: TEdit
    Left = 96
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '0'
    Visible = False
    OnChange = edPositionChange
  end
  object udPosition: TUpDown
    Left = 217
    Top = 48
    Width = 17
    Height = 21
    Associate = edPosition
    Max = 100000
    TabOrder = 7
    Thousands = False
    Visible = False
  end
  object btSaveCurrent: TButton
    Left = 36
    Top = 512
    Width = 161
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Keep current trace'
    TabOrder = 8
    OnClick = btSaveCurrentClick
  end
  object btClearSaved: TButton
    Left = 370
    Top = 512
    Width = 161
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Clear traces'
    TabOrder = 9
    OnClick = btClearSavedClick
  end
  object btShowAllSlices: TButton
    Left = 203
    Top = 512
    Width = 161
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Show all slices'
    TabOrder = 10
    OnClick = btShowAllSlicesClick
  end
  object btToClipboard: TButton
    Left = 36
    Top = 543
    Width = 161
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Copy to clipboard'
    TabOrder = 11
    OnClick = btToClipboardClick
  end
  object btAllToClipboard: TButton
    Left = 203
    Top = 543
    Width = 161
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Copy all slices to clipboard'
    TabOrder = 12
    OnClick = btAllToClipboardClick
  end
  object cbLogDistance: TCheckBox
    Left = 840
    Top = 480
    Width = 117
    Height = 17
    Anchors = [akRight, akBottom]
    Caption = 'Log distance scale'
    TabOrder = 13
    OnClick = cbLogDistanceClick
  end
  object cbShowdepth: TCheckBox
    Left = 496
    Top = 480
    Width = 145
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Show depth for this slice'
    TabOrder = 14
    OnClick = cbShowdepthClick
  end
end
