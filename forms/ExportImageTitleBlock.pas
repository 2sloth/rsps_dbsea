unit ExportImageTitleBlock;


{this form is used for creating a BMP titleblock, that will be pasted into the larger BMP
when we do an image export. it contains the same info as is written at the top of
files when we export text data.}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Main, helperFunctions, spectrum, problemContainer,
  globalsUnit;

type
  TFormExportTitleBlock = class(TForm)
    lblProjName: TLabel;
    lblSolver: TLabel;
    lblWeightingName: TLabel;
    lblFrequency: TLabel;
    lblWeightingSpect: TLabel;
    procedure FormCreate(Sender: TObject);
    function  getBMP(const w,h : integer): TBitmap;
  private
  public
  end;

var
  FormExportTitleBlock: TFormExportTitleBlock;

implementation

{$R *.dfm}

procedure TFormExportTitleBlock.FormCreate(Sender: TObject);
begin
    lblProjName.Caption := currentProjectName;
    lblSolver.Caption := container.scenario.solversText;
    lblWeightingName.Caption := 'Weighting: ' + container.scenario.weighting.Name;
    lblFrequency.Caption := 'Frequency: ' +
                            tostr(TSpectrum.getFbyInd(container.scenario.startfi,container.scenario.Bandwidth)) +
                            ' Hz to ' +
                            tostr(TSpectrum.getFbyInd(container.scenario.endfi,container.scenario.Bandwidth)) +
                            ' Hz';
    lblWeightingSpect.Caption := '';
end;

function  TFormExportTitleBlock.getBMP(const w,h : integer): TBitmap;
begin
    self.ClientWidth := w;
    self.ClientHeight := h;
    self.Color := clWhite;

    self.Invalidate;  //force form to redraw
    result := self.GetFormImage;
end;

end.
