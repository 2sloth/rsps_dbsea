unit RayVisibilityFrm;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes,
  system.StrUtils,
  system.Generics.collections,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  RayPlotting, Vcl.StdCtrls, Vcl.Grids;

type

  TRayVisibilityForm = class(TForm)
    btOK: TButton;
    sg: TStringGrid;
    btNone: TButton;
    btAll: TButton;
    btDraw: TButton;
    procedure CheckBoxClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btAllClick(Sender: TObject);
    procedure btNoneClick(Sender: TObject);
    procedure sgMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btDrawClick(Sender: TObject);
  private
    procedure myUpdate();
  public
    startAngle,endAngle: double;
  end;

var
  RayVisibilityForm: TRayVisibilityForm;

implementation

{$R *.dfm}

uses GLWindow;

{ TRayVisibilityForm }

procedure TRayVisibilityForm.btAllClick(Sender: TObject);
var
    sliceRays: TSliceRays;
begin
    for sliceRays in sliceRayss do
        sliceRays.visible := true;
    myUpdate;
end;

procedure TRayVisibilityForm.btNoneClick(Sender: TObject);
var
    sliceRays: TSliceRays;
begin
    for sliceRays in sliceRayss do
        sliceRays.visible := false;
    myUpdate;
end;

procedure TRayVisibilityForm.btDrawClick(Sender: TObject);
begin
    GLForm.forceDrawNow;
end;

procedure TRayVisibilityForm.CheckBoxClick(Sender: TObject);
var
    cb: TCheckBox;
begin
    if not (sender is TCheckBox) then exit;
    cb := sender as TCheckBox;
    sliceRayss[cb.tag].visible := cb.Checked;
end;

procedure TRayVisibilityForm.FormShow(Sender: TObject);
begin
    myUpdate;
end;

procedure TRayVisibilityForm.sgMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    aCol, aRow: Integer;
    sliceRays: TSliceRays;
begin
    sg.MouseToCell(X, Y, aCol, aRow);
    if (aRow = 0) or (aRow - 1 >= sliceRayss.count) then exit;
    sliceRays := sliceRayss[aRow - 1];
    if (aCol = 1) then
        sliceRays.visible := not sliceRays.visible
    else if (acol = 2) then
        sliceRays.setAllRaysVisible(true)
    else if (aCol = 3) then
        sliceRays.setAllRaysVisible(false)
    else if (aCol - 4 >= 0) and (aCol - 4 < sliceRays.rays.count) then
        sliceRays.toggleVisible(aCol - 4);

    myUpdate;
end;

procedure TRayVisibilityForm.myUpdate;
var
    sliceRays: TSliceRays;
    ray: TRay;
begin
    sg.RowCount := sliceRayss.count + 1;
    if sliceRayss.count = 0 then exit;
    sg.ColCount := 4 + sliceRayss[0].rays.count;

    sg.Cells[0,0] := 'Slice';
    sg.Cells[1,0] := 'Show';
    sliceRays := sliceRayss[0];
    for ray in sliceRays.rays do
        sg.Cells[4 + sliceRays.rays.IndexOf(ray),0] := floattostrf(
            sliceRays.rays.IndexOf(ray) *
            (endAngle - startAngle) /
            sliceRays.rays.count +
            startAngle,fffixed,4,0);

    for sliceRays in sliceRayss do
    begin
        sg.Cells[0,sliceRayss.IndexOf(sliceRays)+1] := floattostrf(sliceRayss.IndexOf(sliceRays)*360/sliceRayss.count,fffixed,4,0);//inttostr(sliceRayss.IndexOf(sliceRays));
        sg.Cells[1,sliceRayss.IndexOf(sliceRays)+1] := ifthen(sliceRays.visible,'*','');
        sg.Cells[2,sliceRayss.IndexOf(sliceRays)+1] := 'All';
        sg.Cells[3,sliceRayss.IndexOf(sliceRays)+1] := 'None';
        for ray in sliceRays.rays do
            sg.Cells[4 + sliceRays.rays.IndexOf(ray),sliceRayss.IndexOf(sliceRays)+1] := ifthen(ray.visible,ifthen(sliceRays.visible,'*','.'),'');
    end;
end;

end.
