object UTMConversionFrm: TUTMConversionFrm
  Left = 0
  Top = 0
  Caption = 'UTM conversion'
  ClientHeight = 603
  ClientWidth = 834
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    834
    603)
  PixelsPerInch = 96
  TextHeight = 13
  object lbRefZone: TLabel
    Left = 24
    Top = 582
    Width = 49
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'lbRefZone'
    ExplicitTop = 505
  end
  object lbHint: TLabel
    Left = 24
    Top = 528
    Width = 166
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'All entries use the reference zone:'
  end
  object lbArrows: TLabel
    Left = 544
    Top = 224
    Width = 30
    Height = 13
    Anchors = [akTop, akRight]
    Caption = '> > >'
    ExplicitLeft = 538
  end
  object sgLatLng: TStringGrid
    Left = 24
    Top = 104
    Width = 500
    Height = 363
    Hint = 
      'Latitiude '#176' N of equator, Longitude '#176' E of Greenwich. Use decima' +
      'l degrees'
    Anchors = [akLeft, akTop, akRight]
    ColCount = 6
    DefaultColWidth = 80
    FixedCols = 0
    RowCount = 14
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnKeyDown = sgLatLngKeyDown
    OnKeyUp = sgLatLngKeyUp
    OnSelectCell = sgLatLngSelectCell
  end
  object sgEastingNorthing: TStringGrid
    Left = 593
    Top = 104
    Width = 209
    Height = 363
    Hint = 'Easting and Northing are referenced to the current UTM zone'
    Anchors = [akTop, akRight]
    ColCount = 2
    DefaultColWidth = 100
    FixedCols = 0
    RowCount = 14
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object btOK: TButton
    Left = 727
    Top = 562
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Done'
    ModalResult = 1
    TabOrder = 2
    OnClick = btOKClick
  end
  object btFromClipboard: TButton
    Left = 24
    Top = 473
    Width = 97
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'From clipboard'
    TabOrder = 3
    OnClick = btFromClipboardClick
  end
  object btToClipboard: TButton
    Left = 705
    Top = 473
    Width = 97
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'To clipboard'
    TabOrder = 4
    OnClick = btToClipboardClick
  end
  object rgFormat: TRadioGroup
    Left = 24
    Top = 16
    Width = 185
    Height = 65
    Caption = 'Format'
    ItemIndex = 0
    Items.Strings = (
      'Decimal degrees'
      'Degrees, minutes, seconds')
    TabOrder = 5
    OnClick = rgFormatClick
  end
  object rbValidRow: TRadioButton
    Left = 24
    Top = 552
    Width = 113
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'First valid row'
    Checked = True
    TabOrder = 6
    TabStop = True
    OnClick = rbValidRowClick
  end
  object rbUserSetZone: TRadioButton
    Left = 184
    Top = 552
    Width = 81
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'This zone:'
    TabOrder = 7
    OnClick = rbUserSetZoneClick
  end
  object edUserSetZone: TEdit
    Left = 271
    Top = 550
    Width = 65
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 8
    OnChange = edUserSetZoneChange
  end
end
