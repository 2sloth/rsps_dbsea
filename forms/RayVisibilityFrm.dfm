object RayVisibilityForm: TRayVisibilityForm
  Left = 0
  Top = 0
  Caption = 'Ray visibility'
  ClientHeight = 528
  ClientWidth = 423
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btOK: TButton
    Left = 0
    Top = 478
    Width = 423
    Height = 25
    Align = alBottom
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object sg: TStringGrid
    Left = 0
    Top = 50
    Width = 423
    Height = 428
    Align = alClient
    DefaultColWidth = 30
    TabOrder = 1
    OnMouseDown = sgMouseDown
  end
  object btNone: TButton
    Left = 0
    Top = 25
    Width = 423
    Height = 25
    Align = alTop
    Caption = 'None'
    TabOrder = 2
    OnClick = btNoneClick
  end
  object btAll: TButton
    Left = 0
    Top = 0
    Width = 423
    Height = 25
    Align = alTop
    Caption = 'All'
    TabOrder = 3
    OnClick = btAllClick
  end
  object btDraw: TButton
    Left = 0
    Top = 503
    Width = 423
    Height = 25
    Align = alBottom
    Caption = 'Draw'
    TabOrder = 4
    OnClick = btDrawClick
  end
end
