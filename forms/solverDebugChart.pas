unit solverDebugChart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, vcltee.Series, ExtCtrls, vcltee.Chart, StdCtrls, dbSeaColours,
  VCLTee.TeEngine, VclTee.TeeGDIPlus, VCLTee.TeeProcs;

type
  TsolverDebugForm = class(TForm)
    chart1: TChart;
    sr1: TFastLineSeries;
    Chart2: TChart;
    sr2: TFastLineSeries;
    Chart3: TChart;
    sr3: TFastLineSeries;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  solverDebugForm: TsolverDebugForm;

implementation

{$R *.dfm}

procedure TsolverDebugForm.FormShow(Sender: TObject);
begin
    sr1.Color := TDBSeaColours.darkBlue;
    sr2.Color := TDBSeaColours.darkBlue;
    sr3.Color := TDBSeaColours.darkBlue;
end;

end.
