unit LevelLimitsDlg;

interface

uses
    syncommons,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, baseTypes, MCKLib, math, ComCtrls,
  Series, ExtCtrls, Chart, DBSeaColours, spectrum, helperFunctions,
  levelLimits, dBSeaconstants, SQLiteTable3, levelConverter, thresholdLevels, types,
  VCLTee.TeEngine, VclTee.TeeGDIPlus, VCLTee.TeeProcs, HistogramCalc,
  globalsUnit, nullable, SmartPointer, eventLogging;

type
    TLevelConversionFunc = reference to function(const level: single): single;


  TLevelLimitsDialog = class(TForm)
    edMax: TEdit;
    edMin: TEdit;
    btOK: TButton;
    btCancel: TButton;
    lblMaxLevel: TLabel;
    lnlMinLevel: TLabel;
    udMax: TUpDown;
    udMin: TUpDown;
    btReset: TButton;
    chLevelHistogram: TChart;
    srActiveHistogram: TBarSeries;
    rgBandSpacing: TRadioGroup;
    cbExclusionZone: TCheckBox;
    srInactiveHistogram: TBarSeries;
    pnExclusionZone: TPanel;
    btSetExclusionZoneColor: TButton;
    pbExclusionZoneColor: TPaintBox;
    edExclusionZone: TEdit;
    lbExclusionZone: TLabel;
    cbThresholds: TComboBox;
    ttPaintboxDraw: TTimer;
    lbGatheringLevels: TLabel;
    pbGatheringLevels: TProgressBar;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure levelsChanged(Sender : TObject);
    //procedure evenSpacingDivsClick(Sender: TObject);
    procedure btResetClick(Sender: TObject);
    procedure udMaxClick(Sender: TObject; Button: TUDBtnType);
    procedure udMinClick(Sender: TObject; Button: TUDBtnType);
    procedure rgBandSpacingClick(Sender: TObject);
    procedure cbExclusionZoneClick(Sender: TObject);
    procedure btSetExclusionZoneColorClick(Sender: TObject);
    procedure cbThresholdsChange(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure edExclusionZoneChange(Sender: TObject);
    procedure ttPaintboxDrawTimer(Sender: TObject);
  private
    updateFromCode : boolean;

    procedure EditBoxesInit();
    procedure checkLevelValues();
    procedure drawPaintbox();
    //procedure updateAllLevelsArray(var allActiveLevels : TSingleDynArray);
  public
    dLevMax, dLevMin: single;
    dExclusionZoneLevel: single;
    dLevelSpacing: single;
    bUserSet: boolean;
    levelType: TLevelType;
    allActiveLevels, allActiveLevelsAsUserLevelType: Nullable<TSingleDynArray>;
    customColors: string;
    exclusionZonecolor: TColor;

    levelConversion: TLevelConversionFunc;  //convert any levels before showing them to the user
    levelUnConversion: TLevelConversionFunc; //on getting values back from user

    aLevelLimits: TLevelLimits;

    procedure updateHistogram();
  end;

var
    LevelLimitsDialog: TLevelLimitsDialog;

implementation

{$R *.dfm}

uses GLWindow;

procedure TLevelLimitsDialog.FormCreate(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'FormCreate');
    try
        aLevelLimits := TLevelLimits.Create(nil);
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TLevelLimitsDialog.FormDestroy(Sender: TObject);
begin
    aLevelLimits.Free;
    TThresholdLevels.clearThresholdsBoxObjects(self.cbThresholds);
end;

procedure TLevelLimitsDialog.FormShow(Sender: TObject);
//var
//    i : integer;
begin
    updateFromCode := true;
    lbGatheringLevels.Visible := true;

    TThresholdLevels.thresholdsBoxInit(self.cbThresholds,MDASldb,userSldb);
    cbThresholds.Enabled := levelType <> kSPLCrestFactor;

    {write the values to local variables}
    dLevMax := alevelLimits.max;
    dLevMin := alevelLimits.min;
    dLevelSpacing := alevelLimits.levelSpacing;
    dExclusionZoneLevel := alevelLimits.exclusionZoneLevel;
    bUserSet := alevelLimits.bUserSetLimits;
    cbExclusionZone.Checked := alevelLimits.showExclusionZone;

    //updateAllLevelsArray();
    EditBoxesInit();
    updateHistogram();
    ttPaintboxDraw.Enabled := true; //start a quick timer so that it will properly draw the paintbox (won't get drawn otherwise?)
    updateFromCode := false;
    cursor := crDefault;
end;

procedure TLevelLimitsDialog.checkLevelValues();
begin
    if not (isnan(dLevMin) or isnan(dLevMax) or isnan(dLevelSpacing)) and
        (dLevMax < dLevMin + dLevelSpacing) then
        dLevmax := dLevMin + dLevelSpacing;
end;

procedure TLevelLimitsDialog.editBoxesInit();
begin
    updateFromCode := true;

    checkLevelValues();

    if isNaN(dLevMax) then
        edMax.Text := '-'
    else
        edMax.Text := tostr(ceilx(levelConversion(dLevMax),dLevelSpacing));

    if isNaN(dLevMin) then
        edMin.Text := '-'
    else
        edMin.Text := tostr(floorx(levelConversion(dLevMin),dLevelSpacing));

    if isNaN(dExclusionZoneLevel) then
        edExclusionZone.Text := '-'
    else
        edExclusionZone.Text := tostrWithTestAndRound1(levelConversion(dExclusionZoneLevel));

    case saferound(dLevelSpacing) of
        1  : rgBandSpacing.ItemIndex := 0;
        3  : rgBandSpacing.ItemIndex := 1;
        5  : rgBandSpacing.ItemIndex := 2;
        10 : rgBandSpacing.ItemIndex := 3;
        20 : rgBandSpacing.ItemIndex := 4;
    else
        rgBandSpacing.ItemIndex := 2;
    end;

    updateFromCode := false;
end;

procedure TLevelLimitsDialog.drawPaintbox();
begin
    pbExclusionZoneColor.Canvas.Brush.Color := exclusionZoneColor;
    pbExclusionZoneColor.Canvas.Pen.Color := exclusionZoneColor;
    pbExclusionZoneColor.Canvas.Pen.Width := 1;
    pbExclusionZoneColor.Canvas.Rectangle(pbExclusionZoneColor.ClientRect);
end;

procedure TLevelLimitsDialog.ttPaintboxDrawTimer(Sender: TObject);
begin
    drawPaintbox();
    ttPaintboxDraw.Enabled := false;
end;

procedure TLevelLimitsDialog.btOKClick(Sender: TObject);
begin
    if cbExclusionZone.Checked then
    begin
        if not isfloat(edExclusionZone.Text) then
        begin
            showmessage('Enter a valid number for exclusion zone level to show exclusion zone');
            exit;
        end;
        if isnan(dExclusionZoneLevel) then
        begin
            showmessage('Enter a valid number in exclusion zone level box to show exclusion zone');
            exit;
        end;
    end;

    if isFloat(edMax.Text) then
        dLevMax := roundx( levelunconversion( tofl(edMax.Text) ), dLevelSpacing);
    if isFloat(edMin.Text) then
        dLevMin := roundx( levelunconversion( tofl(edMin.Text) ), dLevelSpacing);

    modalResult := mrOK;
end;

procedure TLevelLimitsDialog.btResetClick(Sender: TObject);
begin
    updateFromCode := true;
    bUserSet := false;
    dLevMax := alevelLimits.cmax;
    dLevMin := alevelLimits.cmin;
    dLevelSpacing := 5;

    EditBoxesInit();
    updateHistogram();
    updateFromCode := false;
end;

procedure TLevelLimitsDialog.btSetExclusionZoneColorClick(Sender: TObject);
var
    ColorDialog: ISmart<TColorDialog>;
begin
    ColorDialog := TSmart<TColorDialog>.create(TColorDialog.create(self));
    ColorDialog.CustomColors.CommaText := customColors;
    if ColorDialog.execute then
        exclusionZonecolor := ColorDialog.color;
    customColors := ColorDialog.CustomColors.CommaText;
    if Assigned(GLForm) then
    begin
        GLForm.setRedrawResults;
        GLForm.force2DrawNow;
    end;
    drawPaintbox();
end;

procedure TLevelLimitsDialog.cbExclusionZoneClick(Sender: TObject);
begin
    EditBoxesInit();
    updateHistogram();
end;

procedure TLevelLimitsDialog.cbThresholdsChange(Sender: TObject);
begin
    if updateFromCode then exit;
    if cbThresholds.ItemIndex = 0 then exit;

    {need to convert the level to kSPL, as it is pulled out of the database in the hopefully correct format}
    {NB the NOAA limits aren't really meant to be used with incorrect assessment period but we will just have to assume
    the user has the correct period set (or that they know what they are doing)}
    dExclusionZoneLevel := levelUnConversion( TThresholdLevels(cbThresholds.Items.Objects[cbthresholds.ItemIndex]).threshold(levelType) );
    if isnan(dExclusionZoneLevel) then
        edExclusionZone.Text := '-'
    else
    begin
        edExclusionZone.Text := tostrWithTestAndRound1(levelConversion(dExclusionZoneLevel));
        cbExclusionZone.Checked := true;
    end;
end;


procedure TLevelLimitsDialog.edExclusionZoneChange(Sender: TObject);
begin
    if isFloat(edExclusionZone.Text) then
        dExclusionZoneLevel := levelUnConversion( tofl(edExclusionZone.Text) );
end;

procedure TLevelLimitsDialog.levelsChanged(Sender: TObject);
begin
    if not updateFromCode then
    begin
        if isFloat(edMax.Text) then
            dLevMax := roundx( levelunconversion( tofl(edMax.Text) ), dLevelSpacing);
        if isFloat(edMin.Text) then
            dLevMin := roundx( levelunconversion( tofl(edMin.Text) ), dLevelSpacing);

        bUserSet := true;
        //EditBoxesInit();
        //updateHistogram();
    end;
end;

procedure TLevelLimitsDialog.rgBandSpacingClick(Sender: TObject);
var
    newSpacing : integer;
begin
    case rgBandSpacing.ItemIndex of
        0 : newSpacing := 1;
        1 : newSpacing := 3;
        2 : newSpacing := 5;
        3 : newSpacing := 10;
        4 : newSpacing := 20;
    else
        newSpacing := 5;
    end;

    if dLevelSpacing <> newSpacing then
    begin
        dLevelSpacing   := newSpacing;
        dLevMin         := floorx(dLevMin,dLevelSpacing);
        dLevMax         := ceilx(dLevMax,dLevelSpacing);
        //dExclusionZone  := roundx(dExclusionZone,dLevelSpacing);
        EditBoxesInit();
        updateHistogram();
    end;

end;

procedure TLevelLimitsDialog.udMaxClick(Sender: TObject; Button: TUDBtnType);
begin
    if isNaN(dLevMax) and isNaN(dLevMin) then
    begin
        dLevMin := 0;
        dLevMax := dLevelSpacing;
    end
    else if isNaN(dLevMax) then
        dLevMax := dLevMin + dLevelSpacing
    else if isNaN(dLevMin) then
        dLevMin := dLevMax - dLevelSpacing
    else
    begin
        if button = btNext then
            dLevMax := dLevMax + dLevelSpacing
        else
            dLevMax := dLevMax - dLevelSpacing;
    end;

    bUserSet := true;
    EditBoxesInit();
    updateHistogram();
end;

procedure TLevelLimitsDialog.udMinClick(Sender: TObject; Button: TUDBtnType);
begin
    if isNaN(dLevMax) and isNaN(dLevMin) then
    begin
        dLevMin := 0;
        dLevMax := dLevelSpacing;
    end
    else if isNaN(dLevMax) then
        dLevMax := dLevMin + dLevelSpacing
    else if isNaN(dLevMin) then
        dLevMin := dLevMax - dLevelSpacing
    else
    begin
        if button = btNext then
            dLevMin := dLevMin + dLevelSpacing
        else
            dLevMin := dLevMin - dLevelSpacing;
    end;

    bUserSet := true;
    EditBoxesInit();
    updateHistogram();
end;

procedure TLevelLimitsDialog.updateHistogram;
var
    i                   : integer;
    binCounts           : TIntegerDynArray;
    tmp, binLevels      : TSingleDynArray;
begin
    if alevelLimits.MaxOrMinNaN or not allActiveLevels.HasValue then
    begin
        self.srActiveHistogram.clear;
        self.srInactiveHistogram.clear;
        self.lbGatheringLevels.Visible := true;
        self.pbGatheringLevels.Visible := true;
        Exit;
    end
    else
    begin
        self.lbGatheringLevels.Visible := false;
        self.pbGatheringLevels.Visible := false;
    end;

    //convert all the levels we received to the user leveltype
    if not self.allActiveLevelsAsUserLevelType.HasValue then
    begin
        setlength(tmp, length(self.allActiveLevels.val));
        self.allActiveLevelsAsUserLevelType := tmp;
        for i := 0 to length(self.allActiveLevels.val) - 1 do
            allActiveLevelsAsUserLevelType.val[i] := self.levelConversion(allActiveLevels.val[i]);
    end;

    checkLevelValues();

    TMyHistogram.calcHistogramBins(self.allActiveLevelsAsUserLevelType, round(dLevelSpacing), binLevels, binCounts);

    chLevelHistogram.BottomAxis.Title.Text := 'Level (dB ' + TLevelConverter.levelTypeName(self.levelType) + ')';
    srActiveHistogram.Clear();
    srInactiveHistogram.Clear();
    srActiveHistogram.ColorEachPoint := true;
    srInactiveHistogram.ColorEachPoint := true;
    for i := 0 to length(binLevels) - 1 do
    begin
        if checkrange(round1(binLevels[i]),levelConversion(dLevMin),levelConversion(dLevMax)) then
        begin
            srActiveHistogram.AddXY(round1(binLevels[i]), binCounts[i], '', TdBSeaColours.darkBlue());
            srInactiveHistogram.AddXY(round1(binLevels[i]), 0, '', TdBSeaColours.lightBlue());
        end
        else
        begin
            srActiveHistogram.AddXY(round1(binLevels[i]), 0, '', TdBSeaColours.darkBlue());
            srInactiveHistogram.AddXY(round1(binLevels[i]), binCounts[i], '', TdBSeaColours.lightBlue());
        end;
    end;
end;

end.
