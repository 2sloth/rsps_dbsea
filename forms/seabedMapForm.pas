unit seabedMapForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, system.UITypes, math,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids, Vcl.StdCtrls,
  baseTypes, dBSeaColours, generics.collections, helperFunctions, mcklib,
  Vcl.ComCtrls, Vcl.Buttons, nullable;

type
  DrawingTool = (kRect, kEllipse, kFreehand, kBrush);

  TPBClick = class(TObject)
  public
  var
    x,y: integer;
    mouseButton: Nullable<TMouseButton>;
    shift: TShiftState;

    constructor Create(const ax, ay: integer; const ashift: TShiftState; const amouseButton: TMouseButton);
  end;
  TPBClicksList = TobjectList<TPBClick>;

  TSeabedIndexMap = class(TObject)
  public
  var
    map: TIntegerMatrix;

    procedure cloneFrom(const source: TSeabedIndexMap);
  end;
  TSeabedIndexMapsList = TObjectList<TSeabedIndexMap>;

  TSeabedMapFrm = class(TForm)
    pnTools: TPanel;
    btOK: TButton;
    btCancel: TButton;
    sgPalette: TStringGrid;
    btUndo: TButton;
    btSet: TButton;
    Timer1: TTimer;
    imMap: TImage;
    lbInfo: TLabel;
    tbBrushSize: TTrackBar;
    sbRect: TSpeedButton;
    sbEllipse: TSpeedButton;
    sbFreehand: TSpeedButton;
    sbBrush: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure sgPaletteDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btUndoClick(Sender: TObject);
    procedure btSetClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure imMapClick(Sender: TObject);
    procedure imMapMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imMapMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imMapMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure sbRectClick(Sender: TObject);
    procedure sbEllipseClick(Sender: TObject);
    procedure sbFreehandClick(Sender: TObject);
    procedure sbBrushClick(Sender: TObject);
    procedure sgPaletteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
  const
    maxPastMaps = 100;
  var
    colours: TColorArray;
    pastMaps: TSeabedIndexMapsList;
    currentClicks: TPBClicksList;
    layerInd: integer;
    tool: DrawingTool;
    mbDown: boolean;
    tmpMap: TSeabedIndexMap;
    bmp: TBitmap;

    function  screenToMap(const clickPoint: TPBClick):TPoint;
    function  screenToMapX(const x: integer):integer;
    function  screenToMapY(const y: integer):integer;
    //function  mapToScreen(const mapPoint: TPoint):TPoint;

    procedure editboxesInit;
    procedure updateBMP;
    procedure updatePaintbox(const x: integer = -1; const y: integer = -1);
    function  getColour(const i: integer): TColor;
    function  getLayerIndAtScreenXY(const i,j: integer): integer;
    function  distToClick(const i,j: integer; const click: TPBClick): double;
    //procedure setMapIndicesWithinRadiusFromXY(const x,y,radius,layerInd: integer);
    function  addMap: TSeabedIndexMap;
  public
    layerNames: TStringList;
    depths: TSingleMatrix;

    function  getMap: TSeabedIndexMap;
  end;

implementation

{$B-}
{$R *.dfm}

constructor TPBClick.Create(const ax, ay: integer; const ashift: TShiftState; const amouseButton: TMouseButton);
begin
    inherited Create;

    x := ax;
    y := ay;
    shift := ashift;
    mouseButton := amouseButton;
end;

procedure TSeabedIndexMap.cloneFrom(const source:TSeabedIndexMap);
begin
    self.map := source.map.copy;
end;

procedure TSeabedMapFrm.btSetClick(Sender: TObject);
    procedure freehandPoints(const map: TSeabedIndexMap);
    var
        points: TPointArray;
        i,j: integer;
    begin
        setlength(points,self.currentClicks.Count);
        for i := 0 to self.currentClicks.Count - 1 do
            points[i] := self.screenToMap(self.currentClicks[i]);

        for i := 0 to length(map.map) - 1 do
            for j := 0 to length(map.map[0]) - 1 do
                if TMCKLib.pointInPolygon(points,i,j) then
                    map.map[i,j] := self.layerInd;
    end;

    function ellipseEQ(const x,y,ecx,ecy,esmx2,esmy2: integer): double;
    begin
        result := (x - ecx) * (x - ecx) / esmx2 +
                  (y - ecy) * (y - ecy) / esmy2;
    end;
var
    map: TSeabedIndexMap;
    i,j: integer;
    p0, p1: TPoint;
    ellipsesemiMajorx2, ellipsesemiMajory2: integer;
begin
    map := self.addMap;
    if length(map.map) > 0 then
    begin
        if ((tool = kRect) or (tool = kEllipse)) and (self.currentClicks.Count >= 2) then
        begin
            p0 := self.screenToMap( self.currentClicks.First );
            p1 := self.screenToMap( self.currentClicks.Last );

            if tool = kRect then
                for i := min(p0.x,p1.x) to max(p0.x,p1.x) do
                    for j := min(p0.y,p1.y) to max(p0.y,p1.y) do
                        map.map[i,j] := self.layerInd;

            if tool = kEllipse then
            begin
                ellipsesemiMajorx2 := (p0.x - p1.x) * (p0.x - p1.x);
                ellipsesemiMajory2 := (p0.y - p1.y) * (p0.y - p1.y);
                for i := min(2*p0.x - p1.x,p1.x) to max(2*p0.x - p1.x,p1.x) do
                    for j := min(2*p0.y - p1.y,p1.y) to max(2*p0.y - p1.y,p1.y) do
                        if ellipseEQ(i,j,p0.x,p0.y,ellipsesemiMajorX2,ellipsesemiMajorY2) < 1.0 then
                            if (i >= 0) and (i < length(map.map)) and (j >= 0) and (j < length(map.map[0])) then
                                map.map[i,j] := self.layerInd;
            end;
        end;

        if (tool = kFreehand) and (self.currentClicks.Count >= 3) then
        begin
            freehandPoints(map);
            self.currentClicks.Clear;
        end;
    end;
    self.updateBMP;
    self.editboxesInit;
    self.updatePaintbox;
end;

function TSeabedMapFrm.addMap: TSeabedIndexMap;
begin
    result := TSeabedIndexMap.Create;
    result.cloneFrom(self.getMap);
    self.pastMaps.Add(result);
    while pastmaps.Count > maxPastMaps do
        pastMaps.Delete(0);
end;

procedure TSeabedMapFrm.btUndoClick(Sender: TObject);
begin
    currentClicks.Clear;
    if self.pastMaps.Count > 1 then
        self.pastMaps.delete(self.pastMaps.count - 1);
    self.updateBMP;
    self.updatePaintbox;
end;

procedure TSeabedMapFrm.FormCreate(Sender: TObject);
var
    i: integer;
begin
    tool := kRect;
    layerInd := 0;
    pastMaps := TSeabedIndexMapsList.create(true);
    pastMaps.add(TSeabedIndexMap.Create);
    tmpMap := TSeabedIndexMap.Create;
    bmp := TBitmap.create;
    bmp.PixelFormat := pf24bit;

    currentClicks := TPBClicksList.create(true);

    layerNames := TStringList.Create;

    self.sgPalette.ColWidths[0] := 20;
    self.sgPalette.ColWidths[1] := 40;
    self.sgPalette.ColWidths[2] := 140;
    setlength(colours, length(TDBSeaColours.defaultColors) + length(TDBSeaColours.alternateColors) + length(TDBSeaColours.defaultCustomColors));
    for i := 0 to length(TDBSeaColours.defaultColors) - 1 do
        colours[i] := TDBSeaColours.defaultColors[i];
    for i := 0 to length(TDBSeaColours.alternateColors) - 1 do
        colours[i + length(TDBSeaColours.defaultColors)] := TDBSeaColours.alternateColors[i];
    for i := 0 to length(TDBSeaColours.defaultCustomColors) - 1 do
        colours[i + length(TDBSeaColours.defaultColors) + length(TDBSeaColours.alternateColors)] := TDBSeaColours.defaultCustomColors[i];

    lbInfo.Caption := '';
end;

procedure TSeabedMapFrm.FormDestroy(Sender: TObject);
begin
    pastMaps.Clear;
    pastMaps.Destroy;
    layerNames.Destroy;
    currentClicks.Clear;
    currentClicks.Free;
    tmpMap.free;
    bmp.Free;
end;

procedure TSeabedMapFrm.FormShow(Sender: TObject);
begin
    if length(depths) > 0 then
        setlength(getMap.map,length(depths),length(depths[0]));
    self.updateBMP;
    editboxesInit;
    self.Timer1.Enabled := true;
end;

procedure TSeabedMapFrm.sgPaletteDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
    {change the font/colour for certain cells in the grid}
    with TStringGrid(Sender) do
    begin
        {header row with background}
        if (acol = 1) then
            Canvas.Brush.Color := self.getColour(aRow);

        {mitigation row in mid blue}
        if (ARow = layerInd) and (aCol = 2) then
        begin
            Canvas.Brush.Color := TDBSeaColours.darkBlue;
            canvas.Font.Color := clWhite;
            canvas.Font.Style := Canvas.Font.Style + [fsBold];
        end;

        {$IFDEF VER280}
        if DefaultDrawing then
            Rect.Left := Rect.Left - 4;
        Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+3, Cells[ACol, ARow]);
        {$ELSE}
        Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
        {$ENDIF}
    end;
end;

procedure TSeabedMapFrm.sgPaletteMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    aCol, aRow: Integer;
begin
    if (button = mbLeft) then
    begin
        sgPalette.MouseToCell(X, Y, aCol, aRow);
        self.layerInd := arow;
        self.sgPalette.Invalidate;
    end;
end;

procedure TSeabedMapFrm.Timer1Timer(Sender: TObject);
begin
    self.updatePaintbox;
    self.Timer1.Enabled := false;
end;

procedure TSeabedMapFrm.updatePaintbox(const x: integer = -1; const y: integer = -1);
var
    click1, click2: TPBClick;
    i, radius: integer;
    arect: TRect;
begin
    arect := self.imMap.ClientRect;
    if self.bmp.Width < self.bmp.Height then
        arect.Width := saferound(arect.height * bmp.Width / bmp.Height)
    else
        arect.Height := saferound(arect.width * bmp.height / bmp.width);
    self.imMap.Canvas.StretchDraw(aRect,self.bmp);

    self.imMap.Canvas.Pen.Color := clWhite;
    self.imMap.Canvas.Pen.Width := 3;
    self.imMap.Canvas.Brush.Style := bsClear;
    //self.immap.canvas.brush.color := clYellow;
    if self.mbDown then
    begin
        if (self.tool = kRect) and (self.currentClicks.Count >= 2) then
            self.imMap.Canvas.Rectangle(currentClicks.First.x, currentClicks.First.y, currentClicks.Last.x, currentClicks.Last.y);

        if (self.tool = kEllipse) and (self.currentClicks.Count >= 2) then
        begin
            click1 := self.currentClicks.First;
            click2 := self.currentClicks.Last;
            self.imMap.Canvas.ellipse(2*click1.x - click2.x,2*click1.y - click2.y,click2.x,click2.y);
        end;

        if (self.tool = kFreehand) and (self.currentClicks.Count >= 2) then
        begin
            click1 := self.currentClicks.first;
            self.imMap.Canvas.MoveTo(click1.x,click1.y);
            for i := 1 to self.currentClicks.Count - 1 do
            begin
                click2 := self.currentClicks[i];
                self.imMap.Canvas.LineTo(click2.x,click2.y);
            end;
        end;

        if (self.tool = kBrush) then
        begin
            radius := self.tbBrushSize.Position;
            self.imMap.Canvas.Brush.Style := bsSolid;
            self.immap.Canvas.Brush.Color := self.getColour(self.layerInd);
            self.imMap.Canvas.Pen.Width := 0;
            self.immap.canvas.pen.Color := self.getColour(self.layerInd);
            for click1 in self.currentClicks do
                self.imMap.Canvas.Ellipse(click1.x - radius, click1.y - radius, click1.x + radius, click1.y + radius);
        end;
    end
    else
    begin
    end;

    if (self.tool = kBrush) and (x >= 0) and (y >= 0) then
    begin
        self.imMap.Canvas.Pen.Color := clWhite;
        self.imMap.Canvas.Pen.Width := 2;
        radius := self.tbBrushSize.Position;
        self.imMap.Canvas.Ellipse(x - radius,y - radius, x + radius, y + radius);
    end;
end;

procedure TSeabedMapFrm.updateBMP;
    function landMask(const c: TColor): TColor;
    const
        darkenBy = 0.8;
    begin
        result := RGB(saferound(GetRValue(c) * darkenBy),saferound(GetGValue(c) * darkenBy), saferound(GetBValue(c) * darkenBy));
    end;
var
    map: TSeabedIndexMap;
    i,j: integer;
begin
    if self.pastMaps.Count > 0 then
    begin
        map := self.getMap;
        if length(map.map) > 0 then
        begin
            bmp.Width := length(map.map);
            bmp.height := length(map.map[0]);
            for i := 0 to bmp.Width - 1 do
                for j := 0 to bmp.height - 1 do
                begin
                    bmp.Canvas.Pixels[i,j] := getColour(map.map[i,j]);
                    if depths[i,j] < 0 then
                        bmp.Canvas.Pixels[i,j] := landMask(bmp.Canvas.Pixels[i,j]);
                end;
        end;
    end;
end;

procedure TSeabedMapFrm.sbBrushClick(Sender: TObject);
begin
    if tool <> kBrush then
    begin
        self.currentClicks.Clear;
        self.updatePaintbox();
    end;
    tool := kBrush;
end;

procedure TSeabedMapFrm.sbEllipseClick(Sender: TObject);
begin
    if tool <> kEllipse then
    begin
        self.currentClicks.Clear;
        self.updatePaintbox();
    end;
    tool := kEllipse;
end;

procedure TSeabedMapFrm.sbFreehandClick(Sender: TObject);
begin
    if tool <> kFreehand then
    begin
        self.currentClicks.Clear;
        self.updatePaintbox();
    end;
    tool := kFreehand;
end;

procedure TSeabedMapFrm.sbRectClick(Sender: TObject);
begin
    if tool <> kRect then
    begin
        self.currentClicks.Clear;
        self.updatePaintbox();
    end;
    tool := kRect;
end;

function  TSeabedMapFrm.screenToMap(const clickPoint: TPBClick):TPoint;
begin
    result.x := self.screenToMapX(clickPoint.x);
    result.y := self.screenToMapY(clickPoint.y);
end;

function  TSeabedMapFrm.screenToMapX(const x: integer):integer;
    function xaspectRatio(const s: TIntegerMatrix): double;
    begin
        result := length(s) / max(length(s),length(s[0]));
    end;
var
    map: TSeabedIndexMap;
begin
    map := self.getMap;
    if length(map.map) = 0 then
        result := 0
    else
        result := saferound(x / imMap.Width  * length(map.map) / xaspectRatio(map.map));
end;

function  TSeabedMapFrm.screenToMapY(const y: integer):integer;
    function yaspectRatio(const s: TIntegerMatrix): double;
    begin
        result := length(s[0]) / max(length(s),length(s[0]));
    end;
var
    map: TSeabedIndexMap;
begin
    map := self.getMap;
    if length(map.map) = 0 then
        result := 0
    else
        result := saferound(y / imMap.Height * length(map.map[0]) / yaspectRatio(map.map));
end;

//function  TSeabedMapFrm.mapToScreen(const mapPoint: TPoint):TPoint;
//var
//    map: TSeabedIndexMap;
//begin
//    map := self.getMap;
//    if length(map.map) = 0 then
//    begin
//        result.x := 0;
//        result.y := 0;
//    end
//    else
//    begin
//        result.x := saferound(mapPoint.x / length(map.map) * imMap.Width);
//        if length(map.map[0]) = 0 then
//            result.y := 0
//        else
//            result.y := saferound(mapPoint.y / length(map.map[0]) * imMap.Height);
//    end;
//end;

function  TSeabedMapFrm.getColour(const i: integer): TColor;
begin
    result := self.colours[i mod length(self.colours)];
end;

function  TSeabedMapFrm.getLayerIndAtScreenXY(const i,j: integer): integer;
var
    m,n: integer;
    x,y: double;
    map: TSeabedIndexMap;
begin
    map := self.getMap;
    if (not assigned(map)) or (length(map.map) = 0) then exit(0);
    if not (i < self.imMap.Width) and (j < self.imMap.Height) then exit(0);

    x := i / self.imMap.Width;
    y := j / self.imMap.Height;
    m := saferound(x * length(map.map));
    n := saferound(y * length(map.map[0]));
    if not ((m < length(map.map)) and (n < length(map.map[0]))) then exit(0);
    result := map.map[m,n];
end;

procedure TSeabedMapFrm.editboxesInit;
var
    i: integer;
begin
    self.sgPalette.ColCount := 3;
    self.sgPalette.ColWidths[0] := 20;
    self.sgPalette.ColWidths[1] := 40;
    self.sgPalette.ColWidths[2] := 130;

    self.sgPalette.RowCount := self.layerNames.Count;
    for I := 0 to self.layerNames.count - 1 do
    begin
        self.sgPalette.Cells[0,i] := tostr(i);
        self.sgPalette.Cells[1,i] := '';
        self.sgPalette.Cells[2,i] := self.layerNames[i];
    end;
end;

function  TSeabedMapFrm.getMap: TSeabedIndexMap;
begin
    if self.pastMaps.count = 0 then exit(nil);
    result := self.pastMaps.last;
end;

procedure TSeabedMapFrm.imMapClick(Sender: TObject);
begin
    case self.tool of
        kRect: ;
        kEllipse: ;
        kFreehand: ;
    else ;
    end;
end;

procedure TSeabedMapFrm.imMapMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    self.mbDown := true;
    case self.tool of
        kRect, kEllipse: begin
            self.currentClicks.Clear;
            self.currentClicks.add(TPBClick.Create(x,y,shift,button));
            self.currentClicks.add(TPBClick.Create(x,y,shift,button));
        end;
        kFreehand :begin
            self.currentClicks.add(TPBClick.Create(x,y,shift,button));
            self.updatePaintbox(x,y);
        end;
        kBrush: begin
            self.addMap;
            self.currentClicks.Clear;
            self.currentClicks.add(TPBClick.Create(x,y,shift,button));
            self.updatePaintbox(x,y);
        end;
    else ;
    end;
end;

procedure TSeabedMapFrm.imMapMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
const
    minDragDist = 5.0;
begin
    if self.mbDown then
    begin
        case self.tool of
        kRect, kEllipse: if self.distToClick(x,y,self.currentClicks.First) > minDragDist then begin
            self.currentClicks.Last.x := x;
            self.currentClicks.Last.y := y;
        end;
        kFreehand,kBrush: self.currentClicks.add(TPBClick.Create(x,y,shift,self.currentClicks.Last.mouseButton));
        else ;
        end;
        self.updatePaintbox(x,y);
    end
    else
    begin
        if self.getLayerIndAtScreenXY(x,y) < self.layerNames.Count then
            self.lbInfo.Caption := 'Layer under mouse: ' + tostr(self.getLayerIndAtScreenXY(x,y)) + ' - ' + self.layerNames[self.getLayerIndAtScreenXY(x,y)]
        else
            self.lbInfo.Caption := 'Layer under mouse: ' + tostr(self.getLayerIndAtScreenXY(x,y));
        if self.tool = kBrush then
            self.updatePaintbox(x,y);
    end;
end;

procedure TSeabedMapFrm.imMapMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
    radius: integer;
    click1: TPBClick;
    i,j: integer;
    map: TSeabedIndexMap;
begin
    self.mbDown := false;
    self.currentClicks.add(TPBClick.Create(x,y,shift,button));
    case self.tool of
        kRect: begin
        end;
        kEllipse: begin
        end;
        kFreehand :begin
        end;
        kBrush: begin
            radius := self.tbBrushSize.Position;
            if length(self.getMap.map) = 0 then exit;
            map := self.addMap;
            for click1 in self.currentClicks do
            begin
                for i := click1.x - radius to click1.x + radius do
                    for j := click1.y - radius to click1.y + radius do
                        if hypot(click1.x - i,click1.y - j) < radius then
                            if (screenToMapX(i) >= 0) and (screenToMapX(i) < length(map.map)) and
                               (screenToMapY(j) >= 0) and (screenToMapY(j) < length(map.map[0])) then
                                map.map[screenToMapX(i),screenToMapY(j)] := self.layerInd;
            end;
            updateBMP;
            self.updatePaintbox(x,y);
        end;
    else ;
    end;
end;

function TSeabedMapFrm.distToClick(const i,j: integer; const click: TPBClick): double;
var
    dx, dy: integer;
begin
    dx := click.x - i;
    dy := click.y - j;
    result := hypot(dx,dy);
end;

//procedure TSeabedMapFrm.setMapIndicesWithinRadiusFromXY(const x,y,radius,layerInd: integer);
//var
//    i,j: integer;
//    map: TSeabedIndexMap;
//begin
//    map := self.pastMaps.last;
//    if length(map.map) = 0 then exit;
//
//    for i := max(0,x - radius) to min(length(map.map) - 1,x + radius) do
//        for j := max(0,y - radius) to min(length(map.map[0]) - 1,y + radius) do
//            if hypot(i - x,j - y) < radius then
//                map.map[i,j] := layerInd;
//    self.updateBMP;
//end;

end.
