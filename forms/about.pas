{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit about;

interface

uses
    syncommons,
    Windows, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, pngimage, StrUtils, Dialogs, SysUtils,
  dBSeaconstants, mcklib, basetypes, OpenGL, globalsUnit,
  eventlogging;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    OKButton: TButton;
    ProgramIcon: TImage;
    lblProductName: TLabel;
    lblVersion: TLabel;
    lblCopyright: TLabel;
    lblGL: TLabel;
    mmGPL: TMemo;
    lblSerialNo: TLabel;
    lblComments: TLabel;
    lblExpiryDate: TLabel;
    lblMDAInternalUse: TLabel;
    lbBuild: TLabel;
    lbRunningMode: TLabel;
    lbDBInfo: TLabel;
    lbArchitecture: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
    AboutBox                : TAboutBox;
implementation

{$R *.dfm}

uses main;

procedure TAboutBox.FormCreate(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'FormCreate');

    self.lblComments.Caption := '';
    self.lblSerialNo.Caption := '';
    self.lblExpiryDate.Caption := '';
end;

procedure TAboutBox.FormShow(Sender: TObject);
begin
    lblVersion.Caption := 'Version ' + majorVersion(application.ExeName);

    {$IFDEF NON_MDA}
        lblMDAInternalUse.visible := false;
    {$ENDIF}

    lblGl.Caption := 'OpenGL ' + string(glGetString(GL_VENDOR)) + ' ' + string(glGetString(GL_RENDERER)) +
        ' ' + string(glGetString(GL_VERSION));

    lbBuild.Caption := 'Complete version: ' + fileVersion(application.ExeName);

     case runningMode of
        rmBlocked : lbRunningMode.Caption := 'Mode: blocked';
        rmDemo : lbRunningMode.Caption := 'Mode: demo';
        rmUnlicensed : lbRunningMode.Caption := 'Mode: unlicensed';
        rmTrial : lbRunningMode.Caption := 'Mode: trial';
        rmNormalKey : lbRunningMode.Caption := 'Mode: HASP key';
     end;

     {$IF Defined(LITE)}
     lbRunningMode.Caption := 'Free version';
     {$ENDIF}

     lbDBInfo.caption := 'Database versions: library ' + inttostr(mainform.DBVersion(TDataSource.dsMDA)) +
                                          '  user ' + inttostr(mainform.DBVersion(TDataSource.dsUser)) +
                                          '  program ' + inttostr(currentDBVersion);

     {$IFDEF CPUX64}
     lbArchitecture.Caption := 'Architecture: 64 bit MS Windows';
     {$ELSE}
     lbArchitecture.Caption := 'Architecture: 32 bit MS Windows';
     {$ENDIF}
end;

end.
 
