﻿{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit GLWindow;

interface

uses
    syncommons,
  system.Threading, system.SyncObjs, system.math, system.math.vectors, system.Typinfo,
  system.SysUtils, system.Classes, system.StrUtils, system.types,
  winapi.Windows, winapi.ShellApi, winapi.MMSystem,
  vcl.Graphics, vcl.Forms, vcl.Controls, vcl.Menus,
  vcl.StdCtrls, vcl.Dialogs, vcl.Buttons, winapi.Messages, vcl.ExtCtrls,
  vcl.ComCtrls, vcl.StdActns, vcl.imaging.jpeg, vcl.imaging.pngimage,
  vcl.imaging.gifimg,
  vcl.ActnList, vcl.ToolWin, vcl.ImgList,
  vcl.AppEvnts,
  Generics.Collections,
  synPdf,
  baseTypes, MCKLib, GLTypes,
    WorldScaleDialog, dglOpenGL,
   dBSeaColours, helperFunctions, srcPoint, crossSecEndpoint,
  probe, soundSource, dBSeaconstants,
  receiverArea, glDrawProgress, crosssec, flock,
  mycolor, levelConverter, haspSecurity, glRect,
  problemcontainer, geosetup, bathymetry, dBSeaOptions,
  globalsUnit, dBSeaObject, geooverlay, ImageOverlay,
  wrappedFPointArray,
  GLWindowOptions,
  animat, OtlParallel, SmartPointer, RayPlotting,
  eventLogging, QuaternionFuncs, marchingSquares,
  ESRIReader, latlong2UTM, importedLocationUnit;

type
    {$ScopedEnums on}
    TClickInd = (kStart,kEnd);
    TWatermarkVersion = (none,trial,lite);
    {$ScopedEnums off}
  TFPointScaleClicks = array[TClickInd] of TFPoint;
  TScaleClicks = array [TClickInd] of TPoint;

  GLColor = array[0..2] of double;

  TGLForm = class(TForm)
    aeDetectKeypress: TApplicationEvents;
    ilWatermark: TImageList;
    sdExportImage: TSaveDialog;

    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);

    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
                Shift: TShiftState; X, Y: Integer);
    procedure FormMouseDownSetScale(Sender: TObject; Button: TMouseButton;
                Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
                Shift: TShiftState; X, Y: Integer);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
                WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseEnter(Sender: TObject);
    procedure FormMouseLeave(Sender: TObject);
    procedure FormDblClick(Sender: TObject);

    procedure aeDetectKeypressMessage(var Msg: tagMSG; var Handled: Boolean);
    procedure tmWaterTextureOffsetTimer(Sender: TObject);
  private
  const
    SCENE1                     : uint = 1;
    buttonRotationAngle        : uint = 15;
    buttonPanDist              = 0.2;
    mouseRotationSensitivity   = 0.25;
    mousePanSensitivity        = 0.006;
    mouseZoomSensitivity       = 0.01;
    glMinZoom                  = -80;
    mouseWheelZoomSensitivity  = 0.05;
    CrossSecEndpointSize       = 0.015;
    sourceSelectThreshold      = 0.05;
    xsecGuideSelectThreshold   = 0.02;
    probeSelectThreshold       = 0.05;
    receiverAreaSelectThreshold = 0.01;
    sourceProbeTextTimeout     : LongWord = 10000;
    sourceThresholdTimeout     : LongWord = 10000;
    iTextHeightSmall           = 9;
    iTextHeightLarge           = 12;
    iGLFontChars               = 96;
    sGLFontName                = 'Lucida Sans Unicode';
    dGLTextXOffset             = 0.025;
    dGLTextYOffset             = -0.025;
    glAxesWidth                = 3.0;
    glCSEPBarWidth             = 3.0;
    XSecGuideX0                = 0.8;
    XSecGuideXw                = 0.2;
    XSecGuideY0                = 0.8;
    XSecGuideYw                = 0.2;
    CrossSecGuideEndpointSize  = 0.0015;
    timeToWaitBeforeShowDrawProgress: cardinal = 600; //in ms
    timeToWaitBeforeReShowDrawProgress: cardinal = 1500;
  var
    glContext                   : HGLRC;
    glDC                        : HDC;
    state                       : TGLWindowState;
    options                     : TGLWindowOptions;
    selectedObject     : TdBSeaObject;

    glQuaternion                : TQuaternion3D;
    glRot,glPan,lastGLSpins,lastGLPan : TFPoint;
    glZoom,lastGLZoom           : double;     //zoom factor

    sSourceProbeText            : string;
    dSourceProbeTextGX          : double;
    dSourceProbeTextGY          : double;
    dSourceProbeTextGZ          : double;
    lwSourceProbeTextSetTime    : LongWord;

    {variables needed for texture plotting}
    BathyTexture                : GLUint;
    BathyTextureIsCurrent       : boolean;

    //waterTexture                : GLUint;
    imageOverlayTexture         : GLuint;
    RenderTexture               : GLUint;
    fboId                       : GLUint;
    rboId                       : GLUInt;
    glRenderTextureZfix         : double;
    renderTextureSize           : integer;

    bEXTFramebufferObjectExists : boolean;

    {variables for setting worldscale}
    ScaleClicks                 : TScaleClicks;
    clicksCollected             : integer;

    fbDrawCrossSection          : boolean; //do we need to redraw GL Scene?

    theBathymetry               : GLUint; //needed for gl bathymetry display list
    theResults                  : GLUint; //needed for gl results display list
    FontSmall                   : GLuint; // Number of basic display list of characters
    FontLarge                   : GLuint; // Number of basic display list of characters

    glDrawBeginTime             : Cardinal;
    glProgressLastHide          : Cardinal;

    waterTextureOffset          : TFPoint;

    //procedure createWaterTexture;
    procedure ClearTexture(theTex: GLUint; aWidth, aHeight: integer);
    procedure SetupRenderTexture;
    function  BuildFont(hdc: HDC; fontHeight: integer): GLUint;
    procedure KillFont(theFont: GLUint);  //

    function  glPrint(const text: string; const theFont: GLUint; const x,y,z: double; const color: TColor): integer;
    procedure glPrint90degrees(const s: string; const theFont: GLUint; const x,y,z: double; const aCol: TColor);

    procedure drawSphereAt(const x,y,z,aSize,alpha: double; color: TColor);
    procedure drawMousePos(const x,y: integer; const dx: double = nan; const dy: double = nan);
    procedure drawSourceProbeText;
    procedure drawSourceThreshold;

    {drawing 3D and results}
    procedure draw3D(const componentVisibility: TBooleanDynArray);
    procedure draw3DBathymetry;
    procedure draw3DWater;
    procedure draw3DSources;
    procedure draw3DAxes;
    procedure draw3DProbes;
    procedure draw3DCrossSecGuide(const xsec: TCrossSec);
    procedure draw3DBorderText;
    procedure draw3DFlock;
    procedure draw3DScaleBar;
    procedure draw3DGrid;
    procedure draw3dRays;
    procedure draw3dOverlays;
    procedure draw3dSettingImageOverlay;

    procedure draw3DResults(const componentVisibility: TBooleanDynArray);
    procedure draw3DResultsSmooth(const LevsRA: TSingleMatrix; const iZInd: integer);
    procedure draw3DResultsSquares(const LevsRA: TSingleMatrix; const dLowerLevel,dUpperLevel: single; const iThisLevel, iZInd: integer);
    procedure draw3DResultsContours(const LevsRA: TSingleMatrix; const dContourLevel: single; const alpha, darkening: double; const iThisLevel, iZInd: integer);
    procedure draw3DExclusionZones(const drawFill, drawContour: boolean);
    procedure draw3DResultsPointCloud(const LevsRA: TSingleMatrix; const iZInd: integer);


    {drawing cross section and xsec results}
    procedure draw2D;
    procedure draw2DWater(const glmn,glmx,minDepth,maxDepth,dWaterGZ: double);
    procedure draw2DSeafloor(const glmn,glmx,minDepth,maxDepth,dLandGZ: double; const zProfile: TRangeValArray);
    procedure draw2dSources(const glmn,glmx,minDepth,maxDepth,range,maxPointDistRatio,dSourceGZ: double; const CSEP0,CSEP1: TCrossSecEndpoint);
    procedure draw2dProbes(const glmn,glmx,minDepth,maxDepth,range,maxPointDistRatio,dSourceGZ: double; const CSEP0,CSEP1: TCrossSecEndpoint);
    procedure draw2DAxesTicks(const glmn,glmx,minDepth,maxDepth,range,dTxtGZ: double);
    procedure draw2DLocationGuide;
    procedure draw2DLocationGuideResults(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix; const ColorMap: TMatrix; const alphaResults: double);

    procedure draw2DResults(const componentVisibility: TBooleanDynArray; const glmn,glmx,minDepth,maxDepth,range,dResultsSquaresGZ,dResultsContoursGZ: double; const CSEP0,CSEP1: TCrossSecEndpoint);
    procedure draw2DResultsSmooth(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix; const ColorMap: TMatrix; const alphaResults: double);
    procedure draw2DResultsSquares(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix;
                    const dLowerLevel,dUpperLevel: double;
                    const alphaResults: double; const iThisLevel: integer);
    procedure draw2DResultsContours(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix;
                    const dContourLevel: double;
                    const alpha, darkening: double; const iThisLevel: integer);


    {drawing setworldscale stuff}
    procedure drawSetWorldScale;

    {marching squares stuff}
    procedure drawMarchingTrisLines(const miMSLines: TIntPairMatrix; const xs, ys: TDoubleDynArray; const zs: double);
    procedure drawMarchingTrisBands(const miMSBands: TIntPairMatrix; const xs, ys: TDoubleDynArray; const zs: double);
    procedure drawMarchingSquaresLines(const miMSLines: TIntegerMatrix; const xs, ys: TDoubleDynArray; const zs: double);
    procedure drawMarchingSquaresBands(const miMSBands: TIntegerMatrix; const xs, ys: TDoubleDynArray; const zs: double);

    procedure setGLColorAndVertex(const col: tglColor; const point: TFPoint3);
    procedure setGLTextureAndVertex(const point: TFPoint3; const tex: TFPoint; const NanColor: glColor; const alpha: double);
    procedure drawglRectTriangle1(const rect: tglRect);
    procedure drawglRectTriangle2(const rect: tglRect);
    procedure drawglRectTriangle1Texture(const rect: tglRect; const NanColor: glColor; const alpha: double);
    procedure drawglRectTriangle2Texture(const rect: tglRect; const NanColor: glColor; const alpha: double);

    procedure setDrawCrossSection(const aBool: boolean);
    procedure updateLastGLValues;

    function  WorldToScreenFloat(const afP: TfPoint): TPoint;

    function  isBirdsEyeView: boolean;
    function  XSecCoord(const mn, mx, p: double): double;
    function  ViewCoordsString(const X,Y :integer): string;
    procedure checkMouseLocationObjects3D(const mX, mY: integer; const isMouseDown: boolean);
    procedure checkMouseLocationObjects2D(const mX, mY: integer);
    procedure updateBathymetryTexture;

    function  XSecGuideX(aX:double; reverse: boolean = false): double; inline;
    function  XSecGuideY(aY:double; reverse: boolean = false): double; inline;

    procedure checkDrawTime;
    function  getShowSourceThreshold: boolean;
    function  isMouseLocInXSecGuideView(const p: TPoint): boolean;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    pntXSec1, pntXSec2          : TPoint; //used when setting the cross section
    showFlock                   : boolean;
    currentLevsRA               : TSingleMatrix;
    resultsMovingPosIndForGIFTool         : integer;

    {bDrawCrossSection: whether we are drawing a cross section or not. I wanted
    to have this as a parameter to drawnow(boolean), but then the current state doesn't persist}
    property  bDrawCrossSection: boolean read fbDrawCrossSection write setDrawCrossSection;
    property  showSourceThreshold: boolean read getShowSourceThreshold;
    procedure didChangeVisibility(const ind: integer);

    procedure drawNow;
    procedure resetView;
    procedure initialSpinAnimation;

    procedure ExportCurrentScene(const iImageType: integer);
    function  get2ClicksForSetScale(): TFPointScaleClicks;
    procedure SetRuler;

    procedure forceDrawNow;//call this to force a draw right now
    procedure force2DrawNow;//call this to force 2x draw right now - ie draw both buffers

    procedure setRedraw;
    procedure setRedrawResults;
    procedure setRedrawBathymetry;
    procedure invalidateBathyTexture;
    procedure setSourceProbeText(const s: string; const wx,wy: double);
    procedure clearSourceProbeText;
    procedure setDrawSourceThresholds();
    procedure clearSourceThreshold;
    procedure rotateBy(x,y: double);
    function  screenToWorldFloat(const aP : TPoint): TFPoint;
    function  getExportBMP(const bmp: TBitmap; const suppressMessages: boolean): boolean;
    procedure BMPForMovingPosInd(const p: integer; const bmp: TBitmap);
    procedure bathyColorsBMP(const bmp: TBitmap);
    procedure setImageOverlayTexture(const bmp: TBitmap);
    procedure clearImageOverlayTexture();
    procedure setSettingOverlayScale(const makeActive: boolean);
    function  getSettingOverlayScale(): boolean;
    class function  getMarchingSquaresXorYGrid(const li: integer; const amin, amax: double): TDoubleDynArray;
    class function  levelChecking(const thisLev, convertedMax, convertedMin: single): single;
  end;

var
    GLForm: TGLForm;

implementation

uses Main, EditSourceFrame, EditWaterFrame, EditResultsFrame,
    editProbeFrame, EditSolveFrame, EditSetupFrame,
    EditSedimentFrame, EditFishFrame, ExportImageTitleBlock, pos3, options;

resourcestring
  rsGeneratingResultsDisplay = 'Generating results display...';
  rsIncorrectLengthOfComponentVisibilityArray = 'Incorrect length of compone' +
  'nt visibility array';
  rsPreparingLevelsGraphicsLayer = 'Preparing levels graphics layer...';
  rsGLErrorRenderingTexture = 'GL error rendering texture';
  rsSetScaleInstructions = 'Left click two positions to set distance scale. ' +
  'Enter the distance between the points in the world scale box. Press ESC t' +
  'o cancel';
  rsWrongGLVersion = 'The version of OpenGL installed on this computer does ' +
  'not support EXT_framebuffer_object and associated extensions. Exclusion z' +
  'ones cannot be drawn. Please upgrade to the latest OpenGL version.';
  rsGeneratingBathymetryDisplay = 'Generating bathymetry display...';
  rsValidHASPKeyNotFound = 'Valid HASP key not found, inserting watermark';
  rsDemoVersionExportDisabled = 'Demo version. Export is disabled';

{$R *.dfm}
{$B-}


{$REGION 'Setup_teardown'}
procedure TGLForm.FormCreate(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'FormCreate');

    try
        {version using dglOpenGL}
        eventLog.log(log, 'Get GL device context with handle ' + inttostr(self.handle));
        glDC := getDC(self.handle);        //get device context - from the windows API.

        if (glDC = 0) then
        begin
            eventLog.log(log, 'Unable to get GL device context!');
            eventLog.log(log, 'Last OS Error: ' + inttostr(GetLastError));
        end;

        eventLog.log(log, 'Create GL context with DC ' + uinttostr(glDC));
        GLContext := CreateRenderingContext(glDC,
                                            [opDoubleBuffered],
                                            24, {if colorbits changes, also update the image export functions}
                                            24,
                                            0,0,0,0);

        if (GLContext = 0) then
        begin
            eventLog.log(log, 'Unable to create GL rendering context!');
            eventLog.log(log, 'Last OS Error: ' + inttostr(GetLastError));
        end;

        eventLog.log(log, 'Activate rendering context: ' + UIntToStr(glContext));
        ActivateRenderingContext(glDC, GLContext);

        if GLContext = 0 then
            raise Exception.Create('wglCreateContext failed ' + ToStr(GetLastError));
        if not wglMakeCurrent(glDC,GLContext) then
            raise Exception.Create('wglMakeCurrent failed ' + ToStr(GetLastError));
        state.OpenGLReady := true;

        eventLog.log(log, 'Create viewport');
        glViewport(0, 0, clientWidth, ClientHeight);    //make the openGL viewport
        glPixelStorei(GL_PACK_ALIGNMENT, 1); //no padding at the end of rows

        //glDraw3 := TGLDraw3D.create(self);

        eventLog.log(log, 'Build GL fonts');
        FontSmall := BuildFont(glDC,iTextHeightSmall);
        FontLarge := BuildFont(glDC,iTextHeightLarge);

        glQuaternion := TQuaternion3D.Identity;
        glRot.x := 20;              // rotation X
        glRot.y := -60;              // rotation Y
        glPan.x := 0;           // panning X
        glPan.y := 0;           // panning Y
        glZoom := 0;      //zoom factor
        updateLastGLValues;

        {create the points defining the ends of the cross section}
        pntXSec1.X := 0;
        pntXSec1.Y := 0;
        pntXSec2.X := 0;
        pntXSec2.Y := 0;

        state.bFirstRun                 := true;
        state.mouse                     := TMouseInfo.create(0,0);
        state.bDrawNowNeeded            := true;
        fbDrawCrossSection              := false;
        state.bLastGLWasXSec            := false;
        state.bBathymetryListReady      := false;
        state.bResultsListReady         := false;
        options.bSettingWorldScale      := false;
        options.bSettingRuler           := false;
        state.canShowProgressForm       := true;
        state.canHideProgressForm       := true;
        options.fShowSourceThreshold    := false;
        state.imageOverlaySelectedCorner.null;
        showFlock               := false;
        glProgressLastHide      := 0;
        clicksCollected         := 0;
        selectedObject   := nil;
        lwSourceProbeTextSetTime     := timeGetTime;
        sSourceProbeText             := '';
        options.showSourceProbeText         := false;
        invalidateBathyTexture;
        glRenderTextureZfix          := 0;
        resultsMovingPosIndForGIFTool := -1;

        {check if the local version of GL on the user's machine has the framebuffer extensions needed
        so that we can draw onto a framebuffer other than the screen render framebuffer. }
        eventLog.log(log, 'Check for frame buffer objects API');
        bEXTFramebufferObjectExists := AnsiContainsText(string(glGetString(GL_EXTENSIONS)),'EXT_framebuffer_object') and
            (wglGetProcAddress('glBindFramebufferEXT'           ) <> nil) and
            (wglGetProcAddress('glGenRenderbuffersEXT'          ) <> nil) and
            (wglGetProcAddress('glBindFramebufferEXT'           ) <> nil) and
            (wglGetProcAddress('glFramebufferTexture2DEXT'      ) <> nil) and
            (wglGetProcAddress('glFramebufferRenderbufferEXT'   ) <> nil) and
            (wglGetProcAddress('glCheckFramebufferStatusEXT'    ) <> nil);

        if bEXTFramebufferObjectExists then
        begin
            eventLog.log(log, 'Create render texture');
            SetupRenderTexture
        end
        else
            showmessage(rsWrongGLVersion);

        {generate call lists}
        eventLog.log(log, 'Generate call lists');
        theBathymetry := glGenLists(1);
        theResults := glGenLists(1);
        //createWaterTexture;

    except on e: Exception do
    begin
        eventLog.logException(log, e.toString);
        showmessage('There was an error on creating the main graphics area: ' + e.tostring);
    end;
    end;
end;

procedure TGLForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    glDeleteLists(theBathymetry,1);
    glDeleteLists(theResults,1);

    Action := caFree;
end;

procedure TGLForm.FormDestroy(Sender: TObject);
begin
    glDeleteFramebuffersEXT(1, @fboId);
    glDeleteTextures(1, @RenderTexture);
    glDeleteRenderbuffersEXT(1, @rboId);

    glDeleteTextures(1, @BathyTexture );

    glDeleteLists(theBathymetry,1);
    glDeleteLists(theResults,1);
    KillFont(FontSmall);
    KillFont(FontLarge);

    wglDeleteContext(GLContext);     //release openGL render context
    ReleaseDC( handle, glDC );       //release openGL device conte

    selectedObject := nil;
end;

procedure TGLForm.FormPaint(Sender: TObject);
begin
    ForceDrawNow;
end;

procedure TGLForm.FormResize(Sender: TObject);
begin
    ActivateRenderingContext(glDC, GLContext);
    {create the openGL viewport}
    glViewport(0,0,clientWidth,clientHeight);

    {the size of the fbo etc has changed, recreate them}
    if bEXTFramebufferObjectExists then
        SetupRenderTexture;

    //pParams := @params;
    //glGetIntegerv(Gl_Viewport, pParams);
    //setglwidth(ClientWidth);
    //setglHeight(ClientHeight);
end;

{$ENDREGION}

procedure TGLForm.CreateParams(var Params: TCreateParams);
begin
    inherited CreateParams(Params);
    Params.style := Params.style and not WS_CAPTION;
    Params.style := Params.style and not WS_SIZEBOX;
end;

//procedure TGLForm.createWaterTexture;
//const
//    dim = 2000;
//var
//    pixels: TBytes;
//    i,j,k: integer;
//    b: byte;
//begin
//    setlength(pixels, dim * dim * 4);
//    for j := 0 to dim - 1 do
//        for i := 0 to dim - 1 do
//        begin
//            k := ((j * dim) + i) * 4;
//            b := round(255 * RandomRange(500, 999) / 1000);
//            pixels[k    ] := b;
//            pixels[k + 1] := b;
//            pixels[k + 2] := b;
//            pixels[k + 3] := 255; //alpha
//        end;
//
//    if glIsTexture(waterTexture) then
//        glDeleteTextures(1, @waterTexture);
//    glGenTextures( 1, @waterTexture );
//    glBindTexture( GL_TEXTURE_2D, waterTexture );
//    ClearTexture(imageOverlayTexture, dim, dim);
//
//    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//
//    gluBuild2DMipmaps( GL_TEXTURE_2D, GL_RGBA, dim, dim, GL_RGBA, GL_UNSIGNED_BYTE, @pixels[0]);
//end;

function  TGLForm.XSecGuideX(aX:double; reverse: boolean = false): double;
begin
    if reverse then
        result := 2*(aX - XSecGuideX0)/XSecGuideXw - 1
    else
        result := XSecGuideX0 + XSecGuideXw*(aX + 1)/2;
end;

function  TGLForm.XSecGuideY(aY:double; reverse: boolean = false): double;
begin
    if reverse then
        result := 2*(aY - XSecGuideY0)/XSecGuideYw - 1
    else
        result := XSecGuideY0 + XSecGuideYw*(aY + 1)/2;
end;

procedure TGLForm.ExportCurrentScene(const iImageType: integer);
    procedure produceOutputImageFromBMP(const bmp: TBitmap; const savefilename: TFileName);
    var
        jpg: ISmart<TJpegImage>;
        png: ISmart<TPNGImage>;
        pdf: ISmart<TPdfDocumentGDI>;
        gif: ISmart<TGIFImage>;
    begin
        case iImageType of
        0: bmp.SaveToFile(savefilename);
        1: begin
                jpg := TSmart<TJpegImage>.create(TJpegImage.Create());
                jpg.CompressionQuality := 95;
                jpg.Assign(bmp);
                jpg.SaveToFile(savefilename);
            end;
        2: begin
                png := TSmart<TPNGImage>.create(TPNGImage.Create());
                png.Assign(bmp);
                png.SaveToFile(savefilename);
            end;
        3: begin
                {using the synpdf library, which allows us access to a TCanvas to draw on}
                pdf := TSmart<TPdfDocumentGDI>.create(TPdfDocumentGDI.Create());
                //TODO some work on this so that it will be more sensible on A4
                pdf.DefaultPaperSize := psA3;
                pdf.DefaultPageLandscape := true;
                pdf.AddPage;
                pdf.VCLCanvas.Draw(0,0,bmp);
                //pdf.VCLCanvas.StretchDraw(pdf.VCLCanvas.ClipRect,bmp);
                pdf.SaveToFile(savefilename);
            end;
        4: begin
                gif := TSmart<TGIFImage>.create(TGIFImage.Create);
                gif.SetSize(bmp.width, bmp.height);
                gif.add(bmp);
                gif.SaveToFile(savefilename);
            end;
        end;
    end;
const
    imageTypes: array[0..4] of string = ('bmp','jpg','png','pdf','gif');
    filterTypes: array[0..4] of string = ('BMP file|*.bmp','JPG file|*.jpg','PNG file|*.png','PDF file|*.pdf','GIF file|*.gif');
var
    bmpGL: ISmart<TBitmap>;
begin
    if runningMode = rmDemo then
    begin
        showmessage(rsDemoVersionExportDisabled);
        Exit();
    end;

    if runningMode = rmTrial then
    begin
        //showmessage('Trial version. Export is disabled');
        //Exit();
    end;

    if not directoryExists(DefaultSavePath) then
        DefaultSavePath := THelper.GetMyDocumentsPath;
    sdExportImage.InitialDir := DefaultSavePath;
    sdExportImage.FilterIndex := 0;
    sdExportImage.Filter := filterTypes[iImageType] + '|All files|*.*';
    sdExportImage.DefaultExt := imageTypes[iImageType];

    if not sdExportImage.Execute then exit;
    DefaultSavePath := ExtractFilePath(sdExportImage.filename);

    bmpGL := TSmart<TBitmap>.create(TBitmap.create);
    if not getExportBMP(bmpGL, false) then exit;
    produceOutputImageFromBMP(bmpGL, sdExportImage.filename);
end;

function TGLForm.getExportBMP(const bmp: TBitmap; const suppressMessages: boolean): boolean;
const
    iItemSpacing     = 8;
    iTitleBlockWidth = 300;

    function addWatermark(const bmp: TBitmap; const watermarkVersion: TWatermarkVersion): boolean;
    const
        watermarkAlpha: double = 0.3;
    var
        bmpWaterMark: ISmart<TBitmap>;
        i,j: integer;
    begin
        bmpWaterMark := TSmart<TBitmap>.create(TBitmap.Create());

        bmpWaterMark.PixelFormat := pf24bit;
        bmpWaterMark.AlphaFormat := afIgnored;
        if not ilWatermark.GetBitmap(ifthen(watermarkVersion = TWatermarkVersion.trial,0,1),bmpWaterMark) then
            Exit(false)
        else
           for i := 0 to bmp.width - 1 do
                for j := 0 to bmp.height - 1 do
                    if bmpWaterMark.Canvas.Pixels[i mod bmpWaterMark.Width,
                                                  j mod bmpWaterMark.Height] > 0 then
                        //don't fill in every possible square - just some of them
                        if (((i div bmpWaterMark.Width ) mod 2) = ((j div bmpWaterMark.Height) mod 2)) then
                        begin
                            bmp.Canvas.Pixels[i,j] := TDBSeaColours.colorMix(bmp.Canvas.Pixels[i,j],
                                bmpWaterMark.Canvas.Pixels[i mod bmpWaterMark.Width,j mod bmpWaterMark.Height],
                                watermarkAlpha);
                        end;

        result := true;
    end;

    function  generateLengthScaleBarBMP(d1px: double; aWidth: integer): TBitmap;
    const
        iDefaultHeight = 100;
        iBarHeight = 10;
        iBarTop = 50;
        iBarLeft = 25;
    var
        iBarWidth: integer;
        i: integer;
        dTens: double;
    begin
        result := TBitmap.Create();
        result.SetSize(aWidth, iDefaultHeight);

        iBarWidth := aWidth - 50;
        if iBarWidth <= 0 then Exit;

        if d1px <= 0 then Exit;

        d1px := m2f(d1px, UWAOpts.DispFeet);

        dTens := power(10,safefloor(log10(d1px*iBarWidth)));
        iBarWidth := safetrunc(dTens/d1px);

    //    if 4*iBarWidth < aWidth - 50 then
    //        iBarWidth := iBarWidth*4
    //    else
        if 2*iBarWidth < aWidth - 50 then
        begin
            iBarWidth := iBarWidth * 2;
            dTens := dTens * 2;
        end;

        {colour a border to the bar}
        result.Canvas.Pen.Color := clBlack;
        result.Canvas.Pen.Width := 1;
        result.Canvas.Brush.Color := clWhite;
        result.Canvas.Rectangle(iBarLeft,
                                iBarTop,
                                iBarLeft + iBarWidth,
                                iBarTop + iBarHeight);

        for i := 0 to 1 do
        begin
            if i mod 2 = 0 then
                result.Canvas.Brush.Color := clWhite
            else
                result.Canvas.Brush.Color := clBlack;
            result.Canvas.Rectangle(iBarLeft + i*(iBarWidth div 2),
                                    iBarTop,
                                    iBarLeft + (i+1)*(iBarWidth div 2),
                                    iBarTop + iBarHeight);
        end;

        result.Canvas.Font := Font;
        result.Canvas.Brush.Color := clWhite;
        result.Canvas.Font.Name := sGLFontName;
        result.Canvas.Font.Size := 8;
        result.Canvas.TextOut(iBarLeft,20,'0');
        result.Canvas.TextOut(iBarLeft + iBarWidth,20,tostr(dTens) + ifthen(UWAOpts.DispFeet,' ft',' m'));
        //result.Canvas.TextOut(50,50,'test2');
    end;

    procedure addLengthScaleToBMP(const bmpTitleBlock: TBitmap; var iCurrTop: integer);
    var
        bmpTmp: ISmart<TBitmap>;
        P1: TPoint;
        Pf1: TFPoint;
        dx1,dx2: double;
        d1px: double;
    begin
        P1.X := 0;
        P1.Y := 0;
        Pf1 := screenToWorldFloat(P1);
        dx1 := Pf1.X;
        P1.X := self.width;
        P1.Y := 0;
        Pf1 := screenToWorldFloat(P1);
        dx2 := Pf1.X;
        d1px := (dx2 - dx1)/self.width;
        bmpTmp := TSmart<TBitmap>.create(generateLengthScaleBarBMP(d1px,iTitleBlockWidth));
        bmpTitleBlock.Canvas.Draw(0,iCurrTop,bmpTmp);
        iCurrTop := iCurrTop + bmpTmp.Height;
    end;

var
    i,j            : integer;
    bmpTitleBlock  : ISmart<TBitmap>;
    bmpTmp         : ISmart<TBitmap>;
    data           : TBytes;
    //thisInd        : integer;
    titleBlockFrame: ISmart<TFormExportTitleBlock>;
    iCurrTop       : integer;
    tmpForm        : ISmart<TForm>;
    tmpMemo        : ISmart<TMemo>;
    watermarkVersion: TWatermarkVersion;
begin
    bmpTitleBlock := TSmart<TBitmap>.create(TBitmap.Create());

    bmpTitleBlock.Width := iTitleBlockWidth;
    bmpTitleBlock.Height := 5000;
    iCurrTop := 0;

    {get the bmpTitleBlock from the created frame, and paste it onto the top of our bmp}
    bmpTmp := nil;
    titleBlockFrame := TSmart<TFormExportTitleBlock>.create(TFormExportTitleBlock.create(self)); //The Title Block, contains info about the project
    bmpTmp := TSmart<TBitmap>.create(titleBlockFrame.getBMP(iTitleBlockWidth,titleBlockFrame.ClientHeight));
    bmpTitleBlock.Canvas.Draw(0,iCurrTop,bmpTmp);
    iCurrTop := iCurrTop + bmpTmp.Height + iItemSpacing;

    {add the TMemo freetext field from the options form. For some reason, doing this with a
    TMemo is particularly hard}
    if optionsForm.mmExportText.Text <> '' then
    begin
        tmpForm := TSmart<TForm>.create(TForm.Create(self));
        tmpMemo := TSmart<TMemo>.create(TMemo.Create(tmpForm));

        {making this tmpform slightly smaller than the memo and meving the memo to -1,-1
        means we don't pick up the tmemo border of 1 px width. could also use
        borderstyle = bsnone}
        tmpForm.ClientWidth  := optionsForm.mmExportText.Width  - 2;
        tmpForm.ClientHeight := optionsForm.mmExportText.Height - 2;
        tmpMemo.Parent := tmpForm;
        tmpMemo.Left := -1;
        tmpMemo.Top  := -1;
        tmpMemo.Width  := optionsForm.mmExportText.Width ;
        tmpMemo.Height := optionsForm.mmExportText.Height;
        tmpMemo.Text   := optionsForm.mmExportText.Text;

        {calling form.show makes the form paint and paint the tmemo. put it offscreen so we don't see}
        tmpForm.Left := -1000; // put it off the screen
        tmpForm.Show();
        bmpTmp := TSmart<TBitmap>.create(tmpForm.GetFormImage());

        bmpTitleBlock.Canvas.Draw(0,iCurrTop,bmpTmp);
        iCurrTop := iCurrTop + bmpTmp.Height + iItemSpacing;
    end;

    {if results exist, then we also want to include the results colorbar
    and the level labels next to it. we make the image a little wider, and
    use GetFormImage to grab the whole mainform bitmap (not sure if you
    can use the same method to get opengl bitmap or not)}
    if container.scenario.resultsExist() then
    begin
        {if we just opened a file, then it is possible that the graphics on the results tab are not yet updated}
        framResults.frameshow(nil);

        {create a temp bmp containing the results colorbar area, then add it to the current BMP}
        bmpTmp := TSmart<TBitmap>.create(framResults.getResultsScaleBMP(iTitleBlockWidth,
                                                framResults.imResultsColorBar.Height + 20,
                                                0,
                                                clWhite));
        bmpTitleBlock.Canvas.Draw(0,iCurrTop,bmpTmp);
        iCurrTop := iCurrTop + bmpTmp.Height + iItemSpacing;
    end;

    {add the logo image from the options form}
    if OptionsForm.bLogoImageLoaded then
    begin
        bitBlt(bmpTitleBlock.Canvas.Handle,0,iCurrTop,OptionsForm.imLogo.Width,OptionsForm.iLogoImageHeight,OptionsForm.imLogo.Picture.Bitmap.Canvas.Handle,0,0,SRCCOPY);
        iCurrTop := iCurrTop + OptionsForm.iLogoImageHeight + iItemSpacing;
    end;

    if isBirdsEyeView() then
        addLengthScaleToBMP(bmpTitleBlock,iCurrTop);

    {copy screen pixels to bmp}
    bmp.PixelFormat := pf24bit; //3x 8 bit  RGB
    bmp.SetSize(self.Width + iTitleBlockWidth, self.Height);
    {if necessary to accomodate extra height of scale bar, increase bmp height}
    if bmp.Height < iCurrTop then
        bmp.Height := iCurrTop;

    {fill in the entire bmp with the background colour, which may be needed if glheight is
    less than iCurrTop}
    bmp.Canvas.Pen.Color      := UWAOpts.BackgroundColor;
    bmp.Canvas.Brush.Color    := UWAOpts.BackgroundColor;
    bmp.Canvas.Rectangle(bmp.Canvas.ClipRect);

    setlength(data,3*ClientWidth);
    //glReadPixels(0,0,ClientWidth,clientHeight,GL_RGB,GL_UNSIGNED_BYTE,@data[0]);
    for j := 0 to clientHeight - 1 do
    begin
        //read 1 row at a time
        glReadPixels(0,self.height - 1 - j,self.Width,1,GL_RGB,GL_UNSIGNED_BYTE,@data[0]);
        for i := 0 to ClientWidth - 1 do
            bmp.Canvas.Pixels[i, j] := RGB(data[0 + 3*i], //write each pixel to the bitmap. note the reversed y axis
                                           data[1 + 3*i],
                                           data[2 + 3*i]);
    end;

    bmp.Canvas.Draw(self.width,0,bmpTitleBlock); {copy all the title bar bmp to the bmpGL}

    //watermark
    watermarkVersion := TWatermarkVersion.{$IFDEF LITE}lite{$ELSE}none{$ENDIF};
    {$IFNDEF DEBUG}
    if ((runningMode = rmNormalKey) and (not theHASPSecurity.HASPFormWhileRunningChecks)) then
    begin
        if not suppressMessages then
            showmessage(rsValidHASPKeyNotFound);
        watermarkVersion := TWatermarkVersion.trial;
    end;
    {$ENDIF}

    if (runningMode in [rmDemo, rmTrial]) or (watermarkVersion <> TWatermarkVersion.none) then
        if (not addWatermark(bmp,watermarkVersion)) then exit(false);

    result := true;
end;

{$REGION 'mouse_callbacks'}
procedure TGLForm.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    state.mouse := TMouseInfo.createWithButton(x,y,Button);
    if bDrawCrossSection then
        checkMouseLocationObjects2D(X,Y)
    else
        checkMouseLocationObjects3D(X,Y,true);

    if not assigned(Mainform) then Exit;

    if assigned(selectedObject) then
    begin
        if selectedObject is TSource then
        begin
            {go to sources page, and update the currently selected source}
            Mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Source);
            if assigned(framSource) then
            begin
                framSource.cbSource.ItemIndex := container.scenario.sources.IndexOf(TSource(selectedObject));
                framSource.cbSourceChange(self);
            end;
        end
        else
        if selectedObject is TProbe then
        begin
            {go to weightings page, and update the currently selected probe}
            Mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Probes);
            if assigned(framProbe) then
            begin
                framProbe.cbProbe.ItemIndex := container.scenario.theRA.probes.IndexOf(TProbe(selectedObject));
                framProbe.cbProbeChange(self);
            end;
        end;
    end;
end;

procedure TGLForm.FormMouseDownSetScale(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    if Button = MBLeft then
    begin
        {rangecheck, just in case the user double clicked}
        if clicksCollected <= integer(high(TClickInd)) then
        begin
            ScaleClicks[TClickInd(clicksCollected)].X := X;
            ScaleClicks[TClickInd(clicksCollected)].Y := Y;
            inc(clicksCollected);
        end;
    end;
end;

procedure TGLForm.FormDblClick(Sender: TObject);
begin
    if self.isMouseLocInXSecGuideView(state.mouse.point) then
        framSetup.btSetupCrossSecClick(self);
end;

procedure TGLForm.FormMouseEnter(Sender: TObject);
begin
    Screen.Cursor := crCross;
end;

procedure TGLForm.FormMouseLeave(Sender: TObject);
begin
    Screen.Cursor := crDefault;
end;

function TGLForm.isMouseLocInXSecGuideView(const p: TPoint): boolean;
var
    mgX, mgY: double;
begin
    //this is ONLY for the xsec guide at top right, will need something else if we want to do things in the main xsec view
    mgX := (p.X/ClientWidth*2) - 1;
    mgY := ((clientHeight-p.Y)/clientHeight*2) - 1;
    mgX := XSecGuideX(mgX,true);
    mgY := XSecGuideY(mgY,true);
    result := (mgX > -1) and (mgX < 1) and (mgY > -1) and (mgY < 1);
end;

procedure TGLForm.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
    procedure rotationFunc();
    begin
        UWAOpts.setVisibility(ord(veEndpoints),false); {turn off endpoints visibility, as we don't want to still see them if we start rotating}
        self.glQuaternion := (TQuaternion3D.create(pi / 180 * mouseRotationSensitivity*(X - state.mouse.X),
            pi / 180 * mouseRotationSensitivity*(Y - state.mouse.Y),
            0).Normalize * self.glQuaternion).Normalize;
        glRot.x := TQuaternionFuncs.toEulerAngles(self.glQuaternion).x / pi * 180;
        glRot.y := TQuaternionFuncs.toEulerAngles(self.glQuaternion).y / pi * 180;
        //glRot.x := glRot.x - mouseRotationSensitivity*(state.mouse.X - X);
        //glRot.y := glRot.y - mouseRotationSensitivity*(state.mouse.Y - Y);
    end;

    procedure panFunc();
    begin
        glPan.x := glPan.x - mousePanSensitivity*(state.mouse.X - X)*(1 + mouseZoomSensitivity*glZoom);
        glPan.y := glPan.y + mousePanSensitivity*(state.mouse.Y - Y)*(1 + mouseZoomSensitivity*glZoom);
    end;

    procedure crossSecUpdateMouseLoc(const X, Y: integer; out tmpX, tmpY: double);
    var
        mgX, mgY: double;
    begin
        //this is ONLY for the xsec guide at top right, will need something else if we want to do things in the main xsec view
        mgX := (X/ClientWidth*2) - 1;
        mgY := ((clientHeight-Y)/clientHeight*2) - 1;
        mgX := XSecGuideX(mgX,true);
        mgY := XSecGuideY(mgY,true);
        tmpX := container.geoSetup.g2w(mgX);
        tmpY := container.geoSetup.g2w(mgY);
    end;

    procedure normalUpdateMouseLoc(const x, y: integer; out tmpX, tmpY: double);
    var
        mgX, mgY: double;
    begin
        mgX := ((X/ClientWidth*2) - 1 - glPan.x)/(1 + mouseZoomSensitivity*glZoom);
        mgY := (((clientHeight-Y)/clientHeight*2) - 1 -glPan.y)/(1 + mouseZoomSensitivity*glZoom);
        tmpX := container.geoSetup.g2w(mgX);
        tmpY := container.geoSetup.g2w(mgY);
    end;

    procedure updateSourcePos(const tmpX, tmpY: double; const src: TSource);
    begin
        if src.dragPos(tmpX,tmpY) then
            self.setRedraw();
        clearSourceProbeText();
        clearSourceThreshold;
        framSource.EditBoxesInit();
        lastAction := TLastAction.kMoveSource;
    end;

    procedure updateSourceMovingPos(const tmpX, tmpY: double; const srcMovingPos: TSrcPoint);
    begin
        if srcMovingPos.dragMovingPos(tmpX,tmpY) then
            self.setRedraw();
        clearSourceProbeText();
        lastAction := TLastAction.kMoveSourceMovingPos;
    end;

    procedure updateProbePos(const tmpX, tmpY: double; const prb: TProbe);
    begin
        if prb.dragPos(tmpX,tmpY) then
            self.setRedraw();
        clearSourceProbeText();
        framProbe.EditBoxesInit();
        Application.ProcessMessages;
        lastAction := TLastAction.kMoveProbe;
    end;

    procedure updateCrossSecEndpointPos(const tmpX, tmpY: double; const csep: TCrossSecEndpoint);
    begin
        if csep.setPos(tmpX,tmpY) then
            self.setRedraw();
    end;

    procedure updateImageOverlayPos(const tmpX, tmpY: double; const corner: TRectControl);
    begin
        container.image.setCornerPos(tmpx, tmpy, corner);
        //self.setRedrawBathymetry;
        self.setRedraw;
    end;
var
    tmpX, tmpY: double;
begin
    {this is the main function where we react to the user's mouse input on the TGLwin.
    check for various combinations of mouse click and shiftstate, and do things like
    spin/pan/zoom accordingly}

    {make sure to set bDrawNowNeeded := true; for every single instance where it is needed!}

    if state.mouse.button.HasValue then
    begin
        case state.mouse.button.val of
            mbleft :
            begin
                {Left mouse button: check state of ctrl, alt and shift keys}

                if ssCtrl in Shift then
                    rotationFunc();

                if ssShift in Shift then
                    glZoom := glZoom + (state.mouse.X - X) + (state.mouse.Y - Y);

                if ssAlt in Shift then
                    panFunc();

                if [ssShift, ssCtrl, ssAlt] * Shift = [] then {none of alt, ctrl, shift are held}
                begin
                    if bDrawCrossSection then
                        crossSecUpdateMouseLoc(x,y, tmpX, tmpY)
                    else
                        normalUpdateMouseLoc(x, y, tmpX, tmpY);

                    if options.bSettingImageOverlay and state.imageOverlaySelectedCorner.HasValue then
                        updateImageOverlayPos(tmpx,tmpY,self.state.imageOverlaySelectedCorner)
                    else if assigned(selectedObject) then
                    begin
                        forceRange(tmpX, container.scenario.theRA.Pos1.X, container.scenario.theRA.Pos2.X);
                        forceRange(tmpY, container.scenario.theRA.Pos1.Y, container.scenario.theRA.Pos2.Y);

                        if selectedObject is TSource then
                            updateSourcePos(tmpX,tmpY,TSource(selectedObject))
                        else if selectedObject is TSrcPoint then
                            updateSourceMovingPos(tmpX,tmpY, TSrcPoint(selectedObject))
                        else if selectedObject is TProbe then
                            updateProbePos(tmpX,tmpY,TProbe(selectedObject))
                        else if selectedObject is TCrossSecEndpoint then
                            updateCrossSecEndpointPos(tmpX,tmpY,TCrossSecEndpoint(selectedObject));
                    end;
                end;
            end;
            mbMiddle:
            begin
                {decided to remove this, as it was causing problems}

                //{middle mouse button: zoom}
                //glZoom := glZoom + (mouseX - X) + (mouseY - Y);
            end;
            mbRight:
            begin
                {right mouse button: pan}

                glPan.x := glPan.x - mousePanSensitivity*(state.mouse.X - X)*(1 + mouseZoomSensitivity*glZoom);
                glPan.y := glPan.y + mousePanSensitivity*(state.mouse.Y - Y)*(1 + mouseZoomSensitivity*glZoom);
            end;
        end; //case mousebutton
    end //mouseButtonHeld
    else
    begin
        {no mousebutton held}
        cursor := crDefault;

        checkMouseLocationObjects3D(X,Y,false); //checks to see if we should change any source colors
        if assigned(selectedObject) then
            state.bDrawNowNeeded := true;
    end;

    if not inputDisabled then
        drawnow();

    state.mouse.X := X;
    state.mouse.Y := Y;
end;

procedure TGLForm.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
    procedure updateSelectedSource(const tmpX,tmpY: double; const src: TSource);
    begin
        if src.setPos(tmpX, tmpY, src.Pos.Z) then
            self.setRedraw();
        clearSourceProbeText();
        clearSourceThreshold;
        framSource.EditBoxesInit();
        state.bDrawNowNeeded := true;
    end;

    procedure updateSelectedProbe(const tmpX,tmpY: double; const prb: TProbe);
    begin
        if prb.setPos(tmpX,tmpY,prb.Pos.Z) then
            self.setRedraw();
        clearSourceProbeText();
        framProbe.EditBoxesInit();
    end;
var
    mgX, mgY: double;
    tmpX, tmpY: double;
begin
    if (Button = mbleft) and ([ssShift, ssCtrl, ssAlt] * Shift = []) then
    begin
        {none of alt, ctrl, shift are held}

        { if we already have clicked and held on a source then srcMoving is true}
        mgX := ((X/ClientWidth*2) - 1 - glPan.x)/(1 + mouseZoomSensitivity*glZoom);
        mgY := (((clientHeight-Y)/clientHeight*2) - 1 -glPan.y)/(1 + mouseZoomSensitivity*glZoom);

        tmpX := container.geoSetup.g2w(mgX);
        tmpY := container.geoSetup.g2w(mgY);
        forceRange(tmpX, container.scenario.theRA.Pos1.X, container.scenario.theRA.Pos2.X);
        forceRange(tmpY, container.scenario.theRA.Pos1.Y, container.scenario.theRA.Pos2.Y);

        if options.bSettingImageOverlay then
        begin
            if self.state.imageOverlaySelectedCorner.HasValue then
                self.setRedrawBathymetry;
            self.state.imageOverlaySelectedCorner.null
        end
        else if assigned(selectedObject) then
        begin
            if selectedObject is TSource then
            begin
                if not TSource(selectedObject).isMovingSource then
                    updateSelectedSource(tmpX,tmpY,TSource(selectedObject));
            end
            else if selectedObject is TProbe then
                updateSelectedProbe(tmpX,tmpY,TProbe(selectedObject));
        end;
    end;

    state.mouse.button.null();
    drawnow();
end;

procedure TGLForm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
const
    mouseWheelPanSensitivity: double = 0.15; //a fudge factor to get it working approximately
var
    sign: double;
    windowPoint: TPoint;
begin
    glZoom := glZoom + wheeldelta*mouseWheelZoomSensitivity;

    {panning towards the current mousepos - need to convert mousepos into current window location. it is currently screen location.
    compare the window location to the centre of the window.}
    sign := ifthen(wheelDelta > 0,-1,1);
    windowPoint := ScreenToClient(MousePos);
    glPan.x := glPan.x + sign*(windowPoint.X - ClientWidth/2 )/ClientWidth * mouseWheelPanSensitivity;
    glPan.y := glPan.y - sign*(windowPoint.Y - clientHeight/2)/clientHeight* mouseWheelPanSensitivity ;

    drawnow();
end;

{$ENDREGION}


procedure TGLForm.checkMouseLocationObjects3D(const mX, mY: integer; const isMouseDown: boolean);
    procedure sourceIsUnderCursorFunc(const src: TSource);
    var
        s: string;
    begin
        selectedObject := src; //set movingReturn data
        framSource.cbSource.ItemIndex := container.scenario.sources.IndexOf(src); //select this source on the framEditsource page

        src.setAltColor(true); //set this source to alternate colour
        self.setRedraw;

        //set TSource GL text
        s := src.Name;
        if src.SrcSpectrumSet then
            s := s + ' ' + container.scenario.formatLevelForSource(src);
        setSourceProbeText(s,src.Pos.X,src.Pos.Y);
        Cursor := crSize; //change cursor
    end;

    procedure sourceMovingPosIsUnderCursorFunc(const src: TSource; const movingPos: TSrcPoint);
    begin
        selectedObject := movingPos;
        framSource.cbSource.ItemIndex := container.scenario.sources.IndexOf(src);

        src.setAltColor(true); //set this source to alternate colour
        self.setRedraw;
        setSourceProbeText(src.name + ' pos ' + inttostr(src.movingPos.IndexOf(movingPos) + 1),movingPos.X,movingPos.Y); //set onscreen text
        Cursor := crSize; //change cursor
    end;

    procedure probeIsUnderCursorFunc(const prb: TProbe);
    begin
        selectedObject := prb; //set movingReturn data
        framProbe.cbProbe.ItemIndex := container.scenario.theRA.probes.IndexOf(prb); //select this probe on the framProbe page

        if prb.setAltColor(true) then //set this probe alternate color
            self.setRedraw();

        setSourceProbeText(prb.Name,prb.Pos.X,prb.Pos.Y); //set TProbe GL text
        Cursor := crSize; //set cursor
    end;

    procedure CSEPIsUnderCursorFunc(const csep: TCrossSecEndpoint);
    begin
        //csep under cursor
        selectedObject := csep;
        if csep.setColor(csep.altcolor) then
            self.setRedraw();
        Cursor := crSize;
    end;

    procedure ImageOverlayCornerFunc(const corner: TRectControl);
    begin
        self.state.imageOverlaySelectedCorner := corner;
        cursor := crSize;
    end;

    function thresholdDistanceTest(const mouseGL, pos: TFPoint; const threshold: double): boolean;
    var
        qx,qy: double;
    begin
        qx := container.geoSetup.w2g(pos.X);
        qy := container.geoSetup.w2g(pos.Y);
        result := ((abs(mouseGL.x - qx)) < threshold) and ((abs(mouseGL.y - qy)) < threshold);
    end;
var
    src: TSource;
    prb: TProbe;
    mouseGL: TFPoint;
    movingPos: TSrcPoint;
    csep: TCrossSecEndpoint;
    corner: TRectControl;
begin
    {check the current mouse location (if we are looking directly down) to see if
    any selectable objects are beneath it. if so, 'select' them via TMovingGraphicsObjectReturn}

    selectedObject := nil;
    { only do this checking for objects under the mouse location if we are looking directly down}
    if not isBirdsEyeView then exit;

    {mouse position in gl coords. also takes account of panning and zooming}
    mouseGL.x := (((mX/ClientWidth*2) - 1) - glPan.x)/(1 + mouseZoomSensitivity*glZoom);
    mouseGL.y := ((((clientHeight-mY)/clientHeight*2) - 1) - glPan.y)/(1 + mouseZoomSensitivity*glZoom);

    if not (options.bSettingWorldScale or options.bSettingRuler) then
    begin
        if options.bSettingImageOverlay then
            for corner := low(TRectControl) to high(TRectControl) do
                if thresholdDistanceTest(mouseGL, container.image.cornerPos(corner) ,sourceSelectThreshold) then
                    ImageOverlayCornerFunc(corner);

        if UWAOpts.ComponentVisibility[ord(veEndpoints)] then
            for csep in container.crossSec.endpoints do
                if thresholdDistanceTest(mouseGL,csep.pos.asFPoint,sourceSelectThreshold) then
                begin
                    CSEPIsUnderCursorFunc(csep);
                    exit;
                end
                else
                    if csep.setColor(csep.origcolor) then
                        self.setRedraw();

        if UWAOpts.ComponentVisibility[ord(veProbes)] and ((not isMouseDown) or UWAOpts.canSelectProbes) then
            for prb in container.scenario.theRA.probes do
                if container.scenario.theRA.enabled and prb.visible then
                begin
                    {check if the mouse is over the probe}
                    if thresholdDistanceTest(mouseGL,prb.Pos.asFPoint,probeSelectThreshold) then
                    begin
                        probeIsUnderCursorFunc(prb);
                        exit;
                    end
                    else
                        if prb.setAltColor(false) then {set probe orig color}
                            self.setRedraw();
                end; //container.scenario.theRA.enabled and prb.visible

        if UWAOpts.ComponentVisibility[ord(veSources)] and ((not isMouseDown) or UWAOpts.canSelectSources) then
        begin
            for src in container.scenario.sources do
                if not src.isMovingSource then
                begin
                    if thresholdDistanceTest(mouseGL,src.Pos.asFPoint,sourceSelectThreshold) then
                    begin
                        sourceIsUnderCursorFunc(src);
                        exit;
                    end
                    else
                        src.setAltColor(false); {set source back to normal color}
                end;

            for src in container.scenario.sources do
                if src.isMovingSource then
                    for movingPos in src.movingPos do
                        if thresholdDistanceTest(mouseGL,movingPos.asFPoint,sourceSelectThreshold) then
                        begin
                            sourceMovingPosIsUnderCursorFunc(src,movingPos);
                            exit;
                        end
                        else
                            src.setAltColor(false); {set source back to normal color}
        end;
    end; //not (bSettingWorldScale or bSettingRuler)

    {no objects selected, put cursor back to normal}
    if not assigned(selectedObject) then
        cursor := crDefault;
end;

procedure TGLForm.checkMouseLocationObjects2D(const mX, mY: integer);
var
    csep: TCrossSecEndpoint;
    mgX, mgY: double;
begin
    {check the current mouse location (if we are looking directly down) to see if
    any selectable objects are beneath it. if so, 'select' them via TMovingGraphicsObjectReturn}
    selectedObject := nil;

    {mouse position in gl coords. also takes account of panning and zooming}
    mgX := (mX/ClientWidth*2) - 1;
    mgY := ((clientHeight - mY)/clientHeight*2) - 1;

    for csep in container.crossSec.endpoints do
    begin
        if (abs(mgX - XSecGuideX(container.geoSetup.w2g(csep.pos.X))) < xsecGuideSelectThreshold) and
           (abs(mgY - XSecGuideY(container.geoSetup.w2g(csep.pos.Y))) < xsecGuideSelectThreshold) then
        begin
            selectedObject := csep;
            Cursor := crSize;
            exit;
        end;
    end;

    {no objects selected, put cursor back to normal}
    if not assigned(selectedObject) then
        cursor := crDefault;
end;

function  TGLForm.isBirdsEyeView;
begin
    result := (glRot.x = 0) and (glRot.y = 0) and not self.bDrawCrossSection;
end;

function  TGLForm.ViewCoordsString(const X,Y :integer): String;
Const
    GB = 1073741824;
    MB = 1048576;
var
    P: TPoint;
    Pf: TFPoint;
    tmp: TEastNorth;
    mwX, mwY, lev, localDepth: double;
    slocalDepth, sXandY, sLatLng, sLev, sMem: string;
    iMeM: int64;
begin
    result := '';
    if not assigned(container) then exit;
    if not assigned(container.scenario.theRA) then exit;

    {use screen X and Y coords to get world x and y}
    P.X := X;
    P.Y := Y;
    try
        Pf := screenToWorldFloat(P);
    except
        result := '';
    end;
    mwX := Pf.X;
    mwY := Pf.Y;

    {find the local depth under the mouse and make a string for it. only display when the view is birdseye}
    //aPos := ;
    if not (checkrange(mwX,0,container.scenario.theRA.xMax) and checkrange(mwY,0,container.scenario.theRA.yMax)) then
        localDepth := NaN
    else
        localDepth := container.getBathymetryAtPos(mwX,mwY);

    if isnan(localDepth) then
        slocalDepth := ''
    else
    if not isBirdsEyeView then
        slocalDepth := ''
    else
    begin
        if localDepth >= 0 then
            slocalDepth := '   Depth under cursor: ' + container.geoSetup.makeZString(localDepth) + ' ' +
                                    ifthen(UWAOpts.DispFeet,'ft','m')
        else
            slocalDepth := '   Height under cursor: ' + container.geoSetup.makeZString(-localDepth) + ' ' +
                                    ifthen(UWAOpts.DispFeet,'ft','m');
    end;

    sLev := '';
    if isBirdsEyeView and container.scenario.resultsExist() then
    begin
        lev := container.scenario.theRA.getLevAtXY(mwX, mwY,
                                                   UWAOpts.LevelsDisplay,
                                                   UWAOPts.DisplayZInd,true);
        if (not isnan(lev)) and (lev > lowLev) then
        begin
            lev := container.scenario.convertLevel(lev);
            sLev := '  Level at cursor: ' + tostr(round1(lev)) + ' dB' + TLevelConverter.levelTypeName(container.scenario.levelType);
        end;
    end;

    iMem := TotalMemoryUsed;// prob.estimateMemUsage;// CurrentMemoryUsage;
    if iMem < MB then
        sMem := '  ' + tostr(roundx(iMem/MB,0.01)) + ' MB'
    else
    if iMem < GB then
        sMem := '  ' + tostr(saferound(iMem/MB)) + ' MB'
    else
        sMem := '  ' + tostr(roundx(iMem/GB,0.01)) + ' GB';

    mwX := mwX + container.geoSetup.Easting;
    mwY := mwY + container.geoSetup.Northing;

    if isBirdsEyeView and importedLocation.hasUTMData then
    begin
        tmp.latitude := mwY;
        tmp.longitude := mwX;
        tmp := Tlatlong2UTM.UTM2LatLong(importedLocation.utmLongZone,importedLocation.utmLatZone,tmp);
        slatLng := floattostrf(abs(tmp.latitude), ffFixed, 14, 8)
            + ifthen(tmp.latitude >= 0, ' N', ' S')
            + '  ' + floattostrf(abs(tmp.longitude), ffFixed, 14, 8)
            + ifthen(tmp.longitude >= 0, ' E', ' W') + '   ';
    end
    else
        slatLng := '';

    {convert to feet if necessary}
    if UWAOpts.dispFeet then
    begin
        mwX := saferound(m2f(mwX));
        mwY := saferound(m2f(mwY));
    end;

    if bDrawCrossSection then
        result := ''
    else
    if state.mouse.button.HasValue and assigned(selectedObject) then
        result := 'New X --> ' + tostr(round1(mwX)) + '  New Y --> ' + tostr(round1(mwY)) + slocalDepth
    else
    begin
        //result := 'mX'+tostr(X)+' mY'+tostr(Y)+' wX'+tostr(mwX)+' wY'+tostr(mwY)+' pX'+tostr(glPans[0])+' pY'+tostr(glPans[1])+' rX'+tostr(glSpins[0])+' rY'+tostr(glSpins[1])+ ' zoom'+tostr(glZoom);
        sXandY := ifthen(isBirdsEyeView,
            'x ' + tostr(saferound(mwX))
            + ' y ' + tostr(saferound(mwY)) + '   ' ,'');
        result := sXandY
            + slatLng
            //+ 'pan x '  + tostr(roundx(glPan.x,0.01)) + ' pan y ' + tostr(roundx(glPan.y,0.01))
            //+ ' rot x ' + tostr(round1(glRot.x)) + ' rot y ' + tostr(round1(glRot.y))
            //+ ' zoom '  + tostr(round1(glZoom))
            + slocalDepth + sLev + sMem;
    end;
end;

{$REGION 'conversions'}
function    TGLForm.WorldToScreenFloat(const afP : TfPoint): TPoint;
var
    GLPos: TFPoint;
begin
    GLPos.x := container.geoSetup.w2g(afP.X);
    GLPos.y := container.geoSetup.w2g(afP.Y);
    result.X := saferound(((GLPos.x * (1 + mouseZoomSensitivity*glZoom)) + 1 + glPan.x)/2*ClientWidth);
    result.Y := saferound(clientHeight - (((GLPos.y * (1 + mouseZoomSensitivity*glZoom)) + 1 + glPan.y)/2*clientHeight));
end;

function    TGLForm.screenToWorldFloat(const aP: TPoint): TFPoint;
var
    GLPos: TFPoint;
begin
    GLPos.x := ((aP.X/ClientWidth*2) - 1 - glPan.x) / (1 + mouseZoomSensitivity*glZoom);
    GLPos.y := (((clientHeight - aP.Y)/clientHeight*2) - 1 - glPan.y) / (1 + mouseZoomSensitivity*glZoom);
    result.X := container.geoSetup.g2w(GLPos.x); //mouse location converted to world coords. NB only works when view is straight down
    result.Y := container.geoSetup.g2w(GLPos.y);
end;

//    function    TGLWin.screenToWorld(const aP : TPoint): TPoint;
//    var
//        fP: TFPoint;
//    begin
//        fP := screenToWorldFloat(aP);
//        result.X := saferound(fP.X); //mouse location converted to world coords. NB only works when view is straight down
//        result.Y := saferound(fP.Y);
//    end;

function  TGLForm.XSecCoord(const mn, mx, p: double): double;
begin
    result := mn + (mx - mn)*p;
end;

{$ENDREGION}

procedure TGLForm.forceDrawNow;
begin
    state.bDrawNowNeeded := true;
    drawNow();
end;

procedure TGLForm.force2DrawNow;
begin
    //we don't want to hide then show the progressform immediately
    state.canHideProgressForm := false;
    try
        forceDrawNow();
    finally
        state.canHideProgressForm := true;
    end;
    forceDrawNow();
end;

{$REGION 'drawing'}
procedure TGLForm.drawMousePos(const x,y: integer; const dx: double = nan; const dy: double = nan);
const
    yLineSpacing: double = -0.033;
    textZ: double = 1.0;
var
    GLPos, worldPos: TFPoint;
    text: string;
begin
    {its worth keeping mgX mgY here, as we need them to know where to plot the text}
    GLPos.x := ((X/ClientWidth*2) - 1 - glPan.x)/(1 + mouseZoomSensitivity*glZoom);
    GLPos.y := (((clientHeight-Y)/clientHeight*2) - 1 - glPan.y)/(1 + mouseZoomSensitivity*glZoom);
    worldPos.x := saferound(container.geoSetup.g2w(GLPos.x) + container.geoSetup.Easting); //mouse location converted to world coords. NB only works when view is straight down
    worldPos.y := saferound(container.geoSetup.g2w(GLPos.y) + container.geoSetup.Northing);
    {convert to feet if necessary}
    if UWAOpts.dispFeet then
    begin
        worldPos.x := saferound(m2f(worldPos.x,UWAOpts.dispFeet));
        worldPos.y := saferound(m2f(worldPos.y,UWAOpts.dispFeet));
    end;
    text := 'x ' + tostr(worldPos.x) + '   y ' + tostr(worldPos.y);
    glPrint(text,
            FontLarge,
            GLPos.x + dGLTextXOffset,
            GLPos.y + dGLTextYOffset + yLineSpacing,
            textZ,
            UWAOpts.GLTextColor);

    if (not isnan(dx)) and (not isnan(dy)) then
        glPrint('dx ' + tostr(saferound(m2f(dx,UWAOpts.dispFeet))) + '   dy ' + tostr(saferound(m2f(dy,UWAOpts.dispFeet))),
                FontLarge,
                GLPos.x + dGLTextXOffset,
                GLPos.y + dGLTextYOffset + 2*yLineSpacing,
                textZ,
                UWAOpts.GLTextColor);
end;

procedure TGLForm.drawSourceProbeText;
begin
    glPrint(sSourceProbeText, FontLarge, dSourceProbeTextGX, dSourceProbeTextGY, dSourceProbeTextGZ, UWAOpts.GLTextColor);
    THelper.asyncWait(SourceProbeTextTimeout, procedure begin
        self.clearSourceProbeText;
        ForcedrawNow;
    end);
end;

procedure TGLForm.drawSourceThreshold;
const
    thresholdGLZPos = 0.001;
var
    i: integer;
    cosA, sinA: double;
    sliceMaxDist: TDoubleDynArray;
    src: TSource;
begin
    src := framSource.sourceForThreshold();
    if not assigned(src) then exit;

    if length(src.maxDistsToThresholdForSlices) > 0 then
    begin
        GLLineWidth(3.0);
        GLColor4f(1.0,1.0,1.0,0.5);

        sliceMaxDist := src.maxDistsToThresholdForSlices.copy;
        TMCKLib.arrayLoopSmoothNaNs(sliceMaxDist);

        GLBegin(GL_LINE_LOOP);
        for i := 0 to length(sliceMaxDist) - 1 do
        begin
            if (not isnan(sliceMaxDist[i])) then
            begin
                system.SineCosine(2.0 * PI * i / length(sliceMaxDist), sinA, cosA);
                glVertex3f(container.geoSetup.w2g(src.pos.x + sliceMaxDist[i]*cosA),
                           container.geoSetup.w2g(src.pos.y + sliceMaxDist[i]*sinA),
                           thresholdGLZPos);
            end;
        end;
        GLEnd;
    end;
end;

procedure TGLForm.drawNow();
    procedure drawBackground();
    begin
        {currently only flat background colour is used.}
        glClearColor(GetRValue(UWAOpts.backgroundColor)/255,
                     GetGValue(UWAOpts.backgroundcolor)/255,
                     GetBValue(UWAOpts.backgroundcolor)/255,
                     1.0);   // Background
        //    {draw a graduated colored background}
        //    glBegin(GL_QUADS);
        //        glColor4f(GetRValue(backgroundTopcolor)/255,GetGValue(backgroundTopcolor)/255,GetBValue(backgroundTopcolor)/255,1.0);
        //        glVertex3f(+1.0,+1.0,-1.5);
        //        glVertex3f(-1.0,+1.0,-1.5);
        //        glColor4f(GetRValue(backgroundBottomcolor)/255,GetGValue(backgroundBottomcolor)/255,GetBValue(backgroundBottomcolor)/255,1.0);
        //        glVertex3f(-1.0,-1.0,-1.5);
        //        glVertex3f(+1.0,-1.0,-1.5);
        //    glEnd();
    end;
var
    bLocalRedraw: boolean;
    light: array of GLFloat;
    m: TGLMatrixf4;
begin
    {this is the function that is called from all other points in code, when
    we need to (possibly) redraw the GL Window. note, we only do a draw if
    bDrawNowNeeded := true;, and then set that back to false. So everywhere else in
    the code, where we change soemting visible, we need to set that to true. maybe
    a better idea would be to have a temp list of all drawables, and check whether anything
    has changed since the last draw.}

    if GLNoDraw then Exit;

    if not assigned(container) then Exit;

    GLNoDraw := true; //to prevent any concurrent calls to drawnNow
    inputDisabled := true;
    glDrawBeginTime := timegettime;
    try
        {compare current spin and pan and zoom with previous, to check if we need to redraw}
        bLocalRedraw := (glRot.x <> lastGLSpins.x) or
                        (glRot.y <> lastGLSpins.y) or
                        (glPan.x <> lastGLPan.x) or
                        (glPan.y <> lastGLPan.y) or
                        (glZoom <> lastGLZoom);

        if (state.bDrawNowNeeded or bLocalRedraw or options.bSettingWorldScale or options.bSettingRuler) then
        begin
            glRenderTextureZfix := 0;
            glClearDepth(1.0);                       // clear depth buffer to this value
            glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The color  and depth buffers
            glLoadIdentity();                            // Reset The View
            glBindTexture(GL_TEXTURE_2D, 0); //no texture activated
            glBindFramebufferEXT(GL_FRAMEBUFFER, 0);// no FBO activated

            glShadeModel(GL_SMOOTH);             // Enables Smooth Color Shading

            {hints for antialiasing. can be GL_NICEST, GL_FASTEST, GL_DONT_CARE }
            glHint(GL_POINT_SMOOTH_HINT,GL_FASTEST);
            glHint(GL_LINE_SMOOTH_HINT,GL_FASTEST);
            glHint(GL_POLYGON_SMOOTH_HINT,GL_FASTEST);
            glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

            //glutInitDisplayMode (GLUT_DOUBLE);  //double buffering
            glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
            glDepthFunc(GL_LESS);             // The Type Of Depth Test To Do
            glEnable (GL_BLEND);                         //enabling alpha
            glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //enabling alpha
            //glEnable(GL_ALPHA_TEST);

            drawBackground();

            glOrtho(-1.0, 1.0, -1.0, 1.0, -3.0, 3.0);

            {only do zoom, pan etc if we are not drawing a cross section}
            if not bDrawCrossSection then
            begin


                //    glEnable(GL_LIGHT0);
                //    light := [0.3,0,0,0.3];
                //    glLightfv(GL_LIGHT0, GL_AMBIENT, @(light[0]));
                //    light := [1,0,1,1];
                //    glLightfv(GL_LIGHT0, GL_DIFFUSE, @(light[0]));
                //    glLightfv(GL_LIGHT0, GL_SPECULAR, @(light[0]));
                //    light := [1,0,-2,1]; //last parameter: 0 = direction, 1 = position
                //    glLightfv(GL_LIGHT0, GL_POSITION, @(light[0]));

                glEnable(GL_LIGHT1);
                light := [0.3,0.3,0.3,0.3];
                glLightfv(GL_LIGHT1, GL_AMBIENT, @(light[0]));
                light := [1,1,1,0.7];
                glLightfv(GL_LIGHT1, GL_DIFFUSE, @(light[0]));
                glLightfv(GL_LIGHT1, GL_SPECULAR, @(light[0]));
                light := [-1,1,-2,1]; //last parameter: 0 = direction, 1 = position
                glLightfv(GL_LIGHT1, GL_POSITION, @(light[0]));

                {panZ,spinZ are always 0 }
                {panning }
                glTranslatef(glPan.x,glPan.y,0.0);        //pan X and Y

                {rotation}
                m := TQuaternionFuncs.toGLMatrixf(glQuaternion);
                glMultMatrixf(@m);
                {fmod makes sure spin angles are in range 0 - 360}
                //glRotatef(fmod(glRot.x,360),0.0,1.0,0.0);      // Rotate X
                //glRotatef(fmod(glRot.y,360),1.0,0.0,0.0);      // Rotate Y

                {zoom}
                {if zoom < -1/mouseSensitivity, the image is inverted and gets larger. set a lower limit to zoom out}
                if glZoom < glMinZoom then
                    glZoom := glMinZoom;
                glScalef(1 + mouseZoomSensitivity*glZoom,
                        1 + mouseZoomSensitivity*glZoom,
                        1 + mouseZoomSensitivity*glZoom);
            end;

            {branch to XSec or normal draw}
            if options.bSettingWorldScale or options.bSettingRuler then
                drawSetWorldScale()
            else
            if bDrawCrossSection then
                draw2D()
            else
                draw3D(UWAOpts.ComponentVisibility);
            state.bLastGLWasXSec := bDrawCrossSection;    //needed so we know if we need to redo the results colormap

            glFinish();
            //glFlush; //single buffering
            SwapBuffers(glDC); //double buffering

            updateLastGLValues();
        end;

        Mainform.sbStatusbar.SimpleText := ViewCoordsString(state.mouse.X,state.mouse.Y); //updates info on current mouse position
        {$IFDEF DEBUG}
            {a little marker to show if we just redrew}
            if state.bDrawNowNeeded or bLocalRedraw then
                Mainform.sbStatusbar.SimpleText := Mainform.sbStatusbar.SimpleText + ' *';
        {$ENDIF}
    finally
        if state.canHideProgressForm then
        begin
            glDrawProgressForm.Hide();
            glProgressLastHide := timegettime;
        end;
        inputDisabled := false;
        GLNoDraw := false;
    end;
end;



procedure TGLForm.draw3D(const componentVisibility: TBooleanDynArray);
//var
//    light: array of GLFloat;
begin
    {it seems like things can't be drawn with alpha unless there
    is something already drawn with alpha = 1.0, to blend with}

    if not (length(componentVisibility) - 1) = integer(high(TVisibilityEnum)) then
    begin
        raise Exception.Create(rsIncorrectLengthOfComponentVisibilityArray);
        Exit();
    end;

//    glEnable(GL_LIGHTING);

//    glEnable(GL_LIGHT0);
//    light := [0.3,0,0,0.3];
//    glLightfv(GL_LIGHT0, GL_AMBIENT, @(light[0]));
//    light := [1,0,1,1];
//    glLightfv(GL_LIGHT0, GL_DIFFUSE, @(light[0]));
//    glLightfv(GL_LIGHT0, GL_SPECULAR, @(light[0]));
//    light := [1,0,-2,1]; //last parameter: 0 = direction, 1 = position
//    glLightfv(GL_LIGHT0, GL_POSITION, @(light[0]));

//    glEnable(GL_LIGHT1);
//    light := [0.3,0.3,0.3,0.3];
//    glLightfv(GL_LIGHT1, GL_AMBIENT, @(light[0]));
//    light := [1,1,1,0.7];
//    glLightfv(GL_LIGHT1, GL_DIFFUSE, @(light[0]));
//    glLightfv(GL_LIGHT1, GL_SPECULAR, @(light[0]));
//    light := [-1,1,-2,1]; //last parameter: 0 = direction, 1 = position
//    glLightfv(GL_LIGHT1, GL_POSITION, @(light[0]));

//    glColorMaterial( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
//    glEnable ( GL_COLOR_MATERIAL ) ;

    if componentVisibility[Ord(veAxes)] then
        draw3DAxes();

    if showFlock then
        draw3DFlock();

    {bathymetry - using a display list to speed things up after
    it is drawn for the first time.}
    if componentVisibility[ord(veBathymetry)] then
        draw3DBathymetry();

    checkDrawTime();

    if componentVisibility[ord(veSources)] then
        draw3DSources();

    if componentVisibility[ord(veProbes)] then
        draw3DProbes();

//    {ReceiverAreas}
//    if componentVisibility[ord(veRAs)] then
//        drawReceiverAreas(container.scenario.theRA);

    draw3DResults(componentVisibility);
    if componentVisibility[ord(veRays)] then
        draw3dRays;

    {if there are no results showing, it can look nice to have a mostly transparent water surface.
    it does cause some problems though, particularly if there is another surface trying to show
    at the exact same Z or close to it}
    if componentVisibility[ord(veWater)] and
        not ((componentVisibility[ord(veResults)] or componentVisibility[ord(veContours)]) and
        container.scenario.resultsExist()) then //don't draw water surface if we drew results
        draw3DWater();

    if isBirdsEyeView() and componentVisibility[ord(veEndpoints)] then
        draw3DCrossSecGuide(container.crossSec);

    {mouse position text}
    if isBirdsEyeView() and state.mouse.button.HasValue and assigned(selectedObject) then
        drawMousePos(state.mouse.X,state.mouse.Y);

    if isBirdsEyeView() then
        draw3DBorderText();

    if componentVisibility[ord(veScaleBar)] then
        draw3DScaleBar;

    {source or probe name text}
    if options.showSourceProbeText then
    begin
        if (timeGetTime - lwSourceProbeTextSetTime) < SourceProbeTextTimeout then
            drawSourceProbeText()
        else
            clearSourceProbeText();
    end;

    {source threshold}
    if showSourceThreshold then
        drawSourceThreshold();

    if componentVisibility[ord(veGrid)] then
        draw3DGrid;

    draw3dOverlays;
    draw3dSettingImageOverlay;

//    glDisable ( GL_COLOR_MATERIAL ) ;
//    glDisable(GL_LIGHTING);
end;

procedure TGLForm.setGLColorAndVertex(const col: tglColor; const point: TFPoint3);
begin
    glColor4f(col.r,col.g,col.b, col.alpha);
    glVertex3f(point.x,point.y,point.z);
end;

procedure TGLForm.drawglRectTriangle1(const rect: tglRect);
begin
    glBegin(GL_TRIANGLES);
    setGLColorAndVertex(rect.ulColor,rect.ul);
    setGLColorAndVertex(rect.urColor,rect.ur);
    setGLColorAndVertex(rect.llColor,rect.ll);
    glEnd();
end;

procedure TGLForm.drawglRectTriangle2(const rect: tglRect);
begin
    glBegin(GL_TRIANGLES);
    setGLColorAndVertex(rect.urColor,rect.ur);
    setGLColorAndVertex(rect.llColor,rect.ll);
    setGLColorAndVertex(rect.lrColor,rect.lr);
    glEnd();
end;

procedure TGLForm.setGLTextureAndVertex(const point: TFPoint3; const tex: TFPoint; const NanColor: glColor; const alpha: double);
begin
    if (tex.x < 0) or (tex.x > 1) or (tex.y < 0) or (tex.y > 1) then
        glColor4f(nancolor[0],nancolor[1],nancolor[2], alpha)
    else
        glTexCoord2f(tex.x, tex.y);
    glVertex3f(point.x,point.y,point.z);
end;

procedure TGLForm.drawglRectTriangle1Texture(const rect: tglRect; const NanColor: glColor; const alpha: double);
begin
    glBegin(GL_TRIANGLES);
    setGLTextureAndVertex(rect.ul, rect.textureUl, nanColor, alpha);
    setGLTextureAndVertex(rect.ur, rect.textureUr, nanColor, alpha);
    setGLTextureAndVertex(rect.ll, rect.textureLl, nanColor, alpha);
    glEnd();
end;

procedure TGLForm.drawglRectTriangle2Texture(const rect: tglRect; const NanColor: glColor; const alpha: double);
begin
    glBegin(GL_TRIANGLES);
    setGLTextureAndVertex(rect.ur, rect.textureUr, nanColor, alpha);
    setGLTextureAndVertex(rect.ll, rect.texturell, nanColor, alpha);
    setGLTextureAndVertex(rect.lr, rect.texturelr, nanColor, alpha);
    glEnd();
end;

procedure TGLForm.bathyColorsBMP(const bmp: TBitmap);

    function getColor(const unRoundedIndex: double; const colorMap: TMatrix): TColor;
    var
        ind: integer;
    begin
        ind := safetrunc(unRoundedIndex);
        if (ind < 0) then ind := 0;
        result := rgb(byte(round(colorMap[ind,0]*255)),
            byte(round(colorMap[ind,1]*255)),
            byte(round(colorMap[ind,2]*255)));
    end;

var
    i,j,li,lj,cwi,maxCmapWater: integer;
    cmapWaterK,landColormapIndexScaling: double; //used for pos in colormap calc
    cmapWater,cmapLand: TMatrix; //colormap
    minGDepth,maxGDepth: double;
    bMatMinIsNegative: boolean;
    gDepths: TSingleMatrix;
begin
    {NB j,y is the first dimension in the array}
    gDepths := container.bathymetry.gDepths();
    lj := length(gDepths);      //getting the size of 2d array
    li := length(gDepths[0]);
    bmp.PixelFormat := pf24bit;
    bmp.SetSize(li,lj);

    {varying colormap for water and land (based on depths)}
    cmapWater := UWAOpts.bathymetrycolorMap;
    cmapLand := UWAOpts.LandColorMap;

    maxGDepth := gDepths.max;
    minGDepth := gDepths.min;

    maxCmapWater := high(cmapWater);  {max index}
    cmapWaterK := maxCmapWater / maxGDepth;  {inverted so we can use multiply not divide later on in the loop}
    if isInfinite(cmapWaterK) then
        cmapWaterK := 1;

    landColormapIndexScaling := maxCmapWater/minGDepth;
    {find out if the minimum value is actually properly negative, ie there are heights encoded as well as depths.
    firstly, the matrix min should be less than 0, and also the min should be at least a certain magnitude -
    this was failing previously due to some values that should have been 0 but were very slightly negative eg -0.002.
    Also, the negative values should not be all the same}
    bMatMinIsNegative := (minGDepth < 0) and
                         (-minGDepth/(maxGDepth - minGDepth) > 0.02) and
                         (not gDepths.allValuesEqual());

    {draw water}
    for i := 0 to li - 1 do
    begin
        for j := 0 to lj - 1 do
        begin
            if isnan(gDepths[j,i]) then
                bmp.Canvas.Pixels[i,j] := UWAOpts.NaNColor //nans
            else
            begin
                cwi := safetrunc(cmapWaterK*gDepths[j,i]);
                if cwi >= 0 then
                    //normal bathymetry.
                    bmp.Canvas.Pixels[i,j] := rgb(byte(round(cmapWater[cwi,0]*255)),
                        byte(round(cmapWater[cwi,1]*255)),
                        byte(round(cmapWater[cwi,2]*255)))
                else if bMatMinIsNegative then
                    //land
                    bmp.Canvas.Pixels[i,j] := getColor(landColormapIndexScaling*gDepths[j,i],cmapLand)
                else
                    //land when we can't find a color
                    bmp.Canvas.Pixels[i,j] := UWAOpts.LandColor;
            end;
        end;
    end;
end;

procedure TGLForm.draw3DBathymetry;
{draw the water and land. this version draws both as a series of triangle strips, in 3d.
the colours are also generated based on the depth.
we are now drawing water and land separately, so that we don't have blurred edges (or odd shaped triangles)
along the coasts. also, 'water' gets drawn underneath the land, so that there is
no gap along the coasts.
note that this is all within a gl display list so runs relatively fast}

    function getWaterUnderLandColor(const gDepths: TSingleMatrix; const cmapWater: TMatrix; const maxGDepth, cmapWaterK: double): glColor;
    var
        i,j, ind: integer;
        minNonLandDepth: double;
    begin
        {color for water that will fill in underneath land. getting the colour that is also used for
        the minimum water depth - which is not necessarily the first index into the colormap. Note that
        the blue transarent water layer is making this level look lighter}
        minNonLandDepth := NaN;
        for i := 0 to length(gDepths) - 1 do
            for j := 0 to length(gDepths[0]) - 1 do
                if (not isnan(gDepths[i,j])) and (gDepths[i,j] > 0) then
                    lesser(minNonLandDepth,gDepths[i,j]);

        ind := safetrunc(cmapWaterK * minNonLandDepth / maxGDepth);
        result[0] := cmapWater[ind,0];
        result[1] := cmapWater[ind,1];
        result[2] := cmapWater[ind,2];
    end;

    procedure drawEdgesBetweenLandAndWater(const gDepths: TSingleMatrix; const alpha: double; const xs,ys: TDoubleDynArray);
    var
        mapEdges: array[0..2] of byte;
        iEdges: array [0..1] of integer;
        i,j,li,lj: integer;
        rect: tglRect;
    begin
        //for some reason, GetRValue etc is doing strange things when it is returning to a double. So here mapEdges is an array of bytes
        mapEdges[0] := getRValue(TDBSeaColours.LandMapEdges);
        mapEdges[1] := getGValue(TDBSeaColours.LandMapEdges);
        mapEdges[2] := getBValue(TDBSeaColours.LandMapEdges);

        //glColor4f(mapEdges[0]/255,mapEdges[1]/255,mapEdges[2]/255, alpha);
        rect.setAllPointsColor(mapEdges[0]/255,mapEdges[1]/255,mapEdges[2]/255, alpha);
        iEdges[0] := 0;

        lj := length(gDepths);
        li := length(gDepths[0]);

        //along i edges
        iEdges[1] := li - 1;
        for i in iEdges do
            for j := 0 to lj - 2 do
                if (gDepths[j  ,i  ] <= 0) and (gDepths[j+1,i  ] <= 0) then //both indices are above water level
                begin
                    rect.ul.setPos(xs[i],ys[j  ],UWAOpts.ZLandFactor*gDepths[j  ,i  ]);
                    rect.ur.setPos(xs[i],ys[j+1],UWAOpts.ZLandFactor*gDepths[j+1,i  ]);
                    rect.ll.setPos(xs[i],ys[j  ],0);
                    rect.lr.setPos(xs[i],ys[j+1],0);
                    drawglRectTriangle1(rect);
                    drawglRectTriangle2(rect);
                end;

        //along j edges
        iEdges[1] := lj - 1;
        for j in iEdges do
            for i := 0 to li - 2 do
                if (gDepths[j  ,i  ] <= 0) and (gDepths[j  ,i+1] <= 0) then //both indices are above water level
                begin
                    rect.ul.setPos(xs[i  ],ys[j],UWAOpts.ZLandFactor*gDepths[j  ,i  ]);
                    rect.ur.setPos(xs[i+1],ys[j],UWAOpts.ZLandFactor*gDepths[j  ,i+1]);
                    rect.ll.setPos(xs[i  ],ys[j],0);
                    rect.lr.setPos(xs[i+1],ys[j],0);
                    drawglRectTriangle1(rect);
                    drawglRectTriangle2(rect);
                end;
    end; //draw edges land/water

    procedure setRectVertexColor(const unRoundedIndex, alpha: double; const color: TglColor; const colorMap: TMatrix);
    var
        ind: integer;
    begin
        ind := safetrunc(unRoundedIndex);
        if (ind < 0) then ind := 0;
        color.setColor(colorMap[ind,0],colorMap[ind,1],colorMap[ind,2], alpha);
    end;

    function anyIsnan(const d1, d2, d3: double): boolean;
    begin
        result := isnan(d1) or isnan(d2) or isnan(d3);
    end;

    function anyIsnanOrAtLeast1Negative(const d1, d2, d3: double): boolean;
    begin
        result := anyIsnan(d1,d2,d3) or ((d1 <= 0) or (d2 <= 0) or (d3 <= 0));
    end;

    function notIsnanAndAtLeast1Negative(const d1, d2, d3: double): boolean;
    begin
        result := (not anyIsnan(d1,d2,d3))
            and ((d1 <= 0) or (d2 <= 0) or (d3 <= 0));
    end;
const
    defaultLandZ = -1.0;
    checkTimerInterval = 100;
var
    i,j,k,li,lj,biggerDimLength,cwi,maxCmapWater: integer;
    xs,ys, tex_xs, tex_ys: TDoubleDynArray;
    cmapWaterK,landColormapIndexScaling: double; //used for pos in colormap calc
    cmapWater,cmapLand: TMatrix; //colormap
    waterUnderLandColor,defaultLandColor,NaNColor: glColor;  //colours
    minGDepth,maxGDepth: double;
    bMatMinIsNegative: boolean;
    drawTriangle1: boolean;
    drawTriangle2: boolean;
    gDepths: TSingleMatrix;
    //invMaxGDepth: double;
    GLZCoordForDummyLand: double;
    rect: tglRect;
    alpha: double;
    useTexture: boolean;
begin
    if state.bBathymetryListReady then
        glCallList(theBathymetry)
    else
    begin
        Mainform.sbStatusbar.SimpleText := rsGeneratingBathymetryDisplay;
        application.ProcessMessages(); //must not be within a glcalllist

        glDeleteLists(theBathymetry,1);
        glNewList(theBathymetry, GL_COMPILE);

        {NB j,y is the first dimension in the array}
        gDepths := container.bathymetry.gDepths();
        lj := length(gDepths);      //getting the size of 2d array
        li := length(gDepths[0]);
        if li > lj then
            biggerDimLength := li
        else
            biggerDimLength := lj;

        alpha := UWAOpts.bathymetryAlpha;

        {varying colormap for water and land (based on depths)}
        cmapWater := UWAOpts.bathymetrycolorMap;
        cmapLand := UWAOpts.LandColorMap;

        maxGDepth := gDepths.max;
        //invMaxGDepth := 1 / maxGDepth;
        minGDepth := gDepths.min;

        maxCmapWater := high(cmapWater);  {max index}
        cmapWaterK := maxCmapWater / maxGDepth;  {inverted so we can use multiply not divide later on in the loop}
        if isInfinite(cmapWaterK) then
            cmapWaterK := 1;

        landColormapIndexScaling := maxCmapWater/minGDepth;
        {find out if the minimum value is actually properly negative, ie there are heights encoded as well as depths.
        firstly, the matrix min should be less than 0, and also the min should be at least a certain magnitude -
        this was failing previously due to some values that should have been 0 but were very slightly negative eg -0.002.
        Also, the negative values should not be all the same}
        bMatMinIsNegative := (minGDepth < 0) and
                             (-minGDepth/(maxGDepth - minGDepth) > 0.02) and
                             (not gDepths.allValuesEqual());
        glZCoordForDummyLand := UWAOpts.ZLandFactor*defaultLandZ/container.bathymetry.zMax;

        waterUnderLandColor := getWaterUnderLandColor(gDepths,cmapWater,maxGDepth,cmapWaterK);

        {color for land, if we are unable to use the landcolormap}
        defaultLandColor[0] := getRValue(UWAOpts.LandColor)/255;
        defaultLandColor[1] := getGValue(UWAOpts.LandColor)/255;
        defaultLandColor[2] := getBValue(UWAOpts.LandColor)/255;

        {color for NaN/NODATA}
        NaNColor[0] := getRValue(UWAOpts.NaNColor)/255;
        NaNColor[1] := getGValue(UWAOpts.NaNColor)/255;
        NaNColor[2] := getBValue(UWAOpts.NaNColor)/255;

        useTexture := glIsTexture(imageOverlayTexture) and not options.bSettingImageOverlay;
        setlength(xs,li);
        setlength(ys,lj);
        setlength(tex_xs,li);
        setlength(tex_ys,lj);
        for i := 0 to li - 1 do
        begin
            xs[i] := -1.0 + 2*(i           )/(biggerDimLength - 1);
            if useTexture then
                tex_xs[i] := container.image.textureS(container.geoSetup.g2w(xs[i]));
        end;
        for j := 0 to lj - 1 do
        begin
            ys[j] := -1.0 + 2*((lj - 1) - j)/(biggerDimLength - 1);    {note reversed indices on y axis}
            if useTexture then
                tex_ys[j] := container.image.textureT(container.geoSetup.g2w(ys[j]));
        end;

        checkDrawTime;

        {draw water}
        for i := 0 to li - 2 do
        begin
            if i mod checkTimerInterval = 0 then
               checkDrawTime();

            glBegin(GL_TRIANGLE_STRIP);
            for j := 0 to lj - 1 do
            begin
                for k := 0 to 1 do  //just going right to left as we go along the triangle strip
                begin
                    {* triangle strips are the fastest to draw. So we are going to split each square into 2 triangles to render.*}
                    if isnan(gDepths[j,i+k]) and not useTexture then
                    begin
                        //grey for nan areas
                        glColor4f(NaNcolor[0],NaNcolor[1],NaNcolor[2], alpha);
                        glVertex3f(xs[i+k],ys[j],0);
                    end
                    else
                    begin
                        cwi := safetrunc(cmapWaterK*gDepths[j,i+k]);
                        if cwi < 0 then
                        begin
                            {draw water going under the land, at z = 0, as this gets rid of the gaps along the coasts}
                            //glColor4f(getRValue(TDBSeaColours.LandMapEdges)/255,getGValue(TDBSeaColours.LandMapEdges)/255,getBValue(TDBSeaColours.LandMapEdges)/255, alpha);
                            glColor4f(waterUnderLandColor[0],waterUnderLandColor[1],waterUnderLandColor[2], alpha);
                            glVertex3f(xs[i+k],ys[j],0);
                        end
                        else
                        begin
                            {draw normal bathymetry. note that gdepths has already been divided by zclip}
                            glColor4f(cmapWater[cwi,0],cmapWater[cwi,1],cmapWater[cwi,2], alpha);
                            glVertex3f(xs[i+k],ys[j],UWAOpts.ZFactor*gDepths[j,i+k]); //gdepths already normalised
                        end; //ci
                    end; //isnan
                end; //k
            end;  //j
            glEnd;
        end;   //i

        {draw land}
        if useTexture then
        begin
            //loop through twice: once for the area within the texture, once for external areas

            glBindTexture(GL_TEXTURE_2D, imageOverlayTexture);
            glEnable(GL_TEXTURE_2D);
            glColor4d(1.0,1.0,1.0,1.0);

            //testing
//            glBegin(GL_QUADS);
//            glTexCoord2f(0,0);
//            glvertex3f(0,0,1);
//            glTexCoord2f(1,0);
//            glvertex3f(1,0,1);
//            glTexCoord2f(1,1);
//            glvertex3f(1,1,1);
//            glTexCoord2f(0,1);
//            glvertex3f(0,1,1);
//            glend();

            for i := 0 to li - 2 do
            begin
                if i mod checkTimerInterval = 0 then
                    checkDrawTime();

                rect.ul.x := xs[i  ];
                rect.ur.x := xs[i+1];
                rect.ll.x := xs[i  ];
                rect.lr.x := xs[i+1];

                rect.textureUl.x := tex_xs[i];
                rect.textureLl.x := tex_xs[i];
                rect.textureUr.x := tex_xs[i+1];
                rect.texturelr.x := tex_xs[i+1];

                if (rect.textureUl.x < 0) or (rect.textureUl.x > 1) or (rect.textureUr.x < 0) or (rect.textureUr.x > 1) then continue;

                for j := 0 to lj - 2 do
                begin
                    //also draw nan areas
                    drawTriangle1  := anyIsnanOrAtLeast1Negative(gDepths[j  ,i  ],gDepths[j  ,i+1],gDepths[j+1,i  ]);
                    drawTriangle2 := anyIsnanOrAtLeast1Negative(gDepths[j  ,i+1],gDepths[j+1,i  ],gDepths[j+1,i+1]);

                    if not (drawTriangle1 or drawTriangle2) then continue;

                    rect.textureUl.y := tex_ys[j];
                    rect.textureUr.y := tex_ys[j];
                    rect.textureLl.y := tex_ys[j+1];
                    rect.texturelr.y := tex_ys[j+1];

                    if (rect.textureUl.y < 0) or (rect.textureUl.y > 1) or (rect.textureLl.y < 0) or (rect.textureLl.y > 1) then continue;

                    rect.ul.y := ys[j  ];
                    rect.ur.y := ys[j  ];
                    rect.ll.y := ys[j+1];
                    rect.lr.y := ys[j+1];

                    {if bMatMinIsNegative, negative depths have not been clipped at 0, and we can happily plot heights}
                    if bMatMinIsNegative or anyIsnan(gDepths[j  ,i  ],gDepths[j  ,i+1],gDepths[j+1,i  ]) then
                    begin
                        rect.ul.z := ifthen(gDepths[j  ,i  ] > 0,0,UWAOpts.ZLandFactor*gDepths[j  ,i  ]);
                        rect.ur.z := ifthen(gDepths[j  ,i+1] > 0,0,UWAOpts.ZLandFactor*gDepths[j  ,i+1]);
                        rect.ll.z := ifthen(gDepths[j+1,i  ] > 0,0,UWAOpts.ZLandFactor*gDepths[j+1,i  ]);
                        rect.lr.z := ifthen(gDepths[j+1,i+1] > 0,0,UWAOpts.ZLandFactor*gDepths[j+1,i+1]);
                    end
                    else
                        rect.setAllPointsZ(glZCoordForDummyLand);

                    if drawTriangle1 then
                        self.drawglRectTriangle1Texture(rect,NaNcolor,alpha);
                    if drawTriangle2 then
                        self.drawglRectTriangle2Texture(rect,NaNcolor,alpha);
                end; //j
            end; //i

            glDisable(GL_TEXTURE_2D);
            glBindTexture( GL_TEXTURE_2D, 0 );

            for i := 0 to li - 2 do
            begin
                if i mod checkTimerInterval = 0 then
                    checkDrawTime();

                rect.ul.x := xs[i  ];
                rect.ur.x := xs[i+1];
                rect.ll.x := xs[i  ];
                rect.lr.x := xs[i+1];

                rect.textureUl.x := tex_xs[i];
                rect.textureLl.x := tex_xs[i];
                rect.textureUr.x := tex_xs[i+1];
                rect.texturelr.x := tex_xs[i+1];

                for j := 0 to lj - 2 do
                begin
                    //also draw nan areas
                    drawTriangle1  := anyIsnanOrAtLeast1Negative(gDepths[j  ,i  ],gDepths[j  ,i+1],gDepths[j+1,i  ]);
                    drawTriangle2 := anyIsnanOrAtLeast1Negative(gDepths[j  ,i+1],gDepths[j+1,i  ],gDepths[j+1,i+1]);

                    if not (drawTriangle1 or drawTriangle2) then continue;

                    rect.textureUl.y := tex_ys[j];
                    rect.textureUr.y := tex_ys[j];
                    rect.textureLl.y := tex_ys[j+1];
                    rect.texturelr.y := tex_ys[j+1];

                    if (rect.textureUl.x > 0) and (rect.textureUl.x < 1) and (rect.textureUr.x > 0) and (rect.textureUr.x < 1)
                        and (rect.textureUl.y > 0) and (rect.textureUl.y < 1) and (rect.textureLl.y > 0) and (rect.textureLl.y < 1) then continue;

                    rect.ul.y := ys[j  ];
                    rect.ur.y := ys[j  ];
                    rect.ll.y := ys[j+1];
                    rect.lr.y := ys[j+1];

                    {if bMatMinIsNegative, negative depths have not been clipped at 0, and we can happily plot heights}
                    if bMatMinIsNegative or anyIsnan(gDepths[j  ,i  ],gDepths[j  ,i+1],gDepths[j+1,i  ]) then
                    begin
                        {draw land at the actual negative depth (note, using ZLandFactor instead of ZFactor). get land color}
                        rect.ul.z := ifthen(gDepths[j  ,i  ] > 0,0,UWAOpts.ZLandFactor*gDepths[j  ,i  ]);
                        rect.ur.z := ifthen(gDepths[j  ,i+1] > 0,0,UWAOpts.ZLandFactor*gDepths[j  ,i+1]);
                        rect.ll.z := ifthen(gDepths[j+1,i  ] > 0,0,UWAOpts.ZLandFactor*gDepths[j+1,i  ]);
                        rect.lr.z := ifthen(gDepths[j+1,i+1] > 0,0,UWAOpts.ZLandFactor*gDepths[j+1,i+1]);
                    end
                    else
                        rect.setAllPointsZ(glZCoordForDummyLand);

                    rect.setAllPointsColor(defaultLandColor[0],defaultLandColor[1],defaultLandColor[2], alpha);

                    if drawTriangle1 then
                        self.drawglRectTriangle1(rect);
                    if drawTriangle2 then
                        self.drawglRectTriangle2(rect);
                end; //j
            end; //i

        end
        else
        begin
            //no texture version

            glDisable(GL_TEXTURE_2D);
            glBindTexture( GL_TEXTURE_2D, 0 );

            for i := 0 to li - 2 do
            begin
                if i mod checkTimerInterval = 0 then
                    checkDrawTime();

                {DRAW THE LAND WITH INDIVIDUAL TRIANGLES: slower than triangle strips? but guaranteed to look ok}
                rect.ul.x := xs[i  ];
                rect.ur.x := xs[i+1];
                rect.ll.x := xs[i  ];
                rect.lr.x := xs[i+1];

                for j := 0 to lj - 2 do
                begin
                    rect.ul.y := ys[j  ];
                    rect.ur.y := ys[j  ];
                    rect.ll.y := ys[j+1];
                    rect.lr.y := ys[j+1];

                    drawTriangle1  := notIsnanAndAtLeast1Negative(gDepths[j  ,i  ],gDepths[j  ,i+1],gDepths[j+1,i  ]);
                    drawTriangle2 := notIsnanAndAtLeast1Negative(gDepths[j  ,i+1],gDepths[j+1,i  ],gDepths[j+1,i+1]);

                    if not (drawTriangle1 or drawTriangle2) then continue;

                    {if bMatMinIsNegative, negative depths have not been clipped at 0, and we can happily plot heights}
                    if bMatMinIsNegative then
                    begin
                        {draw land at the actual negative depth (note, using ZLandFactor instead of ZFactor). get land color}

                        //here we set the colour indices to the lowest value. If we are transitioning from land to water, we are going
                        //to draw a triangle down to z = 0. Note that this isn't drawn in exactly the right place: it would be possible
                        //to interpolate between the above and below water points to find the exact transition location (although this gets a little
                        //tricky due to our 2d grid). However, for finer grids the difference will be negligible - it will only be obvious
                        //for very coarse grids.
                        setRectVertexColor(landColormapIndexScaling*gDepths[j  ,i  ],alpha,rect.ulColor,cmapLand);
                        setRectVertexColor(landColormapIndexScaling*gDepths[j  ,i+1],alpha,rect.urColor,cmapLand);
                        setRectVertexColor(landColormapIndexScaling*gDepths[j+1,i  ],alpha,rect.llColor,cmapLand);
                        setRectVertexColor(landColormapIndexScaling*gDepths[j+1,i+1],alpha,rect.lrColor,cmapLand);

                        rect.ul.z := ifthen(gDepths[j  ,i  ] > 0,0,UWAOpts.ZLandFactor*gDepths[j  ,i  ]);
                        rect.ur.z := ifthen(gDepths[j  ,i+1] > 0,0,UWAOpts.ZLandFactor*gDepths[j  ,i+1]);
                        rect.ll.z := ifthen(gDepths[j+1,i  ] > 0,0,UWAOpts.ZLandFactor*gDepths[j+1,i  ]);
                        rect.lr.z := ifthen(gDepths[j+1,i+1] > 0,0,UWAOpts.ZLandFactor*gDepths[j+1,i+1]);
                    end //bMatMinIsNegative
                    else
                    begin
                        {bMatMinIsNegative = false. so we can't use a proper index into the
                        land colormap, so just use the landColor. draw land at a default negative depth.}
                        rect.setAllPointsColor(defaultLandColor[0],defaultLandColor[1],defaultLandColor[2], alpha);
                        rect.setAllPointsZ(glZCoordForDummyLand);
                    end; // not bMatMinIsNegative

                    if drawTriangle1 then
                        self.drawglRectTriangle1(rect);
                    if drawTriangle2 then
                        self.drawglRectTriangle2(rect);
                end; //j
            end; //i
        end;

        glDisable(GL_TEXTURE_2D);
        glBindTexture( GL_TEXTURE_2D, 0 );

        if bMatMinIsNegative then
            drawEdgesBetweenLandAndWater(gDepths,alpha,xs,ys);

        glEndList();
        glCallList(theBathymetry);
        state.bBathymetryListReady := true;
    end;
end;

procedure TGLForm.draw3DWater;
const
    waterGLZPos: double = 0.0001;
var
    mx,my: double;
begin
    mx := container.bathymetry.xAspectRatio();
    my := container.bathymetry.yAspectRatio();

    {glPushAttrib(GL_ENABLE_BIT);

    glBindTexture(GL_TEXTURE_2D, waterTexture);
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);}

    glColor4f(getRValue(UWAOpts.waterSurfacecolor)/255,
        getGValue(UWAOpts.waterSurfacecolor)/255,
        getBValue(UWAOpts.waterSurfacecolor)/255,
        UWAOpts.WaterSurfaceAlpha);      // Set the color blue and fairly transparent

    {draw rectangle with water colour, covering the area that the openGL bathymetry matrix covers}
    glBegin(GL_QUADS);
        //glTexCoord2f(0 + waterTextureOffset.x,0 + waterTextureOffset.y);
        glVertex3f(-1.0       , -1.0       , waterGLZPos);
        //glTexCoord2f(1 + waterTextureOffset.x,0 + waterTextureOffset.y);
        glVertex3f(-1.0 + 2*mx, -1.0       , waterGLZPos);
        //glTexCoord2f(1 + waterTextureOffset.x,1 + waterTextureOffset.y);
        glVertex3f(-1.0 + 2*mx, -1.0 + 2*my, waterGLZPos);
        //glTexCoord2f(0 + waterTextureOffset.x,1 + waterTextureOffset.y);
        glVertex3f(-1.0       , -1.0 + 2*my, waterGLZPos);
    glEnd;

    {glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    glPopAttrib;}
end;

procedure TGLForm.draw3DAxes;
var
    d: double;
begin
    d := 0.2; //how long are the axes lines (in GL units, remember view is from -1 to 1)
    glLineWidth(glAxesWidth);
    glColor4f(0.0, 0.0, 1.0, 1.0);      // blue
    glBegin(GL_LINES);
        glVertex3f( -1.0, -1.0, 0.0);
        glVertex3f( -1.0, -1.0+d, 0.0);
    glEnd();
    glColor4f(1.0, 0.0, 0.0, 1.0);      // red
    glBegin(GL_LINES);
        glVertex3f( -1.0, -1.0, 0.0);
        glVertex3f( -1.0+d, -1.0, 0.0);
    glEnd();
    glColor4f(0.0, 1.0, 0.0, 1.0);      // green
    glBegin(GL_LINES);
        glVertex3f( -1.0, -1.0, 0.0);
        glVertex3f( -1.0, -1.0, 0.0+d);
    glEnd();
end;

procedure TGLForm.drawSphereAt(const x,y,z,aSize,alpha: double;color: TColor);
var
    pQ : pgluquadricobj;
begin
    glColor4f(getRValue(color)/255, getGValue(color)/255, getBValue(color)/255, alpha);
    {we temporarily translate to the centre of the sphere, and then translate back (with trans[0..2]}
    glPushMatrix();

    glTranslatef(x,y,z);
    pQ := glunewquadric;
    //gluSphere(pQ,aSize*(1 + mouseZoomSensitivity*glZoom), 10, 10); //the last two are the number of divisions around the sphere
    gluSphere(pQ,aSize, 10, 10); //the last two are the number of divisions around the sphere

    glPopMatrix();
end;

procedure TGLForm.draw3dRays;
var
    invZMax: double;
    sliceRays: TSliceRays;
    ray: TRay;
    r: TWrappedRayPos;
begin
    invZMax := 1/container.bathymetry.zMax;
    glLineWidth(1.0);
    glPointSize(1.0);
    glColor4d(138/255,51/255,1.0,1.0);
    for sliceRays in sliceRayss do
    begin
        if not sliceRays.visible then continue;

        for ray in sliceRays.rays do
        begin
            if not ray.visible then continue;

            //glBegin(GL_LINE_STRIP);
            glBegin(GL_POINTS);
                for r in ray do
                    glVertex3d(container.geoSetup.w2g(r.ray.x),
                               container.geoSetup.w2g(r.ray.y),
                               UWAOpts.ZFactor*r.ray.z*invZMax);
            glEnd();
        end;
    end;
end;

procedure TGLForm.draw3DScaleBar;
const
    z0: double = 0.0;
    z1: double = 0.001;
var
    y0,dx,dy: double;
begin
    dx := power(10,floor(log10( container.geoSetup.worldScale )));
    dy := dx * 0.08;
    y0 := -2.5 * dy;

    GLBegin(GL_QUADS);
        //white background
        glcolor4f(1,1,1,1);
        glVertex3f(container.geoSetup.w2g(0 ),container.geoSetup.w2g(y0     ),z0);
        glVertex3f(container.geoSetup.w2g(dx),container.geoSetup.w2g(y0     ),z0);
        glVertex3f(container.geoSetup.w2g(dx),container.geoSetup.w2g(y0 + dy),z0);
        glVertex3f(container.geoSetup.w2g(0 ),container.geoSetup.w2g(y0 + dy),z0);

        //black quads
        glcolor4f(0,0,0,1);
        glVertex3f(container.geoSetup.w2g(0   ),container.geoSetup.w2g(y0 + dy/2),z1);
        glVertex3f(container.geoSetup.w2g(dx/4),container.geoSetup.w2g(y0 + dy/2),z1);
        glVertex3f(container.geoSetup.w2g(dx/4),container.geoSetup.w2g(y0 + dy  ),z1);
        glVertex3f(container.geoSetup.w2g(0   ),container.geoSetup.w2g(y0 + dy  ),z1);

        glVertex3f(container.geoSetup.w2g(dx/4),container.geoSetup.w2g(y0       ),z1);
        glVertex3f(container.geoSetup.w2g(dx/2),container.geoSetup.w2g(y0       ),z1);
        glVertex3f(container.geoSetup.w2g(dx/2),container.geoSetup.w2g(y0 + dy/2),z1);
        glVertex3f(container.geoSetup.w2g(dx/4),container.geoSetup.w2g(y0 + dy/2),z1);

        glVertex3f(container.geoSetup.w2g(dx/2),container.geoSetup.w2g(y0 + dy/2),z1);
        glVertex3f(container.geoSetup.w2g(dx  ),container.geoSetup.w2g(y0 + dy/2),z1);
        glVertex3f(container.geoSetup.w2g(dx  ),container.geoSetup.w2g(y0 + dy  ),z1);
        glVertex3f(container.geoSetup.w2g(dx/2),container.geoSetup.w2g(y0 + dy  ),z1);
    GLEnd();

    glLineWidth(1.0);
    glcolor4f(0,0,0,1);
    GLBegin(GL_LINE_LOOP);
        //lines around edges
        glVertex3f(container.geoSetup.w2g(0   ),container.geoSetup.w2g(y0     ),z1);
        glVertex3f(container.geoSetup.w2g(dx  ),container.geoSetup.w2g(y0     ),z1);
        glVertex3f(container.geoSetup.w2g(dx  ),container.geoSetup.w2g(y0 + dy),z1);
        glVertex3f(container.geoSetup.w2g(0   ),container.geoSetup.w2g(y0 + dy),z1);
    GLEnd;

    glBegin(GL_LINES);
        //line through the middle
        glVertex3f(container.geoSetup.w2g(0   ),container.geoSetup.w2g(y0 + dy/2),z1);
        glVertex3f(container.geoSetup.w2g(dx  ),container.geoSetup.w2g(y0 + dy/2),z1);
    GLEnd();
end;

procedure TGLForm.draw3dSettingImageOverlay;
const
    alpha = 1.0;
    guideImageAlpha = 0.4;
    imageZ = 0.09;
    z = 0.1;
    c = clRed;
    cSelected = clWebOrange;
var
    x1,x2,y1,y2: double;
begin
    if not self.getSettingOverlayScale then exit;
    if not container.image.valid then exit;

    x1 := container.geoSetup.w2g(container.image.anchorWorldX1);
    x2 := container.geoSetup.w2g(container.image.anchorWorldX2);
    y1 := container.geoSetup.w2g(container.image.anchorWorldY1);
    y2 := container.geoSetup.w2g(container.image.anchorWorldY2);

    if state.imageOverlaySelectedCorner.HasValue and (state.imageOverlaySelectedCorner = kLL) then
        drawSphereAt(x1,y1,z,CrossSecEndpointSize,alpha,cSelected)
    else
        drawSphereAt(x1,y1,z,CrossSecEndpointSize,alpha,c);

    if state.imageOverlaySelectedCorner.HasValue and (state.imageOverlaySelectedCorner = kUL) then
        drawSphereAt(x1,y2,z,CrossSecEndpointSize,alpha,cSelected)
    else
        drawSphereAt(x1,y2,z,CrossSecEndpointSize,alpha,c);

    if state.imageOverlaySelectedCorner.HasValue and (state.imageOverlaySelectedCorner = kUR) then
        drawSphereAt(x2,y2,z,CrossSecEndpointSize,alpha,cSelected)
    else
        drawSphereAt(x2,y2,z,CrossSecEndpointSize,alpha,c);

    if state.imageOverlaySelectedCorner.HasValue and (state.imageOverlaySelectedCorner = kLR) then
        drawSphereAt(x2,y1,z,CrossSecEndpointSize,alpha,cSelected)
    else
        drawSphereAt(x2,y1,z,CrossSecEndpointSize,alpha,c);

    if state.imageOverlaySelectedCorner.HasValue and (state.imageOverlaySelectedCorner = kCenter) then
        drawSphereAt((x1 + x2) / 2,(y1 + y2) / 2,z,CrossSecEndpointSize,alpha,cSelected)
    else
        drawSphereAt((x1 + x2) / 2,(y1 + y2) / 2,z,CrossSecEndpointSize,alpha,c);

    glLineWidth(1.0);
    glcolor4f(1,0,0,alpha);

    glPushAttrib(GL_ENABLE_BIT);
    glLineStipple(1, $3FC9);
    glEnable(GL_LINE_STIPPLE);

    glBegin(GL_LINE_LOOP);
        glVertex3f(x1,y1,z);
        glVertex3f(x1,y2,z);
        glVertex3f(x2,y2,z);
        glVertex3f(x2,y1,z);
    glEnd;

    glBegin(GL_LINES);
        glVertex3f(x1,y1,z);
        glVertex3f(x2,y2,z);
        glVertex3f(x1,y2,z);
        glVertex3f(x2,y1,z);
    glEnd;

    glPopAttrib();

    if glIsTexture(imageOverlayTexture) then
    begin
        glBindTexture(GL_TEXTURE_2D, imageOverlayTexture);
        glEnable(GL_TEXTURE_2D);
        glColor4d(1,1,1,guideImageAlpha);

        glBegin(GL_QUADS);
        glTexCoord2f(0,0);
        glVertex3f(x1,y1,imageZ);
        glTexCoord2f(0,1);
        glVertex3f(x1,y2,imageZ);
        glTexCoord2f(1,1);
        glVertex3f(x2,y2,imageZ);
        glTexCoord2f(1,0);
        glVertex3f(x2,y1,imageZ);
        glEnd();

        glDisable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0);
    end;
end;

procedure TGLForm.draw3DSources;
var
    src: TSource;
    zLocation: double;
    tsp: TSrcPoint;
    invZMax: double;
    p: integer;
    wp: TFPoint3;
begin
    invZMax := 1 / container.bathymetry.zMax;
    for src in container.scenario.sources do
    begin

        if src.getMovingPosMax > 1 then
        begin //moving source, draw line
            glColor4f(getRValue(src.color)/255,
                      getGValue(src.color)/255,
                      getBValue(src.color)/255,
                      src.Alpha);

            glLineWidth(1.0);//UWAOpts.SourceLineWidth);

            glBegin(GL_LINE_STRIP);
                for tsp in src.MovingPos do
                begin
                    zLocation := UWAOpts.ZFactor*tsp.Z*invZMax;
                    glVertex3f(container.geoSetup.w2g(tsp.X), container.geoSetup.w2g(tsp.Y), zlocation);
                end;
            glEnd;

            glPointSize(6.0);
            glColor4f(getRValue(src.color)/255,
                      getGValue(src.color)/255,
                      getBValue(src.color)/255,
                      src.Alpha);
            glBegin(GL_POINTS);
                for tsp in src.MovingPos do
                begin
                    zLocation := UWAOpts.ZFactor*tsp.Z*invZMax;
                    glVertex3f(container.geoSetup.w2g(tsp.X), container.geoSetup.w2g(tsp.Y), zlocation);
                end;
            glEnd;

            glPointSize(6.0);
            glColor4f(0.8, 0.8, 0.0, src.Alpha);
            glBegin(GL_POINTS);
                for p := 0 to src.getMovingPosMax - 1 do
                begin
                    wp := src.getMovingPos(p);
                    zLocation := UWAOpts.ZFactor*wp.Z*invZMax;
                    glVertex3f(container.geoSetup.w2g(wp.X), container.geoSetup.w2g(wp.Y), zlocation);
                end;
            glEnd();
        end
        else
        begin //Stationary source, draw sphere (or could also be moving source, with only 1 point set)
            zLocation := UWAOpts.ZFactor*src.Pos.Z*invZMax; //UWAOpts.ZFactor*w2gz(src.Pos.Z,container.worldscale),
            drawSphereAt(container.geoSetup.w2g(src.Pos.X),
                         container.geoSetup.w2g(src.Pos.Y),
                         zLocation,
                         UWAOpts.SourceSize,
                         src.Alpha,
                         src.color);
        end;
    end;
end;

procedure TGLForm.draw3DProbes;
var
    prb: TProbe;
    tmpColor: TColor;
    zLocation: double;
begin
    if not assigned(container) then exit;
    if not assigned(container.scenario.theRA) then exit;

    for prb in container.scenario.theRA.probes do
        if prb.visible then
        begin
            //if theRA.enabled then
            if prb.enabled then
                tmpColor := prb.color
            else
                tmpColor := prb.disabledColor;

            if UWAOpts.LevelsDisplay = ldMax then
                zLocation := 0
            else
            begin
                //use the z location stored in the probe
                //zLocation := UWAOpts.ZFactor*prob.depths(UWAOpts.DisplayZInd)*UWAOpts.invZClip;
                zLocation := UWAOpts.ZFactor*prb.Pos.Z / container.bathymetry.zMax;
            end;

            glColor4f(getRValue(tmpColor)/255, getGValue(tmpColor)/255, getBValue(tmpColor)/255, prb.alpha);      // Set the color and alpha of the plane
            drawSphereAt(container.geoSetup.w2g(prb.Pos.X), container.geoSetup.w2g(prb.Pos.Y),
                zLocation,UWAOpts.ProbeSize,prb.Alpha,tmpColor);
        end;
end;

procedure TGLForm.draw3DCrossSecGuide(const xsec: TCrossSec);
const
    crosssecZ: double = 0.5;
var
    EPcolor: array [0..1] of TColor;
    linecolor: Tcolor;
    tmpAlpha: double;
    xs,ys,zs: array [0..1] of double;
    i: integer;
    sl: TStringList;
    sRulerText: string;
    lev: double;
    sLev: string;
begin
    EPcolor[0] := xsec.endpoints[0].Color;
    EPcolor[1] := xsec.endpoints[1].Color;
    linecolor  := xsec.endpoints[0].OrigColor;
    tmpAlpha := 1;

    sl := TStringList.Create();
    sl.Add('A');
    sl.Add('B');

    xs[0] := container.geoSetup.w2g(xsec.endpoints[0].Pos.X);
    xs[1] := container.geoSetup.w2g(xsec.endpoints[1].Pos.X);
    ys[0] := container.geoSetup.w2g(xsec.endpoints[0].Pos.Y);
    ys[1] := container.geoSetup.w2g(xsec.endpoints[1].Pos.Y);
    zs[0] := crosssecZ;
    zs[1] := crosssecZ;

    {draw a line connecting the 2 endpoints}
    glColor4f(getRValue(linecolor)/255, getGValue(linecolor)/255,
        getBValue(linecolor)/255, tmpAlpha);
    glLineWidth(glCSEPBarWidth);
    glBegin(GL_LINES);
        for i := 0 to 1 do
            glVertex3f(xs[i],ys[i],zs[i]);
    GLEnd();

    {draw the 2 endpoints}
    for i := 0 to 1 do
    begin
        drawSphereAt(xs[i],ys[i],zs[i],CrossSecEndpointSize,tmpAlpha,EPcolor[i]);
        //if bSettingRuler then
        //begin
        if i = 0 then
        begin
            //do nothing
            //glPrint(sl[i],FontLarge,xs[i] + dGLTextXOffset,ys[i] + dGLTextYOffset,zs[i],UWAOpts.GLTextColor)
        end
        else
        begin
            //glprinting the text showing the length of the ruler. work out the distance between the points, and
            //convert to feet if needed. note tjat we have also hidden the normal mouse cursor text

            //get the level at the end point
            sLev := '';
            if isBirdsEyeView and container.scenario.resultsExist() then
            begin
                lev := container.scenario.theRA.getLevAtXY(xsec.endpoints[1].Pos.x,
                                                           xsec.endpoints[1].Pos.y,
                                                           UWAOpts.LevelsDisplay,
                                                           UWAOPts.DisplayZInd,true);
                if (not isnan(lev)) and (lev > lowLev) then
                begin
                    lev := container.scenario.convertLevel(lev);
                    sLev := tostr(round1(lev)) + ' dB';
                end;
            end;
            sRulerText := tostr(saferound(f2m(hypot(xsec.dx,xsec.dy), UWAOpts.DispFeet))) + ' ' +
                          ifthen(UWAOpts.DispFeet,'ft','m') + '  ' +
                          sLev;
            glPrint(sRulerText,FontLarge,xs[i] + dGLTextXOffset,ys[i] + dGLTextYOffset,zs[i],UWAOpts.GLTextColor);
        end;
        //end
        //else
        //    glPrint(sl[i],FontLarge,xs[i] + dGLTextXOffset,ys[i] + dGLTextYOffset,zs[i],UWAOpts.GLTextColor);
    end;
end;

procedure TGLForm.draw3DBorderText;
const
    textXLoc   : double = -1.03;
    textYLoc   : double = -1.03;
    tickYLoc   : double = -1.01;
    tickXLoc   : double = -1.01;
    TxtZ       : double = 0.5;
var
    aMin, aMax: double;
    tmp: integer;
    //dtmp: double;
    HorizontalTicks, VerticalTicks: integer;
    i: integer;
    glxmn, glxmx, glymn, glymx: double;
    horizYloc, vertXloc: double;
    dHalfCharsize: double;
    dXOffset: double;
begin
    //dtmp := container.scenario.theRA.xMax / larger(container.scenario.theRA.xMax,container.scenario.theRA.yMax);
    glxmn := -1;
    glxmx := -1 + 2*container.bathymetry.xAspectRatio;   //container.scenario.theRA.xMax / larger(container.scenario.theRA.xMax,container.scenario.theRA.yMax);
    //dtmp := container.scenario.theRA.yMax / larger(container.scenario.theRA.xMax,container.scenario.theRA.yMax);
    glymn := -1;
    glymx := -1 + 2*container.bathymetry.yAspectRatio;    //container.scenario.theRA.yMax / larger(container.scenario.theRA.xMax,container.scenario.theRA.yMax);

    horizYloc := textYLoc;

    vertXloc := textXLoc;

    {horizontal scale along bottom}
    dHalfCharsize :=  iTextHeightSmall / ClientWidth; //approx width of 1/2 char
    aMin := m2f(container.geoSetup.easting,UWAOpts.DispFeet);
    aMax := m2f(container.geoSetup.easting + container.bathymetry.xAspectRatio * container.geoSetup.worldScale ,UWAOpts.DispFeet); //m2f(container.geoSetup.easting + container.scenario.theRA.xMax,UWAOpts.DispFeet);
    tmp := safetrunc(power(10,safetrunc(log10(aMax - aMin))));
    if tmp < 1 then
        tmp := 1;
    if (aMax - aMin)/tmp < 3 then
        HorizontalTicks := saferound(tmp*0.5)
    else
        HorizontalTicks := tmp;
    if HorizontalTicks < 1 then
        HorizontalTicks := 1;
    for i := safetrunc(aMin) to safetrunc(aMax) do
        if (i mod HorizontalTicks) = 0 then
        begin
            {dXOffset is approximately how far we should move the text to the left, to account for the number
            of characters (we want it approximately centered on the axis tick)}
            if i > 0 then
                dXOffset := (-0.5 + larger(1.0,safetrunc(log10(i))))*dHalfCharsize
            else
                dXOffset := 0.5*dHalfCharsize;
            glPrint(tostr(i),
                    FontSmall,
                    XSecCoord(glxmn,glxmx,(i - aMin)/(aMax - aMin)) - dXOffset,
                    horizYloc,
                    TxtZ,
                    UWAOpts.GL3DBorderTextColor);
            glPrint('|',
                    FontSmall,
                    XSecCoord(glxmn,glxmx,(i - aMin)/(aMax - aMin)) - 0.5*dHalfCharsize,
                    tickYLoc,
                    TxtZ,
                    UWAOpts.GL3DBorderTextColor);
        end;

    {vertical scale down left side (NB reversed)}
    aMin := m2f(container.geoSetup.northing,UWAOpts.DispFeet);
    aMax := m2f(container.geoSetup.northing + container.bathymetry.yAspectRatio * container.geoSetup.worldScale ,UWAOpts.DispFeet);//m2f(container.geoSetup.northing + container.scenario.theRA.yMax,UWAOpts.DispFeet);
    tmp := safetrunc(power(10,1.0*safetrunc(log10(aMax - aMin))));
    if tmp < 1 then
        tmp := 1;
    if (aMax - aMin)/tmp < 3 then
        VerticalTicks := saferound(tmp*0.5)
    else
        VerticalTicks := tmp;
    if VerticalTicks < 1 then
        VerticalTicks := 1;
    for i := 0 to (safetrunc(aMax) - safetrunc(aMin)) do
        if ((safetrunc(aMax) - i) mod VerticalTicks) = 0 then
        begin
            if i > 0 then
                dXOffset := (-0.5 + larger(1.0,safetrunc(log10(safetrunc(aMax) - i))))*dHalfCharsize
            else
                dXOffset := 0.5*dHalfCharsize;

            if bEXTFramebufferObjectExists then
            begin
                glPrint90degrees(tostr(safetrunc(aMax) - i),
                                 FontSmall,
                                 vertXloc,
                                 XSecCoord(glymx,glymn,i/(aMax - aMin)) + dXOffset,
                                 TxtZ,
                                 UWAOpts.GL3DBorderTextColor)
            end
            else
            begin
                glPrint(tostr(safetrunc(aMax) - i),
                        FontSmall,
                        vertXloc,
                        XSecCoord(glymx,glymn,i/(aMax - aMin)) + dXOffset,
                        TxtZ,
                        UWAOpts.GL3DBorderTextColor);
            end;
            glPrint('-',
                    FontSmall,
                    tickXLoc,
                    XSecCoord(glymx,glymn,i/(aMax - aMin)) - 0.5*dHalfCharsize,
                    TxtZ,
                    UWAOpts.GL3DBorderTextColor);
//           glPrint(tostr(abs(safetrunc(aMax) - i)),bmpTextBaseSmall,XSecCoord(glymx,glymn,(i - aMin)/(aMax - aMin)),vertXloc,
//               TxtZ,UWAOpts.GL3DBorderTextColor);
//           glPrint('_',bmpTextBaseSmall,XSecCoord(glymx,glymn,(i - aMin)/(aMax - aMin)),tickXLoc,
//               TxtZ,UWAOpts.GL3DBorderTextColor);
        end;

//    if bEXT_framebuffer_object_Exists then
//    begin
//        for i := 0 to 10 do
//        begin
//            //glPrint90degrees('testing',bmpTextBaseLarge,0.1*random,0.1*random,0.1*random,clBlue);
//            glPrint90degrees('testing',bmpTextBaseLarge,0.1*random,0.1*random,0.1*random,clRed);
//        end;
//    end;
end;

procedure TGLForm.draw3DFlock;
var
    a: TAnimat;
    invZMax: double;
    i: integer;
    pMin,pMax: TFPoint3;
begin
    if not assigned(container.scenario.flock) then exit();
    if not container.scenario.flock.isReady then exit();

    pMin := container.scenario.theRA.pos1.asFPoint3();
    pMax := container.scenario.theRA.pos2.asFPoint3();

    invZMax := 1 / container.bathymetry.zMax;

    glPointSize(5.0);
    GLBegin(GL_POINTS);
    for a in container.scenario.flock.animats do
    begin
        //current position
        if (not a.removeFromCalc) and (a.pos.z > 0) then
        begin
           if a.lastLev > a.thresholdLevel then
                glColor4f(0.0,0.5,1.0,1.0)
            else
                glColor4f(0.2,0.6,0.6,0.8);
            glvertex3d(container.geoSetup.w2g(a.Pos.X),
                       container.geoSetup.w2g(a.Pos.Y),
                       UWAOpts.ZFactor*a.Pos.Z*invZMax);
        end;
    end;
    GLEnd();

    //trail of last positions
    glPointSize(2.0);
    glColor4f(0.3,0.3,0.3,0.6);
    GLBegin(GL_POINTS);
    if container.scenario.flock.keepPosHistory then
        for a in container.scenario.flock.animats do
            for I := 0 to a.posHistory.Count - 1 do
                if (a.posHistory[i].z > 0) and
                   (a.posHistory[i].x > pMin.x) and (a.posHistory[i].x < pMax.x) and
                   (a.posHistory[i].y > pMin.y) and (a.posHistory[i].y < pMax.y) then
                    glvertex3d(container.geoSetup.w2g(a.posHistory[i].X),
                               container.geoSetup.w2g(a.posHistory[i].Y),
                               UWAOpts.ZFactor*a.posHistory[i].Z*invZMax);
    GLEnd();
end;

procedure TGLForm.draw3DGrid;
var
    i,j,d: integer;
    z,invZMax, angle, sinA, cosA: double;
    src: TSource;
begin
    invZMax := 1/container.bathymetry.zMax;
    glLineWidth(1.0);
    glBegin(GL_LINES);
        glColor4d(1.0,0.5,0.4,0.6);
        z := 0.02;
        for I := 0 to container.scenario.theRA.iMax - 1 do
        begin
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(i)),
                       container.geoSetup.w2g(container.scenario.theRA.ys(0)),
                       z);
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(i)),
                       container.geoSetup.w2g(container.scenario.theRA.yMax ),
                       z);
        end;
        for j := 0 to container.scenario.theRA.jMax - 1 do
        begin
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(0)),
                       container.geoSetup.w2g(container.scenario.theRA.ys(j)),
                       z);
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xMax ),
                       container.geoSetup.w2g(container.scenario.theRA.ys(j)),
                       z);
        end;

        for d := 0 to container.scenario.dMax - 1 do
        begin
            z := UWAOpts.ZFactor*container.scenario.depths(d)*invZMax;
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(0)),
                       container.geoSetup.w2g(container.scenario.theRA.ys(0)),
                       z);
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xmax ),
                       container.geoSetup.w2g(container.scenario.theRA.ys(0)),
                       z);

            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(0)),
                       container.geoSetup.w2g(container.scenario.theRA.ys(0)),
                       z);
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(0)),
                       container.geoSetup.w2g(container.scenario.theRA.ymax ),
                       z);
        end;
        z := UWAOpts.ZFactor*container.scenario.depths(container.scenario.dMax - 1)*invZMax;
        for I := 0 to container.scenario.theRA.iMax - 1 do
        begin
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(i)),
                       container.geoSetup.w2g(container.scenario.theRA.ys(0)),
                       0);
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(i)),
                       container.geoSetup.w2g(container.scenario.theRA.ys(0)),
                       z);
        end;
        for j := 0 to container.scenario.theRA.jMax - 1 do
        begin
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(0)),
                       container.geoSetup.w2g(container.scenario.theRA.ys(j)),
                       0);
            glVertex3d(container.geoSetup.w2g(container.scenario.theRA.xs(0) ),
                       container.geoSetup.w2g(container.scenario.theRA.ys(j)),
                       z);
        end;
    glEnd();

    glPointSize(2.0);
    glBegin(GL_POINTS);
        glColor4d(0.5,0.7,0.4,1.0);
        z := 0.02;
        for src in container.scenario.sources do
        begin
            for i := 0 to src.nSlices - 1 do
            begin
                angle := i / src.nSlices * 2 * PI;
                system.SineCosine(angle, sinA, cosA);
                for j := 0 to container.scenario.sourcesIMax - 1 do
                    if src.isMovingSource and (src.getMovingPosMax > 0) then
                        glVertex3d(container.geoSetup.w2g(src.movingPos.first.x + src.ranges(j)*cosA),
                                   container.geoSetup.w2g(src.movingPos.first.y + src.ranges(j)*sinA),
                                   z)
                    else
                        glVertex3d(container.geoSetup.w2g(src.pos.x + src.ranges(j)*cosA),
                                   container.geoSetup.w2g(src.pos.y + src.ranges(j)*sinA),
                                   z);
            end;
        end;
    glEnd();
end;


procedure TGLForm.draw3dOverlays;
var
    z,invZMax: double;
    o: TGeoOverlay;
    loop: TWrappedFPointArray;
    p, center: TFPoint;
    i: integer;
begin
    invZMax := -1/container.bathymetry.zMax;
    glLineWidth(2.0);

    glPushMatrix;
    //glTranslatef(container.geoSetup.w2g(container.geoSetup.Easting), container.geoSetup.w2g(container.geoSetup.Northing), 0);

    for o in container.geoOverlays do
    begin
        if not o.visible then continue;

        z := UWAOpts.ZFactor*o.z*invZMax;

        //contour around edge
        glColor4f(o.color.r/255,
            o.color.g/255,
            o.color.b/255,
            1.0);
        for loop in o.loops do
        begin
            glBegin(GL_LINE_LOOP);

            for p in loop.points do
//                glVertex3d(container.geoSetup.w2g(p.x - container.geoSetup.Easting),
//                    container.geoSetup.w2g(p.y - container.geoSetup.Northing),
//                    z);
                glVertex3d(container.geoSetup.w2g(p.x),
                    container.geoSetup.w2g(p.y),
                    z);

            glEnd;
        end;

        if not o.fillVisible then continue;

        //simple triangle fan drawing of polygon around barycenter (goes wrong if loop points arent ordered in angle wrt barycenter)
        glColor4f(o.color.r/255,
            o.color.g/255,
            o.color.b/255,
            o.fillOpacity);
        for loop in o.loops do
        begin
            center := loop.center;

            glBegin(GL_TRIANGLE_FAN);

//            glVertex3d(container.geoSetup.w2g(center.x - container.geoSetup.Easting),
//                container.geoSetup.w2g(center.y - container.geoSetup.Northing),
//                z);
//
//            for i := 0 to length(loop.points) - 1 do
//            begin
//                glVertex3d(container.geoSetup.w2g(loop.points[i].x - container.geoSetup.Easting),
//                    container.geoSetup.w2g(loop.points[i].y - container.geoSetup.Northing),
//                    z);
//
//                glVertex3d(container.geoSetup.w2g(loop.points[(i + 1) mod length(loop.points)].x - container.geoSetup.Easting),
//                    container.geoSetup.w2g(loop.points[(i + 1) mod length(loop.points)].y - container.geoSetup.Northing),
//                    z);
//            end;

            glVertex3d(container.geoSetup.w2g(center.x),
                container.geoSetup.w2g(center.y),
                z);

            for i := 0 to length(loop.points) - 1 do
            begin
                glVertex3d(container.geoSetup.w2g(loop.points[i].x),
                    container.geoSetup.w2g(loop.points[i].y),
                    z);

                glVertex3d(container.geoSetup.w2g(loop.points[(i + 1) mod length(loop.points)].x),
                    container.geoSetup.w2g(loop.points[(i + 1) mod length(loop.points)].y),
                    z);
            end;

            glEnd;
        end;
    end;

    glPopMatrix;
end;

//procedure TGLForm.draw3DRA;
//var
//    z,invZMax: double;
//begin
//    invZMax := 1/container.bathymetry.zMax;
//    glLineWidth(2.0);
//    z := 0.01;
//    glColor4d(1.0,0.5,0.4,0.6);
//    glBegin(GL_LINE_LOOP);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos1.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos1.y),
//                   z);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos2.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos1.y),
//                   z);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos2.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos2.y),
//                   z);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos1.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos2.y),
//                   z);
//    glEnd();
//
//    glColor4d(1.0,0.5,0.4,0.15);
//    glBegin(GL_POLYGON);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos1.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos1.y),
//                   z);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos2.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos1.y),
//                   z);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos2.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos2.y),
//                   z);
//        glVertex3d(container.geoSetup.w2g(container.scenario.theRA.pos1.x),
//                   container.geoSetup.w2g(container.scenario.theRA.pos2.y),
//                   z);
//    glEnd();
//end;

class function TGLForm.levelChecking(const thisLev, convertedMax, convertedMin: single): single;
begin
    {if we are NOT doing exclusion zone, we want the levels to be shown everywhere, just with the lowest
    possible color. so make sure the levels are just above the lower limit, so they get drawn. if we ARE
    doing exclusion zone, then just make the highest values be below the max}
    result := thisLev;
    if not isNaN(result) then
    begin
        if result < lowLev then
            result := nan
        else
        begin
            result := container.scenario.convertLevel(result);
            if UWAOpts.levelLimits.showExclusionZone then
            begin
                if result > convertedMax then
                    result := convertedMax;
            end
            else
            forceRange(result,convertedMin,convertedMax)
        end; //lowlev
    end; //isnan
end;

procedure TGLForm.setImageOverlayTexture(const bmp: TBitmap);
var
    pixelsUpsideDown, pixels: TBytes;
    i,j: integer;
    //b: BITMAP;
begin
    //getobject(bmp.handle, sizeof(BITMAP), b);
    setlength(pixelsUpsideDown, bmp.width * bmp.height * 4);
    setlength(pixels, bmp.width * bmp.height * 4);
    GetBitmapBits(bmp.Handle, length(pixelsUpsideDown), pixelsUpsideDown);
    for j := 0 to bmp.Height - 1 do
        for i := 0 to bmp.width - 1 do
        begin
            //pixelsUpsideDown[(((j * bmp.width) + i) * 4) + 3] := 255; //set alpha 1.0
            pixels[((((bmp.Height - 1 - j) * bmp.width) + i) * 4)    ] := pixelsUpsideDown[(((j * bmp.width) + i) * 4) + 2];
            pixels[((((bmp.Height - 1 - j) * bmp.width) + i) * 4) + 1] := pixelsUpsideDown[(((j * bmp.width) + i) * 4) + 1];
            pixels[((((bmp.Height - 1 - j) * bmp.width) + i) * 4) + 2] := pixelsUpsideDown[(((j * bmp.width) + i) * 4)    ];
            pixels[((((bmp.Height - 1 - j) * bmp.width) + i) * 4) + 3] := 255;
        end;

    if glIsTexture(imageOverlayTexture) then
        glDeleteTextures(1, @imageOverlayTexture);
    glGenTextures( 1, @imageOverlayTexture );// allocate a texture name
    glBindTexture( GL_TEXTURE_2D, imageOverlayTexture ); // select our current texture
    ClearTexture(imageOverlayTexture, bmp.Width,bmp.Height);

    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bmp.Width, bmp.Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, @pixels[0]);
    gluBuild2DMipmaps( GL_TEXTURE_2D, GL_RGBA, bmp.Width, bmp.Height, GL_RGBA, GL_UNSIGNED_BYTE, @pixels[0]);

    //if glGetError <> GL_NO_ERROR then
    //    showmessage(inttostr(glGetError));
end;

procedure TGLForm.draw3DResults(const componentVisibility: TBooleanDynArray);
    function  multipleDepthInds: boolean;
    begin
        result := UWAOpts.levelsDisplay = ldAllLayers;
    end;

    function  getPlotDepthInds: TIntegerDynArray;
    var
        i: integer;
    begin
        if multipleDepthInds() then
        begin
            setlength(result, container.scenario.dMax);
            for i := 0 to length(result) - 1 do
                result[i] := i;
            result := result.reverse;
        end
        else if UWAOpts.levelsDisplay = ldMax then
            result := [0] //show at surface
        else
            result := [UWAOpts.displayZInd];
    end;

    function getLevsRA(const iZInds: TIntegerDynArray; const pb: TProgressbar; const lb: TLabel): TSingleField;
    const
        updateNSteps = 25;
    var
        k,i,j: integer;
        thisLev: single;
        prevActivePage: integer;
        convertedMax, convertedMin: single;
        levDisp: TLevelsDisplay;
    begin
        prevActivePage := mainform.mainPageControl.ActivePageIndex;
        mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Solve);
        setlength(result,length(iZInds),container.scenario.theRA.imax,container.scenario.theRA.jmax);

        if multipleDepthInds() then
            levDisp := ldSingleLayer
        else
            levDisp := UWAOpts.levelsDisplay;

        if lb <> nil then
        begin
            lb.Caption := rsPreparingLevelsGraphicsLayer;
            lb.Visible := true;
        end;

        if pb <> nil then
        begin
            pb.Max := length(iZInds) * container.scenario.theRA.imax;
            pb.Step := updateNSteps;
            pb.Position := 0;
            pb.Visible := true;
        end;

        convertedMin := container.scenario.convertLevel(UWAOpts.levelLimits.Min) + 0.01;
        convertedMax := container.scenario.convertLevel(UWAOpts.levelLimits.Max);

        for k := 0 to length(iZInds) - 1 do
        begin
            for i := 0 to container.scenario.theRA.imax - 1 do
            begin
                if ((i mod updateNSteps) = 0) and (pb <> nil) then
                begin
                    pb.StepIt();
                    Application.ProcessMessages;
                end;

                for j := 0 to container.scenario.theRA.jmax - 1 do
                begin
                    if UWAOpts.levelLimits.MaxOrMinNaN then
                        result[k,i,j] := nan
                    else
                    begin
                        {getLev calls the appropriate getlev_* function - projected or not}
                        thisLev := container.levelServer.getLev(iZInds[k],i,j,resultsMovingPosIndForGIFTool,
                                                                levDisp,
                                                                container.scenario.NaNSeafloorLevels);
                        result[k,i,j] := levelChecking(thisLev, convertedMax, convertedMin);
                    end;
                end;//j
            end; //i
        end; //k

        mainform.mainPageControl.ActivePageIndex := prevActivePage;
        if pb <> nil then
            pb.Visible := false;
        if lb <> nil then
            lb.Visible := false;
        if (pb <> nil) or (lb <> nil) then
            Application.ProcessMessages();
    end;

//    function getLevsRAThreaded(const iZInd: integer; const pb: TProgressbar; const lb: TLabel): TSingleMatrix;
//    const
//        updateNSteps = 25;
//    var
//        prevActivePage: integer;
//        convertedMax, convertedMin: single;
//        pResult: PSingleMatrix;
//        levDisp: TLevelsDisplay;
//    begin
//        prevActivePage := mainform.mainPageControl.ActivePageIndex;
//        mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Solve);
//        setlength(result,container.scenario.theRA.imax,container.scenario.theRA.jmax);
//        //setlength(mat,container.scenario.theRA.imax,container.scenario.theRA.jmax);
//        pResult := @result;
//
//        if multipleDepthInds() then
//            levDisp := UWAOpts.levelsDisplay
//        else
//            levDisp := ldSingleLayer;
//
//        if lb <> nil then
//        begin
//            lb.Caption := rsPreparingLevelsGraphicsLayer;
//            lb.Visible := true;
//        end;
//
//        if pb <> nil then
//        begin
//            pb.Max := container.scenario.theRA.imax;
//            pb.Step := updateNSteps;
//            pb.Position := 0;
//            pb.Visible := true;
//        end;
//
//        convertedMin := container.scenario.convertLevel(UWAOpts.levelLimits.Min) + 0.01;
//        convertedMax := container.scenario.convertLevel(UWAOpts.levelLimits.Max);
//        //TParallel.for(0, container.scenario.theRA.imax - 1, procedure(i: integer)
//        Parallel.For(0, container.scenario.theRA.imax - 1).Execute(procedure (i: integer)
//        var
//            j: integer;
//            thisLev: single;
//        begin
//            for j := 0 to container.scenario.theRA.jmax - 1 do
//            begin
//                if UWAOpts.levelLimits.MaxOrMinNaN then
//                    pResult^[i,j] := nan
//                else
//                begin
//                    {getLev calls the appropriate getlev_* function - projected or not}
//                    thisLev := container.levelServer.getLev(iZInd,i,j,
//                                                            levDisp,
//                                                            container.scenario.NaNSeafloorLevels);
//                    pResult^[i,j] := levelChecking(thisLev, convertedMax, convertedMin);
//                end;
//            end;//j
//        end); //i
//
//        mainform.mainPageControl.ActivePageIndex := prevActivePage;
//        pb.Visible := false;
//        lb.Visible := false;
//        Application.ProcessMessages();
//    end;
var
    d, i, k    : integer;
    LevsRA     : TSingleMatrix;
    glContours : TDoubleDynArray;
    LevsRAs    : TSingleField;
begin
    //TODO add a downsampling to the results, same as the bathymetry, if the number of points exceeds say GLwidthxGLheight

    if not (length(componentVisibility) - 1) = integer(high(TVisibilityEnum)) then
    begin
        raise Exception.Create(rsIncorrectLengthOfComponentVisibilityArray);
        Exit;
    end;

    if not container.scenario.resultsExist() then exit();

    {If we are plotting exclusion zones (via the texture generation method) do that here.
    Don't do the FBO texture generation stuff inside the display list. The plotting step is fast (4 vertices) so no need for display list}
    if UWAOpts.levelLimits.showExclusionZone then
    begin
        {if last plot was XSec, then we need to redo the results colors and colormap}
        if state.bLastGLWasXSec then
            UWAOpts.updateCMaxCMinandLevelColors();
        if container.scenario.resultsExist() then
            draw3DExclusionZones(componentVisibility[ord(veResults)] , componentVisibility[ord(veContours)]);
    end
    else
    begin
        {if the callList is already prepared, just call it. if not, clear and make new list and do the drawing routines}
        if state.bResultsListReady then
            glCallList(theResults)
        else
        begin
            Mainform.sbStatusbar.SimpleText := rsGeneratingResultsDisplay;
            application.ProcessMessages(); //MAKE SURE THIS IS NOT WITHIN A GL DISPLAY LIST

            {if last plot was XSec, then we need to redo the results colors and colormap}
            if state.bLastGLWasXSec then
                UWAOpts.updateCMaxCMinandLevelColors();

            {delete and refresh gl call list}
            glDeleteLists(theResults,1);
            glNewList(theResults, GL_COMPILE);

            if length(currentLevsRA) = 0 then
                LevsRAs := getLevsRA(getPlotDepthInds(), framSolve.pbSolveProgress, framSolve.lbSolving);
            for k := 0 to length(getPlotDepthInds()) - 1 do
            begin
                d := getPlotDepthInds()[k];

                if length(currentLevsRA) <> 0 then
                    LevsRA := currentLevsRA.copy //levels injected from flock
                else
                    LevsRA := LevsRAs[k];

                if componentVisibility[ord(veResults)] then
                begin
                    if UWAOpts.ResultsColormapSmooth then
                        draw3DResultsSmooth(LevsRA, d)
                    else
                    begin
                        glContours := UWAOpts.getLevelContours.reverse;
                        for i := 0 to length(glContours) - 2 do
                            draw3DResultsSquares(LevsRA,
                                                 container.scenario.convertLevel(glContours[i + 1]),
                                                 container.scenario.convertLevel(glContours[i]),
                                                 i, d);
                    end;
                end;

                if componentVisibility[ord(veContours)] and {countour lines between levels. only draw these if we are using marching squares, ie flat colours not blended}
                    (not UWAOpts.ResultsColormapSmooth) then
                begin
                    glContours := UWAOpts.getLevelContours.reverse;
                    for i := 0 to length(glContours) - 2 do
                        draw3DResultsContours(LevsRA,
                                              container.scenario.convertLevel(glContours[i + 1]),
                                              UWAOpts.resultsContoursAlpha,
                                              UWAOpts.resultsContoursDarkening,
                                              i,d);
                end;
            end; //d in getPlotDepthInds

            if false then
                if not UWAOpts.levelLimits.MaxOrMinNaN then
                    for i := 0 to container.scenario.dMax - 1 do
                        self.draw3DResultsPointCloud(getLevsRA([i], nil, nil)[0], i);

            glEndList(); //list is now ready
            glCallList(theResults); //call the list we just made
            state.bResultsListReady := true;

            Mainform.sbStatusbar.SimpleText := '';
            application.ProcessMessages(); //MAKE SURE THIS IS NOT WITHIN A GL DISPLAY LIST
        end; // not resultsListReady
    end; //exclusion zone
end;

procedure TGLForm.draw3DResultsSmooth(const LevsRA: TSingleMatrix; const iZInd: integer);
{draw results as smoothed colours, ie not with the marching squares algorithm}
const
    firstTri : array [0..2] of array [0..1] of integer = ((0,0),(1,0),(0,1));
    secondTri: array [0..2] of array [0..1] of integer = ((1,0),(0,1),(1,1));
var
    i,j,hrcm           : integer;
    li,lj              : integer;
    //iZInd              : integer;
    LkpC               : TMatrix;
    zs                 : double;
    xs,ys              : array of double;
    a,b,q              : integer;
    CMap               : TMatrix;
    convertedMax, convertedMin: single;
begin
    if not container.scenario.resultsExist() then Exit;

    CMap := UWAOpts.ResultsColormap;
    hrcm := high(CMap);

    if iZInd >= container.scenario.dmax then Exit;

    li := container.scenario.theRA.imax;  //getting the size of 2d array
    lj := container.scenario.theRA.jmax;

    {get the x,y,z locations for each point in the grid}
    setlength(xs,li);
    setlength(ys,lj);
    for i := 0 to li - 1 do
        xs[i] := container.geoSetup.w2g(container.scenario.theRA.xs(i));
    for j := 0 to lj - 1 do
        ys[j] := container.geoSetup.w2g(container.scenario.theRA.ys(j));
    {put z slightly above the theRA (but below ground at -1m)}
    zs := UWAOpts.ZFactor*(container.scenario.depths(iZInd) - 0.66) / container.bathymetry.zMax;

    convertedMax := container.scenario.convertLevel( UWAOpts.levelLimits.Max );
    convertedMin := container.scenario.convertLevel( UWAOpts.levelLimits.Min );

    {generate a matrix LRA containing lookup values (into the colormap)
    for each drawing matrix position.}
    setlength(LkpC,li,lj);
    for i := 0 to li - 1 do
        for j := 0 to lj - 1 do
        begin
            if (isNaN(LevsRA[i,j]) or (LevsRA[i,j] < lowLev)) then
                LkpC[i,j] := NaN
            else
            begin
                LkpC[i,j] := hrcm*(convertedMax - LevsRA[i,j])/(convertedMax - convertedMin);

                {remove values outside the colormap range - this only happens if UWAOpts.lowerexclusionzone is true}
                if not checkrange(LkpC[i,j],0,hrcm) then
                    LkpC[i,j] := NaN;
            end;
        end;//i,j

    {BEGIN DRAWING}
    for i := 0 to li - 2 do  //to li-2 because our strips spans i to i+1
    begin
        for j := 0 to lj - 2 do
        begin
            //first triangle
            if not(isnan(LkpC[i  ,j  ]) or isnan(LkpC[i+1,j  ]) or isnan(LkpC[i  ,j+1])) then
            begin
                glBegin(GL_TRIANGLES);
                for q := 0 to 2 do
                begin
                    a := firstTri[q,0];
                    b := firstTri[q,1];

                    glColor4f(CMap[safetrunc(LkpC[i+a,j+b]),0],CMap[safetrunc(LkpC[i+a,j+b]),1],CMap[safetrunc(LkpC[i+a,j+b]),2], UWAOpts.ResultsAlpha);
                    glVertex3f(xs[i+a],ys[j+b],zs);
                end;
                glEnd;
            end;

            //second triangle
            if not(isnan(LkpC[i+1,j  ]) or isnan(LkpC[i  ,j+1]) or isnan(LkpC[i+1,j+1])) then
            begin
                glBegin(GL_TRIANGLES);
                for q := 0 to 2 do
                begin
                    a := secondTri[q,0];
                    b := secondTri[q,1];

                    glColor4f(CMap[safetrunc(LkpC[i+a,j+b]),0],CMap[safetrunc(LkpC[i+a,j+b]),1],CMap[safetrunc(LkpC[i+a,j+b]),2], UWAOpts.ResultsAlpha);
                    glVertex3f(xs[i+a],ys[j+b],zs);
                end;
                glEnd;
            end;
        end; //j
    end;  //i
end;

procedure TGLForm.draw3DResultsSquares(const LevsRA: TSingleMatrix; const dLowerLevel,dUpperLevel: single; const iThisLevel, iZInd: integer);
const
    zFightingConst: double = -0.0001; //multiply the current level index by this, to avoid z-fighting issues with undecidable squares
var
    i,j                : integer;
    LevelGtLt          : TIntegerMatrix;
    zs                 : double;
    xs,ys              : TDoubleDynArray;
    myCol              : TMyColorRec;
begin
    if not container.scenario.resultsExist() then Exit;
    if length(LevsRA) = 0 then exit;

    myCol := UWAOpts.ResultsColorsRec(iThisLevel);
    if not myCol.Visible then Exit;

    xs := self.getMarchingSquaresXorYGrid(container.scenario.theRA.imax,
                                          container.scenario.theRA.xs(0),
                                          container.scenario.theRA.xMax);
    ys := self.getMarchingSquaresXorYGrid(container.scenario.theRA.jmax,
                                          container.scenario.theRA.ys(0),
                                          container.scenario.theRA.yMax);

    {put z slightly above the theRA (below ground, and contour lines)}
    zs := UWAOpts.ZFactor / container.bathymetry.zMax * (container.scenario.depths(iZInd) - 0.33) + iThisLevel*zFightingConst;

    setlength(LevelGtLt,length(LevsRA),length(LevsRA[0]));
    for i := 0 to length(LevsRA) - 1 do
        for j := 0 to length(LevsRA[0]) - 1 do
            if isnan(LevsRA[i,j]) or (LevsRA[i,j] <= dLowerLevel) then
                LevelGtLt[i,j] := 0
            else if LevsRA[i,j] > dUpperLevel then
                LevelGtLt[i,j] := 2
            else
                LevelGtLt[i,j] := 1;

    glColor4f(getRValue(myCol.Color)/255,
        getGValue(myCol.Color)/255,
        getBValue(myCol.Color)/255, UWAOpts.ResultsAlpha);

    if UWAOpts.ResultsTris then
        drawMarchingTrisBands(TMarchingSquares.MarchingTrisTrits(LevelGtLt),xs,ys,zs)
    else
        drawMarchingSquaresBands(TMarchingSquares.MarchingSquaresTrits(LevelGtLt),xs,ys,zs);
end;

procedure TGLForm.draw3DResultsContours(const LevsRA: TSingleMatrix; const dContourLevel: single; const alpha, darkening: double; const iThisLevel, iZInd: integer);
var
    i,j                : integer;
    mbLRA              : TBoolMatrix;
    zs                 : double;
    xs,ys              : TDoubleDynArray;
    myCol              : TMyColorRec;
begin
    if not container.scenario.resultsExist() then Exit;
    if length(LevsRA) = 0 then exit;

    myCol := UWAOpts.ResultsColorsRec(iThisLevel);
    if not myCol.Visible then Exit;

    xs := self.getMarchingSquaresXorYGrid(container.scenario.theRA.imax,
                                          container.scenario.theRA.xs(0),
                                          container.scenario.theRA.xMax);
    ys := self.getMarchingSquaresXorYGrid(container.scenario.theRA.jmax,
                                          container.scenario.theRA.ys(0),
                                          container.scenario.theRA.yMax);

    {put z slightly above the theRA (but below ground at -1m)}
    zs := UWAOpts.ZFactor*(container.scenario.depths(iZind) - 0.66) / container.bathymetry.zMax;

    setlength(mbLRA,length(LevsRA), length(LevsRA[0]));
    for i := 0 to length(LevsRA) - 1 do
        for j := 0 to length(LevsRA[0]) - 1 do
            mbLRA[i,j] := LevsRA[i,j] > dContourLevel;

    glLineWidth( UWAOpts.resultsContoursWidth );
    glColor4f(getRValue(myCol.Color)/255*darkening,
        getGValue(myCol.Color)/255*darkening,
        getBValue(myCol.Color)/255*darkening, alpha);

    if UWAOpts.ResultsTris then
        drawMarchingTrisLines(TMarchingSquares.MarchingTrisBits(mbLRA),xs,ys,zs)
    else
        drawMarchingSquaresLines(TMarchingSquares.MarchingSquaresBits(mbLRA),xs,ys,zs);
end;

procedure TGLForm.draw3DResultsPointCloud(const LevsRA: TSingleMatrix; const iZInd: integer);
var
    i, j, c: integer;
    x,z: double;
    glContours: TDoubleDynArray;
    myCol: TMyColorRec;
begin
    z := UWAOpts.ZFactor*(container.scenario.depths(iZind)) / container.bathymetry.zMax;

    glContours := UWAOpts.getLevelContours.reverse;

    glPointSize(2.0);
    glBegin(GL_POINTS);
        for c := 0 to length(glcontours) - 2 do
        begin
            myCol := UWAOpts.ResultsColorsRec(c);
            glcolor4f(myCol.color.r / 255,
                      myCol.color.g / 255,
                      myCol.color.b / 255,
                      1.0);
            for i := 0 to container.scenario.theRA.iMax - 1 do
            begin
                x := container.scenario.theRA.xs(i);
                for j := 0 to container.scenario.theRA.jMax - 1 do
                    if (LevsRA[i,j] < glContours[c]) and (LevsRA[i,j] >= glContours[c + 1]) then
                        glVertex3f(container.geoSetup.w2g(x),
                                   container.geoSetup.w2g(container.scenario.theRA.ys(j)),
                                   z);
            end;
        end;
    glEnd;
end;






procedure TGLForm.draw2D;
const
    maxPointDistRatio  : double = 0.05;
    dLandGZ            : double = 0.8; //bring land to front, over results
    dSourceGZ          : double = 0.9;
    dResultsSquaresGZ  : double = 0;
    dResultsContoursGZ : double = 0.1;
    dTxtGZ             : double = 0.5;
    dWaterGZ           : double = -0.1;
    glmx               : double = 0.8;
    glmn               : double = -0.8;
var
    zProfile             : TRangeValArray;
    angle,range            : double;
    maxDepth               : double;
    minDepth               : double;
    CSEP0, CSEP1           : TCrossSecEndpoint; //convenience aliases
begin
    if not assigned(container) then exit;
    if not assigned(UWAOpts) then exit;

    container.makeCrossSecInRA;
    CSEP0 := container.crossSec.endpoints[0];
    CSEP1 := container.crossSec.endpoints[1];

    {GET SEA BOTTOM PROFILE}
    range := TMCKLib.Distance3D(CSEP0.Pos.X,CSEP1.Pos.X,
                        CSEP0.Pos.Y,CSEP1.Pos.Y,
                        CSEP0.Pos.Z,CSEP1.Pos.Z);
    angle := arctan2(CSEP1.Pos.Y - CSEP0.Pos.Y,
                     CSEP1.Pos.X - CSEP0.Pos.X);
    zProfile := container.getBathymetryVector2(CSEP0.Pos.X, CSEP0.Pos.Y, angle, range, 0);

    {get the max and min depth (min depth = -max height). make sure neither crosses over 0}
    minDepth := zProfile.min;//.column(1).min;
    if minDepth > 0 then
        minDepth := 0;
    if UWAOpts.XSecTruncateAtWaterSurface then
        minDepth := 0;

    if UWAOpts.FullXSecDepth then
        maxDepth := container.bathymetry.zMax
    else
        maxDepth := zProfile.max;//.column(1).max;
    if maxDepth < container.scenario.depths(1) then
        maxDepth := container.scenario.depths(1);

    //START DRAWING CALLS----------------------------------------------

    draw2DWater(glmn,glmx,minDepth,maxDepth,dWaterGZ);

    {RESULTS}
    if container.scenario.resultsExist() then
        if UWAOpts.componentVisibility[ord(veResults)] or
           (UWAOpts.componentVisibility[ord(veContours)] and not UWAOpts.ResultsColormapSmooth) then
            draw2DResults(UWAOpts.componentVisibility,glmn,glmx,minDepth,maxDepth,range,dResultsSquaresGZ,dResultsContoursGZ,CSEP0,CSEP1);

    //seafloor
    if UWAOpts.GLShowLandinXSec then
        draw2DSeafloor(glmn,glmx,minDepth,maxDepth,dLandGZ,zProfile);

    {check if any objects need to be shown (sources, probes)}
    if UWAOpts.ComponentVisibility[ord(veSources)] then
        draw2DSources(glmn,glmx,minDepth,maxDepth,range,maxPointDistRatio,dSourceGZ,CSEP0,CSEP1);

    if UWAOpts.ComponentVisibility[ord(veProbes)] then
        draw2DProbes(glmn,glmx,minDepth,maxDepth,range,maxPointDistRatio,dSourceGZ,CSEP0,CSEP1);

    //draw axes ticks
    draw2DAxesTicks(glmn,glmx,minDepth,maxDepth,range,dTxtGZ);

    //draw the brids eye overview at top right
    draw2DLocationGuide;
end;

procedure TGLForm.draw2DResults(const componentVisibility: TBooleanDynArray; const glmn,glmx,minDepth,maxDepth,range,dResultsSquaresGZ,dResultsContoursGZ: double; const CSEP0,CSEP1: TCrossSecEndpoint);
    procedure drawResultsSquaresFunc(const gXs, gYs: TDoubleDynArray; const maCrossSecLevels: TSingleMatrix);
    var
        i: integer;
        glContours: TDoubleDynArray;
    begin
        {get the values where we change between contour bands. reverse and set max and min to very large and very small}
        glContours := UWAOpts.getLevelContours.reverse;
        for i := 0 to length(glContours) - 2 do
            self.draw2DResultsSquares(gXs,gYs,dResultsSquaresGZ,maCrossSecLevels, container.scenario.convertLevel( glContours[i + 1] ),container.scenario.convertLevel( glContours[i] ),1.0,i);
    end;
    procedure drawContoursFunc(const gXs, gYs: TDoubleDynArray; const maCrossSecLevels: TSingleMatrix);
    var
        i: integer;
        glContours: TDoubleDynArray;
    begin
        glContours := UWAOpts.getLevelContours.reverse;
        for i := 0 to length(glContours) - 2 do {skip the [end] value}
            self.draw2DResultsContours(gXs,gYs,dResultsContoursGZ,
                                       maCrossSecLevels,
                                       container.scenario.convertLevel( glContours[i] ),
                                       UWAOpts.ResultsContoursAlpha, UWAOpts.resultsContoursDarkening,
                                       i);
    end;
var
    i,d, iLenDepths                : integer;
    distX, distY, angle, sinA, cosA            : double;
    dx, dy                         : double;
    dDistBetweenPoints             : double;
    iLenRange,iLenRangeX,iLenRangey: integer;
    xs,ys,tmpXs,tmpYs              : TDoubleDynArray;
    maCrossSecLevels               : TSingleMatrix;
    convertedLevs                  : TSingleMatrix;
    gXs, gYs                       : TDoubleDynArray;
    iInd,jInd                      : integer;
begin
    {set up the number of depth divisions}
    if UWAOpts.FullXSecDepth then
        iLenDepths := container.scenario.dMax
    else
    begin
        iLenDepths := 0;
        for i := 0 to container.scenario.dMax - 1 do
            if container.scenario.depths(i) < maxDepth then
                iLenDepths := i + 1;
    end;
    if iLenDepths = 0 then exit;


    {set up locations for the xsec results (which should try and match the project grid spacing, not the seafloor spacing)}
    distX := CSEP1.Pos.X - CSEP0.Pos.X;
    distY := CSEP1.Pos.Y - CSEP0.Pos.Y;
    angle := arctan2(distY,distX);
    system.SineCosine(angle, sinA, cosA);

    dx := container.scenario.theRA.xs(1) - container.scenario.theRA.xs(0);
    dy := container.scenario.theRA.ys(1) - container.scenario.theRA.ys(0);
    iLenRangeX := safeceil(distX/dx);
    iLenRangeY := safeceil(distY/dy);
    iLenRange := safeceil(sqrt(iLenRangeX*iLenRangeX + iLenRangeY*iLenRangeY));
    dDistBetweenPoints := range/iLenRange;

    setlength(xs,iLenRange);
    setlength(ys,iLenRange);
    for i := 0 to iLenRange - 1 do
    begin
        xs[i] := CSEP0.Pos.X + i*dDistBetweenPoints*cosA;
        ys[i] := CSEP0.Pos.Y + i*dDistBetweenPoints*sinA;
    end;

    {Get levels matrix - at every point in the z profile and every depth}
    setlength(maCrossSecLevels,iLenRange,iLenDepths);
    {put xs and ys into temp arrays so we can use arrayFind}
    tmpXs := container.scenario.theRA.getXsArray;
    tmpYs := container.scenario.theRA.getYsArray;

    if UWAOpts.levelLimits.MaxOrMinNaN then
        maCrossSecLevels.setAllVal(nan)
    else
    for i := 0 to iLenRange - 1 do
        for d := 0 to iLenDepths - 1 do
        begin
            iInd := saferound(xs[i]/dx);
            forceRange(iInd,0,high(tmpXs));
            jInd := saferound(ys[i]/dy);
            forceRange(jInd,0,high(tmpYs));

            {AtInds ends up being faster than AtPos when we use it a lot, as it doesnt have to rebuild xs and ys}
            maCrossSecLevels[i,d] := container.scenario.theRA.getLevAtInds(d,iInd,jInd,self.resultsMovingPosIndForGIFTool,ldSingleLayer,false);  //this call picks up the weighting also
        end; //i,d

    {convert levels matrix to user LevelType, also without the 'after land' NullLev values, because matrixMin of maCrossSecLevels will probably be NullLev}
    convertedLevs := maCrossSecLevels.copy;
    for i := 0 to length(convertedLevs) - 1 do
        for d := 0 to length(convertedLevs[0]) - 1 do
            if not isnan(convertedLevs[i,d]) then
                if convertedLevs[i,d] < LowLev then
                    convertedLevs[i,d] := NaN
                else
                    convertedLevs[i,d] := container.scenario.convertLevel(convertedLevs[i,d]); //convert level to user type

    setlength(gXs,iLenRange);
    setlength(gYs,iLenDepths);
    for i := 0 to iLenRange - 1 do
        gXs[i] := XSecCoord(glmn,glmx,i/(iLenRange - 1));
    for d := 0 to iLenDepths - 1 do
        gYs[d] := XSecCoord(glmx,glmn,(container.scenario.depths(d) - minDepth)/(maxDepth - minDepth)); //NB reversed

    {PLOT RESULTS (SMOOTH OR SOLID COLOURS)}
    {if our last plot was not XSec, then update CMax and Cmin to be the max and min in the current XSec}
    if not state.bLastGLWasXSec then
    begin
        UWAOpts.levelLimits.setLimsWithSpacing(container.scenario.unconvertLevel( convertedLevs.min ),
                                                container.scenario.unconvertLevel( convertedLevs.max ),
                                                UWAOpts.levelLimits.LevelSpacing,false);
        UWAOpts.updateResultsLevelColors;
        framResults.updateAllGraphics;
    end;

    if componentVisibility[ord(veResults)] then
    begin
        if UWAOpts.ResultsColormapSmooth then
            draw2DResultsSmooth(gXs,gYs,dResultsSquaresGZ,maCrossSecLevels,UWAOpts.resultsColorMap,1.0)
        else
            drawResultsSquaresFunc(gXs, gYs, maCrossSecLevels);
    end;

    if componentVisibility[ord(veContours)] and not UWAOpts.ResultsColormapSmooth then
        drawContoursFunc(gXs, gYs, maCrossSecLevels);
end;

procedure TGLForm.draw2DResultsSmooth(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix; const ColorMap: TMatrix; const alphaResults: double);
{draw results as smoothed colours, ie not with the marching squares algorithm}
var
    hrcm,li,lj,i,j     : integer;
    mappedLevs         : TMatrix;
    convertedMax, convertedMin: single;
begin
    if length(gXs) = 0 then
        exit;

    hrcm := high(Colormap);

    li := length(gXs);  //getting the size of 2d array
    lj := length(gYs);

    convertedMax := container.scenario.convertLevel( UWAOpts.levelLimits.Max );
    convertedMin := container.scenario.convertLevel( UWAOpts.levelLimits.Min );

    {generate a matrix LRA containing lookup values (into the colormap)
    for each drawing matrix position.}
    setlength(mappedLevs,li,lj);
    for i := 0 to li - 1 do
        for j := 0 to lj - 1 do
        begin
            if (not isNaN(maResults[i,j])) and (maResults[i,j] > lowLev) then
            begin
                mappedLevs[i,j] := hrcm*(convertedMax - maResults[i,j])/(convertedMax - convertedMin);
                forceRange(mappedLevs[i,j],0,hrcm);
            end
            else
                mappedLevs[i,j] := NaN;
        end;//i,j

    {draw the colours. the locations are already given (simple grid) but at
    each point we look up the colour from the colormap using the values in
    LRA}
    for i := 0 to li - 2 do  //to li - 2 because our tris strip spans i to i+1
    begin
        glBegin(GL_TRIANGLES);
        for j := 0 to lj  - 2 do
        begin
            if not(isnan(mappedLevs[i,j]) or isnan(mappedLevs[i+1,j]) or isnan(mappedLevs[i,j+1])) then
            begin
                glColor4f(ColorMap[safetrunc(mappedLevs[i  ,j  ]),0],ColorMap[safetrunc(mappedLevs[i  ,j  ]),1],ColorMap[safetrunc(mappedLevs[i  ,j  ]),2], alphaResults);
                glVertex3f(gXs[i  ],gYs[j  ],zs);
                glColor4f(ColorMap[safetrunc(mappedLevs[i+1,j  ]),0],ColorMap[safetrunc(mappedLevs[i+1,j  ]),1],ColorMap[safetrunc(mappedLevs[i+1,j  ]),2], alphaResults);
                glVertex3f(gXs[i+1],gYs[j  ],zs);
                glColor4f(ColorMap[safetrunc(mappedLevs[i  ,j+1]),0],ColorMap[safetrunc(mappedLevs[i  ,j+1]),1],ColorMap[safetrunc(mappedLevs[i  ,j+1]),2], alphaResults);
                glVertex3f(gXs[i  ],gYs[j+1],zs);
            end;
            if not(isnan(mappedLevs[i+1,j+1]) or isnan(mappedLevs[i+1,j]) or isnan(mappedLevs[i,j+1])) then
            begin
                glColor4f(ColorMap[safetrunc(mappedLevs[i+1,j+1]),0],ColorMap[safetrunc(mappedLevs[i+1,j+1]),1],ColorMap[safetrunc(mappedLevs[i+1,j+1]),2], alphaResults);
                glVertex3f(gXs[i+1],gYs[j+1],zs);
                glColor4f(ColorMap[safetrunc(mappedLevs[i+1,j  ]),0],ColorMap[safetrunc(mappedLevs[i+1,j  ]),1],ColorMap[safetrunc(mappedLevs[i+1,j  ]),2], alphaResults);
                glVertex3f(gXs[i+1],gYs[j  ],zs);
                glColor4f(ColorMap[safetrunc(mappedLevs[i  ,j+1]),0],ColorMap[safetrunc(mappedLevs[i  ,j+1]),1],ColorMap[safetrunc(mappedLevs[i  ,j+1]),2], alphaResults);
                glVertex3f(gXs[i  ],gYs[j+1],zs);
            end;
        end;
        glEnd();
    end;//for i := 0 to hi-1 do
end;

procedure TGLForm.draw2DResultsContours(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix;
                    const dContourLevel: double;
                    const alpha, darkening: double; const iThisLevel: integer);
var
    i,j,hi,hj          : integer;
    mbLRA              : TBoolMatrix;
    xs,ys              : TDoubleDynArray;
    xmin,xmax,ymin,ymax: double;
    miMSLines          : TIntegerMatrix;
    miMSLines3         : TIntPairMatrix;
    myCol              : TMyColorRec;
begin
    if length(gXs) = 0 then
        exit;

    myCol := UWAOpts.ResultsColorsRec(iThisLevel);
    if not myCol.Visible then Exit;

    hi := high(gXs);  //getting the size of 2d array
    hj := high(gYs);

    {generate a matrix LRA containing lookup values (into the colormap)
    for each drawing matrix position.}
    setlength(mbLRA,hi+1,hj+1);
    for i := 0 to hi do
        for j := 0 to hj do
            mbLRA[i,j] := maResults[i,j] > dContourLevel;

    if UWAOpts.ResultsTris then
        miMSLines3 := TMarchingSquares.MarchingTrisBits(mbLRA)
    else
        miMSLines := TMarchingSquares.MarchingSquaresBits(mbLRA);


    {xs and ys are different in this algorithm, as we need to
    have an extra point between current points}
    setlength(xs,2*hi+1);
    setlength(ys,2*hj+1);
    xmin := gXs[0];
    xmax := gXs[hi];
    ymin := gYs[0];
    ymax := gYs[hj];
    for i := 0 to 2*hi do
        xs[i] := xmin + (xmax - xmin)*i/(2*hi);
    for j := 0 to 2*hj do
        ys[j] := ymin + (ymax - ymin)*j/(2*hj);

    glLineWidth( UWAOpts.resultsContoursWidth );
    glColor4f(getRValue(myCol.color)/255*darkening,
              getGValue(myCol.color)/255*darkening,
              getBValue(myCol.color)/255*darkening, alpha);

    if UWAOpts.ResultsTris then
        drawMarchingTrisLines(miMSLines3,xs,ys,zs)
    else
        drawMarchingSquaresLines(miMSLines,xs,ys,zs);
end;

procedure TGLForm.draw2DResultsSquares(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix;
                    const dLowerLevel,dUpperLevel: double;
                    const alphaResults: double; const iThisLevel: integer);
var
    hi,hj,i,j: integer;
    LevelGtLt           : TIntegerMatrix;
    xs,ys              : TDoubleDynArray;
    xmin,xmax,ymin,ymax: double;
    miMSBands          : TIntegerMatrix;
    miMSBands3         : TIntPairMatrix;
    myCol              : TMyColorRec;
begin
    if length(gXs) = 0 then
        exit;

    myCol := UWAOpts.ResultsColorsRec(iThisLevel);
    if not myCol.Visible then Exit;

    hi := high(gXs);  //getting the size of 2d array
    hj := high(gYs);

    {for each result point, check whether it is below, at or above the
    current level band. generate a matrix of trits}
    setlength(LevelGtLt,hi+1,hj+1);
    for i := 0 to hi do
        for j := 0 to hj do
        begin
            if isnan(maResults[i,j]) or (maResults[i,j] <= dLowerLevel) then
                LevelGtLt[i,j] := 0
            else
            if maResults[i,j] > dUpperLevel then
                LevelGtLt[i,j] := 2
            else
                LevelGtLt[i,j] := 1;
        end;//i,j

    if UWAOpts.ResultsTris then
        miMSBands3 := TMarchingSquares.MarchingTrisTrits(LevelGtLt)
    else
        miMSBands := TMarchingSquares.MarchingSquaresTrits(LevelGtLt);

    {xs and ys are different in this algorithm, as we need to
    have an extra point between current points}
    setlength(xs,2*hi+1);
    setlength(ys,2*hj+1);
    xmin := gXs[0];
    xmax := gXs[hi];
    ymin := gYs[0];
    ymax := gYs[hj];
    for i := 0 to 2*hi do
        xs[i] := xmin + (xmax - xmin)*i/(2*hi);
    for j := 0 to 2*hj do
        ys[j] := ymin + (ymax - ymin)*j/(2*hj);

    glColor4f(getRValue(myCol.color)/255,
              getGValue(myCol.color)/255,
              getBValue(myCol.color)/255, alphaResults);

    if UWAOpts.ResultsTris then
        drawMarchingTrisBands(miMSBands3,xs,ys,zs)
    else
        drawMarchingSquaresBands(miMSBands,xs,ys,zs);
end;

procedure TGLForm.draw2DWater(const glmn,glmx,minDepth,maxDepth,dWaterGZ: double);
begin
    {DRAW WATER}
    glColor4f(getRValue(UWAOpts.waterSurfacecolor)/255,
        getGValue(UWAOpts.waterSurfacecolor)/255,
        getBValue(UWAOpts.waterSurfacecolor)/255,
        UWAOpts.WaterSurfaceAlpha);      // Set the color blue and fairly transparent
    glBegin(GL_QUADS);
        glVertex3f(glmn,glmn,dWaterGZ);
        glVertex3f(glmx,glmn,dWaterGZ);
        glVertex3f(glmx,glmx + (glmn - glmx)*(0 - minDepth)/(maxDepth - minDepth),dWaterGZ);
        glVertex3f(glmn,glmx + (glmn - glmx)*(0 - minDepth)/(maxDepth - minDepth),dWaterGZ);
    glEnd;
end;

procedure TGLForm.draw2DSeafloor(const glmn,glmx,minDepth,maxDepth,dLandGZ: double; const zProfile: TRangeValArray);
var
    seafloorRMax: double;
    i: integer;
begin
    seafloorRMax := zProfile[length(zProfile) - 1].range;
    {line at seafloor}
    //glColor4f(147/255, 110/255, 22/255, 1); //darker brown //glColor4f(205/255, 133/255, 63/255, 1); //brown
    glColor4f(getRValue(TDBSeaColours.XSecSedimentBorder)/255,
              getGValue(TDBSeaColours.XSecSedimentBorder)/255,
              getBValue(TDBSeaColours.XSecSedimentBorder)/255,
              1);
    glBegin(GL_LINE_STRIP);
        for I := 0 to length(zProfile) - 1 do
            glVertex3f(XSecCoord(glmn,glmx,zProfile[i].range/seafloorRMax),
                        XSecCoord(glmx,glmn,(zProfile[i].val - minDepth)/(maxDepth - minDepth)),dLandGZ);
    glEnd;

    {solidly colour under the seafloor}
    glColor4f(getRValue(TDBSeaColours.XSecSediment)/255,
              getGValue(TDBSeaColours.XSecSediment)/255,
              getBValue(TDBSeaColours.XSecSediment)/255,
              1);
    for I := 0 to length(zProfile) - 2 do
    begin
        glBegin(GL_QUADS);
            glVertex3f(XSecCoord(glmn,glmx,zProfile[i].range/seafloorRMax),glmn,dLandGZ);
            glVertex3f(XSecCoord(glmn,glmx,zProfile[i].range/seafloorRMax),
                XSecCoord(glmx,glmn,(zProfile[i].val - minDepth)/(maxDepth - minDepth)),dLandGZ);
            glVertex3f(XSecCoord(glmn,glmx,zProfile[i + 1].range/seafloorRMax),
                XSecCoord(glmx,glmn,(zProfile[i + 1].val - minDepth)/(maxDepth - minDepth)),dLandGZ);
            glVertex3f(XSecCoord(glmn,glmx,zProfile[i + 1].range/seafloorRMax),glmn,dLandGZ);
        glEnd;
    end;
end;

procedure TGLForm.draw2DSources(const glmn,glmx,minDepth,maxDepth,range,maxPointDistRatio,dSourceGZ: double; const CSEP0,CSEP1: TCrossSecEndpoint);
var
    src: TSource;
    angleLine,angleToPoint, distToPoint: double;
    distToLine,distAlongLine: double;
begin
    for src in container.scenario.sources do
    begin
        {check whether the position is between the endpoints}
        if checkRange(src.Pos.X,smaller(CSEP0.Pos.X,CSEP1.Pos.X),
                                larger(CSEP0.Pos.X,CSEP1.Pos.X)) and
             checkRange(src.Pos.Y,smaller(CSEP0.Pos.Y,CSEP1.Pos.Y),
                                    larger(CSEP0.Pos.Y,CSEP1.Pos.Y)) then
        begin

            {get distance to the line and distance along the line, for this source pos}
            angleLine := arctan2(CSEP1.Pos.Y - CSEP0.Pos.Y,
                                    CSEP1.Pos.X - CSEP0.Pos.X);
            angleToPoint := arctan2(src.Pos.Y - CSEP0.Pos.Y,
                                    src.Pos.X - CSEP0.Pos.X);
            distToPoint := hypot(src.Pos.Y - CSEP0.Pos.Y,src.Pos.X - CSEP0.Pos.X);

            distToLine      := abs(sin(angleLine - angleToPoint)*distToPoint);
            distAlongLine   := abs(cos(angleLine - angleToPoint)*distToPoint);

            if distToLine < maxPointDistRatio*container.geoSetup.WorldScale then
            begin
                drawSphereAt(XSecCoord(glmn,glmx,distAlongLine/range),
                    XSecCoord(glmx,glmn,(src.Pos.Z - minDepth)/(maxDepth - minDepth)),
                    dSourceGZ,
                    UWAOpts.SourceSize,src.Alpha,src.Color);
            end;
        end;
    end;
end;

procedure TGLForm.draw2DProbes(const glmn,glmx,minDepth,maxDepth,range,maxPointDistRatio,dSourceGZ: double; const CSEP0,CSEP1: TCrossSecEndpoint);
var
    prb: TProbe;
    angleLine,angleToPoint, distToPoint: double;
    distToLine,distAlongLine: double;
begin
    for prb in container.scenario.theRA.probes do
    begin
        {check whether the position is between the endpoints}
        if checkRange(prb.Pos.X,smaller(CSEP0.Pos.X,CSEP1.Pos.X),
                                larger(CSEP0.Pos.X,CSEP1.Pos.X)) and
             checkRange(prb.Pos.Y,smaller(CSEP0.Pos.Y,CSEP1.Pos.Y),
                                    larger(CSEP0.Pos.Y,CSEP1.Pos.Y)) then
        begin

            {get distance to the line and distance along the line, for this pos}
            angleLine := arctan2(CSEP1.Pos.Y - CSEP0.Pos.Y,
                                    CSEP1.Pos.X - CSEP0.Pos.X);
            angleToPoint := arctan2(prb.Pos.Y - CSEP0.Pos.Y,
                                    prb.Pos.X - CSEP0.Pos.X);
            distToPoint := hypot(prb.Pos.Y - CSEP0.Pos.Y,prb.Pos.X - CSEP0.Pos.X);

            distToLine      := abs(sin(angleLine - angleToPoint)*distToPoint);
            distAlongLine   := abs(cos(angleLine - angleToPoint)*distToPoint);

            if distToLine < maxPointDistRatio*container.geoSetup.WorldScale then
            begin
                drawSphereAt(XSecCoord(glmn,glmx,distAlongLine/range),
                    XSecCoord(glmx,glmn,(prb.Pos.Z - minDepth)/(maxDepth - minDepth)),
                    dSourceGZ,
                    UWAOpts.SourceSize,prb.Alpha,prb.Color);
            end;
        end;
    end;
end;

procedure TGLForm.draw2DAxesTicks(const glmn,glmx,minDepth,maxDepth,range,dTxtGZ: double);
var
    aMin,aMax: double;
    VerticalTicks, HorizontalTicks: integer;
    i, tmp   : integer;
begin
    {horizontal scale along bottom}
    //HorizontalTicks := (safetrunc(range) div 5);
    aMin := m2f(0,UWAOpts.DispFeet);
    aMax := m2f(range,UWAOpts.DispFeet);
    tmp := safetrunc(power(10,safetrunc(log10(aMax - aMin))));
    if tmp < 1 then
        tmp := 1;
    if (aMax - aMin)/tmp < 3 then
        HorizontalTicks := saferound(tmp*0.5)
    else
        HorizontalTicks := tmp;
    if HorizontalTicks < 1 then
        HorizontalTicks := 1;
    for i := safetrunc(aMin) to safetrunc(aMax) do
        if (i mod HorizontalTicks) = 0 then
        begin
            glPrint(tostr(i),FontSmall,XSecCoord(glmn,glmx,(i - aMin)/(aMax - aMin)),glmn - 0.05,
                dTxtGZ,UWAOpts.GLXSecTextColor);
            glPrint('|',FontSmall,XSecCoord(glmn,glmx,(i - aMin)/(aMax - aMin)),glmn - 0.02,
                dTxtGZ,UWAOpts.GLXSecTextColor);
        end;

    {vertical scale down left side (NB abs, so both height and depth are positive)}
    //VerticalTicks := (safetrunc(maxDepth - minDepth) div 5);
    aMin := m2f(minDepth,UWAOpts.DispFeet);
    aMax := m2f(maxDepth,UWAOpts.DispFeet);
    tmp := safetrunc(power(10,1.0*safetrunc(log10(aMax - aMin))));
    if tmp < 1 then
        tmp := 1;
    if (aMax - aMin)/tmp < 3 then
        VerticalTicks := saferound(tmp*0.5)
    else
        VerticalTicks := tmp;
    if VerticalTicks < 1 then
        VerticalTicks := 1;
    for i := safetrunc(aMin) to safetrunc(aMax) do
        if (i mod VerticalTicks) = 0 then
        begin
            glPrint(tostr(abs(i)),FontSmall,glmn - 0.05,XSecCoord(glmx,glmn,(i - aMin)/(aMax - aMin)),
                dTxtGZ,UWAOpts.GLXSecTextColor);
            glPrint('_',FontSmall,glmn - 0.02,XSecCoord(glmx,glmn,(i - aMin)/(aMax - aMin)),
                dTxtGZ,UWAOpts.GLXSecTextColor);
        end;
end;


procedure TGLForm.draw2DLocationGuide;
const
    zLoc: double = 0.1;
    zLocResults: double = 0.15;
    zLocEndpoints: double = 0.2;

    procedure drawGuideCrossSecEndpoints(const startPoint,endPoint: TCrossSecEndpoint);
    const
        tmpAlpha: double = 1;
        arrowHeadAngle: double = 0.4;
    var
        EPcolor: array [0..1] of TColor;
        linecolor: Tcolor;
        xs,ys: array [0..1] of double;
        i: integer;
        theta: double;
        arrowHeadLength: double;
    begin
        EPcolor[0] := startPoint.Color;
        EPcolor[1] := endPoint.Color;
        linecolor := startPoint.OrigColor;

        xs[0] := container.geoSetup.w2g(startPoint.Pos.X);
        xs[1] := container.geoSetup.w2g(endPoint  .Pos.X);
        ys[0] := container.geoSetup.w2g(startPoint.Pos.Y);
        ys[1] := container.geoSetup.w2g(endPoint  .Pos.Y);

        {draw a line connecting the 2 endpoints}
        glColor4f(getRValue(linecolor)/255, getGValue(linecolor)/255,
            getBValue(linecolor)/255, tmpAlpha);
        glLineWidth(glCSEPBarWidth);
        glBegin(GL_LINES);
            for i := 0 to 1 do
                glVertex3f(XSecGuideX(xs[i]),XSecGuideY(ys[i]),zLocEndpoints);
        GLEnd;

        //arrowhead
        theta := arctan2(ys[1] - ys[0],
                         xs[1] - xs[0]) - PI;
        arrowHeadLength := TMCKLib.Distance2D(xs[0],xs[1],ys[0],ys[1]) * 0.15;
        glBegin(GL_LINES);
            glVertex3f(XSecGuideX(xs[1] + cos(theta + arrowHeadAngle)*arrowHeadLength),
                       XSecGuideY(ys[1] + sin(theta + arrowHeadAngle)*arrowHeadLength),
                       zLocEndpoints);
            glVertex3f(XSecGuideX(xs[1]),XSecGuideY(ys[1]),zLocEndpoints); //the arrow head tip
            glVertex3f(XSecGuideX(xs[1] + cos(theta - arrowHeadAngle)*arrowHeadLength),
                       XSecGuideY(ys[1] + sin(theta - arrowHeadAngle)*arrowHeadLength),
                       zLocEndpoints);
        GLEnd;

        {draw the 2 endpoints}
        for i := 0 to 1 do
            drawSphereAt(XSecGuideX(xs[i]),XSecGuideY(ys[i]),zLocEndpoints,CrossSecGuideEndpointSize,tmpAlpha,EPcolor[i]);
    end;

var
    xf,yf: double;
    LevsRA     : TSingleMatrix;
    wpx, hpx: integer;
    i,j: integer;
    gXs,gYs: TDoubleDynArray;
    ii,jj: integer;
    convertedMax,convertedMin: single;
begin
    if not assigned(container) then exit;
    if not assigned(UWAOpts) then exit;

    xf := -1 + 2*container.bathymetry.xAspectRatio;
    yf := -1 + 2*container.bathymetry.yAspectRatio;

    {check if we need to update 2d texture data}
    if not BathyTextureIsCurrent then
        updateBathymetryTexture;

    //bathymetry texture---------------------------------------
    glBindTexture( GL_TEXTURE_2D, BathyTexture ); // select our current texture
    glEnable(GL_TEXTURE_2D);
    glColor4f(1.0, 1.0, 1.0, 1.0);      // Set the colour white
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(XSecGuideX(-1), XSecGuideY(-1), zLoc);
        glTexCoord2f(1.0, 0.0); glVertex3f(XSecGuideX(xf), XSecGuideY(-1), zLoc);
        glTexCoord2f(1.0, 1.0); glVertex3f(XSecGuideX(xf), XSecGuideY(yf), zLoc);
        glTexCoord2f(0.0, 1.0); glVertex3f(XSecGuideX(-1), XSecGuideY(yf), zLoc);
    glEnd;
    glDisable(GL_TEXTURE_2D);
    glBindTexture( GL_TEXTURE_2D, 0 ); // select our current texture

    //results---------------------------------------------------------
    if container.scenario.resultsExist() then
    begin
        if UWAOpts.levelLimits.maxOrMinNaN() then
            UWAOpts.updateCMaxCMinandLevelColors();

        convertedMin := container.scenario.convertLevel(UWAOpts.levelLimits.min)  + 0.01;
        convertedMax := container.scenario.convertLevel(UWAOpts.levelLimits.max);
        wpx := saferound(ClientWidth  * XSecGuideXw / 2 * container.bathymetry.xAspectRatio);
        hpx := saferound(clientHeight * XSecGuideYw / 2 * container.bathymetry.yAspectRatio);
        setlength(LevsRA,wpx,hpx);
        LevsRA.setAllVal(nan);
        setlength(gXs,wpx);
        setlength(gYs,hpx);
        for i := 0 to wpx - 1 do
        begin
            gXs[i] := XSecGuideX(2*i/(wpx - 1) - 1); //build x locations as we go
            //ii is the effective index into the RA, for this i in LevsRA
            ii := saferound(i / wpx * container.scenario.theRA.iMax*max(container.scenario.theRA.xMax,container.scenario.theRA.yMax)/container.scenario.theRA.xMax);
            if ii < container.scenario.theRA.iMax then
            begin
                for j := 0 to hpx - 1 do
                begin
                    gYs[j] := XSecGuideY(2*j/(hpx - 1) - 1);
                    //get a levels matrix for the much reduced set of points given by the guide pixels
                    jj := saferound(j / hpx * container.scenario.theRA.jMax*max(container.scenario.theRA.xMax,container.scenario.theRA.yMax)/container.scenario.theRA.yMax);
                    if jj < container.scenario.theRA.jMax then
                    begin
                        LevsRA[i,j] := container.scenario.theRA.getLevAtInds(UWAOpts.depthViewInd,ii,jj,self.resultsMovingPosIndForGIFTool,
                                                                             UWAOpts.LevelsDisplay,
                                                                             true);

                        if not isNaN(LevsRA[i,j]) then
                        begin
                            if LevsRA[i,j] < lowLev then
                                LevsRA[i,j] := nan
                            else
                            if not UWAOpts.levelLimits.MaxOrMinNaN then
                            begin
                                LevsRA[i,j] := container.scenario.convertLevel(LevsRA[i,j]);
                                if not UWAOpts.levelLimits.showExclusionZone then
                                    forceRange(LevsRA[i,j],convertedMin,convertedMax)
                                else if LevsRA[i,j] > convertedMax then
                                    LevsRA[i,j] := convertedMax;
                            end; //lowlev
                        end; //insan
                    end;//jj
                end; //j
            end; //ii
        end; //i

        draw2DLocationGuideResults(gXs,gYs,zLocResults,LevsRA,UWAOpts.resultsColorMap,UWAOpts.ResultsAlpha);
    end;

    //endpoints--------------------------------------------------------
    if assigned(container) then
        drawGuideCrossSecEndpoints(container.crossSec.endpoints[0],container.crossSec.endpoints[1]);
end;

procedure TGLForm.draw2DLocationGuideResults(const gXs,gYs: TDoubleDynArray; const zs: double; const maResults: TSingleMatrix; const ColorMap: TMatrix; const alphaResults: double);
{draw results as smoothed colours, ie not with the marching squares algorithm}
var
    hrcm,li,lj,i,j     : integer;
    LRA                : TMatrix;
    convertedMax,convertedMin: single;
begin
    hrcm := high(Colormap);

    li := length(gXs);  //getting the size of 2d array
    lj := length(gYs);

    convertedMin := container.scenario.convertLevel(UWAOpts.levelLimits.min);
    convertedMax := container.scenario.convertLevel(UWAOpts.levelLimits.max);

    {generate a matrix LRA containing lookup values (into the colormap)
    for each drawing matrix position.}
    setlength(LRA,li,lj);
    for i := 0 to li - 1 do
        for j := 0 to lj - 1 do
        begin
            if (not isNaN(maResults[i,j])) and (maResults[i,j] > lowLev) then
            begin
                LRA[i,j] := hrcm*(convertedMax - maResults[i,j])/(convertedMax - convertedMin);
                forceRange(LRA[i,j],0,hrcm);
            end
            else
                LRA[i,j] := NaN;
        end;//i,j

    for i := 0 to li - 2 do  //to li - 2 because our tris span i to i+1
    begin
        glBegin(GL_TRIANGLES);
        for j := 0 to lj - 2 do
        begin
            if not(isnan(LRA[i,j]) or isnan(LRA[i+1,j]) or isnan(LRA[i,j+1])) then
            begin
                glColor4f(ColorMap[safetrunc(LRA[i  ,j  ]),0],ColorMap[safetrunc(LRA[i  ,j  ]),1],ColorMap[safetrunc(LRA[i  ,j  ]),2], alphaResults);
                glVertex3f(gXs[i  ],gYs[j  ],zs);
                glColor4f(ColorMap[safetrunc(LRA[i+1,j  ]),0],ColorMap[safetrunc(LRA[i+1,j  ]),1],ColorMap[safetrunc(LRA[i+1,j  ]),2], alphaResults);
                glVertex3f(gXs[i+1],gYs[j  ],zs);
                glColor4f(ColorMap[safetrunc(LRA[i  ,j+1]),0],ColorMap[safetrunc(LRA[i  ,j+1]),1],ColorMap[safetrunc(LRA[i  ,j+1]),2], alphaResults);
                glVertex3f(gXs[i  ],gYs[j+1],zs);
            end;

            if not(isnan(LRA[i+1,j]) or isnan(LRA[i,j+1]) or isnan(LRA[i+1,j+1])) then
            begin
                glColor4f(ColorMap[safetrunc(LRA[i+1,j+1]),0],ColorMap[safetrunc(LRA[i+1,j+1]),1],ColorMap[safetrunc(LRA[i+1,j+1]),2], alphaResults);
                glVertex3f(gXs[i+1],gYs[j+1],zs);
                glColor4f(ColorMap[safetrunc(LRA[i+1,j  ]),0],ColorMap[safetrunc(LRA[i+1,j  ]),1],ColorMap[safetrunc(LRA[i+1,j  ]),2], alphaResults);
                glVertex3f(gXs[i+1],gYs[j  ],zs);
                glColor4f(ColorMap[safetrunc(LRA[i  ,j+1]),0],ColorMap[safetrunc(LRA[i  ,j+1]),1],ColorMap[safetrunc(LRA[i  ,j+1]),2], alphaResults);
                glVertex3f(gXs[i  ],gYs[j+1],zs);
            end;
        end;
        glEnd();
    end;//for i
end;

procedure TGLForm.drawSetWorldScale;
var
    tmpVis: TBooleanDynArray;
    i: integer;
    xsec: ISmart<TCrossSec>;
    tmpP: TPoint;
    tmpfP1, tmpfP2: TFPoint;
begin
    if not (length(UWAOpts.componentVisibility) - 1) = integer(high(TVisibilityEnum)) then
    begin
        raise Exception.Create(rsIncorrectLengthOfComponentVisibilityArray);
        Exit;
    end;

    setlength(tmpVis,length(UWAOpts.ComponentVisibility));
    for i := 0 to length(tmpVis) - 1 do
        tmpVis[i] := UWAOpts.ComponentVisibility[i];
    tmpVis[ord(veResults)] := false;
    tmpVis[ord(veContours)] := false;
    tmpVis[ord(veEndpoints)] := false;

    draw3D(tmpVis);

    {use the same code used to draw cross sec endpoints,
     to draw the worldscale endpoints}
    xsec := TSmart<TCrossSec>.create(TCrossSec.Create);
    if clicksCollected = 0 then
        drawMousePos(state.mouse.X,state.mouse.Y)
    else
    begin
        try
            tmpP.X := scaleClicks[TClickInd.kStart].X;
            tmpP.Y := scaleClicks[TClickInd.kStart].Y;
            tmpfP1 := screenToWorldFloat(tmpP);
            tmpP.X := state.mouse.X;
            tmpP.Y := state.mouse.Y;
            tmpfP2 := screenToWorldFloat(tmpP);
            if xsec.setPos(tmpfP1.X,tmpfP1.Y,tmpfP2.X,tmpfP2.Y) then
                self.setRedraw;
            if xsec.endpoints[0].setColor(clWhite) or
               xsec.endpoints[1].setColor(clWhite) then
                self.setRedraw;
            draw3DCrossSecGuide(xsec);
            drawMousePos(state.mouse.X,state.mouse.Y,xsec.dx,xsec.dy);
        except
        end;
    end;
end;


procedure TGLForm.draw3DExclusionZones(const drawFill, drawContour: boolean);
var
    bkColor: array [0..3] of GLfloat;
    src: TSource;
    i,p: integer;
    zs: double;
    iZInd: integer;
    aExclusionZonePoints: TExclusionZonePointArray;
begin
    if not bEXTFramebufferObjectExists then
    begin
        showmessage(WrapText(rsWrongGLVersion));
        Exit;
    end;

    //we are going to draw the exclusion zone into a frame buffer object, render, and pull it out as a texture
    //which we can then draw

    iZInd := UWAOpts.depthViewInd;//get the z index we are plotting
    if iZInd >= container.scenario.dMax then Exit;
    zs := UWAOpts.ZFactor*(container.scenario.depths(iZInd) - 0.66) / container.bathymetry.zMax;

    glPushMatrix;

    glGetFloatv(GL_COLOR_CLEAR_VALUE, @bkColor); //store previous clear color

    glBindFramebufferEXT(GL_FRAMEBUFFER, fboId); //the correct texture is already attached to this fbo
    glClearColor(0.0, 0.0, 0.0, 0.0);   // no Background

    glBindTexture(GL_TEXTURE_2D, 0);
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT); {clear buffers}
    //glMatrixMode(GL_PROJECTION);
    glLoadIdentity;          // Reset The View
    glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();

    glDisable(GL_POLYGON_OFFSET_FILL);

    //no need for push-pop matrix here as we will draw immediately after the translation
    glScalef(renderTextureSize/ClientWidth,
             renderTextureSize/clientHeight,
             1);
    glTranslatef((renderTextureSize - ClientWidth )/renderTextureSize,
                 (renderTextureSize - clientHeight)/renderTextureSize,
                 1);

    for src in container.scenario.sources do
        if src.SrcSolved then
        begin
            setlength(aExclusionZonePoints,src.NSlices);

            for p := 0 to src.getMovingPosMax - 1 do {loop through for each source position, if it is moving, or just static pos if not}
            begin
                if src.isMovingSource and (self.resultsMovingPosIndForGIFTool >= 0) and (self.resultsMovingPosIndForGIFTool <> p) then continue;

                {get a local temp version of the src exclusion zone, to check against theRA limits}
                aExclusionZonePoints := container.scenario.exclusionZoneAtPos(UWAOpts.levelLimits.exclusionZoneLevel,src,p,self.resultsMovingPosIndForGIFTool,UWAOpts.exclusionZoneStopAtLand);
                for i := 0 to src.NSlices - 1 do
                begin
                    forceRange(aExclusionZonePoints[i].p.X,0,container.scenario.theRA.xMax);
                    forceRange(aExclusionZonePoints[i].p.Y,0,container.scenario.theRA.yMax);
                end;

                glColor4f(getRValue(UWAOpts.ExclusionZoneColor)/255,
                          getGValue(UWAOpts.ExclusionZoneColor)/255,
                          getBValue(UWAOpts.ExclusionZoneColor)/255,
                          1.0); //alpha is 1 here. later we set to the correct alpha value

                {draw the overall polygon as a series of triangles radiating out from the source.
                This will be a bit slow, but causes no errors, and we only need to do this once
                to generate the texture, so there is not much of a performance hit. NB z location
                is not important, as long as it is in the clipping range. we are looking
                with birdseye view when generating the texture.}
                if drawFill then
                begin
                    for i := 0 to src.NSlices - 1 do
                    begin
                        glBegin(GL_POLYGON);
                            glVertex3f(container.geoSetup.w2g(aExclusionZonePoints[i].p.X),
                                       container.geoSetup.w2g(aExclusionZonePoints[i].p.Y),
                                       -0.5);
                            glVertex3f(container.geoSetup.w2g(aExclusionZonePoints[integerWrap(i + 1,src.NSlices - 1)].p.X),
                                       container.geoSetup.w2g(aExclusionZonePoints[integerWrap(i + 1,src.NSlices - 1)].p.Y),
                                       -0.5);
                            glVertex3f(container.geoSetup.w2g(src.getMovingPos(p).X),
                                       container.geoSetup.w2g(src.getMovingPos(p).Y),
                                       -0.5);
                        glEnd;
                    end; //nslices
                end;

                {lines joining perimeter. NB this needs to be improved on for moving sources and multiple sources.}
                if drawContour then
                begin
                    glColor4f(getRValue(UWAOpts.ExclusionZoneColor)/255*UWAOpts.resultsContoursDarkening,
                              getGValue(UWAOpts.ExclusionZoneColor)/255*UWAOpts.resultsContoursDarkening,
                              getBValue(UWAOpts.ExclusionZoneColor)/255*UWAOpts.resultsContoursDarkening,
                              1.0);
                    glLineWidth( UWAOpts.resultsContoursWidth );
                    glBegin(GL_LINE_LOOP);
                        for i := 0 to src.NSlices - 1 do
                            glVertex3f(container.geoSetup.w2g(aExclusionZonePoints[i].p.X),
                                       container.geoSetup.w2g(aExclusionZonePoints[i].p.Y),
                                       -0.3);
                    glEnd;
                end;
            end; //p
        end; //srcSolved

    glFinish;

    glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, renderTextureSize, renderTextureSize, 0);

    glBindFramebufferEXT(GL_FRAMEBUFFER, 0);// unbind FBO, go back to window provided

    glPopMatrix;

    glBindTexture(GL_TEXTURE_2D, RenderTexture);
    glEnable(GL_TEXTURE_2D);
    glColor4f(1.0, 1.0, 1.0, UWAOpts.ResultsAlpha);      // Set the colour white, but with the correct alpha
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(-1, -1, zs + glRenderTextureZfix);
        glTexCoord2f(1.0, 0.0); glVertex3f( 1, -1, zs + glRenderTextureZfix);
        glTexCoord2f(1.0, 1.0); glVertex3f( 1,  1, zs + glRenderTextureZfix);
        glTexCoord2f(0.0, 1.0); glVertex3f(-1,  1, zs + glRenderTextureZfix);
    glEnd;
    glDisable(GL_TEXTURE_2D);
    glBindTexture( GL_TEXTURE_2D, 0 ); // select our current texture

    glRenderTextureZfix := glRenderTextureZfix + 0.01;

    glClearColor(bkColor[0],bkColor[1],bkColor[2],bkColor[3]);  //reset previous clear color
end;

{$ENDREGION}


procedure TGLForm.setSettingOverlayScale(const makeActive: boolean);
begin
    self.options.bSettingImageOverlay := makeActive;
    self.setRedrawBathymetry;
end;

procedure TGLForm.setSourceProbeText(const s: string; const wx,wy: double);
begin
    sSourceProbeText := s;

    {set the time this text was set}
    lwSourceProbeTextSetTime := timeGetTime;

    {set location to show text - inputs are expected as 'world', which we convert to GL}
    dSourceProbeTextGX := container.geoSetup.w2g(wx) + dGLTextXOffset;
    dSourceProbeTextGY := container.geoSetup.w2g(wy) + dGLTextYOffset;
    dSourceProbeTextGZ := 0.05;
    options.showSourceProbeText := true;

    THelper.asyncWait(SourceProbeTextTimeout,procedure begin
        self.clearSourceProbeText;
        self.forceDrawNow;
    end);

    setRedraw;
end;

procedure TGLForm.clearImageOverlayTexture;
begin
    if glIsTexture(imageOverlayTexture) then
        glDeleteTextures(1, @imageOverlayTexture);
end;

procedure TGLForm.clearSourceProbeText;
begin
    options.showSourceProbeText := false;
    setRedraw;
end;

procedure TGLForm.setDrawSourceThresholds();
begin
    options.fShowSourceThreshold        := true;
    setRedraw;
end;

procedure TGLForm.clearSourceThreshold;
begin
    options.fShowSourceThreshold := false;
    setRedraw;
end;

procedure TGLForm.initialSpinAnimation;
{an nice spin animation of the current GL win, used after loading the program}
const
    waitMS: cardinal = 50; //min time between frames (may be slower due to render speed)
    imax: integer = 20;  //number of frames
var
    tmpSpinX: double;
    tmpspinY: double;              // rotation Y
    tmppanX: double;           // panning X
    tmppanY: double;           // panning Y
    tmpzoom: double;      //zoom factor
    i: integer;
    sysTime: LongWord;
    p: TPoint3d;
begin
    //bCanShowProgressForm    := false;
    //try
    tmpSpinX := glRot.x;              // rotation X
    tmpspinY := glRot.y;              // rotation Y
    tmppanX := glPan.x;           // panning X
    tmppanY := glPan.y;           // panning Y
    tmpzoom := glZoom;      //zoom factor

    glQuaternion := TQuaternion3D.Identity;

    {go from everything at zero to the stored values in imax steps}
    sysTime := 0;
    for i := 0 to imax do
    begin
        glRot.x := tmpSpinX*i/imax;
        glRot.y := tmpspinY*i/imax;
        p.x := glRot.x / 180 * pi;
        p.y := glRot.y / 180 * pi;
        glQuaternion := TQuaternionFuncs.fromEulerAngles(p);

        glPan.x := tmppanX*i/imax;
        glPan.y := tmppanY*i/imax;
        glZoom := tmpzoom*i/imax;

        {check if we need to wait, to slow animation}
        if i > 0 then
            if (timeGetTime - sysTime) < waitMS then
                sleep(waitMS - (timeGetTime - sysTime));

        sysTime := timeGetTime;

        drawnow;
    end;
    //finally
        //bCanShowProgressForm    := true;
    //end;
end;

procedure TGLForm.resetView;
begin
    glQuaternion := TQuaternion3D.Identity;
    glRot.x := 0.0;
    glRot.y := 0.0;
    glPan.x := 0.0;
    glPan.y := 0.0;
    glZoom := -10; //zoom out a little so we can see axes text
end;

procedure TGLForm.setRedraw;
begin
    state.bDrawNowNeeded := true;
end;

procedure TGLForm.setRedrawResults;
begin
    state.bDrawNowNeeded := true;
    state.bResultsListReady := false;
end;

procedure TGLForm.setRedrawBathymetry;
begin
    state.bDrawNowNeeded := true;
    state.bBathymetryListReady := false;
end;

function TGLForm.get2ClicksForSetScale(): TFPointScaleClicks;
var
    i: integer;
begin
    options.bSettingWorldScale  := true;
    try
        {Setting from 2 clicked points}

        {make sure we are not in xsec view - could cause an error}
        fbDrawCrossSection := false;
        options.bSettingWorldScaleCancelled := false;
        clicksCollected     := 0;

        {change the onmousedown event}
        OnMouseDown    := FormMouseDownSetScale;
        try
            showmessage(WrapText(rsSetScaleInstructions));
            resetView;
            drawNow;

            i := 0;
            while (clicksCollected < 2) and options.bSettingWorldScale do
                application.processMessages;//sleep(100);//showStatusbarTextSpinningAnimation(i);
        finally
            {put onmousedown back to normal}
            OnMouseDown := FormMouseDown;
        end;

        {check if operation was cancelled}
        if options.bSettingWorldScaleCancelled then Exit;

        result[TClickInd.kStart] := screenToWorldFloat(ScaleClicks[TClickInd.kStart]);
        result[TClickInd.kEnd] := screenToWorldFloat(ScaleClicks[TClickInd.kEnd]);
    finally
        options.bSettingWorldScale := false;
    end;
end;

procedure TGLForm.SetRuler;
var
    i: integer;
begin
    {make sure we are not in xsec view - could cause an error}
    fbDrawCrossSection := false;

    {set flags}
    options.bSettingRuler := true;
    options.bSettingWorldScaleCancelled := false;
    clicksCollected     := 0;
    try
        {change the onmousedown event}
        OnMouseDown    := FormMouseDownSetScale;
        try
            resetView;
            drawNow;

            i := 0;
            while (clicksCollected < 2) and options.bSettingRuler do
                application.processMessages;//sleep(100);//showStatusbarTextSpinningAnimation(i);
        finally
            {put onmousedown back to normal}
            OnMouseDown := FormMouseDown;
        end;

        {check if operation was cancelled}
        if options.bSettingWorldScaleCancelled then Exit;

    finally
        options.bSettingRuler := false;
        forcedrawNow;
    end;
end;

procedure TGLForm.setDrawCrossSection(const aBool: boolean);
begin
    if aBool <> bDrawCrossSection then
    begin
        fbDrawCrossSection := aBool;
        state.bDrawNowNeeded := true;
    end;
end;

procedure TGLForm.checkDrawTime;
begin
    if state.canShowProgressForm then
        if (timegettime - glDrawBeginTime) > timeToWaitBeforeShowDrawProgress then
            if (timegettime - glProgressLastHide) > timeToWaitBeforeReShowDrawProgress then
            begin
                glDrawProgressForm.Show;
                application.ProcessMessages;
            end;
end;

procedure TGLForm.rotateBy(x,y: double);
begin
    glRot.x := glRot.x + x;
    glRot.y := glRot.y + y;
    force2DrawNow;
end;


{$REGION 'marching_tris_squares'}
    procedure TGLForm.drawMarchingTrisLines(const miMSLines: TIntPairMatrix; const xs,ys: TDoubleDynArray; const zs: double);
    var
        ps: TPosPairList;
        p: TPosPair;
    begin
        ps := TMarchingSquares.drawMarchingTrisLines(miMSLines,xs,ys);
        try
            for p in ps do
            begin
                glBegin(GL_LINES);
                glVertex3f(p.p1.x,p.p1.y,zs);
                glVertex3f(p.p2.x,p.p2.y,zs);
                glEnd;
            end;
        finally
            ps.free;
        end;
    end;

    procedure TGLForm.drawMarchingTrisBands(const miMSBands: TIntPairMatrix; const xs,ys: TDoubleDynArray; const zs: double);
    const
        {cc contains all the vertices needed when drawing the bands.
        [a,b,c] :
        a is used for first or second triangle
        b is the indices around the 2-unit square laid out like the
        keys on a calculator (and then rotated for the second triangle)
        c is whether we want the x or y coordinate}
        cc: array[0..1,1..9,0..1] of integer = (((0,0),(1,0),(2,0),
                                                    (0,1),(1,1),(2,1),
                                                    (0,2),(1,2),(2,2)),
                                                    ((2,2),(1,2),(0,2),
                                                    (2,1),(1,1),(0,1),
                                                    (2,0),(1,0),(0,0)));
    var
        i,j,leni,lenj: integer;
        i2,j2: TIntegerDynArray;
        q: integer;
    begin
        {each square is made up of 2 triangles
        first triangle :
        4
        | \
        1- 2
        second triangle :
        2 - 1
          \ |
            4
        }

        leni := length(miMSBands);
        lenj := length(miMSBands[0]);
        {create 2*i and 2*j vectors for later quicker accessing.}
        setlength(i2,leni);
        for i := 0 to leni - 1 do
            i2[i] := 2*i;
        setlength(j2,lenj);
        for j := 0 to lenj - 1 do
            j2[j] := 2*j;

        for i := 0 to leni - 2 do
            for j := 0 to lenj - 1 do
            begin
                {this is the marching squares algorithm drawing part.
                based on the values we got from our trit corners of
                each square, choose where to draw lines. (NB my y (j)
                direction is reversed to the normal sense)
                do nothing for 0 and 80, ie all true or all false.
                drawing is with triangle strips, and takes a bit of
                thinking about how to get it to draw sometimes}

                for q := 0 to 1 do
                begin
                    glBegin(GL_TRIANGLE_STRIP);
                    case miMSBands[i,j,q] of
                        13: begin  {3x1}
                            glVertex3f(xs[i2[i] + cc[q,1,0]],ys[j2[j] + cc[q,1,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,3,0]],ys[j2[j] + cc[q,3,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,7,0]],ys[j2[j] + cc[q,7,1]],zs);
                        end;
                        {-------------------------------------}
                        {1x1 and 2x0 or 2x2}
                        9,17: begin //4 corner (7 corner)
                            glVertex3f(xs[i2[i] + cc[q,7,0]],ys[j2[j] + cc[q,7,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]],zs);
                        end;
                        3,23: begin //2 corner (3 corner)
                            glVertex3f(xs[i2[i] + cc[q,3,0]],ys[j2[j] + cc[q,3,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]],zs);
                        end;
                        1,25: begin //1 corner (1 corner)
                            glVertex3f(xs[i2[i] + cc[q,1,0]],ys[j2[j] + cc[q,1,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],zs);
                        end;
                        {-------------------------------------}
                        {2x1}
                        4,22: begin // not 4 corner (not 7 corner)
                            glVertex3f(xs[i2[i] + cc[q,1,0]],ys[j2[j] + cc[q,1,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,3,0]],ys[j2[j] + cc[q,3,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]],zs);
                        end;
                        10,16: begin // not 2 corner (not 3 corner)
                            glVertex3f(xs[i2[i] + cc[q,1,0]],ys[j2[j] + cc[q,1,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,7,0]],ys[j2[j] + cc[q,7,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]],zs);
                        end;
                        12,14: begin // not 1 corner (not 1 corner)
                            glVertex3f(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,7,0]],ys[j2[j] + cc[q,7,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,3,0]],ys[j2[j] + cc[q,3,1]],zs);
                        end;
                        {-------------------------------------}
                        {1x1 and 1x0 and 1x2}
                        11,15: begin // 4 corner (7 corner)
                            glVertex3f(xs[i2[i] + cc[q,7,0]],ys[j2[j] + cc[q,7,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],zs);
                        end;
                        5,21: begin // 2 corner (3 corner)
                            glVertex3f(xs[i2[i] + cc[q,3,0]],ys[j2[j] + cc[q,3,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],zs);
                        end;
                        7,19: begin // 1 corner (1 corner)
                            glVertex3f(xs[i2[i] + cc[q,1,0]],ys[j2[j] + cc[q,1,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],zs);
                            glVertex3f(xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]],zs);
                        end;

                        {there are 3 leftover possibilities that are not covered,
                        however they are unusual and not likely to cause
                        a big problem. they each have a mix of 0 and 2. and doing them
                        properly would mean having more xs and ys set between the current points}
                    end;//case
                    glEnd;
                end;  //q
            end;//i,j
    end;

    procedure TGLForm.drawMarchingSquaresLines(const miMSLines: TIntegerMatrix; const xs,ys: TDoubleDynArray; const zs: double);
    var
        ps: TPosPairList;
        p: TPosPair;
    begin
        ps := TMarchingSquares.generateMarchingSquaresLines(miMSLines,xs,ys);
        try
            for p in ps do
            begin
                glBegin(GL_LINES);
                glVertex3f(p.p1.x,p.p1.y,zs);
                glVertex3f(p.p2.x,p.p2.y,zs);
                glEnd;
            end;
        finally
            ps.free;
        end;
    end;

    procedure TGLForm.drawMarchingSquaresBands(const miMSBands: TIntegerMatrix; const xs,ys: TDoubleDynArray; const zs: double);
    var
        i,j: integer;
    begin
        for i := 0 to high(miMSBands) do
            for j := 0 to high(miMSBands[0]) do
            begin
                glBegin(GL_TRIANGLE_STRIP);

                {this is the marching squares algorithm drawing part.
                based on the values we got from our trit corners of
                each square, choose where to draw lines. (NB my y (j)
                direction is reversed to the normal sense)
                do nothing for 0 and 80, ie all true or all false.
                drawing is with triangle strips, and takes a bit of
                thinking about how to get it to draw sometimes - you have to
                zig zag your way across the vertices}
                case miMSBands[i,j] of
                    0,80: ;    //do nothing
                    40: begin  //full square  1111
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;
                    {-------------------------------------}
                    {wikipedia 'single rectangle'}
                    {2 of 4 in range}
                    36,44: begin //bottom half  1100 1122
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                    end;
                    4,76: begin //top half     0011 2211
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;
                    12,68: begin //right half  0110 2112
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;
                    28,52: begin //left half   1001 1221
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                    end;
                    {0022, 0220, 2002, 2200}
                    8,24,56,72: ; //do nothing
                    {-------------------------------------}
                    {wikipedia 'single triangle'}
                    {1 of 4 in range}
                    27,53: begin //upper left corner 1000 1222
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                    end;
                    9,71: begin //upper right corner 0100 2122
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                    end;
                    3,77: begin //lower right corner 0010 2212
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                    end;
                    1,79: begin //lower left corner 0001 2221
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                    end;
                    {-------------------------------------}
                    {wikipedia 'single pentagon' top 2 rows}
                    {3 of 4 in range}
                    37,43: begin   //   1101 1121
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                    end;
                    31,49: begin  //  1011 1211
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;
                    13,67: begin        //  0111 2111
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                    end;
                    39,41: begin  //  1110 1112
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                    end;


                    {middle 2 rows}
                    35,45: begin //top left wedge going right 1022 1200
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                    end;
                    15,65: begin // top right wedge going down 0120 2102
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                    end;
                    5,75: begin // bottom right wedge going left 0012 2210
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;
                    25,55: begin // bottom left wedge going up  2001 0221
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                    end;

                    {bottom 2 rows}
                    29,51: begin //top left wedge going down 1002 1220
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                    end;
                    17,63: begin // top right wedge going left 2100 0122
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                    end;
                    21,59: begin // bottom right wedge going up 0210 2012
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;
                    7,73: begin // bottom left wedge going right 0021 2201
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                    end;

                    {-------------------------------------------}
                    {wikipedia 'single hexagon'}

                    22,58: begin  //2 bottom in range 0211 2011
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                    end;
                    14,66: begin  //2 right in range 0112 2110
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                    end;
                    38,42: begin  //2 top in range 1102 1120
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                    end;
                    34,46: begin  //2 left in range 1021 1201
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                    end;

                    {hexagons going diagonally. these also include some of the cases from
                    what are entered under the 'saddles' entries on the wikipedia page.}
                    16,64,10,70: begin  //bot left, top right 0121 2101, 0101,2121
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                    end;
                    32,48,30,50: begin  //bot right, top left 1012 1210, 1010,1212
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;

                    {-------------------------------------------}
                    {wikipedia 'saddles'
                    NB I've had to make some choices here as to which I think are the most
                    likely out of 2 options. The proper way to do it, would be to get the
                    centre value between the 4 corners (which we don't currently have) so
                    we know which option is correct.}

                    11,69: begin  //heptagon with top right corner 0102 2120
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 0],zs);
                    end;
                    33,47: begin  //heptagon with top left corner 1020 1202
                        glVertex3f(xs[2*i + 0],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                    end;
                    19,61: begin  //heptagon with bottom left corner 0201 2021
                        glVertex3f(xs[2*i + 0],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                    end;
                    23,57: begin  //heptagon with bottom right corner 0212 2010
                        glVertex3f(xs[2*i + 0],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 0],zs);
                        glVertex3f(xs[2*i + 1],ys[2*j + 2],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 1],zs);
                        glVertex3f(xs[2*i + 2],ys[2*j + 2],zs);
                    end;

                end; //case MarchingSquares

                glEnd;
            end;//i,j
    end;

    class function TGLForm.getMarchingSquaresXorYGrid(const li: integer; const amin, amax: double): TDoubleDynArray;
    var
        i: integer;
    begin
        {xs and ys are different in this algorithm, as we need to
        have an extra point between current points}
        setlength(result,2*(li - 1) + 1);
        for i := 0 to 2*(li - 1) do
            result[i] := container.geoSetup.w2g(amin + (amax - amin)*i/(2*(li - 1)));
    end;

function TGLForm.getSettingOverlayScale: boolean;
begin
    result := self.options.bSettingImageOverlay;
end;

function TGLForm.getShowSourceThreshold: boolean;
begin
    result := self.options.fShowSourceThreshold;
end;

{$ENDREGION}

procedure TGLForm.updateLastGLValues;
begin
    state.bDrawNowNeeded := false; //don't redraw, unless something changes
    lastGLSpins := glRot;
    lastGLPan := glPan;
    lastGLZoom := glZoom;
end;

procedure TGLForm.aeDetectKeypressMessage(var Msg: tagMSG; var Handled: Boolean);
var
    src: TSource;
    srcIndex: integer;
    floatPos: TFPoint;
    shouldRedrawGL: boolean;
begin
    //detect key presses

    case Msg.message of
        WM_KEYDOWN: begin
            //13 = enter key
            if (Msg.wParam = 13) and isBirdsEyeView and container.CrossSecReady(shouldRedrawGL) and UWAOpts.componentVisibility[ord(veEndpoints)] then
            begin
                if assigned(UWAOpts) then
                    UWAOpts.setVisibility(ord(veEndpoints),false); {we no longer need to show the endpoints}

                self.bDrawCrossSection := true;
                self.didChangeVisibility(ord(veEndpoints));
                handled := true;
                self.forceDrawNow();
            end;

            //27 = escape key. stop any setworldscale/setruler operation
            if (Msg.wParam = 27) and isBirdsEyeView and UWAOpts.componentVisibility[ord(veEndpoints)] then
            begin
                options.bSettingWorldScaleCancelled := true;
                options.bSettingWorldScale := false; //break out of setting world scale loop
                options.bSettingRuler := false;

                if assigned(UWAOpts) then
                    UWAOpts.setVisibility(ord(veEndpoints),false); {we no longer need to show the endpoints}
                self.didChangeVisibility(ord(veEndpoints));
                handled := true;
                forceDrawNow;
            end;

            //numerical keys, try to match to a source from container.scenario.sources and use that as the first point
            //for setworldscale/setruler/move crosssecendpoints
            if (Msg.wParam >= 49) and (Msg.wParam <= 57) and isBirdsEyeView and UWAOpts.componentVisibility[ord(veEndpoints)] then
            begin
                srcIndex := Msg.wParam - 49; //get source index (zero indexed)
                if srcIndex < container.scenario.sources.Count then
                begin
                    src := container.scenario.sources[srcIndex];

                    if (options.bSettingRuler or options.bSettingWorldScale) and ((clicksCollected = 0) or (clicksCollected = 1)) then
                    begin
                        floatPos.X := src.Pos.X;
                        floatPos.Y := src.Pos.Y;
                        ScaleClicks[TClickInd.kStart] := WorldToScreenFloat(floatPos);
                        if (clicksCollected = 0) then
                            inc(clicksCollected);
                    end
                    else
                    if UWAOpts.ComponentVisibility[ord(veEndpoints)] then
                    begin
                        container.crossSec.endpoints[0].setPos(src.Pos.X,src.Pos.Y);
                    end;
                end;

                handled := true;
                force2DrawNow;
            end;
        end;
    end;
end;

procedure TGLForm.didChangeVisibility(const ind: integer);
begin
    {for things that are drawn with a displaylist, we need to
    specifically set those to redraw}
    if (ind = ord(veResults)) or (ind = ord(veContours)) then
        setRedrawResults();
    if (ind = ord(veBathymetry)) then
        setRedrawBathymetry();

    forceDrawNow();
end;

procedure TGLForm.BMPForMovingPosInd(const p: integer; const bmp: TBitmap);
begin
    self.resultsMovingPosIndForGIFTool := p;
    self.setRedrawResults;
    self.force2DrawNow;
    self.getExportBMP(bmp, true);
end;

{$REGION 'gl_fonts'}
function TGLForm.BuildFont(hdc: HDC; fontHeight: integer): GLUint;
{create a font, for displaying text directly in the openGL window. Return the list base for the given font}
var
    Font: HFONT;
begin
    result := glGenLists(iGLFontChars);                   // make call lists for iGLFontChars characters
    font := CreateFont(-fontHeight,                       // height (-ve integer means font size in pt)
                      0,                                  // Width
                      0,                                  // The angle of escapement
                      0,                                  // angle of orientation
                      FW_NORMAL,                          // font weight
                      0,                                  // italics
                      0,                                  // underline
                      0,                                  // strikethrough
                      ANSI_CHARSET,                       // character Set
                      OUT_TT_PRECIS,                      // Output precision (TrueType)
                      CLIP_DEFAULT_PRECIS,                // clipping precision
                      ANTIALIASED_QUALITY,                // The output quality
                      FF_DONTCARE or DEFAULT_PITCH,       // Family and pitch
                      PWideChar(sGLFontName));            // font name
    SelectObject(hdc,font);                               // Selecting fonts to DC
    wglUseFontBitmaps(hdc,32,iGLFontChars,result);        // Generates iGLFontChars characters, beginning at 32 in Ascii
end;

procedure TGLForm.KillFont(theFont: GLUint);
{delete the font used in openGL window}
begin
    glDeleteLists(theFont,iGLFontChars);                                 //Deletes all 96 characters (display lists)
end;

function TGLForm.glPrint(const text: string; const theFont: GLUint; const x,y,z: double; const color: TColor): integer;
{display text directly in openGL window.
as gl expects an ansiString (c-style) we need to first cast it
to ansistring. otherwise it hits the 00 at the first unicode character and stops.}
var
    ansitext: AnsiString;
    //params: array[0..3] of GLfloat;
    iparams: array[0..3] of GLint;
begin
    if text = '' then exit(0);

    glColor4f(getRValue(color)/255,
              getGValue(color)/255,
              getBValue(color)/255,
              1);

    glRasterPos3f(x,y,z);

    //glGetFloatv(GL_CURRENT_RASTER_POSITION,@params[0]);
    glGetIntegerv(GL_CURRENT_RASTER_POSITION,@iparams[0]);
    result := -iparams[0];

    ansiText := ansistring(text);  //cast to ansistring                               //
    glPushAttrib(GL_LIST_BIT);                              // Saves the current state of display lists
    glListBase(theFont - 32);
    glCallLists(length(ansiText),GL_UNSIGNED_BYTE,Pansichar(ansiText)); // Draw the display lists
    glPopAttrib;                                            // Restore the original status display lists

    //glGetFloatv(GL_CURRENT_RASTER_POSITION,@params[0]);
    glGetIntegerv(GL_CURRENT_RASTER_POSITION,@iparams[0]);
    result := result + iparams[0];
end;

procedure TGLForm.glPrint90degrees(const s: string; const theFont: GLUint; const x,y,z: double; const aCol: TColor);
var
    bkColor: array [0..3] of GLfloat;
    xPxDist, yPxDist: integer;
    a,b: double;
begin
    glGetFloatv(GL_COLOR_CLEAR_VALUE, @bkColor); //store previous clear color

    glPushMatrix;

        glBindFramebufferEXT(GL_FRAMEBUFFER, fboId); //the correct texture is already attached to this fbo
        glClearColor(0.0, 0.0, 0.0, 0.0);   // no Background

        glBindTexture(GL_TEXTURE_2D, 0);
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT); {clear buffers}

        glLoadIdentity;          // Reset The View
        glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);

        xPxDist := glPrint(s,theFont,0.0,0.0,0,aCol);
        ypxDist := 12;

        glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, xPxDist, yPxDist, 0);

        glBindFramebufferEXT(GL_FRAMEBUFFER, 0);// unbind FBO, go back to window provided

    glPopMatrix;

    a := yPxDist/renderTextureSize;
    b := xPxDist/renderTextureSize;

    glBindTexture(GL_TEXTURE_2D, RenderTexture);
    glEnable(GL_TEXTURE_2D);
    glColor4f(1.0, 1.0, 1.0, 1.0);      // Set the colour white
    glBegin(GL_QUADS);
        glTexCoord2f(b   , 0.0 ); glVertex3f(x      , y - 2*b, z + glRenderTextureZfix);
        glTexCoord2f(b   , a   ); glVertex3f(x + 2*a, y - 2*b, z + glRenderTextureZfix);
        glTexCoord2f(0.0 , a   ); glVertex3f(x + 2*a, y      , z + glRenderTextureZfix);
        glTexCoord2f(0.0 , 0.0 ); glVertex3f(x      , y      , z + glRenderTextureZfix);
    glEnd;
    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    glRenderTextureZfix := glRenderTextureZfix + 0.01;

    glClearColor(bkColor[0],bkColor[1],bkColor[2],bkColor[3]);  //reset previous clear color
end;

{$ENDREGION}

{$REGION 'textures'}
procedure TGLForm.ClearTexture(theTex: GLUint; aWidth, aHeight: integer);
var
    Data: Pointer;
    tmpTex: GLUint;
begin
    {store the current texture}
    glGetIntegerv(GL_TEXTURE_BINDING_2D,@tmpTex);
    {bind to the texture we want to clear}
    glBindTexture(GL_TEXTURE_2D, theTex);
    {blank pixel data}
    GetMem(Data, aWidth*aHeight*4);
    {write the blank data to the bound texture}
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, aWidth, aHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, Data); //fill texture with empty pixels from Data
    FreeMem(Data);
    {bind back to the original texture}
    glBindTexture(GL_TEXTURE_2D, tmpTex);
end;

procedure TGLForm.SetupRenderTexture;
begin
    if not bEXTFramebufferObjectExists then Exit;

    renderTextureSize := smaller(ClientWidth, clientHeight);

    glGenFramebuffersEXT(1, @fboid);
    if glIsTexture(RenderTexture) then
        glDeleteTextures(1, @renderTexture);
    glGenTextures(1, @RenderTexture);
    glBindTexture(GL_TEXTURE_2D, RenderTexture);

    glGenRenderbuffersEXT(1, @rboid);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    ClearTexture(RenderTexture, renderTextureSize, renderTextureSize);

    {bind FBO and attach}
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboid);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT,GL_TEXTURE_2D, RenderTexture, 0);

    { bind a render buffer as our depth buffer and attach it}
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, rboid);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, renderTextureSize, renderTextureSize);
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,GL_DEPTH_ATTACHMENT_EXT,GL_RENDERBUFFER_EXT, rboid);

    //status := glCheckFramebufferStatusEXT(GL_FRAMEBUFFER);
//    if (glCheckFramebufferStatusEXT(GL_FRAMEBUFFER) <> GL_FRAMEBUFFER_COMPLETE) then ashow(2);//check FBO is ready
//    GL_FRAMEBUFFER_UNDEFINED;
//    GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT;
//    GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT;
//    GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER;
//    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE;
//    GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER;
//    GL_FRAMEBUFFER_UNSUPPORTED;
//    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE;
//    GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS;

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);    // Go back to regular frame buffer rendering
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    if glGetError <> 0 then showmessage(rsGLErrorRenderingTexture);
end;

procedure TGLForm.tmWaterTextureOffsetTimer(Sender: TObject);
begin
    waterTextureOffset.x := waterTextureOffset.x + 0.005;
    if waterTextureOffset.x > 1 then
        waterTextureOffset.x := waterTextureOffset.x - 1;

    waterTextureOffset.y := waterTextureOffset.y + 0.0032;
    if waterTextureOffset.y > 1 then
        waterTextureOffset.y := waterTextureOffset.y - 1;

    //forceDrawNow;
end;

procedure TGLForm.updateBathymetryTexture;
type
    glByteArray = array of GLUByte;
    glByteMatrix = array of array of GLUByte;

    function bytesFromColormap(const colormap: TMatrix): glByteMatrix;
    var
        i,j: integer;
    begin
        setlength(result,length(colormap),length(colormap[0]));
        for i := 0 to high(colormap) do
            for j := 0 to high(colormap[0]) do
                result[i,j] := safetrunc(colormap[i,j]*255);
    end;

    function generateTextureArray(const gDepths: TSingleMatrix;
                                  const width,height: integer;
                                  const alpha, minGDepths, maxGDepths: double;
                                  const WaterColorArrayByte, landColorArrayByte: glByteMatrix): glByteArray;
    const
        nanbyteColor = 180;
    var
        bMinGDepthIsNegative: boolean;
        i,j,thisPixel: integer;
        waterColorArrayInd, landColorArrayInd: integer;
        thisDepth, waterColorIndScaling, landColorIndScaling: double;
    begin
        if maxGDepths <> 0 then
            waterColorIndScaling := high(WaterColorArrayByte)/maxGDepths
        else
            waterColorIndScaling := 0;
        if minGDepths <> 0 then
            landColorIndScaling := high(LandColorArrayByte)/minGDepths
        else
            landColorIndScaling := 0;
        bMinGDepthIsNegative :=  minGDepths < 0;

        setlength(result,width*height*4);
        for j := 0 to height - 1 do
            for i := 0 to width - 1 do
            begin
                thisPixel := 4*((height - 1 - j)*width + i);
                result[thisPixel + 3] := safetrunc(alpha*255);

                thisDepth := gDepths[j,i];
                if isnan(thisDepth) then
                begin
                    {NAN}
                    result[thisPixel    ] := nanbyteColor;
                    result[thisPixel + 1] := nanbyteColor;
                    result[thisPixel + 2] := nanbyteColor;
                end
                else
                if thisDepth > 0 then
                begin
                    {water}
                    waterColorArrayInd := safetrunc(waterColorIndScaling*thisDepth);
                    result[thisPixel    ] := WaterColorArrayByte[waterColorArrayInd,0];
                    result[thisPixel + 1] := WaterColorArrayByte[waterColorArrayInd,1];
                    result[thisPixel + 2] := WaterColorArrayByte[waterColorArrayInd,2];
                end
                else
                begin
                    {land}
                    if not bMinGDepthIsNegative then
                    begin
                        {land, and all land is at 0 so just use landcolor}
                        result[thisPixel    ] := getRValue(UWAOpts.landColor);
                        result[thisPixel + 1] := getGValue(UWAOpts.landColor);
                        result[thisPixel + 2] := getBValue(UWAOpts.landColor);
                    end
                    else
                    begin
                        {land, and using the colormap for different heights}
                        landColorArrayInd := min(length(landColorArrayByte) - 1, max(0,safetrunc(landColorIndScaling*thisDepth)));
                        result[thisPixel    ] := LandColorArrayByte[landColorArrayInd,0];
                        result[thisPixel + 1] := LandColorArrayByte[landColorArrayInd,1];
                        result[thisPixel + 2] := LandColorArrayByte[landColorArrayInd,2];
                    end;
                end;
            end;
    end;

var
    textureData    : glByteArray;
    gDepths        : TSingleMatrix;
begin
    {NB j,y is the first dimension in the array}
    gDepths := container.bathymetry.gDepths;

    textureData := generateTextureArray(gDepths,
                                        length(gDepths[0]),length(gDepths),
                                        UWAOpts.bathymetryAlpha,
                                        gDepths.min,
                                        gDepths.max,
                                        bytesFromColormap(UWAOpts.bathymetrycolorMap),
                                        bytesFromColormap(UWAOpts.landColorMap));

    if glIsTexture(BathyTexture) then
        glDeleteTextures(1, @bathyTexture);
    glGenTextures( 1, @BathyTexture );// allocate a texture name
    glBindTexture( GL_TEXTURE_2D, BathyTexture ); // select our current texture
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );  // select modulate to mix texture with color for shading
    { min_filter: when texture area is small. see gl documentation for details}
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR );
    { mag_filter: when texture area is large. see gl documentation for details}
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    {create the mipmaps}
    gluBuild2DMipmaps( GL_TEXTURE_2D, GL_RGBA, length(gDepths[0]),length(gDepths),
                       GL_RGBA, GL_UNSIGNED_BYTE, @TextureData[0] ); // build our texture mipmaps from the textureData

    BathyTextureIsCurrent := true;
end;

procedure TGLForm.invalidateBathyTexture;
begin
    BathyTextureIsCurrent := false;
end;

{$ENDREGION}


end.
