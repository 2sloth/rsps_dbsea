object frmScenarioSummary: TfrmScenarioSummary
  Left = 0
  Top = 0
  Caption = 'Summary of scenarios'
  ClientHeight = 672
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    662
    672)
  PixelsPerInch = 96
  TextHeight = 13
  object btOK: TButton
    Left = 564
    Top = 624
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object sgSummary: TStringGrid
    Left = 0
    Top = 0
    Width = 662
    Height = 609
    Align = alTop
    TabOrder = 1
  end
  object btToclipboard: TButton
    Left = 408
    Top = 624
    Width = 139
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Copy to clipboard'
    TabOrder = 2
    OnClick = btToclipboardClick
  end
end
