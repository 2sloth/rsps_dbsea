object LatLongEntryForm: TLatLongEntryForm
  Left = 0
  Top = 0
  Caption = 'Latitude - longitude entry'
  ClientHeight = 148
  ClientWidth = 460
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    460
    148)
  PixelsPerInch = 96
  TextHeight = 13
  object lbLongitude: TLabel
    Left = 254
    Top = 67
    Width = 55
    Height = 13
    Caption = 'Longitude '#176
  end
  object lbLatitude: TLabel
    Left = 20
    Top = 67
    Width = 47
    Height = 13
    Caption = 'Latitude '#176
  end
  object lbUTMZone: TLabel
    Left = 20
    Top = 28
    Width = 47
    Height = 13
    Caption = 'UTM zone'
  end
  object btOK: TButton
    Left = 273
    Top = 106
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
    OnClick = btOKClick
    ExplicitLeft = 208
    ExplicitTop = 192
  end
  object btCancel: TButton
    Left = 361
    Top = 106
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    ExplicitLeft = 296
    ExplicitTop = 192
  end
  object edLat: TEdit
    Left = 81
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '0.0'
  end
  object edLong: TEdit
    Left = 315
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '0.0'
    OnKeyUp = edLongKeyUp
  end
  object edUTMZone: TEdit
    Left = 81
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'edUTMZone'
    OnKeyUp = edUTMZoneKeyUp
  end
end
