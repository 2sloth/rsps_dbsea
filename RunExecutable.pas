unit RunExecutable;

interface

uses system.SysUtils, system.Strutils, system.Classes, system.Math,
    system.rtti, system.types,
    vcl.Dialogs,
    vcl.forms,
     vcl.stdctrls,
     vcl.comctrls,
     winapi.Windows,
     Winapi.ShellAPI,
     SQLiteTable3, baseTypes, helperFunctions, psAPI,
     smartPointer;

type
    TRunExecutable = class(TObject)
        class function runDostoString(DosApp, sWorkingDir: String; const showConsoleWindow, showTextDuringRun: boolean; const sMemoFirstLine: string; const outputMemo: TMemo): string;
        class function runDosStdinStdout(DosApp, stdin: String): string;
        class function runSimple(filename, parameters: string): integer;
    end;

implementation

class function TRunExecutable.runDostoString(DosApp, sWorkingDir: String; const showConsoleWindow, showTextDuringRun: boolean; const sMemoFirstLine: string; const outputMemo: TMemo): string;
{as we are skipping writing to disk, this function captures the output from stdout
of an external process, and puts it into a string}
const
    ReadBuffer = 2400;
    iLinesToShow = 18;
    iCharsToShow = 80;
var
    Security: TSecurityAttributes;
    ReadPipe, WritePipe: THandle;
    start: TStartUpInfo;
    ProcessInfo: TProcessInformation;
    Buffer: Pansichar;
    BytesRead, BytesAvailable: LongWord;
    Apprunning: LongWord;
    s: String;
    sl1: ISmart<TStringList>;
    i: integer;
    bTLData: boolean;
begin
    result := '';
    sl1 := TSmart<TStringList>.create(TStringList.Create);
    bTLData := false;

    Security.nlength := SizeOf(TSecurityAttributes) ;
    Security.binherithandle := true;
    Security.lpsecuritydescriptor := nil;

    {the single pipe created here has 2 end handles, readpipe is within dBSea and writepipe
    is the handle for the end attached to the external exe. if you want to also send text
    to stdin, create another pipe and connect it.}
    if Createpipe (ReadPipe, WritePipe,@Security, 0) then
    begin
        Buffer := AllocMem(ReadBuffer + 1) ;
        FillChar(Start,Sizeof(Start),#0) ;
        start.cb := SizeOf(start) ;
        start.hStdOutput := WritePipe;//the pipe end connected to stdout of the exe. read pipe data at readpipe
        //start.hStdInput := --some other pipe read handle--
        start.dwFlags := STARTF_USESTDHANDLES + STARTF_USESHOWWINDOW;
        start.lpTitle := 'Solver running...';
        if showConsoleWindow then
            start.wShowWindow := SW_SHOWNORMAL
        else
            start.wShowWindow := SW_HIDE;

        UniqueString(DosApp);
        sWorkingDir := IncludeTrailingPathDelimiter(sWorkingDir);
        UniqueString(sWorkingDir);
        //showmessage(sWorkingDir);
        //if CreateProcess(nil,PChar(DosApp),@Security,@Security,true,CREATE_NEW_CONSOLE,nil,nil,start,ProcessInfo) then
        if CreateProcess(
            nil,
            PChar(DosApp),
            @Security,
            @Security,
            true,
            NORMAL_PRIORITY_CLASS,
            nil,
            PChar(sWorkingDir),
            start,
            ProcessInfo) then
        begin
            if showTextDuringRun then
            begin
                {show and clear the form return messages}
                if assigned(outputMemo) then
                    outputMemo.Text := '';
            end;
            {wait for the console exe to finish executing, and check for data in the pipe as it runs}
            repeat
                Apprunning := WaitForSingleObject(ProcessInfo.hProcess,100) ;  {wait time in ms}
                {whether we are showing the memo text or not, pull the output from the pipe
                as some programs will stall if this doesn't get cleared}

                {PeekNamedPipe looks at the data in the pipe, without clearing it from the pipe}
                PeekNamedPipe(ReadPipe,nil,0,nil,@BytesAvailable,nil);
                if BytesAvailable > 0 then
                Repeat
                    BytesRead := 0;
                    ReadFile(ReadPipe,Buffer[0],ReadBuffer,BytesRead,nil) ;
                    Buffer[BytesRead]:= #0;
                    OemToAnsi(Buffer,Buffer);
                    s := s + String(Buffer);
                until (BytesRead < ReadBuffer);  //until on the last read, the buffer is not full

                {check if we are in the TL Data output section of the output, at which point
                stop showing the output as this just slows down things}
                if AnsiContainsText (s,'TL Data') then
                begin
                    if not bTLData then
                    begin
                        if assigned(outputMemo) then
                            outputMemo.Text := 'Reading results...';
                        bTLData := true;
                    end;
                end
                else
                begin
                    {cut output down to n lines and print it to the memo box so it looks like it is scrolling}
                    sl1.Text := s;
                    while sl1.Count > iLinesToShow do
                        sl1.Delete(0);

                    {shorten each line to just n characters, to speed up display of tmemo}
                    for i := 0 to sl1.Count - 1 do
                        sl1[i] := leftStr(sl1[i],iCharsToShow);
                    {Add 2 lines at the start, giving info about where we are in the process}
                    sl1.Insert(0,sMemoFirstLine);
                    sl1.Insert(1,'------------------');

                    {all the remaining text, send it to the TMemo}
                    if assigned(outputMemo) then
                        outputMemo.Text := sl1.Text;
                end;
                Application.ProcessMessages;
            until (Apprunning <> WAIT_TIMEOUT);

            {check if any remaining data in pipe}
            PeekNamedPipe(ReadPipe,nil,0,nil,@BytesAvailable,nil);

            {any remaining data, add it onto string s}
            if BytesAvailable > 0 then
            Repeat
                BytesRead := 0;
                ReadFile(ReadPipe,Buffer[0],ReadBuffer,BytesRead,nil) ;
                Buffer[BytesRead]:= #0;
                OemToAnsi(Buffer,Buffer) ;
                s := s + String(Buffer);
            until (BytesRead < ReadBuffer) ;  //until on the last read, there was not a full buffer

            {copy all program output to the result}
            result := s;

            {clear the form memo again}
            if assigned(outputMemo) then
                outputMemo.Text := '';
        end
        else
            showmessage('Could not start external executable ' + dosapp);

        FreeMem(Buffer) ;
        CloseHandle(ProcessInfo.hProcess) ;
        CloseHandle(ProcessInfo.hThread) ;
        CloseHandle(ReadPipe) ;
        CloseHandle(WritePipe) ;
    end;
end;

class function TRunExecutable.runSimple(filename, parameters: string): integer;
begin
    result := shellexecute(0, nil, pwidechar(filename), pwidechar(parameters), '', SW_HIDE);
end;

class function TRunExecutable.runDosStdinStdout(DosApp, stdin: String): string;
const
    BufferLength = 2400;
var
    security: TSecurityAttributes;
    stdOutReadPipe,stdoutWritePipe, stdinReadPipe,stdinWritePipe: THandle;
    start: TStartUpInfo;
    ProcessInfo: TProcessInformation;
    stdinAnsi: AnsiString;
    stdoutBuffer: Pansichar;
    BytesRead,BytesAvailable: LongWord;
    Apprunning: LongWord;
    s: String;
    lpNumberOfBytesWritten: DWORD;
begin
    result := '';
    security.nlength := SizeOf(TSecurityAttributes);
    security.binherithandle := true;
    security.lpsecuritydescriptor := nil;

    if Createpipe(stdOutReadPipe,stdoutWritePipe,@security,0) and Createpipe(stdinReadPipe,stdinWritePipe,@security,0) then
    begin
        stdoutBuffer := AllocMem(BufferLength + 1);

        stdinAnsi := ansiString(stdin);
        //stdinBuffer := AllocMem(2*length(stdin) + 1);
        //copy(stdinAnsi,stdinBuffer,length(stdin));

        FillChar(Start,Sizeof(Start),#0) ;
        start.cb := SizeOf(start) ;
        start.hStdOutput := stdoutWritePipe;//the pipe end connected to stdout of the exe. read pipe data at readpipe
        start.hStdInput := stdinReadPipe;
        start.dwFlags := STARTF_USESTDHANDLES + STARTF_USESHOWWINDOW;
        start.wShowWindow := SW_HIDE;

        UniqueString(DosApp);
        if CreateProcess(nil,PChar(DosApp),@security,@security,true,NORMAL_PRIORITY_CLASS,nil,nil,start,ProcessInfo) then
        begin
            writeFile(stdinWritePipe,stdinAnsi[1],length(stdinAnsi),lpNumberOfBytesWritten,nil);

            {wait for the console exe to finish executing, and check for data in the pipe as it runs}
            repeat
                Apprunning := WaitForSingleObject(ProcessInfo.hProcess,10) ;  {wait time in ms}
                {whether we are showing the memo text or not, pull the output from the pipe
                as some programs will stall if this doesn't get cleared}

                {PeekNamedPipe looks at the data in the pipe, without clearing it from the pipe}
                PeekNamedPipe(stdOutReadPipe,nil,0,nil,@BytesAvailable,nil);
                if BytesAvailable > 0 then
                Repeat
                    BytesRead := 0;
                    ReadFile(stdOutReadPipe,stdoutBuffer[0],BufferLength,BytesRead,nil) ;
                    stdoutBuffer[BytesRead]:= #0;
                    OemToAnsi(stdoutBuffer,stdoutBuffer);
                    s := s + String(stdoutBuffer);
                until (BytesRead < BufferLength);  //until on the last read, the buffer is not full

            until (Apprunning <> WAIT_TIMEOUT);

            {check if any remaining data in pipe}
            PeekNamedPipe(stdOutReadPipe,nil,0,nil,@BytesAvailable,nil);

            {any remaining data, add it onto string s}
            if BytesAvailable > 0 then
            Repeat
                BytesRead := 0;
                ReadFile(stdOutReadPipe,stdoutBuffer[0],BufferLength,BytesRead,nil) ;
                stdoutBuffer[BytesRead]:= #0;
                OemToAnsi(stdoutBuffer,stdoutBuffer) ;
                s := s + String(stdoutBuffer);
            until (BytesRead < BufferLength) ;  //until on the last read, there was not a full buffer

            result := s;
        end
        else
            showmessage('Could not start external executable ' + dosapp);

        //freemem(stdinBuffer);
        FreeMem(stdoutBuffer);
        CloseHandle(ProcessInfo.hProcess);
        CloseHandle(ProcessInfo.hThread);
        CloseHandle(stdOutReadPipe);
        CloseHandle(stdoutWritePipe);
        CloseHandle(stdinReadPipe);
        CloseHandle(stdoutReadPipe);
    end;
end;


end.
