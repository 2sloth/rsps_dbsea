unit dBSeaRay3D1;

{$B-}

interface

uses
    system.classes, system.math, system.SysUtils, system.types,
    system.StrUtils, system.math.vectors,
    System.Generics.collections,
    Mcomplex, baseTypes,
    helperFunctions, dBSeaconstants, material,
    multiLayerReflectionCoefficient, seafloorScheme, directivity,
    mcklib, dBSeaRayArrivals, RaySolverCommon,
    waterprops, solversGeneralFunctions, raySolverTypes, raySolverTypes3D,
    globalsUnit, nullable, SmartPointer, RayPlotting, OtlParallel;

type


  TdBSeaRay3D1Solver = class(TObject)
  private
    //class function  distToLine2D(const px,py: double; const xprev,xnext: TRayIterator3D): double;
    //class function  shouldUpdatePoint(const px,py: double; const xprev,xnext: TRayIterator3D): boolean;
  public
    class procedure main(const f: TDoubleDynArray;
                        const solveThisFreq: TBooleanDynArray;
                        const srcX, srcY, srcZ, xmax, ymax, zmax, rmax, dr, startInitialAngle, endInitialAngle, initialStepsize: double;
                        const nx,ny,nz,nSlices,nPhi0,nTheta0, maxBottomReflections: integer;
                        const depths: TSingleMatrix;
                        const aCProfile2: TDepthValArray;
                        const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp: double;
                        const returnTL, outputsliceRays: boolean;
                        const seafloorScheme: TSeafloorScheme;
                        const directivities: TDirectivityList;
                        const fs: double;
                        const outputArrivals, calculateVolumeAttenuationAtEachStep: boolean;
                        const raySolverIterationMethod: TRaySolverIterationMethod;
                        const cancel: TWrappedBool;
                        const fractionDone: TWrappedDouble;
                        out tlMultiFreq: TArray<TDoubleField>{f,i,j,k};
                        out pFieldMultiFreq: TComplexFieldArray{f,i,j,k};
                        var arrivals: TArrivalsField;
                        var solverErrors: TSolverErrors;
                        out deaths: TRayDeaths);
  end;

implementation


//class function TdBSeaRay3D1Solver.distToLine2D(const px,py: double; const xprev,xnext: TRayIterator3D): double;
//var
//    dx, dy: double;
//begin
//    dx := xnext.x-xprev.x;
//    dy := xnext.y-xprev.y;
//    result := abs(dx*(xprev.y-py)-(xprev.x-px)*dy) / sqrt(sqr(dx) + sqr(dy));
//end;
//
//class function TdBSeaRay3D1Solver.nearestPointOnLine(const px,py: double; const xprev,xnext: TRayIterator3D): double;
//var
//    dx, dy: double;
//begin
//    dx := xnext.x-xprev.x;
//    dy := xnext.y-xprev.y;
//    result := abs(dx*(xprev.y-py)-(xprev.x-px)*dy) / sqrt(sqr(dx) + sqr(dy));
//end;

class procedure TdBSeaRay3D1Solver.main(const f: TDoubleDynArray;
                        const solveThisFreq: TBooleanDynArray;
                        const srcX, srcY, srcZ, xmax, ymax, zmax, rmax, dr, startInitialAngle, endInitialAngle, initialStepsize: double;
                        const nx,ny,nz, nSlices, nPhi0, nTheta0, maxBottomReflections: integer;
                        const depths: TSingleMatrix;
                        const aCProfile2: TDepthValArray;
                        const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp: double;
                        const returnTL, outputsliceRays: boolean;
                        const seafloorScheme: TSeafloorScheme;
                        const directivities: TDirectivityList;
                        const fs: double;
                        const outputArrivals, calculateVolumeAttenuationAtEachStep: boolean;
                        const raySolverIterationMethod: TRaySolverIterationMethod;
                        const cancel: TWrappedBool;
                        const fractionDone: TWrappedDouble;
                        out tlMultiFreq: TArray<TDoubleField>{f,i,j,k};
                        out pFieldMultiFreq: TComplexFieldArray{f,i,j,k};
                        var arrivals: TArrivalsField;
                        var solverErrors: TSolverErrors;
                        out deaths: TRayDeaths);
//NB kill ray after maxBottomReflections reflections from the bottom
const
    defaultNTheta = 1000;
    defaultNPhi = 1000;
var
    c,dcdz,d2cdz2,z,x,y,theta,phi: TDoubleDynArray;
    constSSP: boolean;
    nf,nTheta,nPhi,i,ix,iy: integer;
    deltaz,deltax,deltay,delTheta,delPhi: double;
    directivityMat: TSingleMatrix;
    konst, c0: double;
    solveFreqInds: TArray<integer>;
    nr: integer;
    internalFractionDone: ISmart<TDoubleList>;
    localRayDeaths: TRayDeaths;
    aRayDeath: TRayDeath;
    localSolverErrors: TSolverErrors;
    aSolverError: TSolverError;
    amplitudeCutoff: double;
    localArrivals: TArrivalsField;
    localPFieldMultiFreq: pComplexFieldArray;

//    procedure intensityCalcs2();
//    const
//        eps = 0.01;
//    var
//        iz, fi, izStart, izEnd: integer;
//        mayRayWidth, distToRay, magnitude, startX, endX, startY, endY, startZ, endZ: double;
//        ax,ay: double;
//        nStart, nEnd, n, n0, ir, irStart, irEnd: integer;
//
//        function boxSpansSource(): boolean;
//        begin
//            result := (startX <= 0) and (endX >= 0) and (startY <= 0) and (endY >= 0);
//        end;
//
//        function boxSpansThetaWrap(): boolean;
//        begin
//            result := (startX >= 0) and (endX > 0) and (startY <= 0) and (endY >= 0);
//        end;
//
//        function sliceForXY(const x,y: double): integer;
//        begin
//            result := round(nSlices * ArcTan2(y,x) / (2*pi));
//            if result < 0 then
//                result := result + nSlices;
//        end;
//
//        function rangeForXY(const x,y: double): integer;
//        begin
//            result := round((sqr(x) + sqr(y)) / sqr(dr));
//        end;
//
//        procedure boxRange(out mn,mx: integer);
//        var
//            q: integer;
//        begin
//            mx := rangeForXY(startX,startY);
//            mn := mx;
//            q := rangeForXY(startX,endY);  //todo start/end?
//            if q > mx then
//                mx := q
//            else if q < mn then
//                 mn := q;
//            q := rangeForXY(endX,startY);
//            if q > mx then
//                mx := q
//            else if q < mn then
//                 mn := q;
//            q := rangeForXY(endX,endY);
//            if q > mx then
//                mx := q
//            else if q < mn then
//                 mn := q;
//
//            if mn >= nr then
//                mn := nr - 1;
//            if mx >= nr then
//                mx := nr - 1;
//        end;
//
//        procedure sliceRange(out mn,mx: integer);
//        var
//            q: integer;
//        begin
//            mx := sliceForXY(startX,startY);
//            mn := mx;
//            q := sliceForXY(startX,endY); //todo start/end?
//            if q > mx then
//                mx := q
//            else if q < mn then
//                 mn := q;
//            q := sliceForXY(endX,startY);
//            if q > mx then
//                mx := q
//            else if q < mn then
//                 mn := q;
//            q := sliceForXY(endX,endY);
//            if q > mx then
//                mx := q
//            else if q < mn then
//                 mn := q;
//        end;
//
//        procedure getXY(const n,ir: integer; out x,y: double);
//        begin
//            system.SineCosine(2*pi/nSlices*n, x, y);
//            x := dr*ir*x;
//            y := dr*ir*y;
//        end;
//    begin
//        mayRayWidth := 2*rayHeight.max;
//        startX := (min(xprev.x,xnext.x) - srcX) - mayRayWidth;
//        endX := (max(xprev.x,xnext.x) - srcX) + mayRayWidth;
//        startY := (min(xprev.y,xnext.y) - srcY + eps) - mayRayWidth;  //add eps so that we aren't travelling directly along phi = 0
//        endY := (max(xprev.y,xnext.y) - srcY + eps) + mayRayWidth;
//        startZ := min(xprev.z,xnext.z) - mayRayWidth;
//        endZ := max(xprev.z,xnext.z) + mayRayWidth;
//        izStart := max(0,safefloor(startZ / deltaZ) - 1);
//        izEnd := min(length(z) - 1,safeceil(endZ / deltaZ) + 1);
//
//        if (startX = endX) or (startY = endY) or (startZ = endZ) then exit;
//
//        boxRange(irStart,irEnd);
//        if boxSpansSource then
//        begin
//            nStart := 0;
//            nEnd := nSlices - 1;
//            irStart := 0;
//        end
//        else if boxSpansThetaWrap then
//        begin
//            nStart := sliceForXY(startX,starty); //NB reversed
//            nEnd := sliceForXY(startX,endY) + nSlices;
//        end
//        else
//            sliceRange(nStart,nEnd);
//
//        for n0 := nStart to nEnd do
//        begin
//            n := n0 mod nSlices;
//            for ir := irStart to irEnd do
//            begin
//                getXY(n,ir,ax,ay);
//                if not TSolversGeneralFunctions.lineSegmentPassesPoint2D(ax,ay,xprev.x,xprev.y,xnext.x,xnext.y) then continue;
//
//                for fi in solveFreqInds do //:= 0 to nf - 1 do
//                begin
//                    {check all the z points that are within the range of the raywidth}
//                    for iz := iZStart to iZEnd do
//                    begin
//                        distToRay := TSolversGeneralFunctions.distPointToLineSegment3d(ax,ay,z[iz],xprev.x,xprev.y,xprev.z,xnext.x,xnext.y,xnext.z); //takes account of line segment endpoints
//
//                        if (distToRay > 2*rayHeight[fi]) then continue; //2*waywidth gets us to 2 StdDev, which is around -30 dB
//
//                        //magnitude := (1 - distToRay/rayWidth); //hat function
//                        magnitude := exp(-sqr(distToRay/rayHeight[fi])); //gaussian -- we can speed this line up
//
//                        if outputArrivals then
//                        begin
//                            {add this arrival to the arrivals list for this point}
//                            arrivals.addArrival(fi, ir, n, iz,
//                                                ReflectionAmplitudePhase[fi].r*Abeam[fi]*magnitude*cos(rayPhase[fi]),
//                                                delay);
//                        end
//                        else
//                        begin
//                            {add the contribution from this ray at this point to the output field}
//                            pFieldMultiFreq[n,fi,iz,ir] := pFieldMultiFreq[n,fi,iz,ir] + ComplexEXP(makeComplex(0,rayPhase[fi])) *
//                                                                                     ReflectionAmplitudePhase[fi] *
//                                                                                     (Abeam[fi]*magnitude); //includes accumulated reflection amplitude/phase
//                        end;
//                    end; //iz
//                end;
//            end;
//        end;
//    end;

    procedure tlOutput(const nSlices,nf,nz,nr: integer);
    var
        fi, n, ir, iz: integer;
        thisModulusSquared, d: double;
    begin
        setlength(tlMultiFreq,nSlices,nf,nz,nr);
        for n := 0 to nSlices - 1 do
            for fi := 0 to nf - 1 do
                for iz := 0 to nz - 1 do
                    for ir := 0 to nr - 1 do
                        if solveThisFreq[fi] then
                        begin
                            thisModulusSquared := pFieldMultiFreq[n,fi,iz,ir].modulusSquared;
                            if thisModulusSquared > 0 then
                            begin
                                d := -10*log10(thisModulusSquared);
                                tlMultiFreq[n,fi,iz,ir] := d
                            end
                            else
                                tlMultiFreq[n,fi,iz,ir] := 0;
                        end
                        else
                            tlMultiFreq[n,fi,iz,ir] := nan;
    end;


    //{$Define RAYOUTPUT}
    //{$Define TESTRAY}
    //{$Define TESTHEADING}
    {$IF defined (DEBUG) and (DEFined (RAYOUTPUT) or defined (Testtray) or defined (TESTHEADING))}
var
    s: string;
    prevHeading, heading: double;
begin
    s := '';
    heading := 0;
    prevHeading := 0;
    {$ELSE}
begin
    {$ENDIF}

    solveFreqInds := TSolversGeneralFunctions.getSolveFreqInds(solveThisFreq);

    amplitudeCutoff := undB20(-400);
    TRayDeathsHelper.init(deaths);
    TRayDeathsHelper.init(localRayDeaths);
    localArrivals := arrivals;
    localPFieldMultiFreq := @pFieldMultiFreq;

    if solveThisFreq.allFalse() then
        include(solverErrors,kNoFrequencies);

    konst := 1/power(2*pi,0.25);
    c0 := aCProfile2.average;

    nr := max(1, saferound(rmax/dr));

    sliceRayss.Clear;
    nTheta := ifthen(nTheta0 > 0, nTheta0, defaultNTheta);
    nPhi := ifthen(nPhi0 > 0, nPhi0, defaultNPhi);
    setlength(phi, nPhi);
    delPhi := 2*pi/nPhi;
    for i := 0 to nPhi - 1 do
        phi[i] := i*delPhi;

    //{$DEFINE DEBUG_RAY3D_SETUP}
    {$IF defined (DEBUG) and defined (DEBUG_RAY3D_SETUP)}{nTheta := 8;} if nPhi = 1 then phi[0] := 3*pi/4;{$ENDIF}

    //setting all needed multi-freq arrays
    nf := length(f);
    //layers := TSmart<TSeafloorReflectionLayers>.create(TSeafloorReflectionLayers.create(true));

    deltaz := zmax / max(1,(nz - 1));
    setlength(z,nz);
    for i := 0 to nz - 1 do
        z[i] := i*deltaz;

    deltax := xmax / nx;
    setlength(x,nx);
    for ix := 0 to nx - 1 do
        x[ix] := ix*deltax;

    deltay := ymax / ny;
    setlength(y,ny);
    for iy := 0 to ny - 1 do
        y[iy] := iy*deltay;

    {$IF defined (DEBUG) and defined (RAYOUTPUT)}s := s + '<seafloor>' + #13#10; for i := 0 to bsteps - 1 do s := s + floattostr(round1(r_zb[i])) + ',' + floattostr(round1(zb[i])) + ';'; s := s + #13#10 + '</seafloor>' + #13#10;{$ENDIF}

    TRaySolverCommon.initCProfiles(aCProfile2, constSSP, c, dcdz, d2cdz2);

    {angles}
    TSolversGeneralFunctions.setupRayAngles(startInitialAngle,endInitialAngle,nTheta,theta,delTheta);
    directivityMat := TSolversGeneralFunctions.getDirectivityAsMatrix(f,theta,directivities);

    if outputArrivals then
    begin
        if assigned(arrivals) then
            arrivals.reinitialise(nf,nr,nSlices,nz,fs)
        else
            arrivals := TArrivalsField.Create(nf,nr,nSlices,nz,fs);
    end
    else
        setlength(pFieldMultiFreq,nSlices,nf,nz,nr);

    internalFractionDone := TSmart<TDoubleList>.create(TDoubleList.create);
    internalFractionDone.init(nPhi);

    {iterate through rays}
    Parallel.for(0, nPhi - 1).Execute(procedure (iPhi: integer)
    //for iPhi := 0 to nPhi - 1 do
    var
        iTheta, fi: integer;
        xnext,xprev: TRayIterator3D;
        inSeafloor,inAir: boolean;
        sinTheta0,cosTheta0,theta0: double;
        ReflectionAmplitudePhase: TComplexVect; //reflection amplitude loss and phase change, accumulated over reflections
        nReflections,iz: integer;
        cHere,stepDist: double; //average c for this step
        delay: double;
        rayPhase, rayHeight, rayWidth: TDoubleDynArray;
        bPastRayWidthXOver: TBooleanDynArray;
        Abeam, ABeamWidth: TDoubleDynArray;
        ds: double; //initial stepsize
        localAttenuation: Single;
        thisBottom: TGetDepthRec3D;
        iz1,iz2: integer;
        sliceRays: TSliceRays;
        ray: TRay;
        layers: ISmart<TSeafloorReflectionLayers>;
        phi0,cosPhi0,sinPhi0: double;
        maxInitialAmplitude: double;

        function maxAmplitude(): double;
        var
            fi: integer;
        begin
            result := nan;
            for fi in solveFreqInds do
                greater(result,ReflectionAmplitudePhase[fi].modulusSquared);
        end;
        function minAmplitude(): double;
        var
            fi: integer;
        begin
            result := nan;
            for fi in solveFreqInds do
                lesser(result,ReflectionAmplitudePhase[fi].modulusSquared);
        end;
        function inAirCheck_shouldBreak(): boolean;
        var
            inTheta, outTheta, stepdx, stepdy, stepdz: double;
            fi: integer;
        begin
            result := false;
            if (xnext.z < 0) then
            begin
                {ray hit the surface}
                if (inAir) then
                begin
                    {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'ray ' + IntToStr(itheta) + ' in air' + #13#10;{$ENDIF}
                    inc(localRayDeaths[kAir]);
                    exit(true);
                end;
                inAir := true;

                stepdx := xnext.x - xprev.x;
                stepdy := xnext.y - xprev.y;
                stepdz := xnext.z - xprev.z;
                inTheta := arctan2(stepdz,hypot(stepdx,stepdy));
                outTheta := -inTheta;
                cHere := c[min(length(c) - 1,max(0,saferound(xprev.z)))]; //todo average for this ray
                xnext.zeta := sin(outTheta)/cHere;

                xnext.x := xprev.x - xprev.z/stepdz*stepdx;  //stepdz is negative
                xnext.y := xprev.y - xprev.z/stepdz*stepdy;  //stepdz is negative
                xnext.z := 0;
                //if isnan(xprev.x) or isnan(xprev.z) then
                //    break; //todo but why nan?

                for fi in solveFreqInds do
                    ReflectionAmplitudePhase[fi] := ReflectionAmplitudePhase[fi] * (-1); //phase change on reflection
            end
            else
                inAir := false;
        end;

        function inSeabedCheck_shouldBreak(const layers: TSeafloorReflectionLayers): boolean;
        var
            fi: integer;
        begin
            result := false;
            //get bottom info at this point
            if TSolversGeneralFunctions.rayHitSeafloor3D(depths,xmax/length(depths[0]),ymax/length(depths),cHere,ds,xnext,xprev,thisBottom) then
            begin
                {hit the bottom}
                if (inSeafloor) then
                begin
                    {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'ray ' + IntToStr(itheta) + ' in seafloor' + #13#10;{$ENDIF}
                    inc(localRayDeaths[kSeafloor]);
                    exit(true);
                end;

                inSeafloor := true;
                {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'inc(nReflections) ' + floattostr(xnext.z) + ' > ' + FloatToStr(thisBottom.z) + #13#10;{$ENDIF}
                inc(nReflections);

                //kill ray if it has had more than the max number of reflections
                if nReflections >= maxBottomReflections then
                begin
                    {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'ray ' + IntToStr(itheta) + ' nreflections ' + IntToStr(nReflections) + ' >= ' + IntToStr(maxBottomReflections) + #13#10;{$ENDIF}
                    inc(localRayDeaths[kBounces]);
                    exit(true);
                end;

                //todo get seafloor at this location
                TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(seafloorScheme,thisBottom.z, layers);
                for fi in solveFreqInds do
                    ReflectionAmplitudePhase[fi] := ReflectionAmplitudePhase[fi] * TMultiLayerReflectionCoefficient.calcR(f[fi], thisBottom.theta1, layers);
            end
            else
                inSeafloor := false;
        end;
        procedure updateRayParameters();
        var
            fi: integer;
            k1,k2,k3, range: double;
            k1Width, k2Width, k3Width: double;
        begin
            for fi in solveFreqInds do
            begin
                rayPhase[fi] := rayPhase[fi] + (2 * pi * f[fi])/cHere*stepDist;

                if sign(xnext.q) <> sign(xprev.q) then
                    rayPhase[fi] := rayPhase[fi] + pi;  //phase change at caustics

                rayHeight[fi] := abs(xnext.q * delTheta);
                rayWidth[fi] := abs(xnext.q * delPhi);

                if (not bPastRayWidthXOver[fi]) and (rayHeight[fi] > pi*cHere/f[fi]) then
                    bPastRayWidthXOver[fi] := true;

                if bPastRayWidthXOver[fi] then
                    rayHeight[fi] := max(pi*cHere/f[fi],rayHeight[fi]);

                //todo
                range := hypot(xnext.x - srcX, xnext.y - srcY);
                k1 := delTheta/range*cHere/c0;    //eq 3.76
                k2 := cosTheta0/rayHeight[fi];
                k3 := sqrt(k1*2*k2);
                Abeam[fi] := konst*k3;

                //Abeam[fi] := 1/power(2*pi,0.25)*sqrt(delTheta/xnext.x*cHere/c0*2*cosTheta0/rayWidth[fi]);
                rayHeight[fi] := min(thisBottom.z,rayHeight[fi]); //reduce ray height when the channel is shallow. but keep intensity as it would have been

                k1Width := delPhi/range*cHere/c0;
                k2Width := cosPhi0/rayWidth[fi];
                k3Width := sqrt(k1Width*2*k2Width);
                AbeamWidth[fi] := konst*k3Width;
            end;
        end;

        procedure intensityCalcs3();
        var
            dx, dy, ang, dist, distToRay, distToRayWidth, magnitude: double;
            n,ir,iz,fi: integer;
            cosA, sinA: double;
        begin
            //not trying to find bounding box, just the nearest point

            dx := xnext.x - srcX;
            dy := xnext.y - srcY;
            ang := arctan2(dy,dx);
            if ang < 0 then
                ang := ang + 2*pi;
            n := round(ang / 2 / pi * nSlices) mod nSlices;
            system.SineCosine(ang,sinA,cosA);

            dist := hypot(dx,dy);
            ir := round(dist / dr);
            if ir >= nr then exit;

            if xnext.z < 0 then exit;
            iz := round(xnext.z / deltaZ);
            if iz >= nz then exit;

            for fi in solveFreqInds do
            begin
                //todo CHECK THIS
                distToRay := TSolversGeneralFunctions.distPointToLineSegment3d(0,0,z[iz],0,0,xprev.z,0,0,xnext.z); //takes account of line segment endpoints
                distToRayWidth := TSolversGeneralFunctions.distPointToLineSegment3d(srcX + cosA * dist,srcY + sinA * dist,0,xprev.x,xprev.y,0,xnext.x,xnext.y,0); //takes account of line segment endpoints

                if (distToRay > 2*rayHeight[fi]) then continue; //2*raywidth gets us to 2 StdDev, which is around -30 dB
                if (distToRayWidth > 2*rayWidth[fi]) then continue;

                //magnitude := (1 - distToRay/rayWidth); //hat function
                magnitude := exp(-sqr(distToRay/rayHeight[fi])) * exp(-sqr(distToRayWidth/rayWidth[fi])); //gaussian -- we can speed this line up

                if outputArrivals then
                begin
                    {add this arrival to the arrivals list for this point}
                    localArrivals.addArrival(fi, ir, n, iz,
                                        ReflectionAmplitudePhase[fi].r*Abeam[fi]*ABeamWidth[fi]*magnitude*cos(rayPhase[fi]),
                                        delay);
                end
                else
                begin
                    {add the contribution from this ray at this point to the output field}
                    localPFieldMultiFreq^[n,fi,iz,ir] := localPFieldMultiFreq^[n,fi,iz,ir] + ComplexEXP(makeComplex(0,rayPhase[fi])) *
                                                                             ReflectionAmplitudePhase[fi] *
                                                                             (Abeam[fi]*ABeamWidth[fi]*magnitude); //includes accumulated reflection amplitude/phase
                end;
            end;
        end;

    begin
        setlength(rayPhase,nf);
        setlength(rayHeight,nf);
        setlength(rayWidth,nf);
        setlength(bPastRayWidthXOver,nf);
        setlength(Abeam,nf);
        setlength(ABeamWidth,nf);
        setlength(ReflectionAmplitudePhase,nf);
        layers := TSmart<TSeafloorReflectionLayers>.create(TSeafloorReflectionLayers.create(true));

        ray := nil;
        phi0 := phi[iPhi];
        system.SineCosine(phi0, sinPhi0, cosPhi0);
        if outputsliceRays then
            sliceRays := sliceRayss.addSlice(iPhi)
        else
            sliceRays := nil;
        for itheta := 0 to ntheta - 1 do
        begin
            {$IF defined (DEBUG) and defined (RAYOUTPUT)}if itheta > 0 then s := s + #13#10 + '</ray>' + #13#10; s := s + '<ray>' + #13#10;{$ENDIF}
            {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'ray ' + IntToStr(itheta) + #13#10;{$ENDIF}

            //thisBottom.count := 0;

            //UI feedback
            //if Assigned(updateLabelProc) and (itheta mod 20 = 0) then updateLabelProc('Ray ' + tostr(itheta + 1) + ' of ' + tostr(ntheta));
            if (cancel <> nil) and cancel.b then exit;

            theta0 := theta[itheta];
            for fi in solveFreqInds do
            begin
                ReflectionAmplitudePhase[fi] := undB20(-directivityMat[fi,itheta]); //amplitude from the directivity: NB -ve
                rayPhase[fi] := 0;
                bPastRayWidthXOver[fi] := false;
            end;
            maxInitialAmplitude := maxAmplitude();
            delay := 0; //keeping track of time taken, for arrivals calc
            ds := initialStepsize; //initial stepsize

            system.SineCosine(theta0, sinTheta0, cosTheta0);
            //ray initial conditions: r,z,zeta,q,p
            xnext.x := srcX;
            xnext.y := srcY;
            xnext.z := srcZ;
            xnext.xi := cosTheta0*cosPhi0/c0;   //3.266
            xnext.eta := cosTheta0*sinPhi0/c0;  //3.266
            xnext.zeta := sinTheta0/c0;           //3.266
            xnext.q := 0;
            xnext.p := 1/c0;

            cHere := c[min(length(c) - 1,max(0,saferound(srcZ)))];
            inSeafloor := false;
            inAir := false;
            nReflections := 0;

            //source can't be below the seafloor (currently)
            //TODO how to get the depth here?
            xprev.cloneFrom(xnext);
            xprev.x := xprev.x + 1; //to avoid problems in rayHitSeafloor3D
            if TSolversGeneralFunctions.rayHitSeafloor3D(depths,xmax/length(depths[0]),ymax/length(depths),cHere,ds,xnext,xprev,thisBottom) then
            //if srcZ > aZProfile2[0].val then
            begin
                include(localSolverErrors,kSourceBelowSeafloorRaySolver);
                break;
            end;

            if outputsliceRays then
                ray := sliceRays.addRay;

            while true do
            begin
                if (cancel <> nil) and cancel.b then break;

                xprev.cloneFrom(xnext);

                {$IF defined (DEBUG) and defined (RAYOUTPUT)}s := s + floattostr(round1(xprev.x)) + ',' + floattostr(round1(xprev.z)) + ';';{$ENDIF}
                {$IF defined (DEBUG) and defined (TESTRAY)}{write(outFile, floattostr(round1(xprev.x)) + ',' + floattostr(round1(xprev.z)) + ';');}{$ENDIF}

                {take a step, via 2nd or 4th order runge kutta (depending on whether the SSP is const or not)}
                iz := min(length(c) - 1,max(0,saferound(xprev.z)));
                if ((raySolverIterationMethod = kConstSSP) and constSSP) or (raySolverIterationMethod = kRK2) then
                    xnext := TSolversGeneralFunctions.rk2Step3D(xprev,ds,c[iz],dcdz[iz],d2cdz2[iz])
                else
                    xnext := TSolversGeneralFunctions.rk4Step3D(xprev,ds,c[iz],dcdz[iz],d2cdz2[iz]);
                //todo smaller stepsize accuracy checking/rk-fehlberg

                {$IF defined (DEBUG) and defined (TESTHEADING)}
                    //try get just the ray we want to test
                    //if itheta <> 5 then break;

                    prevHeading := heading;
                    heading := ArcTan2(xnext.y - xprev.y, xnext.x - xprev.x);
                    if heading > prevHeading then
                        heading := heading + 1.0 - 1.0;
                {$ENDIF}

                if assigned(ray) then
                    ray.addPos3D(xnext);

                if isnan(xprev.x) or isnan(xprev.y) or isnan(xprev.z) then
                begin
                    inc(localRayDeaths[kError]);
                    break; //todo but why nan? eg repeated points in SSP
                end;

                if inAirCheck_shouldBreak then break;

                if inSeabedCheck_shouldBreak(layers) then break;

                if thisBottom.outOfBounds then
                begin
                    //range checks - range < 0 means the ray exited the bounding box to the left
                    if (xnext.x < 0) then
                    begin
                        {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'KWEST ray ' + IntToStr(itheta) + ' xnext.x ' + floattostr(xnext.x) + ' xnext.y ' + floattostr(xnext.y) + #13#10;{$ENDIF}
                        inc(localRayDeaths[kWest]);
                    end;

                    if (xnext.x >= xmax) then
                    begin
                        {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'KEAST ray ' + IntToStr(itheta) + ' xnext.x ' + floattostr(xnext.x) + ' xnext.y ' + floattostr(xnext.y) + #13#10;{$ENDIF}
                        inc(localRayDeaths[kEast]);
                    end;

                    if (xnext.y < 0) then
                    begin
                        {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'KSOUTH ray ' + IntToStr(itheta) + ' xnext.x ' + floattostr(xnext.x) + ' xnext.y ' + floattostr(xnext.y) + #13#10;{$ENDIF}
                        inc(localRayDeaths[kSouth]);
                    end;

                    if (xnext.y >= ymax) then
                    begin
                        {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'KNORTH ray ' + IntToStr(itheta) + ' xnext.x ' + floattostr(xnext.x) + ' xnext.y ' + floattostr(xnext.y) + #13#10;{$ENDIF}
                        inc(localRayDeaths[kNorth]);
                    end;

                    break;
                end;

                if thisBottom.z <= 0 then
                begin
                    {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'ray ' + IntToStr(itheta) + ' thisBottom.z <= 0' + #13#10;{$ENDIF}
                    inc(localRayDeaths[kLand]);
                    break;
                end;

                iz1 := min(length(c) - 1,max(0,saferound(xnext.z)));
                iz2 := min(length(c) - 1,max(0,saferound(xprev.z)));
                cHere := (c[iz1] + c[iz2])/2; //average c for this step
                if inAir or inSeafloor then
                    stepDist := ds
                else
                    stepDist := sqrt(sqr(xnext.x - xprev.x) + sqr(xnext.y - xprev.y) + sqr(xnext.z - xprev.z));
                delay := delay + stepDist / cHere; //keep track of time delay, for arrivals calc

                if calculateVolumeAttenuationAtEachStep then
                    for fi in solveFreqInds do
                    begin
                        localAttenuation := TWaterProps.seawaterAbsorptionCoefficient(waterTemp,f[fi],(xnext.z + xprev.z)/2);
                        localAttenuation := undB20(-localAttenuation*stepDist);
                        ReflectionAmplitudePhase[fi] := ReflectionAmplitudePhase[fi]*localAttenuation;
                    end;

                if (not isnan(maxInitialAmplitude)) and (not isnan(minAmplitude())) then
                    if ((minAmplitude() / maxInitialAmplitude) < amplitudeCutoff) then
                    begin
                        {$IF defined (DEBUG) and defined (TESTING)}write(outFile, 'KAMPLITUDE ray ' + IntToStr(itheta) + ' xnext.r ' + floattostr(xnext.x) + #13#10);{$ENDIF}
                        inc(localRayDeaths[kAmplitude]);
                        break;
                    end;

                updateRayParameters();

                intensityCalcs3();
            end; //not rayTerminate, do intensity calcs
            //fractionDone.d := (iPhi + itheta / ntheta) / nPhi;  //todo change if parallel
            internalFractionDone[iPhi].d := itheta / ntheta;
            fractionDone.d := internalFractionDone.totalFractionDone();
        end; //for itheta
    end);

    {$IF defined (DEBUG) and defined (RAYOUTPUT)}
    s := s + #13#10 + '</ray>';
    {$ENDIF}

    if (not outputArrivals) and returnTL then
        tlOutput(nSlices,nf,nz,nr);

    {$IF defined (RAYOUTPUT) or defined (Testtray) or defined (TESTHEADING)}
    THelper.setClipboard(s);
    {$ENDIF}

    for aRayDeath := Low(TRayDeath) to High(TRayDeath) do
        deaths[aRayDeath] := deaths[aRayDeath] + localRayDeaths[aRayDeath];

    for aSolverError := Low(TSolverError) to High(TSolverError) do
        if aSolverError in localSolverErrors then
            include(solverErrors, aSolverError);
end;



end.
