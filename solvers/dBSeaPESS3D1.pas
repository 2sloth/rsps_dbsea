
unit dBSeaPESS3D1;

{PESS3D1: 3d split step, solution by 2d fft.
    Following Duda THREE-DIMENSIONAL SOUND PROPAGATION MODELS USING THE
    PARABOLIC-EQUATION APPROXIMATION AND THE SPLIT-STEP FOURIER METHOD}

interface

uses
    system.classes, system.math, system.sysutils, system.types, system.threading,
    vcl.forms,
    helperFunctions,
    fourier, matrices, MComplex,
    dBSeaconstants, baseTypes,
    seafloorScheme, material,
    directivity, solversGeneralFunctions, mcklib, globalsUnit,
    PadeApproximants, TriDiagonal,
    OtlParallel, otlcommon, OtlCommon.Utils,
    otltask, OtlTaskControl, OtlThreadPool;

type
  TdBSeaPESS3D1Solver = class(TObject)
  public
    //mainFFT isn't working right now. Seems to give 'banded' solutions
    class function mainFFT(const f, srcZ, rmax, dr, zmax: double;
                        const dMax, nSlices: integer;
                        const aZProfile2: TArray<TRangeValArray>;
                        const aCProfile2: TDepthValArray;
                        const starter: TStarter;
                        const seafloorScheme: TSeafloorScheme;
                        const pCancel: TWrappedBool;
                        var solverErrors: TSolverErrors): TDoubleField;
//    class function mainPadeOTL(const f, srcZ, rmax, dr, zmax: double;
//                        const dMax, nSlices, nPadeTerms: integer;
//                        const aZProfile2: TArray<TRangeValArray>;
//                        const aCProfile2: TArray<TDepthValArray>;
//                        const starter: TStarter;
//                        const seafloorScheme: TSeafloorScheme;
//                        const pCancel: TWrappedBool;
//                        const fractionDone: TWrappedDouble): TDoubleField;
    class function mainPade(const f, srcZ, rmax, dr, zmax: double;
                        const dMax, nSlices, nPadeTerms, nPadeTermsHorizontal: integer;
                        const rInit: TDoubleDynArray;
                        const aZProfile2: TArray<TRangeValArray>;
                        const aCProfile2: TArray<TDepthValArray>;
                        const zOversamplingFactor, rOversamplingFactor, phiOversamplingFactor: double;
                        const seafloorScheme: TSeafloorScheme;
                        const starter: TStarter;
                        const pCancel: TWrappedBool;
                        const fractionDone: TWrappedDouble;
                        var solverErrors: TSolverErrors): TDoubleField;
  end;

implementation

{$B-}

class function TdBSeaPESS3D1Solver.mainFFT(const f, srcZ, rmax, dr, zmax: double;
                               const dMax, nSlices: integer;
                               const aZProfile2: TArray<TRangeValArray>;
                               const aCProfile2: TDepthValArray;
                               const starter: TStarter;
                               const seafloorScheme: TSeafloorScheme;
                               const pCancel: TWrappedBool;
                               var solverErrors: TSolverErrors): TDoubleField;
var
    c1: TDoubleDynArray;
    psiComplexMat: TComplexMat; {phi,z}
    layerDensityList: TArray<TDepthValArray>;
    omega, c0,rho0,k0,k02: double;
    deltaz,deltar,deltazOutput: double;
    atten, c_ir, rhoThis: double;
    zbThis,zbPrev: TdoubleDynArray;
    nPhi,nz,nr: integer;
    attCConst: double;
    ir,iz,izz,iPhi: integer;
    nSqr: double;
    q1: double;
    SubDepth1,SubDepth2: double;
    dhank2: double;
    dBottomExtension: double;
    dpress2: double;
    aMaterial: TMaterial;
    nSqz: TComplexMat;
    z: TDoubleDynArray;

    f1Phi,f2Phi,f1z,f2z: array of TVector2;

    om: TComplexField; //output matrix
    wns: TComplexMat; //wavenumberspectrum --> this is what we are marching, eq 14

    function r(const ir: integer): double;
    begin
        result := ir * deltar;
    end;

    function localC(const iz: integer; const zbThis: double): double;
    var
        aMaterial: TMaterial;
    begin
        if z[iz] < zbThis then
            result := c1[iz]         //ssp c
        else
        begin
            //todo seafloor referenced from sea surface?
            aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz], zbThis);
            if assigned(aMaterial) then
                result := aMaterial.c
            else
                result := water.c;
        end;
    end;

    // fftSolverStep3 splits the nSq step into 2, is more accurate than fftSolverStep1
    //NB we march the wavenumber spectrum
    procedure fftSolverStep3_3d(const w: TComplexMat; const r: double);
    var
        iz,iPhi,lenPhi,lenz: integer;
        q: TComplex;
        kz, k02, abskTransSq, thisR: double;
    begin
        //eq 6.131
        if (length(f1Phi) = 0) or (length(f1z) = 0) then exit;

        lenz := f1Phi[0].length;
        lenPhi := f1z[0].length;
        k02 := sqr(k0);
        thisR := max(deltar, r);

        //X = F^-1 [Q*om]
        for iPhi := 0 to lenPhi div 2 - 1 do
        begin
            f1Phi[iPhi][0] := NullComplex;
            f1Phi[iPhi][lenz - 1] := NullComplex;
            f1Phi[lenPhi - 1 - iPhi][0] := NullComplex;
            f1Phi[lenPhi - 1 - iPhi][lenz - 1] := NullComplex;
            for iz := 1 to lenz div 2 - 1 do
            begin
                kz := pi * iz / lenz / 2 / deltaz;
                abskTransSq := k02/sqr(thisR) + sqr(kz);  //between eq 12 and eq 13
                q := complexExp(MulComplexParI(deltar*(-k0 + complexSqrt(k02 - abskTransSq)))); //eq 16
                f1Phi[iPhi][iz] := w[iPhi,iz] * q;
                f1Phi[iPhi][lenz - 1 - iz] := -w[iPhi,iz] * q; //negative reflection 'above surface' so we have antisymmetric fft input
                f1Phi[lenPhi - 1 - iPhi][iz] := -w[lenPhi - 1 - iPhi,iz] * q;
                f1Phi[lenPhi - 1 - iPhi][lenz - 1 - iz] := w[lenPhi - 1 - iPhi,iz] * q;
            end;
            ifft(lenz,f1Phi[iPhi],f2Phi[iPhi]);
            ifft(lenz,f1Phi[lenPhi - 1 - iPhi],f2Phi[lenPhi - 1 - iPhi]);
            for iz := 1 to lenz - 1 do
            begin
                f1z[iz][iPhi] := f2Phi[iPhi][iz];
                f1z[iz][lenPhi - 1 - iPhi] := f2Phi[lenPhi - 1 - iPhi][iz];
            end;
            f1z[0][iPhi] := NullComplex;
            f1z[lenz - 1][iPhi] := NullComplex;
            f1z[0][lenPhi - 1 - iPhi] := NullComplex;
            f1z[lenz - 1][lenPhi - 1 - iPhi] := NullComplex;
        end;
        for iz := 1 to lenz - 1 do
        begin
            ifft(lenPhi,f1z[iz],f2z[iz]);

            //divide by range, eq 13.2
            for iPhi := 0 to lenPhi - 1 do
                f2z[iz][iPhi] := f2z[iz][iPhi] / thisR;
        end;

        //F[B*X]
        for iz := 0 to lenz div 2 - 1 do
        begin
            for iPhi := 0 to nPhi - 1 do
            begin
                q := ComplexEXP(mulcomplexParI(k0*deltar*(nSqz[iPhi,iz] - 1)));  //eq 15
                f2z[iz][iPhi] := f2z[iz][iPhi] * q;
                f2z[iz][2*nPhi - 1 - iPhi] := f2z[iz][iPhi] * q;
                f2z[lenz - 1 - iz][iPhi] := f2z[lenz - 1 - iz][iPhi] * q;
                f2z[lenz - 1 - iz][2*nPhi - 1 - iPhi] := f2z[lenz - 1 - iz][iPhi] * q;
            end;
        end;
        for iz := 0 to lenz - 1 do
        begin
            fft(lenPhi, f2z[iz], f1z[iz]);

            for iPhi := 0 to lenPhi - 1 do
                f2Phi[iPhi][iz] := f1z[iz][iPhi];
        end;
        for iPhi := 0 to lenPhi - 1 do
        begin
            ifft(lenz,f2Phi[iPhi],f1Phi[iPhi]);

            //multiply by range, eq 13.2
            //if lenPhi < (lenPhi div 2) then
                for iz := 0 to lenz - 1 do
                    w[iPhi,iz] := f1Phi[iPhi][iz] * thisR;
        end;
    end;

    procedure toWaveNumberSpectrum(const p, w: TComplexMat; const r: double);
    var
        iz,iPhi,lenPhi,lenz: integer;
        q: TComplex;
        thisR: double;
    begin
        if (length(f1Phi) = 0) or (length(f1z) = 0) then exit;

        lenz := f1Phi[0].length;
        lenPhi := f1z[0].length;
        thisR := max(deltar, r);

        q := sqrt(thisR) / ComplexEXP(MakeComplex(0,k0*thisR)); //eq 4
        for iz := 0 to lenz div 2 - 1 do
        begin
            for iPhi := 0 to nPhi - 1 do
            begin
                f2z[iz][iPhi] := p[iPhi][iz] * q;
                f2z[iz][2*nPhi - 1 - iPhi] := p[iPhi][iz] * q;
            end;

            fft(lenPhi, f2z[iz], f1z[iz]);
        end;
        for iz := 0 to lenz div 2 - 1 do
        begin
            //if iz < (lenz div 2) then
            for iPhi := 0 to lenPhi - 1 do
            begin
                w[iPhi][iz] := f1z[iz][iPhi]; //todo does this need to be double length?
                w[iPhi][lenz - 1 - iz] := f1z[iz][iPhi];
            end;
        end;
    end;

    procedure fromWaveNumberSpectrum(const w,p: TComplexMat; const r: double);
    var
        iz,iPhi,lenPhi,lenz: integer;
        q: TComplex;
        thisR: double;
    begin
        if (length(f1Phi) = 0) or (length(f1z) = 0) then exit;

        lenz := f1Phi[0].length;
        lenPhi := f1z[0].length;
        thisR := max(deltar, r);

        q := ComplexEXP(MakeComplex(0,k0*thisR)) / sqrt(thisR); //eq 4
        for iz := 0 to lenz div 2 - 1 do
        begin
            for iPhi := 0 to lenPhi - 1 do
            begin
                f2z[iz][iPhi] := w[iPhi][iz];
                f2z[lenz - 1 - iz][iPhi] := w[iPhi][lenz - 1 - iz];
            end;

            ifft(lenPhi, f2z[iz], f1z[iz]);
        end;
        for iz := 0 to lenz div 2 - 1 do
        begin
            if iz < (lenz div 2) then
                for iPhi := 0 to nPhi - 1 do
                    p[iPhi][iz] := f1z[iz][iPhi] * q;
        end;
    end;
begin
    omega := 2 * pi * f;  //angular freq

    nPhi := nextPow2(nSlices);

    {water properties}
    c0 := aCProfile2.average;
    rho0 := water.rho_g;  //water density = 1 g/cm3
    k0 := omega / c0;
    k02 := sqr(k0);

    {sub bottom sediment stuff}
    dBottomExtension := 2;
    if (zmax * dBottomExtension) < c0 / f then
        dBottomExtension := c0 / f / zmax;

    SubDepth1 := zmax * (dBottomExtension - 0.5);
    SubDepth2 := zmax * dBottomExtension;

    {depths}
    deltazOutput := zmax / dmax; //used for psiM
    nz := 1 + (round(dBottomExtension * dMax) - 1);
    //nz must be a power of 2 for fft. Also we will wrap around 0 depth
    nz := nextPow2(nz);
    deltaz := SubDepth2 / (nz-1);
    //deltaz2 := deltaz * deltaz;
    SetLength(z,nz);
    for iz := 0 to nz - 1 do
        z[iz] := deltaz * iz;

    {sound speed profile and wavenumbers}
    setlength(c1,nz);
    for iz := 0 to nz - 1 do
        c1[iz] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2,z[iz]);

    {ranges}
    nr := max(1, 1 + (max(1, saferound(rmax/dr,1)) - 1));
    deltar := rmax / max(1, (nr - 1));
    if deltar = 0 then
        deltar := 1;

    setlength(om,nr,nPhi,nz); //Duda eq 14 except we put r first for convenience
    setlength(psiComplexMat,nPhi,nz);
    setlength(wns,2*nPhi,2*nz);
    setlength(result,nPhi,dMax,nr);
    setlength(zbThis,nPhi);
    setlength(zbPrev,nPhi);
    setlength(layerDensityList,nPhi);
    setlength(nSqz,nPhi,nz);
    SetLength(f1Phi, nPhi*2);
    SetLength(f2Phi, nPhi*2);
    for iPhi := 0 to nPhi*2 - 1 do
    begin
        setlength(f1Phi[iPhi].re, nz*2);
        setlength(f1Phi[iPhi].im, nz*2);
        setlength(f2Phi[iPhi].re, nz*2);
        setlength(f2Phi[iPhi].im, nz*2);
    end;

    SetLength(f1z, nz*2);
    SetLength(f2z, nz*2);
    for iz := 0 to nz*2 - 1 do
    begin
        setlength(f1z[iz].re, nPhi*2);
        setlength(f1z[iz].im, nPhi*2);
        setlength(f2z[iz].re, nPhi*2);
        setlength(f2z[iz].im, nPhi*2);
    end;

    if (srcZ < 0) or (srcZ > nz*deltaz) then
    begin
        include(solverErrors,kSourceBelowSubDepth);
        exit;
    end;

    {sediment layers}
    //TODO support for changing seafloor scheme in range
    for iPhi := 0 to nPhi - 1 do
    begin
        psiComplexMat[iPhi] := TSolversGeneralFunctions.PEStarterInterpolation(deltaz,nz,starter.Z,starter.psi);
        if iPhi < nslices then //todo interp
            zbThis[iPhi] := TSolversGeneralFunctions.interpolationFunPE(aZProfile2[iPhi],0);
        zbPrev[iPhi] := nan;
        seafloorScheme.getLayerDensityList(zbThis[iPhi], rho0, layerDensityList[iPhi]);

        for iz := 1 to dMax - 1 do
        begin
            {copy over to the output matrix. If starterR is large, we may need to copy the starter to multiple ranges}
            if iz = 1 then
                om[0, iPhi, 1 ] := psiComplexMat[iPhi,1] // todo interp psiComplexVect[iz] //shift down by 1 due to pressure release surface
            else
                om[0, iPhi, iz] := TSolversGeneralFunctions.interpolationFunPE(starter.z, starter.psi, deltazOutput * iz); //z spacing differs between the solvers, so interpolate
        end;
    end;

    toWaveNumberSpectrum(psiComplexMat, wns, 0);

    c_ir := 1500; // stop compiler warning
    THelper.RemoveWarning(c_ir);
    attCConst := 1/(20*log10(exp(1))/f); //convert to nepers/m    eq 6.156 (lambda = c / f and we include c(z) later)
    //nilSedimentError := false;
    //-------------START RANGE LOOPING-------------------------
    for ir := 0 to nr - 2 do
    begin
        //calc next om[ir + 1]

        for iPhi := 0 to nPhi - 1 do
            if zbThis[iPhi] <> zbPrev[iPhi] then
                for iz := 0 to nz - 1 do
                begin
                    if z[iz] < zbThis[iPhi] then
                    begin
                        c_ir := c1[iz];         //ssp c
                        atten := 0;// atten0*attC;   //water attenuation (todo do bulk attenuation here)
                    end
                    else
                    begin
                        aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz] , zbThis[iPhi]); //could move this inside checks for seafloor
                        if not assigned(aMaterial) then
                        begin
                            //nilSedimentError := true;
                            aMaterial := water;
                        end;

                        c_ir := aMaterial.c;       //sediment c
                        atten := aMaterial.attenuation*attCConst/c_ir;  //sediment attenuation

                        if z[iz] > SubDepth1 then
                            atten := atten + TSolversGeneralFunctions.additionalAttenuationInBottomLayer(z[iz],subDepth1,subDepth2);//increase attenuation at the bottom of the sediment layer
                    end;

                    {check if close enough to the bottom to consider calculating the
                    density difference function (function is 0 for constant density)}

                    //get rho, check if we are near an interface and so need to smooth rho

                    TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[iPhi],
                                                                     z[iz],  //this depth
                                                                     k0,
                                                                     k02,
                                                                     rhoThis, //rho gets set
                                                                     q1); //q1 gets set

                    nSqr := c0*c0 / c_ir/c_ir; //nsquared, not n
                    nSqz[iPhi,iz].r := nSqr + q1; //add in the contribution from the sediment density change
                    nSqz[iPhi,iz].i := (nSqr + q1)*atten; //q1 has no imaginary part eq 6.158
                end; //iz, iphi

        //--------------------------------March solution in range------------------------------
        fftSolverStep3_3d(wns,r(ir));
        fromWaveNumberSpectrum(wns,psiComplexMat,r(ir));
        //psiComplexMat is now for ir+1

        for iPhi := 0 to nPhi - 1 do
        begin
            zbPrev[iPhi] := zbThis[iPhi];
            if iPhi < nslices then //todo
                zbThis[iPhi] := TSolversGeneralFunctions.interpolationFunPE(aZProfile2[iPhi],r(ir + 1));

            //currently at land
            //if zbThis[iPhi] <= 0 then
            //    for izz := 0 to nz - 1 do
            //        wns[iPhi,izz] := NullComplex;

            //get the seabed layers at this location
            if not isnan(zbThis[iPhi]) and (zbThis[iPhi] <> zbPrev[iPhi]) then
                seafloorScheme.getLayerDensityList(zbThis[iPhi], water.rho_g, layerDensityList[iPhi]);

            {put the outgoing psi vector into our psiM matrix, un-reducing by sqrt(c*rho)}
            for izz := 0 to dMax - 1 do
            begin
                iz := ifthen(izz = 0, 1, izz); //shift down by 1, as we force psiComplexVect[0] = 0
                TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[iPhi],z[iz],k0,k02,rhoThis,q1);
                om[ir + 1,iPhi,izz] := psiComplexMat[iPhi,izz] * sqrt(localC(iz, zbThis[iPhi])*rhoThis); //un-reduce by sqrt(c*rho) see sec 6.8 p508
            end;
        end;

        //UI feedback
        if (pcancel <> nil) and pcancel.b then exit;
        //fractionDone.d := ir / nr;
    end; //ir

    {put the abs(hankel function) back into the envelope function to get the pressure.
    6.5 gives Ho(1)(k0r) = sqrt(2/(pi*k0*r)) exp(i *(k0r - pi/4))
    we can ignore the complex exponential as we just want magnitude.
    so squaring the magnitude part of 6.5 we have 2/(pi*k0*r). NB k0r >> 1 for this approximation}

    {if we have a modal starterR, the starter was calculated at a distance out from the source or starterR.
    We want our hankel func to pass through 1 at range starterR (and be == 1 before that). For the fallback starters, use a normal hankel func}
    for ir := 0 to nr - 1 do
    begin
        if (r(ir) <= 1) then
            dhank2 := 1
        else
            dhank2 := 1 / r(ir);

        for iPhi := 0 to nPhi - 1 do
            for iz := 0 to dMax - 1 do
            begin
                dpress2 := om[ir,iPhi,iz].modulusSquared * dhank2; {eq 6.3 squared}
                if (dpress2 > 0) then
                    result[iPhi,iz,ir] := -10*log10(dpress2);
            end;
    end;
end;


//class function TdBSeaPESS3D1Solver.mainPadeOTL(const f, srcZ, rmax, dr, zmax: double; const dMax, nSlices, nPadeTerms: integer; const aZProfile2: TArray<TRangeValArray>; const aCProfile2: TArray<TDepthValArray>; const starter: TStarter; const seafloorScheme: TSeafloorScheme; const pCancel: TWrappedBool; const fractionDone: TWrappedDouble): TDoubleField;
//const
//    starterR = 1.0;
//var
//    c1: TMatrix;
//    Cz, Bz: TArray<TArray<TTriDiagonal>>;
//    Btheta,Ctheta,CShortTheta: TArray<TTriDiagonal>;
//    psiComplexMat: TComplexMat; {theta,z}
//    layerDensityList: TArray<TDepthValArray>;
//    omega, c0,rho0,k0,k02: double;
//    deltaz,deltar,deltazOutput,dTheta: double;
//    atten, cThis, rhoThis, rhoPrev: double;
//    zbThis,zbPrev: TdoubleDynArray;
//    nz,nr,k: integer;
//    attCConst: double;
//    ir,iz,itheta,nrInit,rOversamp: integer;
//    nSqr,nSqrTilde: double;
//    nSq1,nSq2: TComplex;
//    q1: double;
//    SubDepth1,SubDepth2: double;
//    dhank2,fac: double;
//    iBottomExtension: double;
//    dpress2: double;
//    aMaterial: TMaterial;
//    nSqz: TComplexMat;
//    z: TDoubleDynArray;
//    rhsN,tmpCUpperN,tmpRhsN: TComplexMat;
//    rhsTheta, tmpCUpperTheta, tmpRhsTheta, pTrans: TComplexMat;
//    xoneTheta,xtwoTheta,rhsShortTheta,firstAndLastOnlyRhsShortTheta,tmpUpperShortTheta,tmpRhsShortTheta: TComplexMat;
//    diagTerms: TArray<TDiagTerms>;
//    padeTerms: TPadeTerms;
//    k0SqdeltazSqOver2: double;
//    matrixFormers: TArray<TMatrixFormer>;
//    deltaROverk0OverRSquared: double;
//    workerSlice, workerTheta: IOmniBackgroundWorker;
//    allSliceJobs, allThetaJobs: TArray<TArray<IOmniWorkItem>>;
//    sliceJobs, thetaJobs: TArray<IOmniWorkItem>;
//    irLoopFunc, outputFunc: TProc;
//    rFunc: TFunc<integer,double>;
//    localCFunc: TFunc<integer,integer,double,double>;
//    starterPointer: pStarter;
//    pResult: pDoubleField;
//    om: TComplexField;
//
//
////    procedure padeStep_3d_parallel(const p: TComplexMat; const r: double);
////    var
////        n: integer;
////    begin
//        //in place version for less memory assigns
//        {we are solving C psi(ir+1) = B psi(ir), where C and B are tridiagonal, for each pade term}
//
////        //for n := 0 to nSlices - 1 do
////        //Parallel.For(0,nSlices - 1)
////        //    .Execute(procedure (n: integer)
////        tparallel.for(0, nslices - 1, procedure (n: integer)
////        var
////            k, iz: integer;
////        begin
////            for k := 0 to max(1, nPadeTerms) - 1 do
////            begin
////                //form rhs the right hand side, ie B*psi(ir). Tridiagonal.
////                TSolversGeneralFunctions.TDMAMultiplyComplexInPlace(Bz[n,k],p[n],rhsN[n]);
////                //solve C psi(ir+1) = rhs for psi(ir+1). Can use the tridiagonal solver method
////                TSolversGeneralFunctions.TDMASolverComplexInPlace(Cz[n,k],rhsN[n],tmpCUpperN[n],tmpRhsN[n],p[n]);
////            end;
////
////            //get the current field in theta coords
////            for iz := 0 to nz - 1 do
////                pTrans[iz,n] := p[n,iz];
////        end);
//
////        //todo oversample in theta
////        //Parallel.for(0, nz - 1)
////        //   .execute(procedure (iz: integer)
////        tparallel.for(0, nz - 1, procedure (iz: integer)
////        var
////            k, n: integer;
////        begin
////            for k := 0 to max(1, nPadeTerms) - 1 do
////            begin
////                TSolversGeneralFunctions.TDMAMultiplyComplexInPlaceWrap(Btheta[k],pTrans[iz],rhsTheta[iz]);
////
////                TSolversGeneralFunctions.TDMASolverComplexInPlaceWrap(Ctheta[k],
////                    CShortTheta[k],
////                    rhsTheta[iz],
////                    tmpCUpperTheta[iz],
////                    tmpRhsTheta[iz],
////                    xoneTheta[iz],
////                    xtwoTheta[iz],
////                    rhsShortTheta[iz],
////                    firstAndLastOnlyRhsShortTheta[iz],
////                    tmpUpperShortTheta[iz],
////                    tmpRhsShortTheta[iz],
////                    pTrans[iz]);
////            end;
////
////            //get the field back in z coords
////            for n := 0 to nSlices - 1 do
////                p[n,iz] := pTrans[iz,n];
////        end);
//
////        for n := 0 to nSlices - 1 do
////            sliceJobs[n] := workerSlice.CreateWorkItem(n);
////        for n := 0 to nSlices - 1 do
////            workerSlice.Schedule(sliceJobs[n]);
//
////        //todo do this via callbacks
////        while not sliceDone do
////            sleep(5);
////
////        for n := 0 to nSlices - 1 do
////            sliceJobs[n] := nil;
////
////        for iz := 0 to nz - 1 do
////        begin
////            thetaJobs[iz] := workerTheta.CreateWorkItem(iz);
////            workerTheta.Schedule(thetaJobs[iz]);
////        end;
////
////        //todo do this via callbacks
////        while not thetaDone do
////            sleep(5);
////
////        for iz := 0 to nz - 1 do
////            thetaJobs[iz] := nil;
////
////        for n := 0 to nSlices - 1 do
////            p[n,0] := NullComplex; //enforce pressure release surface
////    end;
//
//
//begin
//    starterPointer := @starter;
//    rFunc := function(ir: integer): double begin result := ir*deltar; end;
//
//    localCFunc := function(itheta,iz: integer; zbThis: double): double
//    var
//        aMaterial: TMaterial;
//    begin
//        if z[iz] < zbThis then
//            result := c1[itheta,iz]         //ssp c
//        else
//        begin
//            //todo seafloor referenced from sea surface?
//            aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz], zbThis);
//            if assigned(aMaterial) then
//                result := aMaterial.c
//            else
//                result := water.c;
//        end;
//    end;
//
//    outputFunc := procedure()
//    var
//        ir, itheta, iz: integer;
//    begin
//        {put the abs(hankel function) back into the envelope function to get the pressure.
//        6.5 gives Ho(1)(k0r) = sqrt(2/(pi*k0*r)) exp(i *(k0r - pi/4))
//        we can ignore the complex exponential as we just want magnitude.
//        so squaring the magnitude part of 6.5 we have 2/(pi*k0*r). NB k0r >> 1 for this approximation}
//
//        {if we have a modal starterR, the starter was calculated at a distance out from the source or starterR.
//        We want our hankel func to pass through 1 at range starterR (and be == 1 before that). For the fallback starters, use a normal hankel func}
//        for ir := 0 to nrInit - 1 do
//        begin
//            if starterPointer.isOK and (rFunc(ir*rOverSamp) <= starterR) then
//                dhank2 := 1
//            else
//                dhank2 := fac / sqr(rFunc(ir*rOverSamp)); //extra cylindrical factor taken out, according to Duda / rui_cheng 3D PE (gives it as sqrt r ?)
//
//            for itheta := 0 to nSlices - 1 do
//                for iz := 0 to dMax - 1 do
//                begin
//                    dpress2 := om[ir,itheta,iz].modulusSquared * dhank2; {eq 6.3 squared}
//                    if (dpress2 > 0) then
//                        pResult^[itheta,iz,ir] := -10*log10(dpress2);
//                end;
//        end;
//
//        workerSlice := nil;
//        workerTheta := nil;
//    end;
//
//    irLoopFunc := procedure()
//        function calcDiagonalTerms(const matFormer: TMatrixFormer): TDiagTerms;
//        begin
//            result.v := rhoPrev / rhoThis; //6.187
//            result.u := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1 +
//                k0SqdeltazSqOver2*((nSq1 - 1) + result.v*(nSq2 - 1)); //6.186
//
//            result.uHat := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqdeltazSqOver2Timesw1Overw2Minus1 +
//                k0SqdeltazSqOver2*((nSq1 - 1) + result.v*(nSq2 - 1)); //6.188
//        end;
//
//    var
//        n, itheta, iz, k: integer;
//    begin
//        deltaROverk0OverRSquared := deltar/k0/max(1,rFunc(ir))/max(1,rFunc(ir));
//        for itheta := 0 to nSlices - 1 do
//        begin
//
//            for k := 0 to max(1,nPadeTerms) - 1 do
//            begin
//                Btheta[k].lower[itheta] := MakeComplex(0, deltaROverk0OverRSquared);
//                Btheta[k].upper[itheta] := MakeComplex(0, deltaROverk0OverRSquared);
//                Btheta[k].diag [itheta] := MakeComplex(4*dTheta*dTheta,-2*deltaROverk0OverRSquared);
//                Ctheta[k].lower[itheta] := MakeComplex(0,-deltaROverk0OverRSquared);
//                Ctheta[k].upper[itheta] := MakeComplex(0,-deltaROverk0OverRSquared);
//                Ctheta[k].diag [itheta] := MakeComplex(4*dTheta*dTheta, 2*deltaROverk0OverRSquared);
//
//                if itheta < nslices - 1 then
//                begin
//                    CShortTheta[k].lower[iTheta] := CTheta[k].lower[iTheta];
//                    CShortTheta[k].upper[iTheta] := CTheta[k].upper[iTheta];
//                    CShortTheta[k].diag [iTheta] := CTheta[k].diag [iTheta];
//                end;
//            end;
//
//            if (ir = 0) or (zbThis[itheta] <> zbPrev[itheta]) then
//                for iz := 0 to nz - 1 do
//                begin
//                    if z[iz] < zbThis[itheta] then
//                    begin
//                        cThis := c1[itheta,iz];         //ssp c
//                        atten := 0;// atten0*attC;   //water attenuation (todo do bulk attenuation here)
//                    end
//                    else
//                    begin
//                        aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz] , zbThis[itheta]); //could move this inside checks for seafloor
//                        if not assigned(aMaterial) then
//                            aMaterial := water;
//
//                        cThis := aMaterial.c;       //sediment c
//                        atten := aMaterial.attenuation*attCConst/cThis;  //sediment attenuation
//
//                        if z[iz] > SubDepth1 then
//                            atten := atten + TSolversGeneralFunctions.additionalAttenuationInBottomLayer(z[iz],subDepth1,subDepth2);//increase attenuation at the bottom of the sediment layer
//                    end;
//
//                    {check if close enough to the bottom to consider calculating the
//                    density difference function (function is 0 for constant density)}
//
//                    //get rho, check if we are near an interface and so need to smooth rho
//
//                    TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[itheta],
//                                                                     z[iz],  //this depth
//                                                                     k0,
//                                                                     k02,
//                                                                     rhoThis, //rho gets set
//                                                                     q1); //q1 gets set
//
//                    nSqr := c0*c0 / cThis/cThis; //nsquared, not n. 6.158, real part
//                    nSqrTilde := nSqr + q1; // eq 6.152
//
//                    nSq2 := MakeComplex(nSqrTilde,nSqrTilde*atten); //should the attenuation be multiplied by nSqr or nSqrTilde?
//                    if iz = 0 then
//                    begin
//                        rhoPrev := rhoThis;
//                        nSq1 := nSq2;
//                    end;
//
//                    //eq 6.189
//                    //diagonals C -> u, B -> uHat * w2/wstar2
//                    //lower C -> 1, B -> 1 * w2 / wstar2
//                    //upper C -> v, B -> v * w2 / wstar2
//                    for k := 0 to max(1,nPadeTerms) - 1 do
//                    begin
//                        diagTerms[k] := calcDiagonalTerms(matrixFormers[k]); //calculates u, uHat and v
//                        Cz[iTheta,k].diag[iz]   := diagTerms[k].u;
//                        Bz[iTheta,k].diag[iz]   := diagTerms[k].uHat * matrixFormers[k].w2Overwstar2;
//                        Cz[iTheta,k].lower[iz]  := 1;
//                        Bz[iTheta,k].lower[iz]  := matrixFormers[k].w2Overwstar2;
//                        Cz[iTheta,k].upper[iz]  := diagTerms[k].v;
//                        Bz[iTheta,k].upper[iz]  := diagTerms[k].v * matrixFormers[k].w2Overwstar2;
//                    end;
//
//                    rhoPrev := rhoThis;
//                    nSq1 := nSq2;
//                end; //iz, itheta
//        end;
//
//        //start solving
//        //if ir >= nr then
//        //    ir := nr - 1;
//        sliceJobs := allSliceJobs[ir];
//        thetaJobs := allThetaJobs[ir];
//        for n := 0 to nSlices - 1 do
//            sliceJobs[n] := workerSlice.CreateWorkItem(n);
//        for n := 0 to nSlices - 1 do
//            workerSlice.Schedule(sliceJobs[n]);
//    end;
//
//    workerSlice := Parallel.BackgroundWorker
//        .NumTasks(1)//Environment.System.Affinity.Count)
//        .TaskConfig(parallel.TaskConfig.SetPriority(TOTLThreadPriority.tpHighest))
//        .OnRequestDone(procedure(const Sender: IOmniBackgroundWorker; const workItem: IOmniWorkItem)
//        var
//            iz: integer;
//
//            function slicesDone: boolean;
//            var
//                wi: IOmniWorkItem;
//            begin
//                for wi in sliceJobs do
//                    if assigned(wi) and wi.Result.IsEmpty then
//                        exit(false);
//
//                result := true;
//            end;
//        begin
//            if not slicesDone then exit;
//
//            //for n := 0 to nSlices - 1 do
//            //    sliceJobs[n] := nil;
//
//            for iz := 0 to nz - 1 do
//                thetaJobs[iz] := workerTheta.CreateWorkItem(iz);
//            for iz := 0 to nz - 1 do
//                workerTheta.Schedule(thetaJobs[iz]);
//        end)
//        .Execute(procedure (const workItem: IOmniWorkItem)
//        var
//            n, k, iz: integer;
//        begin
//            n := workItem.data.AsInteger;
//
//            for k := 0 to max(1, nPadeTerms) - 1 do
//            begin
//                //form rhs the right hand side, ie B*psi(ir). Tridiagonal.
//                TSolversGeneralFunctions.TDMAMultiplyComplexInPlace(Bz[n,k],psiComplexMat[n],rhsN[n]);
//                //solve C psi(ir+1) = rhs for psi(ir+1). Can use the tridiagonal solver method
//                TSolversGeneralFunctions.TDMASolverComplexInPlace(Cz[n,k],rhsN[n],tmpCUpperN[n],tmpRhsN[n],psiComplexMat[n]);
//            end;
//
//            //get the current field in theta coords
//            for iz := 0 to nz - 1 do
//                pTrans[iz,n] := psiComplexMat[n,iz];
//
//            workItem.Result := true;
//        end);
//
//
//    workerTheta := Parallel.BackgroundWorker
//        .NumTasks(1)//Environment.System.Affinity.Count)
//        .TaskConfig(parallel.TaskConfig.SetPriority(TOTLThreadPriority.tpHighest))
//        .OnRequestDone(procedure(const Sender: IOmniBackgroundWorker; const workItem: IOmniWorkItem)
//        var
//            n, iz, itheta, izz: integer;
//
//            function thetasDone: boolean;
//            var
//                wi: IOmniWorkItem;
//            begin
//                for wi in thetaJobs do
//                    if assigned(wi) and wi.Result.IsEmpty then
//                        exit(false);
//
//                result := true;
//            end;
//        begin
//            if (not thetasDone) then exit;
//
//            for n := 0 to nSlices - 1 do
//                psiComplexMat[n,0] := NullComplex; //enforce pressure release surface
//
//            //post solve, output stage
//            if ir + 1 < nr then
//                for itheta := 0 to nSlices - 1 do
//                begin
//                    zbPrev[itheta] := zbThis[itheta];
//                    zbThis[itheta] := TSolversGeneralFunctions.interpolationFunPE(aZProfile2[itheta],rFunc(ir + 1));
//
//                    //currently at land
//                    if zbThis[iTheta] <= 0 then
//                        for izz := 0 to nz - 1 do
//                            psiComplexMat[itheta,izz] := NullComplex;
//
//                    //put the outgoing psi vector into our psiM matrix, un-reducing by sqrt(c*rho)
//                    for izz := 0 to dMax - 1 do
//                    begin
//                        iz := ifthen(izz = 0, 1, izz); //shift down by 1, as we force psiComplexVect[0] = 0
//                        TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[itheta],z[iz],k0,k02,rhoThis,q1);
//                        om[ir + 1,itheta,izz] := psiComplexMat[itheta,iz] * sqrt(localCFunc(itheta, iz, zbThis[itheta])*rhoThis); //un-reduce by sqrt(c*rho) see sec 6.8 p508
//                    end;
//                end;
//
//            //UI feedback
//            if (pcancel <> nil) and pcancel^ then exit;
//            fractionDone.d := ir / nr;
//
//            inc(ir);
//
//            //keep looping
//            if ir < nr - 1 then
//                irLoopFunc
//            else
//                outputFunc;
//        end)
//        .Execute(procedure (const workItem: IOmniWorkItem)
//        var
//            iz, k, n: integer;
//        begin
//            iz := workItem.data.AsInteger;
//
//            for k := 0 to max(1, nPadeTerms) - 1 do
//            begin
//                TSolversGeneralFunctions.TDMAMultiplyComplexInPlaceWrap(Btheta[k],pTrans[iz],rhsTheta[iz]);
//
//                TSolversGeneralFunctions.TDMASolverComplexInPlaceWrap(Ctheta[k],
//                    CShortTheta[k],
//                    rhsTheta[iz],
//                    tmpCUpperTheta[iz],
//                    tmpRhsTheta[iz],
//                    xoneTheta[iz],
//                    xtwoTheta[iz],
//                    rhsShortTheta[iz],
//                    firstAndLastOnlyRhsShortTheta[iz],
//                    tmpUpperShortTheta[iz],
//                    tmpRhsShortTheta[iz],
//                    pTrans[iz]);
//            end;
//
//            //get the field back in z coords
//            for n := 0 to nSlices - 1 do
//                psiComplexMat[n,iz] := pTrans[iz,n];
//
//            workItem.Result := true;
//        end);
//
//
//    omega := 2 * pi * f;  //angular freq
//
//    {water properties}
//    c0 := aCProfile2[0].average;
//    rho0 := water.rho_g;  //water density = 1 g/cm3
//    k0 := omega / c0;
//    k02 := k0*k0;
//    fac := ifthen(starter.isOK,starterR,2/(pi*k0));
//
//    {sub bottom sediment stuff}
//    iBottomExtension := 2;
//    if (zmax * iBottomExtension) < c0 / f then
//        iBottomExtension := c0 / f / zmax;
//
//    SubDepth1 := zmax * (iBottomExtension - 0.5);
//    SubDepth2 := zmax * iBottomExtension;
//
//    {depths}
//    deltazOutput := zmax / dmax; //used for psiM
//    nz := 1 + (round(iBottomExtension * dMax) - 1);
//    deltaz := SubDepth2 / (nz-1);
//    //deltaz2 := deltaz * deltaz;
//    SetLength(z,nz);
//    for iz := 0 to nz - 1 do
//        z[iz] := deltaz * iz;
//
//    {sound speed profile and wavenumbers}
//    setlength(c1,nSlices,nz);
//    for itheta := 0 to nSlices - 1 do
//        for iz := 0 to nz - 1 do
//            c1[itheta,iz] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2[iTheta],z[iz]);
//
//    {ranges}
//    nrInit := max(1, saferound(rmax/dr,1));
//    rOverSamp := 1;// max(1, saferound(rOversamplingFactor*f*rmax/nrInit/c0));
//    nr := max(1, 1 + (nrInit - 1)*rOverSamp);
//    //nr := max(1, 1 + (max(1, saferound(rmax/dr,1)) - 1));
//    deltar := rmax / max(1, (nr - 1));
//    if deltar = 0 then
//        deltar := 1;
//
//    dTheta := 2*pi/nSlices;
//
//    setlength(om,nr,nSlices,nz); //Duda eq 14 except we put r first for convenience
//    setlength(Cz,nSlices,max(1,nPadeTerms),nz);
//    setLength(Bz,nSlices,max(1,nPadeTerms),nz);
//    setlength(Ctheta,max(1,nPadeTerms),nSlices); //use same Ctheta Btheta at all depths
//    setlength(CShortTheta,max(1,nPadeTerms),nSlices - 1);
//    setLength(Btheta,max(1,nPadeTerms),nSlices);
//    SetLength(rhsN,nSlices,nz);
//    SetLength(tmpCUpperN,nSlices,nz);
//    SetLength(tmpRhsN,nSlices,nz);
//    setlength(psiComplexMat,nSlices,nz);
//    setlength(zbThis,nSlices);
//    setlength(zbPrev,nSlices);
//    setlength(layerDensityList,nSlices);
//    setlength(nSqz,nSlices,nz);
//    setlength(pTrans, nz, nSlices);
//    setlength(rhsTheta, nz, nSlices);
//    setlength(tmpCUpperTheta, nz, nSlices);
//    setlength(tmpRhsTheta, nz, nSlices);
//    setlength(xoneTheta, nz, nSlices - 1);
//    setlength(xtwoTheta, nz, nSlices - 1);
//    setlength(rhsShortTheta, nz, nSlices - 1);
//    setlength(firstAndLastOnlyRhsShortTheta, nz, nSlices - 1);
//    setlength(tmpUpperShortTheta, nz, nSlices - 1);
//    setlength(tmpRhsShortTheta, nz, nSlices - 1);
//    SetLength(diagTerms, max(1,nPadeTerms));
//    setlength(matrixFormers, max(1,nPadeTerms));
//    setlength(allSliceJobs, nr, nSlices);
//    setlength(allThetaJobs, nr, nz);
//
//    k0SqdeltazSqOver2 := k0*k0*deltaz*deltaz/2;
//    //k := 0;
//    if nPadeTerms = 0 then
//    begin
//        //Greene's approximation
//        matrixFormers[0].w1     := MakeComplex( TSolversGeneralFunctions.b0,  k0*deltar/2*(TSolversGeneralFunctions.a0 - TSolversGeneralFunctions.b0));  //6.180 to 6.183
//        matrixFormers[0].wstar1 := matrixFormers[0].w1.conjugate;
//        matrixFormers[0].w2     := MakeComplex( TSolversGeneralFunctions.b1,  k0*deltar/2*(TSolversGeneralFunctions.a1 - TSolversGeneralFunctions.b1));
//        matrixFormers[0].wstar2 := matrixFormers[0].w2.conjugate;
//        matrixFormers[0].w2Overwstar2 := matrixFormers[0].w2 / matrixFormers[0].wstar2;
//        matrixFormers[0].k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1 := k0SqdeltazSqOver2 * matrixFormers[0].wstar1 / matrixFormers[0].wstar2 - 1;
//        matrixFormers[0].k0SqdeltazSqOver2Timesw1Overw2Minus1 := k0SqdeltazSqOver2 * matrixFormers[0].w1 / matrixFormers[0].w2 - 1;
//    end
//    else
//    begin
//        padeTerms := TPadeCalc.padeParameters(nPadeTerms);
//        for k := 0 to nPadeTerms - 1 do
//        begin
//            //a0 = 0, b0 = 1, a1 = pade.a = ajn, a2 = pade.b = bjn
//            matrixFormers[k].w1     := MakeComplex(1,  -k0*deltar/2);  //6.180 to 6.183
//            matrixFormers[k].wstar1 := matrixFormers[k].w1.conjugate;
//            matrixFormers[k].w2     := padeTerms[k].b + MulComplexParI( k0*deltar/2*(padeTerms[k].a - padeTerms[k].b));
//            matrixFormers[k].wstar2 := matrixFormers[k].w2.conjugate;
//            matrixFormers[k].w2Overwstar2 := matrixFormers[k].w2 / matrixFormers[k].wstar2;
//            matrixFormers[k].k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1 := k0SqdeltazSqOver2 * matrixFormers[k].wstar1 / matrixFormers[k].wstar2 - 1;
//            matrixFormers[k].k0SqdeltazSqOver2Timesw1Overw2Minus1 := k0SqdeltazSqOver2 * matrixFormers[k].w1 / matrixFormers[k].w2 - 1;
//        end;
//    end;
//
//    {sediment layers}
//    //TODO support for changing seafloor scheme in range
//    for itheta := 0 to nSlices - 1 do
//    begin
//        psiComplexMat[itheta] := TSolversGeneralFunctions.PEStarterModal(deltaz,nz,starter.Z,starter.psi);
//        //for iz := 1 to dMax - 1 do
//        //    psiComplexMat[itheta,iz] := psiComplexMat[itheta,iz] * sqrt(dr);
//
//        if itheta < nslices then //todo interp
//            zbThis[itheta] := TSolversGeneralFunctions.interpolationFunPE(aZProfile2[itheta],0);
//        zbPrev[itheta] := nan;
//        seafloorScheme.getLayerDensityList(zbThis[itheta], rho0, layerDensityList[itheta]);
//
//        for iz := 1 to dMax - 1 do
//        begin
//            {copy over to the output matrix. If starterR is large, we may need to copy the starter to multiple ranges}
//            if iz = 1 then
//                om[0, itheta, 1] := psiComplexMat[itheta,1] // todo interp psiComplexVect[iz] //shift down by 1 due to pressure release surface
//            else
//                om[0, itheta, iz] := TSolversGeneralFunctions.interpolationFunPE(starter.z, starter.psi, deltazOutput * iz); //z spacing differs between the solvers, so interpolate
//
//            TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[itheta], z[iz], k0, k02, rhoThis, q1);
//            psiComplexMat[itheta,iz] := psiComplexMat[itheta,iz] / sqrt(localCFunc(itheta, iz, zbThis[itheta])*rhoThis);
//        end;
//    end;
//
//    cThis := 1500; // stop compiler warning
//    THelper.RemoveWarning(cThis);
//    attCConst := 1/(20*log10(exp(1))/f); //convert to nepers/m
//
//    //-------------START RANGE LOOPING-------------------------
//
//    if nSlices < 2 then exit;
//
//    setlength(result,nSlices,dMax,nr);
//    pResult := @result;
//    ir := 0;
//
//    Parallel.Async(procedure() begin
//        irLoopFunc();
//    end);
//
//    while (pCancel^ <> true) and (workerTheta <> nil) do
//    begin
//        sleep(200);
//        application.processMessages;
//    end;
//end;


class function TdBSeaPESS3D1Solver.mainPade(const f, srcZ, rmax, dr, zmax: double;
    const dMax, nSlices, nPadeTerms, nPadeTermsHorizontal: integer;
    const rInit: TDoubleDynArray;
    const aZProfile2: TArray<TRangeValArray>;
    const aCProfile2: TArray<TDepthValArray>;
    const zOversamplingFactor, rOversamplingFactor, phiOversamplingFactor: double;
    const seafloorScheme: TSeafloorScheme;
    const starter: TStarter;
    const pCancel: TWrappedBool;
    const fractionDone: TWrappedDouble;
    var solverErrors: TSolverErrors): TDoubleField;
const
    starterR = 1.0;
var
    c1: TMatrix;
    Cz, Bz: TArray<TArray<TTriDiagonal>>;
    BPhi,CPhi,CShortPhi: TArray<TArray<TTriDiagonal>>;
    psiComplexMat: TComplexMat; {phi,z}
    layerDensityList: TArray<TDepthValArray>;
    omega, c0,rho0,k0,k02: double;
    deltaz,deltazOutput,dPhi: double;
    atten, cThis, rho2, rho1: double;
    zbThis,zbPrev: TdoubleDynArray;
    rhoPrevZ, rhoThisZ: TDoubleDynArray;
    nz,nr,k: integer;
    attCConst: double;
    ir,irr,iz,izz,iPhi,iiPhi,nrInit,rOversamp,phiOversamp: integer;
    nSqr,nSqrTilde: double;
    nSq1,nSq2: TComplex;
    q1: double;
    SubDepth1,SubDepth2: double;
    dhank2,fac: double;
    dBottomExtension: double;
    zOverSamp, nzInit, nPhi: integer;
    dpress2: double;
    aMaterial: TMaterial;
    nSqz: TComplexMat;
    z: TDoubleDynArray;
    rhsN,tmpCUpperN,tmpRhsN: TComplexMat;
    rhsPhi, tmpCUpperPhi, tmpRhsPhi, pTrans: TComplexMat;
    xonePhi,xtwoPhi,rhsShortPhi,firstAndLastOnlyRhsShortPhi,tmpUpperShortPhi,tmpRhsShortPhi: TComplexMat;
    diagTerms: TDiagTerms;
    padeTerms, padeTermsHorizontal: TPadeTerms;
    k0SqdeltazSqOver2, k0SqrSqdeltaThetaSqOver2, deltaROverk0OverRSquared, rSq: double;
    matrixFormers: TArray<TMatrixFormer>;
    matrixFormersHorizontal: TArray<TMatrixFormerHorizontal>;
    r: TDoubleDynArray;
    om: TComplexField;

    function localC(const iPhi,iz: integer; const zbThis: double): double;
    var
        aMaterial: TMaterial;
    begin
        if z[iz] < zbThis then
            result := c1[iPhi,iz]         //ssp c
        else
        begin
            //todo seafloor referenced from sea surface?
            aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz], zbThis);
            if assigned(aMaterial) then
                result := aMaterial.c
            else
                result := water.c;
        end;
    end;

    procedure padeStep_3d_parallel(const p: TComplexMat; const r: double);
    const
        SPLIT = 10; //split the workload a little, to reduce the number of threads as otl doesn't cope well with it
    var
        nn: integer;
        {$IFDEF DEBUG}
        iz_split: integer;
        k, n, iz, izs: integer;
        {$ENDIF}
    begin
        //in place version for less memory assigns
        {we are solving C psi(ir+1) = B psi(ir), where C and B are tridiagonal, for each pade term}

        //for n := 0 to nPhi - 1 do
        {$IFDEF DEBUG}
        //parallel.for is VERY slow in debug mode, but system.threading crashes
        //tparallel.for(0, nPhi div SPLIT, procedure (n_split: integer)
        for nn := 0 to nPhi - 1 do (procedure (n_split: integer)
        {$ELSE}
        Parallel.For(0, nPhi div SPLIT).Execute(procedure (n_split: integer)
        //for nn := 0 to nPhi - 1 do (procedure (n: integer)
        {$ENDIF}
        var
            k, iz, n, ns: integer;
        begin
            for ns := 0 to SPLIT - 1 do
            begin
                n := (n_split * SPLIT) + ns;
                if n >= nPhi then continue;

                for k := 0 to max(1, nPadeTerms) - 1 do
                begin
                    //form rhs the right hand side, ie B*psi(ir). Tridiagonal.
                    TSolversGeneralFunctions.TDMAMultiplyComplexInPlace(
                        Bz[n,k],
                        p[n],
                        rhsN[n]);
                    //solve C psi(ir+1) = rhs for psi(ir+1). Can use the tridiagonal solver method
                    TSolversGeneralFunctions.TDMASolverComplexInPlace(
                        Cz[n,k],
                        rhsN[n],
                        tmpCUpperN[n],
                        tmpRhsN[n],
                        p[n]);
                end;

                //get the current field in phi coords
                for iz := 0 to nz - 1 do
                    pTrans[iz,n] := p[n,iz];
            end;
        {$IFDEF DEBUG}
        end)(nn);
        {$ELSE}
        end);
        {$ENDIF}

        //application.processMessages; //not in main thread! But this is here as otherwise Parallel.for causes memory leaks

        //todo oversample in phi
        {$IFDEF DEBUG}
        //TParallel.for(0, nz div SPLIT, procedure (iz_split: integer)
        for iz_split := 0 to nz div SPLIT - 1 do// (procedure (iz_split: integer)
        begin
        {$ELSE}
        Parallel.For(0, nz div SPLIT).Execute(procedure (iz_split: integer)
        //for iizz := 0 to nz div SPLIT - 1 do (procedure (iz_split: integer)

        var
            k, n, iz, izs: integer;
        begin
        {$ENDIF}
            for izs := 0 to SPLIT - 1 do
            begin
                iz := (iz_split * SPLIT) + izs;
                if iz >= nz then continue;

                for k := 0 to max(1, nPadeTermsHorizontal) - 1 do
                begin
                    //form rhs the right hand side, ie B*pTrans
                    TSolversGeneralFunctions.TDMAMultiplyComplexInPlaceWrap(
                        BPhi[iz,k],
                        pTrans[iz],
                        rhsPhi[iz]);
                    //solve C ptrans = rhs
                    TSolversGeneralFunctions.TDMASolverComplexInPlaceWrap(
                        CPhi[iz,k],
                        CShortPhi[iz,k],
                        rhsPhi[iz],
                        tmpCUpperPhi[iz],
                        tmpRhsPhi[iz],
                        xonePhi[iz],
                        xtwoPhi[iz],
                        rhsShortPhi[iz],
                        firstAndLastOnlyRhsShortPhi[iz],
                        tmpUpperShortPhi[iz],
                        tmpRhsShortPhi[iz],
                        pTrans[iz]);
                end;

                //get the field back in z coords
                for n := 0 to nPhi - 1 do
                    p[n,iz] := pTrans[iz,n];
            end;
        {$IFDEF DEBUG}
        end;//)(iizz);
        {$ELSE}
        end);
        {$ENDIF}

        application.processMessages; //not in main thread! But this is here as otherwise Parallel.for causes memory leaks

        for nn := 0 to nPhi - 1 do
            p[nn,0] := NullComplex; //enforce pressure release surface
    end;

    function calcDiagonalTerms(const matFormer: TMatrixFormer;
        const rhoPrev, rhoThis, k0SqdeltazSqOver2: double;
        const nSqPrev, nSqThis: TComplex): TDiagTerms;
    begin
        result.v := rhoPrev / rhoThis; //6.187
        result.u := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1 +
            k0SqdeltazSqOver2*((nSqPrev - 1) + result.v*(nSqThis - 1)); //6.186

        result.uHat := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqdeltazSqOver2Timesw1Overw2Minus1 +
            k0SqdeltazSqOver2*((nSqPrev - 1) + result.v*(nSqThis - 1)); //6.188
    end;

    function calcDiagonalTermsHorizontal(const matFormer: TMatrixFormerHorizontal;
        const rhoPrev, rhoThis: double): TDiagTerms;
    begin
        result.v := rhoPrev / rhoThis; //6.187
        result.u := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqrSqdeltaThetaSqOver2Timeswstar1Overwstar2Minus1; //6.186 modified for theta
        result.uHat := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqrSqdeltaThetaSqOver2Timesw1Overw2Minus1; //6.188 modified for theta
    end;

    procedure myCleanup(var q: TComplexMat);
    var
        i: integer;
    begin
        for i := 0 to length(q) - 1 do
            setlength(q[i],0);
        setlength(q,0);
    end;

    function deltar(const ir: integer): double;
    begin
        //allows for unequal r spacing
        if ir >= length(r) - 1 then
            result := r[length(r) - 1] - r[length(r) - 2]
        else
            result := r[ir+1] - r[ir];
    end;
begin
    omega := 2 * pi * f;  //angular freq

    {water properties}
    c0 := aCProfile2[0].average;
    rho0 := water.rho_g;  //water density = 1 g/cm3
    k0 := omega / c0;
    k02 := sqr(k0);
    fac := ifthen(starter.isOK,starterR,2/(pi*k0));

    {sub bottom sediment stuff}
    dBottomExtension := 2;
    if (zmax * dBottomExtension) < c0 / f then
        dBottomExtension := c0 / f / zmax;

    SubDepth1 := zmax * (dBottomExtension - 0.5);
    SubDepth2 := zmax * dBottomExtension;

    {depths}
    zOverSamp := TSolversGeneralFunctions.PEZOversamplingFactor(zOversamplingFactor,f,zMax,c0,dmax); //amount of oversampling - reduce for faster, less accurate
    nzInit := round(dBottomExtension * dMax);
    nz := 1 + (nzInit - 1) * zOverSamp;
    deltaz := SubDepth2 / (nz-1);
    deltazOutput := zmax / dmax; //used for psiM
    //nz := 1 + (round(dBottomExtension * dMax) - 1);
    //deltaz := SubDepth2 / (nz-1);
    //deltaz2 := deltaz * deltaz;
    SetLength(z,nz);
    for iz := 0 to nz - 1 do
        z[iz] := deltaz * iz;

    {azimuth angles}
    phiOversamp := round(phiOversamplingFactor);
    nPhi := nSlices * phiOversamp;
    dPhi := 2*pi/nPhi;

    {sound speed profile and wavenumbers}
    setlength(c1,nPhi,nz);
    for iPhi := 0 to nSlices - 1 do
        for iz := 0 to nz - 1 do
        begin
            //todo multiple location properties
            for iiPhi := 0 to phiOversamp - 1 do
                c1[iPhi * phiOversamp + iiPhi,iz] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2[iPhi],z[iz]);
        end;

    {ranges}
    r := rInit.truncateAtVal(rMax);
    nrInit := max(1, saferound(length(r),1));
    rOverSamp := max(1, saferound(rOversamplingFactor*f*r.max/nrInit/c0));  //reduce for faster, less accurate
    r := TSolversGeneralFunctions.resample(r,nrInit*rOverSamp);
    nr := max(1, length(r));
//    nr := max(1, 1 + (nrInit - 1)*rOverSamp);
//    //nr := max(1, 1 + (max(1, saferound(rmax/dr,1)) - 1));
//    deltar := rmax / max(1, (nr - 1));
//    if deltar = 0 then
//        deltar := 1;

    setlength(Cz,nPhi,max(1,nPadeTerms),nz);
    setLength(Bz,nPhi,max(1,nPadeTerms),nz);
    setlength(CPhi,nz,max(1,nPadeTermsHorizontal),nPhi); //use same Cphi Bphi at all depths
    setlength(CShortPhi,nz,max(1,nPadeTermsHorizontal),nPhi - 1);
    setLength(BPhi,nz,max(1,nPadeTermsHorizontal),nPhi);
    SetLength(rhsN,nPhi,nz);
    SetLength(tmpCUpperN,nPhi,nz);
    SetLength(tmpRhsN,nPhi,nz);
    setlength(psiComplexMat,nPhi);
    setlength(zbThis,nPhi);
    setlength(zbPrev,nPhi);
    setlength(layerDensityList,nPhi);
    setlength(nSqz,nPhi,nz);
    setlength(pTrans, nz, nPhi);
    setlength(rhsPhi, nz, nPhi);
    setlength(tmpCUpperPhi, nz, nPhi);
    setlength(tmpRhsPhi, nz, nPhi);
    setlength(xonePhi, nz, nPhi - 1);
    setlength(xtwoPhi, nz, nPhi - 1);
    setlength(rhsShortPhi, nz, nPhi - 1);
    setlength(firstAndLastOnlyRhsShortPhi, nz, nPhi - 1);
    setlength(tmpUpperShortPhi, nz, nPhi - 1);
    setlength(tmpRhsShortPhi, nz, nPhi - 1);
    setlength(rhoPrevZ, nz);
    setlength(rhoThisZ, nz);

    //not oversampled
    setlength(om,nrInit,nSlices,nzInit); //Duda eq 14 except we put r first for convenience
    setlength(result,nSlices,dMax,nr);

    if (srcZ < 0) or (srcZ > nz*deltaz) then
    begin
        include(solverErrors,kSourceBelowSubDepth);
        exit;
    end;

    k0SqdeltazSqOver2 := k02*sqr(deltaz)/2;
    padeTerms := TPadeCalc.padeParameters(nPadeTerms);
    padeTermsHorizontal := TPadeCalc.padeParameters(nPadeTermsHorizontal);
    TSolversGeneralFunctions.calcMatrixFormers(nPadeTerms,padeTerms,k0,k0SqdeltazSqOver2,deltar(0),matrixFormers);

    {sediment layers}
    //TODO support for changing seafloor scheme in range
    for iPhi := 0 to nPhi - 1 do
    begin
        psiComplexMat[iPhi] := TSolversGeneralFunctions.PEStarterInterpolation(deltaz,nz,starter.Z,starter.psi);
        zbThis[iPhi] := TSolversGeneralFunctions.interpolationFunPE(aZProfile2[iPhi div phiOversamp],0);
        zbPrev[iPhi] := nan;
        seafloorScheme.getLayerDensityList(zbThis[iPhi], rho0, layerDensityList[iPhi]);

        for iz := 1 to dMax - 1 do
        begin
            {copy over to the output matrix. If starterR is large, we may need to copy the starter to multiple ranges}
            if iPhi mod phiOversamp = 0 then
            begin
                if iz = 1 then
                    om[0, iPhi div phiOversamp, 1] := psiComplexMat[iPhi,1] // todo interp psiComplexVect[iz] //shift down by 1 due to pressure release surface
                else
                    om[0, iPhi div phiOversamp, iz] := TSolversGeneralFunctions.interpolationFunPE(starter.z, starter.psi, deltazOutput * iz); //z spacing differs between the solvers, so interpolate
            end;

            TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[iPhi], z[iz], k0, k02, rho2, q1);
            psiComplexMat[iPhi,iz] := psiComplexMat[iPhi,iz] / sqrt(localC(iPhi, iz, zbThis[iPhi])*rho2);
        end;
    end;

    rho1 := nan;
    THelper.RemoveWarning(rho1);
    cThis := 1500; // stop compiler warning
    THelper.RemoveWarning(cThis);
    attCConst := 1/(20*log10(exp(1))/f); //convert to nepers/m
    //-------------START RANGE LOOPING-------------------------
    for ir := 0 to nr - 2 do
    begin
        rSq := sqr(max(1,r[ir]));
        deltaROverk0OverRSquared := deltar(ir)/k0/rSq; //used for non-pade azimuth step (Collins and Chin-Bing), so not used now
        THelper.RemoveWarning(deltaROverk0OverRSquared);
        k0SqrSqdeltaThetaSqOver2 := k02 * rSq * sqr(dPhi) / 2;

        //due to dependence on r^2, we have to recalc the matrix formers at each range step
        TSolversGeneralFunctions.calcMatrixFormersHorizontal(nPadeTermsHorizontal,
            padeTermsHorizontal,
            k0,
            k0SqrSqdeltaThetaSqOver2,
            deltar(ir),
            matrixFormersHorizontal);

        for iPhi := 0 to nPhi - 1 do
        begin

            for iz := 0 to nz - 1 do
            begin

                if iPhi = 0 then
                begin
                    //to loop around theta
                    TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[nPhi - 1],
                                                     z[iz],  //this depth
                                                     k0,
                                                     k02,
                                                     rhoPrevZ[iz], //rho gets set
                                                     q1); //q1 gets set
                end;

                TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[iPhi],
                                                 z[iz],  //this depth
                                                 k0,
                                                 k02,
                                                 rhoThisZ[iz], //rho gets set
                                                 q1); //q1 gets set


                for k := 0 to max(1,nPadeTermsHorizontal) - 1 do
                begin
                    //Collins and Chin-Bing A three-dimensional parabolic equation model that includes the effects of rough boundaries, JASA 1989
                    //crank nicolson on eq 8, d2P/dtheta2 term, ie eq 10
                    // dP/dr = z terms + i/2/k0/r^2*(d2P/dTheta2)
                    //
                    // P[m+1][l] - P[m][l]/deltar = 1/2(i*deltar/2/k0/r^2)((P[m+1][l-1] - 2P[m+1][l] + P[m+1][l+1]) + (P[m][l-1] - 2P[m][l] + P[m][l+1]))/ dTheta^2
                    // P[m+1][l](1 + 2ideltar/4/k0/r^2/dTheta^2) - (P[m+1][l-1]+P[m+1][l+1])(ideltar/4/k0/r^2/dTheta^2)
                    //    = P[m][l](1 - 2ideltar/4/k0/r^2/dTheta^2) + (P[m][l-1] + P[m][l+1])(ideltar/4/k0/r^2/dTheta^2)
                    // P[m+1][l](4 dTheta^2 + 2ideltar/k0/r^2) - (P[m+1][l-1]+P[m+1][l+1])(ideltar/k0/r^2)
                    //    = P[m][l](4 dTheta^2 - 2ideltar/k0/r^2) + (P[m+1][l-1]+P[m+1][l+1])(ideltar/k0/r^2)
                    //
                    // theta -> phi
                    // pade shouldn't be used with this formulation, so just 1 term

                    //BPhi[k].lower[iPhi] := MakeComplex(0, deltaROverk0OverRSquared);
                    //BPhi[k].upper[iPhi] := MakeComplex(0, deltaROverk0OverRSquared);
                    //BPhi[k].diag [iPhi] := MakeComplex(4*sqr(dPhi),-2*deltaROverk0OverRSquared);
                    //CPhi[k].lower[iPhi] := MakeComplex(0,-deltaROverk0OverRSquared);
                    //CPhi[k].upper[iPhi] := MakeComplex(0,-deltaROverk0OverRSquared);
                    //CPhi[k].diag [iPhi] := MakeComplex(4*sqr(dPhi), 2*deltaROverk0OverRSquared);

                    //rewrite of 2d matrix forming, to horizontal
                    diagTerms := calcDiagonalTermsHorizontal(matrixFormersHorizontal[k], rhoPrevZ[iz], rhoThisZ[iz]); //calculates u, uHat and v
                    CPhi[iz,k].diag[iPhi]   := diagTerms.u;
                    BPhi[iz,k].diag[iPhi]   := diagTerms.uHat * matrixFormersHorizontal[k].w2Overwstar2;
                    CPhi[iz,k].lower[iPhi]  := 1;
                    BPhi[iz,k].lower[iPhi]  := matrixFormersHorizontal[k].w2Overwstar2;
                    CPhi[iz,k].upper[iPhi]  := diagTerms.v;
                    BPhi[iz,k].upper[iPhi]  := diagTerms.v * matrixFormersHorizontal[k].w2Overwstar2;

                    if iPhi < nPhi - 1 then
                    begin
                        CShortPhi[iz,k].lower[iPhi] := CPhi[iz,k].lower[iPhi];
                        CShortPhi[iz,k].upper[iPhi] := CPhi[iz,k].upper[iPhi];
                        CShortPhi[iz,k].diag [iPhi] := CPhi[iz,k].diag [iPhi];
                    end;
                end;

                rhoPrevZ[iz] := rhoThisZ[iz];
            end;

            if (ir = 0) or (zbThis[iPhi] <> zbPrev[iPhi]) then
                for iz := 0 to nz - 1 do
                begin
                    if z[iz] < zbThis[iPhi] then
                    begin
                        cThis := c1[iPhi,iz];         //ssp c
                        atten := 0;// atten0*attC;   //water attenuation (todo do bulk attenuation here)
                    end
                    else
                    begin
                        aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz] , zbThis[iPhi]); //could move this inside checks for seafloor
                        if not assigned(aMaterial) then
                            aMaterial := water;

                        cThis := aMaterial.c;       //sediment c
                        atten := aMaterial.attenuation*attCConst/cThis;  //sediment attenuation

                        if z[iz] > SubDepth1 then
                            atten := atten + TSolversGeneralFunctions.additionalAttenuationInBottomLayer(z[iz],subDepth1,subDepth2);//increase attenuation at the bottom of the sediment layer
                    end;

                    {check if close enough to the bottom to consider calculating the
                    density difference function (function is 0 for constant density)}

                    //get rho, check if we are near an interface and so need to smooth rho

                    TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[iPhi],
                                                                     z[iz],  //this depth
                                                                     k0,
                                                                     k02,
                                                                     rho2, //rho gets set
                                                                     q1); //q1 gets set

                    nSqr := sqr(c0/cThis); //nsquared, not n. 6.158, real part
                    nSqrTilde := nSqr + q1; // eq 6.152

                    nSq2 := MakeComplex(nSqrTilde,nSqrTilde*atten); //should the attenuation be multiplied by nSqr or nSqrTilde?
                    if iz = 0 then
                    begin
                        rho1 := rho2;
                        nSq1 := nSq2;
                    end;

                    //eq 6.189
                    //diagonals C -> u, B -> uHat * w2/wstar2
                    //lower C -> 1, B -> 1 * w2 / wstar2
                    //upper C -> v, B -> v * w2 / wstar2
                    for k := 0 to max(1,nPadeTerms) - 1 do
                    begin
                        diagTerms := calcDiagonalTerms(matrixFormers[k], rho1, rho2, k0SqdeltazSqOver2, nSq1, nSq2); //calculates u, uHat and v
                        Cz[iPhi,k].diag[iz]   := diagTerms.u;
                        Bz[iPhi,k].diag[iz]   := diagTerms.uHat * matrixFormers[k].w2Overwstar2;
                        Cz[iPhi,k].lower[iz]  := 1;
                        Bz[iPhi,k].lower[iz]  := matrixFormers[k].w2Overwstar2;
                        Cz[iPhi,k].upper[iz]  := diagTerms.v;
                        Bz[iPhi,k].upper[iz]  := diagTerms.v * matrixFormers[k].w2Overwstar2;
                    end;

                    rho1 := rho2;
                    nSq1 := nSq2;
                end; //iz, iphi
        end;

        //--------------------------------March solution in range------------------------------
        padeStep_3d_parallel(psiComplexMat,r[ir]);
        //psiComplexMat is now for ir+1

        for iPhi := 0 to nPhi - 1 do
        begin
            zbPrev[iPhi] := zbThis[iPhi];
            zbThis[iPhi] := TSolversGeneralFunctions.interpolationFunPE(aZProfile2[iPhi div phiOversamp],r[ir + 1]);

            //currently at land
            if zbThis[iPhi] <= 0 then
                for izz := 0 to nz - 1 do
                    psiComplexMat[iPhi,izz] := NullComplex;

            //get the seabed layers at this location
            if not isnan(zbThis[iPhi]) and (zbThis[iPhi] <> zbPrev[iPhi]) then
                seafloorScheme.getLayerDensityList(zbThis[iPhi], water.rho_g, layerDensityList[iPhi]);

            //put the outgoing psi vector into our output matrix, un-reducing by sqrt(c*rho)
            if (iPhi mod phiOversamp = 0) and (((ir + 1) mod rOversamp) = 0) then
            begin
                irr := (ir + 1) div rOversamp;
                for izz := 0 to dMax - 1 do
                begin
                    iz := ifthen(izz = 0, 1, izz); //shift down by 1, as we force psiComplexVect[0] = 0
                    if iz >= nz then break;
                    TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList[iPhi],z[iz * zOverSamp],k0,k02,rho2,q1);
                    om[irr,iPhi div phiOversamp,izz] := psiComplexMat[iPhi,iz * zOverSamp] * sqrt(localC(iPhi, iz * zOverSamp, zbThis[iPhi])*rho2); //un-reduce by sqrt(c*rho) see sec 6.8 p508
                end;
            end;
        end;

        //UI feedback
        if (pcancel <> nil) and pcancel.b then exit;
        fractionDone.d := ir / nr;
    end; //ir

    {put the abs(hankel function) back into the envelope function to get the pressure.
    6.5 gives Ho(1)(k0r) = sqrt(2/(pi*k0*r)) exp(i *(k0r - pi/4))
    we can ignore the complex exponential as we just want magnitude.
    so squaring the magnitude part of 6.5 we have 2/(pi*k0*r). NB k0r >> 1 for this approximation}

    {if we have a modal starterR, the starter was calculated at a distance out from the source or starterR.
    We want our hankel func to pass through 1 at range starterR (and be == 1 before that). For the fallback starters, use a normal hankel func}
    for ir := 0 to nrInit - 1 do
    begin
        if ir*rOverSamp >= length(r) then break;

        if starter.isOK and (r[ir*rOverSamp] <= starterR) then
            dhank2 := 1
        else
            dhank2 := fac / sqr(r[ir*rOverSamp]); //extra cylindrical factor taken out, according to Duda / rui_cheng 3D PE (gives it as sqrt r ?)

        for iPhi := 0 to nSlices - 1 do
            for iz := 0 to dMax - 1 do
            begin
                dpress2 := om[ir,iPhi,iz].modulusSquared * dhank2; {eq 6.3 squared}
                if (dpress2 > 0) then
                    result[iPhi,iz,ir] := -10*log10(dpress2);
            end;
    end;
end;


end.

