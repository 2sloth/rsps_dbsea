unit dBSeaPEElastic1;

interface

uses
    system.classes, system.math, system.sysutils, system.types,
    helperFunctions,
    fourier, matrices, MComplex,
    dBSeaconstants, MMatrix, baseTypes,
    seafloorScheme, material,
    directivity, solversGeneralFunctions, mcklib, globalsUnit,
    PadeApproximants, mathlib, TriDiagonal, BandSolver,
    PEStarterPadeDouble, complex80;

type

    TLame = record
        la,mu: TComplex;

        class function  fromParams(const rho, cp, cp2, cs, alphap, alphas: double): TLame; static;
    end;
    TLames = array of TLame;
    TLamesHelper = record helper for TLames
    private
        //function getJ(const i,j,n: integer): integer;
        //function getDivisor(const i,n: integer): integer;
    public
        function laMu(const i: integer): TComplex;
        function la2Mu(const i: integer): TComplex;
        function getLa2Mu(const i,n: integer): TComplex;
        function getLa(const i,n: integer): TComplex;
        function getMu(const i,n: integer): TComplex;
        function getLaMu(const i,n: integer): TComplex;
    end;


  TdBSeaPEElastic1Solver = class(TObject)
  private
  const
  //  taylorSeriesConstants: array of double = [1,-12,90,-560,3150,-16632,84084,-411840,1969110,-9237800,42678636,-194699232,878850700,-3931426800,17450721000,-76938289920];

  //pade terms for (1+x)^(-3/4) from Collins A self-starter for the parabolic equation method, JASA 92
    powerThreeQuarterPadeTermsA: array[0..6] of array[0..6] of TComplex = (
        ((r:0.9617117;i:-0.02027027),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.3115076;i:-0.07760893),		(r:0.9384962;i:-0.0087452318),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.2586824;i:-0.0061094389),		(r:0.7074158;i:-0.010215726),		(r:0.9816584;i:-0.0013283753),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.1492602;i:-0.050289673),		(r:0.4250684;i:-0.097180339),		(r:0.7563306;i:-0.045166144),		(r:0.9835059;i:-0.0029834402),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.090767245;i:-0.007934321),		(r:0.3068318;i:-0.016935283),		(r:0.5880934;i:-0.015393326),		(r:0.8459556;i:-0.0069070212),		(r:0.9900555;i:-0.00047626251),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.078587773;i:-0.00030979066),		(r:0.2648318;i:-0.0010142631),		(r:0.5083819;i:-0.0018200377),		(r:0.7454339;i:-0.0022547549),		(r:0.9175307;i:-0.0016590421),		(r:0.9952318;i:-0.00016800734),		(r:0;i:0)),
        ((r:0.046336161;i:0.014075255),		(r:0.1566772;i:-0.030835107),		(r:0.3287549;i:-0.029859713),		(r:0.540791;i:-0.021123314),		(r:0.7491779;i:-0.011620495),		(r:0.9113141;i:-0.0041167107),		(r:0.9944228;i:-0.0002590247)));

    powerThreeQuarterPadeTermsB: array[0..6] of array[0..6] of TComplex = (
        ((r:0.2117117;i:-0.02027027),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.024628758;i:-0.019229166),		(r:0.475375;i:-0.067124996),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.026217116;i:-0.00068646898),		(r:0.368896;i:-0.0081275983),		(r:0.8026434;i:-0.0088394731),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.015154991;i:-0.005023163),		(r:0.2123657;i:-0.070038913),		(r:0.5046994;i:-0.089651044),		(r:0.831945;i:-0.030906476),		(r:0;i:0),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.008552886;i:-0.00087635465),		(r:0.1348384;i:-0.010787142),		(r:0.3741093;i:-0.017620952),		(r:0.6585616;i:-0.013565065),		(r:0.8956414;i:-0.0047967008),		(r:0;i:0),		(r:0;i:0)),
        ((r:0.0074121593;i:-0.000029462602),		(r:0.1166687;i:-0.0004576248),		(r:0.3227288;i:-0.0012213019),		(r:0.5708001;i:-0.0019865855),		(r:0.7963831;i:-0.002229233),		(r:0.946005;i:-0.0013016879),		(r:0;i:0)),
        ((r:0.0044700366;i:-0.0014674149),		(r:0.068315215;i:-0.019444767),		(r:0.194054;i:-0.032061996),		(r:0.3796118;i:-0.02799061),		(r:0.5949968;i:-0.018682277),		(r:0.7955468;i:-0.0094790954),		(r:0.9404795;i:-0.00276346)));

    class function  localC(const iz: integer; const zbThis: double; const z,c1: TDoubleDynArray; const seafloorScheme: TSeafloorScheme): double;
    class procedure formMminusk02L_v3(const Lk02,M: TComplexMat; var MMinusk02L: TComplexMat);
    class procedure setupLames(const iz: integer;
        const zbThis, SubDepth1, SubDepth2, c0, k0, k02: double;
        const z, c1, cpz, cpz2, alphapz, csz, alphasz: TDoubleDynArray;
        const seafloorScheme: TSeafloorScheme;
        const layerDensityList: TDepthValArray;
        const rhoz: TDoubleDynArray;
        const lames: TLames);
    class procedure formLMat_CederbergCollins_kTerm(const n: integer; const dz, omega, k02: double; const rhoz, cpz: TDoubleDynArray; var L: TComplexMat);
    class procedure formLMat_CederbergCollins_rhoTerm(const n: integer; const dz, omega, k02: double; const rhoz, cpz: TDoubleDynArray; var L: TComplexMat);

  public
//    class function  selfStarter(const f, srcZ, zmax: double;
//                        const nz,nPadeTerms: integer;
//                        const aZProfile2: TRangeValArray;
//                        const aCProfile2: TDepthValArray;
//                        const seafloorScheme: TSeafloorScheme;
//                        var solverErrors: TSolverErrors): TStarter;

    class function  selfStarterCederbergCollins(const f, srcZ, zmax: double;
                        const nz, nPadeTermsIgnored: integer;
                        const aZProfile2: TRangeValArray;
                        const aCProfile2: TDepthValArray;
                        const seafloorScheme: TSeafloorScheme;
                        var solverErrors: TSolverErrors): TStarter;

    class procedure formLk02Mat_v3(const n: integer; const dz,k02: double; const lames: TLames; var L: TComplexMat);
    class procedure formMMat_v3(const n: integer; const dz,omega,omega2: double; const rhoz: TDoubleDynArray; const lames: TLames; var M: TComplexMat);
    class function  solverStep_v3(const q: TComplexVect; const lames: TLames; const rhoz: TDoubleDynArray; const k0,k02,dz,dr,omega: double; const nPadeTerms: integer): TComplexVect;
    class function  elastic_v3(const f, srcZ, rmax, dr, zmax: double;
                        const dMax,nPadeTerms: integer;
                        const aZProfile2: TRangeValArray;
                        const aCProfile2: TDepthValArray;
                        const starter: TStarter;
                        const seafloorScheme: TSeafloorScheme;
                        const pCancel: TWrappedBool;
                        const fractionDone: TWrappedDouble;
                        var solverErrors: TSolverErrors): TMatrix;
  end;


implementation

class function TLame.fromParams(const rho, cp, cp2, cs, alphap, alphas: double): TLame;
const
    k = 27.29;
var
    alphasp: double;

    eta: double;
    CCp: TComplex;
    CCs: TComplex;
begin
    //4.30, 4.31, 4.49 - 4.54

    //constraint: as/ap < 3/4(cp/cs)^2
    if (alphas > 0) and (cs > 0) and (alphas > alphap*3/4*(cp2/cs/cs)) then
        alphasp := alphap*3/4*(cp2/cs/cs)
    else
        alphasp := alphas;

    //Collins self starter between eq 24 and 25, and PE for poroelastic media section I
    eta := 1/(40*pi*log10(exp(1)));
    CCp := cp / MakeComplex(1,eta*alphap);
    CCs := cs / MakeComplex(1,eta*alphasp);

    result.la := rho*(CCp*CCp - 2*CCs*CCs);
    result.mu := rho*CCs*CCS;
end;

function TLamesHelper.getLa2Mu(const i, n: integer): TComplex;
begin
    if (i < 0) or (i >= n) then
        result := nullComplex
    else
        result := self.la2Mu(i);
end;

function TLamesHelper.getLaMu(const i, n: integer): TComplex;
begin
    if (i < 0) or (i >= n) then
        result := nullComplex
    else
        result := self.laMu(i);
end;

function TLamesHelper.getLa(const i, n: integer): TComplex;
begin
    if (i < 0) or (i >= n) then
        result := nullComplex
    else
        result := TLame(self[i]).la;
    //result := (TLame(self[i + getJ(i,1,n)]).la - TLame(self[i + getJ(i,-1,n)]).la) / self.getDivisor(i,n) / dz;
end;

function TLamesHelper.getMu(const i, n: integer): TComplex;
begin
    if (i < 0) or (i >= n) then
        result := nullComplex
    else
        result := TLame(self[i]).mu;
    //result := (TLame(self[i + getJ(i,1,n)]).mu - TLame(self[i + getJ(i,-1,n)]).mu) / self.getDivisor(i,n) / dz;
end;

function TLamesHelper.la2mu(const i: integer): TComplex;
begin
    result := TLame(self[i]).la + 2*TLame(self[i]).mu;
end;

function TLamesHelper.laMu(const i: integer): TComplex;
begin
    result := TLame(self[i]).la + TLame(self[i]).mu;
end;



class function TdBSeaPEElastic1Solver.elastic_v3(const f, srcZ, rmax, dr, zmax: double;
    const dMax, nPadeTerms: integer;
    const aZProfile2: TRangeValArray;
    const aCProfile2: TDepthValArray;
    const starter: TStarter;
    const seafloorScheme: TSeafloorScheme;
    const pCancel: TWrappedBool;
    const fractionDone: TWrappedDouble;
    var solverErrors: TSolverErrors): TMatrix;
var
    q: TComplexVect; // q: delta and w in a column vect eq 6.61, but reduced by srt(r) ?
    initialField: TStarter;
    lames: TLames;
    c0,k0,omega,rho0,k02: double;
    rhoz,cpz,cpz2,csz,alphapz,alphasz,z,c1: TDoubleDynArray;
    ir,nr,iz,nz: integer;
    dpress2: double;
    r,iBottomExtension,SubDepth1,SubDepth2: double;
    deltar,deltaz: double;
    zbThis,zbPrev,rho_ir: double;
    layerDensityList: TDepthValArray;
    //aMaterial: TMaterial;
    izs: integer; //src depth index
    laZSrc: TComplex;
    //nilSedimentError: boolean;

    //{$DEFINE EPEOUT}
    {$IF defined (DEBUG) and defined (EPEOUT)}
    s: string;
    {$ENDIF}


    procedure output(iir: integer);
    var
        iz: integer;
    begin
        //Collins higher order... after eq 28 gives TL = -20log10 abs(lambda(zs)*del) + 10log10(r)

        //dhank2 := 2/(pi*k0) / power(r,1.5); //maybe check < 1? // u = sqrt(r)U, v = sqrt(r)V where we are solving for u and v, and U and V are the actual displacements
        for iz := 0 to nz - 1 do
        begin
            zbPrev := zbThis;
            zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,r);
            if (zbThis <> zbPrev) then
                seafloorScheme.getLayerDensityList(zbThis, rho0, layerDensityList);

            rho_ir := TSolversGeneralFunctions.depthIsCloseToInterfaceNoSmoothing(layerDensityList, z[iz], k0, k02);
            //rho_ir := rho_ir*1000;
            dpress2 := (laZSrc*q[2*iz]).modulusSquared * localC(iz, zbThis, z, c1, seafloorScheme)*rho_ir; //unreduce by sqrt(rho c)
            //if dpress2 > 0 then
            //    result[iz,iir] := -10*log10(dpress2 * sqrt(localC(iz, zbThis, z, c1, seafloorScheme)*rho_ir) * dhank2);

            if dpress2 > 0 then
                result[iz,iir] := -10*log10(dpress2 / r);
        end;
    end;
begin
    {$IF defined (DEBUG) and defined (EPEOUT)}
    s := '';
    {$ENDIF}

    omega := 2 * pi * f;
    c0 := aCProfile2.average;
    rho0 := water.rho_g;  //water density = 1 g/cm3
    k0 := omega / c0;
    k02 := sqr(k0);

    {sub bottom sediment stuff}
    iBottomExtension := 2;
    if (zmax * iBottomExtension) < c0 / f then
        iBottomExtension := c0 / f / zmax;

    SubDepth1 := zmax * (iBottomExtension - 0.5);
    SubDepth2 := zmax * iBottomExtension;

    {depths}
    nz := 1 + (round(iBottomExtension * dMax) - 1);
    deltaz := SubDepth2 / (nz-1);
    izs := round(srcZ / deltaz);
    SetLength(z,nz);
    for iz := 0 to nz - 1 do
        z[iz] := deltaz * iz;

    {sound speed profile and wavenumbers}
    setlength(c1,nz);
    for iz := 0 to nz - 1 do
        c1[iz] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2,z[iz]);

    {ranges}
    nr := max(1, 1 + (max(1, saferound(rmax/dr,1)) - 1));
    deltar := rmax / max(1, (nr - 1));
    if deltar = 0 then
        deltar := 1;

    setlength(lames, nz);
    setlength(rhoz, nz);
    setlength(cpz, nz);
    setlength(cpz2, nz);
    setlength(csz, nz);
    setlength(alphapz, nz);
    setlength(alphasz, nz);

    setlength(result, nz, nr);

    zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,0);
    zbPrev := nan;
    seafloorScheme.getLayerDensityList(zbThis, rho0, layerDensityList); {includes a setlength call} //returned values are in g/cm3

    setlength(q,2*nz);

    initialField := selfStarterCederbergCollins(f, srcZ, zmax,
        nz,3,
        aZProfile2,
        aCProfile2,
        seafloorScheme,
        solverErrors);

    //temp: get starter to the correct z spacing for this. Not needed if not using external starter
    initialField.psi := TSolversGeneralFunctions.PEStarterInterpolation(deltaz,nz,initialField.z,initialField.psi);
    //initialField.psi := TSolversGeneralFunctions.PEStarterModal(deltaz,nz,starter.z,starter.psi);

    for iz := 0 to nz - 1 do
    begin
        {reduce the psi field by sqrt(c*rho) - or the part we are calculating with (psiComplexVect) at least.
        This ensures conservation of energy, on upward/downward sloping zb problems.
        We need to later on put the reduction factor back in to retrieve psi. see sec 6.8 p508}
        rho_ir := TSolversGeneralFunctions.depthIsCloseToInterfaceNoSmoothing(layerDensityList, z[iz], k0, k02);
        initialField.psi[iz] := initialField.psi[iz] / sqrt(localC(iz, zbThis, z, c1, seafloorScheme)*rho_ir);
    end;

    r := 0;
    for ir := 0 to nr - 2 do
    begin
        if (ir = 0) or (zbThis <> zbPrev) then  {new depth profile - also fires at ir = 0}
        begin
            for iz := 0 to nz - 1 do
                setupLames(iz,zbThis, SubDepth1, SubDepth2, c0, k0, k02,
                    z, c1, cpz, cpz2, alphapz, csz, alphasz,
                    seafloorScheme,
                    layerDensityList,
                    rhoz,
                    lames);

            //set initial dilatation (now we have lames[izs])
            if ir = 0 then
            begin
                laZSrc := lames[izs].la;
                for iz := 0 to nz - 1 do
                begin
                    q[2*iz] := 1/laZSrc * initialField.psi[iz]; //del (dilatation) from Collins higher order... elastic bottom eq 27
                    q[2*iz+1] := -1 / sqr(omega) * initialField.psi.get1stDeriv(iz,nz,deltaz); //w (vertical displacement) from Collins higher order... elastic bottom eq 28

                    //dpress2 := (lames[izs].la*q[2*iz]).modulusSquared;
                    //if dpress2 > 0 then
                    //    result[iz,ir] := -10*log10(dpress2 * sqrt(localC(iz, zbThis, z, c1, seafloorScheme)*rho_ir));
                end;
            end;

        end; //new profile

        r := r + dr;

        if ir = 0 then
            output(0); //put this after r+dr so we don't get r = 0

        q := solverStep_v3(q,lames,rhoz,k0,k02,deltaz,deltar,omega,nPadeTerms); //q is now for ir + 1

        output(ir+1);

        if (pcancel <> nil) and pcancel.b then exit;
        fractionDone.d := ir / nr;
    end;

    {$IF defined (DEBUG) and defined (EPEOUT)}
    try
        THelper.setClipboard(s);
    except
    end;
    {$ENDIF}
end;


class procedure TdBSeaPEElastic1Solver.formLk02Mat_v3(const n: integer; const dz,k02: double; const lames: TLames; var L: TComplexMat);
var
    i: integer;
    halfn: integer;
begin
    //L is heptadiagonal. See Collins ..elastic bottom for details
    //p1 = lamda, p2 = mu Lame constants

    halfn := n div 2;
    setlength(L,0);
    setlength(L,n,7);

    for i := 0 to halfn - 1 do
    begin
        //(la + 2mu) eq 6  L del del, A4
        L.mySetValWithDenom(-1,0,0,i,k02*(lames.getLa2Mu(i-1,halfn) + lames.getLa2Mu(i,halfn)),12);
        L.mySetValWithDenom( 0,0,0,i,k02*(lames.getLa2Mu(i-1,halfn) + 6*lames.getLa2Mu(i,halfn) + lames.getLa2Mu(i+1,halfn)),12);
        L.mySetValWithDenom( 1,0,0,i,k02*(lames.getLa2Mu(i+1,halfn) + lames.getLa2Mu(i,halfn)),12);

        //2 dmu/dz eq 6   L del v,  A7
        L.mySetValWithDenom(-1,0,1,i,k02*2*(-lames.getMu(i-1,halfn) + lames.getMu(i  ,halfn)),6*dz);
        L.mySetValWithDenom( 0,0,1,i,k02*2*(-lames.getMu(i-1,halfn) + lames.getMu(i+1,halfn)),3*dz);
        L.mySetValWithDenom( 1,0,1,i,k02*2*( lames.getMu(i+1,halfn) - lames.getMu(i  ,halfn)),6*dz);

        //L v del = 0

        //mu eq 6  L v v, A4
        L.mySetValWithDenom(-1,1,0,i,k02*(lames.getMu(i-1,halfn) + lames.getMu(i,halfn)),12);
        L.mySetValWithDenom( 0,1,0,i,k02*(lames.getMu(i-1,halfn) + 6*lames.getMu(i,halfn) + lames.getMu(i+1,halfn)),12);
        L.mySetValWithDenom( 1,1,0,i,k02*(lames.getMu(i+1,halfn) + lames.getMu(i,halfn)),12);
    end;
end;

class procedure TdBSeaPEElastic1Solver.formLMat_CederbergCollins_kTerm(const n: integer; const dz, omega, k02: double; const rhoz, cpz: TDoubleDynArray; var L: TComplexMat);
    function tryInverse(const a,b: double): double;
    begin
        if (a = 0) or (b = 0) then exit(1);
        result := 1/a - 1/b;
    end;
    function tryInverse2(const a,b,c: double): double;
    begin
        if (a = 0) or (b = 0) or (c = 0) then exit(1);
        result := 2/a - 1/b - 1/c;
    end;
var
    iz: integer;
    dz2, rho, rho_m, rho_p: double;
begin
    //L is tridiagonal. NB L is NOT the same as the L.k0^2 matrix used in the rest of this unit
    //See Collins Cederberg - Application of an improved self-starter to geoacoustic inversion
    // and Collins - Applications and solution of higher-order parabolic equations, for the Galerkin discretization
    // L = rho d/dz 1/rho d/dz + k^2
    // t -> rho
    // o -> 1/rho
    //
    // rho etc term
    // L = (ti-1 + 2.ti)(oi-1 - oi).ui-1/6dz2
    //    + ( ti-1(oi - oi-1) + 2.ti(2.oi - oi-1 - oi+1) + ti+1(oi - oi+1) ).ui/6dz2
    //    + (ti+1 + 2.ti)(oi+1 - oi).ui+1/6dz2

    dz2 := sqr(dz);
    setlength(L,0);
    setlength(L,n,3);

    for iz := 0 to n - 1 do
    begin
        // rho etc term
        if iz = 0 then
            rho_m := rhoz[0]
        else
            rho_m := rhoz[iz - 1];
        rho := rhoz[iz];
        if iz = n - 1 then
            rho_p := rhoz[n - 1]
        else
            rho_p := rhoz[iz + 1];

        L[iz,0] := ( (rho_m + 2*rho) * tryInverse(rho_m, rho) ) / 6*dz2;
        L[iz,1] := (
            rho_m * tryInverse(rho, rho_m)
            + 2 * rho * tryInverse2(rho, rho_m, rho_p)
            + rho_p * tryInverse(rho, rho_p)
            ) / 6*dz2;
        L[iz,2] := ( (rho_p + 2*rho) * tryInverse(rho_p, rho) ) / 6*dz2;

//        if iz = 0 then
//            k2_m := sqr(omega / cpz[0])
//        else
//            k2_m := sqr(omega / cpz[iz - 1]);
//        k2 := sqr(omega / cpz[iz]);
//        if iz = n - 1 then
//            k2_p := sqr(omega / cpz[n - 1])
//        else
//            k2_p := sqr(omega / cpz[iz + 1]);
//
//        // k^2 term
//        L[iz,0] := L[iz,0] + (k2_m + k2) / 12;
//        L[iz,1] := L[iz,1] + (k2_m + 6*k2 + k2_p) / 12;
//        L[iz,2] := L[iz,2] + (k2_p + k2) / 12;
    end;
end;

class procedure TdBSeaPEElastic1Solver.formLMat_CederbergCollins_rhoTerm(const n: integer; const dz, omega, k02: double; const rhoz, cpz: TDoubleDynArray; var L: TComplexMat);
    function tryInverse(const a,b: double): double;
    begin
        if (a = 0) or (b = 0) then exit(1);
        result := 1/a - 1/b;
    end;
    function tryInverse2(const a,b,c: double): double;
    begin
        if (a = 0) or (b = 0) or (c = 0) then exit(1);
        result := 2/a - 1/b - 1/c;
    end;
var
    iz: integer;
    dz2,k2, k2_m{ki-1}, k2_p{ki+1}: double;
begin
    //L is tridiagonal. NB L is NOT the same as the L.k0^2 matrix used in the rest of this unit
    //See Collins Cederberg - Application of an improved self-starter to geoacoustic inversion
    // and Collins - Applications and solution of higher-order parabolic equations, for the Galerkin discretization
    // L = rho d/dz 1/rho d/dz + k^2
    // t -> rho
    // o -> 1/rho
    //
    // rho etc term
    // L = (ti-1 + 2.ti)(oi-1 - oi).ui-1/6dz2
    //    + ( ti-1(oi - oi-1) + 2.ti(2.oi - oi-1 - oi+1) + ti+1(oi - oi+1) ).ui/6dz2
    //    + (ti+1 + 2.ti)(oi+1 - oi).ui+1/6dz2

    dz2 := sqr(dz);
    setlength(L,0);
    setlength(L,n,3);

    for iz := 0 to n - 1 do
    begin
//        // rho etc term
//        if iz = 0 then
//            rho_m := rhoz[0]
//        else
//            rho_m := rhoz[iz - 1];
//        rho := rhoz[iz];
//        if iz = n - 1 then
//            rho_p := rhoz[n - 1]
//        else
//            rho_p := rhoz[iz + 1];
//
//        L[iz,0] := ( (rho_m + 2*rho) * tryInverse(rho_m, rho) ) / 6*dz2;
//        L[iz,1] := (
//            rho_m * tryInverse(rho, rho_m)
//            + 2 * rho * tryInverse2(rho, rho_m, rho_p)
//            + rho_p * tryInverse(rho, rho_p)
//            ) / 6*dz2;
//        L[iz,2] := ( (rho_p + 2*rho) * tryInverse(rho_p, rho) ) / 6*dz2;

        if iz = 0 then
            k2_m := sqr(omega / cpz[0])
        else
            k2_m := sqr(omega / cpz[iz - 1]);
        k2 := sqr(omega / cpz[iz]);
        if iz = n - 1 then
            k2_p := sqr(omega / cpz[n - 1])
        else
            k2_p := sqr(omega / cpz[iz + 1]);

        // k^2 term
        L[iz,0] := L[iz,0] + (k2_m + k2) / 12;
        L[iz,1] := L[iz,1] + (k2_m + 6*k2 + k2_p) / 12;
        L[iz,2] := L[iz,2] + (k2_p + k2) / 12;
    end;
end;

class procedure TdBSeaPEElastic1Solver.formMMat_v3(const n: integer; const dz,omega,omega2: double; const rhoz: TDoubleDynArray; const lames: TLames; var M: TComplexMat);
var
    i,halfn: integer;
    dz2, dz3: double;
begin
    //M is heptadiagonal. See Collins ..elastic bottom for details

    dz2 := sqr(dz);
    dz3 := dz2*dz;

    halfn := n div 2;
    setlength(M,0);
    setlength(M,n,7);

    for i := 0 to halfn - 1 do
    begin
        //-----M del del-----
        //(la + 2mu) d2delta / dz2   eq8 M del del, A6
        M.myAddValWithDenom(-1,0,0,i,   lames.getLa2Mu(i,halfn),dz2);
        M.myAddValWithDenom( 0,0,0,i,-2*lames.getLa2Mu(i,halfn),dz2);
        M.myAddValWithDenom( 1,0,0,i,   lames.getLa2Mu(i,halfn),dz2);

        //rho omega^2 delta   eq8 M del del, A4
        M.myAddValWithDenom(-1,0,0,i,omega2*(rhoz.getVal(i-1,halfn) + rhoz.getVal(i,halfn)),12);
        M.myAddValWithDenom( 0,0,0,i,omega2*(rhoz.getVal(i-1,halfn) + 6*rhoz.getVal(i,halfn) + rhoz.getVal(i+1,halfn)),12);
        M.myAddValWithDenom( 1,0,0,i,omega2*(rhoz.getVal(i+1,halfn) + rhoz.getVal(i,halfn)),12);

        //(dla/dz + 2dmu/dz) ddelta/dz   eq8 M del del, A8
        M.myAddValWithDenom(-1,0,0,i,(lames.getLa2Mu(i-1,halfn) - lames.getLa2Mu(i,halfn)),2*dz2);
        M.myAddValWithDenom( 0,0,0,i,(2*lames.getLa2Mu(i,halfn) - lames.getLa2Mu(i-1,halfn) - lames.getLa2Mu(i+1,halfn)),2*dz2);
        M.myAddValWithDenom( 1,0,0,i,(lames.getLa2Mu(i+1,halfn) - lames.getLa2Mu(i,halfn)),2*dz2);

        //d/dz(dla/dz delta)  eq8 M del del, A9
        M.myAddValWithDenom(-1,0,0,i,(lames.getLa(i-1,halfn) - lames.getLa(i,halfn)),2*dz2);
        M.myAddValWithDenom( 0,0,0,i,(lames.getLa(i-1,halfn) - 2*lames.getLa(i,halfn) + lames.getLa(i+1,halfn)),2*dz2);
        M.myAddValWithDenom( 1,0,0,i,(lames.getLa(i+1,halfn) - lames.getLa(i,halfn)),2*dz2);

        //---- M del v ------
        //omega^2 drho/dz v   eq 9 M del w, A7
        M.myAddValWithDenom(-1,0,1,i,omega2*(-rhoz.getVal(i-1,halfn) + rhoz.getVal(i  ,halfn)),6*dz);
        M.myAddValWithDenom( 0,0,1,i,omega2*(-rhoz.getVal(i-1,halfn) + rhoz.getVal(i+1,halfn)),3*dz);
        M.myAddValWithDenom( 1,0,1,i,omega2*( rhoz.getVal(i+1,halfn) - rhoz.getVal(i  ,halfn)),6*dz);

        // 2 d/dz(dmu/dz dv/dz)   eq9 M del v, A10
        M.myAddValWithDenom(-1,0,1,i,2*(-lames.getMu(i-1,halfn) + lames.getMu(i  ,halfn)),dz3);
        M.myAddValWithDenom( 0,0,1,i,2*( lames.getMu(i-1,halfn) - lames.getMu(i+1,halfn)),dz3);
        M.myAddValWithDenom( 1,0,1,i,2*( lames.getMu(i+1,halfn) - lames.getMu(i  ,halfn)),dz3);

        //----- M v del -------
        // (la + mu) d del / dz  eq10 M v del, A5
        M.myAddValWithDenom(-1,1,-1,i,(-(lames.getLa(i-1,halfn) + lames.getMu(i-1,halfn)) - 2*(lames.getLa(i  ,halfn) + lames.getMu(i  ,halfn))),6*dz);
        M.myAddValWithDenom( 0,1,-1,i,( (lames.getLa(i-1,halfn) + lames.getMu(i-1,halfn)) -   (lames.getLa(i+1,halfn) + lames.getMu(i+1,halfn))),6*dz);
        M.myAddValWithDenom( 1,1,-1,i,( (lames.getLa(i+1,halfn) + lames.getMu(i+1,halfn)) + 2*(lames.getLa(i  ,halfn) + lames.getMu(i  ,halfn))),6*dz);

        // dla/dz del   eq10 M v del, A7
        M.myAddValWithDenom(-1,1,-1,i,(-lames.getLa(i-1,halfn) + lames.getLa(i  ,halfn)),6*dz);
        M.myAddValWithDenom( 0,1,-1,i,(-lames.getLa(i-1,halfn) + lames.getLa(i+1,halfn)),3*dz);
        M.myAddValWithDenom( 1,1,-1,i,( lames.getLa(i+1,halfn) - lames.getLa(i  ,halfn)),6*dz);

        //----- M v v ---------
        //mu d2v/dz2       eq 11 Mvv, A6
        M.myAddValWithDenom(-1,1,0,i,   lames.getMu(i,halfn),dz2);
        M.myAddValWithDenom( 0,1,0,i,-2*lames.getMu(i,halfn),dz2);
        M.myAddValWithDenom( 1,1,0,i,   lames.getMu(i,halfn),dz2);

        //rho omega^2 v       eq 11 Mvv, A4
        M.myAddValWithDenom(-1,1,0,i,omega2*(rhoz.getVal(i-1,halfn) + rhoz.getVal(i,halfn)),12);
        M.myAddValWithDenom( 0,1,0,i,omega2*(rhoz.getVal(i-1,halfn) + 6*rhoz.getVal(i,halfn) + rhoz.getVal(i+1,halfn)),12);
        M.myAddValWithDenom( 1,1,0,i,omega2*(rhoz.getVal(i+1,halfn) + rhoz.getVal(i,halfn)),12);

        //2 dmu/dz dv/dz      eq 11 Mvv, A8
        M.myAddValWithDenom(-1,1,0,i,2*(  lames.getMu(i-1,halfn) - lames.getMu(i  ,halfn)),2*dz2);
        M.myAddValWithDenom( 0,1,0,i,2*(2*lames.getMu(i  ,halfn) - lames.getMu(i-1,halfn) - lames.getMu(i+1,halfn)),2*dz2);
        M.myAddValWithDenom( 1,1,0,i,2*(  lames.getMu(i+1,halfn) - lames.getMu(i  ,halfn)),2*dz2);
    end;
end;

class function TdBSeaPEElastic1Solver.localC(const iz: integer; const zbThis: double; const z,c1: TDoubleDynArray; const seafloorScheme: TSeafloorScheme): double;
var
    aMaterial: TMaterial;
begin
    if z[iz] < zbThis then
        result := c1[iz]         //ssp c
    else
    begin
        //todo seafloor referenced from sea surface?
        aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz], zbThis);
        if assigned(aMaterial) then
            result := aMaterial.c
        else
            result := water.c;
    end;
end;

class procedure TdBSeaPEElastic1Solver.formMminusk02L_v3(const Lk02, M: TComplexMat; var MMinusk02L: TComplexMat);
var
    i,j: integer;
begin
    setlength(MMinusk02L,0);
    setlength(MMinusk02L,length(Lk02),length(Lk02[0]));

    for i := 0 to length(Lk02) - 1 do
        for j := 0 to length(Lk02[0]) - 1 do
            MMinusk02L[i,j] := M[i,j] - Lk02[i,j];
end;


//class function  TdBSeaPEElastic1Solver.selfStarter(const f, srcZ, zmax: double;
//                        const nz,nPadeTerms: integer;
//                        const aZProfile2: TRangeValArray;
//                        const aCProfile2: TDepthValArray;
//                        const seafloorScheme: TSeafloorScheme;
//                        var solverErrors: TSolverErrors): TStarter;
//var
//    q0Shift,q: TComplexVect; // q: delta and w in a column vect eq 6.61, but reduced by srt(r) ?
//    lames: TLames;
//    c0,k0,omega,rho0,k02: double;
//    rhoz,cpz,cpz2,csz,alphapz,alphasz,c1: TDoubleDynArray;
//    iz: integer;
//    deltaz, iBottomExtension, subDepth1, subDepth2: double;
//    zbThis: double;
//    layerDensityList: TDepthValArray;
//    //aMaterial: TMaterial;
//    izs: integer; //src depth index
//    //laZSrc: TComplex; `
//
//    M,MU,ML,Lk02,Mk02L,lhs,lhsL,rhsMat: TComplexMat;
//    //MH: TComplexMatrix;
//    d: double;
//    indx: TArray<integer>;
//    i,j,k: integer;
//    qShift,rhs: TComplexVect;
//    ajm,bjm: TComplex;
//begin
//    result.isOK := false;
//    omega := 2 * pi * f;
//    c0 := aCProfile2.average;
//    rho0 := water.rho_g;  //water density = 1 g/cm3
//    k0 := omega / c0;
//    k02 := sqr(k0);
//
//    iBottomExtension := 2;
//    if (zmax * iBottomExtension) < c0 / f then
//        iBottomExtension := c0 / f / zmax;
//    SubDepth1 := zmax * (iBottomExtension - 0.5);
//    SubDepth2 := zmax * iBottomExtension;
//
//    {depths}
//    deltaz := SubDepth2 / (nz-1);
//    izs := round(srcZ / deltaz);
//    SetLength(result.z,nz);
//    for iz := 0 to nz - 1 do
//        result.z[iz] := deltaz * iz;
//
//    if (izs < 0) or (izs >= nz) then
//    begin
//        include(solverErrors,kSourceBelowSubDepth);
//        exit;
//    end;
//
//    {sound speed profile and wavenumbers}
//    setlength(c1,nz);
//    for iz := 0 to nz - 1 do
//        c1[iz] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2,result.z[iz]);
//
//    setlength(lames, nz);
//    setlength(rhoz, nz);
//    setlength(cpz, nz);
//    setlength(cpz2, nz);
//    setlength(csz, nz);
//    setlength(alphapz, nz);
//    setlength(alphasz, nz);
//
//    zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,0);
//    seafloorScheme.getLayerDensityList(zbThis, rho0, layerDensityList); //returned values are in g/cm3
//
//    for iz := 0 to nz - 1 do
//        setupLames(iz,zbThis, SubDepth1, SubDepth2, c0, k0, k02,
//            result.z, c1, cpz, cpz2, alphapz, csz, alphasz,
//            seafloorScheme,
//            layerDensityList,
//            rhoz,
//            lames);
//
//    formLk02Mat_v3(2*nz,deltaz,k02,lames,Lk02);
//    formMMat_v3(2*nz,deltaz,omega,sqr(omega),rhoz,lames,M);
//    formMminusk02L_v3(Lk02,M,Mk02L);
//
//    setlength(MU,2*nz+1,7+1); //upper
//    for i := 0 to 2*nz-1 do
//        for j := 0 to 7 - 1 do
//            MU[i+1,j+1] := M[i,j];
//
//    //step 1 eq 6.79
//    setlength(ML, 2*nz+1, 7+1);  //lower
//    setlength(indx, 2*nz+1);
//    setlength(q, 2*nz);
//    setlength(q0Shift,2*nz+1);
//    //for I := 0 to 2*nz - 1 do
//    q0Shift[izs*2   + 1] := makeComplex(0,-1/2);
//    //q0Shift[izs*2+2 + 1] := makeComplex(0,-1/2); //todo check that izs is within bounds
//    TBandSolver.bandecOrig(MU,ML,2*nz,3,3,indx,d);  //decompose
//    TBandSolver.banbksOrig(MU,ML,2*nz,3,3,indx,q0Shift); //solve
//    for I := 0 to 2*nz - 1 do
//        q[i] := q0Shift[i+1];
//
//    //step 2
//    q := solverStep_v3(q,lames,rhoz,k0,k02,deltaz,c0/f,omega,nPadeTerms);  //step by 1 wavelength to eliminate evansecent modes
//
//    //step 3  eq 6.80, 6.81
//    for k := 1 to min(7,nPadeTerms) do
//    begin
//        bjm := powerThreeQuarterPadeTermsB[min(7,nPadeTerms)-1,k-1];
//
//        setlength(lhs,0);
//        setlength(lhs,2*nz+1,7+1);
//        setlength(lhsL,0);
//        setlength(lhsL,2*nz+1,7+1);
//        for i := 0 to 2*nz - 1 do
//            for j := 0 to 7 - 1 do
//                lhs[i+1,j+1] := bjm*Mk02L[i,j] + Lk02[i,j];
//
//        ajm := powerThreeQuarterPadeTermsA[min(7,nPadeTerms)-1,k-1];
//        setlength(rhsMat,0);
//        setlength(rhsMat,2*nz+1,7+1);
//        for i := 0 to 2*nz - 1 do
//            for j := 0 to 7 - 1 do
//                rhsMat[i+1,j+1] := ajm*Mk02L[i,j] + Lk02[i,j];
//
//        //form rhs
//        setlength(qShift,0);
//        setlength(qShift,2*nz+1);
//        for i := 0 to 2*nz - 1 do
//            qShift[i+1] := q[i];
//
//        qShift[1] := NullComplex;
//        qShift[2*nz] := NullComplex;
//        setlength(rhs,0);
//        rhs := rhsMat.multByVectHeptaDiag(qShift,2*nz);
//
//        //then solve lhs* dq/dr
//        setlength(indx,0);
//        setlength(indx,2*nz+1);
//        TBandSolver.bandecOrig(lhs,lhsL,2*nz,3,3,indx,d);  //decompose
//        TBandSolver.banbksOrig(lhs,lhsL,2*nz,3,3,indx,rhs); //solve
//        for i := 0 to 2*nz - 1 do
//            q[i] := rhs[i+1];
//
//        //resultZeroEnds;
//    end;
//    setlength(result.psi, nz);
//    for i := 0 to nz - 1 do
//        result.psi[i] := MulComplexParI(k0*q[2*i]);
//
//    result.isOK := true;
//    result.scale(srcZ);
//end;

class function TdBSeaPEElastic1Solver.selfStarterCederbergCollins(const f, srcZ, zmax: double; const nz, nPadeTermsIgnored: integer; const aZProfile2: TRangeValArray; const aCProfile2: TDepthValArray; const seafloorScheme: TSeafloorScheme; var solverErrors: TSolverErrors): TStarter;
//following Application of an Improved Self-Starter to Geoacoustic Inversion, Robert J. Cederberg and Michael D. Collins. IEEE JOURNAL OF OCEANIC ENGINEERING, VOL. 22, NO. 1, JANUARY 1997
//eqs 12 - 15
{var
    q0Shift: TComplexVect; // q: delta and w in a column vect eq 6.61, but reduced by srt(r) ?
    c0,k0,omega,rho0,k02: double;
    rhoz,cpz,cpz2,csz,alphapz,alphasz,c1: TDoubleDynArray;
    iz: integer;
    deltaz: double;
    zbThis: double;
    layerDensityList: TDepthValArray;
    izs: integer; //src depth index
    MU_rho,ML_rho,MU_k,ML_k,L_rhoTerm_not_shifted,L_kTerm_not_shifted,L_rhoTerm,L_kTerm,lhs,lhsL,rhsMat: TComplexMat;
    d: double;
    indx_rho,indx_k: TArray<integer>;
    i,j,k: integer;
    qShift,rhs: TComplexVect;
    r0: double;
    SubDepth1, SubDepth2: double;
    iBottomExtension: double;
    outputConst: TComplex;
    padeA,padeB: TComplexVect;

    lames: TLames;

    procedure doClipBoard(shifted: boolean);
    var
        s: string;
        i: integer;
    begin
        s := '';
        for i := 0 to nz - 1 do
            s := s + q0Shift[i + 1].toString() + #13#10;
        THelper.setClipboard(s);
    end;

    procedure formTmpMat(padeConst: TComplex; mat: TComplexMat);
    var
        i,j: integer;
    begin
        for i := 0 to nz - 1 do
        begin
            for j := 0 to 3 - 1 do
                mat[i+1,j+1] := padeConst*L_rhoTerm[i+1,j+1] + padeConst*L_kTerm[i+1,j+1];
            mat[i+1,1+1] := mat[i+1,1+1] + 1 - padeConst; //diagonal only
        end;
    end;

    procedure zeroEnds(vect: TComplexVect);
    begin
        vect[1] := NullComplex;
        vect[nz] := NullComplex;
    end;}
begin
{    result.isOK := false;
    omega := 2 * pi * f;
    c0 := aCProfile2.average;
    rho0 := water.rho_g;  //water density = 1 g/cm3
    k0 := omega / c0;
    k02 := sqr(k0);

    //pade terms
    r0 := c0 / f; //1 wavelength
    TPEStarterPadeDouble.calcPadeTerms(12,padeA,padeB);

    iBottomExtension := 2;
    if (zmax * iBottomExtension) < c0 / f then
        iBottomExtension := c0 / f / zmax;
    SubDepth1 := zmax * (iBottomExtension - 0.5);
    SubDepth2 := zmax * iBottomExtension;

    //depths
    deltaz := SubDepth2 / (nz-1);
    izs := round(srcZ / deltaz);
    SetLength(result.z,nz);
    for iz := 0 to nz - 1 do
        result.z[iz] := deltaz * iz;

    if (izs < 0) or (izs >= nz) then
    begin
        include(solverErrors,kSourceBelowSubDepth);
        exit;
    end;

    //sound speed profile and wavenumbers
    setlength(c1,nz);
    for iz := 0 to nz - 1 do
        c1[iz] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2,result.z[iz]);

    setlength(rhoz, nz);
    setlength(cpz, nz);
    setlength(cpz2, nz);
    setlength(csz, nz);
    setlength(alphapz, nz);
    setlength(alphasz, nz);
    setlength(lames, nz);

    zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,0);
    seafloorScheme.getLayerDensityList(zbThis, rho0, layerDensityList); //returned values are in g/cm3

    for iz := 0 to nz - 1 do
    begin
        setupLames(iz, zbThis, SubDepth1, SubDepth2,
            c0, k0, k02, result.z, c1,cpz, cpz2, alphapz,
            csz, alphasz, seafloorScheme, layerDensityList, rhoz, lames);

//        rhoz[iz] := TSolversGeneralFunctions.depthIsCloseToInterfaceNoSmoothing(layerDensityList,
//                                                     result.z[iz],  //this depth
//                                                     k0,
//                                                     k02);
    end;

    formLMat_CederbergCollins_rhoTerm(nz,deltaz,omega,k02,rhoz,cpz,L_rhoTerm_not_shifted);
    formLMat_CederbergCollins_kTerm(nz,deltaz,omega,k02,rhoz,cpz,L_kTerm_not_shifted);

    setlength(L_rhoTerm,nz+1,3+1); //upper
    setlength(L_kTerm, nz+1, 3+1);  //lower
    setlength(MU_rho,nz+1,3+1); //upper
    setlength(ML_rho, nz+1, 3+1);  //lower
    setlength(MU_k,nz+1,3+1); //upper
    setlength(ML_k, nz+1, 3+1);  //lower
    for i := 0 to nz - 1 do
        for j := 0 to 3 - 1 do
        begin
            L_rhoTerm[i+1,j+1] := L_rhoTerm_not_shifted[i,j] / k02;
            MU_rho[i+1,j+1] := L_rhoTerm_not_shifted[i,j] / k02; // --> X = 1/k02 * (L - k02);  1 + X = L / k0^2, eq 10 & 11
            L_kTerm[i+1,j+1] := L_kTerm_not_shifted[i,j] / k02;
            MU_k[i+1,j+1] := L_kTerm_not_shifted[i,j] / k02; // --> X = 1/k02 * (L - k02);  1 + X = L / k0^2, eq 10 & 11
        end;

    setlength(indx_rho, nz+1);
    setlength(indx_k, nz+1);
    setlength(q0Shift,nz+1); //for use with 1-indexed algos
    //for I := 0 to 2*nz - 1 do
    q0Shift[izs + 1] := makeComplex(0,-1/2);
    //q0Shift[izs*2+2 + 1] := makeComplex(0,-1/2); //todo check that izs is within bounds
    //doClipBoard(true);
    TBandSolver.bandecOrig(MU_rho,ML_rho,nz,1,1,indx_rho,d);  //decompose
    TBandSolver.bandecOrig(MU_k,ML_k,nz,1,1,indx_k,d);  //decompose
    //eq 13 (1 + X)^2 thetaz = deltaz, so solve twice by (1 + X)
    TBandSolver.banbksOrig(MU_rho,ML_rho,nz,1,1,indx_rho,q0Shift); //solve 1
    TBandSolver.banbksOrig(MU_k,ML_k,nz,1,1,indx_k,q0Shift); //solve 1
    //doClipBoard(true);
    TBandSolver.banbksOrig(MU_rho,ML_rho,nz,1,1,indx_rho,q0Shift); //solve 2
    TBandSolver.banbksOrig(MU_k,ML_k,nz,1,1,indx_k,q0Shift); //solve 1
    //doClipBoard(true);

    //07/01/2017: we get to here, and q0Shift is unchanged. I guess because rho isn't changing in the water

    setlength(rhs,0);
    setlength(rhs,nz+1);
    for i := 0 to nz - 1 do
        rhs[i+1] := q0Shift[i+1];

    setlength(result.psi, nz);
    //eq 15 sort of - we haven't factorised

    //rhs stuff
    setlength(rhsMat,nz+1,3+1);
    for k := 0 to length(padeA) - 1 do
    begin
        formTmpMat(padeA[k], rhsMat);
        zeroEnds(rhs);
        rhs := rhsMat.multByVectTriDiag(rhs.copy,nz);
    end;

    setlength(qShift,nz+1);
    for i := 0 to nz - 1 do
        qShift[i+1] := rhs[i+1];

    setlength(lhs,nz+1,3+1);
    setlength(lhsL,nz+1,3+1);
    setlength(indx_rho,nz+1);
    for k := 0 to length(padeA) - 1 do
    begin
        formTmpMat(padeB[k],lhs);
        zeroEnds(qShift);

        TBandSolver.bandecOrig(lhs,lhsL,nz,1,1,indx_rho,d); //decompose
        TBandSolver.banbksOrig(lhs,lhsL,nz,1,1,indx_rho,qShift);
    end;
    outputConst := ComplexEXP(MakeComplex(0,k0*r0)) / sqrt(k0*r0);
    for i := 0 to nz - 1 do
        result.psi[i] := outputConst * qShift[i + 1];
    result.isOK := true;

    result.scale(srcZ);    }
end;

class procedure TdBSeaPEElastic1Solver.setupLames(const iz: integer;
    const zbThis, SubDepth1, SubDepth2, c0, k0, k02: double;
    const z, c1, cpz, cpz2, alphapz, csz, alphasz: TDoubleDynArray;
    const seafloorScheme: TSeafloorScheme;
    const layerDensityList: TDepthValArray;
    const rhoz: TDoubleDynArray;
    const lames: TLames);
var
    aMaterial: TMaterial;
begin
    if z[iz] < zbThis then
    begin
        cpz[iz] := c1[iz];
        cpz2[iz] := sqr(c1[iz]);         //ssp c
        alphapz[iz] := 0;// atten0*attC;   //water attenuation (todo do bulk attenuation here)
    end
    else
    begin
        aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz] , zbThis); //could move this inside checks for seafloor
        if not assigned(aMaterial) then
        begin
            //nilSedimentError := true;
            aMaterial := water;
        end;

        cpz[iz] := aMaterial.c;
        cpz2[iz] := sqr(aMaterial.c);       //sediment c
        alphapz[iz] := aMaterial.attenuation;  //sediment attenuation

        if z[iz] > SubDepth1 then
            //todo additional is in Np/m, for this calc we want dB/lamda
            alphapz[iz] := alphapz[iz] + TSolversGeneralFunctions.additionalAttenuationInBottomLayer(z[iz],subDepth1,subDepth2);//increase attenuation at the bottom of the sediment layer
    end;

    rhoz[iz] := TSolversGeneralFunctions.depthIsCloseToInterfaceNoSmoothing(layerDensityList,
                                                     z[iz],  //this depth
                                                     k0,
                                                     k02);
    lames[iz] := TLame.fromParams(rhoz[iz],cpz[iz],cpz2[iz],csz[iz],alphapz[iz],alphasz[iz]);
end;

class function TdBSeaPEElastic1Solver.solverStep_v3(const q: TComplexVect; const lames: TLames; const rhoz: TDoubleDynArray; const k0, k02, dz, dr, omega: double; const nPadeTerms: integer): TComplexVect;
var
    Lk02,M,MMinusk02L,lhs,lhsL,rhsMat: TComplexMat;
    indx: TArray<integer>;
    //b: boolean;
    d: double;
    i,k,n: integer;
    rhs, result_shifted: TComplexVect;
    //padeTerms: TPadeTerms;
    //term: TPadeTerm;
    z, zstar: tcomplex;
    j: Integer;

    procedure resultZeroEnds();
    begin
        result[0] := NullComplex; //pressure release surface
        result[1] := NullComplex;  //?
        result[n-2] := NullComplex;
        result[n-1] := NullComplex; //?
    end;
begin
    n := length(q);
    setlength(result, n);

    //6.68
    z := (makecomplex(1,k0*dr/2) / makecomplex(1,-k0*dr/2));
    for i := 0 to n - 1 do
        result[i] := q[i] * z; //crank nicolson

    //6.69 here onwards
    formLk02Mat_v3(n,dz,k02,lames,Lk02);
    formMMat_v3(n,dz,omega,sqr(omega),rhoz,lames,M);
    formMminusk02L_v3(Lk02,M,MMinusk02L);

    // k02 = k0.k0
    // X = M - k02.L
    // V is our delta and w vector
    //[k02.L + bj.X] d/dr V = [i.k0.aj.X] V    eq 6.69
    //crank nicolson
    // [k02.L + bj.X] ( (Vn+1 - Vn) / deltar ) = [i.k0.aj.X]( (Vn+1 + Vn) / 2)
    // [k02.L + bj.X - i.k0.aj.X.deltar/2](Vn+1) = [k02.L + bj.X + i.k0.aj.X.deltar/2](Vn)
    // [k02.L + (bj - i.k0.aj.deltar/2)X](Vn+1) = [k02.L + (bj + i.k0.aj.deltar/2)X](Vn)
    // z = (bj - i.k0.aj.deltar/2), z* = (bj + i.k0.aj.deltar/2)
    // [k02.L + z.X](Vn+1) = [k02.L + z*.X](Vn)

    //padeTerms := TPadeCalc.padeParameters(nPadeTerms);
    for k := 1 to nPadeTerms do
    begin
        z := TPadeCalc.padeParameter(k,nPadeTerms).b - MulComplexParI(1/2*k0*dr*TPadeCalc.padeParameter(k,nPadeTerms).a);

        setlength(lhs,0);
        setlength(lhs,n+1,7+1);
        setlength(lhsL,0);
        setlength(lhsL,n+1,7+1);
        for i := 0 to n - 1 do
            for j := 0 to 7 - 1 do
                lhs[i+1,j+1] := Lk02[i,j] + z*MMinusk02L[i,j];

        zstar := TPadeCalc.padeParameter(k,nPadeTerms).b + MulComplexParI(1/2*k0*dr*TPadeCalc.padeParameter(k,nPadeTerms).a);
        setlength(rhsMat,0);
        setlength(rhsMat,n+1,7+1);
        for i := 0 to n - 1 do
            for j := 0 to 7 - 1 do
                rhsMat[i+1,j+1] := Lk02[i,j] + zstar*MMinusk02L[i,j];

        //shift to 1-indexing
        setlength(result_shifted,0);
        setlength(result_shifted,n+1);
        for i := 0 to n - 1 do
            result_shifted[i+1] := result[i];

        //multiply rhs: [k02.L + z*.X](Vn)
        rhs := rhsMat.multByVectHeptaDiag(result_shifted,n);

        //solve lhs: [k02.L + z.X](Vn+1) = rhs
        setlength(indx,0);
        setlength(indx,n+1);
        TBandSolver.bandecOrig(lhs,lhsL,n,3,3,indx,d);
        TBandSolver.banbksOrig(lhs,lhsL,n,3,3,indx,rhs);

        //shift back to 0-indexing
        for i := 0 to n - 1 do
            result[i] := rhs[i+1];

        resultZeroEnds;
    end;
end;




end.
