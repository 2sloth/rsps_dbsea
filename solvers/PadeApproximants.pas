unit PadeApproximants;

interface
uses system.math, MComplex;

type

    TPadeTerm = record
        a,b: TComplex; //ajn, bjn
    end;
    TPadeTerms = array of TPadeTerm;

    TPadeCalc = class(TObject)
    public
        class function  padeParameter(const j,n: integer): TPadeTerm;
        class function  rotatedPadeParameter(const j,n: integer; const alpha: double): TPadeTerm;
        class function  padeParameters(const n: integer): TPadeTerms;
        class function  rotatedPadeParameters(const n: integer; const alpha: double): TPadeTerms;
        class function  rotatedC0(const n: integer; const alpha: double): TComplex;
    end;

implementation

class function TPadeCalc.padeParameter(const j,n: integer): TPadeTerm;
var
    q,d: double;
begin
    //not rotated, ie alpha = 0;

    //approximate sqrt(1 + q)
    //following Collins Collins higher-order parabolic equation for wave propagation in an ocean overlying an elastic bottom eq 14, 15 (also Jensen 6.34, 6.35)

    q := 2*n + 1;
    d := sqr(sin(j*pi/q));
    result.a := 2/q * d; //6.34
    result.b := 1 - d; // = cos(2*n + 1)^2 //6.35
end;


class function TPadeCalc.padeParameters(const n: integer): TPadeTerms;
var
    j: integer;
begin
    setlength(result,n);
    for j := 0 to n - 1 do
        result[j] := padeParameter(j + 1,n);
end;



class function TPadeCalc.rotatedC0(const n: integer; const alpha: double): TComplex;
var
    j: integer;
    unrotateds: TPadeTerms;
    q: tcomplex;
begin
    //Milinazzo
    unrotateds := padeParameters(n);

    q := ComplexEXP(MakeComplex(0,-alpha)) - 1;
    result := 1;
    for j := 0 to n - 1 do
        result := result + unrotateds[j].a*q / (1 + unrotateds[j].b*q);
    result := ComplexEXP(MulComplexParI( alpha/2*result));
end;

class function TPadeCalc.rotatedPadeParameter(const j, n: integer; const alpha: double): TPadeTerm;
var
    unrotated: TPadeTerm;
    q,denom: TComplex;
begin
    //Milinazzo Rational square root approximations 1996

    q := ComplexEXP( MakeComplex(0,-alpha));
    unrotated := padeParameter(j,n);

    result.b := (unrotated.b * q) / (1 + unrotated.b*(q - 1));

    denom := 1 + unrotated.b*(q - 1);
    result.a := ComplexEXP(MakeComplex(0,-alpha/2))*unrotated.a / (denom*denom);
end;

class function TPadeCalc.rotatedPadeParameters(const n: integer; const alpha: double): TPadeTerms;
var
    j: integer;
begin
    setlength(result,n);
    for j := 0 to n - 1 do
        result[j] := rotatedPadeParameter(j + 1, n, alpha);
end;

end.
