unit raySolverTypes3D;

interface

uses system.sysutils, system.math.vectors,
    Mcomplex;

type

    //phi: angle around source -> slice (azimuth)
    //theta: angle in plane of slice  (declination)

  TRayIterator3D = record
    x,y,z: double;
    xi, eta, zeta: double;

	q: double;
    p: double;
    terminated: longint;
    procedure cloneFrom(const source: TRayIterator3D); overload;
    //procedure cloneFrom(const source: TRayIterator3D; const xi: double); overload;
    //function  compare(const source: TRayIterator3D; const xi: double): Boolean;
    //procedure multiplyBy(const d: double);
    function  rayNormal: TPoint3D;
    procedure multiplyBy(const d: double);
  end;
  TRayIterator2Array = array of TRayIterator3D;

implementation

procedure TRayIterator3D.cloneFrom(const source: TRayIterator3D);
begin
    self.x := source.x;
    self.y := source.y;
    self.z := source.z;
    self.zeta := source.zeta;
    self.xi := source.xi;
    self.eta := source.eta;
    self.q := source.q;
    self.p := source.p;
    self.terminated := source.terminated;
end;

//procedure TRayIterator3D.cloneFrom(const source: TRayIterator3D; const xi: double);
//begin
//    self.r := source.r;
//    self.z := source.z;
//    self.zeta := source.zeta;
//    self.xi := xi;
//    self.q := source.q;
//    self.p := source.p;
//    terminated := 0;
//end;
//
//function  TRayIterator3D.compare(const source: TRayIterator3D; const xi: double): Boolean;
//begin
//    result := (self.r = source.r) and
//              (self.z = source.z) and
//              (self.zeta = source.zeta) and
//              (self.xi = xi) and
//              (self.q = source.q) and
//              (self.p = source.p);
//end;
//
//procedure TRayIterator3D.multiplyBy(const d: double);
//begin
//    self.r := self.r * d;
//    self.z := self.z * d;
//    self.zeta := self.zeta * d;
//    self.q := self.q * d;
//    self.p := self.p * d;
//end;

procedure TRayIterator3D.multiplyBy(const d: double);
begin
    self.x := self.x*d;
    self.y := self.y*d;
    self.z := self.z*d;
    self.xi := self.xi*d;
    self.eta := self.eta*d;
    self.zeta := self.zeta*d;
	self.q := self.x*q;
    self.p := self.x*p;
end;

function TRayIterator3D.rayNormal: TPoint3D;
begin
    result.x := self.xi;  //3.262
    result.y := self.eta;  //3.263
    result.z := self.zeta; //3.264
    result := result.Normalize;
end;

end.
