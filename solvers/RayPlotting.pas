unit RayPlotting;

interface

uses system.generics.collections, raySolverTypes, raySolverTypes3d;

type

  TWrappedRayPos = class(TObject)
      ray: TRayIterator3D;
      constructor Create3D(aRayPos: TRayIterator3D);
      constructor Create2D(aRayPos: TRayIterator2; const x0,y0,angle: double);
  end;
  TRay = class(TObjectList<TWrappedRayPos>)
    visible: boolean;

    constructor Create();
    procedure addPos3D(aRayPos: TRayIterator3D);
    procedure addPos2D(aRayPos: TRayIterator2; const x0,y0,angle: double);
  end;
  TRayList = class(TObjectList<TRay>)
    function addRay: TRay;
  end;
  TSliceRays = class(TObject)
    n: integer;
    rays: TRayList;
    visible: boolean;

    constructor create(an: integer);
    destructor Destroy; override;

    procedure setAllRaysVisible(val: boolean);
    procedure toggleVisible(n: integer);
    function  addRay: TRay;
  end;
  TSliceRaysList = class(TObjectList<TSliceRays>)
    function addSlice(n: integer): TSliceRays;
  end;

var
    sliceRayss: TSliceRaysList;

implementation


function TSliceRays.addRay: TRay;
begin
    result := self.rays.addRay;
end;

constructor TSliceRays.create(an: integer);
begin
    inherited create;
    visible := true;
    self.n := an;
    self.rays := TRayList.create(true);
end;

destructor TSliceRays.Destroy;
begin
    self.rays.free;
  inherited;
end;

procedure TSliceRays.setAllRaysVisible(val: boolean);
var
    ray: TRay;
begin
    for ray in self.rays do
        ray.visible := val;
end;

procedure TSliceRays.toggleVisible(n: integer);
begin
    if (n < 0) or (n >= self.rays.count) then exit;
    self.rays[n].visible := not self.rays[n].visible;
end;

constructor TWrappedRayPos.Create3D(aRayPos: TRayIterator3D);
begin
    inherited create;

    self.ray.cloneFrom(aRayPos);
end;

{ TRayList }

function TRayList.addRay: TRay;
begin
    result := TRay.create;
    self.add(result);
end;

{ TRay }

procedure TRay.addPos3D(aRayPos: TRayIterator3D);
begin
    self.add(TWrappedRayPos.create3D(aRayPos));
end;

procedure TRay.addPos2D(aRayPos: TRayIterator2; const x0,y0,angle: double);
begin
    self.add(TWrappedRayPos.create2D(aRayPos, x0,y0,angle));
end;

constructor TRay.Create;
begin
    inherited create(true);
    visible := true;
end;

{ TSliceRaysList }

function TSliceRaysList.addSlice(n: integer): TSliceRays;
begin
    result := TSliceRays.create(n);
    self.add(result);
end;

constructor TWrappedRayPos.Create2D(aRayPos: TRayIterator2; const x0,y0,angle: double);
begin
    inherited create;

    self.ray.x := x0 + aRayPos.r*cos(angle);
    self.ray.y := y0 + aRayPos.r*sin(angle);
    self.ray.z := aRayPos.z;
    self.ray.xi := aRayPos.xi;
    self.ray.zeta := aRayPos.zeta;
    self.ray.q := aRayPos.q;
    self.ray.p := aRayPos.p;
    self.ray.terminated := aRayPos.terminated;
end;

initialization
    sliceRayss := TSliceRaysList.create(true);

finalization
    sliceRayss.free;

end.
