unit dBSeaPESS1;

{PESS1: split step, solution by fft}

interface

uses
    system.classes, system.math, system.sysutils, system.types,
    helperFunctions,
    fourier, matrices, MComplex,
    dBSeaconstants, MMatrix, baseTypes,
    seafloorScheme, material,
    directivity, solversGeneralFunctions, mcklib, globalsUnit,
    PadeApproximants, mathlib, TriDiagonal, BandSolver;

type


  TdBSeaPESS1Solver = class(TObject)
  private
  //const
    //taylorSeriesConstants: array of double = [1,-12,90,-560,3150,-16632,84084,-411840,1969110,-9237800,42678636,-194699232,878850700,-3931426800,17450721000,-76938289920];
    class function  localC(const iz: integer; const zbThis: double; const z,c1: TDoubleDynArray; const seafloorScheme: TSeafloorScheme): double;
  public
    class function main(const f, srcZ, rmax, dr, zmax: double;
                        const dMax: integer;
                        const aZProfile2: TRangeValArray;
                        const aCProfile2: TDepthValArray;
                        const starter: TStarter;
                        const seafloorScheme: TSeafloorScheme;
                        const pCancel: TWrappedBool;
                        const fractionDone: TWrappedDouble;
                        const zOversamplingFactor, rOversamplingFactor: double;
                        out solverErrors: TSolverErrors
                        ): TMatrix;
  end;

implementation

{$B-}

class function TdBSeaPESS1Solver.localC(const iz: integer; const zbThis: double; const z,c1: TDoubleDynArray; const seafloorScheme: TSeafloorScheme): double;
var
    aMaterial: TMaterial;
begin
    if z[iz] < zbThis then
        result := c1[iz]         //ssp c
    else
    begin
        //todo seafloor referenced from sea surface?
        aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz], zbThis);
        if assigned(aMaterial) then
            result := aMaterial.c
        else
            result := water.c;
    end;
end;

class function TdBSeaPESS1Solver.main(const f, srcZ, rmax, dr, zmax: double;
                               const dMax: integer;
                               const aZProfile2: TRangeValArray;
                               const aCProfile2: TDepthValArray;
                               const starter: TStarter;
                               const seafloorScheme: TSeafloorScheme;
                               const pCancel: TWrappedBool;
                               const fractionDone: TWrappedDouble;
                               const zOversamplingFactor, rOversamplingFactor: double;
                               out solverErrors: TSolverErrors
                               ): TMatrix;
const
    starterR = 1.0;
var
    c1: TDoubleDynArray;
    psiComplexVect: TComplexVect;
    layerDensityList: TDepthValArray;
    psiM: TComplexMat;
    omega, c0,rho0,k0,k02: double;
    deltaz,deltar,deltazOutput: double;
    atten, c_ir, rho_ir: double;
    zbThis, zbPrev: double;
    nz,nr: integer;
    attCConst: double;
    ir,irr,iz,izz,irAtStarter: integer;
    nSqr: double;
    q1,fac: double;
    nzInit, nrInit: integer;
    rOverSamp, zOverSamp: integer;
    SubDepth1,SubDepth2: double;
    dhank2: double;
    iBottomExtension: double;
    starterSum: double;
    dpress2: double;
    nilSedimentError: boolean;
    aMaterial: TMaterial;
    directivityStarterScaling: double;
    bUseModalStarter: boolean;
    nSqz: TComplexVect;
    z: TDoubleDynArray;


    //{$DEFINE MKLFFT}
    {$IFDEF MKLFFT}
    status: longint;
    pHandle: pointer;
    f1,f2: TComplexVect;
    {$ELSE}
    f1,f2: TVector2;
    {$ENDIF}

    function r(const ir: integer): double;
    begin
        result := ir * deltar;
    end;

    procedure fftSolverStep1(const p: TComplexVect);
    var
        iz: integer;
        len: integer;
        q: TComplex;
        kz: double;
    begin
        //eq 6.129

        len := length(p);
        f1[0] := NullComplex;
        f1[len * 2 - 1] := NullComplex;
        for iz := 1 to len - 1 do
        begin
            f1[iz] := p[iz];
            f1[len * 2 - 1 - iz] := -p[iz]; //negative reflection 'above surface' so we have antisymmetric fft input
        end;

        {$IFDEF MKLFFT}
        backwardFFTDouble(pHandle,@(f1[0]),@(f2[0]));
        {$ELSE}
        fft(len*2,f1,f2);  //eq 6.129, term F{psi(ro,z)}
        {$ENDIF}

        for iz := 0 to len - 1 do
        begin
            {vertical wavenumber corresponding to the fft band.
            In a time/freq FFT, period dt (= 1/fs), each band is omega(j) = 2pij/dt/N.
            So in k-space, 2pij/deltaz/N. And here N = 2*len}
            kz := pi * iz / len / deltaz;
            q := complexExp(MakeComplex(0, -deltar/2/k0*kz*kz)); //eq 6.129, term e(-i deltar / (2*k0) * kz^2)
            f2[iz] := q * f2[iz];
            f2[len * 2 - 1 - iz] := q * f2[len * 2 - 1 - iz];
        end;

        {$IFDEF MKLFFT}
        forwardFFTDouble(pHandle,@(f2[0]),@(f1[0]));
        {$ELSE}
        ifft(len*2, f2, f1); //eq 6.129, term F^-1{}
        {$ENDIF}

        for iz := 0 to len - 1 do
        begin
            q := complexExp(MulComplexParI(k0/2*(nSqz[iz] - 1)*deltar)); //eq 6.129, term e*ik0/2[n(r0,z)^2 - 1]deltar
            p[iz] := q * f1[iz];
        end;
    end;

    // fftSolverStep3 splits the nSq step into 2, is more accurate than fftSolverStep1
    procedure fftSolverStep3(const p: TComplexVect);
    var
        iz: integer;
        len: integer;
        q: TComplex;
        kz: double;
    begin
        //eq 6.131

        len := length(p);
        f1[0] := NullComplex;
        f1[len * 2 - 1] := NullComplex;
        for iz := 1 to len - 1 do
        begin
            q := complexExp(MulComplexParI(k0/4*(nSqz[iz] - 1)*deltar)); //eq 6.131
            f1[iz] := p[iz] * q;
            f1[len * 2 - 1 - iz] := -p[iz] * q; //negative reflection 'above surface' so we have antisymmetric fft input
        end;

        {$IFDEF MKLFFT}
        backwardFFTDouble(pHandle,@(f1[0]),@(f2[0]));
        {$ELSE}
        fft(len*2,f1,f2);  //eq 6.131, term F{}
        {$ENDIF}

        for iz := 0 to len - 1 do
        begin
            {vertical wavenumber corresponding to the fft band.
            In a time/freq FFT, period dt (= 1/fs), each band is omega(j) = 2pij/dt/N.
            So in k-space, 2pij/deltaz/N. And here N = 2*len}
            kz := pi * iz / len / deltaz;
            q := complexExp(MakeComplex(0, -deltar/2/k0*kz*kz)); //eq 6.131
            f2[iz] := q * f2[iz];
            f2[len * 2 - 1 - iz] := q * f2[len * 2 - 1 - iz];
        end;

        {$IFDEF MKLFFT}
        forwardFFTDouble(pHandle,@(f2[0]),@(f1[0]));
        {$ELSE}
        ifft(len*2, f2, f1); //eq 6.131, term F^-1{}
        {$ENDIF}

        for iz := 0 to len - 1 do
        begin
            q := complexExp(MulComplexParI(k0/4*(nSqz[iz] - 1)*deltar));
            p[iz] := q * f1[iz];
        end;
    end;
begin
    //solverErrorMessages := '';

    omega := 2 * pi * f;  //angular freq

    c0 := aCProfile2.average;
    rho0 := water.rho_g;  //water density = 1 g/cm3
    k0 := omega / c0;
    k02 := k0*k0;

    {sub bottom sediment stuff}
    iBottomExtension := 2;
    if (zmax * iBottomExtension) < c0 / f then
        iBottomExtension := c0 / f / zmax;

    SubDepth1 := zmax * (iBottomExtension - 0.5);
    SubDepth2 := zmax * iBottomExtension;

    {depths}
    deltazOutput := zmax / dmax; //used for psiM
    zOverSamp := TSolversGeneralFunctions.PEZOversamplingFactor(zOversamplingFactor,f,zMax,c0,dmax); //amount of oversampling - reduce for faster, less accurate
    nzInit := round(iBottomExtension * dMax);
    nz := 1 + (nzInit - 1) * zOverSamp;
    //nz must be a power of 2 for fft. Also we will wrap around 0 depth
    nz := nextPow2(nz);
    deltaz := SubDepth2 / (nz-1);
    //deltaz2 := deltaz * deltaz;
    SetLength(z,nz);
    for iz := 0 to nz - 1 do
        z[iz] := deltaz * iz;

    {sound speed profile and wavenumbers}
    setlength(c1,nz);
    for iz := 0 to nz - 1 do
        c1[iz] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2,z[iz]);

    {ranges}
    nrInit := max(1, saferound(rmax/dr,1));
    rOverSamp := max(1, saferound(rOversamplingFactor*f*rmax/nrInit/c0));  //reduce for faster, less accurate
    nr := max(1, 1 + (nrInit - 1)*rOverSamp);
    deltar := rmax / max(1, (nr - 1));
    if deltar = 0 then
        deltar := 1;

    setlength(nSqz,nz);
    {$IFDEF MKLFFT}
    setlength(f1, nz*2);
    setlength(f2, nz*2);
    status := setupFFTDescriptorDouble(pHandle, nz*2, false);
    THelper.RemoveWarning(status);
    {$ELSE}
    f1.setlength(nz*2);
    f2.setlength(nz*2);
    {$ENDIF}

    setlength(psiM,dMax,nrInit);

    irAtStarter := 0;

    //{$DEFINE DIRECTIVITYSTARTER}
    {$IF defined (DEBUG) and defined (DIRECTIVITYSTARTER)}
    //todo what about scaling for this?
    TSolversGeneralFunctions.PEStarterWithDirectivity(f,k0,srcZ,deltaz,nz,directivities,psiComplexVect,directivityStarterScaling);
    {$ELSE}
    directivityStarterScaling := 1; //implicitly 0 dB for non-directional starter

    //see if we can use the modal starter: if all the starterPsi = 0 we can't use it. If any are non-nan then we can
    bUseModalStarter := false;
    for iz := 0 to length(starter.psi) - 1 do
    begin
        if not (starter.psi[iz] = NullComplex) then
        begin
            bUseModalStarter := true;
            break;
        end;
    end;

    if starter.IsOK and bUseModalStarter then
    begin
        psiComplexVect := TSolversGeneralFunctions.PEStarterInterpolation(deltaz,nz,starter.z,starter.psi);
        irAtStarter := max(0, round(starterR / deltar)); //dBSeaModes doesn't range oversample. irAtStarter is something slightly less than rOversamp
    end
    else
    begin
        include(solverErrors,kFallbackStarterUsed);

        //analytical fallback starter
        //psiComplexVect := TSolversGeneralFunctions.PEStarterGeneralisedGaussian(k0,srcZ,halfWidth,changeAngleRadians,deltaz,nz);
        //psiComplexVect := TSolversGeneralFunctions.PEStarterGaussian(k0,srcZ,deltaz,nz);
        psiComplexVect := TSolversGeneralFunctions.PEStarterGreene(k0,srcZ,deltaz,nz);
        starterSum := ComplexVectModulusSum(psiComplexVect);
        {normalise the starter and correct for the size of deltaz
        directivityStarterScaling is needed as the overall directivity sum <> 0 dB}
        for iz := 0 to nz - 1 do
            psiComplexVect[iz] := psiComplexVect[iz] * 4 / deltaz / starterSum * directivityStarterScaling;
    end;
    {$ENDIF}

    {sediment layers}
    //TODO support for changing seafloor scheme in range
    zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,r(irAtStarter));
    zbPrev := nan;
    seafloorScheme.getLayerDensityList(zbThis, rho0, layerDensityList); {includes a setlength call} //returned values are in g/cm3

    for iz := 1 to dMax - 1 do
    begin
        {copy over to the output matrix. If starterR is large, we may need to copy the starter to multiple ranges}
        for ir := 0 to irAtStarter div rOverSamp do
        begin
            if iz = 1 then
                psiM[1, ir] := psiComplexVect[1] // todo interp psiComplexVect[iz] //shift down by 1 due to pressure release surface
            else
                psiM[iz div zOversamp, ir] := TSolversGeneralFunctions.interpolationFunPE(starter.z, starter.psi, deltazOutput * iz); //z spacing differs between the solvers, so interpolate
        end;
    end;

    for iz := 0 to nz - 1 do
    begin
        {reduce the psi field by sqrt(c*rho) - or the part we are calculating with (psiComplexVect) at least.
        This ensures conservation of energy, on upward/downward sloping zb problems.
        We need to later on put the reduction factor back in to retrieve psi. see sec 6.8 p508}
        TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList, z[iz], k0, k02, rho_ir, q1);
        psiComplexVect[iz] := psiComplexVect[iz] / sqrt(localC(iz, zbThis, z, c1, seafloorScheme)*rho_ir);
    end;

    c_ir := 1500; // stop compiler warning
    THelper.RemoveWarning(c_ir);
    attCConst := 1/(20*log10(exp(1))/f); //convert to nepers/m    eq 6.156 (lambda = c / f and we include c(z) later)
    //q2 := 1/deltaz2/2 ; //1/2 factor comes because we always divide A matrix by 2
    //q3 := makeComplex(0,2 * k0 /deltar);
    //q4 := -1/deltaz2;
    nilSedimentError := false;
    //-------------START RANGE LOOPING-------------------------
    for ir := irAtStarter to nr - 2 do
    begin
        if (zbThis <> zbPrev) then  {new depth profile - also fires at ir = 0}
        begin
            for iz := 0 to nz - 1 do
            begin
                if z[iz] < zbThis then
                begin
                    c_ir := c1[iz];         //ssp c
                    atten := 0;// atten0*attC;   //water attenuation (todo do bulk attenuation here)
                end
                else
                begin
                    aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz] , zbThis); //could move this inside checks for seafloor
                    if not assigned(aMaterial) then
                    begin
                        nilSedimentError := true;
                        aMaterial := water;
                    end;

                    c_ir := aMaterial.c;       //sediment c
                    atten := aMaterial.attenuation*attCConst/c_ir;  //sediment attenuation

                    if z[iz] > SubDepth1 then
                        atten := atten + TSolversGeneralFunctions.additionalAttenuationInBottomLayer(z[iz],subDepth1,subDepth2);//increase attenuation at the bottom of the sediment layer
                end;

                {check if close enough to the bottom to consider calculating the
                density difference function (function is 0 for constant density)}

                //get rho, check if we are near an interface and so need to smooth rho
                TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList,
                                                                 z[iz],  //this depth
                                                                 k0,
                                                                 k02,
                                                                 rho_ir, //rho gets set
                                                                 q1); //q1 gets set

                nSqr := c0*c0 / c_ir/c_ir; //nsquared, not n
                nSqz[iz].r := nSqr + q1; //add in the contribution from the sediment density change
                nSqz[iz].i := (nSqr + q1)*atten; //q1 has no imaginary part eq 6.158
            end; //iz
        end; //new profile

        //--------------------------------March solution in range------------------------------
        fftSolverStep3(psiComplexVect);
        //psiComplexVect is now for ir+1

        zbPrev := zbThis;
        zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,r(ir + 1));
        if (zbThis <> zbPrev) then
            seafloorScheme.getLayerDensityList(zbThis, rho0, layerDensityList);

        {put the outgoing psi vector into our psiM matrix, un-reducing by sqrt(c*rho)}
        if ((ir + 1) mod rOversamp) = 0 then
        begin
            irr := (ir + 1) div rOversamp;
            for izz := 0 to dMax - 1 do
            begin
                iz := ifthen(izz = 0, 1, izz); //shift down by 1, as we force psiComplexVect[0] = 0
                TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList,z[iz],k0,k02,rho_ir,q1);
                psiM[izz, irr] := TSolversGeneralFunctions.interpolationFunPE(z, psiComplexVect, deltazOutput * iz) * //get the solution back to our psiM grid
                    sqrt(localC(iz, zbThis, z, c1, seafloorScheme)*rho_ir); //un-reduce by sqrt(c*rho) see sec 6.8 p508
            end;
        end;

        //UI feedback
        //if Assigned(updateLabelProc) and (ir mod 1000 = 0) then updateLabelProc('Marching solution in range. Step ' + tostr(ir) + ' of ' + tostr(nr));
        if (pcancel <> nil) and pcancel.b then exit;
        fractionDone.d := ir / nr;
    end; //ir

    if nilSedimentError then
        include(solverErrors,kNoSediment);// := solverErrorMessages + 'Sediment not found, check sediment for this scenario' + #13#10;

    {put the abs(hankel function) back into the envelope function to get the pressure.
    6.5 gives Ho(1)(k0r) = sqrt(2/(pi*k0*r)) exp(i *(k0r - pi/4))
    we can ignore the complex exponential as we just want magnitude.
    so squaring the magnitude part of 6.5 we have 2/(pi*k0*r). NB k0r >> 1 for this approximation}

    {if we have a modal starterR, the starter was calculated at a distance out from the source or starterR.
    We want our hankel func to pass through 1 at range starterR (and be == 1 before that). For the fallback starters, use a normal hankel func}
    if starter.IsOk then
        fac := starterR
    else
        fac := 2/(pi*k0);

    setlength(result,dMax,nrInit);
    for ir := 0 to nrInit - 1 do
    begin
        if starter.IsOK and (r(ir*rOverSamp) <= starterR) then
            dhank2 := 1
        else
            dhank2 := fac / r(ir*rOverSamp);

        for iz := 0 to dMax - 1 do
        begin
            dpress2 := psiM[iz,ir].modulusSquared * dhank2; {eq 6.3 squared}
            if (dpress2 > 0) then
                result[iz,ir] := -10*log10(dpress2);
        end;
    end;

    {$IFDEF MKLFFT}
    freeFFTDescriptor(pHandle);
    {$ENDIF}
end;

end.

