unit raySolverTypes;

interface

uses system.sysutils,
    {$IF Defined(CLSOLVE)}CL,{$ENDIF}
    Mcomplex;

type

    TRayDeath = (kWest,kEast,kSouth,kNorth,kAir,kSeafloor,kLand,kBounces,kAmplitude,kError);
    TRayDeaths = array[TRayDeath] of integer;
    TRayDeathsHelper = class(TObject) //nb can't use record helper, as TRayDeaths is not considered a record
        class procedure init(out rd: TRayDeaths);
        class procedure add(var rd: TRayDeaths; const other: TRayDeaths);
        class function  summary(const rd: TRayDeaths): string;
        class function  summary3D(const rd: TRayDeaths): string;
    end;


  TRayIterator2 = record
  {$IF Defined(CLSOLVE)}
    r: cl_double;
    z: cl_double;
    zeta: cl_double;
    xi: cl_double;
	q: cl_double;
    p: cl_double;
    terminated: cl_int;
    {$ELSE}
    r: double;
    z: double;
    zeta: double;
    xi: double;
	q: double;
    p: double;
    terminated: longint;
    {$ENDIF}
    // ray tangent = c[xi, zeta];
    // ray normal = c[-zeta, xi];

    procedure cloneFrom(const source: TRayIterator2); overload;
    procedure cloneFrom(const source: TRayIterator2; const xi: double); overload;
    function  compare(const source: TRayIterator2; const xi: double): Boolean;
    procedure multiplyBy(const d: double);
  end;
  TRayIterator2Array = array of TRayIterator2;

      {$IF Defined(CLSOLVE)}
    MyComplex = record
        r: cl_double;
        i: cl_double;
    end;

  t_platform_device = record
      platform_id: cl_platform_id; // index for platform_id
      device_type: cl_device_type; // CL_DEVICE_TYPE_CPU; CL_DEVICE_TYPE_GPU; CL_DEVICE_TYPE_ACCELERATOR
  end;
  {$ENDIF}
implementation

procedure TRayIterator2.cloneFrom(const source : TRayIterator2);
begin
    self.r := source.r;
    self.z := source.z;
    self.zeta := source.zeta;
    self.xi := source.xi;
    self.q := source.q;
    self.p := source.p;
    self.terminated := source.terminated;
end;

procedure TRayIterator2.cloneFrom(const source : TRayIterator2; const xi : double);
begin
    self.r := source.r;
    self.z := source.z;
    self.zeta := source.zeta;
    self.xi := xi;
    self.q := source.q;
    self.p := source.p;
    terminated := 0;
end;

function  TRayIterator2.compare(const source : TRayIterator2; const xi : double) : Boolean;
begin
    result := (self.r = source.r) and
              (self.z = source.z) and
              (self.zeta = source.zeta) and
              (self.xi = xi) and
              (self.q = source.q) and
              (self.p = source.p);
end;

procedure TRayIterator2.multiplyBy(const d : double);
begin
    self.r := self.r * d;
    self.z := self.z * d;
    self.zeta := self.zeta * d;
    self.xi := self.xi * d;
    self.q := self.q * d;
    self.p := self.p * d;
end;


{ TRayDeathsHelper }

class procedure TRayDeathsHelper.add(var rd: TRayDeaths; const other: TRayDeaths);
var
    k: TRayDeath;
begin
    for k := Low(TRayDeath) to High(TRayDeath) do
        rd[k] := rd[k] + other[k];
end;

class procedure TRayDeathsHelper.init(out rd: TRayDeaths);
var
    k: TRayDeath;
begin
    for k := Low(TRayDeath) to High(TRayDeath) do
        rd[k] := 0;
end;

class function TRayDeathsHelper.summary(const rd: TRayDeaths): string;
begin
//kAir,kSeafloor,kLand,kBounces,kAmplitude,kError
    result := 'exited at zero range: ' + inttostr(rd[kWest]) +
        ', exited at max range: ' + inttostr(rd[kEast]) +
        ', exited in air: ' + inttostr(rd[kAir]) +
        ', exited in seafloor: ' + inttostr(rd[kSeafloor]) +
        ', killed due to maximum seafloor bounces: ' + inttostr(rd[kBounces]) +
        ', killed due to amplitude: ' + inttostr(rd[kAmplitude]) +
        ', killed due to error: ' + inttostr(rd[kError]);
end;

class function TRayDeathsHelper.summary3D(const rd: TRayDeaths): string;
begin
//kAir,kSeafloor,kLand,kBounces,kAmplitude,kError
    result := 'exited at zero x: ' + inttostr(rd[kWest]) +
        ', exited at max x: ' + inttostr(rd[kEast]) +
        ', exited at zero y: ' + inttostr(rd[kSouth]) +
        ', exited at max y: ' + inttostr(rd[kNorth]) +
        ', exited in air: ' + inttostr(rd[kAir]) +
        ', exited in seafloor: ' + inttostr(rd[kSeafloor]) +
        ', killed due to hitting land: ' + inttostr(rd[kLand]) +
        ', killed due to maximum seafloor bounces: ' + inttostr(rd[kBounces]) +
        ', killed due to amplitude: ' + inttostr(rd[kAmplitude]) +
        ', killed due to error: ' + inttostr(rd[kError]);
end;

end.
