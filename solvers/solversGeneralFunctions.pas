unit solversGeneralFunctions;

interface

uses uTExtendedX87,
    system.math, system.SysUtils, system.Types, system.math.Vectors,
    system.Generics.collections,
    fmx.types3d,
    matrices, basetypes, directivity, raySolverTypes, raySolverTypes3D,
    helperFunctions, mcklib, mcomplex, nullable,
    fourier, TriDiagonal, PadeApproximants;

type

    TSolverError = (
        kNoSediment,
        kNoModes,
        kNoFrequencies,
        kFallbackStarterUsed,
        kSourceBelowSubDepth,
        kSourceBelowSeafloorRaySolver
    );
    TSolverErrors = set of TSolverError;
    TSolverErrorHelper = record helper for TSolverError
        function  toString: string;
    end;

    TStarter = record
        psi: TComplexVect;
        z: TDoubleDynArray;
        isOK: boolean;

        procedure scale(const srcZ: double);
    end;

    TGetDepthRec = record
        z,ang,m,c: double;
    end;

    TGetDepthRec3D = record
        //count: integer;
        z,theta1: double;
        outOfBounds: boolean;
    end;

    TMatrixFormer = record
        w1,w2,wstar1,wstar2,w2Overwstar2: TComplex;
        k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1, k0SqdeltazSqOver2Timesw1Overw2Minus1: TComplex;
    end;

    TMatrixFormerHorizontal = record
        w1,w2,wstar1,wstar2,w2Overwstar2: TComplex;
        k0SqrSqdeltaThetaSqOver2Timeswstar1Overwstar2Minus1, k0SqrSqdeltaThetaSqOver2Timesw1Overw2Minus1: TComplex;
    end;

    TDiagTerms = record
        u,v,uHat: TComplex;
    end;

    TSolversGeneralFunctions = class
    const
        xlen = 5;
        EPS    = 0.000001;         // smallest positive value: less than that to be considered zero
        EPSEPS = EPS*EPS;        // and its square

        //according to Greene. eq 6.31. Good to about 40 degrees
        a0 = 0.99987;
        a1 = 0.79624;
        b0 = 1.0;
        b1 = 0.30102;
    private
        //class procedure TDMASolverComplexInPlaceFirstAndLastOnly(const C: TTriDiagonal; const rhs,tmpUpper,tmpRhs,result: TComplexVect);
        class function  sqLineMagnitude(const x1, y1, x2, y2: double): double;
    public
        class procedure calcMatrixFormers(const nPadeTerms: integer; const padeTerms: TPadeTerms; const k0, k0SqdeltazSqOver2, thisDeltar: double; var result: TArray<TMatrixFormer>);
        class procedure calcMatrixFormersHorizontal(const nPadeTerms: integer; const padeTerms: TPadeTerms; const k0, k0SqrSqdeltaThetaSqOver2, thisDeltar: double; var result: TArray<TMatrixFormerHorizontal>); //allowing for variable deltar
        class function  calcPlaneNormal(const x0, x1, x2, y0, y1, y2, z0, z1, z2: double): TPoint3d;
        class function  rayIntersectsPlane(const ray: TRayIterator3D; const rayNormal, planeNormal: TPoint3d; const px0, py0, pz0: double; var intersection: TPoint3d): boolean;
        class function  myInterp(const newx,oldx,oldy: TDoubleDynArray): TDoubleDynArray;
        class procedure myinterp2(const n: integer; const dx: double; const cProfile: TDepthValArray; var result: TDoubleDynArray);
        class function  getDirectivityAtFreq2(const f: double; const theta: TDoubleDynArray; const directivities: TDirectivityList): TDepthValArray;
        class function  getDirectivityAtFreq(const f: double; const theta: TDoubleDynArray; const directivities: TDirectivityList): TSingleDynArray;
        class function  getDirectivityAsMatrix(const f: TDoubleDynArray; const theta: TDoubleDynArray; const directivities: TDirectivityList): TSingleMatrix;
        class function  distPointToLineSegment(const px, py, x1, y1, x2, y2: double): double;
        class function  distPointToLineSegment3D(const px,py,pz, ax,ay,az, bx,by,bz: double): double;
        class function  lineSegmentPassesPoint2D(const px, py, x1, y1, x2, y2: double): boolean;
        class function  addIterator(const x, dx: TRayIterator2; const ds: double): TRayIterator2;
        class function  addIterator3D(const x, dx: TRayIterator3D; const ds: double): TRayIterator3D;
        class function  rayf2(const x: TRayIterator2; const c, dc_dz, d2c_dz2, prev_c, prev_dc_dz, prevR, prev_dc_dr: double; var dc_dr_return: double): TRayIterator2; overload;
        class function  rayf3D(const x: TRayIterator3D; const c, dcdz, d2cdz2: double): TRayIterator3D; overload;
        class function  rk2Step3D(const x0: TRayIterator3D; const ds, c, dc_dz, d2c_dz2: double): TRayIterator3D;
        class function  rk2StepMCK(const x0: TRayIterator2; const ds, prevR, c, dc_dz, d2c_dz2, prev_c, prev_dc_dz: double; var prev_dc_dr: double): TRayIterator2;
        class function  rk4Step3D(const x0: TRayIterator3D; const ds, c, dc_dz, d2c_dz2: double): TRayIterator3D;
        class function  rk4StepMCK(const x0: TRayIterator2; const ds, prevR, c, dc_dz, d2c_dz2, prev_c, prev_dc_dz: double; var prev_dc_dr: double): TRayIterator2;
        class function  getdepth(const zProfile: TRangeValArray; const r : double): TGetDepthRec;
        class function  rayHitSeafloor3D(const depths: TSingleMatrix; const deltax, deltay, c, stepSize: double; var xnext: TRayIterator3D; const xprev: TRayIterator3D; out xp: TGetDepthRec3D): boolean;
        class function  TDMAsolver(const a,b,c0,d0: TDoubleDynArray): TDoubleDynArray;
        class procedure TDMAsolverInPlace(const a,b,c0,d0,result: TDoubleDynArray);
        class function  my2norm(const x, rho: TDoubleDynArray): double;
        class function  psiNormalise(const x, rhoz: TDoubleDynArray): TDoubleDynArray;
        class procedure psiNormaliseInPlace(const x, rhoz, result: TDoubleDynArray);
        class function  tridDeterminant(const a,b,c: TDoubleDynArray; const lambda: double): TExtendedX87;
        class function  modalInterpolationFn(const rd: TRangeValArray; const range: double): double; overload;
        class function  modalInterpolationFn(const dv: TDepthValArray; const d: double): double; overload;
        class function  modalInterpolationFn(const aM: TMatrix; const d: double): double; overload;
        class function  tridEigs(const a,b,c: TDoubleDynArray; const nlam,nlam2: integer; const upperlimit : double; const maxModes: integer): TDoubleDynArray;
        class function  TDMASolverComplex(const a,b,c0,d0: TComplexVect): TComplexVect; overload;
        class function  TDMASolverComplex(const C: TTriDiagonal; const d0: TComplexVect): TComplexVect; overload;
        //class procedure TDMASolverBiComplexInPlace(const C: TQuadTriDiagonal; const tmpUpper: TQuadComplexArray; const rhs, tmpRhs, result: TBiComplexArray);
        class procedure TDMASolverComplexInPlace(const C: TTriDiagonal; const rhs,tmpUpper,tmpRhs,result: TComplexVect);
        class procedure TDMASolverComplexInPlaceWrap(const C, CShort: TTriDiagonal; const rhs,tmpUpper,tmpRhs,xone,xtwo,rhsShort,firstAndLastOnlyRhsShort,tmpUpperShort,tmpRhsShort,result: TComplexVect);
        class function  TDMAMultiplyComplex(const a,b,c0,d0: TComplexVect): TComplexVect;
        class procedure TDMAMultiplyComplexInPlace(const B: TTriDiagonal; const vect, result: TComplexVect);
        class procedure TDMAMultiplyComplexInPlaceWrap(const B: TTriDiagonal; const vect, result: TComplexVect);
        class procedure depthIsCloseToInterface(const dv: TDepthValArray; const depth, k0, k02: double; out rho, q1: double);
        class function  depthIsCloseToInterfaceNoSmoothing(const dv: TDepthValArray; const depth, k0, k02: double): double;
        class function  interpolationFunPE(const aM: TMatrix; const d: double): double; overload;
        class function  interpolationFunPE(const rd: TRangeValArray; const d: double): double; overload;
        class function  interpolationFunPE(const rd: TDepthValArray; const d: double): double; overload;
        class function  interpolationFunPE(const z: TDoubleDynArray; const aC: TComplexVect; const d: double): TComplex; overload;
        class function  resample(const v: TDoubleDynArray; const n: integer): TDoubleDynArray;
        class procedure setupRayAngles(const thetaStart, thetaEnd: double; const ntheta: integer; out theta: TDoubleDynArray; out delTheta: double);
        class function  PEStarterThompson(const k0, srcZ, halfBeamWidth, deltaz: double; const nz: integer): TComplexVect;
        class function  PEStarterGreene(const k0, srcZ, deltaz: double; const nz: integer): TComplexVect;
        class function  PEStarterGaussian(const k0, srcZ, deltaz: double; const nz: integer): TComplexVect;
        class function  PEStarterGeneralisedGaussian(const k0, srcZ, halfWidth, angleChangeRadians, deltaz: double; const nz : integer): TComplexVect;
        class procedure PEStarterAngleChange(const f, c0, angleChangeRadians, deltaz: double; const nz: integer; const psiComplexVect: TComplexVect);
        class function  PEStarterInterpolation(const deltaz: double; const nz: integer; const modalStarterZ: TDoubleDynArray; const modalStarterPsi: TComplexVect): TComplexVect;
        class procedure PEStarterWithDirectivity(const f, k0, srcZ, deltaz: double; const nz: integer; const directivities: TDirectivityList; out starter: TComplexVect; out starterScalingFactor: double);
        class procedure applyDirectivityToStarter(const f, k0, srcZ, deltaz: double; const nz: integer; const directivities: TDirectivityList; const starter: TStarter);
        class procedure modesKcAndPsi(const im, nz: integer;
                                      const omega, h: double;
                                      const theEigs, Dd, Ed, rhozVect, attenVect, cVect: TDoubleDynArray;
                                      var tmpPsi1, tmpPsi2, mdmid: TDoubleDynArray;
                                      var kc: TComplexVect; var psi: TMatrix);
        class function  PEZOversamplingFactor(const zOversamplingFactor, f, zMax, c0: double; const dMax: integer): integer;
        class function  targetPressureAtRange(const r, srcZ: double): double;
        class function  additionalAttenuationInBottomLayer(const z, subDepth1, subDepth2: double): double;
        class function  nLamIncrease(const f: double): double;
        class function getSolveFreqInds(const solveThisFreq: TBooleanDynArray): TArray<integer>;
    end;

implementation

{$B-}

class procedure TSolversGeneralFunctions.calcMatrixFormers(const nPadeTerms: integer; const padeTerms: TPadeTerms; const k0, k0SqdeltazSqOver2, thisDeltar: double; var result: TArray<TMatrixFormer>); //allowing for variable deltar
var
    k: integer;
begin
    if length(result) = 0 then
        setlength(result, max(1,nPadeTerms));

    if nPadeTerms = 0 then
    begin
        //Greene's approximation
        result[0].w1     := MakeComplex( TSolversGeneralFunctions.b0,  k0*thisDeltar/2*(TSolversGeneralFunctions.a0 - TSolversGeneralFunctions.b0));  //6.180 to 6.183
        result[0].wstar1 := result[0].w1.conjugate;
        result[0].w2     := MakeComplex( TSolversGeneralFunctions.b1,  k0*thisDeltar/2*(TSolversGeneralFunctions.a1 - TSolversGeneralFunctions.b1));
        result[0].wstar2 := result[0].w2.conjugate;
        result[0].w2Overwstar2 := result[0].w2 / result[0].wstar2;
        result[0].k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1 := k0SqdeltazSqOver2 * result[0].wstar1 / result[0].wstar2 - 1;
        result[0].k0SqdeltazSqOver2Timesw1Overw2Minus1 := k0SqdeltazSqOver2 * result[0].w1 / result[0].w2 - 1;
    end
    else
    begin
        //see notes page 501 section 6.62 re cancellation when doing pade version
        //w1 = b0 + ik0deltar/2*(a0 - b0) -> w1 = b0 + ik0deltar/2*(a0)
        //w2 = b1 + ik0deltar/2*(a1 - b1) -> w2 = b1 + ik0deltar/2*(a1)

        for k := 0 to nPadeTerms - 1 do
        begin
            //a0 = 0, b0 = 1, a1 = pade.a = ajn, a2 = pade.b = bjn
            //result[k].w1     := MakeComplex(1,  -k0*thisDeltar/2);  //6.180 to 6.183
            result[k].w1 := 1;
            result[k].wstar1 := result[k].w1.conjugate;
            //result[k].w2     := padeTerms[k].b + MulComplexParI( k0*thisDeltar/2*(padeTerms[k].a - padeTerms[k].b));
            result[k].w2     := padeTerms[k].b + MulComplexParI( k0*thisDeltar/2*(padeTerms[k].a));
            result[k].wstar2 := result[k].w2.conjugate;
            result[k].w2Overwstar2 := result[k].w2 / result[k].wstar2;
            result[k].k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1 := k0SqdeltazSqOver2 * result[k].wstar1 / result[k].wstar2 - 1;
            result[k].k0SqdeltazSqOver2Timesw1Overw2Minus1 := k0SqdeltazSqOver2 * result[k].w1 / result[k].w2 - 1;
        end;
    end;
end;

class procedure TSolversGeneralFunctions.calcMatrixFormersHorizontal(
    const nPadeTerms: integer;
    const padeTerms: TPadeTerms;
    const k0, k0SqrSqdeltaThetaSqOver2, thisDeltar: double;
    var result: TArray<TMatrixFormerHorizontal>); //allowing for variable deltar
var
    k: integer;
begin
    if length(result) = 0 then
        setlength(result, max(1,nPadeTerms));

    if nPadeTerms = 0 then
    begin
        //Greene's approximation
        result[0].w1     := MakeComplex( TSolversGeneralFunctions.b0,  thisDeltar*k0/2*(TSolversGeneralFunctions.a0 - TSolversGeneralFunctions.b0));  //6.180 to 6.183
        result[0].wstar1 := result[0].w1.conjugate;
        result[0].w2     := MakeComplex( TSolversGeneralFunctions.b1,  thisDeltar*k0/2*(TSolversGeneralFunctions.a1 - TSolversGeneralFunctions.b1));
        result[0].wstar2 := result[0].w2.conjugate;
        result[0].w2Overwstar2 := result[0].w2 / result[0].wstar2;
        result[0].k0SqrSqdeltaThetaSqOver2Timeswstar1Overwstar2Minus1 := k0SqrSqdeltaThetaSqOver2 * result[0].wstar1 / result[0].wstar2 - 1;
        result[0].k0SqrSqdeltaThetaSqOver2Timesw1Overw2Minus1 := k0SqrSqdeltaThetaSqOver2 * result[0].w1 / result[0].w2 - 1;
    end
    else
    begin
        //see notes page 501 section 6.62 re cancellation when doing pade version
        //w1 = b0 + ik0deltar/2*(a0 - b0) -> w1 = b0 + ik0deltar/2*(a0)
        //w2 = b1 + ik0deltar/2*(a1 - b1) -> w2 = b1 + ik0deltar/2*(a1)

        for k := 0 to nPadeTerms - 1 do
        begin
            //a0 = 0, b0 = 1, a1 = pade.a = ajn, b1 = a2 = pade.b = bjn
            //result[k].w1     := MakeComplex(1,  -thisDeltar*k0/2);  //6.180 to 6.183
            result[k].w1 := 1;
            result[k].wstar1 := result[k].w1.conjugate;
            //result[k].w2     := padeTerms[k].b + MulComplexParI( thisDeltar*k0/2*(padeTerms[k].a - padeTerms[k].b));
            result[k].w2     := padeTerms[k].b + MulComplexParI( thisDeltar*k0/2*(padeTerms[k].a));
            result[k].wstar2 := result[k].w2.conjugate;
            result[k].w2Overwstar2 := result[k].w2 / result[k].wstar2;
            result[k].k0SqrSqdeltaThetaSqOver2Timeswstar1Overwstar2Minus1 := k0SqrSqdeltaThetaSqOver2 * result[k].wstar1 / result[k].wstar2 - 1;
            result[k].k0SqrSqdeltaThetaSqOver2Timesw1Overw2Minus1 := k0SqrSqdeltaThetaSqOver2 * result[k].w1 / result[k].w2 - 1;
        end;
    end;
end;

class function TSolversGeneralFunctions.myinterp(const newx,oldx,oldy: TDoubleDynArray): TDoubleDynArray;
var
    i,ip,iq,len,lenoldx : integer;
begin
    len := length(newx) - 1;
    lenoldx := length(oldx) - 1;
    setlength(result,len + 1);
    for i := 0 to len do
    begin
        if newx[i] <= oldx[0] then
            result[i] := oldy[0]
        else
        if newx[i] >= oldx[lenoldx] then
            result[i] := oldy[lenoldx]
        else
        begin
            iq := 1;  //iq is the point past the desired interp point
            while newx[i] > oldx[iq] do
                iq := iq + 1;
            ip := iq - 1; //the point before the interp point
            result[i] := oldy[ip] + (newx[i] - oldx[ip])*(oldy[iq] - oldy[ip])/(oldx[iq] - oldx[ip]);
        end;
    end;
end;

class procedure TSolversGeneralFunctions.myinterp2(const n: integer; const dx: double; const cProfile: TDepthValArray; var result: TDoubleDynArray);
var
    i,ip,iq,lenoldx: integer;
    thisDepth: double;
begin
    lenoldx := length(cProfile) - 1;

    if length(result) <> n then
        setlength(result,n);

    for i := 0 to n - 1 do
    begin
        thisDepth := i*dx;
        if thisDepth <= cProfile[0].depth then
            result[i] := cProfile[0].val
        else
        if thisDepth >= cProfile[lenoldx].depth then
            result[i] := cProfile[lenoldx].val
        else
        begin
            iq := 1;  //iq is the point past the desired interp point
            while thisDepth > cProfile[iq].depth do
                inc(iq);
            ip := iq - 1; //the point before the interp point
            result[i] := cProfile[ip].val + (thisDepth - cProfile[ip].depth)*(cProfile[iq].val - cProfile[ip].val)/(cProfile[iq].depth - cProfile[ip].depth);
        end;
    end;
end;

class function  TSolversGeneralFunctions.getDirectivityAtFreq(const f: double; const theta: TDoubleDynArray; const directivities: TDirectivityList): TSingleDynArray;
var
    fArray : TDoubleDynArray;
    directivityMatrix : TSingleMatrix;
begin
    setlength(fArray,1);
    fArray[0] := f;
    directivityMatrix := TSolversGeneralFunctions.getDirectivityAsMatrix(fArray,theta,directivities);
    result := directivityMatrix[0].copy;
end;

class function TSolversGeneralFunctions.getDirectivityAtFreq2(const f: double; const theta: TDoubleDynArray; const directivities: TDirectivityList): TDepthValArray;
var
    fArray: TDoubleDynArray;
    directivityMatrix: TSingleMatrix;
    i: integer;
begin
    fArray := [f];
    directivityMatrix := TSolversGeneralFunctions.getDirectivityAsMatrix(fArray,theta,directivities);
    setlength(result, length(theta));
    for I := 0 to length(theta) - 1 do
    begin
        result[i].depth := theta[i];
        result[i].val := directivityMatrix[0,i];
    end;
end;

class function TSolversGeneralFunctions.getSolveFreqInds(const solveThisFreq: TBooleanDynArray): TArray<integer>;
var
    p: TPair<integer,boolean>;
begin
    for p in THelper.iterate<boolean>(TArray<boolean>(solveThisFreq)) do
        if p.Value then
            THelper.append<integer>(p.Key,result);
end;

class function  TSolversGeneralFunctions.getDirectivityAsMatrix(const f: TDoubleDynArray; const theta: TDoubleDynArray; const directivities: TDirectivityList): TSingleMatrix;
    function  getDirectivityAtFreq(const f: double; const directivities: TDirectivityList): TDoubleDynArray;
    var
        directivity: TDirectivity;
    begin
        for directivity in directivities do
            if directivity.f = f then
                exit(directivity.directivity.toDoubleArray);
        setlength(result,2);
        result[0] := 0;
        result[1] := 0;
    end;
var
    fi, i: integer;
    inThetaV: TDoubleDynArray;
    thisDirectivity: TDoubleDynArray;
begin
    setlength(result,length(f));
    for fi := 0 to length(f) - 1 do
    begin
        //interpolate the thisDirectivity to our angle range
        thisDirectivity := getDirectivityAtFreq(f[fi],directivities);

        setlength(inThetaV,length(thisDirectivity));
        for i := 0 to length(thisDirectivity) - 1 do
            inThetaV[i] := -pi/2 + pi * i / (length(thisDirectivity) - 1); //-90 deg to +90 deg

        result[fi] := TSolversGeneralFunctions.myInterp(theta,inThetaV,thisDirectivity).toSingleArray;
    end;
end;

class function TSolversGeneralFunctions.lineSegmentPassesPoint2D(const px, py, x1, y1, x2, y2: double): boolean;
var
    SqLineMag,              // square of line's magnitude
    u:double;         // see Paul Bourke's original article(s)
begin
    SqLineMag := SqLineMagnitude(x1, y1, x2, y2);
    if SqLineMag < EPSEPS then
        exit(false);

    u := ( (px - x1)*(x2 - x1) + (py - y1)*(y2 - y1) ) / SqLineMag;
    result := not ((u < EPS) or (u > 1));
end;


class function TSolversGeneralFunctions.distPointToLineSegment3D(const px,py,pz, ax,ay,az, bx,by,bz: double): double;
var
    v, w, bp{, pMinusPb}: TPoint3D;
    c1, c2, b: double;
begin
    //http://geomalgorithms.com/a02-_lines.html
    //with the line from a to b, and the point p

    v.x := bx - ax; //the line segment
    v.y := by - ay;
    v.z := bz - az;

    w.x := px - ax; //vector to a, start of line segment
    w.y := py - ay;
    w.z := pz - az;

    c1 := w.DotProduct(v);
    if (c1 <= 0.0) then  // p behind start of v
        exit(w.length()); // distance a to b

    bp.x := px - bx; //vector to b, end of line segment
    bp.y := py - by;
    bp.z := pz - bz;

    c2 := v.DotProduct(v); //p after end of v
    if ( c2 <= c1 ) then
        exit(bp.Length()); //distance p to b

    b := c1 / c2; //distance fraction along line segment of projected point Pb
    //Pb is the perpedicular projection of p on v
    //pMinusPb.x := px - (ax + b * v.x);
    //pMinusPb.y := py - (ay + b * v.y);
    //pMinusPb.z := pz - (az + b * v.z);
    //result := pMinusPb.length();
    result := sqrt(sqr(px - (ax + b * v.x)) + sqr(py - (ay + b * v.y)) + sqr(pz - (az + b * v.z)));
end;

class function TSolversGeneralFunctions.distPointToLineSegment(const px, py, x1, y1, x2, y2: double ): double;
var
    SqLineMag,              // square of line's magnitude
    u,                      // see Paul Bourke's original article(s)
    ix,                     // intersecting point X
    iy:     TExtendedx87;   // intersecting point Y
begin
    SqLineMag := SqLineMagnitude(x1, y1, x2, y2);
    if SqLineMag < EPSEPS then
    begin
        result := -1.0;
        exit;
    end;

    u := ( (px - x1)*(x2 - x1) + (py - y1)*(y2 - y1) ) / SqLineMag;

    if (u < EPS) or (u > 1) then
    begin
        //  Closest point does not fall within the line segment,
        //    take the shorter distance to an endpoint
        ix := SqLineMagnitude(px, py, x1, y1);
        iy := SqLineMagnitude(px, py, x2, y2);
        result := min(ix, iy);
    end //  if (u < EPS) or (u > 1)
    else
    begin
        //  Intersecting point is on the line, use the formula
        ix := x1 + u * (x2 - x1);
        iy := y1 + u * (y2 - y1);
        result := SqlineMagnitude(px, py, ix, iy);
    end; //  else NOT (u < EPS) or (u > 1)

    // finally convert to actual distance not its square
    result := system.sqrt(result);
end;


class function TSolversGeneralFunctions.addIterator(const x, dx: TRayIterator2; const ds: double): TRayIterator2;
begin
    result.r := x.r + dx.r * ds;
    result.z := x.z + dx.z * ds;
    result.zeta := x.zeta + dx.zeta * ds;
    result.xi := x.xi + dx.xi * ds;
    result.q := x.q + dx.q * ds;
    result.p := x.p + dx.p * ds;
    result.terminated := x.terminated;
end;

class function TSolversGeneralFunctions.rayf2(const x: TRayIterator2; const c, dc_dz, d2c_dz2, prev_c, prev_dc_dz, prevR, prev_dc_dr: double; var dc_dr_return: double): TRayIterator2;
var
    c2, cnn, dr, d2c_dr2, d2c_drdz, dc_dr: double;
begin
    c2 := system.sqr(c);
    dr := x.r - prevR;

    if dr = 0 then
    begin
        dc_dr := 0;
        d2c_dr2 := 0;
        d2c_drdz := 0;
    end
    else
    begin
        dc_dr := (c - prev_c) / dr;
        d2c_dr2 := (dc_dr - prev_dc_dr) / dr;
        d2c_drdz := (dc_dz - prev_dc_dz) / dr;
    end;

    cnn := (d2c_dr2 * system.sqr(x.zeta) +
        d2c_drdz * x.zeta * x.xi +
        d2c_dz2*system.sqr(x.xi)); //eq 3.62 (range dependent sound speed) NB c^2 factor removed
    // d2f_dxdy = f(i+1,j+1) - f(i,j+1) - f(i+1,j) + f(i,j) / (delx*dely)

    result.r := c*x.xi; //dr/ds := c*xi  //eq 3.23
    result.z := c*x.zeta; //dz/ds := c*zeta     //eq 3.24
    result.zeta := -dc_dz/c2;   //dzeta/ds := -1/c^2*(dc/dz)   //eq 3.24
    result.xi := -dc_dr/c2; //dxi/ds := -1/c^2*(dc/dr) //eq 3.23 //if no range dependent sound speed, dxi/ds = 0
    result.q := c*x.p; //dq/ds := c*p      //eq 3.58
    result.p := -cnn*x.q; //dp/ds := -cnn/c2*q  //eq 3.58  NB 1/c^2 factor removed -> see cnn calc
    result.terminated := x.terminated;

    dc_dr_return := dc_dr;
end;

class function TSolversGeneralFunctions.rayf3D(const x: TRayIterator3D; const c, dcdz, d2cdz2: double): TRayIterator3D;
var
    c2, cnn, c2inv: double;
begin
    c2 := system.sqr(c);
    c2inv := 1/c2;
    cnn := c2*(d2cdz2*system.sqr(x.xi)); //full expression includes other terms with dc/dr which is always 0 for our problems. //eq 3.62

    result.x := c*x.xi; //eq 3.262
    result.y := c*x.eta; //3.263
    result.z := c*x.zeta; //3.264
    result.xi := 0; // dc/dx = 0  3.262  //todo range dependent
    result.eta := 0; // dc/dy = 0  3.263
    result.zeta := -dcdz*c2inv;   //dzeta/ds := -1/c^2*(dc/dz)   //eq 3.264
    result.q := c*x.p; //dq/ds := c*p      //eq 3.58
    result.p := -cnn*c2inv*x.q; //dp/ds := -cnn/c2*q  //eq 3.58
    result.terminated := x.terminated;

    //TODO update for dc/dx <> 0
end;

class function TSolversGeneralFunctions.rk4Step3D(const x0: TRayIterator3D; const ds, c, dc_dz, d2c_dz2: double): TRayIterator3D;
const
    RKConsts: array[1..4] of double = (0.16666667,0.33333333,0.33333333,0.16666667);
var
    k1,k2,k3,k4,tmp1,tmp2,tmp3: TRayIterator3d;
begin
    k1 := TSolversGeneralFunctions.rayf3D(x0  ,c,dc_dz,d2c_dz2);
    k1.multiplyBy(ds);
    tmp1 := TSolversGeneralFunctions.addIterator3D(x0,k1,1/2);

    k2 := TSolversGeneralFunctions.rayf3D(tmp1,c,dc_dz,d2c_dz2);
    k2.multiplyBy(ds);
    tmp2 := TSolversGeneralFunctions.addIterator3D(x0,k2,1/2);

    k3 := TSolversGeneralFunctions.rayf3D(tmp2,c,dc_dz,d2c_dz2);
    k3.multiplyBy(ds);
    tmp3 := TSolversGeneralFunctions.addIterator3D(x0,k3,1);

    k4 := TSolversGeneralFunctions.rayf3D(tmp3,c,dc_dz,d2c_dz2);
    k4.multiplyBy(ds);

    result.x    := x0.x    + RKConsts[1]*k1.x    + RKConsts[2]*k2.x    + RKConsts[3]*k3.x    + RKConsts[4]*k4.x;
    result.y    := x0.y    + RKConsts[1]*k1.y    + RKConsts[2]*k2.y    + RKConsts[3]*k3.y    + RKConsts[4]*k4.y;
    result.z    := x0.z    + RKConsts[1]*k1.z    + RKConsts[2]*k2.z    + RKConsts[3]*k3.z    + RKConsts[4]*k4.z;
    result.xi   := x0.xi   + RKConsts[1]*k1.xi   + RKConsts[2]*k2.xi   + RKConsts[3]*k3.xi   + RKConsts[4]*k4.xi;
    result.eta  := x0.eta  + RKConsts[1]*k1.eta  + RKConsts[2]*k2.eta  + RKConsts[3]*k3.eta  + RKConsts[4]*k4.eta;
    result.zeta := x0.zeta + RKConsts[1]*k1.zeta + RKConsts[2]*k2.zeta + RKConsts[3]*k3.zeta + RKConsts[4]*k4.zeta;
    result.q    := x0.q    + RKConsts[1]*k1.q    + RKConsts[2]*k2.q    + RKConsts[3]*k3.q    + RKConsts[4]*k4.q;
    result.p    := x0.p    + RKConsts[1]*k1.p    + RKConsts[2]*k2.p    + RKConsts[3]*k3.p    + RKConsts[4]*k4.p;
    result.terminated := x0.terminated;
end;

class function TSolversGeneralFunctions.rk2Step3D(const x0: TRayIterator3D; const ds, c, dc_dz, d2c_dz2: double): TRayIterator3D;
var
    k1,k2,tmp: TRayIterator3D;
begin
    k1 := TSolversGeneralFunctions.rayf3D(x0,c,dc_dz,d2c_dz2);
    tmp := TSolversGeneralFunctions.addIterator3D(x0,k1,ds/2);

    k2 := TSolversGeneralFunctions.rayf3D(tmp,c,dc_dz,d2c_dz2);
    result := TSolversGeneralFunctions.addIterator3D(x0,k2,ds);
end;

class function TSolversGeneralFunctions.rk2StepMCK(const x0: TRayIterator2; const ds, prevR, c, dc_dz, d2c_dz2, prev_c, prev_dc_dz: double; var prev_dc_dr: double): TRayIterator2;
var
    tmp_dc_dr, dummy: double;
    k1,k2,tmp: TRayIterator2;
begin
    k1 := TSolversGeneralFunctions.rayf2(x0,c,dc_dz,d2c_dz2,prev_c, prev_dc_dz, prevR, prev_dc_dr, tmp_dc_dr);
    tmp := TSolversGeneralFunctions.addIterator(x0,k1,ds/2);

    k2 := TSolversGeneralFunctions.rayf2(tmp,c,dc_dz,d2c_dz2,prev_c, prev_dc_dz, prevR, prev_dc_dr, dummy);
    result := TSolversGeneralFunctions.addIterator(x0,k2,ds);

    prev_dc_dr := tmp_dc_dr;
end;

class function TSolversGeneralFunctions.rk4StepMCK(const x0: TRayIterator2; const ds, prevR, c, dc_dz, d2c_dz2, prev_c, prev_dc_dz: double; var prev_dc_dr: double): TRayIterator2;
const
    RKConsts: array[1..4] of double = (0.16666667,0.33333333,0.33333333,0.16666667);
var
    tmp_dc_dr, dummy: double;
    k1,k2,k3,k4,tmp1,tmp2,tmp3: TRayIterator2;
begin
    k1 := TSolversGeneralFunctions.rayf2(x0  ,c,dc_dz,d2c_dz2,prev_c, prev_dc_dz, prevR, prev_dc_dr, tmp_dc_dr);
    k1.multiplyBy(ds);
    tmp1 := TSolversGeneralFunctions.addIterator(x0,k1,1/2);

    k2 := TSolversGeneralFunctions.rayf2(tmp1,c,dc_dz,d2c_dz2,prev_c, prev_dc_dz, prevR, prev_dc_dr, dummy);
    k2.multiplyBy(ds);
    tmp2 := TSolversGeneralFunctions.addIterator(x0,k2,1/2);

    k3 := TSolversGeneralFunctions.rayf2(tmp2,c,dc_dz,d2c_dz2,prev_c, prev_dc_dz, prevR, prev_dc_dr, dummy);
    k3.multiplyBy(ds);
    tmp3 := TSolversGeneralFunctions.addIterator(x0,k3,1);

    k4 := TSolversGeneralFunctions.rayf2(tmp3,c,dc_dz,d2c_dz2,prev_c, prev_dc_dz, prevR, prev_dc_dr, dummy);
    k4.multiplyBy(ds);

    result.r    := x0.r    + RKConsts[1]*k1.r    + RKConsts[2]*k2.r    + RKConsts[3]*k3.r    + RKConsts[4]*k4.r;
    result.z    := x0.z    + RKConsts[1]*k1.z    + RKConsts[2]*k2.z    + RKConsts[3]*k3.z    + RKConsts[4]*k4.z;
    result.zeta := x0.zeta + RKConsts[1]*k1.zeta + RKConsts[2]*k2.zeta + RKConsts[3]*k3.zeta + RKConsts[4]*k4.zeta;
    result.xi   := x0.xi   + RKConsts[1]*k1.xi   + RKConsts[2]*k2.xi   + RKConsts[3]*k3.xi   + RKConsts[4]*k4.xi;
    result.q    := x0.q    + RKConsts[1]*k1.q    + RKConsts[2]*k2.q    + RKConsts[3]*k3.q    + RKConsts[4]*k4.q;
    result.p    := x0.p    + RKConsts[1]*k1.p    + RKConsts[2]*k2.p    + RKConsts[3]*k3.p    + RKConsts[4]*k4.p;
    result.terminated := x0.terminated;

    prev_dc_dr := tmp_dc_dr;
end;

class function  TSolversGeneralFunctions.getdepth(const zProfile: TRangeValArray; const r: double): TGetDepthRec;
var
    i, len : integer;
    thisdz,thisdr : double;
begin
    len := length(zProfile) - 1;
    if r <= 0 then
    begin
        i := 0;
        result.z := zProfile[i].val;
        result.ang := 0;
        result.m := 0;
    end
    else
    if r >= zProfile[len].range then
    begin
        i := len;
        result.z := zProfile[i].val;
        result.ang := 0;
        result.m := 0;
    end
    else
    begin
        i := safeceil(r/(zProfile[1].range - zProfile[0].range));

        if (i = 0) or (i >= len) then
        begin
            result.z := zProfile[i].val;
            result.ang := 0;
            result.m := 0;
        end
        else
        begin
            i := i - 1; //get the point before the interp point
            result.z := zProfile[i].val + (zProfile[i + 1].val - zProfile[i].val) *
                (r - zProfile[i].range)/(zProfile[i + 1].range - zProfile[i].range);
            thisdz := zProfile[i + 1].val - zProfile[i].val;
            thisdr := zProfile[i + 1].range - zProfile[i].range;
            result.ang := arctan2(thisdz,thisdr);
            result.m := thisdz/thisdr; //y := mx + c
        end;
    end;
    result.c := zProfile[i].val - result.m*zProfile[i].range; //y := mx + c
end;

class function TSolversGeneralFunctions.rayHitSeafloor3D(const depths: TSingleMatrix; const deltax, deltay, c, stepSize: double; var xnext: TRayIterator3D; const xprev: TRayIterator3D; out xp: TGetDepthRec3D): boolean;
type
    TzTest = (deeperThanAllPoints, shallowerThanAllPoints, inRange);
var
    ix, iy, ny: integer;
    upperTriangle: boolean;
    intersection: TPoint3D;
    distToIntersection: double;
    pz0,pz1,pz2: double;
    rayNormal, planeNormal: TPoint3D;
    outNormal, outpos: TPoint3D;
    zTestNext, zTestPrev: TzTest;
    theta, phi, cosTheta, sinTheta, sinPhi, cosPhi: double;

    //{$DEFINE HEADINGTEST}
    {$IF defined (DEBUG) and defined (HEADINGTEST)}
    phiIn: double;
    {$ENDIF}

    function inZRange(const z: double): TzTest;
    begin
        if (z > pz0) and (z > pz1) and (z > pz2) then
            exit(deeperThanAllPoints)
        else if (z < pz0) and (z < pz1) and (z < pz2) then
            exit(shallowerThanAllPoints);

        result := inRange;
    end;
begin
    //todo
    ny := length(depths);
    ix := floor((xnext.x + xprev.x) / 2 / deltax);  //NB this is an issue, if xnext and xprev don't fall in the same cell
    iy := floor((xnext.y + xprev.y) / 2 / deltay); //REVERSED!!!

    xp.outOfBounds := (iy <= 0) or (iy > length(depths) - 1) or (ix < 0) or (ix >= length(depths[0]) - 1);
    if xp.outOfBounds then
    begin
        xp.z := 0;
        exit(false);
    end;

    upperTriangle := xnext.x/deltax - ix > xnext.y/deltay - iy; //which triangle part of depth cell the ray is in
    if upperTriangle then
    begin
        pz0 := depths[ny - 1 - iy  ,ix];
        pz1 := depths[ny - 1 - iy  ,ix+1];
        pz2 := depths[ny - 1 - iy+1,ix+1];

        //todo careful with normal direction
        planeNormal := calcPlaneNormal(ix*deltax,(ix+1)*deltax,(ix+1)*deltax,
                iy*deltay,iy*deltay,(iy+1)*deltay,
                pz0,pz1,pz2);
    end
    else
    begin
        pz0 := depths[ny - 1 - iy  ,ix];
        pz1 := depths[ny - 1 - iy+1,ix+1];
        pz2 := depths[ny - 1 - iy+1,ix];

        planeNormal := calcPlaneNormal(ix*deltax,(ix+1)*deltax,ix*deltax,
                iy*deltay,(iy+1)*deltay,(iy+1)*deltay,
                pz0,pz1,pz2);
    end;
    if planeNormal.z < 0 then
        planeNormal := -planeNormal;

    zTestNext := inZRange(xnext.z);
    if zTestNext = shallowerThanAllPoints then
    begin
        xp.z := (pz0 + pz1 + pz2) / 3;
        exit(false);
    end;

    zTestPrev := inZRange(xprev.z);
    if (zTestNext = deeperThanAllPoints) and (zTestPrev = deeperThanAllPoints) then
    begin
        //both start and end are below seafloor
        xp.z := (pz0 + pz1 + pz2) / 3;
        exit(true);
    end;

    rayNormal.x := xnext.x - xprev.x;
    rayNormal.y := xnext.y - xprev.y;
    rayNormal.z := xnext.z - xprev.z;
    rayNormal := rayNormal.Normalize;

    result := rayIntersectsPlane(xprev,
        rayNormal, planeNormal,
        ix*deltax,iy*deltay,pz0,
        intersection);
    xp.z := intersection.Z;

    if not result then
    begin
        xp.z := (pz0 + pz1 + pz2) / 3;
        exit;
    end;

    outNormal := -2*(rayNormal.DotProduct(planeNormal))*planeNormal + rayNormal;
    outNormal := outNormal.Normalize;

    if zTestPrev = deeperThanAllPoints then
        outpos := intersection
    else
    begin
        distToIntersection := sqrt(sqr(xprev.x - intersection.x) + sqr(xprev.y - intersection.y) + sqr(xprev.z - intersection.z));
        if stepSize < distToIntersection then exit(false);
        outpos := intersection + outNormal*(stepSize - distToIntersection);
    end;

    xp.theta1 := pi/2 - rayNormal.DotProduct(planeNormal); //todo check signs

    xnext.x := outpos.x;
    xnext.y := outpos.y;
    xnext.z := outpos.z;

    phi := ArcTan2(outNormal.y, outNormal.x);
    theta := ArcTan2(outNormal.z, hypot(outNormal.x, outNormal.y));

    {$IF defined (DEBUG) and defined (HEADINGTEST)}
    phiIn := arctan2(rayNormal.y, rayNormal.x);
    if phi < phiIn then
        if phiIn = 432943289 then
            phi := 0;

    //inc(xp.count);
    {$ENDIF}

    system.SineCosine(theta, sinTheta, cosTheta);
    system.SineCosine(phi, sinPhi, cosPhi);
    xnext.xi := cosTheta*cosPhi/c;   //3.266
    xnext.eta := cosTheta*sinPhi/c;  //3.266
    xnext.zeta := sinTheta/c;           //3.266
end;

class function TSolversGeneralFunctions.rayIntersectsPlane(const ray: TRayIterator3D; const rayNormal, planeNormal: TPoint3d; const px0, py0, pz0: double; var intersection: TPoint3d): boolean;
var
    RayPos, PlanePoint: TPoint3D;
begin
    RayPos.x := ray.x;
    RayPos.y := ray.y;
    RayPos.z := ray.z;

//    RayDir.x := c*ray.xi;  //3.262
//    RayDir.y := c*ray.eta;  //3.263
//    RayDir.z := c*ray.zeta; //3.264

    PlanePoint.x := px0;
    PlanePoint.y := py0;
    PlanePoint.z := pz0;

    //PlaneNormal := calcPlaneNormal(px0, px1, px2, py0, py1, py2, pz0, pz1, pz2);

    result := fmx.types3d.RayCastPlaneIntersect(RayPos, RayNormal, PlanePoint, PlaneNormal, Intersection);
end;

class function TSolversGeneralFunctions.resample(const v: TDoubleDynArray; const n: integer): TDoubleDynArray;
var
    d: double;
    i, ind, n0: integer;
begin
    n0 := length(v);
    setlength(result,n);
    for i := 0 to n - 1 do
    begin
        d := i / n * n0;
        ind := floor(d);
        if ind = n0 - 1 then
            result[i] := v[n0 - 1]
        else
            result[i] := v[ind] + (v[ind + 1] - v[ind]) * (d - ind);
    end;
end;

class function TSolversGeneralFunctions.calcPlaneNormal(const x0, x1, x2, y0, y1, y2, z0, z1, z2: double): TPoint3d;
begin
    result.x := (y1 - y0)*(z2 - z0) - (z1 - z0)*(y2 - y0);
    result.y := (z1 - z0)*(x2 - x0) - (x1 - x0)*(z2 - z0);
    result.z := (x1 - x0)*(y2 - y0) - (y1 - y0)*(x2 - x0);
    result := result.Normalize;
end;

class function TSolversGeneralFunctions.TDMAsolver(const a,b,c0,d0 : TDoubleDynArray) : TDoubleDynArray;
var
    n,i : integer;
    tmp : double;
    c,d : TDoubleDynArray; //internal use only, to avoid side effects
    bGoNegative, bSignSet : boolean;
    xmax : double;
begin
    {NB inverse of diagonal matrix just has the inverse of each of the diagonal terms}

    {for solving Ax = d
    a (lower), b (diagonal), c (upper) are the column vectors for the compressed tridiagonal matrix A, d is the right hand side vector}
    n := length(d0); //% n is the number of rows

    {make internal copies so as not to change the input vectors}
    c := c0.copy;
    d := d0.copy;

    {Modify the first-row coefficients}
    c[0] := c[0] / b[0];  //  % Division by zero risk.
    d[0] := d[0] / b[0];

    for i := 1 to n - 2 do
    begin
        tmp := b[i] - a[i]*c[i-1];
        c[i] := c[i] / tmp;
        d[i] := (d[i] - a[i]*d[i-1]) / tmp;
    end;

    d[n-1] := (d[n-1] - a[n-1]*d[n-2]) / (b[n-1] - a[n-1]*c[n-2]);

    {back substitute to get result}
    setlength(result,n);
    result[n-1] := d[n-1];
    xmax := abs(d[n-1]);
    for i := n - 2 downto 0 do
    begin
        result[i] := d[i] - c[i] * result[i + 1];
        if abs(result[i]) > xmax then
            xmax := abs(result[i]);
    end;

    {make sure all the modes go positive in the first 1/2 cycle below the surface}
    xmax := xmax*1e-3;
    bSignSet := false;
    bGoNegative := false;
    for i := 0 to (n div 2) - 1 do
        if (not bSignSet) and (abs(result[i]) > xmax) then
        begin
            bSignSet := true;
            if result[i] < 0 then
                bGoNegative := true;
        end;

    if bGoNegative then
        for i := 0 to n - 1 do
            result[i] := -result[i];
end;


class function TSolversGeneralFunctions.my2norm(const x, rho: TDoubleDynArray): double;
var
    q : double;
    i : integer;
    hx : integer;
begin
    {return the 2-norm of x}
    hx := high(x);
    q := 0;
    for i := 0 to hx do
        q := q + system.sqr(x[i])/rho[i];
    //q := x[0]*x[0]/rho[0]/2;
    //i := 1 to hx - 1 do
    //    q := q + x[i]*x[i]/rho[i];
    //q := q + x[hx]*x[hx]/rho[hx]/2;
    result := system.sqrt(q);
end;


class function TSolversGeneralFunctions.psiNormalise(const x, rhoz: TDoubleDynArray): TDoubleDynArray;
var
    r: double;
    i: integer;
begin
    {normalise by the 2-norm}
    r := my2norm(x,rhoz);
    setlength(result,length(x));
    for i := 0 to length(x) - 1 do
        result[i] := x[i]/r;
end;

class procedure TSolversGeneralFunctions.psiNormaliseInPlace(const x, rhoz, result: TDoubleDynArray);
var
    r: double;
    i: integer;
begin
    {normalise by the 2-norm}
    r := my2norm(x,rhoz);
    for i := 0 to length(x) - 1 do
        result[i] := x[i]/r;
end;

class function TSolversGeneralFunctions.tridDeterminant(const a,b,c: TDoubleDynArray; const lambda: double): TExtendedX87;
var
    i: integer;
    f0, f1: TExtendedX87;
begin
    result := 0;
    f0 := b[0] - lambda;
    f1 := (b[1] - lambda)*f0 - a[1]*c[0];
    for i := 2 to length(b) - 1 do
    begin
        result := (b[i] - lambda)*f1 - a[i]*c[i-1]*f0;
        f0 := f1;
        f1 := result;
    end;
end;

class function TSolversGeneralFunctions.modalInterpolationFn(const rd: TRangeValArray; const range: double) : double;
var
    ind: integer;
begin
    ind := rd.findRange(range);
    if ind < 0 then
        result := rd[0].val
    else if (ind = (length(rd) - 1)) or (rd[ind + 1].range = rd[ind].range) then
        result := rd[ind].val
    else
        result := rd[ind].val + (range             - rd[ind].range) /
                              (rd[ind + 1].range - rd[ind].range) *
                              (rd[ind + 1].val - rd[ind].val);
end;

class function TSolversGeneralFunctions.modalInterpolationFn(const dv: TDepthValArray; const d: double): double;
var
    ind: integer;
begin
    {this will also work for d < min(aM)}
    ind := dv.findDepth(d);
    if ind < 0 then
        result := dv[0].val
    else if (ind = (length(dv) - 1)) or (dv[ind + 1].depth = dv[ind].depth) then
        result := dv[ind].val
    else
        result := dv[ind].val + (d             - dv[ind].depth) /
                              (dv[ind + 1].depth - dv[ind].depth) *
                              (dv[ind + 1].val - dv[ind].val);
end;

class function TSolversGeneralFunctions.modalInterpolationFn(const aM: TMatrix; const d: double): double;
var
    ind: integer;
begin
    {this will also work for d < min(aM)}
    ind := aM.column(0).find(d);
    if ind < 0 then
        result := aM[1,0]
    else if (ind = (length(aM[0]) - 1)){where d > max(aM)} or (aM[0,ind + 1] = aM[0,ind]) then
        result := aM[1,ind]
    else
        result := aM[1,ind] + (d             - aM[0,ind]) /
                              (aM[0,ind + 1] - aM[0,ind]) *
                              (aM[1,ind + 1] - aM[1,ind]);
end;

class function TSolversGeneralFunctions.tridEigs(const a,b,c: TDoubleDynArray; const nlam,nlam2: integer; const upperlimit: double; const maxModes: integer): TDoubleDynArray;
    function mySign(const val: TExtendedX87): boolean;
    begin
        result := val > 0;
    end;
const
    incResultLengthBy = 10;
var
    i, i2, lc: integer;
    dlam, dlam2: TExtendedX87;
    lamdaPrev, lamdaThis: TExtendedX87;
    lamda2Prev, lamda2This: TExtendedX87;
    detPrev,detThis: TExtendedX87; //determinants
    det2Prev,det2This: TExtendedX87; //determinants
begin
    {search along the real axis for determinant=0 of the sparse matrix given by the 3 column vectors a b c.
    first step along the axis in nlam steps from upperlimit down to 0. then use a secant
    stepping method to get better approx to the roots, for all the 0 crossings identified.}

    dlam := -upperlimit/(nlam - 1); //STEP BACKWARDS

    lc := 0;
    setlength(result,incResultLengthBy);
    {$IFDEF CPUX86}
    detPrev := 0;
    lamdaPrev := 0;
    {$ENDIF}
    for i := 0 to nlam - 1 do
        if (maxModes = 0) or (lc < maxModes) then //only look for more modes if we want all modes (0) or we have less than the target number
        begin
            lamdaThis := upperlimit + i*dlam;
            detThis := TSolversGeneralFunctions.tridDeterminant(a,b,c,lamdaThis);

            if (i > 0) and (mySign(detThis) <> mySign(detPrev)) then  //a zero crossing was encountered
            //nb this will miss if 2 crossings are very close
            begin

                {2nd round refine - NB lamdas is reversed, and we also want to step backwards for lamdas2}
                dlam2 := (lamdaThis - lamdaPrev)/(nlam2 - 1);
                {$IFDEF CPUX86}
                //x86 compiler isn't smart enough to realise these aren't called first time
                det2Prev := 0;
                lamda2Prev := 0;
                {$ENDIF}
                for i2 := 0 to nlam2 - 1 do
                begin
                    lamda2This := lamdaPrev + i2*dlam2;
                    det2This := TSolversGeneralFunctions.tridDeterminant(a,b,c,lamda2This);
                    if (i2 > 0) and (mySign(det2This) <> mySign(det2Prev)) then
                    begin
                        inc(lc);
                        if lc > length(result) then
                            setlength(result,lc + incResultLengthBy);
                        if isnan(lamda2Prev) then
                            result[lc - 1] := 0
                        else
                            result[lc - 1] := lamda2Prev;
                        //break;
                    end; //sign mismatch

                    lamda2Prev := lamda2This;
                    det2Prev := det2This;
                end; //i2

            end; //sign mismatch

            lamdaPrev := lamdaThis;
            detPrev := detThis;
        end; //i
    setlength(result,lc);
end;

class function TSolversGeneralFunctions.TDMASolverComplex(const C: TTriDiagonal; const d0: TComplexVect): TComplexVect;
var
    cu,d : TComplexVect;
    n,i : integer;
    tmp : TComplex;
begin
    {c and d are modified by the algorithm, so make local versions}

    setlength(cu,length(c));
    for i := 0 to length(c) - 1 do
        cu[i] := c.upper[i];

    setlength(d,length(d0));
    for i := 0 to length(d0) - 1 do
        d[i] := d0[i];

    {for solving Ax = d
    a (lower), b (diagonal), c (upper) are the column vectors for the compressed tridiagonal matrix A, d is the right hand side vector}
    n := length(d); //% n is the number of rows

    {Modify the first-row coefficients}
    cu[0] := cu[0] / c.diag[0];  //  % Division by zero risk.
    d[0] := d[0] / c.diag[0];

    for i := 1 to n - 2 do
    begin
        tmp := c.diag[i] - c.lower[i] * cu[i-1];
        cu[i] := cu[i] / tmp;
        d[i] := (d[i] - c.lower[i] * d[i-1]) / tmp;
    end;

    d[n-1] := (d[n-1] - c.lower[n-1]*d[n-2]) / (c.diag[n-1] - c.lower[n-1]*cu[n-2]);

    //back substitute.
    setlength(result,n);
    result[n-1] := d[n-1];
    for i := n - 2 downto 0 do
        result[i] := d[i] - cu[i]*result[i + 1];
end;

class procedure TSolversGeneralFunctions.TDMASolverComplexInPlace(const C: TTriDiagonal; const rhs,tmpUpper,tmpRhs,result: TComplexVect);
var
    n,i: integer;
    tmp: TComplex;
begin
    n := length(tmpRhs); //% n is the number of rows

    //tmpc and tmpd are modified by the algorithm, but are just copies of c0 and d0
    for i := 0 to n - 1 do
        tmpUpper[i] := C.upper[i];
    for i := 0 to n - 1 do
        tmpRhs[i] := rhs[i];

    //Modify the first-row coefficients
    tmpUpper[0] := tmpUpper[0] / C.diag[0];  //  % Division by zero risk.
    tmpRhs[0] := tmpRhs[0] / C.diag[0];

    for i := 1 to n - 2 do
    begin
        tmp := C.diag[i] - C.lower[i] * tmpUpper[i-1];
        tmpUpper[i] := tmpUpper[i] / tmp;
        tmpRhs[i] := (tmpRhs[i] - C.lower[i] * tmpRhs[i-1]) / tmp;
    end;

    tmpRhs[n-1] := (tmpRhs[n-1] - C.lower[n-1]*tmpRhs[n-2]) / (C.diag[n-1] - C.lower[n-1]*tmpUpper[n-2]);

    //back substitute
    result[n-1] := tmpRhs[n-1];
    for i := n - 2 downto 0 do
        result[i] := tmpRhs[i] - tmpUpper[i]*result[i + 1];
end;

class procedure TSolversGeneralFunctions.TDMASolverComplexInPlaceWrap(
    const C, CShort: TTriDiagonal;
    const rhs,tmpUpper,tmpRhs,xone,xtwo,
        rhsShort,firstAndLastOnlyRhsShort,
        tmpUpperShort,tmpRhsShort,result: TComplexVect);
var
    n,i: integer;
begin
    //wrapping version, for use with theta calcs. http://www.cfm.brown.edu/people/gk/chap6/node14.html
    n := length(C); //% n is the number of rows

    for I := 0 to n - 2 do
        rhsShort[i] := rhs[i];
    //NB -ve for firstAndLastOnlyRhsShort
    firstAndLastOnlyRhsShort[0] := -C.lower[0];
    firstAndLastOnlyRhsShort[n - 2] := -C.upper[n - 2];

    TDMASolverComplexInPlace(CShort,rhsShort,tmpUpperShort,tmpRhsShort,xone);
    TDMASolverComplexInPlace(CShort,firstAndLastOnlyRhsShort,tmpUpperShort,tmpRhsShort,xtwo);

    result[n - 1] := (rhs[n-1] - C.upper[n-1]*xone[0] - C.lower[n-1]*xone[n-2]) /
        (C.diag[n-1] + C.upper[n-1]*xtwo[0] + C.lower[n-1]*xtwo[n-2]);
    for I := 0 to n - 2 do
        result[i] := xone[i] + xtwo[i]*result[n - 1];
end;

class procedure TSolversGeneralFunctions.TDMAsolverInPlace(const a, b, c0, d0, result: TDoubleDynArray);
var
    n,i : integer;
    tmp : double;
    c,d : TDoubleDynArray; //internal use only, to avoid side effects
    bGoNegative, bSignSet : boolean;
    xmax : double;
begin
    {NB inverse of diagonal matrix just has the inverse of each of the diagonal terms}

    {for solving Ax = d
    a (lower), b (diagonal), c (upper) are the column vectors for the compressed tridiagonal matrix A, d is the right hand side vector}
    n := length(d0); //% n is the number of rows

    {make internal copies so as not to change the input vectors}
    c := c0.copy;
    d := d0.copy;

    {Modify the first-row coefficients}
    c[0] := c[0] / b[0];  //  % Division by zero risk.
    d[0] := d[0] / b[0];

    for i := 1 to n - 2 do
    begin
        tmp := b[i] - a[i]*c[i-1];
        c[i] := c[i] / tmp;
        d[i] := (d[i] - a[i]*d[i-1]) / tmp;
    end;

    d[n-1] := (d[n-1] - a[n-1]*d[n-2]) / (b[n-1] - a[n-1]*c[n-2]);

    {back substitute to get result}
    result[n-1] := d[n-1];
    xmax := abs(d[n-1]);
    for i := n - 2 downto 0 do
    begin
        result[i] := d[i] - c[i] * result[i + 1];
        if abs(result[i]) > xmax then
            xmax := abs(result[i]);
    end;

    {make sure all the modes go positive in the first 1/2 cycle below the surface}
    xmax := xmax*1e-3;
    bSignSet := false;
    bGoNegative := false;
    for i := 0 to (n div 2) - 1 do
        if (not bSignSet) and (abs(result[i]) > xmax) then
        begin
            bSignSet := true;
            if result[i] < 0 then
                bGoNegative := true;
        end;

    if bGoNegative then
        for i := 0 to n - 1 do
            result[i] := -result[i];
end;

//class procedure TSolversGeneralFunctions.TDMASolverBiComplexInPlace(const C: TQuadTriDiagonal; const tmpUpper: TQuadComplexArray; const rhs, tmpRhs, result: TBiComplexArray);
//var
//    n,i: integer;
//    tmp: TQuadComplex;
//begin
//    n := length(tmpRhs); //% n is the number of rows
//
//    //tmpc and tmpd are modified by the algorithm, but are just copies of c0 and d0
//    for i := 0 to n - 1 do
//        tmpUpper[i] := C.upper[i];
//
//    for i := 0 to n - 1 do
//        tmpRhs[i] := rhs[i];
//
//    //Modify the first-row coefficients
//    tmpUpper[0] := tmpUpper[0] / C.diag[0];  //  % Division by zero risk.
//    tmpRhs[0] := tmpRhs[0] / C.diag[0];
//
//    for i := 1 to n - 2 do
//    begin
//        tmp := C.diag[i] - C.lower[i] * tmpUpper[i-1];
//        tmpUpper[i] := tmpUpper[i] / tmp;
//        tmpRhs[i] := (tmpRhs[i] - C.lower[i] * tmpRhs[i-1]) / tmp;
//    end;
//
//    tmpRhs[n-1] := (tmpRhs[n-1] - C.lower[n-1]*tmpRhs[n-2]) / (C.diag[n-1] - C.lower[n-1]*tmpUpper[n-2]);
//
//    //back substitute
//    result[n-1] := tmpRhs[n-1];
//    for i := n - 2 downto 0 do
//        result[i] := tmpRhs[i] - tmpUpper[i]*result[i + 1];
//end;

class function TSolversGeneralFunctions.TDMASolverComplex(const a,b,c0,d0: TComplexVect): TComplexVect;
var
    c,d : TComplexVect;
    n,i : integer;
    tmp : TComplex;
begin
    {c and d are modified by the algorithm, so make local versions}

    setlength(c,length(c0));
    for i := 0 to length(c0) - 1 do
        c[i] := c0[i];

    setlength(d,length(d0));
    for i := 0 to length(d0) - 1 do
        d[i] := d0[i];

    {for solving Ax = d
    a (lower), b (diagonal), c (upper) are the column vectors for the compressed tridiagonal matrix A, d is the right hand side vector}
    n := length(d); //% n is the number of rows

    {Modify the first-row coefficients}
    c[0] := c[0] / b[0];  //  % Division by zero risk.
    d[0] := d[0] / b[0];

    for i := 1 to n - 2 do
    begin
        tmp := b[i] - a[i] * c[i-1];
        c[i] := c[i] / tmp;
        d[i] := (d[i] - a[i] * d[i-1]) / tmp;
    end;

    d[n-1] := (d[n-1] - a[n-1]*d[n-2]) / (b[n-1] - a[n-1]*c[n-2]);

    //back substitute.
    setlength(result,n);
    result[n-1] := d[n-1];
    for i := n - 2 downto 0 do
        result[i] := d[i] - c[i]*result[i + 1];
end;

class function  TSolversGeneralFunctions.TDMAMultiplyComplex(const a,b,c0,d0 : TComplexVect) : TComplexVect;
var
    i, n : integer;
begin
    {tridiagonal matrix multiply A*d0 = result, a,b and c0 are the lower, diagonal and upper diagonals of A}

    n := length(d0);
    setlength(result,n);
    i := 0;
    result[i] := b[i]*d0[i] + c0[i]*d0[i+1];
    for i := 1 to n - 2 do
        result[i] := a[i]*d0[i-1] + b[i]*d0[i] + c0[i]*d0[i+1];
    i := n - 1;
    result[i] := a[i]*d0[i-1] + b[i]*d0[i];
end;

class procedure TSolversGeneralFunctions.TDMAMultiplyComplexInPlace(const B: TTriDiagonal; const vect, result: TComplexVect);
var
    i, n : integer;
begin
    {tridiagonal matrix multiply A*vect = result; using the lower, diagonal and upper diagonals of A}

    n := length(vect);
    if n = 0 then exit;
    i := 0;
    result[i] := B.diag[i]*vect[i] + B.upper[i]*vect[i+1];
    for i := 1 to n - 2 do
        result[i] := B.lower[i]*vect[i-1] + B.diag[i]*vect[i] + B.upper[i]*vect[i+1];
    i := n - 1;
    result[i] := B.lower[i]*vect[i-1] + B.diag[i]*vect[i];
end;

class procedure TSolversGeneralFunctions.TDMAMultiplyComplexInPlaceWrap(const B: TTriDiagonal; const vect, result: TComplexVect);
var
    i, n : integer;
begin
    {tridiagonal matrix multiply A*vect = result; using the lower, diagonal and upper diagonals of A}
    //this version wraps around, for use in theta calcs

    n := length(vect);
    if n = 0 then exit;

    i := 0;
    result[i] := B.lower[i]*vect[n-1] + B.diag[i]*vect[i] + B.upper[i]*vect[i+1];
    for i := 1 to n - 2 do
        result[i] := B.lower[i]*vect[i-1] + B.diag[i]*vect[i] + B.upper[i]*vect[i+1];
    i := n - 1;
    result[i] := B.lower[i]*vect[i-1] + B.diag[i]*vect[i] + B.upper[i]*vect[0];
end;


class function TSolversGeneralFunctions.addIterator3D(const x, dx: TRayIterator3D; const ds: double): TRayIterator3D;
begin
    result.x := x.x + dx.x * ds;
    result.y := x.y + dx.y * ds;
    result.z := x.z + dx.z * ds;
    result.xi := x.xi + dx.xi * ds;
    result.eta := x.eta + dx.eta * ds;
    result.zeta := x.zeta + dx.zeta * ds;
    result.q := x.q + dx.q * ds;
    result.p := x.p + dx.p * ds;
    result.terminated := x.terminated;
end;

class function TSolversGeneralFunctions.additionalAttenuationInBottomLayer(const z, subDepth1, subDepth2: double): double;
//following eq 6.146, ie Brock, the aesd parabolic equation mode
//resulting atten is an exponential func going from 0 to alpha
const
    alpha = 0.01;
var
    qq: double;
begin
    qq := (z - SubDepth2) / ((subdepth2 - subdepth1) / 3); {D = (subdepth2 - subdepth1) / 3}
    result := exp(-qq*qq) * alpha;
end;

class function TSolversGeneralFunctions.nLamIncrease(const f: double): double;
begin
    result := max(1,log2(f) - 6);
end;

class procedure  TSolversGeneralFunctions.depthIsCloseToInterface(const dv: TDepthValArray; const depth, k0, k02: double; out rho, q1: double);
var
    i, indexOfNearestLayer, indsCount: integer;
    firstInd, lastInd: integer;
    rhoA, rhoB, drho, d2rho, q, rhoQ: double;
    L, distThreshold: double;
    between2Layers: boolean;
begin
    if length(dv) = 0 then
    begin
        rho := 1;
        q1 := 0;
        exit;
    end;

    //the input to tanh should span -2 to +2
    distThreshold := 4/k0;

    indexOfNearestLayer := -1;
    indsCount := 0;
    firstInd := -1;
    lastInd := -1;

    rho := dv[0].val;
    for i := 1 to length(dv) - 1 do
    begin
        if dv[i].depth <= depth then
            rho := dv[i].val; //return rho of this layer
        if (abs(dv[i].depth - depth) < distThreshold) then
        begin
            if (indexOfNearestLayer = -1) or ( (abs(depth - dv[i].depth)) < (abs(depth - dv[indexOfNearestLayer].depth)) ) then
                indexOfNearestLayer := i;

            inc(indsCount);
            if firstInd = -1 then
                firstInd := i;
            lastInd := i;
        end;
    end;

    //check if we need to do change the calc away from the standard
    between2Layers := indsCount > 1;
    if between2Layers then
    begin
        if indexOfNearestLayer = 0 then
            between2Layers := false;

        if depth < dv[indexOfNearestLayer].depth then
        begin
            if indexOfNearestLayer = firstInd  then
                between2Layers := false;
        end
        else if indexOfNearestLayer = lastInd  then
            between2Layers := false;
    end;

    if between2Layers then
    begin
        //the input to tanh should span -2 to +2
        //need to do different things for the nearest layer is above or below the depth
        if depth <= dv[indexOfNearestLayer].depth then
            L := (dv[indexOfNearestLayer].depth - dv[indexOfNearestLayer - 1].depth) / 8
        else
            L := (dv[indexOfNearestLayer + 1].depth - dv[indexOfNearestLayer].depth) / 8;
    end
    else
    begin
        //only 1 or 0 nearby layers found

        //indexOfNearestLayer = 0 means only surface near threshold. -1 means none found.
        //not in a transition zone. all derivatives = 0
        //rho is already set, just set q1 and exit.
        if (indexOfNearestLayer <= 0) then
        begin
            q1 := 0;
            exit;
        end
        else//indexOfNearestLayer > 0 means we have a transition and can calc rho and q1
            L := 2 / k0; // 6.154. we are in a transition zone, with only 1 layer near the depth. Can possibly reduce this, now we are using the split-step version
    end;
    rhoA    := dv[indexOfNearestLayer - 1].val;
    rhoB    := dv[indexOfNearestLayer    ].val;
    q       := tanh((depth - dv[indexOfNearestLayer].depth) / L); //last term in 6.153
    rhoQ    := 0.5*(rhoB - rhoA);
    rho     := 0.5*(rhoB + rhoA) + rhoQ * q;      //6.153

    //d k*tanh(Az)/dz = kA(1 - tanh(Az)^2) = A(sech(Az)^2)
    //d2 k*tanh(Az)/dz2 = -2k A tanh(Az) dtanh(Az) = -2k*A^2/cosh(Az)^2*tanh(Az) = -2k*A^2*(1-TANH(Az)^2)*TANH(Az)
    drho    := rhoQ/L*(1 - system.sqr(q)); //kA(1 - tanh(Az)^2)
    d2rho   := -2/L*q*drho; //See 'calculation of index of refraction in PE.ods'
    q1      := 0.5/k02*(1/rho*d2rho - 1.5/system.sqr(rho)*system.sqr(drho)); //additional term in 6.152
end;

class function  TSolversGeneralFunctions.depthIsCloseToInterfaceNoSmoothing(const dv: TDepthValArray; const depth, k0, k02: double): double;
var
    i: integer;
begin
    if length(dv) = 0 then
        exit(1);

    result := dv[0].val;
    for i := 1 to length(dv) - 1 do
        if dv[i].depth <= depth then
            result := dv[i].val;
end;

class function TSolversGeneralFunctions.interpolationFunPE(const aM: TMatrix; const d: double): double;
var
    ind : integer;
begin
    ind := aM.findWithNoInternalAssigns(d,0);
    if ind < 0 then
        result := aM[1,0]
    else if (ind = (length(aM[0]) - 1)) or (aM[0,ind + 1] = aM[0,ind]) then
        result := aM[1,ind]
    else
        result := aM[1,ind] + (d             - aM[0,ind]) /
                              (aM[0,ind + 1] - aM[0,ind]) *
                              (aM[1,ind + 1] - aM[1,ind]);
end;

class function TSolversGeneralFunctions.interpolationFunPE(const rd: TRangeValArray; const d: double): double;
var
    ind : integer;
begin
    ind := rd.findRange(d);
    if ind < 0 then
        result := rd[0].val
    else if (ind = (length(rd) - 1)) or (rd[ind + 1].val = rd[ind].val) then
        result := rd[ind].val
    else
        result := rd[ind].val + (d             - rd[ind].range) /
                              (rd[ind + 1].range - rd[ind].range) *
                              (rd[ind + 1].val - rd[ind].val);
end;

class function TSolversGeneralFunctions.interpolationFunPE(const rd: TDepthValArray; const d: double): double;
var
    ind : integer;
begin
    ind := rd.findDepth(d);
    if ind < 0 then
        result := rd[0].val
    else if (ind = (length(rd) - 1)) or (rd[ind + 1].val = rd[ind].val) then
        result := rd[ind].val
    else
        result := rd[ind].val + (d             - rd[ind].depth) /
                              (rd[ind + 1].depth - rd[ind].depth) *
                              (rd[ind + 1].val - rd[ind].val);
end;

class function TSolversGeneralFunctions.interpolationFunPE(const z : TDoubleDynArray; const aC : TComplexVect; const d : double) : TComplex;
var
    ind : integer;
begin
    ind := z.find(d);
    if ind < 0 then
        result := aC[0]
    else if (ind = (length(aC) - 1)) or (z [ind + 1] = z [ind]) then
        result := aC[ind]
    else
        result := aC[ind] + (aC[ind + 1] - aC[ind]) * (d           - z [ind]) /
                                                      (z [ind + 1] - z [ind]);
end;

class procedure TSolversGeneralFunctions.setupRayAngles(const thetaStart, thetaEnd: double; const ntheta: integer; out theta: TDoubleDynArray; out delTheta: double);
var
    i: integer;
    t0: double;
begin
    t0 := max(-90, min(thetaStart, thetaEnd)) * pi / 180; // change to radians
    delTheta := ((min(90, max(thetaStart, thetaEnd)) * pi / 180) - t0) / max(1, (ntheta - 1));
    setlength(theta, ntheta);
    for i := 0 to ntheta - 1 do
        theta[i] := t0 + i * delTheta;
end;

class function TSolversGeneralFunctions.sqLineMagnitude(const x1, y1, x2, y2: double): double;
begin
    result := system.sqr(x2 - x1) + system.sqr(y2 - y1);
end;

class function TSolversGeneralFunctions.PEStarterGreene(const k0, srcZ, deltaz: double; const nz: integer): TComplexVect;
var
    iz: integer;
    az: double;
begin
    setlength(result, nz);
    for iz := 0 to nz - 1 do
    begin
        az := deltaz * iz - srcZ;
        result[iz] := system.sqrt(k0) * (1.4467 - 0.4201 * k0 * k0 * az * az) * exp(-k0 * k0 * az * az / 3.0512); { Greene's source eq 6.102 }
    end;
end;

class function TSolversGeneralFunctions.PEStarterGaussian(const k0, srcZ, deltaz: double; const nz: integer): TComplexVect;
var
    iz: integer;
    az: double;
begin
    setlength(result,nz);
    for iz := 0 to nz - 1 do
    begin
        az := deltaz * iz - srcZ;
        result[iz] := system.sqrt(k0) * exp(-k0 * k0 * az * az); {Gaussian starter eq 6.101}
    end;
end;

class function TSolversGeneralFunctions.PEStarterGeneralisedGaussian(const k0, srcZ, halfWidth, angleChangeRadians, deltaz : double; const nz : integer) : TComplexVect;
var
    iz : integer;
    az, k02, tanhalfWidth, tanhalfWidth2 : double;
begin
    setlength(result,nz);
    k02 := system.sqr(k0);
    tanhalfWidth := tan(halfWidth);
    tanhalfWidth2 := tanhalfWidth * tanhalfWidth;
    for iz := 0 to nz - 1 do
    begin
        az := deltaz * iz - srcZ;
        result[iz] := system.sqrt(k0) * tanHalfWidth *
                      exp( - k02 / 2 * system.sqr(az) * tanHalfWidth2 ) *
                      ComplexEXP( MakeComplex( 0, k0 * az * sin(angleChangeRadians)) );
    end;
end;

class procedure TSolversGeneralFunctions.PEStarterAngleChange(const f, c0, angleChangeRadians, deltaz : double; const nz : integer; const psiComplexVect : TComplexVect);
var
    iz : integer;
    angleChange : TComplex;
begin
    if nz <> length(psiComplexVect) then exit;

    for iz := 0 to nz - 1 do
    begin
        angleChange := ComplexEXP(MakeComplex(0, 2 * PI * f * (deltaz*iz) / c0 * tan(angleChangeRadians)));
        psiComplexVect[iz] := psiComplexVect[iz] * angleChange;
    end;
end;

class procedure TSolversGeneralFunctions.applyDirectivityToStarter(const f, k0, srcZ, deltaz: double; const nz: integer; const directivities: TDirectivityList; const starter: TStarter);
const
    thetaLength = 178;
var
    a,b: TVector2;
    iz, len: integer;
    fftdz: double;
    resampledz: TDoubleDynArray;
    resampledPsi: TComplexVect;
    theta: TDoubleDynArray;
    directivity: TDepthValArray;
    itheta: integer;
    thisAngle, thisDirectivity: double;
    z: TDoubleDynArray;

    function wavenumber(iz: integer): double;
    begin
        result := pi * iz / nz / fftdz;
    end;
begin
    //6.103
    len := nextPow2(nz);
    a.SetLength(len*2);
    b.setlength(len*2);

    setlength(z, nz);
    for iz := 0 to nz - 1 do
        z[iz] := iz * deltaz;

    fftdz := deltaz * nz / len;
    setlength(resampledz,len);
    SetLength(resampledPsi,len);

    setlength(theta,thetaLength);
    for iTheta := 0 to thetaLength - 1 do
        theta[iTheta] := (-89 + iTheta) / 180 * PI; //radians
    directivity := TSolversGeneralFunctions.getDirectivityAtFreq2(f, theta, directivities);

    for iz := 0 to len - 1 do
    begin
        a.setBand(iz, TSolversGeneralFunctions.interpolationFunPE(z, starter.psi, fftdz * iz));
        a.setBand(2*len - 1 - iz, a.getBand(iz).conjugate);
    end;
    fft(len*2,a,b);

    for Iz := 0 to len - 1 do
    begin
        thisAngle := ArcSin( wavenumber(iz) /k0);
        thisDirectivity := interpolationFunPE(directivity, thisAngle);
        THelper.RemoveWarning(thisDirectivity);
        b[iz] := ifthen(iz = len/2,1,0) {undb20(thisDirectivity)} * b[iz];

        thisDirectivity := interpolationFunPE(directivity, -thisAngle);
        THelper.RemoveWarning(thisDirectivity);
        b[2*len - 1 - iz] := ifthen(iz = len/2,1,0) {undb20(thisDirectivity)} * b[iz];
    end;

    ifft(len*2,b,a);
    for Iz := 0 to len - 1 do
    begin
        resampledz[iz] := fftdz * iz;
        resampledPsi[iz] := a.getBand(iz);
    end;

    for Iz := 0 to nz - 1 do
        starter.psi[iz] := TSolversGeneralFunctions.interpolationFunPE(resampledz, resampledPsi, deltaz *iz);
end;

class procedure TSolversGeneralFunctions.PEStarterWithDirectivity(const f, k0, srcZ, deltaz: double; const nz: integer; const directivities: TDirectivityList; out starter: TComplexVect; out starterScalingFactor: double);
{
const
    thetaLength = 178;
var
    directivity : TSingleDynArray;
    theta : TDoubleDynArray;
    iTheta, iz : integer;
    amp, halfWidth : double;
    az : double;
    }
begin
{    //NB, we don't want to do tan at 90, -90 so start from 89 deg
    setlength(theta,thetaLength);
    for iTheta := 0 to thetaLength - 1 do
        theta[iTheta] := (-89 + iTheta) / 180 * PI; //radians
    directivity := TSolversGeneralFunctions.getDirectivityAtFreq(f,theta,directivities);

    halfWidth := PI / length(theta) / 2;

    setlength(starter,nz);
    starterScalingFactor := 0;
    for iTheta := 0 to thetaLength - 1 do
    begin
        amp := undB20(-directivity[itheta]);
        starterScalingFactor := starterScalingFactor + amp;
        for iz := 0 to nz - 1 do
        begin
            az := iz*deltaz - srcZ;
            starter[iz] := starter[iz] + amp * system.sqrt(k0) * tan(halfWidth) *
                                         exp( - k0 * k0 / 2 * az * az * tan(halfWidth) * tan(halfWidth) ) *
                                         ComplexEXP( MakeComplex( 0, k0 * az * sin(theta[iTheta])) ); //generalised Gaussian starter eq 6.107. Last term is the tilt angle
        end;
    end;
    starterScalingFactor := starterScalingFactor / thetaLength;

    // scale so that total amplitude is the same as for case with no directionality
    for iz := 0 to nz - 1 do
        starter[iz] := starter[iz] * PI / 2 / thetaLength; //this gets normalised later
        }
end;

class function TSolversGeneralFunctions.PEZOversamplingFactor(const zOversamplingFactor, f, zMax, c0: double; const dMax: integer): integer;
begin
    result := max(1,saferound(zOversamplingFactor * f * zMax / dMax / c0));
end;

class function TSolversGeneralFunctions.targetPressureAtRange(const r, srcZ: double): double;
begin
    if r < srcZ then
        result := 1/r
    else
        result := system.sqrt(1/srcz/r); // equals power(10, 1/20 * (20*log10(1/srcZ) + 10*log10(srcZ / r)));
end;

class function TSolversGeneralFunctions.PEStarterInterpolation(const deltaz: double; const nz: integer; const modalStarterZ: TDoubleDynArray; const modalStarterPsi: TComplexVect): TComplexVect;
var
    iz: integer;
begin
    setlength(result,nz);
    for iz := 0 to nz - 1 do
        result[iz] := TSolversGeneralFunctions.interpolationFunPE(modalStarterZ, modalStarterPsi, deltaz * iz); //z spacing differs between the solvers, so interpolate
end;

class procedure TSolversGeneralFunctions.modesKcAndPsi(const im, nz: integer;
                                                       const omega, h: double;
                                                       const theEigs, Dd, Ed, rhozVect, attenVect, cVect: TDoubleDynArray;
                                                       var tmpPsi1, tmpPsi2, mdmid: TDoubleDynArray;
                                                       var kc: TComplexVect; var psi: TMatrix);
var
    kd, k2d, alpham: double;
    iz: integer;
begin
    k2d := theEigs[im]; //the eigenvalues are ordered largest first (ie largest horizontal wavenumber)
    kd := system.sqrt(k2d); //the hh factor is taken out already, due to our choice of matrix diagonals scaling

    {initial mode shapes, prepare matrix diagonal and right hand side for [A - lambda]*psi0 = [1]}
    if length(mdmid) <> length(Dd) then
        setlength(mdmid, length(dd));
    Dd.plusConstInPlace(-k2d,mdmid);

    if length(tmpPsi1) <> nz then
        SetLength(tmpPsi1,nz);
    tmpPsi1.setAllVal(1);

    if length(tmpPsi2) <> nz then
        setlength(tmpPsi2,nz);

    {estimate mode shapes, with real k and rhs = 1. For better estimates, we can repeatedly solve with tmppsi as the rhs.
    Could also use the last tmppsi as the initial estimate, when the seafloor is not rapidly changing depth}
    TSolversGeneralFunctions.TDMAsolverInPlace(Ed,mdmid,Ed,tmpPsi1,tmppsi2);
    TSolversGeneralFunctions.psiNormaliseInPlace(tmppsi2,rhozVect,tmpPsi1);
    TSolversGeneralFunctions.TDMAsolverInPlace(Ed,mdmid,Ed,tmppsi1,tmppsi2);
    TSolversGeneralFunctions.psinormaliseInPlace(tmppsi2,rhozVect,tmpPsi1);

    //perturbation of k for attenuation. k(z) = w/c(z) + i alpha  eq 5.174. nb alpha in nepers/m
    alpham := 0;
    if kd <> 0 then //do nothing on zero padding wavenumbers
    begin
        for iz := 0 to nz - 1 do
            alpham := alpham + attenVect[iz]*omega*tmppsi1[iz]*tmppsi1[iz]/cVect[iz]/rhozVect[iz]; //eq 5.176
        alpham := alpham/kd;
    end;
    kc[im].r := kd;
    kc[im].i := alpham;

    for iz := 0 to nz - 1 do
        psi[iz,im] := tmppsi1[iz];  //save mode shapes for this step
end;

class function TSolversGeneralFunctions.PEStarterThompson(const k0, srcZ, halfBeamWidth, deltaz: double; const nz: integer): TComplexVect;
var
    a,b: TVector2;
    iz, len: integer;
    kz: double;
    resampledz: TDoubleDynArray;
    resampledPsi: TComplexVect;
    //{$DEFINE TEST_THOMPSON}
    {$IF defined (DEBUG) and defined (TEST_THOMPSON)}
    s: string;
    {$ENDIF}
    function wavenumber(iz: integer): double;
    begin
        result := pi * iz / nz / deltaz;
    end;
begin
    //6.103
    len := nextPow2(nz);
    setlength(result,nz);
    a.SetLength(len*2);
    b.setlength(len*2);
    setlength(resampledz,len);
    SetLength(resampledPsi,len);

    for Iz := 0 to len - 1 do
        if wavenumber(iz) < k0 * system.sin(halfBeamWidth) then
        begin
            kz := wavenumber(iz);
            a[iz] := system.sqrt(8*pi/k0) *
                system.sin(kz*srcz) *
                system.math.power(1 - system.sqr(kz) / system.sqr(k0),-1/4);
            //a[iz] := NullComplex;
            //kz2 := wavenumber(2*len - 1 - iz);
            a[2*len - 1 - iz] := system.sqrt(8*pi/k0)*system.sin(kz*srcz)*system.math.power(1 - system.sqr(kz) / system.sqr(k0),-1/4);
            //a[2*len - 1 - iz] := NullComplex;
        end
        else
        begin
            a[iz] := NullComplex;
            a[2*len - 1 - iz] := NullComplex;
        end;

    {$IF defined (DEBUG) and defined (TEST_THOMPSON)}
    s := '';
    for Iz := 0 to 2*len - 1 do
        s := s + floattostr(a[iz].r) + #09 + floattostr(a[iz].i) + #13#10;
    THelper.setClipboard(s);
    {$ENDIF}

    ifft(len*2,a,b);
    for Iz := 0 to len - 1 do
    begin
        resampledz[iz] := deltaz * nz / len * iz;
        resampledPsi[iz] := b.getBand(iz);
    end;

    for Iz := 0 to nz - 1 do
        Result[iz] := TSolversGeneralFunctions.interpolationFunPE(resampledz, resampledPsi, deltaz *iz);
end;

function TSolverErrorHelper.toString: string;
begin
    case self of
        kNoSediment:  result := 'Sediment not found ';
        kNoModes: result := 'Solver found no modes at source location ';
        kNoFrequencies: result := 'No frequencies to solve';
        kFallbackStarterUsed: result := 'PE solver used analytical starter';
        kSourceBelowSubDepth: result := 'Source is below the calculation sub-depth';
        kSourceBelowSeafloorRaySolver: result := 'Ray solver cannot solve for sources below the seafloor';
    else
        result := '';
    end;
end;


procedure TStarter.scale(const srcZ: double);
    //outputScaling. We want the levels at r[0] and zs to be correctly scaled, given spherical spreading from the source.
    //Target pressure is found from the 20log(rb) + 10glog(r/rb)
    //get the greatest pressure at this range
var
    tmpPForSourceScaling: TComplex;
    iz, nz: integer;
    outputScaling: double;
begin
    tmpPForSourceScaling := NullComplex;
    nz := length(self.psi);
    for iz := 0 to nz - 1 do
        if self.psi[iz].modulusSquared > tmpPForSourceScaling.modulusSquared then
            tmpPForSourceScaling := self.psi[iz];
    if tmpPForSourceScaling.modulusSquared = 0 then
        outputScaling := 1
    else
        outputScaling := TSolversGeneralFunctions.targetPressureAtRange(1,srcZ) / tmpPForSourceScaling.modulus;

    for iz := 0 to nz - 1 do
        self.psi[iz] := self.psi[iz] * outputScaling;
end;

end.
