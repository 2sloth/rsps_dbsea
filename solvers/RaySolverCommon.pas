unit RaySolverCommon;

interface

uses
    system.classes, system.math, system.types, system.Diagnostics,
    dBSeaconstants, helperFunctions, baseTypes, solversGeneralFunctions;

type

  TRaySolverCommon = class(TObject)
  public
    class procedure initCProfiles(const cProfile: TDepthValArray; out constSSP: boolean; var c, dc_dz, d2c_dz2: TDoubleDynArray);
  end;

implementation

{$B-}

class procedure TRaySolverCommon.initCProfiles(const cProfile: TDepthValArray;
                                                  out constSSP: boolean;
                                                  var c, dc_dz, d2c_dz2: TDoubleDynArray);
const
    dDepth = 1.0;  //implicit in following calcs
var
    iz: integer;
begin
    //sound speed profile and 1st and 2nd z derivatives by unequal finite difference, as the input points are maybe not equally spaced

    //resample to 1m vertical spacing
    TSolversGeneralFunctions.myinterp2(safeceil(cProfile.maxDepth / dDepth), dDepth, cProfile, c);

    if length(dc_dz) <> length(c) then
        setlength(dc_dz,length(c));

    if length(d2c_dz2) <> length(c) then
        setlength(d2c_dz2,length(c));

    if length(c) = 2 then
    begin
        //use different calcs if only 2 input points
        dc_dz := [c[1] - c[0], 0];
        d2c_dz2 := [0,0];
    end
    else if length(c) > 2 then
    begin
        //more than 2 points in profile

        //first point
        iz := 0;
        dc_dz[iz] := (-c[iz + 2] + 4*c[iz + 1] - 3*c[iz]) / 2;
        d2c_dz2[iz] := 0;

        //all points except first and last
        for iz := 1 to length(c) - 2 do
        begin
            dc_dz[iz] := (c[iz + 1] - c[iz - 1]) / 2;
            d2c_dz2[iz] := 0;
        end;

        //last point
        iz := length(c) - 1;
        dc_dz[iz] := (3*c[iz] - 4*c[iz - 1] + c[iz - 2]) / 2;
        d2c_dz2[iz] := 0;

        //could use https://mathformeremortals.wordpress.com/2013/01/12/a-numerical-second-derivative-from-three-points/ which is const across the 3 points
        //cubic spline is piecewise linear between points
    end;

    {if constant SSP, we can use less stringent integration methods}
    constSSP := true;
    for iz := 0 to length(dc_dz) - 1 do
        if dc_dz[iz] <> 0 then
        begin
            constSSP := false;
            break;
        end;

    {interpolate c, dcdz and d2cdz2 so that there is 1m spacing between points, so we can
    easily lookup the appropriate index}
    //dcdz := TSolversGeneralFunctions.myinterp(newx, oldx, dcdzin);
    //d2cdz2 := TSolversGeneralFunctions.myinterp(newx, oldx, d2cdz2in);
end;


//class procedure TRaySolverCommon.initCProfiles(const aCProfile2: TDepthValArray;
//                                                  out constSSP: boolean;
//                                                  out c, dcdz, d2cdz2: TDoubleDynArray);
//    procedure resampleC(out c, newz: TDoubleDynArray);
//    var
//        oldC, oldz: TDoubleDynArray;
//        i: integer;
//    begin
//        setlength(oldC,length(aCProfile2));
//        setlength(oldz,length(aCProfile2));
//        for i := 0 to length(aCProfile2) - 1 do
//        begin
//            oldz[i] := aCProfile2[i].depth;
//            oldC[i] := aCProfile2[i].val;
//        end;
//
//        setlength(newz,safeceil(aCProfile2.maxDepth));
//        for i := 0 to length(newz) - 1 do
//            newz[i] := i;
//        c := TSolversGeneralFunctions.myinterp(newz, oldz, oldC);
//    end;
//var
//    //dcdzin, d2cdz2in: TDoubleDynArray;
//    iz: integer;
//    denominator, delx1, delx2: double;
//    z: TDoubleDynArray;
//begin
//    {sound speed profile and 1st and 2nd z derivatives by unequal finite difference, as the
//    input points are maybe not equally spaced}
//
//    resampleC(c, z);
//
//    setlength(dcdz,length(z));
//    setlength(d2cdz2,length(z));
//    if length(z) = 2 then
//    begin
//        {use different calcs if only 2 input points}
//        dcdz[0] := (c[1] - c[0])/(z[1] - z[0]);
//        dcdz[1] := dcdz[0];
//        d2cdz2 := [0,0];
//    end
//    else
//    begin
//        {more than 2 points in profile}
//
//        {first point}
//        iz := 0;
//        delx1 := z[iz + 1] - z[iz];
//        delx2 := z[iz + 2] - z[iz + 1];
//        denominator := delx1*delx2*(delx1+delx2);
//        dcdz[iz] := (-sqr(delx1)*c[iz + 2] +
//                       power((delx1 + delx2),2)*c[iz + 1] -
//                       (sqr(delx2) + 2*delx1*delx2)*c[iz]) /
//                       denominator;
//        d2cdz2[iz] := 0;//2*(delx1*c[iz + 2] -
//                        //(delx1 + delx2)*c[iz + 1] +
//                        //delx2*c[iz]) /
//                        //denominator;
//
//        {all points except first and last}
//        for iz := 1 to length(z) - 2 do
//        begin
//            delx1 := z[iz] - z[iz - 1];
//            delx2 := z[iz + 1] - z[iz];
//            denominator := delx1*delx2*(delx1+delx2);
//            dcdz[iz] := (delx1*delx1*c[iz + 1] +
//                          (delx2*delx2 - delx1*delx1)*c[iz] -
//                          delx2*delx2*c[iz - 1]) /
//                          denominator;
//            d2cdz2[iz] := 0;// 2*(delx1*c[iz + 1] -
//                            //(delx1 + delx2)*c[iz] +
//                            //delx2*c[iz - 1]) /
//                            //denominator;
//        end;
//        {last points}
//        iz := length(z) - 1;
//        delx1 := z[iz - 1] - z[iz - 2];
//        delx2 := z[iz    ] - z[iz - 1];
//        denominator := delx1*delx2*(delx1+delx2);
//        dcdz[iz] := ((sqr(delx1) + 2*delx1*delx2)*c[iz] -
//                        sqr(delx1 + delx2)*c[iz - 1] +
//                        sqr(delx2)*c[iz - 2]) /
//                        denominator;
//        d2cdz2[iz] := 0;//2*(delx1*c[iz] -
//                          //(delx1 + delx2)*c[iz - 1] +
//                          //delx2*c[iz - 2]) /
//                          //denominator;
//        //could use https://mathformeremortals.wordpress.com/2013/01/12/a-numerical-second-derivative-from-three-points/ which is const across the 3 points
//        //cubic spline is piecewise linear between points
//    end;
//
//    {if constant SSP, we can use less stringent integration methods}
//    constSSP := true;
//    for iz := 0 to length(dcdz) - 1 do
//        if dcdz[iz] <> 0 then
//        begin
//            constSSP := false;
//            break;
//        end;
//
//    {interpolate c, dcdz and d2cdz2 so that there is 1m spacing between points, so we can
//    easily lookup the appropriate index}
//    //dcdz := TSolversGeneralFunctions.myinterp(newx, oldx, dcdzin);
//    //d2cdz2 := TSolversGeneralFunctions.myinterp(newx, oldx, d2cdz2in);
//end;


end.
