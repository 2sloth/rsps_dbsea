unit dBSeaPE2;

{PE2 vs PE1:
directivities
more use of functions}

interface

uses
    system.classes, system.math, system.sysutils, system.types,
    system.generics.collections,
    helperFunctions,
    dBSeaconstants, Mcomplex,
    baseTypes,
    seafloorScheme, material,
    directivity, solversGeneralFunctions, mcklib, globalsUnit,
    PadeApproximants, TriDiagonal, Nullable;

type

  TdBSeaPE2Solver = class(TObject)
  public
  const
    dSubDepth1DistFactor = 0.5;

    class procedure depthsInit(const f, zmax, zOversamplingFactor: double;
                        const dMax: integer;
                        const aCProfile3: TDepthValArrayAtRangeArray;
                        var nz, nzInit, zOversamp: integer;
                        var c0, subDepth1, subDepth2, deltaz: double);
    class function main(const f, srcZ, rmax, zmax: double;
                        const dMax, nPadeTerms: integer;
                        const rInit: TDoubleDynArray;
                        const aZProfile2: TRangeValArray;
                        const aCProfile3: TDepthValArrayAtRangeArray;
                        const zOversamplingFactor, rOversamplingFactor: double;
                        const seafloors: TSeafloorRangeArray;
                        const starter: TStarter;
                        const pCancel: TWrappedBool;
                        const fractionDone: TWrappedDouble;
                        {$IFDEF DEBUG}
                        const directivities: TDirectivityList;
                        {$ENDIF}
                        var solverErrors: TSolverErrors): TMatrix;
  end;

implementation

{$B-}

class procedure TdBSeaPE2Solver.depthsInit(const f, zmax, zOversamplingFactor: double;
                        const dMax: integer;
                        const aCProfile3: TDepthValArrayAtRangeArray;
                        var nz, nzInit, zOversamp: integer;
                        var c0, subDepth1, subDepth2, deltaz: double);
var
    dBottomExtension: double;
    SSPRangeInd: integer;
begin
    c0 := aCProfile3.depthValForRange(0,SSPRangeInd).average;

    dBottomExtension := 2;
    if (zmax * dBottomExtension) < c0 / f then
        dBottomExtension := c0 / f / zmax;

    SubDepth1 := zmax * (dBottomExtension - dSubDepth1DistFactor);
    SubDepth2 := zmax * dBottomExtension;

    zOverSamp := TSolversGeneralFunctions.PEZOversamplingFactor(zOversamplingFactor,f,zMax,c0,dmax);
    nzInit := round(dBottomExtension * dMax);
    nz := 1 + (nzInit - 1) * zOverSamp;
    deltaz := SubDepth2 / (nz-1);
end;

class function TdBSeaPE2Solver.main(const f, srcZ, rmax, zmax: double;
                               const dMax, nPadeTerms: integer;
                               const rInit: TDoubleDynArray;
                               const aZProfile2: TRangeValArray;
                               const aCProfile3: TDepthValArrayAtRangeArray;
                               const zOversamplingFactor, rOversamplingFactor: double;
                               const seafloors: TSeafloorRangeArray;
                               const starter: TStarter;
                               const pCancel: TWrappedBool;
                               const fractionDone: TWrappedDouble;
                               {$IFDEF DEBUG}
                               const directivities: TDirectivityList;
                               {$ENDIF}
                               var solverErrors: TSolverErrors): TMatrix;
const
    //dSubDepth1DistFactor = 0.5;
    starterR = 1.0;
var
    c1: TDoubleDynArray;
    C,B: TArray<TTriDiagonal>;
    psiComplexVect: TComplexVect;
    layerDensityList: TDepthValArray;
    psiM: TComplexMat;
    omega,c0,k0,k02: double;
    deltaz: double;
    atten, c_ir, rho_ir: double;
    zbThis, zbPrev: double;
    nz,nr: integer;
    attCConst: double;
    k,ir,irr,iz,izz,irAtStarter: integer;
    irNanBegins: Nullable<integer>;
    nSqr: double;
    q1,fac: double;
    nzInit, nrInit: integer;
    rOverSamp, zOverSamp: integer;
    SubDepth1,SubDepth2: double;
    dhank2: double;
    //dBottomExtension: double;
    starterSum: double;
    dpress2: double;
    nilSedimentError: boolean;
    aMaterial: TMaterial;
    directivityStarterScaling: double;
    bUseModalStarter: boolean;
    rhs,tmpCUpper,tmpRhs: TComplexVect;
    r: TDoubleDynArray;

    diagTerms: TArray<TDiagTerms>;
    padeTerms: TPadeTerms;
    matrixFormers: TArray<TMatrixFormer>;
    k0SqdeltazSqOver2: double;
    rhoPrev,rhoThis,nSqrTilde: double;
    nSq1,nSq2: TComplex;
    psiComplexVectPlusOneThisTerm: TComplexVect;
    deltaThisTerm: TComplexMat;

    seafloor: TSeafloorScheme;
    prevSeafloorIndex, seafloorIndex: integer;
    depthVal: TDepthValArray;
    SSPRangeInd, prevSSPRangeInd: integer;

    function z(const iz: integer): double;
    begin
        result := iz * deltaz;
    end;

    function deltar(const ir: integer): double;
    begin
        //allows for unequal r spacing
        if ir >= length(r) - 1 then
            result := r[length(r) - 1] - r[length(r) - 2]
        else
            result := r[ir+1] - r[ir];
    end;

    function localC(const iz: integer; const aSeafloor: TSeafloorScheme; const zbThis: double): double;
    var
        aMaterial: TMaterial;
    begin
        if z(iz) < zbThis then
            result := c1[iz]         //ssp c
        else
        begin
            //todo seafloor referenced from sea surface?
            aMaterial := aSeafloor.getMaterialAtDepthBelowSurface(z(iz), zbThis);
            if assigned(aMaterial) then
                result := aMaterial.c
            else
                result := water.c;
        end;
    end;

    procedure solverStep(const B,C: TArray<TTriDiagonal>; const psiComplexVect: TComplexVect);
    var
        k: integer;
    begin
        //in place version for less memory assigns
        {we are solving C psi(ir+1) = B psi(ir), where C and B are tridiagonal, for each pade term}

        for k := 0 to max(1, nPadeTerms) - 1 do
        begin
            //form rhs the right hand side, ie B*psi(ir). Tridiagonal.
            TSolversGeneralFunctions.TDMAMultiplyComplexInPlace(B[k],psiComplexVect,rhs);
            //solve C psi(ir+1) = rhs for psi(ir+1). Can use the tridiagonal solver method
            TSolversGeneralFunctions.TDMASolverComplexInPlace(C[k],rhs,tmpCUpper,tmpRhs,psiComplexVect);
        end;

        {enforce pressure release surface}
        psiComplexVect[0].r := 0;
        psiComplexVect[0].i := 0;
    end;

    function calcDiagonalTerms(const matFormer: TMatrixFormer): TDiagTerms;
    begin
        result.v := rhoPrev / rhoThis; //6.187
        result.u := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqdeltazSqOver2Timeswstar1Overwstar2Minus1 +
            k0SqdeltazSqOver2*((nSq1 - 1) + result.v*(nSq2 - 1)); //6.186

        result.uHat := (rhoPrev + rhoThis) / rhoThis * matFormer.k0SqdeltazSqOver2Timesw1Overw2Minus1 +
            k0SqdeltazSqOver2*((nSq1 - 1) + result.v*(nSq2 - 1)); //6.188
    end;

    //{$DEFINE TESTPE}
    //{$DEFINE TEST_N}
    {$IF defined (DEBUG) and (defined(TESTPE) or Defined(TEST_N))}
    var
        s: string;

    function psiComplexVectSum(): double;
    var
        c: TComplex;
    begin
        result := 0;
        for c in psiComplexVect do
            result := result + c.modulusSquared;
    end;
begin
    s := '';
    {$ELSE}
begin
    {$ENDIF}

    //solverErrorMessages := '';

    omega := 2 * pi * f;  //angular freq

    {water properties}
    c0 := aCProfile3.depthValForRange(0,SSPRangeInd).average;
    k0 := omega / c0;
    k02 := k0*k0;

    //cb := container.scenario.sediment.c;
    //rhob := container.scenario.sediment.rho_g; //convert to g/cm3
    //attenb := container.scenario.sediment.attenuation;  //units dB/wavelength

    depthsInit(f, zmax, zOversamplingFactor, dMax,
        aCProfile3,
        nz, nzInit, zOversamp,
        c0, subDepth1, subDepth2, deltaz);

//    {sub bottom sediment stuff}
//    dBottomExtension := 2;
//    if (zmax * dBottomExtension) < c0 / f then
//        dBottomExtension := c0 / f / zmax;
//
//    SubDepth1 := zmax * (dBottomExtension - dSubDepth1DistFactor);
//    SubDepth2 := zmax * dBottomExtension;

//    //depths
//    zOverSamp := TSolversGeneralFunctions.PEZOversamplingFactor(zOversamplingFactor,f,zMax,c0,dmax); //amount of oversampling - reduce for faster, less accurate
//    nzInit := round(dBottomExtension * dMax);
//    nz := 1 + (nzInit - 1) * zOverSamp;
//    deltaz := SubDepth2 / (nz-1);
    //deltaz2 := deltaz * deltaz;

    //sound speed profile and wavenumbers
    setlength(c1,nz);
    depthVal := aCProfile3.depthValForRange(0,SSPRangeInd);
    prevSSPRangeInd := 0;
    for iz := 0 to nz - 1 do
        c1[iz] := TSolversGeneralFunctions.interpolationFunPE(depthVal,z(iz));

    //ranges
    r := rInit.truncateAtVal(rMax);
    nrInit := max(1, saferound(length(r),1));
    //rOverSamp := max(1,saferound(1/2*omega/c0*rmax/nrInit));
    rOverSamp := max(1, saferound(rOversamplingFactor*f*r.max/nrInit/c0));  //reduce for faster, less accurate
    r := TSolversGeneralFunctions.resample(r,nrInit*rOverSamp);
    nr := max(1, length(r));

    {$IF defined (DEBUG) and defined (TESTPE)}
    s := s + 'deltar' + #09 + floattostr(deltar) + #13#10;
    s := s + 'deltaz' + #09 + floattostr(deltaz) + #13#10;
    s := s + 'rOverSamp ' + #09 + IntToStr(rOverSamp) + #13#10;
    s := s + 'zOverSamp ' + #09 + IntToStr(zOverSamp) + #13#10;
    s := s + 'ir' + #09 + 'irr' + #13#10;
    {$ENDIF}

    k0SqdeltazSqOver2 := sqr(k0)*sqr(deltaz)/2;
    padeTerms := TPadeCalc.padeParameters(nPadeTerms);
    TSolversGeneralFunctions.calcMatrixFormers(nPadeTerms, padeTerms, k0, k0SqdeltazSqOver2, deltar(0), matrixFormers);
    SetLength(diagTerms, length(matrixFormers));

    attCConst := 1/(20*log10(exp(1))/f); //convert to nepers/m    eq 6.156 (lambda = c / f and we include c(z) later)

    setlength(C,max(1,nPadeTerms),nz);
    setLength(B,max(1,nPadeTerms),nz);
    SetLength(rhs,nz);
    SetLength(tmpCUpper,nz);
    SetLength(tmpRhs,nz);
    SetLength(psiComplexVectPlusOneThisTerm,nz);
    setlength(deltaThisTerm,nPadeTerms,nz);

    setlength(psiM,dMax,nrInit);
    setlength(result,dMax,nrInit);

    irAtStarter := 0;

    //{$DEFINE DIRECTIVITYSTARTER}
    //{$DEFINE THOMPSONSTARTER}
    {$IF defined (DEBUG) and defined (DIRECTIVITYSTARTER)}
    //todo what about scaling for this?
    TSolversGeneralFunctions.PEStarterWithDirectivity(f,k0,srcZ,deltaz,nz,directivities,psiComplexVect,directivityStarterScaling);
    {$ELSEIF defined (DEBUG) and defined (THOMPSONSTARTER)}
    psiComplexVect := TSolversGeneralFunctions.PEStarterThompson(k0,srcZ,pi/2/3/4,deltaz,nz);
    {$ELSE}
    directivityStarterScaling := 1; //implicitly 0 dB for non-directional starter

    //see if we can use the modal starter: if all the starterPsi = 0 we can't use it. If any are non-nan then we can
    bUseModalStarter := false;
    for iz := 0 to length(starter.psi) - 1 do
    begin
        if not (starter.psi[iz] = NullComplex) then
        begin
            bUseModalStarter := true;
            break;
        end;
    end;

    //testing example, limiting starter to only the source depth
    //for iz := 0 to length(starter.psi) - 1 do
    //    if iz <> round(srcZ / deltaz) then
    //        starter.psi[iz] := nullcomplex;

    if starter.isOK and bUseModalStarter then
    begin
        //TSolversGeneralFunctions.applyDirectivityToStarter(f,k0,srcZ,deltaz,nz,directivities,starter);
        psiComplexVect := TSolversGeneralFunctions.PEStarterInterpolation(deltaz,nz,starter.z,starter.psi);
        irAtStarter := max(0, round(starterR / deltar(0))); //should still normally be 0, unless we have a large rOverSamp. dBSeaModes doesn't range oversample
    end
    else if (srcZ < 0) or (srcZ > nz*deltaz)  then
    begin
        include(solverErrors,kSourceBelowSubDepth);
        exit;
    end
    else
    begin
        include(solverErrors,kFallbackStarterUsed);

        //analytical fallback starter
        //psiComplexVect := TSolversGeneralFunctions.PEStarterGeneralisedGaussian(k0,srcZ,halfWidth,changeAngleRadians,deltaz,nz);
        //psiComplexVect := TSolversGeneralFunctions.PEStarterGaussian(k0,srcZ,deltaz,nz);
        psiComplexVect := TSolversGeneralFunctions.PEStarterGreene(k0,srcZ,deltaz,nz);
        starterSum := ComplexVectModulusSum(psiComplexVect);
        {normalise the starter and correct for the size of deltaz
        directivityStarterScaling is needed as the overall directivity sum <> 0 dB}
        for iz := 0 to nz - 1 do
            psiComplexVect[iz] := psiComplexVect[iz] * 4 / deltaz / starterSum * directivityStarterScaling;
    end;
    {$ENDIF}

    {sediment layers}
    //TODO support for changing seafloor scheme in range
    zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,r[irAtStarter]);
    zbPrev := nan;
    seafloor := seafloors[0].seafloor;
    prevSeafloorIndex := 0;
    seafloor.getLayerDensityList(zbThis, water.rho_g, layerDensityList); {includes a setlength call} //returned values are in g/cm3

    for iz := 0 to nz - 1 do
    begin
        {copy over to the output matrix. If starterR is large, we may need to copy the starter to multiple ranges}
        for ir := 0 to irAtStarter div rOverSamp do
        begin
            if iz = 1 then
                psiM[1, ir] := psiComplexVect[iz] //shift down by 1 due to pressure release surface
            else if (iz > 0) and ((iz mod zOversamp) = 0) and (iz div zOversamp < dMax) then
                psiM[iz div zOversamp, ir] := psiComplexVect[iz];
        end;

        {reduce the psi field by sqrt(c*rho) - or the part we are calculating with (psiComplexVect) at least.
        This ensures conservation of energy, on upward/downward sloping zb problems.
        We need to later on put the reduction factor back in to retrieve psi. see sec 6.8 p508}
        TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList, z(iz), k0, k02, rho_ir, q1);
        psiComplexVect[iz] := psiComplexVect[iz] / sqrt(localC(iz, seafloor, zbThis)*rho_ir);
    end;

    c_ir := 1500; // stop compiler warning
    THelper.RemoveWarning(c_ir);
    for iz := 0 to nz - 1 do
        for k := 0 to max(1,nPadeTerms) - 1 do
        begin
            B[k].lower[iz] := 1;   //6.189
            C[k].lower[iz] := matrixFormers[k].w2Overwstar2; //6.189
        end;

    nilSedimentError := false;
    //-------------START RANGE LOOPING-------------------------
    for ir := irAtStarter to nr - 2 do
    begin
        TSolversGeneralFunctions.calcMatrixFormers(nPadeTerms, padeTerms, k0, k0SqdeltazSqOver2, deltar(ir), matrixFormers);
        //calcMatrixFormers(deltar(ir));

        depthVal := aCProfile3.depthValForRange(r[ir], SSPRangeInd);
        seafloor := seafloors.seafloorForRange(r[ir], seafloorIndex);
        if (zbThis <> zbPrev) or (prevSSPRangeInd <> SSPRangeInd) or (seafloorIndex <> prevSeafloorIndex) then  {new depth profile - also fires at ir = 0}
        begin
            prevSeafloorIndex := seafloorIndex;
            prevSSPRangeInd := SSPRangeInd;

            for iz := 0 to nz - 1 do
            begin
                c1[iz] := TSolversGeneralFunctions.interpolationFunPE(depthVal,z(iz));

                if z(iz) < zbThis then
                begin
                    c_ir := c1[iz];         //ssp c
                    atten := 0; //water attenuation (todo do bulk attenuation here)
                end
                else
                begin
                    aMaterial := seafloor.getMaterialAtDepthBelowSurface(z(iz) , zbThis); //could move this inside checks for seafloor
                    if not assigned(aMaterial) then
                    begin
                        nilSedimentError := true;
                        aMaterial := water;
                    end;

                    c_ir := aMaterial.c;       //sediment c
                    atten := aMaterial.attenuation*attCConst/c_ir;  //sediment attenuation, converted to Nepers/m  eq 6.156

                    if z(iz) > SubDepth1 then
                        atten := atten + TSolversGeneralFunctions.additionalAttenuationInBottomLayer(z(iz),subDepth1,subDepth2);//increase attenuation at the bottom of the sediment layer
                end;

                {check if close enough to the bottom to consider calculating the
                density difference function (function is 0 for constant density)}

                //get rho, check if we are near an interface and so need to smooth rho
                TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList,
                                                                 z(iz),  //this depth
                                                                 k0,
                                                                 k02,
                                                                 rhoThis, //rho gets set
                                                                 q1); //q1 gets set per eq 6.152

                nSqr := sqr(c0) / sqr(c_ir); //nsquared, not n. 6.158, real part
                nSqrTilde := nSqr + q1; // eq 6.152

                {$IF defined (DEBUG) and defined (TEST_N)}
                if ir = irAtStarter then
                    s := s + inttostr(iz) + #09 +
                        floattostr(rhoThis) + #09 +
                        floattostr(q1) + #09 +
                        FloatToStr(nSqr) + #09 +
                        FloatToStr(nSqrTilde) + #13#10;
                {$ENDIF}

                nSq2 := MakeComplex(nSqrTilde,nSqrTilde*atten); //should the attenuation be multiplied by nSqr or nSqrTilde?
                if iz = 0 then
                begin
                    rhoPrev := rhoThis;
                    nSq1 := nSq2;
                end;

                //eq 6.189
                //diagonals C -> u, B -> uHat * w2/wstar2
                //lower C -> 1, B -> 1 * w2 / wstar2
                //upper C -> v, B -> v * w2 / wstar2
                for k := 0 to max(1,nPadeTerms) - 1 do
                begin
                    diagTerms[k] := calcDiagonalTerms(matrixFormers[k]); //calculates u, uHat and v
                    C[k].diag[iz]   := diagTerms[k].u;
                    B[k].diag[iz]   := diagTerms[k].uHat * matrixFormers[k].w2Overwstar2;
                    C[k].lower[iz]  := 1;
                    B[k].lower[iz]  := matrixFormers[k].w2Overwstar2;
                    C[k].upper[iz]  := diagTerms[k].v;
                    B[k].upper[iz]  := diagTerms[k].v * matrixFormers[k].w2Overwstar2;
                end;

                rhoPrev := rhoThis;
                nSq1 := nSq2;
            end; //iz
        end; //new profile

        //--------------------------------Matrix algebra------------------------------
        solverStep(B,C,psiComplexVect);
        //psiComplexVect is now for ir+1

        zbPrev := zbThis;
        zbThis := TSolversGeneralFunctions.interpolationFunPE(aZProfile2,r[ir + 1]);

        if not isnan(zbThis) and (zbThis <> zbPrev) then
            seafloor.getLayerDensityList(zbThis, water.rho_g, layerDensityList);

        {put the outgoing psi vector into our psiM matrix, un-reducing by sqrt(c*rho)}
        if ((ir + 1) mod rOversamp) = 0 then
        begin
            irr := (ir + 1) div rOversamp;

            if not irNanBegins.HasValue and isnan(zbThis) then
            begin
                irNanBegins := irr;    
                break; //STOP RANGE LOOPING
            end;

            for izz := 0 to dMax - 1 do
            begin
                iz := ifthen(izz = 0, 1, izz * zOverSamp); //shift down by 1, as we force psiComplexVect[0] = 0
                if iz >= nz then break;
                TSolversGeneralFunctions.depthIsCloseToInterface(layerDensityList,z(iz),k0,k02,rho_ir,q1);
                psiM[izz, irr] := psiComplexVect[iz] * sqrt(localC(iz, seafloor, zbThis)*rho_ir); //un-reduce by sqrt(c*rho) see sec 6.8 p508
            end;
        end;

        //UI feedback
        //if Assigned(updateLabelProc) and (ir mod 1000 = 0) then updateLabelProc('Marching solution in range. Step ' + tostr(ir) + ' of ' + tostr(nr));
        if (pcancel <> nil) and pcancel.b then exit;
        fractionDone.d := ir / nr;
    end; //ir

    if nilSedimentError then
        include(solverErrors,kNoSediment);

    {put the abs(hankel function) back into the envelope function to get the pressure.
    6.5 gives Ho(1)(k0r) = sqrt(2/(pi*k0*r)) exp(i *(k0r - pi/4))
    we can ignore the complex exponential as we just want magnitude.
    so squaring the magnitude part of 6.5 we have 2/(pi*k0*r). NB k0r >> 1 for this approximation}

    {if we have a modal starterR, the starter was calculated at a distance out from the source or starterR.
    We want our hankel func to pass through 1 at range starterR (and be == 1 before that). For the fallback starters, use a normal hankel func}
    if starter.isOK then
        fac := starterR
    else
        fac := 2/(pi*k0);

    for ir := 0 to nrInit - 1 do
    begin
        if irNanBegins.HasValue and (ir >= irNanBegins.val) then
        begin
            for iz := 0 to dMax - 1 do
                result[iz,ir] := nan;
            Continue;
        end;            
    
        if starter.isOK and (r[ir*rOverSamp] <= starterR) then
            dhank2 := 1
        else
            dhank2 := fac / r[ir*rOverSamp];

        for iz := 0 to dMax - 1 do
        begin
            dpress2 := psiM[iz,ir].modulusSquared * dhank2; {eq 6.3 squared}
            if (dpress2 > 0) then
                result[iz,ir] := -10*log10(dpress2);
        end;
    end;

    {$IF defined (DEBUG) and (defined (TESTPE) or Defined(TEST_N))}
    THelper.setClipboard(s);
    {$ENDIF}
end;


end.

