unit dBSeaSolversThread;

interface

uses
    Classes, sysutils, baseTypes,
    dBSeaPE1, Mcomplex, solvePackage, dBSeaObject,
    generics.collections, math;

  type
  TdBSeaSolversThread = class(TThread)
  private
    fPackage : TSolvePackage;
  protected
    procedure Execute; override;
    procedure DoTerminate; override;
  public
    parent              : TObject;
    dBSeaPESolver       : TdBSeaPE1Solver;

    constructor Create(const aParent : TObject; const aPackage : TSolvePackage); overload;
    destructor Destroy; override;

    property  package : TSolvePackage read fPackage;
  end;

  TdBSeaSolversThreadList = TObjectList<TdBSeaSolversThread>;

implementation

uses soundSource;

constructor TdBSeaSolversThread.Create(const aParent : TObject; const aPackage : TSolvePackage);
begin
    inherited Create(true);
    FreeOnTerminate := true;

    if not (assigned(aParent) and (aParent is TSource)) then
        raise EIncorrectParent.Create('3216: Incorrect parent for TdBSeaSolversThread');
    self.parent := aParent;

    fPackage := aPackage;
    fPackage.assignedToThread := true;
    dBSeaPESolver := TdBSeaPE1Solver.Create;
end;

destructor TdBSeaSolversThread.Destroy;
begin
    dBSeaPESolver.Free;

    inherited;
end;

procedure TdBSeaSolversThread.DoTerminate;
begin
    inherited;

    //send message back to parent that we are done
    //TSource(parent).onSolveThreadFinish(self); //this also destroys this thread
end;

procedure TdBSeaSolversThread.Execute;
var
    tl : TMatrix;
    errorMessages : String;
begin
    if not assigned(package) then exit;

    tl := dBSeaPESolver.main(package.f,
                       package.z,
                       package.rmax,
                       package.dr,
                       package.zmax,
                       package.dmax,
                       package.zProfile,
                       package.cProfile,
                       package.modalStarterZ,
                       package.modalStarterPsi,
                       package.seafloorScheme,
                       package.cancel,
                       package.pcLastlblSolveExtraInfoLastUpdated,
                       package.plblSolveExtraInfo,
                       package.zOversampling,
                       package.rOversampling,
                       errorMessages);
    //then DoTerminate
end;

end.
