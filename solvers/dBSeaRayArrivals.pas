unit dBSeaRayArrivals;

interface

uses
    classes, math, Mcomplex, dialogs, ClipBrd, sysutils, types,
    mmsystem, basetypes, generics.Collections, timeseries,
    helperFunctions;

type

  TArrival = class(TObject)
  public
      amplitudes: TDoubleDynArray; //ray amplitudes at this point, for each freq
      sample: integer;  //delay from t = 0, in samples
      constructor Create(const aSample: integer; const nf: integer);
      function  scaledSample(const sampleScaling: integer = 1): integer;
  end;
  TArrivalsList = TObjectList<TArrival>;

  TArrivalsField = class(TObject)
  private
    procedure clearField;
    procedure createField(const nx, ny, nz: integer);
    //function  timeSeries(const fi, ix, iy, iz: integer): TDoubleDynArray;
  public
  var
    lowAmpCutoff: double;
    arrivals: array of array of array of TArrivalsList;
    fs: double;
    nf: integer;

    constructor Create(const anf, anx, any, anz: integer; const afs: double);
    destructor  Destroy; override;

    procedure   reinitialise(const anf, anx, any, anz: integer; const afs: double);
    procedure   addArrival(const fi, ix, iy, iz: integer; const aAmplitude, aDelay: double);
    //function    arrivalsList(const fi, iz, ir: integer): TMatrix;
    //function    minMaxArrival(const fi, iz, ir: integer): string;

    //procedure   bandTimeSeries(const fi, ix, iy, iz: integer; const bw: TBandwidth; const timeSeries: TTimeSeries);
    //function    directConvolve(const fi, iz, ir: integer; const input: TDoubleDynArray): TDoubleDynArray;
    function    sparseDirectConvolve(const fi, ix, iy, iz: integer; const sampleList: TSparseSampleList; const sampleScale: integer): TDoubleDynArray;
    procedure   verySparseFFT(const fi, ix, iy, iz: integer; var re,im: TDoubleDynArray);

    procedure   appendToAnotherList(const destination: TArrivalsField);
  end;

implementation

constructor TArrival.Create(const aSample: integer; const nf: integer);
begin
    inherited Create;

    if aSample > 10000*44100 then
        raise Exception.Create('error more than 10000 seconds');

    sample := aSample;
    setlength(amplitudes,nf);
end;


constructor TArrivalsField.Create(const anf, anx, any, anz: integer; const afs: double);
begin
    inherited Create;
    fs := afs;
    nf := anf;
    createField(anx,any,anz);
    lowAmpCutoff := 0.1;
end;


destructor TArrivalsField.Destroy;
begin
    clearField;

    inherited;
end;

procedure TArrivalsField.createField(const nx, ny, nz: integer);
var
    ix,iy,iz: integer;
begin
    setlength(arrivals,nx,ny,nz);
    for ix := 0 to nx - 1 do
        for iy := 0 to ny - 1 do
            for iz := 0 to nz - 1 do
                arrivals[ix,iy,iz] := TArrivalsList.create(true);
end;

procedure TArrivalsField.clearField;
var
    ix,iy,iz: integer;
begin
    if length(arrivals) > 0 then
        for ix := 0 to length(arrivals) - 1 do
            for iy := 0 to length(arrivals[0]) - 1 do
                for iz := 0 to length(arrivals[0,0]) - 1 do
                begin
                    arrivals[ix,iy,iz].Clear;
                    arrivals[ix,iy,iz].Free;
                end;
end;


procedure TArrivalsField.reinitialise(const anf, anx, any, anz: integer; const afs: double);
begin
    clearField;
    fs := afs;
    nf := anf;
    createField(anx,any,anz);
end;

procedure   TArrivalsField.addArrival(const fi, ix, iy, iz: integer; const aAmplitude, aDelay: double);
var
    arrival: TArrival;
    aSample: integer;
begin
    aSample := saferound(aDelay * fs);
    for arrival in arrivals[ix,iy,iz] do
        if aSample = arrival.sample then
        begin
            //arrival already exists at this sample, add to it
            arrival.amplitudes[fi] := arrival.amplitudes[fi] + aAmplitude;
            exit;
        end;

    //create a new arrival
    arrival := TArrival.Create(aSample,nf);
    arrival.amplitudes[fi] := aAmplitude;
    arrivals[ix, iy, iz].add(arrival);
end;

//function    TArrivalsField.timeSeries(const fi, ix, iy, iz: integer): TDoubleDynArray;
//begin
//    result := self.directConvolve(fi,ix, iy, iz,[1]);
//end;

//function    TArrivalsField.arrivalsList(const fi, iz, ir : integer) : TMatrix;
//var
//    arrival : TArrival;
//begin
//    setlength(result,2,arrivals[fi,iz,ir].Count);
//    for arrival in arrivals[fi,iz,ir] do
//    begin
//        result[0,arrivals[fi,iz,ir].indexOf(arrival)] := arrival.amplitude;
//        result[1,arrivals[fi,iz,ir].indexOf(arrival)] := arrival.sample;
//    end;
//end;

//function    TArrivalsField.directConvolve(const fi, iz, ir : integer; const input : TDoubleDynArray) : TDoubleDynArray;
////it's ok to direct convolve, because the arrivals are so sparse
//var
//    i : integer;
//    minSample, maxSample : integer;
//    arrival : TArrival;
//    N : integer;
//    j : integer;
//    maxAmp : double;
//begin
//    setlength(result,0);
//    if fi >= nf then exit;
//    if iz > length(arrivals) then exit;
//    if ir > length(arrivals[0]) then exit;
//    if arrivals[iz,ir].Count = 0 then exit;
//
//    arrival := arrivals[iz,ir].First;
//
//    maxAmp := arrival.amplitudes[fi];
//    minSample := arrival.sample;
//    maxSample := arrival.sample;
//
//    for arrival in arrivals[iz,ir] do
//    begin
//        if arrival.amplitudes[fi] > maxAmp then
//            maxAmp := arrival.amplitudes[fi];
//        if arrival.sample < minSample then
//            minSample := arrival.sample;
//        if arrival.sample > maxSample then
//            maxSample := arrival.sample;
//    end;
//
//    N := maxSample - minSample + 1;
//
//    setlength(result, N + length(input) - 1);
//    for arrival in arrivals[iz,ir] do
//        if arrival.amplitudes[fi] > maxAmp * lowAmpCutoff then
//        begin
//            j := arrival.sample - minSample;
//            for i := 0 to length(input) - 1 do
//                result[i + j] := result[i + j] + arrival.amplitudes[fi] * input[i];
//        end;
//end;

function    TArrivalsField.sparseDirectConvolve(const fi, ix, iy, iz: integer; const sampleList: TSparseSampleList; const sampleScale: integer): TDoubleDynArray;
var
    minSample, maxSample, N,j: integer;
    arrival: TArrival;
    maxAmp: double;
    sparseSample: TSparseSample;
    aList: TArrivalsList;
begin
    setlength(result,0);
    if fi >= nf then exit;
    if ix > length(arrivals) then exit;
    if iy > length(arrivals[0]) then exit;
    if iz > length(arrivals[0,0]) then exit;
    if arrivals[ix,iy,iz].Count = 0 then exit;

    aList := arrivals[ix,iy,iz];
    arrival := aList.First;

    maxAmp := arrival.amplitudes[fi];
    minSample := arrival.scaledSample(sampleScale);
    maxSample := arrival.scaledSample(sampleScale);

    for arrival in aList do
    begin
        if arrival.amplitudes[fi] > maxAmp then
            maxAmp := arrival.amplitudes[fi];
        if arrival.scaledSample(sampleScale) < minSample then
            minSample := arrival.scaledSample(sampleScale);
        if arrival.scaledSample(sampleScale) > maxSample then
            maxSample := arrival.scaledSample(sampleScale);
    end;

    N := maxSample - minSample + 1;

    if sampleList.count = 0 then
        setlength(result,0)
    else
    begin
        sparseSample := sampleList.Last;
        setlength(result, N + sparseSample.sample);
        for arrival in aList do
            if arrival.amplitudes[fi] > (maxAmp * lowAmpCutoff) then
            begin
                j := arrival.scaledSample(sampleScale) - minSample;
                for sparseSample in sampleList do
                    if not isnan(sparseSample.amplitude) then
                        result[sparseSample.sample + j] := result[sparseSample.sample + j] + (arrival.amplitudes[fi] * sparseSample.amplitude);
            end;
    end;
end;


//procedure   TArrivalsField.bandTimeSeries(const fi, ix, iy, iz: integer; const bw: TBandwidth; const timeSeries: TTimeSeries);
////it's ok to direct convolve, because the arrivals are so sparse
//var
//    minSample, maxSample : integer;
//    arrival : TArrival;
//    maxAmp : double;
//begin
//    if fi >= nf then
//        exit;
//
//    if ix > length(arrivals) then
//        exit
//    else if iy > length(arrivals[0]) then
//        exit
//    else if iz > length(arrivals[0,0]) then
//        exit;
//
//    if arrivals[ix,iy,iz].Count = 0 then
//        exit;
//
//    arrival := arrivals[ix,iy,iz].First;
//    maxAmp := arrival.amplitudes[fi];
//    minSample := arrival.sample;
//    maxSample := arrival.sample;
//
//    for arrival in arrivals[ix,iy,iz] do
//    begin
//        if arrival.amplitudes[fi] > maxAmp then
//            maxAmp := arrival.amplitudes[fi];
//        if arrival.sample < minSample then
//            minSample := arrival.sample;
//        if arrival.sample > maxSample then
//            maxSample := arrival.sample;
//    end;
//
//    timeSeries.fs := fs;
//    setlength(timeSeries.samples,maxSample - minSample + 1);
//    for arrival in arrivals[ix,iy,iz] do
//        if arrival.amplitudes[fi] > maxAmp * lowAmpCutoff then
//            timeSeries.samples[arrival.sample - minSample] := arrival.amplitudes[fi];
//end;

procedure TArrivalsField.verySparseFFT(const fi, ix, iy, iz: integer; var re,im: TDoubleDynArray);
var
    minSample, maxSample: integer;
    arrival: TArrival;
    w: double;
    N: integer;
    j, k: integer;
    aList: TArrivalsList;
    cosA, sinA: double;
begin
    aList := arrivals[ix,iy,iz];
    arrival := aList.First;
    minSample := arrival.sample;
    maxSample := arrival.sample;

    for arrival in aList do
    begin
        if arrival.sample < minSample then
            minSample := arrival.sample;
        if arrival.sample > maxSample then
            maxSample := arrival.sample;
    end;

    N := maxSample - minSample + 1;
    if N mod 2 = 1 then
        N := N + 1;
    setlength(re,N); //nextPow2
    setlength(im,N); //nextPow2

    w := -2*pi/N;
    for arrival in aList do
    begin
        j := arrival.sample - minSample;
        for k := 0 to N div 2 do
        begin
            system.SineCosine(j*k*w, sinA, cosA);
            re[k] := re[k] + cosA * arrival.amplitudes[fi];
            im[k] := im[k] + sinA * arrival.amplitudes[fi];
        end;

        //mirror about nyquist
        for k := N div 2 + 1 to N - 1 do
        begin
            re[k] := re[N - k];
            im[k] := im[N - k];
        end;
    end;
    //if N > 20 then
    //    showmessage('2');
end;

procedure   TArrivalsField.appendToAnotherList(const destination: TArrivalsField);
var
    ix, iy, iz, fi : integer;
    delay : double;
    arrival : TArrival;
begin
    if (self.fs <> destination.fs) or (self.nf <> destination.nf) then exit;
    if (length(self.arrivals) = 0) or (length(destination.arrivals) = 0) then exit;
    if (length(self.arrivals) <> length(destination.arrivals)) then exit;
    if (length(self.arrivals[0]) <> length(destination.arrivals[0])) then exit;
    if (length(self.arrivals[0,0]) <> length(destination.arrivals[0,0])) then exit;

    for ix := 0 to length(self.arrivals) - 1 do
        for iy := 0 to length(self.arrivals[0]) - 1 do
            for iz := 0 to length(self.arrivals[0,0]) - 1 do
                for arrival in self.arrivals[ix,iy,iz] do
                begin
                    delay := arrival.sample / self.fs;
                    for fi := 0 to length(arrival.amplitudes) do
                        destination.addArrival(fi,ix,iy,iz,arrival.amplitudes[fi],delay);
                end;
end;

function TArrival.scaledSample(const sampleScaling: integer): integer;
begin
    result := self.sample div sampleScaling;
end;

end.
