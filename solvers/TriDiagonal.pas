unit TriDiagonal;

interface
uses system.math, mcomplex;

type
    TTriInd = (kLo=-1,kDi=0,kUp=1);

    TTriComplex = array[TTriInd] of TComplex;
    TTriDiagonal = array of TTriComplex;
    TTriDiagonalHelper = record helper for TTriDiagonal
    private
        function  getJ(const i,n: integer; const j: TTriInd): TTriInd;
        function  get1OrdDivisor(const i,n: integer): integer;
        function  get1OrdInternal(const i,n: integer; const j: TTriInd): TComplex;
        procedure add1OrdInternal(const i,n: integer; const j: TTriInd; const val: TComplex; const dz: double);

        function  getLower(i: integer): Tcomplex;
        procedure setLower(i: integer; val: TComplex);
        function  getDiag (i: integer): TComplex;
        procedure setDiag (i: integer; val: TComplex);
        function  getUpper(i: integer): TComplex;
        procedure setUpper(i: integer; const Val: TComplex);
    public
        procedure add2Ord(const i: integer; const val: TComplex; const dz: double);
        procedure add1Ord(const i, n: integer; const val: TComplex; const dz: double);
        procedure add0Ord(const i: integer; const val: TComplex);  overload;
        procedure add0Ord(const i: integer; const val: double);  overload;
        procedure multByInPlace(const c: TComplex; const result: TTriDiagonal);

        function  invert(): TComplexMat;

        property  lower[i: integer]: TComplex read getLower write setLower;
        property  diag [i: integer]: TComplex read getDiag  write setDiag ;
        property  upper[i: integer]: TComplex read getUpper write setUpper;
    end;
    TTriDiagonalArray = array of TTriDiagonal;


implementation


function TTriDiagonalHelper.get1OrdInternal(const i ,n: integer; const j: TTriInd): TComplex;
begin
    result := self[i,getJ(i,n,j)];
end;

function TTriDiagonalHelper.getDiag(i: integer): TComplex;
begin
    result := self[i,kDi];
end;

function TTriDiagonalHelper.get1OrdDivisor(const i, n: integer): integer;
begin
    result := ifthen((i = 0) or (i = n - 1),1,2);
end;

function TTriDiagonalHelper.getJ(const i, n: integer; const j: TTriInd): TTriInd;
begin
    if (i = 0) and (j = kLo) then
        result := kDi
    else if (i = n - 1) and (j = kUp) then
        result := kDi
    else
        result := j;
end;

function TTriDiagonalHelper.getLower(i: integer): Tcomplex;
begin
    result := self[i,kLo];
end;

function TTriDiagonalHelper.getUpper(i: integer): TComplex;
begin
    result := self[i,kUp];
end;

function TTriDiagonalHelper.invert: TComplexMat;
var
    theta, phi: TComplexVect;
begin
    setlength(result, length(self), length(self));

    setlength(theta, length(self));
    setlength(phi, length(self));

    theta[0] := 1;
    theta[1] := diag[0];
end;

procedure TTriDiagonalHelper.multByInPlace(const c: TComplex; const result: TTriDiagonal);
var
    i: integer;
begin
    for I := 0 to length(self) - 1 do
    begin
        result.lower[i] := self.lower[i] * c;
        result.diag [i] := self.diag [i] * c;
        result.upper[i] := self.upper[i] * c;
    end;
end;

procedure TTriDiagonalHelper.add0Ord(const i: integer; const val: TComplex);
begin
    self.diag[i] := self.diag[i] + val;
end;

procedure TTriDiagonalHelper.add0Ord(const i: integer; const val: double);
begin
    self.add0Ord(i, val);
end;

procedure TTriDiagonalHelper.add1Ord(const i, n: integer; const val: TComplex; const dz: double);
begin
    self.add1OrdInternal(i,n,kLo,self.get1OrdInternal(i,n,kLo) -val, dz);
    self.add1OrdInternal(i,n,kUp,self.get1OrdInternal(i,n,kUp) +val, dz);
end;

procedure TTriDiagonalHelper.add1OrdInternal(const i,n: integer; const j: TTriInd; const val: TComplex; const dz: double);
begin
    self[i,getj(i,n,j)] := self[i,getj(i,n,j)] + val / get1OrdDivisor(i,n) / dz;
end;

procedure TTriDiagonalHelper.add2Ord(const i: integer; const val: TComplex; const dz: double);
begin
    lower[i] := lower[i] +  val / dz / dz;
    diag [i] := diag [i] -2*val / dz / dz;
    upper[i] := upper[i] +  val / dz / dz;
end;

procedure TTriDiagonalHelper.setDiag(i: integer; val: TComplex);
begin
    self[i,kDi] := val;
end;

procedure TTriDiagonalHelper.setLower(i: integer; val: TComplex);
begin
    self[i,kLo] := val;
end;

procedure TTriDiagonalHelper.setUpper(i: integer; const Val: TComplex);
begin
    self[i,kUp] := val;
end;

end.
