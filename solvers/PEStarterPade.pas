unit PEStarterPade;

interface
uses system.math, system.types,
    uTExtendedX87, Complex80, MMatrix80;

type

    TExtendedX87Array = array of TExtendedX87;

    TPEStarterPadeExtended = class(TObject)
    private
        class function  ff(x,k0r0: TExtendedX87): TComplex80;
        class function  gg(x: TExtendedX87; p,q: TComplex80Vect; mm,nn: integer): TComplex80;
        class procedure reduceConditionNumber(var A,b: TComplexMatrix);
    public
        class procedure testing(var x: TExtendedX87Array; var y,z: TComplex80Vect);
        class procedure calcPadeTerms(const f0, c0, r0: double; var p,q: TComplex80Vect);
    end;

implementation


class procedure TPEStarterPadeExtended.calcPadeTerms(const f0, c0, r0: double; var p, q: TComplex80Vect);
const
    nAccuracy = 30;
    nStability = 10;
    mm = 12; //%numerator order (1 + A1x + A2x^2)
    nn = mm; //%denominator order (1 + B1x + B2x^2)
var
    r00: TExtendedX87;
    i,j,nConstraints: integer;
    w0, k0, k0r0, qq: TExtendedX87;
    fx: TComplex80;
    xs: TDoubleDynArray;
    ybar,B,Bt,BtB,BtY,L,U,yy,abar: TComplexMatrix;
begin
    r00 := r0;

    w0 := 2*pi*f0;
    k0 := w0 / c0;
    k0r0 := k0*r00; // normally 2pi

    nConstraints := nAccuracy + nStability;
    //%-2 to -1.1 stability (evanescent), -0.7 to 0 accuracy (exclude 0 to avoid 0 row in B matrix)
    setlength(xs,nConstraints);
    for i := 0 to nConstraints - 1 do
    begin
        if i < nStability then
            xs[i] := -2 + 0.9 * i / (nStability - 1)
        else
            xs[i] := -0.7 + 0.7 * (i - nStability) / (nAccuracy);
    end;

    //Cederberg-Collins
    //eq 19: 1 + sum(ax^j) = fx(1 + sum(bx^j))
    //       1 + sum(ax^j) = fx + fx*sum(bx^j)
    //       1 - fx        = fx*sum(bx^j) - sum(ax^j)
    //   lhs -> ybar, rhs -> B

    B := TComplexMatrix.create(mm+nn,nConstraints);
    ybar := TComplexMatrix.create(1,nConstraints);
    for i := 0 to nConstraints - 1 do
    begin
        fx := ff(xs[i], k0r0);
        for j := 1 to mm do
        begin
            qq := power(xs[i],j);
            B[j,i+1] := makeComplex80(-qq, 0);
            B[mm+j,i+1] := fx * qq;
        end;
        ybar[1,i+1] := 1 - fx;
    end;

    //setClipboard(ybar.toString(1) + #13#10#13#10 + ybar.toString(2));

    Bt := TComplexMatrix.create(B);
    Bt.CopyFromMatrix(B);
    Bt.transposition;

    BtB := mulMatrix(Bt,B);
    BtY := mulMatrix(Bt,ybar);

    //%%Bt*y = (Bt*B)*a
    //%%BtY = BtB*a
    //%abar = (Bt*B)\(Bt*ybar);
    //%abar = BtB\BtY;

    //reduceConditionNumber(BtB,BtY);

    L := TComplexMatrix.create(BtB);
    U := TComplexMatrix.create(BtB);
    yy := TComplexMatrix.create(1,nn+mm);
    abar := TComplexMatrix.create(1,nn+mm);
    BtB.LUDecompose(L,U);
    //[L,U,P] = lu(BtB);
    L.SolveMatrixL(BtY,yy);

    //setClipboard(BtB.toString(1) + #13#10#13#10 + BtB.toString(2));

    //yy := L\(P*(BtY));
    //[U,yy] = reduceConditionNumber(U,yy);
    //abar = U\yy;
    U.SolveMatrixU(yy,abar);

    setlength(p,mm);
    setlength(q,nn);
    for i := 0 to mm - 1 do
        p[i] := abar[1,i+1];
    for i := 0 to nn - 1 do
        q[i] := abar[1,mm+i+1];

        //NB not giving correct results???

    //for k0r0 = 2pi, why not just hard code it
    p[0] := makeComplex80(2.6142154,-1.0095536);
    p[1] := makeComplex80(0.8640949,-2.3729225);
    p[2] := makeComplex80(-2.4471532,-1.3775903);
    p[3] := makeComplex80(-1.7411656,0.1362885);
    p[4] := makeComplex80(0.2600628,0.0883599);
    p[5] := makeComplex80(0.3580331,0.0581497);
    p[6] := makeComplex80(0.1175685,0.1192633);
    p[7] := makeComplex80(0.0693036,-0.003156);
    p[8] := makeComplex80(-0.0291378,0.0011882);
    p[9] := makeComplex80(-0.0488109,0.0035973);
    p[10] := makeComplex80(-0.0156906,0.0003125);
    p[11] := makeComplex80(-0.001622,-0.0000696);

    q[0] := makeComplex80(0.86418,-4.15114);
    q[1] := makeComplex80(-9.41307,-2.53628);
    q[2] := makeComplex80(-3.62274,14.85847);
    q[3] := makeComplex80(17.50913,1.65934);
    q[4] := makeComplex80(-3.13848,-18.63963);
    q[5] := makeComplex80(-12.3547,-2.88259);
    q[6] := makeComplex80(9.91544,-15.54724);
    q[7] := makeComplex80(12.91562,-30.51484);
    q[8] := makeComplex80(17.04837,-15.39923);
    q[9] := makeComplex80(11.94261,-4.77332);
    q[10] := makeComplex80(0.49202,-0.72011);
    q[11] := makeComplex80(-0.45266,-0.54683);
end;

class function TPEStarterPadeExtended.ff(x,k0r0: TExtendedX87): TComplex80;
var
    q,z: TComplex80;
begin
    q := makeComplex80(1+x,0);
    z := k0r0*(-1 + complex80SQRT(q));
    z := z.mulByI;
    result := c80power(q,7/4)*complex80EXP(z);

    //%y = power(1+x,7/4);
    //%y = exp(sqrt(-1)*k0r0*(-1+sqrt(1+x)));
    //%y = power(1+x,1/4);
end;

class function TPEStarterPadeExtended.gg(x: TExtendedX87; p,q: TComplex80Vect; mm,nn: integer): TComplex80;
var
    num, den: TComplex80;
    j: integer;
    xx: double;
begin
    xx := x;

    num := makeComplex80(1,0);
    for j := 0 to mm - 1 do
        num := num + p[j]*makeComplex80(system.math.power(xx,j+1),0);

    den := makeComplex80(1,0);
    for j := 0 to nn - 1 do
        den := den + q[j]*makeComplex80(system.math.power(xx,j+1),0);

    result := num / den;
end;

class procedure TPEStarterPadeExtended.reduceConditionNumber(var A,b: TComplexMatrix);
var
    i,j: integer;
    rowMax: TComplex80;
begin
    for i := 0 to A.RowCount - 1 do
    begin
        rowMax := NullComplex80;
        for j := 0 to A.ColCount - 1 do
            if A[j+1,i+1].modulus > rowMax.modulus then
                rowMax := A[j+1,i+1];

        for j := 0 to A.ColCount - 1 do
            A[j+1,i+1] := A[j+1,i+1] / rowMax;

        b[1,i+1] := b[1,i+1] / rowMax;
    end;
end;

class procedure TPEStarterPadeExtended.testing(var x: TExtendedX87Array; var y,z: TComplex80Vect);
const
    f0 = 35.0;
    c0 = 1500.0;
    n = 1000;
    nAccuracy = 30;
    nStability = 10;
    mm = 12; //%numerator order (1 + A1x + A2x^2)
    nn = mm; //%denominator order (1 + B1x + B2x^2)
var
    r0: TExtendedX87;
    i,j,nConstraints: integer;
    w0, k0, k0r0, qq: TExtendedX87;
    fx: TComplex80;
    xs: TDoubleDynArray;
    ys,p,q: TComplex80Vect;
    ybar,B,Bt,BtB,BtY,L,U,yy,abar: TComplexMatrix;
begin
    r0 := c0/f0;
    //r0 := 500;

    w0 := 2*pi*f0;
    k0 := w0 / c0;
    k0r0 := k0*r0;

    setlength(y,n);// = zeros(1,n);
    setlength(x,n);//x = linspace(-2.5,0.5,n);
    for i := 0 to n - 1 do
    begin
        x[i] := -2.5 + 3 / n * i;
        y[i] := ff(x[i],k0r0);
    end;

    nConstraints := nAccuracy + nStability;
    //%-2 to -1.1 stability (evanescent), -0.7 to 0 accuracy
    setlength(xs,nConstraints);// = [linspace(-2,-1.1,nStability),linspace(-0.7,-1/nAccuray,nAccuray)];
    for i := 0 to nConstraints - 1 do
    begin
        if i < nStability then
            xs[i] := -2 + 0.9 * i / (nStability - 1)
        else
            xs[i] := -0.7 + 0.7 * (i - nStability) / (nAccuracy);
    end;
    setlength(ys,nConstraints);// = zeros(size(xs));

    //Cederberg-Collins
    //eq 19: 1 + sum(ax^j) = fx(1 + sum(bx^j))
    //       1 + sum(ax^j) = fx + fx*sum(bx^j)
    //       1 - fx        = fx*sum(bx^j) - sum(ax^j)
    //   lhs -> ybar, rhs -> B

    B := TComplexMatrix.create(mm+nn,nConstraints);
    ybar := TComplexMatrix.create(1,nConstraints);
    for i := 0 to nConstraints - 1 do
    begin
        fx := ff(xs[i], k0r0);
        ys[i] := fx;
        for j := 1 to mm do
        begin
            qq := power(xs[i],j);
            B[j,i+1] := makeComplex80(-qq, 0);
            B[mm+j,i+1] := fx * qq;
        end;
        ybar[1,i+1] := 1 - fx;
    end;

    //setClipboard(ybar.toString(1) + #13#10#13#10 + ybar.toString(2));

    Bt := TComplexMatrix.create(B);
    Bt.CopyFromMatrix(B);
    Bt.transposition;

    BtB := mulMatrix(Bt,B);
    BtY := mulMatrix(Bt,ybar);

    //%%Bt*y = (Bt*B)*a
    //%%BtY = BtB*a
    //%abar = (Bt*B)\(Bt*ybar);
    //%abar = BtB\BtY;

    //reduceConditionNumber(BtB,BtY);

    L := TComplexMatrix.create(BtB);
    U := TComplexMatrix.create(BtB);
    yy := TComplexMatrix.create(1,nn+mm);
    abar := TComplexMatrix.create(1,nn+mm);
    BtB.LUDecompose(L,U);
    //[L,U,P] = lu(BtB);
    L.SolveMatrixL(BtY,yy);

    //setClipboard(BtB.toString(1) + #13#10#13#10 + BtB.toString(2));

    //yy := L\(P*(BtY));
    //[U,yy] = reduceConditionNumber(U,yy);
    //abar = U\yy;
    U.SolveMatrixU(yy,abar);

    setlength(p,mm);
    setlength(q,nn);
    for i := 0 to mm - 1 do
        p[i] := abar[1,i+1];
    for i := 0 to nn - 1 do
        q[i] := abar[1,mm+i+1];

//    //for k0r0 = 2pi, why not just hard code it
//    p[0] := makeComplex(2.6142154,-1.0095536);
//    p[1] := makeComplex(0.8640949,-2.3729225);
//    p[2] := makeComplex(-2.4471532,-1.3775903);
//    p[3] := makeComplex(-1.7411656,0.1362885);
//    p[4] := makeComplex(0.2600628,0.0883599);
//    p[5] := makeComplex(0.3580331,0.0581497);
//    p[6] := makeComplex(0.1175685,0.1192633);
//    p[7] := makeComplex(0.0693036,-0.003156);
//    p[8] := makeComplex(-0.0291378,0.0011882);
//    p[9] := makeComplex(-0.0488109,0.0035973);
//    p[10] := makeComplex(-0.0156906,0.0003125);
//    p[11] := makeComplex(-0.001622,-0.0000696);
//
//    q[0] := makeComplex(0.86418,-4.15114);
//    q[1] := makeComplex(-9.41307,-2.53628);
//    q[2] := makeComplex(-3.62274,14.85847);
//    q[3] := makeComplex(17.50913,1.65934);
//    q[4] := makeComplex(-3.13848,-18.63963);
//    q[5] := makeComplex(-12.3547,-2.88259);
//    q[6] := makeComplex(9.91544,-15.54724);
//    q[7] := makeComplex(12.91562,-30.51484);
//    q[8] := makeComplex(17.04837,-15.39923);
//    q[9] := makeComplex(11.94261,-4.77332);
//    q[10] := makeComplex(0.49202,-0.72011);
//    q[11] := makeComplex(-0.45266,-0.54683);

    setlength(z,n);//z = zeros(1,n);
    for i := 0 to n - 1 do
        z[i] := gg(x[i],p,q,mm,nn);

    //plot(x,real(z),x,imag(z), x,real(y),x,imag(y));
    //%plot(x,real(z),x,real(y));
    //%plot(x,imag(z),x,imag(y));
    //%plot(x,real(z),x,imag(z), xs,real(ys),xs,imag(ys));

    //yhat = B*abar;

    //BtY = Bt*ybar;
    //BtBa = (Bt*B)*abar;
end;

end.
