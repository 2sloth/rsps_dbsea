unit Jacobian;

interface

uses system.SysUtils, system.rtti,
    fmx.platform,
    combinations, mmatrix, mcomplex;

type
    TJacobian = class(TObject)
    private
        class procedure j(const pade: TComplexVect; const m: TComplexMatrix);
        class procedure f(const pade, polynomial: TComplexVect; const m: TComplexVect);
        class procedure fNumerator(const pade, polynomial, denominator: TComplexVect; const m: TComplexVect);
    public
        class function iterateDenominator(iterations: integer; const convergenceFactor: double; const polynomial: TComplexVect): TComplexVect;
        class function iterateNumerator(iterations: integer; const convergenceFactor: double; const polynomial, denominator: TComplexVect): TComplexVect;
        class procedure setClipboard(const s: string);
        class function getClipboard: string;
    end;

implementation

class procedure TJacobian.fNumerator(const pade, polynomial, denominator, m: TComplexVect);
var
    i,j,n: integer;
    c: TCombinations;
    wrap: TWrappedIntegerDynArray;
    //denomWithPadeInserted: TComplexVect;
begin
    n := length(polynomial);
    //setlength(denomWithPadeInserted, n);

    wrap := nil;
    c := TCombinations.create;
    try
        for j := 1 to n do
        begin
            if not c.list.Contains(wrap) then
                FreeAndNil(wrap);
            c.list.clear;
            wrap := TWrappedIntegerDynArray.Create(j);
            c.combinations(n, j, j, 0, wrap);

            m[j - 1] := -polynomial[j - 1];
            c.forEach(-1, procedure(w: TWrappedIntegerDynArray)
            var
                z: Tcomplex;
            begin
                z := w.product(-1,denominator);
                m[j - 1] := m[j - 1] + z;
            end);

            for i := 1 to n do
            begin
//                for k := 0 to n - 1 do
//                    if i - 1 = k then
//                        denomWithPadeInserted[k] := pade[k]
//                    else
//                        denomWithPadeInserted[k] := denominator[k];

                c.forEach(i-1, procedure(w: TWrappedIntegerDynArray)
                var
                    z: Tcomplex;
                begin
                    z := w.product(i - 1,denominator);
                    z := z * pade[i - 1];
                    m[j - 1] := m[j - 1] + z;
                end);
            end;
        end;
    finally
        if not c.list.Contains(wrap) then
            FreeAndNil(wrap);
        c.Free;
    end;
end;

class function TJacobian.getClipboard: string;
var
    clp: IFMXClipboardService;
begin
    if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService) then
    begin
        clp := IFMXClipboardService(TPlatformServices.Current.GetPlatformService(IFMXClipboardService));
        result := clp.GetClipboard.AsString;
    end;
end;

class procedure TJacobian.setClipboard(const s: string);
var
    clp: IFMXClipboardService;
begin
    if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService) then
    begin
        clp := IFMXClipboardService(TPlatformServices.Current.GetPlatformService(IFMXClipboardService));
        clp.SetClipboard(s);
    end;
end;

class function TJacobian.iterateDenominator(iterations: integer; const convergenceFactor: double; const polynomial: TComplexVect): TComplexVect;
var
    j: TComplexMatrix;
    F,dpade: TComplexVect;
    i,m,n: integer;
    //sb: TStringBuilder;
begin
    //finding product form to match polynomial: 1 + sum P[i] x^i = product(1 + result[i]x)

    n := length(polynomial);
    setlength(result, n);
    for i := 0 to length(polynomial) - 1 do
        result[i] := -polynomial[i];

    j := nil;
    //sb := nil;
    try
        //sb := TStringBuilder.Create;
        j := TComplexMatrix.create(n,n);
        setlength(F,n);
        setlength(dPade,n);

        for i := 0 to iterations - 1 do
        begin
            //sb.append('iteration: ' + inttostr(i) + '-----------------' + #13#10);

            TJacobian.j(result, j);
            //sb.append('J:' + #13#10);
            //sb.Append(j.myToString + #13#10);
            TJacobian.f(result, polynomial, f);
            //sb.append('F:' + #13#10);
            //sb.Append('Modulus sum: ' + FloatToStrF(f.modulusSum, ffFixed, 5, 8) + #13#10);
            //sb.Append(f.toString(false) + #13#10);
            j.InvertMatrix;
            //sb.append('Jinv:' + #13#10);
            //sb.Append(j.myToString + #13#10);
            //dpade.zero;
            j.multByVect(f,dpade);
            //sb.append('pade:' + #13#10);
            //sb.Append(result.toString(false) + #13#10);
            //sb.append('dPade (-ve):' + #13#10);
            //sb.Append(dpade.toString(false) + #13#10);
            for m := 0 to n - 1 do
                result[m] := result[m] - convergenceFactor * dPade[m];

            //setClipboard(sb.ToString);
        end;

    finally
        j.free;
        //sb.Free;
    end;
end;

class function TJacobian.iterateNumerator(iterations: integer; const convergenceFactor: double; const polynomial, denominator: TComplexVect): TComplexVect;
var
    j: TComplexMatrix;
    F,dpade: TComplexVect;
    i,m,n: integer;
    //sb: TStringBuilder;
begin
    //finding sum form to match polynomial
    // 1 + sum P[i]x^i / product (1 + b[i]x) = 1 + sum [ result[i]x / (1 + b[i]x) ]

    n := length(polynomial);
    setlength(result, n);
    for i := 0 to length(polynomial) - 1 do
        result[i] := -polynomial[i];

    j := nil;
    //sb := nil;
    try
        //sb := TStringBuilder.Create;
        j := TComplexMatrix.create(n,n);
        TJacobian.j(denominator, j);
        //sb.append('J:' + #13#10);
        //sb.Append(j.myToString + #13#10);
        j.InvertMatrix;
        //sb.append('Jinv:' + #13#10);
        //sb.Append(j.myToString + #13#10);

        setlength(F,n);
        setlength(dPade,n);

        for i := 0 to iterations - 1 do
        begin
            //sb.append('iteration: ' + inttostr(i) + '-----------------' + #13#10);

            TJacobian.fNumerator(result, polynomial, denominator, f);
            //sb.append('F:' + #13#10);
            //sb.Append('Modulus sum: ' + FloatToStrF(f.modulusSum, ffFixed, 5, 8) + #13#10);
            //sb.Append(f.toString(false) + #13#10);
            //dpade.zero;
            j.multByVect(f,dpade);
            //sb.append('pade:' + #13#10);
            //sb.Append(result.toString(false) + #13#10);
            //sb.append('dPade (-ve):' + #13#10);
            //sb.Append(dpade.toString(false) + #13#10);
            for m := 0 to n - 1 do
                result[m] := result[m] - convergenceFactor * dPade[m];

            //setClipboard(sb.ToString);
        end;

    finally
        j.free;
        //sb.Free;
    end;
end;

class procedure TJacobian.f(const pade, polynomial: TComplexVect; const m: TComplexVect);
var
    i,j,n: integer;
    c: TCombinations;
    wrap: TWrappedIntegerDynArray;
begin
    n := length(polynomial);
    wrap := nil;
    c := TCombinations.create;
    try
        for j := 1 to n do
        begin
            if not c.list.Contains(wrap) then
                FreeAndNil(wrap);
            c.list.clear;
            wrap := TWrappedIntegerDynArray.Create(j);
            c.combinations(n, j, j, 0, wrap);

            m[j - 1] := -polynomial[j - 1];
            c.forEach(-1, procedure(w: TWrappedIntegerDynArray)
            var
                z: Tcomplex;
            begin
                z := w.product(-1,pade);
                m[j - 1] := m[j - 1] + z;
            end);
        end;
    finally
        if not c.list.Contains(wrap) then
            FreeAndNil(wrap);
        c.Free;
    end;
end;

class procedure TJacobian.j(const pade: TComplexVect; const m: TComplexMatrix);
var
    i,j,n: integer;
    c: TCombinations;
    wrap: TWrappedIntegerDynArray;
begin
    n := length(pade);

    wrap := nil;
    c := TCombinations.create;
    try
        for j := 1 to n do
        begin
            if not c.list.Contains(wrap) then
                FreeAndNil(wrap);
            c.list.clear;
            wrap := TWrappedIntegerDynArray.Create(j);
            c.combinations(n, j, j, 0, wrap);

            for i := 1 to n do
            begin
                m[i,j] := 0;
                c.forEach(i - 1, procedure(w: TWrappedIntegerDynArray)
                var
                    z: TComplex;
                begin
                    z := w.product(i - 1,pade);
                    m[i,j] := m[i,j] + z;
                end);
            end;
        end;
    finally
        if not c.list.Contains(wrap) then
            FreeAndNil(wrap);
        c.Free;
    end;
end;

end.
