unit SelfStarter;

interface

uses system.math, system.types,
    MComplex, TriDiagonal, solversGeneralFunctions,
    dBSeaPE2, PEStarterPadeDouble, BandSolver, baseTypes,
    seafloorScheme, material;

type

    TSelfStarter = class(TObject)
    public
      const
        c0 = 1500;

        class function starter(const f, zMax, dz, zbThis, zSrc: double;
            const aCProfile2: TDepthValArray;
            const seafloorScheme: TSeafloorScheme): TStarter;
      private
        class function divideTwice(const dz, zSrc: double; const nz, iz: integer; const lhsMat, rhsMat: TTriDiagonalArray): TStarter;

        class procedure applyOperator(const f, c, dr, dz, zSrc, zbThis: double;
            const nz, iz: integer;
            const c1, cb, rhob, alphapz: TDoubleDynArray;
            const result: TStarter);
        class procedure generateMatrices(const f, dz, thisWaterDepth: double;
          const nz: integer;
          const c1, cb, rhob, attenuation_dBPerWavelength: TDoubleDynArray;
          const padeA, padeB: TComplexVect;
          var lhsMat, rhsMat: TTriDiagonalArray);

        class procedure mySolve(const u: TComplexVect; const nz, iz: integer; const lhsMat, rhsMat: TTriDiagonal);

        class function  starterInternal(const c0, f0, dr, dz, zBottom, zSrc: double;
            const nz: integer;
            const c1, cb, rhob, alphapz: TDoubleDynArray): TStarter;
    end;

implementation


class procedure TSelfStarter.applyOperator(const f, c, dr, dz, zSrc, zbThis: double;
    const nz, iz: integer;
    const c1, cb, rhob, alphapz: TDoubleDynArray;
    const result: TStarter);
var
    i,k: integer;
    k0r0: double;
    outputConst: TComplex;
    p,q, padeA,padeB: TComplexVect;
    lhsMat,rhsMat: TTriDiagonalArray;
begin
    TPEStarterPadeDouble.calcPadeTerms(8, f, c, dr, TPEStarterPadeDouble.ffVersion2, p,q, padeA, padeB);

    generateMatrices(f, dz, zbThis,
        nz,
        c1, cb, rhob, alphapz,
        padeA, padeB,
        lhsMat,rhsMat);

    for k := 0 to length(lhsMat) - 1 do
        mySolve(result.psi, nz, nz, lhsMat[k], rhsMat[k]);

    k0r0 := 2*pi*f / c * dr;
    outputConst := ComplexEXP(MakeComplex(0,k0r0)) / sqrt(k0r0); //eq 15 cederberg-collins
    //outputConst := 1 / sqrt(k0r0); //ram.pdf -> if we haven't included the -1 term in the depth operator
    for i := 0 to nz - 1 do
        result.psi[i] := outputConst * result.psi[i];
    result.scale(zSrc);
end;

class function TSelfStarter.divideTwice(const dz, zSrc: double;
  const nz, iz: integer;
  const lhsMat, rhsMat: TTriDiagonalArray): TStarter;
var
    iSrc: integer;
    //q, tmpUpper, tmpRhs: TComplexVect;
begin
    iSrc := trunc(zSrc / dz);

    setlength(result.psi, nz);
    //setlength(q, nz);
    //setlength(tmpUpper, nz);
    //setlength(tmpRhs, nz);

    result.psi[iSrc] := abs(zSrc - dz * iz) / dz;
    if iSrc = nz - 1 then
        result.psi[iSrc - 1].r := 1 - result.psi[iSrc].r
    else
        result.psi[iSrc + 1].r := 1 - result.psi[iSrc].r;

    mySolve(result.psi, nz, iz, lhsMat[0], rhsMat[0]);
    mySolve(result.psi, nz, iz, lhsMat[0], rhsMat[0]);
end;

class procedure TSelfStarter.mySolve(const u: TComplexVect; const nz, iz: integer; const lhsMat, rhsMat: TTriDiagonal);
const
    eps = 1.0E-30;
var
    i: integer;
    v: TComplexVect;
begin
    setlength(v, nz);

    // Multiply rhs
    for i := 0 to nz - 1 do
    begin
        if i = 0 then
            v[i] :=                             rhsMat[i, kDi] * u[i] + rhsMat[i, kUp] * u[i + 1] + eps
        else if i = nz - 1 then
            v[i] := rhsMat[i, kLo] * u[i - 1] + rhsMat[i, kDi] * u[i] + eps
        else
            v[i] := rhsMat[i, kLo] * u[i - 1] + rhsMat[i, kDi] * u[i] + rhsMat[i, kUp] * u[i + 1] + eps;
    end;

    // elimination
    for i := 1 to nz - 1 do
        v[i] := v[i] - lhsMat[i,kLo] * v[i - 1] + eps;

    // back substitute
    for i := nz - 2 downto 0 do
        u[i] := v[i] - lhsMat[i,kUp] * u[i + 1] + eps;

//    // elimination
//    for i := 1 to iz - 1 do
//        v[i] := v[i] - lhsMat[i,kLo] * v[i - 1] + eps;
//
//    for i := nz - 2 downto iz + 1 do
//        v[i] := v[i] - lhsMat[i,kUp] * v[i + 1] + eps;
//
//    u[iz] := (v[iz] - lhsMat[iz,kLo] * v[iz - 1] - lhsMat[iz,kUp] * v[iz + 1]) * lhsMat[iz,kDi] + eps;
//
//    // back substitute
//    for i := iz - 1 downto 0 do
//        u[i] := v[i] - lhsMat[i,kUp] * u[i + 1] + eps;
//
//    for i := iz + 1 to nz - 1 do
//        u[i] := v[i] - lhsMat[i,kLo] * u[i - 1] + eps;
end;

class function TSelfStarter.starter(const f, zMax, dz, zbThis, zSrc: double;
    const aCProfile2: TDepthValArray;
    const seafloorScheme: TSeafloorScheme): TStarter;
const
    dDepthExtension = 2.0;
var
    i, iz{depth of water/seabed}, nz: integer;
    c1, rho, alphapz{attenuation in db/wavelength}: TDoubleDynArray;
    aMaterial: TMaterial;
begin
    nz := ceil(dDepthExtension * zMax / dz);
    iz := round(zbThis / dz);

    setlength(c1, nz);
    setlength(rho, nz);
    setlength(alphapz, nz);

    setlength(Result.z, nz);
    for i := 0 to nz - 1 do
    begin
        result.z[i] := dz * i;

        if result.z[i] < zbThis then
        begin
            c1[i] := TSolversGeneralFunctions.interpolationFunPE(aCProfile2,result.z[iz]);
            rho[i] := TMaterial.water.rho_g;
            alphapz[i] := 0; //todo bulk attenuation here
        end
        else
        begin
            aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(result.z[i], zbThis);
            if not assigned(aMaterial) then
                aMaterial := water;

            c1[i] := aMaterial.c;
            rho[i] := aMaterial.rho_g;
            alphapz[i] := aMaterial.attenuation;
        end;
    end;

    result := self.starterInternal(c0, f, c0/f {k0r0 = 2pi}, dz, zbThis, zSrc, nz, c1, c1, rho, alphapz);
    for i := 0 to nz - 1 do
        result.z[i] := dz * i;
    result.isOK := true;
    result.scale(zSrc);
end;

class function TSelfStarter.starterInternal(const c0, f0, dr, dz, zBottom, zSrc: double; const nz: integer; const c1, cb, rhob, alphapz: TDoubleDynArray): TStarter;
var
    lhsMat, rhsMat: TTriDiagonalArray;
    pd1, pd2: TComplexVect;
    iz: integer;
begin
    iz := round(zBottom / dz);

    setlength(pd1,1);
    pd1[0] := 0;
    setlength(pd2,1);
    pd2[0] := -1;//-1;  // +1 : divide by (1+X)^2 -> operator is the (1+X)^7/4 version
    // -1: divide by (1-X)^2, operator is the (1-X)^2 * (1+X)^-1/4 version
    SetLength(lhsMat, 1);
    SetLength(rhsMat, 1);
    TSelfStarter.generateMatrices(f0, dz, zBottom,
        nz,
        c1, cb, rhob, alphapz,
        pd1, pd2,
        lhsMat,rhsMat);

    result := TSelfStarter.divideTwice(dz, zSrc, nz, iz, lhsMat, rhsMat);

    TSelfStarter.applyOperator(f0, c0, dr, dz, zSrc, zBottom, nz, iz, c1, cb, rhob, alphapz, result);
    result.isOK := true;
end;

class procedure TSelfStarter.generateMatrices(const f, dz, thisWaterDepth: double;
  const nz: integer;
  const c1, cb, rhob, attenuation_dBPerWavelength: TDoubleDynArray;
  const padeA, padeB: TComplexVect;
  var lhsMat, rhsMat: TTriDiagonalArray);
const
    rhoWater = 1.0;
var
    k0, k02, omega, aOffDiag, aDiag, oneOver2dz2, eqA9, attenFactor, rhoReduced: double;
    dLower, dDiag, dUpper, z, eqA4: TComplex;
    i, j, iz: integer;
    oneOverRho, alpha: TDoubleDynArray;
    kSqrFac: TComplexVect;
begin
    // attenuation is in dB/wavelength
     attenFactor := 1 / (40 * pi * log10(exp(1)));

    omega := 2 * pi * f;
    k0 := omega / c0;
    k02 := sqr(k0);

    aOffDiag := k02 / 6;
    aDiag := 2 * k02 / 3;
    oneOver2dz2 := 1 / sqr(dz) / 2;

    setlength(alpha, nz);
    setlength(oneOverRho, nz);
    setlength(kSqrFac, nz);

    setlength(lhsMat, length(padeA), nz);
    setlength(rhsMat, length(padeA), nz);

    iz := min(nz, round(thisWaterDepth / dz)); // water/seabed interface
    for i := 0 to nz - 1 do
    begin
        // Collins - A two-way parabolic equation for acoustic backscattering in the ocean , eq 6
        // omega = 2 pi f
        // eta = 1/(40*pi*log10(exp(1)))
        // beta = attenuation in dB/wavelength
        // k = omega/c( 1 + i*eta*beta)
        // alpha = sqrt(rho*c) -> reduction factor in energy conserving PE
        // k0 = ref wavenumber = w/c0
        // f(x) = sqrt(1 + x)
        // x = k0^-2(rho/alpha*(d/dz)*rho^-1*(d/dz)*alpha + k^2 - k0^2)

        // nb A two-way parabolic equation for acoustic backscattering in the ocean has full details of pade coeffs
        // although I think I've superseded that now by using jacobian iteration towards pade coeffs for any given func

        if i < iz then
        begin
            // rho/alpha*(d/dz)*rho^-1*(d/dz)*alpha
            alpha[i] := sqrt(rhoWater * c1[i] / c0);
            oneOverRho[i] := 1.0 / rhoWater;
            kSqrFac[i] := sqr(omega / c1[i]) - k02; // k^2 - k02 factor  could add water attenuation here
        end
        else
        begin
            alpha[i] := sqrt(rhob[i] * cb[i] / c0);
            oneOverRho[i] := 1.0 / rhob[i];
            kSqrFac[i] := ComplexSQR((omega / cb[i]) * MakeComplex(1, attenFactor * attenuation_dBPerWavelength[i])) - k02;
        end;
    end;

    // galerkin
    for i := 0 to nz - 1 do
    begin
        // Collins - A higher-order parabolic equation for wave propagation in an ocean overlying an elastic bottom
        // kSqrFac terms:
        // eq A4 (ie both 0th order)
        // dz2Over2 terms:
        // eq A9 d/dz( d Theta/dz * phi)
        // = (Theta[i-1] - Theta[i])*phi[i-1]/2dz^2
        // + (Theta[i-1] -2Theta[i] + Theta[i+1])*phi[i]/2dz^2
        // + (Theta[i+1] - Theta[i])*phi[i+1]/2dz^2
        //
        // rearrange:
        // = (phi[i-1] + phi[i])*Theta[i-1]/2dz^2
        // + (-phi[i-1] -2*phi[i] - phi[i+1])*Theta[i]/2dz^2
        // + (phi[i] + phi[i+1])*Theta[i+1]/2dz^2
        //
        // ---> Theta = alpha, phi = oneOverRho

        if (i < iz) then
            rhoReduced := rhoWater / alpha[i] // not operator
        else
            rhoReduced := rhob[i] / alpha[i];

        if i = 0 then
        begin
            eqA9 := oneOver2dz2 * rhoReduced * (oneOverRho[i - 0] + oneOverRho[i]) * alpha[i - 0];
            eqA4 := (kSqrFac[i - 0] + kSqrFac[i]) / 12;
        end
        else
        begin
            eqA9 := oneOver2dz2 * rhoReduced * (oneOverRho[i - 1] + oneOverRho[i]) * alpha[i - 1];
            eqA4 := (kSqrFac[i - 1] + kSqrFac[i]) / 12;
        end;
        dLower := eqA9 + eqA4;

        if i = 0 then
        begin
            eqA9 := -oneOver2dz2 * rhoReduced * (oneOverRho[i - 0] + 2 * oneOverRho[i] + oneOverRho[i + 1]) * alpha[i];
            eqA4 := (kSqrFac[i - 0] + 6 * kSqrFac[i] + kSqrFac[i + 1]) / 12;
        end
        else if i = nz - 1 then
        begin
            eqA9 := -oneOver2dz2 * rhoReduced * (oneOverRho[i - 1] + 2 * oneOverRho[i] + oneOverRho[i + 0]) * alpha[i];
            eqA4 := (kSqrFac[i - 1] + 6 * kSqrFac[i] + kSqrFac[i + 0]) / 12;
        end
        else
        begin
            eqA9 := -oneOver2dz2 * rhoReduced * (oneOverRho[i - 1] + 2 * oneOverRho[i] + oneOverRho[i + 1]) * alpha[i];
            eqA4 := (kSqrFac[i - 1] + 6 * kSqrFac[i] + kSqrFac[i + 1]) / 12;
        end;
        dDiag := eqA9 + eqA4;

        if i = nz - 1 then
        begin
            eqA9 := oneOver2dz2 * rhoReduced * (oneOverRho[i] + oneOverRho[i + 0]) * alpha[i + 0];
            eqA4 := (kSqrFac[i] + kSqrFac[i + 0]) / 12;
        end
        else
        begin
            eqA9 := oneOver2dz2 * rhoReduced * (oneOverRho[i] + oneOverRho[i + 1]) * alpha[i + 1];
            eqA4 := (kSqrFac[i] + kSqrFac[i + 1]) / 12;
        end;
        dUpper := eqA9 + eqA4;

        for j := 0 to length(padeA) - 1 do
        begin
            lhsMat[j, i, kLo] := aOffDiag + padeB[j]*dLower;
            lhsMat[j, i, kDi] := aDiag + padeB[j]*dDiag;
            lhsMat[j, i, kUp] := aOffDiag + padeB[j]*dUpper;
            rhsMat[j, i, kLo] := aOffDiag + padeA[j]*dLower;
            rhsMat[j, i, kDi] := aDiag + padeA[j]*dDiag;
            rhsMat[j, i, kUp] := aOffDiag + padeA[j]*dUpper;
        end;
    end;

    // decomposition
    for j := 0 to length(padeA) - 1 do
    begin
        for i := 0 to iz - 1 do
        begin
            if (i = 0) or (i = nz - 1) then
                z := lhsMat[j, i, kDi]
            else
                z := lhsMat[j, i, kDi] - lhsMat[j, i, kLo] * lhsMat[j, i - 1, kUp];

            if z.discriminant <> 0 then
            begin
                z := 1 / z;

                lhsMat[j, i, kLo] := lhsMat[j, i, kLo] * z;
                lhsMat[j, i, kUp] := lhsMat[j, i, kUp] * z;

                rhsMat[j, i, kLo] := rhsMat[j, i, kLo] * z;
                rhsMat[j, i, kDi] := rhsMat[j, i, kDi] * z;
                rhsMat[j, i, kUp] := rhsMat[j, i, kUp] * z;
            end;
        end;

        for i := nz - 1 downto iz do
        begin
            if i = nz - 1 then
                z := lhsMat[j, i, kDi]
            else
                z := lhsMat[j, i, kDi] - lhsMat[j, i, kLo] * lhsMat[j, i + 1, kUp];

            if z.discriminant <> 0 then
            begin
                z := 1 / z;

                lhsMat[j, i, kLo] := lhsMat[j, i, kLo] * z;
                lhsMat[j, i, kUp] := lhsMat[j, i, kUp] * z;

                rhsMat[j, i, kLo] := rhsMat[j, i, kLo] * z;
                rhsMat[j, i, kDi] := rhsMat[j, i, kDi] * z;
                rhsMat[j, i, kUp] := rhsMat[j, i, kUp] * z;
            end;
        end;

        if iz < nz - 2 then
        begin
            z := lhsMat[j, iz + 1, kDi] - lhsMat[j, iz + 1, kLo] * lhsMat[j, iz, kUp];
            lhsMat[j, iz + 1, kDi] := 1 / (z - lhsMat[j, iz + 1, kLo] * lhsMat[j, iz + 2, kUp]);
        end;
    end;

//    for i := 0 to iz - 1 do
//    begin
//        if i = 0 then
//            z := lhsMat[i, kDi]
//        else
//            z := lhsMat[i, kDi] - lhsMat[i, kLo] * lhsMat[i - 1, kUp];
//
//        if z.discriminant <> 0 then
//        begin
//            z := 1 / z;
//
//            lhsMat[i, kLo] := lhsMat[i, kLo] * z;
//            lhsMat[i, kUp] := lhsMat[i, kUp] * z;
//
//            rhsMat[i, kLo] := rhsMat[i, kLo] * z;
//            rhsMat[i, kDi] := rhsMat[i, kDi] * z;
//            rhsMat[i, kUp] := rhsMat[i, kUp] * z;
//        end;
//    end;
//
//    for i := nz - 1 downto iz do
//    begin
//        if i = nz - 1 then
//            z := lhsMat[i, kDi]
//        else
//            z := lhsMat[i, kDi] - lhsMat[i, kLo] * lhsMat[i + 1, kUp];
//
//        if z.discriminant <> 0 then
//        begin
//            z := 1 / z;
//
//            lhsMat[i, kLo] := lhsMat[i, kLo] * z;
//            lhsMat[i, kUp] := lhsMat[i, kUp] * z;
//
//            rhsMat[i, kLo] := rhsMat[i, kLo] * z;
//            rhsMat[i, kDi] := rhsMat[i, kDi] * z;
//            rhsMat[i, kUp] := rhsMat[i, kUp] * z;
//        end;
//    end;
//
//    if iz < nz - 2 then
//    begin
//        z := lhsMat[iz + 1, kDi] - lhsMat[iz + 1, kLo] * lhsMat[iz, kUp];
//        lhsMat[iz + 1, kDi] := 1 / (z - lhsMat[iz + 1, kLo] * lhsMat[iz + 2, kUp]);
//    end;
end;

end.
