unit PEStarterPadeDouble;

interface

uses system.classes, system.types, system.math,
    //fmx.platform,
    mcomplex, mmatrix, jacobian;

type
    TApproximationFunc = function(a,b: double): TComplex of object;

    TPEStarterPadeDouble = class(TObject)
    private
        class function  ffVersion1(x,k0r0: double): TComplex;
        class function  gg(x: double; p,q: TComplexVect): TComplex;
    public
        class function  power74(x,k0r0: double): TComplex;
        class function  power34(x,k0r0: double): TComplex;
        class function  ffVersion2(x,k0r0: double): TComplex;
        class procedure reduceConditionNumber(var A,b: TComplexMatrix);
        class procedure main(const f0, r: double; var x: TDoubleDynArray; var y,z: TComplexVect);
        class procedure calcPadeTerms(const nTerms: integer; const f0, c0, r0: double; approxFunc: TApproximationFunc; var p,q,lpp,lqq: TComplexVect);
        //class procedure calcPadeTerms(const k0, r0: double; const order: integer; var p,q: TComplexVect);
    end;

implementation

class procedure TPEStarterPadeDouble.calcPadeTerms(const nTerms: integer; const f0, c0, r0: double; approxFunc: TApproximationFunc; var p,q, lpp, lqq: TComplexVect);
const
    nAccuracy = 30;
    nStability = 10;
    forOctave = true;
var
    i,j,nn,mm,nConstraints: integer;
    r00, w0, k0, k0r0, qq: double;
    fx: TComplex;
    xs: TDoubleDynArray;
    ys: TComplexVect;
    ybar,B,Bt,BtB,BtY,L,U,yy,abar: TComplexMatrix;
    lp, lq: TComplexVect;
    mx: double;
begin
    mm := nTerms;
    nn := nTerms;
    r00 := r0;

    w0 := 2*pi*f0;
    k0 := w0 / c0;
    k0r0 := k0*r00;

    nConstraints := nAccuracy + nStability;
    //%-2 to -1.1 stability (evanescent), -0.7 to 0 accuracy
    setlength(xs,nConstraints);// = [linspace(-2,-1.1,nStability),linspace(-0.7,-1/nAccuray,nAccuray)];
    for i := 0 to nConstraints - 1 do
    begin
        if i < nStability then
            xs[i] := -2 + 0.9 * i / (nStability - 1)
        else
            xs[i] := -0.7 + 0.7 * (i - nStability) / (nAccuracy);
    end;
    setlength(ys,nConstraints);// = zeros(size(xs));

    //Cederberg-Collins
    //eq 19: 1 + sum(ax^j) = fx(1 + sum(bx^j))
    //       1 + sum(ax^j) = fx + fx*sum(bx^j)
    //       1 - fx        = fx*sum(bx^j) - sum(ax^j)
    //   1 - fx -> ybar, fx*sum(bx^j) - sum(ax^j) -> B

    B := TComplexMatrix.create(mm+nn,nConstraints);
    ybar := TComplexMatrix.create(1,nConstraints);
    for i := 0 to nConstraints - 1 do
    begin
        fx := approxFunc(xs[i], k0r0);
        for j := 1 to mm do
        begin
            qq := system.math.power(xs[i],j);
            B[j,i+1] := makeComplex(-qq, 0);
            B[mm+j,i+1] := fx * qq;
        end;
        ybar[1,i+1] := 1 - fx;
    end;

    Bt := TComplexMatrix.create(B);
    Bt.CopyFromMatrix(B);
    Bt.transposition;

    BtB := mulMatrix(Bt,B);
    BtY := mulMatrix(Bt,ybar);

    L := TComplexMatrix.create(BtB);
    U := TComplexMatrix.create(BtB);
    yy := TComplexMatrix.create(1,nn+mm);
    abar := TComplexMatrix.create(1,nn+mm);
    BtB.LUDecompose(L,U);
    //[L,U,P] = lu(BtB);
    L.SolveMatrixL(BtY,yy);

    //yy := L\(P*(BtY));
    //[U,yy] = reduceConditionNumber(U,yy);
    //abar = U\yy;
    U.SolveMatrixU(yy,abar);

    setlength(p,mm);
    setlength(q,nn);
    for i := 0 to mm - 1 do
        p[i] := abar[1,i+1];
    for i := 0 to nn - 1 do
        q[i] := abar[1,mm+i+1];

    //todo reduce amount of iterations
    lqq := TJacobian.iterateDenominator(400, 0.5, q); //denominator first
    lpp := TJacobian.iterateDenominator(400, 0.5, p);
    //lpp := TJacobian.iterateNumerator(400, 0.5, p, lqq); //numerator
end;

//class procedure TPEStarterPadeDouble.calcPadeTerms(const k0, r0: double; const order: integer; var p,q: TComplexVect);
//const
//    nAccuracy = 30;
//    nStability = 10;
//    //mm = 12; //%numerator order (1 + A1x + A2x^2)
//    //nn = mm; //%denominator order (1 + B1x + B2x^2)
//var
//    i,j,nConstraints,nn,mm: integer;
//    k0r0, qq: double;
//    fx: TComplex;
//    xs: TDoubleDynArray;
//    ys: TComplexVect;
//    ybar,B,Bt,BtB,BtY,L,U,yy,abar: TComplexMatrix;
//begin
//    nn := order;
//    mm := order;
//    k0r0 := k0*r0;//2*pi; //1 wavelength
//
//    nConstraints := nAccuracy + nStability;
//    //%-2 to -1.1 stability (evanescent), -0.7 to 0 accuracy
//    setlength(xs,nConstraints);// = [linspace(-2,-1.1,nStability),linspace(-0.7,-1/nAccuray,nAccuray)];
//    for i := 0 to nConstraints - 1 do
//    begin
//        if i < nStability then
//            xs[i] := -2 + 0.9 * i / (nStability - 1)
//        else
//            xs[i] := -0.7 + 0.7 * (i - nStability) / (nAccuracy);
//    end;
//    setlength(ys,nConstraints);// = zeros(size(xs));
//
//    //Cederberg-Collins
//    //eq 19: 1 + sum(ax^j) = fx(1 + sum(bx^j))
//    //       1 + sum(ax^j) = fx + fx*sum(bx^j)
//    //       1 - fx        = fx*sum(bx^j) - sum(ax^j)
//    //   lhs -> ybar, rhs -> B
//
//    B := TComplexMatrix.create(mm+nn,nConstraints);
//    ybar := TComplexMatrix.create(1,nConstraints);
//    for i := 0 to nConstraints - 1 do
//    begin
//        fx := ffVersion2(xs[i], k0r0);
//        for j := 1 to mm do
//        begin
//            qq := system.math.power(xs[i],j);
//            B[j,i+1] := -qq;
//            B[mm+j,i+1] := fx * qq;
//        end;
//        ybar[1,i+1] := 1 - fx;
//    end;
//
//    //setClipboard(ybar.toString(1) + #13#10#13#10 + ybar.toString(2));
//
//    Bt := TComplexMatrix.create(B);
//    Bt.CopyFromMatrix(B);
//    Bt.transposition;
//
//    BtB := mulMatrix(Bt,B);
//    BtY := mulMatrix(Bt,ybar);
//
//    //%%Bt*y = (Bt*B)*a
//    //%%BtY = BtB*a
//    //%abar = (Bt*B)\(Bt*ybar);
//    //%abar = BtB\BtY;
//
//    //reduceConditionNumber(BtB,BtY);
//
//    L := TComplexMatrix.create(BtB);
//    U := TComplexMatrix.create(BtB);
//    yy := TComplexMatrix.create(1,nn+mm);
//    abar := TComplexMatrix.create(1,nn+mm);
//    BtB.LUDecompose(L,U);
//    //[L,U,P] = lu(BtB);
//    L.SolveMatrixL(BtY,yy);
//
//    //setClipboard(BtB.toString(1) + #13#10#13#10 + BtB.toString(2));
//
//    //yy := L\(P*(BtY));
//    //[U,yy] = reduceConditionNumber(U,yy);
//    //abar = U\yy;
//    U.SolveMatrixU(yy,abar);
//
//    setlength(p,mm);
//    setlength(q,nn);
//    for i := 0 to mm - 1 do
//        p[i] := abar[1,i+1];
//    for i := 0 to nn - 1 do
//        q[i] := abar[1,mm+i+1];
//
////    //for k0r0 = 2pi, why not just hard code it
////these ones not factorised - actual coefficients of each power of X
////    p[0] := makeComplex(2.6142154,-1.0095536);
////    p[1] := makeComplex(0.8640949,-2.3729225);
////    p[2] := makeComplex(-2.4471532,-1.3775903);
////    p[3] := makeComplex(-1.7411656,0.1362885);
////    p[4] := makeComplex(0.2600628,0.0883599);
////    p[5] := makeComplex(0.3580331,0.0581497);
////    p[6] := makeComplex(0.1175685,0.1192633);
////    p[7] := makeComplex(0.0693036,-0.003156);
////    p[8] := makeComplex(-0.0291378,0.0011882);
////    p[9] := makeComplex(-0.0488109,0.0035973);
////    p[10] := makeComplex(-0.0156906,0.0003125);
////    p[11] := makeComplex(-0.001622,-0.0000696);
////
////    q[0] := makeComplex(0.86418,-4.15114);
////    q[1] := makeComplex(-9.41307,-2.53628);
////    q[2] := makeComplex(-3.62274,14.85847);
////    q[3] := makeComplex(17.50913,1.65934);
////    q[4] := makeComplex(-3.13848,-18.63963);
////    q[5] := makeComplex(-12.3547,-2.88259);
////    q[6] := makeComplex(9.91544,-15.54724);
////    q[7] := makeComplex(12.91562,-30.51484);
////    q[8] := makeComplex(17.04837,-15.39923);
////    q[9] := makeComplex(11.94261,-4.77332);
////    q[10] := makeComplex(0.49202,-0.72011);
////    q[11] := makeComplex(-0.45266,-0.54683);
//
//
////    //factorised in octave - not sure about it though!
////    //the terms are (x - r1)(x - r2)...
////    //and we want (1 + ax)(1+bx) so take negative inverse of terms
////    p[0] := -1 / makecomplex(-3.96587603022381,-1.40840355709798);
////    p[1] := -1 / makecomplex(-3.86255589063107,+1.14764302143385);
////    p[2] := -1 / makecomplex(-2.68167822554011,+1.65295277564743);
////    p[3] := -1 / makecomplex(0.241211367715894,-1.93254638222316);
////    p[4] := -1 / makecomplex(0.219370762940137,+1.97633817134695);
////    p[5] := -1 / makecomplex(1.68608917214229,+0.757756045270421);
////    p[6] := -1 / makecomplex(1.62044100401806,-0.29333371855275);
////    p[7] := -1 / makecomplex(0.876279496243649,-0.560326317800056);
////    p[8] := -1 / makecomplex(-0.863936661071133,-0.506650549114123);
////    p[9] := -1 / makecomplex(-0.932038970306252,-0.198033171366678);
////    p[10] := -1 / makecomplex(-0.979114185939588,-0.03348133410223);
////    p[11] := -1 / makecomplex(-1.00578930922824,+0.004908254055811);
////
////    q[0] := -1 / makecomplex(4.05772857101495,-3.31611785658981);
////    q[1] := -1 / makecomplex(-2.83941877208775,+1.62938554871374);
////    q[2] := -1 / makecomplex(-0.0051049188136058,+1.69524587392278);
////    q[3] := -1 / makecomplex(0.601389381048859,+0.859448187974553);
////    q[4] := -1 / makecomplex(0.620945577902033,+0.179440389246641);
////    q[5] := -1 / makecomplex(0.433409637615924,-0.235233485726754);
////    q[6] := -1 / makecomplex(0.135774724544367,-0.488353798472346);
////    q[7] := -1 / makecomplex(-0.940019724316165,-0.0473951597365255);
////    q[8] := -1 / makecomplex(-0.239982978219792,-0.554681195043401);
////    q[9] := -1 / makecomplex(-0.859149070554513,-0.158145923196413);
////    q[10] := -1 / makecomplex(-0.749055247013027,-0.296419363206357);
////    q[11] := -1 / makecomplex(-0.555974749847224,-0.447946999403206);
//end;

class function TPEStarterPadeDouble.ffVersion1(x,k0r0: double): TComplex;
var
    q,z: TComplex;
begin
    q := 1+x;
    z := k0r0*(-1 + complexSQRT(q));
    z := z.mulByI;
    result := cpower(q,7/4)*ComplexEXP(z);

    //%y = power(1+x,7/4);
    //%y = exp(sqrt(-1)*k0r0*(-1+sqrt(1+x)));
    //%y = power(1+x,1/4);
end;

class function TPEStarterPadeDouble.ffVersion2(x,k0r0: double): TComplex;
var
    onePlusX,exponentialTerm: TComplex;
begin
    //(1-X)^2 * (1+X)^(-1/4) * exp(i*k0*r*sqrt(1+X)). //because we divided by (1-x)^2 at the delta smoothing step
    onePlusX := 1+x;
    exponentialTerm := k0r0*(-1 + complexSQRT(onePlusX)); // -1 added to follow Cederberg Collins - must multiply by ik0r0 to get pressure
    result := ComplexSQR(1-x) * cpower(onePlusX,-1/4) * ComplexEXP(exponentialTerm.mulByI);
end;

class function TPEStarterPadeDouble.gg(x: double; p,q: TComplexVect): TComplex;
var
    num, den: TComplex;
    j: integer;
begin
    num := 1;
    for j := 0 to length(p) - 1 do
        num := num + p[j]*power(x,j+1);

    den := 1;
    for j := 0 to length(q) - 1 do
        den := den + q[j]*power(x,j+1);

    result := num / den;
end;

class procedure TPEStarterPadeDouble.reduceConditionNumber(var A,b: TComplexMatrix);
var
    i,j: integer;
    rowMax: TComplex;
begin
    for i := 0 to A.RowCount - 1 do
    begin
        rowMax := NullComplex;
        for j := 0 to A.ColCount - 1 do
            if A[j+1,i+1].modulus > rowMax.modulus then
                rowMax := A[j+1,i+1];

        for j := 0 to A.ColCount - 1 do
            A[j+1,i+1] := A[j+1,i+1] / rowMax;

        b[1,i+1] := b[1,i+1] / rowMax;
    end;
end;

class procedure TPEStarterPadeDouble.main(const f0, r: double; var x: TDoubleDynArray; var y,z: TComplexVect);
const
    c0 = 1500.0;
    n = 1000;
var
    i: integer;
    r0, w0, k0, k0r0: double;
    p,q,lpp,lqq: TComplexVect;
begin
    r0 := r;//c0/f0;

    w0 := 2*pi*f0;
    k0 := w0 / c0;
    k0r0 := k0*r0;

    setlength(y,n);
    setlength(x,n);
    for i := 0 to n - 1 do
    begin
        x[i] := -2.5 + 3 / n * i;
        y[i] := ffVersion1(x[i],k0r0);
    end;

    calcPadeTerms(6,f0,c0,r0,power74,p,q,lpp,lqq);

    setlength(z,n);
    for i := 0 to n - 1 do
        z[i] := gg(x[i],p,q);

    //plot(x,real(z),x,imag(z), x,real(y),x,imag(y));
    //%plot(x,real(z),x,real(y));
    //%plot(x,imag(z),x,imag(y));
    //%plot(x,real(z),x,imag(z), xs,real(ys),xs,imag(ys));

    //yhat = B*abar;

    //BtY = Bt*ybar;
    //BtBa = (Bt*B)*abar;
end;


class function TPEStarterPadeDouble.power74(x,k0r0: double): TComplex;
var
    onePlusX,exponentialTerm: TComplex;
begin
    onePlusX := 1 + x;
    exponentialTerm := k0r0*complexSQRT(onePlusX); //Eq 14 in ram.pdf
    k0r0*(-1 + complexSQRT(onePlusX)); //cederberg collins version, with -1 term (makes smoother and is removed later)
    result := cpower(onePlusX,7/4)*ComplexEXP(exponentialTerm.mulByI);
end;

class function TPEStarterPadeDouble.power34(x,k0r0: double): TComplex;
var
    onePlusX,exponentialTerm: TComplex;
begin
    onePlusX := makeComplex(1 + x,0);
    exponentialTerm := k0r0*(-1 + complexSQRT(onePlusX));
    result := cpower(onePlusX,3/4)*ComplexEXP(exponentialTerm.mulByI);
end;

end.
