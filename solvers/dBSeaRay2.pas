unit dBSeaRay2;

{$B-}

interface

uses
    system.classes, system.math, system.SysUtils, system.types,
    system.StrUtils,
    Mcomplex, baseTypes,
    helperFunctions, dBSeaconstants, material,
    multiLayerReflectionCoefficient, seafloorScheme, directivity,
    mcklib, dBSeaRayArrivals,
    RaySolverCommon,
    waterprops, solversGeneralFunctions, raySolverTypes, globalsUnit,
    rayPlotting, SmartPointer, Nullable;

type

  TdBSeaRay2Solver = class(TObject)
  public
    class procedure mainMultipleFrequency(const f: TDoubleDynArray;
                                    const solveThisFreq: TBooleanDynArray;
                                    const srcX, srcY, srcZ, angle, rmax, dr, zmax, startInitialAngle, endInitialAngle, initialStepsize: double;
                                    const nz: integer;
                                    const aZProfile2: TRangeValArray;
                                    const aCProfile3: TDepthValArrayAtRangeArray;
                                    const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp: double;
                                    const maxBottomReflections, aNTheta: integer;
                                    const returnTL: boolean;
                                    const seafloors: TSeafloorRangeArray;
                                    const directivities: TDirectivityList;
                                    const fs: double;
                                    const outputArrivals, calculateVolumeAttenuationAtEachStep: boolean;
                                    const raySolverIterationMethod: TRaySolverIterationMethod;
                                    const cancel: TWrappedBool;
                                    const pSolveriMax: TWrappedInt; {= nr, num range points}
                                    const fractionDone: TWrappedDouble;
                                    const sliceRays: TSliceRays;
                                    out tlMultiFreq: TDoubleField;
                                    out pFieldMultiFreq: TComplexField;
                                    var arrivals: TArrivalsField;
                                    //out solverErrorMessages: String;
                                    out solverErrors: TSolverErrors;
                                    out deaths: TRayDeaths);
  end;

implementation


class procedure TdBSeaRay2Solver.mainMultipleFrequency(const f: TDoubleDynArray;
                                                       const solveThisFreq: TBooleanDynArray;
                                                       const srcX, srcY, srcZ, angle, rmax, dr, zmax, startInitialAngle, endInitialAngle, initialStepsize: double;
                                                       const nz: integer;
                                                       const aZProfile2: TRangeValArray;
                                                       const aCProfile3: TDepthValArrayAtRangeArray;
                                                       const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp: double;
                                                       const maxBottomReflections, aNTheta: integer;
                                                       const returnTL: boolean;
                                                       const seafloors: TSeafloorRangeArray;
                                                       const directivities: TDirectivityList;
                                                       const fs: double;
                                                       const outputArrivals, calculateVolumeAttenuationAtEachStep: boolean;
                                                       const raySolverIterationMethod: TRaySolverIterationMethod;
                                                       const cancel: TWrappedBool;
                                                       const pSolveriMax: TWrappedInt;
                                                       const fractionDone: TWrappedDouble;
                                                       const sliceRays: TSliceRays;
                                                       out tlMultiFreq: TDoubleField{f,i,j};
                                                       out pFieldMultiFreq: TComplexField{f,i,j};
                                                       var arrivals: TArrivalsField;
                                                       //out solverErrorMessages: String;
                                                       out solverErrors: TSolverErrors;
                                                       out deaths: TRayDeaths);
//NB kill ray after maxBottomReflections reflections from the bottom
const
    defaultNTheta = 1000;
var
    c,rayPhase,rayWidth,z,r,theta: TDoubleDynArray;
    fi,nf: integer;
    bPastRayWidthXOver: TBooleanDynArray;
    Abeam: TDoubleDynArray;
    c0,theta1,deltaz,deltar: double;
    nr,i,ir,iz,irStart,irEnd: integer;
    ntheta,itheta: integer;
    ds: double; //stepsize
    localAttenuation: Single;
    constSSP: boolean;
    theta0,inTheta,outTheta,delTheta: double;
    ReflectionAmplitudePhase: TComplexVect; //reflection amplitude loss and phase change, accumulated over reflections
    xnext,xprev: TRayIterator2;
    inSeafloor,inAir: boolean;
    nReflections: integer;
    stepdz,stepdr,mwat,cwat: double;
    iz1,iz2: integer;
    cHere: double; //average c for this step
    stepDist,startR,endR,delay: double;
    layers: ISmart<TSeafloorReflectionLayers>;
    directivityMat: TSingleMatrix;
    konst,thisModulusSquared, sinTheta0, cosTheta0: double;
    thisBottom: TGetDepthRec;
    ray: TRay;
    solveFreqInds: TArray<integer>;
    //maxInitialAmplitude, amplitudeCutoff: double;

    seafloor: TSeafloorScheme;
    seafloorIndex: integer;
    currentCProfile: TDepthValArray;
    CProfileRangeInd, prevProfileRangeInd: integer;
    prev_dc_dr, prevR: double;
    c_all, dc_dz_all, d2c_dz2_all: TMatrix;

    function maxAmplitude(): double;
    var
        fi: integer;
    begin
        result := nan;
        for fi in solveFreqInds do
            greater(result,ReflectionAmplitudePhase[fi].modulusSquared);
    end;
    function minAmplitude(): double;
    var
        fi: integer;
    begin
        result := nan;
        for fi in solveFreqInds do
            lesser(result,ReflectionAmplitudePhase[fi].modulusSquared);
    end;

    procedure updateRayParameters();
    var
        fi: integer;
        k1,k2: double;
    begin
        for fi in solveFreqInds do
        begin
            rayPhase[fi] := rayPhase[fi] + (2 * pi * f[fi])/cHere*stepDist;

            if sign(xnext.q) <> sign(xprev.q) then
                rayPhase[fi] := rayPhase[fi] + pi;  //phase change at caustics

            rayWidth[fi] := abs(xnext.q * delTheta);

            //rayWidth := max(pi*cHere/f,abs(xnext[3]*delTheta)); //abs q*deltheta, but don't let it vanish at caustics
            if not bPastRayWidthXOver[fi] then
                if rayWidth[fi] > pi*cHere/f[fi] then
                    bPastRayWidthXOver[fi] := true;
                //xoverDist := xnext[0];
            if bPastRayWidthXOver[fi] then
                rayWidth[fi] := max(pi*cHere/f[fi],rayWidth[fi]);

            k1 := delTheta/xnext.r*cHere/c0; //eq 3.76
            k2 := cosTheta0/rayWidth[fi];
            Abeam[fi] := konst*sqrt(k1*2*k2);
            //Abeam[fi] := 1/power(2*pi,0.25)*sqrt(delTheta/xnext.r*cHere/c0*2*cos(theta0)/rayWidth[fi]);
            rayWidth[fi] := min(thisBottom.z,rayWidth[fi]); //reduce ray width when the channel is shallow. but keep intensity as it would have been
        end;
    end;

    procedure intensityCalcs();
    var
        ir, fi, iz: integer;
        distToRay: double;
        magnitude: double;
    begin
        startR := min(xprev.r,xnext.r);
        endR := max(xprev.r,xnext.r);
        //get the approx start and end range indices (much faster than looping over all range indices). could actually calculate the real indices to go to here
        irStart := max(0,safefloor(startR / deltaR) - 1);
        irEnd := min(length(r) - 1,safeceil(endR / deltaR) + 1);

        for ir := irStart to irEnd do
            if (r[ir] >= startR) and (r[ir] < endR) then
                for fi in solveFreqInds do
                begin
                    {check all the z points that are within the range of the raywidth}
                    for iz := max(0     ,safefloor((min(xnext.z,xnext.z) - 2*rayWidth[fi])/deltaz)) to
                              min(nz - 1,safeceil ((max(xnext.z,xnext.z) + 2*rayWidth[fi])/deltaz)) do
                    begin
                        //could speed this up by not re-calcing at each frequency
                        distToRay := TSolversGeneralFunctions.distPointToLineSegment(r[ir], z[iz],startR,xprev.z,endR,xnext.z); //takes account of line segment endpoints

                        if (distToRay < 2*rayWidth[fi]) then //2*waywidth gets us to 2 StdDev, which is around -30 dB
                        begin
                            //magnitude := (1 - distToRay/rayWidth); //hat function
                            magnitude := exp(-sqr(distToRay/rayWidth[fi])); //gaussian -- we can speed this line up //could do lookup table

                            if outputArrivals then
                            begin
                                {add this arrival to the arrivals list for this point}
                                arrivals.addArrival(fi, ir, 0, iz,
                                                    ReflectionAmplitudePhase[fi].r*Abeam[fi]*magnitude*cos(rayPhase[fi]),
                                                    delay);
                            end
                            else
                            begin
                                {add the contribution from this ray at this point to the output field}
                                pFieldMultiFreq[fi,iz,ir] := pFieldMultiFreq[fi,iz,ir] + ComplexEXP(makeComplex(0,rayPhase[fi])) *
                                                                                         ReflectionAmplitudePhase[fi] *
                                                                                         (Abeam[fi]*magnitude); //includes accumulated reflection amplitude/phase
                            end;
                        end; //distToRay <
                    end; //iz
                end;
    end;

    procedure outputTL;
    var
        fi, ir, iz: integer;
    begin
        if not ((not outputArrivals) and returnTL) then exit;

        setlength(tlMultiFreq,nf,nz,nr);
        for fi := 0 to nf - 1 do
            for ir := 0 to nr - 1 do
                for iz := 0 to nz - 1 do
                    if solveThisFreq[fi] then
                    begin
                        thisModulusSquared := pFieldMultiFreq[fi,iz,ir].modulusSquared;
                        if thisModulusSquared > 0 then
                            tlMultiFreq[fi,iz,ir] := -10*log10(thisModulusSquared)
                        else
                            tlMultiFreq[fi,iz,ir] := 0;
                    end
                    else
                        tlMultiFreq[fi,iz,ir] := nan;
    end;

    //{$Define RAYOUTPUT}
    //{$Define TESTABEAM}
    {$IF defined (DEBUG) and (defined (RAYOUTPUT) or defined(TESTABEAM))}
    var
    s: string;
    {$ENDIF}
begin
    //amplitudeCutoff := undB20(-500);
    TRayDeathsHelper.init(deaths);
    ray := nil;

    solveFreqInds := TSolversGeneralFunctions.getSolveFreqInds(solveThisFreq);
    if solveThisFreq.allFalse() then
        include(solverErrors,kNoFrequencies);

    konst := 1/power(2*pi,0.25);

    //seafloor := seafloors.seafloorForRange(0, seafloorIndex);
    layers := TSmart<TSeafloorReflectionLayers>.create(TSeafloorReflectionLayers.create(true));

    setlength(c_all, length(aCProfile3));
    setlength(dc_dz_all, length(aCProfile3));
    setlength(d2c_dz2_all, length(aCProfile3));
    currentCProfile := aCProfile3.depthValForRange(0, CProfileRangeInd);

    c0 := currentCProfile.average;

    nr := max(1, saferound(rmax/dr));
    pSolveriMax.i := nr;
    {takeoff angles}
    ntheta := aNTheta; // number of rays
    if ntheta = 0 then //The user may select 0 for default
        ntheta := defaultNTheta;
    ds := initialStepsize; //initial stepsize

    //setting all needed multi-freq arrays
    nf := length(f);
    setlength(rayPhase,nf);
    setlength(rayWidth,nf);
    setlength(bPastRayWidthXOver,nf);
    setlength(Abeam,nf);
    setlength(ReflectionAmplitudePhase,nf);

    deltaz := zmax / max(1,(nz - 1));
    setlength(z,nz);
    for i := 0 to nz - 1 do
        z[i] := i*deltaz;

    deltar := dr;//rmax / (nr - 1);
    if deltar = 0 then
        deltar := 1;
    setlength(r,nr);
    for i := 0 to nr - 1 do
        r[i] := i*deltar;

//    {ranges of seafloor depths}
//    bsteps := length(aZProfile2);
//    setlength(r_zb,bsteps);
//    setlength(zb,bsteps);
//    for i := 0 to bsteps - 1 do
//    begin
//        r_zb[i] := aZProfile2[i].range;
//        zb[i]   := aZProfile2[i].val;
//    end;

    {$IF defined (DEBUG) and defined (RAYOUTPUT)}
    s := s + '<seafloor>' + #13#10;
    for i := 0 to bsteps - 1 do
        s := s + floattostr(round1(r_zb[i])) + ',' + floattostr(round1(zb[i])) + ';';
    s := s + #13#10 + '</seafloor>' + #13#10;
    {$ENDIF}

    TRaySolverCommon.initCProfiles(currentCProfile, constSSP, c_all[CProfileRangeInd], dc_dz_all[CProfileRangeInd], d2c_dz2_all[CProfileRangeInd]);
    c := c_all[CProfileRangeInd];

    {angles}
    TSolversGeneralFunctions.setupRayAngles(startInitialAngle,endInitialAngle,nTheta,theta,delTheta);
    directivityMat := TSolversGeneralFunctions.getDirectivityAsMatrix(f,theta,directivities);

    if outputArrivals then
    begin
        if assigned(arrivals) then
            arrivals.reinitialise(nf,nr,1,nz,fs)
        else
            arrivals := TArrivalsField.Create(nf,nr,1,nz,fs);
    end
    else
    begin
        setlength(pFieldMultiFreq,nf,nz,nr);
        for fi := 0 to nf - 1 do
            for ir := 0 to nr - 1 do
                for iz := 0 to nz - 1 do
                begin
                    pFieldMultiFreq[fi,iz,ir].r := 0;
                    pFieldMultiFreq[fi,iz,ir].i := 0;
                end;
    end;

    {iterate through rays}
    for itheta := 0 to ntheta - 1 do
    begin
        {$IF defined (DEBUG) and defined (RAYOUTPUT)}
        if itheta > 0 then
            s := s + #13#10 + '</ray>' + #13#10;
        s := s + '<ray>' + #13#10;
        {$ENDIF}

        //UI feedback
        //if Assigned(updateLabelProc) and (itheta mod 20 = 0) then updateLabelProc('Ray ' + tostr(itheta + 1) + ' of ' + tostr(ntheta));
        if (cancel <> nil) and cancel.b then exit;

        //source can't be below the seafloor (currently)
        if srcZ > aZProfile2[0].val then
        begin
            include(solverErrors,kSourceBelowSeafloorRaySolver);
            break;
        end;

        theta0 := theta[itheta];
        system.SineCosine(theta0, sinTheta0, cosTheta0);
        for fi in solveFreqInds do
        begin
            //ReflectionAmplitudePhase[fi] := 1; //amplitude starts at 1, will be reduced by each sediment reflection
            ReflectionAmplitudePhase[fi] := undB20(-directivityMat[fi,itheta]); //amplitude from the directivity: NB -ve
            rayPhase[fi] := 0;
            bPastRayWidthXOver[fi] := false;
        end;
        //maxInitialAmplitude := maxAmplitude();
        delay := 0; //keeping track of time taken, for arrivals calc

        //ray initial conditions: r,z,zeta,xi,q,p
        xnext.r := 0.0;
        xnext.z := srcZ;
        xnext.zeta := sinTheta0/c0;
        xnext.xi := cosTheta0/c0;
        xnext.q := 0;
        xnext.p := 1/c0;
        xnext.terminated := 0;

        //rayTerminate := false;
        inSeafloor := false;
        inAir := false;
        nReflections := 0;

        CProfileRangeInd := 0;
        prev_dc_dr := 0;

        if assigned(sliceRays) then
            ray := sliceRays.addRay();

        while true do
        begin
            if (cancel <> nil) and cancel.b then break;

            prevR := xprev.r;
            xprev.cloneFrom(xnext);

            {$IF defined (DEBUG) and defined (RAYOUTPUT)}s := s + floattostr(round1(xprev.r)) + ',' + floattostr(round1(xprev.z)) + ';';{$ENDIF}

            prevProfileRangeInd := CProfileRangeInd;
            currentCProfile := aCProfile3.depthValForRange(xnext.r, CProfileRangeInd);
            if CProfileRangeInd <> prevProfileRangeInd then
            begin
                if length(c_all[CProfileRangeInd]) = 0 then //not pre calculated
                    TRaySolverCommon.initCProfiles(currentCProfile, constSSP, c_all[CProfileRangeInd], dc_dz_all[CProfileRangeInd], d2c_dz2_all[CProfileRangeInd]);
                c := c_all[CProfileRangeInd];
            end;

            {take a step, via 2nd or 4th order runge kutta (depending on whether the SSP is const or not)}
            iz := min(length(c_all[CProfileRangeInd]) - 1,max(0,saferound(xprev.z)));
            if ((raySolverIterationMethod = kConstSSP) and constSSP) or (raySolverIterationMethod = kRK2) then
                xnext := TSolversGeneralFunctions.rk2StepMCK(xprev,
                    ds,
                    prevR,
                    c_all[CProfileRangeInd][iz],
                    dc_dz_all[CProfileRangeInd][iz],
                    d2c_dz2_all[CProfileRangeInd][iz],
                    c_all[prevProfileRangeInd][iz],
                    dc_dz_all[prevProfileRangeInd][iz],
                    prev_dc_dr)
            else
                xnext := TSolversGeneralFunctions.rk4StepMCK(xprev,
                    ds,
                    prevR,
                    c_all[CProfileRangeInd][iz],
                    dc_dz_all[CProfileRangeInd][iz],
                    d2c_dz2_all[CProfileRangeInd][iz],
                    c_all[prevProfileRangeInd][iz],
                    dc_dz_all[prevProfileRangeInd][iz],
                    prev_dc_dr);
            //todo smaller stepsize accuracy checking/rk-fehlberg

            if isnan(xprev.r) or isnan(xprev.z) then
            begin
                inc(deaths[kError]);
                break; //todo but why nan? eg repeated points in SSP
            end;

            if (xnext.z < 0) then
            begin
                //ray hit the surface
                if (inAir) or (xnext.z < -ds) then
                begin
                    inc(deaths[kAir]);
                    break;
                end;
                inAir := true;

                stepdz := xnext.z - xprev.z;
                stepdr := xnext.r - xprev.r;
                inTheta := arctan2(stepdz,stepdr);
                outTheta := -inTheta;
                cHere := c[min(length(c) - 1,max(0,saferound(xprev.z)))]; //todo average for this ray
                xnext.xi := cos(outTheta)/cHere;
                xnext.r := xprev.r - xprev.z/stepdz*stepdr;
                xnext.z := 0;
                //if isnan(xprev.r) or isnan(xprev.z) then
                //    break; //todo but why nan?

                xnext.zeta := sin(outTheta)/cHere;    //zeta

                //rayPhase := rayPhase + pi; //phase change on reflection
                for fi in solveFreqInds do
                    ReflectionAmplitudePhase[fi] := ReflectionAmplitudePhase[fi] * (-1); //phase change on reflection
            end
            else
                inAir := false;

            //get bottom info at this point
            thisBottom := TSolversGeneralFunctions.getdepth(aZProfile2,xnext.r);
            if thisBottom.z <= 0 then
            begin
                {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'ray ' + IntToStr(itheta) + ' thisBottom.z <= 0' + #13#10;{$ENDIF}
                inc(deaths[kLand]);
                break;
            end;

            if (xnext.z > thisBottom.z) then //todo interpolate zb for range
            begin
                //hit the bottom
                if (inSeafloor) or (xnext.z > thisBottom.z + ds) then
                begin
                    {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'ray ' + IntToStr(itheta) + ' in seafloor' + #13#10;{$ENDIF}
                    inc(deaths[kSeafloor]);
                    break;
                end;

                inSeafloor := true;
                {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'inc(nReflections) ' + floattostr(xnext.z) + ' > ' + FloatToStr(thisBottom.z) + #13#10;{$ENDIF}
                inc(nReflections);

                //kill ray if it has had more than the max number of reflections
                if nReflections >= maxBottomReflections then
                begin
                    {$IF defined (DEBUG) and defined (TESTRAY)}
                    s := s + 'ray ' + IntToStr(itheta) + ' nreflections ' + IntToStr(nReflections) + ' >= ' + IntToStr(maxBottomReflections) + #13#10;
                    {$ENDIF}
                    inc(deaths[kBounces]);
                    break;
                end;

                stepdz := xnext.z - xprev.z;
                stepdr := xnext.r - xprev.r;
                inTheta := arctan2(stepdz,stepdr);
                cHere := c[min(length(c) - 1,max(0,saferound(xprev.z)))]; //todo average for this ray
                theta1 := inTheta - thisBottom.ang;

                //theta2c := ComplexARCCOS(firstSeafloorLayerC/c0*cos(theta1)); //input range can be outside 0 to 1, so we need complex
                outTheta := -(inTheta - 2*thisBottom.ang);
                mwat := stepdz/stepdr;
                cwat := xprev.z - mwat*xprev.r;

                xnext.xi := cos(outTheta)/cHere;
                xnext.r := (cwat - thisBottom.c)/(thisBottom.m - mwat);
                xnext.z := mwat*xnext.r + cwat;
                //if isnan(xnext.r) or isnan(xnext.z) then
                //    break;

                seafloor := seafloors.seafloorForRange(xnext.r,seafloorIndex);

                xnext.zeta := sin(outTheta)/cHere; //zeta
                TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(seafloor,thisBottom.z,layers);
                //layers.remove0
                for fi in solveFreqInds do
                    ReflectionAmplitudePhase[fi] := ReflectionAmplitudePhase[fi] * TMultiLayerReflectionCoefficient.calcR(f[fi], theta1, layers);
            end
            else
                inSeafloor := false;

            //range checks - range < 0 means the ray exited the bounding box to the left
            if (xnext.r < 0) then
            begin
                {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'KLEFT ray ' + IntToStr(itheta) + ' xnext.r ' + floattostr(xnext.r) + #13#10;{$ENDIF}
                inc(deaths[kWest]);
                break;
            end;

            if (xnext.r > rmax) then
            begin
                {$IF defined (DEBUG) and defined (TESTRAY)}s := s + 'KRIGHT ray ' + IntToStr(itheta) + ' xnext.r ' + floattostr(xnext.r) + #13#10;{$ENDIF}
                inc(deaths[kEast]);
                break;
            end;

            iz1 := min(length(c) - 1,max(0,saferound(xnext.z)));
            iz2 := min(length(c) - 1,max(0,saferound(xprev.z)));
            cHere := (c[iz1] + c[iz2])/2; //average c for this step
            if inAir or inSeafloor then
                stepDist := ds
            else
                stepDist := hypot(xnext.r - xprev.r, xnext.z - xprev.z);
            delay := delay + stepDist / cHere; //keep track of time delay, for arrivals calc

            if calculateVolumeAttenuationAtEachStep then
                for fi in solveFreqInds do
                begin
                    localAttenuation := TWaterProps.seawaterAbsorptionCoefficient(waterTemp,f[fi],(xnext.z + xprev.z)/2);
                    localAttenuation := undB20(-localAttenuation*stepDist);
                    ReflectionAmplitudePhase[fi] := ReflectionAmplitudePhase[fi]*localAttenuation;
                end;

            if assigned(ray) then
                ray.addPos2D(xnext,srcX,srcY,angle);

                (*
            if not isnan(maxInitialAmplitude) then
                if (not isnan(minAmplitude())) then
                if ((minAmplitude() / maxInitialAmplitude) < amplitudeCutoff) then
                begin
                    {$IF defined (DEBUG) and defined (TESTING)}
                    write(outFile, 'KAMPLITUDE ray ' + IntToStr(itheta) + ' xnext.r ' + floattostr(xnext.r) + #13#10);
                    {$ENDIF}
                    inc(deaths[kAmplitude]);
                    break;
                end;
                *)

            updateRayParameters();
            {$IF defined (DEBUG) and defined (TESTABEAM)}s := s + floattostr(xnext.q) + #09 + floattostr(xnext.p) + #09 + FloatToStr(xnext.r) + #13#10;{$ENDIF}

            intensityCalcs();
        end;
        fractionDone.d := itheta / ntheta;
    end; //for itheta

    {$IF defined (DEBUG) and defined (RAYOUTPUT)}s := s + #13#10 + '</ray>';{$ENDIF}

    outputTL();

    {$IF defined (DEBUG) and (defined (RAYOUTPUT) or defined(TESTRAY) or defined(TESTABEAM))}THelper.setClipboard(s);{$ENDIF}
end;

end.
