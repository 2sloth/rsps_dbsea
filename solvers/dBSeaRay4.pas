unit dBSeaRay4;

{$B-}

interface

uses
    system.classes, system.math, system.types, system.SysUtils,
    Winapi.Windows,
    vcl.forms, vcl.stdCtrls,
    generics.Collections,
    Mcomplex, baseTypes, myThreading,
    helperFunctions, dBSeaconstants, material,
    multiLayerReflectionCoefficient, seafloorScheme, directivity,
     mcklib, dBSeaRayArrivals,
    waterprops, solversGeneralFunctions, raySolverTypes;

type
  TdBSeaRay4Solver = class(TObject)
  private
    class procedure Loop(const itheta, nf, maxBottomReflections, nz, nr : integer;
                         const zs, c0, initialStepSize, firstSeafloorLayerC, rmax, waterTemp, delTheta, deltaz, deltar : double;
                         const constSSP, calculateVolumeAttenuationAtEachStep, outputArrivals : boolean;
                         const theta, c, dcdz, d2cdz2, r_zb, zb, f : TDoubleDynArray;
                         const seafloorScheme : TSeafloorScheme;
                         const directivityMat : TSingleMatrix;
                         const rayPhase, rayWidth, Abeam : TMatrix;
                         const bPastRayWidthXOver : TBoolMatrix;
                         const ReflectionAmplitudePhase : TComplexMat;
                         const arrivalsArray : array of TArrivalsField;
                         const pFieldMultiFreqArray : array of TComplexField);
  public
    class procedure mainMultipleFrequency(const f : TDoubleDynArray;
                                   const srcZ, rmax, dr, zmax, startInitialAngle, endInitialAngle, initialStepsize : double;
                                   const nz : integer;
                                   const aZProfile, aCProfile : TMatrix;
                                   const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp : double;
                                   const maxBottomReflections, aNTheta : integer;
                                   const returnTL : boolean;
                                   const seafloorScheme : TSeafloorScheme;
                                   const directivities : TDirectivityList;
                                   const fs : double;
                                   const outputArrivals, calculateVolumeAttenuationAtEachStep : boolean;
                                   const cancel : pBoolean;
                                   const pSolveriMax : PInteger;
                                   var tlMultiFreq : TDoubleField{f,i,j};
                                   var pFieldMultiFreq : TComplexField{f,i,j};
                                   var arrivals : TArrivalsField;
                                   var solverErrorMessages : String);  end;


implementation

class procedure TdBSeaRay4Solver.Loop(const itheta, nf, maxBottomReflections, nz, nr : integer;
                         const zs, c0, initialStepSize, firstSeafloorLayerC, rmax, waterTemp, delTheta, deltaz, deltar : double;
                         const constSSP, calculateVolumeAttenuationAtEachStep, outputArrivals : boolean;
                         const theta, c, dcdz, d2cdz2, r_zb, zb, f : TDoubleDynArray;
                         const seafloorScheme : TSeafloorScheme;
                         const directivityMat : TSingleMatrix;
                         const rayPhase, rayWidth, Abeam : TMatrix;
                         const bPastRayWidthXOver : TBoolMatrix;
                         const ReflectionAmplitudePhase : TComplexMat;
                         const arrivalsArray : array of TArrivalsField;
                         const pFieldMultiFreqArray : array of TComplexField);
var
    i, iz, ir, fi, nReflections : integer;
    iz1, iz2, irStart, irEnd : integer;
    theta0, delay, xi, ds : double;
    stepdz, stepdr, inTheta, outTheta, cHere : double;
    theta1, mwat, cwat, stepDist, localAttenuation : double;
    startR, endR, distToRay, magnitude : double;
    theta2c : Tcomplex;
    inSeafloor, inAir : boolean;
    xprev, xnext : TRay2Iterator;
    thisBottom : TGetDepthRec;
    layers : TSeafloorReflectionLayers;

    function z(const iz : integer):double;
    begin
        result := deltaz*iz;
    end;

    function r(const ir : integer):double;
    begin
        result := deltar*ir;
    end;
begin
    theta0 := theta[itheta];
    for fi := 0 to nf - 1 do
    begin
        //ReflectionAmplitudePhase[fi] := makeComplex(1,0); //amplitude starts at 1, will be reduced by each sediment reflection
        ReflectionAmplitudePhase[itheta,fi] := makeComplex( undB20(-directivityMat[fi,itheta]) ,0); //amplitude from the directivity: NB -ve
        rayPhase[itheta,fi] := 0;
        bPastRayWidthXOver[itheta,fi] := false;
    end;
    delay := 0; //keeping track of time taken, for arrivals calc

    {ray initial conditions: r,z,zeta,q,p //xi only changes at reflections so we don't integrate it}
    xnext[0] := 0.0;
    xnext[1] := zs;
    xnext[2] := sin(theta0)/c0;
    xnext[3] := 0;
    xnext[4] := 1/c0;
    xi := cos(theta0)/c0;

    {only really need x for debugging, we could just have xnow and xprev. These calls to increase the length
    by 1 per iteration will be a bit slow, so increase it by 1000 and check later if it needs to increase again}
    //setlength(x,1000,xlen);

    inSeafloor := false;
    inAir := false;
    nReflections := 0;
    ds := initialStepSize;

    while true do
    begin
        for i := 0 to TSolversGeneralFunctions.xlen - 1 do
            xprev[i] := xnext[i];

        {take a step, via 2nd or 4th order runge kutta (depending on whether the SSP is const or not)}
        iz := min(length(c) - 1,max(0,saferound(xprev[1])));
        if constSSP then
            xnext := TSolversGeneralFunctions.rk2StepMCK(xprev,ds,xi,c[iz],dcdz[iz],d2cdz2[iz])
        else
            xnext := TSolversGeneralFunctions.rk4StepMCK(xprev,ds,xi,c[iz],dcdz[iz],d2cdz2[iz]);
        //todo smaller stepsize accuracy checking/rk-fehlberg

        if (xnext[1] < 0) then
        begin
            {ray hit the surface}
            if (inAir) then
                break;
            inAir := true;

            stepdz := xnext[1] - xprev[1];
            stepdr := xnext[0] - xprev[0];
            inTheta := arctan2(stepdz,stepdr);
            outTheta := -inTheta;
            cHere := c[min(length(c) - 1,max(0,saferound(xprev[1])))]; //todo average for this ray
            xi := cos(outTheta)/cHere;
            xnext[0] := xprev[0] - xprev[1]/stepdz*stepdr;
            xnext[1] := 0;
            xnext[2] := sin(outTheta)/cHere;    //zeta

            //rayPhase := rayPhase + pi; //phase change on reflection
            for fi := 0 to nf - 1 do
                ReflectionAmplitudePhase[itheta,fi] := ReflectionAmplitudePhase[itheta,fi] * (-1); //phase change on reflection
        end
        else
            inAir := false;

        //get bottom info at this point
        thisBottom := TSolversGeneralFunctions.getdepth(r_zb,zb,xnext[0]);
        if thisBottom.z <= 0 then
        begin
            break; //stop ray at 0 depth
        end
        else
        if (xnext[1] > thisBottom.z) then //todo interpolate zb for range
        begin
            {hit the bottom}
            if (inSeafloor) then
                break;
            inSeafloor := true;
            nReflections := nReflections + 1;

            if nReflections >= maxBottomReflections then
                break;

            stepdz := xnext[1] - xprev[1];
            stepdr := xnext[0] - xprev[0];
            inTheta := arctan2(stepdz,stepdr);
            cHere := c[min(length(c) - 1,max(0,saferound(xprev[1])))]; //todo average for this ray
            theta1 := inTheta - thisBottom.ang;
            theta2c := ComplexARCCOS(makecomplex(firstSeafloorLayerC/c0*cos(theta1),0)); //input range can be outside 0 to 1, so we need complex
            outTheta := -(inTheta - 2*thisBottom.ang);
            mwat := stepdz/stepdr;
            cwat := xprev[1] - mwat*xprev[0];

            xi := cos(outTheta)/cHere;
            xnext[0] := (cwat - thisBottom.c)/(thisBottom.m - mwat);
            xnext[1] := mwat*xnext[0] + cwat;
            xnext[2] := sin(outTheta)/cHere; //zeta
            layers := TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(seafloorScheme,thisBottom.z);
            for fi := 0 to nf - 1 do
                ReflectionAmplitudePhase[itheta,fi] := ReflectionAmplitudePhase[itheta,fi] * TMultiLayerReflectionCoefficient.calcR(f[fi], theta1, layers);
        end
        else
            inSeafloor := false;

        //range checks - range < 0 means the ray exited the bounding box to the left
        if (xnext[0] < 0) or (xnext[0] > rmax) then
            break;

        iz1 := min(length(c) - 1,max(0,saferound(xnext[1])));
        iz2 := min(length(c) - 1,max(0,saferound(xprev[1])));
        cHere := (c[iz1] + c[iz2])/2; //average c for this step
        stepDist := hypot(xnext[0] - xprev[0],
                          xnext[1] - xprev[1]);
        delay := delay + stepDist / cHere; //keep track of time delay, for arrivals calc

        if calculateVolumeAttenuationAtEachStep then
            for fi := 0 to nf - 1 do
            begin
                localAttenuation := TWaterProps.seawaterAbsorptionCoefficient(waterTemp,f[fi],(xnext[1] + xprev[1])/2);
                localAttenuation := undB20(-localAttenuation*stepDist);
                ReflectionAmplitudePhase[itheta,fi] := ReflectionAmplitudePhase[itheta,fi]*localAttenuation;
            end;

        for fi := 0 to nf - 1 do
        begin
            rayPhase[itheta,fi] := rayPhase[itheta,fi] + (2 * pi * f[fi])/cHere*stepDist;

            if sign(xnext[3]) <> sign(xprev[3]) then
                rayPhase[itheta,fi] := rayPhase[itheta,fi] + pi;  //phase change at caustics

            rayWidth[itheta,fi] := abs(xnext[3]*delTheta);

            if not bPastRayWidthXOver[itheta,fi] then
                if rayWidth[itheta,fi] > pi*cHere/f[fi] then
                    bPastRayWidthXOver[itheta,fi] := true;
            if bPastRayWidthXOver[itheta,fi] then
                rayWidth[itheta,fi] := max(pi*cHere/f[fi],rayWidth[itheta,fi]);

            Abeam[itheta,fi] := 1/power(2*pi,0.25)*sqrt(delTheta/xnext[0]*cHere/c0*2*cos(theta0)/rayWidth[itheta,fi]);
            rayWidth[itheta,fi] := min(thisBottom.z,rayWidth[itheta,fi]); //reduce ray width when the channel is shallow. but keep intensity as it would have been
        end;

        { intensity calcs}
        startR := xprev[0];
        endR := xnext[0];
        //get the approx start and end range indices (much faster than looping over all range indices). could actually calculate the real indices to go to here
        irStart := max(0,safefloor(startR / deltaR) - 1);
        irEnd := min(nr - 1,safeceil(endR / deltaR) + 1);
        for ir := irStart to irEnd do
        //for ir := 0 to nr - 1 do
        begin
            if (r(ir) >= startR) and (r(ir) < endR) then
            begin
                for fi := 0 to nf - 1 do
                    begin
                    {check all the z points that are within the range of the raywidth}
                    for iz := max(0     ,safefloor((min(xnext[1],xnext[1]) - 2*rayWidth[itheta,fi])/deltaz)) to
                              min(nz - 1,safeceil ((max(xnext[1],xnext[1]) + 2*rayWidth[itheta,fi])/deltaz)) do
                    begin
                        distToRay := TSolversGeneralFunctions.distPointToLineSegment(r(ir), z(iz),startR,xprev[1],endR,xnext[1]); //takes account of line segment endpoints

                        if (distToRay < 2*rayWidth[itheta,fi]) then //2*waywidth gets us to 2 StdDev, which is around -30 dB
                        begin
                            //magnitude := (1 - distToRay/rayWidth); //hat function
                            magnitude := exp(-power(distToRay/rayWidth[itheta,fi],2)); //gaussian -- we can speed this line up



                            if outputArrivals then
                            begin
                                {add this arrival to the arrivals list for this point}
                                arrivalsArray[itheta].addArrival(fi,
                                                                 iz,
                                                                 ir,
                                                                 ReflectionAmplitudePhase[itheta,fi].r*Abeam[itheta,fi]*magnitude*cos(rayPhase[itheta,fi]),
                                                                 delay);
                            end
                            else
                            begin
                                {add the contribution from this ray at this point to the output field}
                                pFieldMultiFreqArray[itheta,fi,iz,ir] := pFieldMultiFreqArray[itheta,fi,iz,ir] + ComplexEXP(makeComplex(0,rayPhase[itheta,fi])) *
                                                                                         ReflectionAmplitudePhase[itheta,fi] *
                                                                                         (Abeam[itheta,fi]*magnitude); //includes accumulated reflection amplitude/phase
                            end;
                        end; //distToRay <
                    end; //iz
                end; //fi
            end; //ir in range
        end; //ir
    end; //while
end;



class procedure TdBSeaRay4Solver.mainMultipleFrequency(const f : TDoubleDynArray;
                                   const srcZ, rmax, dr, zmax, startInitialAngle, endInitialAngle, initialStepsize : double;
                                   const nz : integer;
                                   const aZProfile, aCProfile : TMatrix;
                                   const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp : double;
                                   const maxBottomReflections, aNTheta : integer;
                                   const returnTL : boolean;
                                   const seafloorScheme : TSeafloorScheme;
                                   const directivities : TDirectivityList;
                                   const fs : double;
                                   const outputArrivals, calculateVolumeAttenuationAtEachStep : boolean;
                                   const cancel : pBoolean;
                                   const pSolveriMax : PInteger;
                                   var tlMultiFreq : TDoubleField{f,i,j};
                                   var pFieldMultiFreq : TComplexField{f,i,j};
                                   var arrivals : TArrivalsField;
                                   var solverErrorMessages : String);
//NB kill ray after maxBottomReflections reflections from the bottom
const
    defaultNTheta = 1000;
var
    c,dcdz,d2cdz2 : TDoubleDynArray;
    z,r : array of double;
    fi,nf : integer;
    rayPhases, rayWidths : TMatrix;
    bPastRayWidthXOvers : TBoolMatrix;
    Abeams : TMatrix;
    zs,c0 : double;
    deltaz,deltar : double;
    nr : integer;
    zb : TDoubleDynArray;
    i,ir,iz : integer;
    ntheta : integer; // number of rays
    bsteps,itheta : integer;
    ds : double; //initial stepsize
    q,oldx,newx : TDoubleDynArray;
    dcdzin,d2cdz2in : TDoubleDynArray;
    delx1,delx2,denominator : double;
    constSSP : boolean;
    theta,r_zb : TDoubleDynArray;
    delTheta : double;
    ReflectionAmplitudePhases : TComplexMat; //reflection amplitude loss and phase change, accumulated over reflections
    directivityMat : TSingleMatrix;
    thisModulusSquared : double;
    arrivalsArray : array of TArrivalsField;
    pFieldMultiFreqArray : array of TComplexField;
    task : ITask;
begin
    solverErrorMessages := '';

    {water properties}
    c0 := TMaterial.water.c;

    nr := saferound(rmax/dr);
    if nr < 1 then
        nr := 1;
    pSolveriMax^ := nr;
    zs := srcZ;
    {takeoff angles}
    ntheta := aNTheta; // number of rays
    if ntheta = 0 then //The user may select 0 for default
        ntheta := defaultNTheta;
    ds := initialStepsize; //initial stepsize

    //setting all needed multi-freq arrays
    nf := length(f);
    setlength(rayPhases,ntheta,nf);
    setlength(rayWidths,ntheta,nf);
    setlength(bPastRayWidthXOvers,ntheta,nf);
    setlength(Abeams,ntheta,nf);
    setlength(ReflectionAmplitudePhases,ntheta,nf);

    deltaz   := zmax / (nz - 1);
    setlength(z,nz);
    for i := 0 to nz - 1 do
        z[i] := i*deltaz;

    deltar := dr;//rmax / (nr - 1);
    if deltar = 0 then
        deltar := 1;
    setlength(r,nr);
    for i := 0 to nr - 1 do
        r[i] := i*deltar;

    {ranges of seafloor depths}
    bsteps := length(aZProfile[0]);
    setlength(r_zb,bsteps);
    setlength(zb,bsteps);
    for i := 0 to bsteps - 1 do
    begin
        r_zb[i] := aZProfile[0,i];
        zb[i]   := aZProfile[1,i];
    end;

    {sound speed profile and 1st and 2nd z derivatives by unequal finite difference, as the
    input points are maybe not equally spaced}
    setlength(q,length(aCProfile[0]));
    setlength(oldx,length(aCProfile[0]));
    for i := 0 to length(aCProfile[0]) - 1 do
    begin
        oldx[i] := aCProfile[0,i];
        q[i] := aCProfile[1,i];
    end;

    setlength(dcdzin,length(oldx));
    setlength(d2cdz2in,length(oldx));
    if length(oldx) = 2 then
    begin
        {use different calcs if only 2 input points}
        dcdzin[0] := (q[1] - q[0])/(oldx[1] - oldx[0]);
        dcdzin[1] := dcdzin[0];
        d2cdz2in[0] := 0;
        d2cdz2in[1] := 0;
    end
    else
    begin
        {more than 2 points in profile}

        {first point}
        iz := 0;
        delx1 := oldx[iz + 1] - oldx[iz];
        delx2 := oldx[iz + 2] - oldx[iz + 1];
        denominator := delx1*delx2*(delx1+delx2);
        dcdzin[iz] := (-delx1*delx1*q[iz + 2] +
                       power((delx1 + delx2),2)*q[iz + 1] -
                       (delx2*delx2 + 2*delx1*delx2)*q[iz]) /
                       denominator;
        d2cdz2in[iz] := 2*(delx1*q[iz + 2] -
                        (delx1 + delx2)*q[iz + 1] +
                        delx2*q[iz]) /
                        denominator;

        {all points except first and last}
        for iz := 1 to length(oldx) - 2 do
        begin
            delx1 := oldx[iz] - oldx[iz - 1];
            delx2 := oldx[iz + 1] - oldx[iz];
            denominator := delx1*delx2*(delx1+delx2);
            dcdzin[iz] := (delx1*delx1*q[iz + 1] +
                          (delx2*delx2 - delx1*delx1)*q[iz] -
                          delx2*delx2*q[iz - 1]) /
                          denominator;
            d2cdz2in[iz] := 2*(delx1*q[iz + 1] -
                            (delx1 + delx2)*q[iz] +
                            delx2*q[iz - 1]) /
                            denominator;
        end;
        {last points}
        iz := length(oldx) - 1;
        delx1 := oldx[iz - 1] - oldx[iz - 2];
        delx2 := oldx[iz    ] - oldx[iz - 1];
        denominator := delx1*delx2*(delx1+delx2);
        dcdzin[iz] := ((delx1*delx1 + 2*delx1*delx2)*q[iz] -
                        power((delx1 + delx2),2)*q[iz - 1] +
                        delx2*delx2*q[iz - 2]) /
                        denominator;
        d2cdz2in[iz] := 2*(delx1*q[iz] -
                          (delx1 + delx2)*q[iz - 1] +
                          delx2*q[iz - 2]) /
                          denominator;
    end;

    {if constant SSP, we can use less stringent integration methods}
    constSSP := true;
    for iz := 0 to length(oldx) - 1 do
        if dcdzin[iz] <> 0 then
            constSSP := false;

    {interpolate c, dcdz and d2cdz2 so that there is 1m spacing between points, so we can
    easily lookup the appropriate index}
    setlength(newx,safeceil(oldx[length(oldx) - 1]));
    for i := 0 to length(newx) - 1 do
        newx[i] := i;
    c := TSolversGeneralFunctions.myinterp(newx, oldx, q);
    dcdz := TSolversGeneralFunctions.myinterp(newx, oldx, dcdzin);
    d2cdz2 := TSolversGeneralFunctions.myinterp(newx, oldx, d2cdz2in);

    {angles}
    TSolversGeneralFunctions.setupRayAngles(startInitialAngle,endInitialAngle,nTheta,theta,delTheta);
    directivityMat := TSolversGeneralFunctions.getDirectivityAsMatrix(f,theta,directivities);

    if outputArrivals then
    begin
        if assigned(arrivals) then
            arrivals.reinitialise(nf,nz,nr,fs)
        else
            arrivals := TArrivalsField.Create(nf,nz,nr,fs);

        for itheta := 0 to ntheta - 1 do
            arrivalsArray[itheta] := TArrivalsField.create(nf,nz,nr,fs);
    end
    else
    begin
        setlength(pFieldMultiFreq,nf,nz,nr);
        for fi := 0 to nf - 1 do
            for ir := 0 to nr - 1 do
                for iz := 0 to nz - 1 do
                begin
                    pFieldMultiFreq[fi,iz,ir].r := 0;
                    pFieldMultiFreq[fi,iz,ir].i := 0;
                end;

        setlength(pFieldMultiFreqArray,ntheta,nf,nz,nr);
    end;

    try
        task := TTask.create(procedure //wrap in TTask to keep off the main thread
        begin
            TParallel.for(0,ntheta - 1,procedure (itheta : integer)
            begin
                TdBSeaRay4Solver.Loop(itheta, nf, maxBottomReflections, nz, nr,
                               zs, c0, initialStepSize, firstSeafloorLayerC, rmax, waterTemp, delTheta, deltaz, deltar,
                               constSSP, calculateVolumeAttenuationAtEachStep, outputArrivals,
                               theta, c, dcdz, d2cdz2, r_zb, zb, f,
                               seafloorScheme,
                               directivityMat,
                               rayPhases, rayWidths, Abeams,
                               bPastRayWidthXOvers,
                               ReflectionAmplitudePhases,
                               arrivalsArray,
                               pFieldMultiFreqArray);
            end);
        end);
        task.start;
        while task.status <> TTaskStatus.Completed do
        begin
            Application.ProcessMessages;
            sleep(40);
        end;
    finally
        if outputArrivals then
        begin
            for itheta := 0 to ntheta - 1 do
            begin
                arrivalsArray[itheta].appendToAnotherList(arrivals);
                arrivalsArray[itheta].Free;
            end;
        end
        else
        begin
            for itheta := 0 to ntheta - 1 do
                for fi := 0 to nf - 1 do
                    for ir := 0 to nr - 1 do
                        for iz := 0 to nz - 1 do
                            pFieldMultiFreq[fi,iz,ir] := pFieldMultiFreq[fi,iz,ir] + pFieldMultiFreqArray[itheta,fi,iz,ir];
        end;
    end;

    if (not outputArrivals) and returnTL then
    begin
        setlength(tlMultiFreq,nf,nz,nr);
        for fi := 0 to nf - 1 do
        begin
            for ir := 0 to nr - 1 do
            begin
                for iz := 0 to nz - 1 do
                begin
                    thisModulusSquared := pFieldMultiFreq[fi,iz,ir].modulusSquared;
                    if thisModulusSquared > 0 then
                        tlMultiFreq[fi,iz,ir] := -10*log10(thisModulusSquared)
                    else
                        tlMultiFreq[fi,iz,ir] := 0;
                end;
            end;
        end;
    end;
end;

end.
