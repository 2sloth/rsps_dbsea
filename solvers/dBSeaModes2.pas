unit dBSeaModes2;

{intention of this version is to include backscatter}

{$B-}

interface

uses
    system.classes, system.math, Mcomplex, system.sysutils,
    system.types,
    baseTypes,
    helperFunctions, mmsystem, dBSeaconstants, seafloorScheme,
    material, solversGeneralFunctions, mcklib, globalsUnit;

type

  TdBSeaModes2Solver = class(TObject)
  public
  var
    class procedure main(const f, srcZ, rmax, dr, zmax: double;
                         const dMax: integer;
                         const aZProfile2: TRangeValArray;
                         const aCProfile2: TDepthValArray;
                         const seafloorScheme: TSeafloorScheme;
                         const maxModes: integer;
                         const zOversamplingFactor, rOversamplingFactor: double;
                         const stopAtLand: boolean;
                         const cancel: TWrappedBool;
                         const fractionDone: TWrappedDouble;
                         out tl: TMatrix;
                         var solverErrors: TSolverErrors);
  end;

implementation

{NB inverse of diagonal matrix just has the inverse of each of the diagonal terms}

class procedure  TdBSeaModes2Solver.main(const f, srcZ, rmax, dr, zmax: double;
                                         const dMax: integer;
                                         const aZProfile2: TRangeValArray;
                                         const aCProfile2: TDepthValArray;
                                         const seafloorScheme: TSeafloorScheme;
                                         const maxModes: integer;
                                         const zOversamplingFactor, rOversamplingFactor: double;
                                         const stopAtLand: boolean;
                                         const cancel: TWrappedBool;
                                         const fractionDone: TWrappedDouble;
                                         out tl: TMatrix;
                                         var solverErrors: TSolverErrors);
const
    dSubDepth1DistFactor = 0.5;
    nlam2 = 50; //number of points for smaller local search
var
    zs,c0,deltar: double;
    nz,nzInit,nm,nr,nrInit,il,iz,ir,im,isd,nmInit,nEigs: integer;

    izb: TIntegerDynArray;
    q, zb, rhozprev: TDoubleDynArray;
    z,r: TDoubleDynArray;
    psi,psiprev: TMatrix;
    cq,rhozq,attenq: TDoubleDynArray;
    tmpPsi1, tmpPsi2, mdmid: TDoubleDynArray; //temp arrays to avoid setlength()

    attCConst,omega,h,hh: double;
    qsum1,qsum2: double;

    Dd,Ed,Ed1,Ed2: TDoubleDynArray;
    theEigs,c1: TDoubleDynArray;

    nlam: integer;
    iBottomExtension: double;
    upperEigLimit, cmin: double;

    rprev,thisModulusSquared: double;
    Ctilde: TMatrix;
    a,b,kc,kcprev,H0z,H01: TComplexVect;
    p,Chat: TComplexMat;
    //H2s                             : array of TComplexVect;
    //R1m,R2m,R3m,R4m                 : array of TComplexVect;
    H1,R1,R1tmp       : TComplexVect;
    tmpPForSourceScaling: TComplex;
    //H2,H2inv             : TComplexVect;

    aMaterial: TMaterial;
    SubDepth1,SubDepth2,sqrttRprevOverR,rMinusRPrev: double;
    zOverSamp,rOverSamp: integer;
    bProfileChange,bLandReachedOrNoModes: boolean;
    outputScaling: double;

    //backscatter variables
    //H2s          : array of TComplexVect;
    //H2,H2inv     : TComplexVect;

    function additionalAttenuationInBottomLayer(const z: double): double;
    //following eq 6.146, ie Brock, the aesd parabolic equation mode
    //resulting atten is an exponential func going from 0 to alpha
    const
        alpha = 0.01;
    var
        qq: double;
    begin
        qq := (z - SubDepth2) / ((subdepth2 - subdepth1) / 3); {D = (subdepth2 - subdepth1) / 3}
        result := exp(-qq*qq) * alpha;
    end;

    function verticalProfilesThisRange(const ir: integer): boolean;
    var
        iz: integer;
    begin
        result := false;
        for iz := 0 to nz - 1 do
        begin
            if (z[iz] < zb[ir]) then
            begin
                {water}
                cq[iz] := c1[iz];  //fill lookup tables with the speed, density, attenuation at those points
                rhozq[iz] := 1;
                attenq[iz] := 0;
            end
            else
            begin
                {sediment}
                //need to do lookup of values from container.scenario.seaFloorScheme
                aMaterial := seafloorScheme.getMaterialAtDepthBelowSurface(z[iz] , zb[ir]);
                if not assigned(aMaterial) then
                begin
                    result := true;
                    aMaterial := water;
                end;
                cq[iz] := aMaterial.c;
                rhozq[iz] := aMaterial.rho_g;
                attenq[iz] := aMaterial.attenuation*attCConst/aMaterial.c;
            end;

            {increase the attenuation at the lower part of the sediment to kill reflections from bottom}
            if (z[iz] > subDepth1) then
                attenq[iz] := attenq[iz] + additionalAttenuationInBottomLayer(z[iz]);
        end; //iz
    end;
begin
    omega := 2 * pi * f;  //angular freq
    c0 := water.c;

    iBottomExtension := 2;
    if (zmax * iBottomExtension) < c0 / f then
        iBottomExtension := c0 / f / zmax;

    nm := 0;
    zs := srcZ;   //source depth
    nlam := round(TSolversGeneralFunctions.nLamIncrease(f) * zmax * iBottomExtension);  //number of points to search along the real axis for eigenvalues

    {sub bottom sediment stuff}
    SubDepth1 := zmax * (iBottomExtension - dSubDepth1DistFactor); //depth where the attenuation increases
    SubDepth2 := zmax * iBottomExtension;

    {depths}
    zOverSamp := max(1, round(zOversamplingFactor));
    nzInit := round(iBottomExtension * dMax);
    nz  := 1 + (nzInit - 1)*zOverSamp;
    h   := SubDepth2 / (nz-1);
    hh  := h * h;
    setlength(z,nz);
    for iz := 0 to nz - 1 do
        z[iz] := iz*h;

    {sound speed profile and wavenumbers}
    setlength(c1,nz);
    for iz := 0 to nz - 1 do
        c1[iz] := TSolversGeneralFunctions.modalInterpolationFn(aCProfile2,z[iz]);

    {ranges}
    nrInit := max(1, saferound(rmax/dr,1));
    rOverSamp := max(1, round(rOversamplingFactor));
    nr := max(1, 1 + (nrInit - 1)*rOverSamp);
    deltar := rmax / (nr - 1 + 1);
    if deltar = 0 then
        deltar := 1;
    setlength(r,nr);
    for ir := 0 to nr - 1 do
        r[ir] := (ir + 1)*deltar;

    {bottom profile}
    setlength(zb,nr);
    setlength(izb,nr);
    for ir := 0 to nr - 1 do
    begin
        zb[ir] := TSolversGeneralFunctions.modalInterpolationFn(aZProfile2,r[ir]);
        izb[ir] := saferound(zb[ir]/h); //index of zbottom
    end;

    attCConst := 1/(20*log10(exp(1))/f); //convert to nepers/m    eq 6.156 (lambda = c / f and we include c(z) later)
    isd := saferound(zs/h); // index of source depth

    outputScaling := 1;
    setlength(cq,nz);
    setlength(rhozq,nz);
    setlength(attenq,nz);
    setlength(q,nz);
    setlength(Dd,nz);
    setlength(Ed,nz);
    setlength(Ed1,nz);
    setlength(Ed2,nz);
    setlength(p,dMax,nrInit);
    setlength(tl,dMax,nrInit);

    if (isd < 0) or (isd >= nz) then
    begin
        include(solverErrors,kSourceBelowSubDepth);
        exit;
    end;

    //TODO support for changing seafloor scheme in range
    if verticalProfilesThisRange(0) then //returns true if there is a problem with the sediment
        include(solverErrors,kNoSediment);

    cmin := c1.min; //the minimum speed anywhere in the problem.
    lesser(cmin,seafloorScheme.getMinimumSpeed);
    if isnan(cmin) then
        cmin := 1500;
    upperEigLimit := 1.01*omega*omega/cmin/cmin;  //no eigenvales will be found above k0^2
    nmInit := 999999; //set it large so it gets picked up by the min function
    rprev := r[0];  //the location of the start of the current flat depth profile
    rhozprev := rhozq.copy;//MatrixRow(rhoz,0); //rho profile of first step
    //rnext := r[1];
    bProfileChange := false;  //no need to recalc coupling on first step
    bLandReachedOrNoModes := false;  //once we reach land, we stop calculating
    //if nr > 50 then
    //    framSolve.lblSolveExtraInfo.Visible := true;
    rMinusRPrev := 0;
    sqrttRprevOverR := 0;
    for ir := 0 to nr - 1 do
    begin
        verticalProfilesThisRange(ir);

        if isnan(zb[ir]) or (stopAtLand and (zb[ir] <= 0)) then
            bLandReachedOrNoModes := true; //will never get set back to false

        if bLandReachedOrNoModes then
            continue;

        if ir > 0 then
        begin
            sqrttRprevOverR := sqrt(rprev/r[ir]); //used later to speed up H1 calc. is referenced to the previous profile
            rMinusRPrev := r[ir] - rprev;         //used later to speed up H1 calc. is referenced to the previous profile

            if (izb[ir] <> izb[ir - 1]) then
            begin
                bProfileChange := true;

                {store the previous psi and k before we recalc them.
                these are used in calculating the coupling across an interface}
                for im := 0 to nm - 1 do
                begin
                    kcprev[im] := kc[im];
                    for iz := 0 to nz - 1 do
                        psiprev[iz,im] := psi[iz,im];
                end;
            end //change in izb
            else
                bProfileChange := false;  //still within old profile
        end; //ir>0

        if (ir = 0) or bProfileChange then
        begin
            for iz := 0 to nz - 1 do
            begin
                {create the matrix diagonals. for details on the matrix see Jensen 5.107}
                //5.108 etc are scaled by 1/h instead of 1/hh, is that ok?
                q[iz] := omega*omega/cq[iz]/cq[iz];
                Dd[iz] := (-2/hh + q[iz])/rhozq[iz]; //diagonal
                Ed[iz] := 1/rhozq[iz]/hh;            //upper and lower diag
            end;

            theEigs := TSolversGeneralFunctions.tridEigs(Ed,Dd,Ed,nlam,nlam2,upperEigLimit,maxModes); //get estimates of k^2
            nEigs := length(theEigs);
            if nEigs = 0 then
                bLandReachedOrNoModes := true; //no more propagation if there are no modes
            if (ir = 0) then
            begin
                if nEigs = 0 then
                    include(solverErrors,kNoModes);

                {ir = 0, initialise stuff}
                nm := length(theEigs);
                nmInit := nm;
                setlength(kc,nm);
                setlength(H0z,nm);
                setlength(H01,nm);
                setlength(a,nm);
                setlength(b,nm);
                //setlength(alist,nm,nr);
                //setlength(blist,nm,nr);
                setlength(Ctilde,nm,nm);
                setlength(Chat,nm,nm);
                setlength(H1,nm);
                //setlength(H2,nm);
                //setlength(H2inv,nm);
                setlength(psi,nz,nm);
                setlength(psiprev,nz,nm);
                //setlength(psic,nz,nm);
                //setlength(psicprev,nz,nm);
                setlength(kcprev,nm);
                setlength(R1,nm);
                setlength(R1tmp,nm);
                //setlength(H2s,nm,nr);
            end
            else
            begin
                //TODO don't truncate the eigs, use all calced ones

                {ir > 0, zero pad or truncate the eigenvalues}
                if length(theEigs) > nmInit then
                    setlength(theEigs,nmInit); //truncate modes outside of our calced range from the first step

                if (length(theEigs) < nmInit) then
                begin
                    setlength(theEigs,nmInit);
                    for im := nEigs to nmInit - 1 do
                        theEigs[im] := 0; //zero pad. but be careful later to avoid divide by zero
                end;
            end;

            for im := 0 to nm - 1 do
                TSolversGeneralFunctions.modesKcAndPsi(im, nz, omega, h,
                                                       theEigs, Dd, Ed, rhozq, attenq, cq,
                                                       tmpPsi1, tmpPsi2, mdmid,
                                                       kc, psi);
        end; //(ir = 0) or bProfileChange


        {If the depth (z index of seafloor) has changed from the last step, we need to recalculate coupling
        matrices to get the 'a' modal amplitudes at the new interface}
        if bProfileChange then
        begin
            for il := 0 to nmInit - 1 do
            begin
                for im := 0 to nmInit - 1 do
                    if im = il then   //remove if you want off diag terms - currently we are only doing adiabatic
                    begin
                        if (il >=nm) or (im >=nm) or //check for null values
                            (kc[im] = NullComplex) or
                            (kcprev[il] = NullComplex) or
                            kc[im].isNaN or
                            kcprev[il].isNaN then
                        begin
                            Ctilde[il,im] := 0;
                            Chat[il,im] := NullComplex;
                        end
                        else
                        begin
                            qsum1 := 0;
                            qsum2 := 0;
                            for iz := 0 to nz - 1 do
                            begin
                                qsum1 := qsum1 + psi[iz,il]*psiprev[iz,im] / rhozq[iz];//rhoz and first psi at j+1
                                qsum2 := qsum2 + psi[iz,il]*psiprev[iz,im] / rhozprev[iz]; //first psi at j+1
                            end;

                            Ctilde[il,im] := qsum1; //Ctilde is always real
                            Chat[il,im] := kc[im] / kcprev[il] * qsum2; //Chat is complex due to wavenumbers
                        end;
                    end;//im

                if (il >= nm) or
                    (kc[il] = NullComplex) or
                    kc[il].isNaN then
                    R1[il] := NullComplex
                else
                    R1tmp[il] := (Chat[il,il] + Ctilde[il,il]) * 0.5;
            end;//il
        end; // new depth profile

        if ir = 0 then
        begin
            {first step, calc first modal amplitudes}

            for im := 0 to nm - 1 do
            begin
                if kc[im].isNaN then continue;

                H0z[im] := kc[im]*r[0];
                H01[im] := ComplexSQRT( makeComplex(2/pi,0)/H0z[im]) * ComplexEXP( makeComplex(0,1) * (H0z[im] - pi/4));
                a[im] := MakeComplex(0,1/4/rhozq[isd]) * psi[isd,im] * H01[im];
                for iz := 0 to dmax - 1 do
                    p[iz,0] := p[iz,0] + a[im]*psi[iz * zOverSamp,im];
            end;

            //outputScaling. We want the levels at r[0] and zs to be correctly scaled, given spherical spreading from the source.
            //Target pressure is found from the 20log(rb) + 10glog(r/rb)
            //get the greatest pressure at this range
            tmpPForSourceScaling := NullComplex;
            for iz := 0 to dmax - 1 do
                if p[iz,0].modulusSquared > tmpPForSourceScaling.modulusSquared then
                    tmpPForSourceScaling := p[iz,0];
            if tmpPForSourceScaling.modulusSquared = 0 then
                outputScaling := 1
            else
                outputScaling := TSolversGeneralFunctions.targetPressureAtRange(r[0],srcZ) / tmpPForSourceScaling.modulus;

            for iz := 0 to dmax - 1 do
                p[iz,0] := p[iz,0] * outputScaling;
        end
        else
        begin
            {ir > 0, march solution out in range}
            for im := 0 to nm - 1 do
            begin
                if (im >= nm) or
                    (kc[im] = NullComplex) or
                    kc[im].isNaN then
                begin
                    H1[im] := NullComplex;
                    a[im] := NullComplex;
                    {and add nothing to pressure field}
                end
                else
                begin
                    H1[im] := ComplexEXP(MakeComplex(-kc[im].i,kc[im].r) * rMinusRPrev) * sqrttRprevOverR; {eq 5.242}
                    if not bProfileChange then
                    begin
                        {no need to change 'a', just carry on using the hankel functions}
                        if (ir mod rOverSamp = 0) then
                            for iz := 0 to dMax - 1 do
                                p[iz,ir div rOverSamp] := p[iz,ir div rOverSamp] + a[im]*H1[im]*psi[iz*zOverSamp,im]*outputScaling; //eq 5.239 {calculate the contributions to the pressure field from each mode}
                    end
                    else
                    begin
                        a[im] := R1tmp[im] * H1[im] * a[im];  //use H1 based on old depth profile from last interface
                        if (ir mod rOverSamp = 0) then
                            for iz := 0 to dMax - 1 do
                                p[iz,ir div rOverSamp] := p[iz,ir div rOverSamp] + a[im]*psi[iz*zOverSamp,im]*outputScaling; //H1 = 1 based on new profile {calculate the contributions to the pressure field from each mode}
                    end;
                end; //mode is ok
                //alist[im,ir] := a[im];
            end; //im
        end; //ir > 0

        {calcs are done, we can now update rprev and rhozprev if we are at an interface}
        if bProfileChange then
        begin
            rprev := r[ir];    //rprev changes at interfaces between different depths, ie we need to know how far we have travelled past the last interface
            rhozprev := rhozq.copy; //MatrixRow(rhoz,ir);
        end;

        //if assigned(updateLabelProc) and (ir mod 500 = 0) then updateLabelProc('Marching solution in range. Step ' + tostr(ir) + ' of ' + tostr(nr));
        if (cancel <> nil) and cancel.b then exit;
        fractionDone.d := ir / nr;
    end;// ir

    for ir := nr - 2 downto 1 do
    begin
//        for im := 0 to nm - 1 do
//            b[im,ir] = diag(inv(R4s(:,:,ir))).*(b(ir + 1) - R3s(:,:,ir)*a(:,ir));
//
//        if ir == 2
//            rmin2 = r(ir - 1);
//            rmin1 = r(ir - 1);
//        elseif ir == 1
//            rmin2 = r(ir);
//            rmin1 = r(ir);
//        else
//            rmin2 = r(ir - 2);
//            rmin1 = r(ir - 1);
//        end
//        for im = 1:nm
//            //H2(im,im) = sqrt(rmin1/r(ir))*exp(%i*kprev(im)*(rmin1-r(ir))); //todo saved H2 from prev
//            //p(:,ir) = p(:,ir) + psi(:,im)*b(im,ir)*H2(im,im);
//            p(:,ir) = p(:,ir) + psi(:,im)*b(im,ir)*H2s(im,im,ir);
//        end
    end;

    for iz := 0 to dMax - 1 do
    begin
        for ir := 0 to nrInit - 1 do
        begin
            thisModulusSquared := p[iz,ir].modulusSquared;
            if (thisModulusSquared > 0) and (thisModulusSquared <= 1) then
                tl[iz,ir] := -10*log10(thisModulusSquared)
            else
                tl[iz,ir] := nan;
        end;
    end;
end;

end.
