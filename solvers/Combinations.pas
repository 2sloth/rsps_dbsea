unit Combinations;

interface

uses system.generics.collections, system.types, mcomplex;

type
    TMask = integer;
    TMasks = TList<TMask>;

    TMaskHelper = record helper for TMask
    public
        function IsBitSet(const TheBit: Byte): Boolean;
        function BitOn(const TheBit: Byte): TMask;
        function BitOff(const TheBit: Byte): TMask;
        function BitToggle(const TheBit: Byte): TMask;
        function nSet(const n: integer): integer;
        function kthValueOff(const n,k: integer): TMask;

        function product(const omit: integer; const vect: TComplexVect): TComplex;
    end;

    TWrappedIntegerDynArray = class(TObject)
    private
        procedure clone(const o: TWrappedIntegerDynArray);
    public
        arr: TIntegerDynArray;
        k: integer;

        constructor Create(n: integer);
        constructor CreateClone(wrap: TWrappedIntegerDynArray);

        function contains(const a: Integer): boolean;
        function product(const omit: integer; const vect: TComplexVect): TComplex;
        procedure resize(n: integer);
    end;

    TMaskProc = reference to procedure(mask: TMask);
    TWrappedIntegerArrProc = reference to procedure(mask: TWrappedIntegerDynArray);

    TCombinations_mask = class(TObject)
    public
    var
        list: TMasks;

        constructor Create();
        destructor Destroy; override;

        function combinations(n, k0, len, startPosition: integer; wrap: TMask): TMask;
        procedure forEach(const i: integer; f: TMaskProc);
    end;

    TCombinations = class(TObject)
    public
    var
        list: TObjectList<TWrappedIntegerDynArray>;

        constructor Create();
        destructor Destroy; override;

        function combinations(n, k0, len, startPosition: integer; wrap: TWrappedIntegerDynArray): TWrappedIntegerDynArray;
        procedure forEach(const i: integer; f: TWrappedIntegerArrProc);
    end;

implementation

function TMaskHelper.IsBitSet(const TheBit: Byte): Boolean;
begin
  Result := (self and (1 shl TheBit)) <> 0;
end;

function TMaskHelper.nSet(const n: integer): integer;
var
    i: integer;
begin
    result := 0;
    for i := 0 to n - 1 do
        if IsBitSet(i) then
            inc(result);
end;

function TMaskHelper.kthValueOff(const n, k: integer): TMask;
var
    i, m: integer;
begin
    m := k;
    for i := 0 to n - 1  do
        if IsBitSet(i) then
        begin
            if m = 0 then
                exit(BitOff(i))
            else
                dec(m);
        end;
    exit(self);
end;

function TMaskHelper.product(const omit: integer; const vect: TComplexVect): TComplex;
var
    i: integer;
begin
    result := 1;
    for i := 0 to length(vect) - 1 do
        if IsBitSet(i) and (i <> omit) then
            result := result * vect[i];
end;

function TMaskHelper.BitOn(const TheBit: Byte): TMask;
begin
  Result := self or (1 shl TheBit);
end;

function TMaskHelper.BitOff(const TheBit: Byte): TMask;
begin
  Result := self and ((1 shl TheBit) xor $FFFFFFFF);
end;

function TMaskHelper.BitToggle(const TheBit: Byte): TMask;
begin
  Result := self xor (1 shl TheBit);
end;

function TCombinations_mask.combinations(n, k0, len, startPosition: integer; wrap: TMask): TMask;
var
    i: integer;
begin
    if (len = 0) then begin
        list.add(wrap);
        exit(wrap);
    end;

    for i := startPosition to n - len do
    begin
        wrap := wrap.kthValueOff(n,k0 - len);
        wrap := wrap.BitOn(i); //result[k0 - len] = arr[i];
        wrap := combinations(n, k0, len-1, i+1, wrap);
    end;
    exit(wrap);
end;

constructor TCombinations_mask.Create;
begin
    inherited;

    list := TMasks.create;
end;

destructor TCombinations_mask.Destroy;
begin
    list.Clear;
    inherited;
end;

procedure TCombinations_mask.forEach(const i: integer; f: TMaskProc);
var
    w: TMask;
begin
    for w in list do
        if (i = -1) or w.IsBitSet(i) then
            f(w);
end;

{ TWrappedIntegerDynArray }

procedure TWrappedIntegerDynArray.clone(const o: TWrappedIntegerDynArray);
var
    i: integer;
begin
    for i := 0 to k - 1 do
        arr[i] := o.arr[i];
end;

function TWrappedIntegerDynArray.product(const omit: integer; const vect: TComplexVect): TComplex;
var
    i: integer;
begin
    result := 1;
    for i in arr do
        if i <> omit then
            result := result * vect[i];
end;

procedure TWrappedIntegerDynArray.resize(n: integer);
begin
    setlength(arr, 0);
    setlength(arr, n);
    self.k := n;
end;

function TWrappedIntegerDynArray.contains(const a: Integer): boolean;
var
    j: integer;
begin
    for j in self.arr do
        if a = j then
            exit(true);
    result := false;
end;

constructor TWrappedIntegerDynArray.Create(n: integer);
begin
    setlength(self.arr, n);
    k := n;
end;

constructor TWrappedIntegerDynArray.CreateClone(wrap: TWrappedIntegerDynArray);
begin
    setlength(self.arr, wrap.k);
    k := wrap.k;
    self.clone(wrap);
end;

{ TCombinations }

function TCombinations.combinations(n, k0, len, startPosition: integer; wrap: TWrappedIntegerDynArray): TWrappedIntegerDynArray;
var
    i: integer;
begin
    if (len = 0) then begin
        list.add(wrap);
        exit(TWrappedIntegerDynArray.CreateClone(wrap));
    end;

    for i := startPosition to n - len do
    begin
        wrap.arr[k0 - len] := i;
        wrap := combinations(n, k0, len-1, i+1, wrap);
    end;
    exit(wrap);
end;

constructor TCombinations.Create;
begin
    inherited;

    list := TObjectList<TWrappedIntegerDynArray>.create(true);
end;

destructor TCombinations.Destroy;
begin
    list.Free;
  inherited;
end;

procedure TCombinations.forEach(const i: integer; f: TWrappedIntegerArrProc);
var
    w: TWrappedIntegerDynArray;
begin
    for w in list do
        if (i = -1) or w.contains(i) then
            f(w);
end;

end.
