unit PESolverTypes;

interface

uses system.types, mcomplex;

type

    TPESolverStarter = class
    var
        isOK : boolean;
        psi : TComplexVect;
        z : TDoubleDynArray;

        constructor create;
    end;

implementation

constructor TPESolverStarter.create;
begin
    isOK := false;
end;

end.
