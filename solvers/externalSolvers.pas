unit externalSolvers;

interface

uses system.SysUtils, system.Math, system.Strutils, system.Classes,
    baseTypes, pos3, helperFunctions,
    seafloorscheme, mcklib, material, SmartPointer;

    type
    TExternalSolvers = class(TObject)
        class function writeRAMInput(const f, zs, rmax, dr: double; const aPos: TFPoint3; const aZProfile, aCProfile: TMatrix; const includeFreq: boolean): string;
        class function writeBellhopInput(const f, zs, rmax, dr: double; const aPos: TFPoint3; const aCProfile: TMatrix): string;
        class function writeBellhopBathymetry(const aZProfile: TMatrix): string;
        class function writeCSNapInput(const f, zs, rmax, dr: double; const aPos: TFPoint3; const aZProfile, aCProfile: TMatrix): string;
        class function readCSnapReturnFile(const Filename: string; delim: char; iHSkip, iVSkip: integer): TMatrix;
        class function writeKrakenInput(const f, zs, rmax, dr, localdepth, maxdepth: double; const aPos: TFPoint3; const aCProfile: TMatrix): string;
        class function writeFieldInput(const f, zs, rmax, dr: double; const iLastWaterInd: integer; const aZProfile: TMatrix): string;
        class function trimSolverReturnString(const s: string): string;
    end;

implementation

uses problemContainer;

class function    TExternalSolvers.trimSolverReturnString(const s : string):string;
{the string returned by solver may have spurious lines at the beginning,
so we need to remove those before parsing the transmission losses.
also return empty string if the solver return did not contain the
TL Data substring, ie if it failed in any way - somewhat common
with Kraken}
var
    //i, imax : integer;
    sl1: ISmart<TStringlist>;
begin
    result := '';

    sl1 := TSmart<TStringlist>.create(TStringlist.Create);
    sl1.Text := s;

    {check whether the solver has returned TL Data, if so strip out everything up to that point}
    if AnsiContainsText (s,' ---TL Data---') then
    begin

        {note that we are getting an added space at the start of the line, compared
        to what is in the fortran code}
        while ansicomparestr(sl1[0],' ---TL Data---') <> 0 do
        begin
            //showmessage('|' + sl1[0] + '|,| ---TL Data---|');
            sl1.Delete(0);
        end;
        sl1.Delete(0);

        result := sl1.text;
    end;
end;

class function TExternalSolvers.writeRAMInput(const f, zs, rmax, dr: double; const aPos: TFPoint3; const aZProfile, aCProfile :TMatrix; const includeFreq: boolean): string;
{create the input to ramGEO (actually ramMDAn.exe) as a string. the full documentation for
the ramGEO input file is found online (ram.pdf). it is all written as one long string
containing line breaks, ie #13#10 (on windows)}
const
    subDepth2Attenuation = 50.0;
var
    s   : string;
    i      : Integer;
    lastDepth: double;
    SubDepth1, SubDepth2 : double;
    localDepth, sedimentDepth: double;
    //material : TMaterial;
    layer: TSeafloorLayer;
    lastAttenuation: double;
begin
    localDepth := aZProfile[1,0];

    s := 'dBSea RAM input text '#13#10;

    {freq, source depth, receiver depth (only used for tl.line file)}
    s := s + ifthen(includeFreq,tostr(round1(f)),'') + ' ' + tostr(round1(zs)) + ' 10.0' {receiver depth for TL.line - not used} + ' freq zs zr '#13#10;

    {max range, range spacing, range decimation}
    s := s + tostr(round1(rmax)) + ' ' + tostr(round1(dr / 1)) + ' 1 rmax dr ndr '#13#10;

    {max depth to calculate, depth spacing, depth decimation, max depth for grid}
    {about the subdepths: they are treated as absolute, also for sediment calcs. so we set the
    sediment attenuation from 0 to subdepth2, not 0 to subdepth2 + zmax}
    SubDepth1 := container.bathymetry.zMax * 2.5;
    SubDepth2 := container.bathymetry.zMax * 3;
    //zmplt should be same as zmax (defines which max z depth to return in tl.grid)
     s := s + tostr(round1(SubDepth2)) + ' ' + tostr(round1(container.scenario.dz / 1)) + ' 1 ' + tostr(round1(container.bathymetry.zMax)) + ' zmax dz ndz zmplt '#13#10;

    {ref sound speed (m/s), n. terms in rational approx, n. stability constraints, max range stability constraints (m)}
    s := s + '1500 8 1 0.0 c0 np ns rs '#13#10;

    {range rb (m) and depth zb (m) of bathymetry. continue for as many points as needed
    write the first profile point}
    s := s + tostr(round1(aZProfile[0,0])) + ' ' + tostr(round1(aZProfile[1,0])) + ' rb zb '#13#10;
    lastDepth := aZProfile[1,0];
    {now for each subsequent point in the profile:}
    for i := 1 to length(aZProfile[0]) - 1 do
    begin
        {only write the new point if there has been a change in depth}
        //TODO should possible change this as linear interp from 1,1,2 is not the same as interp 1,_,2
        //so therefore it would not write the point iff thisDepth = lastDepth = nextDepth
        if aZProfile[1,i] <> lastDepth then
        begin
            s := s + tostr(round1(aZProfile[0,i])) + ' '+ tostr(round1(aZProfile[1,i])) + ' '#13#10;
            lastDepth := aZProfile[1,i];
        end;
    end;
    s := s + '-1 -1 '#13#10;

    {Start of profile block: properties are the same for a given range. changes at the next rp
    depth z and sound speed cw in water : sound speed profile}
    s := s + tostr(round1(aCProfile[0,0])) + ' ' + tostr(round1(aCProfile[1,0])) + ' z cw '#13#10;
    for i := 1 to length(aCProfile[0]) - 1 do
        s := s + tostr(round1(aCProfile[0,i])) + ' ' + tostr(round1(aCProfile[1,i])) + ' '#13#10;
    s := s + '-1 -1 '#13#10;

    {depth z and sound speed cb in sediment : sound speed profile}
    for layer in container.scenario.propsMap.closest(aPos.X,aPos.Y).seabed.layers do
    begin
        if container.scenario.propsMap.closest(aPos.X,aPos.Y).seabed.isReferencedFromSeafloor then
            sedimentDepth := localDepth + layer.depth
        else
            sedimentDepth := layer.depth;

        if sedimentDepth <= subdepth2 then
            s := s + tostr(round1(sedimentDepth)) + ' ' + tostr(round1(layer.material.C)) + ' z cb '#13#10;
    end;
    //s := s + tostr(round1(0)) + ' ';
    //s := s + tostr(round1(material.C)) + ' ';
    //s := s +  'z cb '#13#10;
    s := s + '-1 -1 '#13#10;

    {depth z and density rhob in sediment (NB rho is stored locally as kg/m^3, whereas ramMDA expects rho/rhowater}
    for layer in container.scenario.propsMap.closest(aPos.X,aPos.Y).seabed.layers do
    begin
        if container.scenario.propsMap.closest(aPos.X,aPos.Y).seabed.isReferencedFromSeafloor then
            sedimentDepth := localDepth + layer.depth
        else
            sedimentDepth := layer.depth;

        if sedimentDepth <= subdepth2 then
            s := s + tostr(round1(sedimentDepth)) + ' ' + tostr(round1(layer.material.Rho_g)) + ' z rhob '#13#10;
    end;
    //s := s + tostr(round1(0)) + ' ';
    //s := s + tostr(round1(material.Rho_g)) + ' ';
    //s := s + 'z rhob'#13#10;
    s := s + '-1 -1 '#13#10;

    {depth z and attenuation attn in sediment}
    lastAttenuation := 0.5;
    for layer in container.scenario.propsMap.closest(aPos.X,aPos.Y).seabed.layers do
    begin
        //using zmax instead of localdepth. I think what should actually happen is that the sediment profiles track the seafloor depth?
        if container.scenario.propsMap.closest(aPos.X,aPos.Y).seabed.isReferencedFromSeafloor then
            sedimentDepth := container.bathymetry.zMax + layer.depth
        else
            sedimentDepth := layer.depth;

        if sedimentDepth <= subdepth1 then
        begin
            s := s + tostr(round1(sedimentDepth)) + ' ' + tostr(round1(layer.material.Attenuation)) + ' z attn '#13#10;
            lastAttenuation := layer.material.attenuation;
        end;
    end;
    {increase the attenuation rapidly, to kill reflections from the deep sediment bottom}
    s := s + tostr(round1(SubDepth1)) + ' ' + tostr(round1(lastAttenuation)) + ' z attn '#13#10;
    s := s + tostr(round1(SubDepth2)) + ' ' + tostr(round1(subDepth2Attenuation)) + ' z attn '#13#10;
    s := s + '-1 -1 '#13#10;
    {End of profile block}

    result := s;
end;



class function TExternalSolvers.writeBellhopInput(const f, zs, rmax, dr: double; const aPos: TFPoint3; const aCProfile: TMatrix): string;
{ create the input to bellhop (actually bellhopMDAn.exe) as a string. the full documentation for
  the bellhop input .env file is found online. it is all written as one long string
  containing line breaks, ie #13#10 (on windows) }
var
    s: string;
    i: integer;
    k: integer;
    lastDepthWritten: double;
    material: TMaterial;
begin
    s := '';
    s := s + '''dBSea Bellhop input text'' '#13#10;

    {freq, NMedia (must = 1)}
    s := s + tostr(round1(f)) + ' '#13#10;
    s := s + '1 '#13#10;

    {SSPOPT
    S: cubic spline interpolation of SSP
    V: vacuum above surface
    W: attenuation in dB/wavelength}
    s := s + '''SVW'' '#13#10;

    {N/A, N/A, max depth}
    s := s + '0 0 ' + tostr(round1(container.bathymetry.zMax)) + ' '#13#10;

    {sound speed profile - NB ssp goes to the full depth, but this solution only goes to zmax
    so truncate the greater depths. we also need at entry at zmax}
    k := 0;
    lastDepthWritten := 0;
    for i := 0 to length(aCProfile[0]) - 1 do
        if round1(aCProfile[0,i]) <= round1(container.bathymetry.zMax) then
        begin
            s := s + tostr(round1(aCProfile[0,i])) + ' ';
            s := s + tostr(round1(aCProfile[1,i])) + ' / '#13#10;
            k := i;
            lastDepthWritten := round1(aCProfile[0,i]);
        end;
    {write last point, if not already there (the solver doesn't allow two identical depths}
    if lastDepthWritten <> round1(container.bathymetry.zMax) then
    begin
        s := s + tostr(round1(container.bathymetry.zMax)) + ' ';   //depth
        s := s + tostr(round1(aCProfile[1,k])) + ' / '#13#10; // speed of sound at last point
    end;

    {tell bellhop to read the bathymetry file BTYFiL}
    //s := s + '''A'' 0.0 '#13#10; //ignore bathy file
    s := s + '''A*'' 0.0 '#13#10; //read bathy file

    {bottom line: z-bottom cp(compression)-bottom cs(shear)-bottom density-bottom alpha-bottom}
    material := container.scenario.topLayerMaterial(aPos.X,aPos.Y);
    s := s + tostr(round1(container.bathymetry.zMax)) + ' ';
    s := s + tostr(round1(material.c)) + ' ';
    s := s + tostr(round1(0)) + ' ';
    s := s + tostr(round1(material.rho_g)) + ' '; //(NB rho is stored locally as kg/m^3, whereas ramMDA expects rho/rhowater
    s := s + tostr(round1(0)) + ' / '#13#10;

    {number of sources}
    s := s + '1 '#13#10;

    {source depth}
    s := s + tostr(round1(zs)) + ' / '#13#10;

    {#receivers, depths}
    s := s + tostr(saferound(container.bathymetry.zMax/container.scenario.dz)) + ' '#13#10;
    s := s + tostr(round1(0)) + ' ' + tostr(round1(container.bathymetry.zMax)) + ' / '#13#10;

    {#receivers, ranges (nb in km)}
    s := s + tostr(saferound(rmax/dr)) + ' '#13#10;
    s := s + tostr(round1(0)) + ' ' + tostr(rmax/1000) + ' / '#13#10;

    {options re output type (C/I = coherent/incoherent TL, B = Gaussian beams)  (also you can use 'R' to
    investigat individual ray paths, using the matlab plotray.m)}
    if container.scenario.RayTracingCoherent then
        s := s + '''CB'' '#13#10
    else
        s := s + '''IB'' '#13#10;

    {NBEAMS (0 = let program choose) (0 can make it run verrrry slow)}
    s := s + tostr(container.scenario.BellhopNRays) + ' '#13#10;

    {Alpha 1, 2 (degrees) max and min launching angles}
    s := s + '-30.0 30.0 / '#13#10;

    {STEP (m) - 0 lets bellhop choose, ZBOX (m), RBOX (km) - bounding box beyond which no rays are traced. we make it slightly
    larger than the actualy problem. You can investigate the individual ray paths using the 'R' option, and if the bounding
    box is too close to the problem bounds, it does bad things to the ray paths}
    s := s + '0 ' + tostr(round1(1.2*container.bathymetry.zMax)) + ' ' + tostr(1.2*rmax/1000) + ' '#13#10;

    result := s;
end;

class function TExternalSolvers.writeBellhopBathymetry(const aZProfile:TMatrix):string;
{write the bathymetry input string for bellhop. this will get written to
the bathymetry file. one long string with line breaks included #13#10}
var
    s   : string;
    i   : Integer;
    dLastDepth : double;
    iNPoints : integer;
    bWriteFirstPointTwice : boolean;
begin
    s := '';

    bWriteFirstPointTwice := false;
    {the first line of the file is the number of points. we want to cut out
    points that are repeated, so we have to first count through and see how many
    will remain after that process. how many points are not simply repeats of the
    previous point?}
    iNPoints := 0;
    dLastDepth := -99999;
    for i := 0 to length(aZProfile[0]) - 1 do
        if aZProfile[1,i] <> dLastDepth then
        begin
            iNPoints := iNPoints + 1;
            dLastDepth := aZProfile[1,i];
        end;

    {the solver doesn;t like getting only 1 input point (ie totally flat profiles.)
    so add a second point}
    if iNPoints = 1 then
    begin
        bWriteFirstPointTwice := true;
        iNPoints := 2;
    end;

    {write the number of points (once we get rid of repeated points)}
    s := s + tostr(iNPoints) + ' '#13#10;

    {range rb (km) and depth zb (m) of bathymetry. continue for as many points as needed}
    //s := s + tostr(saferound(aZProfile[0,0])/1000) + ' ';  //range in km - round to m, so we don;t lose detail
    //s := s + tostr(round1(aZProfile[1,0])) + ' '#13#10;
    dLastDepth := -99999;
    for i := 0 to length(aZProfile[0]) - 1 do
    begin
        {only write data if depth is different from previous line}
        if aZProfile[1,i] <> dLastDepth then
        begin
            s := s + tostr(saferound(aZProfile[0,i])/1000) + ' ';  //range in km - round to m, so we don;t lose detail
            s := s + tostr(round1(aZProfile[1,i])) + ' '#13#10;
            dLastDepth := aZProfile[1,i];
        end;
    end;

    if bWriteFirstPointTwice then
    begin
        i := length(aZProfile[0]) - 1;
        s := s + tostr(saferound(aZProfile[0,i])/1000) + ' ';  //range in km - round to m, so we don;t lose detail
        s := s + tostr(round1(aZProfile[1,i])) + ' '#13#10;
    end;

    result := s;
end;

class function TExternalSolvers.writeCSNapInput(const f, zs, rmax, dr: double; const aPos: TFPoint3; const aZProfile, aCProfile: TMatrix): string;
{ create the input to CSnap (actually c-snapMDAn.exe) as a string. the full documentation for
  the CSnap input file is found online. it is all written as one long string
  containing line breaks, ie #13#10 (on windows) }
var
    s: string;
    i, j, k, m: integer;
    dThisDepth, dLastDepth: double;
    material: TMaterial;
    // bZeroDepth : boolean;
begin
    s := '';
    s := s + 'dBSea CSNAP input text '#13#10; //header line

    {number source freqs}
    s := s + '1 '#13#10;

    {source freq}
    s := s + tostr(round1(f)) + ' '#13#10;

    {min mode, max mode, highest mode in propagating field (=max mode)}
    s := s + '1 100 100 '#13#10;

    {MSP (number of vertical grid points), ERMAX - accuracy criterion}
    {number of vertical grid points is based on local depth, not global max depth,
    hence finding the max value in the vector of depths. Not sure if it should
    be ceil or floor, will have to look at what CSnap returns. could be fencepost errors.}
    s := s + tostr(safeceil(azprofile.column(1).max/container.scenario.dz)) + ' 0 '#13#10;

    {individual range section starts here, repeat as often as needed}
    m := 0;
//    bZeroDepth := false;
    dLastDepth := -99999;
    for i := 0 to length(azprofile[0]) - 1 do      //length(zprofile[0]) - 1
    begin
        {get the depth at this location and check if it differs from previous,
        if it does then we start a new range section}
        dThisDepth := azprofile[1,i];
        {solver has a problem with depths <= 0 so stop if we reach one}
        if dThisDepth < 0 then
            dThisDepth := 0;
        if (dThisDepth <> dLastDepth) then
        begin
            m := m + 1; //m is used for debugging, keeping count of the current region
            {water depth, surface roughness, bottom roughness, range at start of this block (km), # subdivisions this range}
            s := s + tostr(dThisDepth) + ' 0. 0. ' + tostr(saferound(azprofile[0,i])/1000) +
                ' 1 ' + '!Start region ' + tostr(m) + ' '#13#10;
            // TODO maybe increment the number of subdivisions?

            {only write speed of sound profile if depth > 0}
            if dThisDepth > 0 then
            begin
                {speed of sound profile - we must have last depth = zmax}
                k := 0;
                for j := 0 to length(acprofile[0]) - 1 do
                begin
                    {check if profile depth is less than current bathymetry waterdepth}
                    if acprofile[0,j] < dThisDepth then
                    begin
                        s := s + tostr(round1(acprofile[0,j])) + ' ';   //depth
                        s := s + tostr(round1(acprofile[1,j])) + ' '#13#10; // speed of sound
                        k := j;
                    end;
                end;
                {write last point at the current bathymetry depth}
                s := s + tostr(dThisDepth) + ' ';   //depth
                s := s + tostr(round1(acprofile[1,k])) + ' '#13#10; // speed of sound at last point
            end;

            {if the 'sediment layer' thickness is 0, then we use the subbottom as the
            sediment, and don't enter a sediment speed of sound profile. The main reason
            for this is that if a sediment layer is included, then we get back a larger
            TL array than expected (ie it extends down into the sediment layer)}

            //TODO check attenuation is in correct units (here, dB per wavelength)
            { Thickness of sediment layer, Density of sediment (g/cm3),Compressional wave attenuation in sediment (dB/wavelength)}
            s := s + '0. 0 0 '#13#10;

            {Density of subbottom, Compressional wave attenuation in subbottom, Compressional speed in subbottom}
            material := container.scenario.topLayerMaterial(aPos.X,aPos.Y);
            s := s + tostr(round1(material.rho_g)) + ' ' + tostr(round1(material.attenuation)) +
                ' ' + tostr(round1(material.c)) + ' '#13#10;

            {Shear wave attenuation in subbottom, Shear speed in subbottom}
            s := s + '0. 0. !End region ' + tostr(m) + ' '#13#10;

            {set the lastDepth so we can check against it on subsequent ranges}
            dLastDepth := dThisDepth;
        end;

    end;  //range section ends

    {options section starts}

    {mesh can be refined to get more accurate wavenumbers by putting a higher value than 1}
    s := s + 'NMESH 1 '#13#10;

    {OPTMZ causes a speedup by pruning non-contributing modes}
    s := s + 'OPTMZ '#13#10;

    {produce gridded transmission loss, incoherent calc}
    s := s + 'CONDR,INC '#13#10;

    {the plotting axis options are to do with the way data is displayed if you use the OASES plotting files}
    //XAXIS  0   4   16   1 //range axis - min (=0 km), rmax (km), axis length, tickmark spacing, N/A
    //YAXIS  0 300   10  50 //depth axis -  min (=0 m), zmax (m), axis length, tickmark spacin
    //ZAXIS 40  67    3  //TL values - min (dB), max (dB), level increment, smoothing

    {rmin : min range (km),rmax : max range (km), delr : range incremen (=dr (km))}
    s := s + '0. ' + tostr(round1(rmax/1000)) + ' ' + tostr(round1(dr/1000)) + ' '#13#10;

    {source depth}
    s := s + tostr(round1(zs)) + ' '#13#10;

    result := s;
end;

class function TExternalSolvers.readCSnapReturnFile(const Filename : string;delim : char; iHSkip, iVSkip : integer):TMatrix;
{generic reading of delimited file into a TMatrix where delim is the delimiter.
this version returns an array of the correct size.
Skip is used to skip n rows at the start. it is now 0 at all times I think, but we used to have a version of ramMDA that
would put some header information at the start of each line which we wanted to skip}
var
    i: integer;
    sl1: ISmart<TStringlist>;
begin
    {Note that the returned file also has the number of rows and columns as the first
    two data entries on the first line, we could use that to get imax and jmax}

    sl1 := TSmart<TStringlist>.create(TStringlist.Create);
    sl1.LoadFromFile(Filename);
    {get past the first VSkip lines}
    for i := 0 to iVSkip - 1 do
        sl1.Delete(0);

    {use overloaded readstring method}
    Result.fromDelimitedString(sl1.Text,delim,true);
end;


class function TExternalSolvers.writeKrakenInput(const f, zs, rmax, dr, localdepth, maxdepth: double; const aPos: TFPoint3; const aCProfile: TMatrix): string;
{ create the input to kraken (actually krakenMDAn.exe) as a string. it is all written as one long string
  containing line breaks, ie #13#10 (on windows) }
var
    s: string;
    i, iDepthInd, iNRcvrs: integer;
    dThisDepth: double;
    dThisC: double;
    // bZeroDepth : boolean;
    cMax, cMin: double;
    dTolerance: double;
    SubDepth1, SubDepth2: double;
    subDepth2Attenuation: double;
    iNMesh1, iNMesh2: integer;
    // material : TMaterial;
    layer: TSeafloorLayer;
    lastC, lastRho, lastAttenuation: double;
    maxCFromSeafloorLayers: double;
    // nMedia : integer;
    // rcvrs : TVect;
begin
    {high attenuation Media. this is the sediment. we let this go to a depth below the water media, and then in the last
     section greatly increase the attenuation, to kill reflections from the subsurface vacuum.
     (NB attenuation is only included if you are using krakenc, not kraken.). The depths here are absolute,
     not relative to the sea bottom, ie subDepth2 becomes the very lowest point assessed, not
     subDepth2 + localdepth}
     subDepth1 := maxdepth*2.5;
     subDepth2 := maxdepth*3;
     subDepth2Attenuation := 50;

    {these options are covered in detail in Porter M. The KRAKEN normal mode program. SACLANT UNDER-
    SEA RESEARCH (memorandum), San Bartolomeo, Italy, 1991}
    s := '';
    s := s + '''dBSea - Kraken input depth: ' + tostr(localdepth) + ' m'' '#13#10; //title
    s := s + tostr(round1(f)) + '        ! Frequency (Hz)'#13#10; //freq
    s := s + '2        ! NMedia '#13#10;  //, NMedia
    {SSPOPT:
    N: N2 linear ssp approximation
    V: vacuum upper halfspace
    W: attenuation in dB/wavelength}
    s := s + '''NVW''     ! Top Option'#13#10;

    {Media 1}
    {Nmesh (~10 per wavelength) = 10*H0*f/c, sigma(m) (rms surface roughness), bottom depth H0}
    iNMesh1 := saferound(10*localdepth*f/aCProfile[1,0]);
    if iNMesh1 < 5 then
        iNMesh1 := 5;
    s := s + tostr(iNMesh1) + ' 0 ' + tostr(round1(localdepth)) + '      ! N sigma depth'#13#10;
    { sound speed profile
    first line has some extra info. z(m); cp (m/s); cs (m/s) ; rho(gm/cm3); ?; ?}
    i := 0;
    iDepthInd := 0;
    s := s + tostr(round1(aCProfile[0,i])) + ' ' + tostr(round1(aCProfile[1,i])) + ' 0 1 0 0 '#13#10;
    cMax := aCProfile[1,i];
    cMin := aCProfile[1,i];
    {now the rest of the points , just z and cp}
    for i := 1 to length(aCProfile[0]) - 1 do
    begin
        dThisDepth  := aCProfile[0,i];
        dThisC      := aCProfile[1,i];
        if round1(dThisDepth) < localdepth then
        begin
            s := s + tostr(round1(dThisDepth)) + ' ' + tostr(round1(dThisC)) + ' / '#13#10;
            if dThisC > cMax then
                cMax := dThisC;
            if dThisC < cMin then
                cMin := dThisC;
            iDepthInd := i;
        end;
    end;
    {last point needs to match depth, so if it doesnt then add another point at depth}
    if (iDepthInd = 0) or (round1(aCProfile[0,iDepthInd]) <> localdepth) then
        s := s + tostr(round1(localdepth)) + ' ' + tostr(round1(aCProfile[1,iDepthInd])) + ' / '#13#10;

     iNMesh2 := saferound(10*(subDepth2 - localdepth)*f/container.scenario.topLayerMaterial(aPos.X,aPos.Y).c);
     if iNMesh2 < 5 then
        iNMesh2 := 5;
     s := s + tostr(iNMesh2) + ' 0 ' + tostr(round1(subDepth2)) + '      ! N sigma depth'#13#10;
     {fake SSP for second media (ie sediment)}
     maxCFromSeafloorLayers := 1500;
     lastC := 1500.0;
     lastRho := 1.0;
     lastAttenuation := 0.5;
     for layer in container.scenario.propsMap.closest(aPos.X,aPos.Y).seabed.layers do
     begin
         if (localdepth + layer.depth) < subDepth1 then
         begin
            //loop through seafloorlayers and add a line for each
            //not sure whether kraken expects the depths to be referenced to the seafloor or not
            s := s + tostr(round1(localdepth + layer.depth)) + ' ' +
                     tostr(round1(layer.material.c)) + ' 0 ' +
                     tostr(round1(layer.material.rho_g)) + ' ' +
                     tostr(round1(layer.material.Attenuation)) + ' 0 '#13#10;
            lastC := layer.material.c;
            lastRho := layer.material.rho_g;
            lastAttenuation := layer.material.attenuation;
            if layer.material.c > maxCFromSeafloorLayers then
                maxCFromSeafloorLayers := layer.material.c;
         end;
     end;
     //s := s + tostr(round1(localdepth)) + ' ' + tostr(round1(material.c)) + ' 0 ' + tostr(round1(material.rho/1000))
     //        + ' ' + tostr(round1(material.Attenuation)) + ' 0 '#13#10;
     s := s + tostr(round1(Subdepth1)) + ' ' + tostr(round1(lastc)) + ' 0 ' + tostr(round1(lastrho))
             + ' ' + tostr(round1(lastattenuation)) + ' 0 '#13#10;
     s := s + tostr(round1(Subdepth2)) + ' ' + tostr(round1(lastc)) + ' 0 ' + tostr(round1(lastrho))
             + ' ' + tostr(round1(subDepth2Attenuation)) + ' 0 '#13#10;

//    //s := s + '''A*'' 0.0 '#13#10; //bottom options
//
//    {bottom line: z-bottom cp(compression)-bottom cs(shear)-bottom density-bottom alpha-bottom.
//    note that although we use the sediment attenuation, KRAKEN ignores it. KRAKENC is the program
//    that takes this into account.}
//    s := s + tostr(round1(localdepth)) + ' ' + tostr(round1(sediment.C)) + ' ' + tostr(round1(0)) + ' ';
//    s := s + tostr(round1(sediment.Rho/1000)) + ' ' + tostr(round1(sediment.Attenuation)) + ' /   ! Bottom Option, sigma'#13#10; //(NB rho is stored locally as kg/m^3, whereas ramMDA expects rho/rhowater
    s := s + '''V'' 0.0        ! Bottom Option, sigma'#13#10; //bottom options

    {CLOW  CHIGH - these control the calculation of modes. long discussion in kraken.txt}
    //if cMin > 1500 then
    //    cMin := 1500;
    cMin := 0; //use 0 to let the solver choose
    if cMax < 1600 then
        cMax := 1600;
    if cMax < maxCFromSeafloorLayers then
        cMax := maxCFromSeafloorLayers;
    cMax := cMax * 3; //setting cmax above any wavespeeds in the problem. not sure how far above is needed - one of the examples had it set at ~10*cmax
    s := s + tostr(cMin) + ' ' + tostr(cMax) + '       ! cLow cHigh (m/s)'#13#10;

    s := s + '0.0     ! RMax (km) '#13#10;//RMAX (km) {set to zero as the range calc is actually done later, in field.exe}

    {source and receiver depths. these are merged, and modes calculated for the union of them. so they can be
    identical, or we can just put 1 source.
    for iNSources: we want the number set by field.exe tolerance. as noted above, subDepth2 is the absolute lowest point
    assessed, not subDepth2 + localdepth or anything like that. }

    //TODO not sure about what happens here when we have a very low number of receivers
    dTolerance := 2*1450/f; //slightly less than (two directions, so) 2*1500/Freq which is what field.exe uses as its tolerance
    //iNRcvrs := larger(safeceil(maxdepth/dTolerance) + 1,safefloor(maxdepth/dz) + 4);
    iNRcvrs := safeceil(subDepth2/dTolerance) + 1;
    if iNRcvrs < (10*subDepth2*f/1500) then
        iNRcvrs := safeceil(10*subDepth2*f/1500);
    if iNRcvrs < (safefloor(subDepth2/container.scenario.dz) + 4) then
        iNRcvrs := (safefloor(subDepth2/container.scenario.dz) + 4);
    s := s + '1       ! NSD'#13#10; //NSrcDepths
    s := s + '0 /         ! SD(1)  ... (m) '#13#10;//SrcDepths
    s := s + tostr(iNRcvrs) + '        ! NRD'#13#10; //NRcvrDepths
    s := s + '0 ' + tostr(subDepth2) + ' /     ! RD(1)  ... (m) '#13#10;//RcvrDepths

    result := s;
end;


class function TExternalSolvers.writeFieldInput(const f, zs, rmax, dr : double; const iLastWaterInd : integer; const aZprofile : TMatrix):string;
{create the input to field (actually fieldMDAn.exe) as a string. the full documentation for
the field input file is found online (its a bit sparse, frankly). it is all written as one long string
containing line breaks, ie #13#10 (on windows). This version writes directly the range of each new profile
read, instead of letting field create a subtable from the start and end values. This means we can skip out
profile reads if the 2 profiles on either side are also the same - not normally important but it
saves a lot of time on problems with deep, flat bottoms eg Munk, Dickins Seamount}
var
    s   : string;
    i   : Integer;
    dMaxDepth : double;
//    bZeroDepth : boolean;
    iNRcvrs : integer;
    iNRanges : integer;
    sCoupledOrAdiabatic : string;
    //lastDist : double;
begin
    s := '';
    s := s + '''dBSea Kraken Field input'' '#13#10; //header line
    {option to choose coupled or adiabatic modes. unfortunately, coupled modes seems to make the results blow up
    quite a lot, so don't use it.}
    if container.scenario.NormalModesCoupledModes then
        sCoupledOrAdiabatic := 'C'
    else
        sCoupledOrAdiabatic := 'A';
    {there is a big difference between coherent and incoherent. it seems like incoherent approximates a
    simple log(range) curve}
    s := s + '''R' + sCoupledOrAdiabatic + ' C'' '#13#10; // OPT 'X/R' cylindrical/spherical, 'C/A' coupled/adiabatic, 'C/I' coherent/incoherent
    s := s + '9999 '#13#10;  // M  (number of modes to include)
    s := s + tostr(length(aZProfile[0])) + ' '#13#10;  // NPROF   number profiles
    {for each profile in the trimmed zprofile, write the range}
    for i := 0 to length(aZProfile[0]) - 1 do
        s := s + tostr(aZProfile[0,i]/1000) + #13#10;
    //s := s + '0.0 ' + tostr(round1(rmax/1000)) + ' / '#13#10; // RPROF(1:NPROF) (km) the ranges of the profiles. these match the mode files
    //lastDist := zprofile[0,iLastWaterInd];
    //s := s + '0.0 ' + tostr(round1(lastDist/1000)) + ' / '#13#10;

    //iNRanges := saferound(zprofile[0,iLastWaterInd]/dr);
    iNRanges := saferound(rmax/dr);
    if iNRanges < 1 then
        iNRanges := 1;
    s := s + tostr(iNRanges) + ' '#13#10;  //NR  number of receivers (range)
    s := s + '0.0 ' + tostr(rmax/1000) + ' / '#13#10; // R(1:NR)   (km) receiver ranges

    s := s + '1 '#13#10;   //NSD number sources
    s := s + tostr(round1(zs)) +' / '#13#10;  //SD(1:NSD)   (m) source depth

    {make sure that we have at least 1 rcvr, as floor can make it go to 0 for shallow water}
    dMaxDepth := 0;
    for i := 0 to length(aZProfile[0]) - 1 do
        if aZProfile[1,i] > dMaxDepth then
            dMaxDepth := aZProfile[1,i];
    iNRcvrs := safefloor(dMaxDepth/container.scenario.dz) + 1;
    if iNRcvrs < 1 then
        iNRcvrs := 1;

    s := s + tostr(iNRcvrs) + ' '#13#10;  //NRcvrDepths
    s := s + '0.0 ' + tostr(round1(dMaxDepth)) + ' / '#13#10; //RcvrDepths

    s := s + tostr(iNRcvrs) + ' '#13#10;//Nrcvr displacements
    s := s + '0.0 / '#13#10;  // rcvr displacements (=0)

    result := s;
end;


end.
