unit BandSolver;

interface

uses system.math, mComplex, mmatrix;

type
  TBandSolver = class(TObject)
  public
    //multiply
    class procedure banmulOrig(const A: TArray<TArray<double>>; n, m1, m2: integer; x: TArray<double>; var b: TArray<double>); overload;
    class procedure banmulOrig(const A: TComplexMat; n, m1, m2: integer; x: TComplexVect; var b: TComplexVect); overload;
    class procedure banmul(const A: TComplexMat; n, m1, m2: integer; x: TComplexVect; var b: TComplexVect);
    //class procedure banmul(const A: TComplexMat; n, m1, m2: integer; x: TComplexVect; var b: TComplexVect); overload;

    //decompose
    class procedure bandecOrig(var A, AL: TArray<TArray<double>>; const n, m1, m2: integer; var indx: TArray<integer>; out d: double); overload;
    class procedure bandecOrig(var A, AL: TComplexMat; const n, m1, m2: integer; var indx: TArray<integer>; out d: double); overload;
    //class procedure bandec(var A, AL: TComplexMat; const n, m1, m2: integer; var indx: TArray<integer>; out b: boolean); overload;

    //solve
    class procedure banbksOrig(const a,al: TArray<TArray<double>>; const n, m1, m2: integer; const indx: TArray<integer>; var b: TArray<double>); overload;
    class procedure banbksOrig(const a,al: TComplexMat; const n, m1, m2: integer; const indx: TArray<integer>; var b: TComplexVect); overload;
    //class procedure banbks(const a,al: TComplexMat; const n, m1, m2: integer; var indx: TArray<integer>; var b: TComplexVect); overload;
    //procedure banmul2(var A: TArray<TArray<double>>; n, m1, m2: integer; x: TArray<double>; var b: TArray<double>);
  end;

implementation

//class procedure TBandSolver.banmul(var A: TArray<TArray<double>>; n, m1, m2: integer; x: TArray<double>; var b: TArray<double>);
////Matrix multiply b = A�x, whereA is band diagonal with m1 rows below the diagonal and m2 rows above.
////The input vector x and output vector b are stored as x[1..n] and b[1..n], respectively.
////The array a[1..n][1..m1+m2+1] stores A as follows: The diagonal elements are in a[1..n][m1+1].
////Subdiagonal elements are in a[j..n][1..m1] (with j>1 appropriate to the number of elements on each subdiagonal).
////Superdiagonal elements are in a[1..j][m1+2..m1+m2+1] with j<n appropriate to the number of elements on each superdiagonal.
//var
//	i,j,k,tmploop: integer;
//begin
//	for i := 0 to n - 1 do
//    begin
//		k := i - m1;
//		tmploop := MIN(m1+m2+1,n-k);
//		b[i] := 0.0;
//		for j := MAX(0,-k) to tmploop - 1 do
//			b[i] := b[i] + a[i][j]*x[j+k];
//    end;
//end;
//
//class procedure TBandSolver.banmul(var A: TComplexMat; n, m1, m2: integer; x: TComplexVect; var b: TComplexVect);
//var
//	i,j,k,tmploop: integer;
//begin
//	for i := 0 to n - 1 do
//    begin
//		k := i - m1;
//		tmploop := MIN(m1+m2+1,n-k);
//		b[i] := NullComplex;
//		for j := MAX(0,-k) to tmploop - 1 do
//			b[i] := b[i] + a[i][j]*x[j+k];
//    end;
//end;

class procedure TBandSolver.bandecOrig(var A, AL: TArray<TArray<double>>; const n, m1, m2: integer; var indx: TArray<integer>; out d: double);
//Given an n�n band diagonal matrix A with m1 subdiagonal rows and m2 superdiagonal rows,
//compactly stored in the array a[1..n][1..m1+m2+1] as described in the comment
//for routine banmul, this routine constructs an LU decomposition of
//a rowwise permutation of A. The upper triangular matrix replaces a,
//while the lower triangular matrix is returned in al[1..n][1..m1].
//indx[1..n] is an output vector which records the row permutation
//effected by the partial pivoting;
//d is output as �1 depending on whether the number of row interchanges was even
//or odd, respectively. This routine is used in combination with banbks to
//solve band-diagonal sets of equations.
const
    TINY = 1.0e-20;
var
	i,j,k,l, mm: integer;
	tmp: double;
begin
	mm:=m1+m2+1;
	l:=m1;
	for i:= 1 to m1 do
    begin
        // Rearrange the storage a bit.
		for j:=m1+2-i to mm do
            a[i][j-l]:=a[i][j];
		dec(l);
		for j:=mm-l to mm do
            a[i][j]:=0.0;
	end;
	d:=1.0;
	l:=m1;
	for k:=1 to n do
    begin
		//For each row...
		tmp := a[k][1];
		i:=k;
		if (l < n) then
            inc(l);
		for j:=k+1 to l do
		begin
        	//Find the pivot element.
			if (abs(a[j][1]) > abs(tmp)) then
            begin
				tmp:=a[j][1];
                i:=j;
            end;
		end;
		indx[k]:=i;

		if (tmp = 0.0) then
            a[k][1] := TINY;//Matrix is algorithmically singular, but proceed anyway with TINY pivot (desirable in some applications).

		if (i <> k) then
		begin
        	//Interchange rows.
			d := -d;
			for j:=1 to mm do
            begin
                tmp := a[k][j];
                a[k][j] := a[i][j];
                a[i][j] := tmp;
                //SWAP(a[k][j],a[i][j]);
            end;
		end;
		for i:=k+1 to l do
        begin
			//Do the elimination.
			tmp:=a[i][1]/a[k][1];
			al[k][i-k]:=tmp;
			for j:=2 to mm do
                a[i][j-1]:=a[i][j]-tmp*a[k][j];
			a[i][mm]:=0.0;
		end;
	end;
end;

class procedure TBandSolver.banbksOrig(const a,al: TArray<TArray<double>>; const n, m1, m2: integer; const indx: TArray<integer>; var b: TArray<double>);
//Given the arrays a, al, andindx as returned from bandec, and given a right-hand side vector b[1..n],
//solves the band diagonal linear equations A�x = b. The solution vector x overwrites b[1..n]. The
//other input arrays are not modified, and can be left in place for successive calls with di?erent right-hand sides.
var
	i,k,l,mm: integer;
    dum: double;
begin
	mm:=m1+m2+1;
	l:=m1;
	for k:=1 to n do
    begin
		//Forward substitution, unscrambling the permuted rows as we go.
		i:=indx[k];
		if (i <> k) then
        begin
            dum := b[k];
            b[k] := b[i];
            b[i] := dum;
        end;
		if (l < n) then
            inc(l);
		for i:=k+1 to l do
            b[i] := b[i] - al[k][i-k]*b[k];
	end;
	l := 1;
	for i := n downto 1 do
    begin
		//Backsubstitution.
		dum:=b[i];
		for k:=2 to l do
			dum := dum - a[i][k]*b[k+i-1];
		b[i]:=dum/a[i][1];
		if (l < mm) then
            inc(l);
	end;
end;

class procedure TBandSolver.banbksOrig(const a, al: TComplexMat; const n, m1, m2: integer; const indx: TArray<integer>; var b: TComplexVect);
var
	i,k,l,mm: integer;
    dum: Tcomplex;
begin
	mm:=m1+m2+1;
	l:=m1;
	for k:=1 to n do
    begin
		//Forward substitution, unscrambling the permuted rows as we go.
		i:=indx[k];
		if (i <> k) then
        begin
            dum := b[k];
            b[k] := b[i];
            b[i] := dum;
        end;
		if (l < n) then
            inc(l);
		for i:=k+1 to l do
            b[i] := b[i] - al[k][i-k]*b[k];
	end;
	l := 1;
	for i := n downto 1 do
    begin
		//Backsubstitution.
		dum:=b[i];
		for k:=2 to l do
			dum := dum - a[i][k]*b[k+i-1];
		b[i]:=dum/a[i][1];
		if (l < mm) then
            inc(l);
	end;
end;

class procedure TBandSolver.bandecOrig(var A, AL: TComplexMat; const n, m1, m2: integer; var indx: TArray<integer>; out d: double);
const
    TINY = 1.0e-20;
var
	i,j,k,l, mm: integer;
	tmp: Tcomplex;
begin
	mm:=m1+m2+1;
	l:=m1;
	for i:= 1 to m1 do
    begin
        // Rearrange the storage a bit.
		for j:=m1+2-i to mm do
            a[i][j-l]:=a[i][j];
		dec(l);
		for j:=mm-l to mm do
            a[i][j]:=NullComplex;
	end;
	d:=1.0;
	l:=m1;
	for k:=1 to n do
    begin
		//For each row...
		tmp := a[k][1];
		i:=k;
		if (l < n) then
            inc(l);
		for j:=k+1 to l do
		begin
        	//Find the pivot element.
			if (a[j][1].modulusSquared > tmp.modulusSquared) then
            begin
				tmp:=a[j][1];
                i:=j;
            end;
		end;
		indx[k]:=i;
		if (tmp.modulusSquared = 0.0) then
            a[k][1]:=TINY;//Matrix is algorithmically singular, but proceed anyway with TINY pivot (desirable in some applications).
		if (i <> k) then
		begin
        	//Interchange rows.
			d := -d;
			for j:=1 to mm do
            begin
                tmp := a[k][j];
                a[k][j] := a[i][j];
                a[i][j] := tmp;
                //SWAP(a[k][j],a[i][j]);
            end;
		end;
		for i:=k+1 to l do
        begin
			//Do the elimination.
			tmp:=a[i][1]/a[k][1];
			al[k][i-k]:=tmp;
			for j:=2 to mm do
                a[i][j-1]:=a[i][j]-tmp*a[k][j];
			a[i][mm]:=NullComplex;
		end;
	end;
end;

class procedure TBandSolver.banmulOrig(const A: TComplexMat; n, m1, m2: integer; x: TComplexVect; var b: TComplexVect);
var
	i,j,k,tmploop: integer;
begin
	for i:=1 to n do
    begin
		k:=i-m1-1;
		tmploop:=MIN(m1+m2+1,n-k);
		b[i]:=nullComplex;
		for j:=MAX(1,1-k) to tmploop do
			b[i] := b[i] + a[i][j]*x[j+k];
    end;
end;


class procedure TBandSolver.banmul(const A: TComplexMat; n, m1, m2: integer; x: TComplexVect; var b: TComplexVect);
var
	i,j,k,tmploop: integer;
begin
	for i := 0 to n - 1 do
    begin
		k := i - m1;
		tmploop := MIN(m1+m2+1,n-k);
		b[i] := nullComplex;
		for j := MAX(0,-k) to tmploop - 1 do
			b[i] := b[i] + a[i][j]*x[j+k];
    end;
end;

class procedure TBandSolver.banmulOrig(const A: TArray<TArray<double>>; n, m1, m2: integer; x: TArray<double>; var b: TArray<double>);
//Matrix multiply b = A�x, whereA is band diagonal with m1 rows below the diagonal and m2 rows above.
//The input vector x and output vector b are stored as x[1..n] and b[1..n], respectively.
//The array a[1..n][1..m1+m2+1] stores A as follows: The diagonal elements are in a[1..n][m1+1].
//Subdiagonal elements are in a[j..n][1..m1] (with j>1 appropriate to the number of elements on each subdiagonal).
//Superdiagonal elements are in a[1..j][m1+2..m1+m2+1] with j<n appropriate to the number of elements on each superdiagonal.
var
	i,j,k,tmploop: integer;
begin
	for i:=1 to n do
    begin
		k:=i-m1-1;
		tmploop:=MIN(m1+m2+1,n-k);
		b[i]:=0.0;
		for j:=MAX(1,1-k) to tmploop do
			b[i] := b[i] + a[i][j]*x[j+k];
    end;
end;

//class procedure TBandSolver.bandec(var A, AL: TArray<TArray<double>>; const n, m1, m2: integer; var indx: TArray<integer>; out b: boolean);
////Given an n�n band diagonal matrix A with m1 subdiagonal rows and m2 superdiagonal rows,
////compactly stored in the array a[1..n][1..m1+m2+1] as described in the comment
////for routine banmul, this routine constructs an LU decomposition of
////a rowwise permutation of A. The upper triangular matrix replaces a,
////while the lower triangular matrix is returned in al[1..n][1..m1].
////indx[1..n] is an output vector which records the row permutation
////effected by the partial pivoting;
////b = true if the number of row interchanges was even.
////This routine is used in combination with banbks to
////solve band-diagonal sets of equations.
//const
//    eps = 1.0e-20;
//var
//	i,j,k,p, mm: integer;
//	tmp: double;
//begin
//	mm := m1+m2+1;
//	p := m1;
//	for i := 0 to m1 - 1 do
//    begin
//        // Rearrange the storage a bit.
//		for j := m1 - i to mm - 1 do
//            a[i][j-p] := a[i][j];
//		dec(p);
//		for j := mm - p - 1 to mm - 1 do
//            a[i][j] := 0.0;
//	end;
//	b := true;
//	p := m1;
//	for k := 0 to n-1 do
//    begin
//		//For each row...
//		tmp := a[k][0];
//        i := k;
//		if (p < n) then
//            inc(p);
//		for j := k + 1 to p - 1 do
//		begin
//        	//Find the pivot element.
//			if (abs(a[j][0]) > abs(tmp)) then
//            begin
//				tmp := a[j][0];
//                i := j;
//            end;
//		end;
//		indx[k] := i;
//		if (tmp = 0.0) then
//            a[k][0] := eps;//Matrix is algorithmically singular, but proceed anyway with TINY pivot (desirable in some applications).
//		if (i <> k) then
//		begin
//        	//Interchange rows.
//			b := not b;
//			for j := 0 to mm - 1 do
//            begin
//                tmp := a[k][j];
//                a[k][j] := a[i][j];
//                a[i][j] := tmp;
//            end;
//		end;
//
//		for i := k + 1 to p - 1 do
//        begin
//			//Do the elimination.
//			tmp := a[i][0]/a[k][0];
//			al[k][i-k-1] := tmp;
//			for j := 1 to mm - 1 do
//                a[i][j-1] := a[i][j] - tmp*a[k][j];
//			a[i][mm-1] := 0.0;
//		end;
//	end;
//end;
//
//class procedure TBandSolver.bandec(var A, AL: TComplexMat; const n, m1, m2: integer; var indx: TArray<integer>; out b: boolean);
//const
//    eps = 1.0e-20;
//var
//	i,j,k,p, mm: integer;
//	tmp: TComplex;
//begin
//	mm := m1+m2+1;
//	p := m1;
//	for i := 0 to m1 - 1 do
//    begin
//        // Rearrange the storage a bit.
//		for j := m1 - i to mm - 1 do
//            a[i][j-p] := a[i][j];
//		dec(p);
//		for j := mm - p - 1 to mm - 1 do
//            a[i][j] := NullComplex;
//	end;
//	b := true;
//	p := m1;
//	for k := 0 to n-1 do
//    begin
//		//For each row...
//		tmp := a[k][0];
//        i := k;
//		if (p < n) then
//            inc(p);
//		for j := k + 1 to p - 1 do
//		begin
//        	//Find the pivot element.
//			if (a[j][0].modulusSquared > tmp.modulusSquared) then
//            begin
//				tmp := a[j][0];
//                i := j;
//            end;
//		end;
//		indx[k] := i;
//		if (tmp.modulusSquared = 0.0) then
//            a[k][0] := eps;//Matrix is algorithmically singular, but proceed anyway with TINY pivot (desirable in some applications).
//		if (i <> k) then
//		begin
//        	//Interchange rows.
//			b := not b;
//			for j := 0 to mm - 1 do
//            begin
//                tmp := a[k][j];
//                a[k][j] := a[i][j];
//                a[i][j] := tmp;
//            end;
//		end;
//
//		for i := k + 1 to p - 1 do
//        begin
//			//Do the elimination.
//			tmp := a[i][0]/a[k][0];
//			al[k][i-k-1] := tmp;
//			for j := 1 to mm - 1 do
//                a[i][j-1] := a[i][j] - tmp*a[k][j];
//			a[i][mm-1] := NullComplex;
//		end;
//	end;
//end;
//
//class procedure TBandSolver.banbks(var a,al: TArray<TArray<double>>; const n, m1, m2: integer; var indx: TArray<integer>; var b: TArray<double>);
////Given the arrays a, al, andindx as returned from bandec, and given a right-hand side vector b[1..n],
////solves the band diagonal linear equations A�x = b. The solution vector x overwrites b[1..n]. The
////other input arrays are not modified, and can be left in place for successive calls with di?erent right-hand sides.
//var
//	i,k,p,mm: integer;
//    dum: double;
//begin
//	mm := m1+m2+1;
//	p := m1;
//	for k := 0 to n - 1 do
//    begin
//		//Forward substitution, unscrambling the permuted rows as we go.
//		i := indx[k]+1;
//		if (i <> k+1) then
//        begin
//            dum := b[k+1];
//            b[k+1] := b[i];
//            b[i] := dum;
//        end;
//		if (p < n) then
//            inc(p);
//		for i := k+1 to p-1 do
//            b[i] := b[i] - al[k][i-k-1]*b[k];
//	end;
//	p := 1;
//	for i := n - 1 downto 0 do
//    begin
//		//Backsubstitution.
//		dum := b[i];
//		for k := 1 to p - 1 do
//			dum := dum - a[i][k]*b[k+i];
//		b[i] := dum/a[i][0];
//		if (p < mm) then
//            inc(p);
//	end;
//end;
//
//class procedure TBandSolver.banbks(var a,al: TComplexMat; const n, m1, m2: integer; var indx: TArray<integer>; var b: TComplexVect);
//var
//	i,k,p,mm: integer;
//    dum: TComplex;
//begin
//	mm := m1+m2+1;
//	p := m1;
//	for k := 0 to n - 1 do
//    begin
//		//Forward substitution, unscrambling the permuted rows as we go.
//		i := indx[k]+1;
//		if (i <> k+1) then
//        begin
//            dum := b[k+1];
//            b[k+1] := b[i];
//            b[i] := dum;
//        end;
//		if (p < n) then
//            inc(p);
//		for i := k+1 to p-1 do
//            b[i] := b[i] - al[k][i-k-1]*b[k];
//	end;
//	p := 1;
//	for i := n - 1 downto 0 do
//    begin
//		//Backsubstitution.
//		dum := b[i];
//		for k := 1 to p - 1 do
//			dum := dum - a[i][k]*b[k+i];
//		b[i] := dum/a[i][0];
//		if (p < mm) then
//            inc(p);
//	end;
//end;

end.
