unit BiComplex;

interface

uses system.math, mcomplex, TriDiagonal;

type

    TBiComplex = record
        del, v: TComplex;

        class operator Subtract(b,a: TBiComplex): TBiComplex;
        class operator Add(b,a: TBiComplex): TBiComplex;
        class operator Multiply(a: TBiComplex; b:double): TBiComplex;
        class operator Multiply(b:double; a: TBiComplex): TBiComplex;
        class operator Multiply(a: TBiComplex; b:TComplex): TBiComplex;
        class operator Multiply(b:TComplex; a: TBiComplex): TBiComplex;
    end;
    TBiComplexArray = array of TBiComplex;

    TQuadComplex = record
        deldel, delv, vdel, vv: TComplex;

        procedure setZero;
        procedure add(const val: TQuadComplex);

        class operator Add(b,a: TQuadComplex): TQuadComplex;
        class operator Subtract(b,a: TQuadComplex): TQuadComplex;
        class operator Multiply(b:double; a: TQuadComplex): TQuadComplex;
        class operator Multiply(b:TComplex; a: TQuadComplex): TQuadComplex;
        class operator Multiply(a: TQuadComplex; b: TBiComplex): TBiComplex;
        class operator Multiply(b: TBiComplex; a: TQuadComplex): TBiComplex;
        class operator Multiply(a,b: TQuadComplex): TQuadComplex;
        class operator Divide(a: TBiComplex; b:TQuadComplex): TBiComplex;
        class operator Divide(a, b:TQuadComplex): TQuadComplex;
    end;
    pQuadComplex = ^TQuadComplex;
    TQuadComplexArray = array of TQuadComplex;

    TQuadTriComplex = array[TTriInd] of TQuadComplex;
    TQuadTriDiagonal = array of TQuadTriComplex;
    TQuadTriDiagonalHelper = record helper for TQuadTriDiagonal
    private
        function  getLowerP(i: integer): pQuadComplex;
        function  getDiagP(i: integer): PQuadComplex;
        function  getUpperP(i: integer): PQuadComplex;

        function  getLower(i: integer): TQuadComplex;
        procedure setLower(i: integer; val: TQuadComplex);
        function  getDiag (i: integer): TQuadComplex;
        procedure setDiag (i: integer; val: TQuadComplex);
        function  getUpper(i: integer): TQuadComplex;
        procedure setUpper(i: integer; const Val: TQuadComplex);
    public
        function  multByVect(q: TBiComplexArray): TBiComplexArray;

        property  lower[i: integer]: TQuadComplex read getLower write setLower;
        property  diag [i: integer]: TQuadComplex read getDiag  write setDiag ;
        property  upper[i: integer]: TQuadComplex read getUpper write setUpper;
        property  plower[i: integer]: PQuadComplex read getLowerP;
        property  pdiag [i: integer]: PQuadComplex read getDiagP;
        property  pupper[i: integer]: PQuadComplex read getUpperP;
    end;

    TFourTriDiagonal = record
        A,B,C,D: TTriDiagonal;
    end;

implementation



function TQuadTriDiagonalHelper.getDiag(i: integer): TQuadComplex;
begin
    result := self[i,kDi];
end;

function TQuadTriDiagonalHelper.getDiagP(i: integer): PQuadComplex;
begin
    result := @(self[i,kDi]);
end;

function TQuadTriDiagonalHelper.getLower(i: integer): TQuadComplex;
begin
    result := self[i,kLo];
end;

function TQuadTriDiagonalHelper.getLowerP(i: integer): pQuadComplex;
begin
    result := @(self[i,kLo]);
end;

function TQuadTriDiagonalHelper.getUpper(i: integer): TQuadComplex;
begin
    result := self[i,kUp];
end;

function TQuadTriDiagonalHelper.getUpperP(i: integer): PQuadComplex;
begin
    result := @(self[i,kUp]);
end;

procedure TQuadTriDiagonalHelper.setDiag(i: integer; val: TQuadComplex);
begin
    self[i,kDi] := val;
end;

procedure TQuadTriDiagonalHelper.setLower(i: integer; val: TQuadComplex);
begin
    self[i,kLo] := val;
end;

procedure TQuadTriDiagonalHelper.setUpper(i: integer; const Val: TQuadComplex);
begin
    self[i,kUp] := val;
end;

procedure TQuadComplex.add(const val: TQuadComplex);
begin
    deldel := deldel + val.deldel;
    delv := delv + val.delv;
    vdel := vdel + val.vdel;
    vv := vv + val.vv;
end;

class operator TQuadComplex.Multiply(b: double; a: TQuadComplex): TQuadComplex;
begin
    result.deldel := a.deldel*b;
    result.delv := a.delv*b;
    result.vdel := a.vdel*b;
    result.vv := a.vv*b;
end;

class operator TQuadComplex.add(b, a: TQuadComplex): TQuadComplex;
begin
    result.deldel := b.deldel + a.deldel;
    result.delv := b.delv + a.delv;
    result.vdel := b.vdel + a.vdel;
    result.vv := b.vv + a.vv;
end;

class operator TQuadComplex.Divide(a, b: TQuadComplex): TQuadComplex;
var
    det: TComplex;
    inv: TQuadComplex;
begin
    det := (b.deldel*b.vv - b.delv*b.vdel);
    inv.deldel := b.vv/det;
    inv.delv := -b.delv/det;
    inv.vdel := -b.vdel/det;
    inv.vv := b.deldel/det;
    result := a*inv;
end;

class operator TQuadComplex.Divide(a: TBiComplex; b: TQuadComplex): TBiComplex;
var
    det: TComplex;
    inv: TQuadComplex;
begin
    det := (b.deldel*b.vv - b.delv*b.vdel);
    inv.deldel := b.vv/det;
    inv.delv := -b.delv/det;
    inv.vdel := -b.vdel/det;
    inv.vv := b.deldel/det;
    result := inv*a;
end;

class operator TQuadComplex.Multiply(a: TQuadComplex; b: TBiComplex): TBiComplex;
begin
    result.del := a.deldel*b.del + a.delv*b.v;
    result.v := a.vdel*b.del + a.vv*b.v;
end;

function TQuadTriDiagonalHelper.multByVect(q: TBiComplexArray): TBiComplexArray;
var
    i,n: integer;
begin
    n := length(q);
    setlength(result, n);

    i := 0;
    result[i] := self.diag[i]*q[i] + self.upper[i]*q[i+1];
    for I := 1 to n - 2 do
    begin
        result[i] := self.lower[i]*q[i - 1] + self.diag[i]*q[i] + self.upper[i]*q[i+1];
    end;
    i := n - 1;
    result[i] := self.lower[i]*q[i - 1] + self.diag[i]*q[i];
end;

class operator TQuadComplex.Multiply(b: TComplex; a: TQuadComplex): TQuadComplex;
begin
    result.deldel := a.deldel*b;
    result.delv := a.delv*b;
    result.vdel := a.vdel*b;
    result.vv := a.vv*b;
end;

procedure TQuadComplex.setZero;
begin
    deldel := NullComplex;
    delv := NullComplex;
    vdel := NullComplex;
    vv := NullComplex;
end;

class operator TQuadComplex.Subtract(b, a: TQuadComplex): TQuadComplex;
begin
    result.deldel := b.deldel - a.deldel;
    result.delv := b.delv - a.delv;
    result.vdel := b.vdel - a.vdel;
    result.vv := b.vv - a.vv;
end;

class operator TBiComplex.Multiply(a: TBiComplex; b: TComplex): TBiComplex;
begin
    result.del := a.del*b;
    Result.v := a.v*b;
end;

class operator TBiComplex.Add(b, a: TBiComplex): TBiComplex;
begin
    result.del := a.del + b.del;
    result.v := a.v + b.v;
end;

class operator TBiComplex.Multiply(b: TComplex; a: TBiComplex): TBiComplex;
begin
    result.del := a.del*b;
    Result.v := a.v*b;
end;

class operator TBiComplex.Subtract(b, a: TBiComplex): TBiComplex;
begin
    result.del := b.del - a.del;
    result.v := b.v - a.v;
end;

{ TBiComplex }

class operator TBiComplex.Multiply(a: TBiComplex; b: double): TBiComplex;
begin
    result.del := a.del*b;
    Result.v := a.v*b;
end;

class operator TBiComplex.Multiply(b: double; a: TBiComplex): TBiComplex;
begin
    result.del := a.del*b;
    Result.v := a.v*b;
end;

class operator TQuadComplex.Multiply(b: TBiComplex; a: TQuadComplex): TBiComplex;
begin
    result.del := a.deldel*b.del + a.delv*b.v;
    result.v := a.vdel*b.del + a.vv*b.v;
end;

class operator TQuadComplex.Multiply(a, b: TQuadComplex): TQuadComplex;
begin
    result.deldel := a.deldel*b.deldel + a.delv*b.vdel;
    result.delv := a.deldel*b.vdel + a.delv*b.vv;
    result.vdel := a.vdel*b.deldel + a.vv*b.vdel;
    result.vv := a.vdel*b.vdel + a.vv*b.vv;
end;

end.
