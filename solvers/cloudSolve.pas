unit cloudSolve;

interface
uses
    system.classes, system.SysUtils, system.StrUtils,
    vcl.forms, vcl.dialogs, vcl.comctrls,
    IdHTTP, IdURI, IdComponent,
    webGetHTTP, SmartPointer, baseTypes;

type
    TCloudSolve = class(Tobject)
    private
        lastWork: integer;
        response: ISmart<TStringStream>;
        procedure IdHTTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
        procedure IdHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
    public
        pbStep: TProcedureWithInt;
        function  postProgress(const url, ramInput: string): string;
        class function  post(const url, ramInput: string): string;
        class function  serverAvailable(const url: string): boolean;
    end;

implementation

class function TCloudSolve.serverAvailable(const url: string): boolean;
const
    sGoodResponse = 'Server path hit';
var
    server: ISmart<TIdHttp>;
    response: ISmart<TStringStream>;
    s: string;
begin
    if not TWebGetHTTP.internetConnectionExists then exit(false);

    server := TSmart<TIdHttp>.create(TIdHttp.create(Application));
    response := TSmart<TStringStream>.create(TStringStream.Create);
    Server.Request.ContentType := 'application/x-www-form-urlencoded';
    Server.Request.Charset := 'utf-8';
    try
        Server.Get(TIdURI.URLEncode(url),response);
    except on e: exception do
        exit(false);
    end;
    response.Position := 0;
    s := response.ReadString(response.Size);
    result := s.StartsWith(sGoodResponse);
end;

procedure TCloudSolve.IdHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
begin
    //
end;

class function  TCloudSolve.post(const url, ramInput: string): string;
var
    server: ISmart<TIdHttp>;
    parameters: ISmart<TStringList>;
    res: ISmart<TStringStream>;
begin
    server := TSmart<TIdHTTP>.create(TIdHttp.create(Application));
    res := TSmart<TStringStream>.Create(TStringStream.create);
    parameters := TSmart<TStringList>.create(TStringList.Create);
    parameters.add('body=' + ramInput);
    Server.Request.ContentType := 'application/x-www-form-urlencoded';
    Server.Request.Charset := 'utf-8';
    Server.Post(TIdURI.URLEncode(url),parameters,res);
    res.Position := 0;
    result := res.ReadString(res.Size);
end;

function  TCloudSolve.postProgress(const url, ramInput: string): string;
var
    server: ISmart<TIdHttp>;
    parameters: ISmart<TStringList>;
begin
    lastWork := 0;
    server := TSmart<TIdHTTP>.create(TIdHttp.create(Application));
    server.OnWorkBegin := self.IdHTTPWorkBegin;
    server.OnWork := self.IdHTTPWork;
    response := TSmart<TStringStream>.Create(TStringStream.create);
    parameters := TSmart<TStringList>.create(TStringList.Create);
    parameters.add('body=' + ramInput);
    Server.Request.ContentType := 'application/x-www-form-urlencoded';
    //server.Request.TransferEncoding := 'chunked';
    Server.Request.Charset := 'utf-8';
    Server.Post(TIdURI.URLEncode(url),parameters,response);
    response.Position := 0;
    result := response.ReadString(response.Size);
end;

procedure TCloudSolve.IdHTTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
begin
    if assigned(pbStep) and (AWorkMode = wmRead) then
    begin
        pbstep(AWorkCount - lastWork);
        lastWork := AWorkCount;
        application.ProcessMessages;
    end;
end;


end.
