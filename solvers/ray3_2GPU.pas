unit ray3_2GPU;

interface

{$IF Defined(CLSOLVE)}

uses classes, math, Mcomplex, dialogs, ClipBrd, sysutils, baseTypes,
    forms, helperFunctions, mmsystem, dBSeaconstants, material,
    multiLayerReflectionCoefficient, seafloorScheme, directivity,
    generics.Collections, mcklib, dBSeaRayArrivals, stdCtrls, types,
    CL, CL_GL, system.Diagnostics, raySolverTypes, ray3GPUBase;

type
    TRay3_2GPU = class(TRay3GPUBase)
    private
      const
        // openCLFile = '../../OpenCL/test1.cl';
        // openCLMethodConstSSP = 'test1';
        // openCLMethodNotConstSSP = 'test1';
        openCLFile = '../../OpenCL/OpenCLdBSeaRay3_2.cl';
        openCLMethodConstSSP = 'takeStepConstSSP';
        openCLMethodNotConstSSP = 'takeStepNotConstSSP';

    var
        GPURays, GPUThisPointInfo: cl_mem;
        globalThreads: array [0 .. 0] of size_t; { [0..0], [0..1] or [0..2] depending on dimensions }

        procedure setupGPUInput(const globalMemBufferSize: cl_ulong;
          const rays: TRayIterator2Array;
          const ds: double;
          const c, dcdz, d2cdz2: TDoubleDynArray);
        procedure loadGPUMem;
    public
    var
        function initContext(const PLATF, DEVICE: integer): boolean;
        function initProgram(): boolean;
        function buildProgram(const PLATF, DEVICE: integer; const constSSP: boolean; var globalMemBufferSize, constMemBufferSize: cl_ulong): boolean;
        procedure loadInput(const nTheta: integer;
          const rays: TRayIterator2Array;
          const ds: double;
          const c, dcdz, d2cdz2: TDoubleDynArray;
          const globalMemBufferSize: cl_ulong);

        function initCL(const nTheta: integer;
          const constSSP: boolean;
          const rays: TRayIterator2Array;
          const ds: double;
          const c, dcdz, d2cdz2: TDoubleDynArray): boolean;
        function takeStep(): boolean;
        procedure getGPUOutput(const rays: TRayIterator2Array);

        procedure cleanup(const rays: TRayIterator2Array);

    end;

implementation

procedure TRay3_2GPU.setupGPUInput(const globalMemBufferSize : cl_ulong;
                                   const rays : TRayIterator2Array;
                                   const ds : double;
                                   const c, dcdz, d2cdz2 : TDoubleDynArray);
type
    TPointInfo = record
        ds,c,dcdz,d2cdz2 : double;
    end;
const
    inFlags : cl_mem_flags = CL_MEM_READ_WRITE or CL_MEM_COPY_HOST_PTR;
    outFlags : cl_mem_flags = CL_MEM_READ_WRITE or CL_MEM_COPY_HOST_PTR;
var
    i,iz : integer;
    requiredMem : cl_ulong;
    pointInfos : array of TPointInfo;
    error: cl_int;
begin
    setlength(pointInfos,length(rays));
    for i := 0 to length(rays) - 1 do
    begin
        iz := min(length(c) - 1,max(0,saferound(rays[i].z)));
        pointInfos[i].ds := ds;
        pointInfos[i].c := c[iz];
        pointInfos[i].dcdz := dcdz[iz];
        pointInfos[i].d2cdz2 := d2cdz2[iz];
    end;

    requiredMem := 0;
    requiredMem := requiredMem + SizeOf(rays[0]) * length(rays);
    requiredMem := requiredMem + sizeof(pointInfos[0]) * length(rays);
    if (requiredMem > globalMemBufferSize) then
    begin
        //showmessage('GPU memory exceeded');
    end;

    //self.GPUTestDoubleArr := clCreateBuffer(context, inFlags,  sizeof(testArray[0]) * length(testArray)     , @testArray[0], nil);
    self.GPURays            := clCreateBuffer(context, inFlags,  sizeof(rays[0]) * length(rays)     , @rays[0], @error);
    self.GPUThisPointInfo    := clCreateBuffer(context, inFlags,  sizeof(pointInfos[0]) * length(rays) , @pointInfos[0], @error);
end;

procedure TRay3_2GPU.loadGPUMem;
begin
    //clSetKernelArg(self.myKernel, 0, sizeof(cl_mem), @self.GPUTestDoubleArr);
    clSetKernelArg(self.kernel, 0, sizeof(cl_mem), @self.GPURays);
    //clSetKernelArg(self.myKernel, 1, sizeof(cl_mem), @self.GPUOutRay);
    clSetKernelArg(self.kernel, 1, sizeof(cl_mem), @self.GPUThisPointInfo);
end;

function TRay3_2GPU.initContext(const PLATF, DEVICE : integer) : boolean;
begin
    result := false;

	// Get compute devices from platform
	errcode_ret := clGetDeviceIDs(platform_devices[PLATF].platform_id, platform_devices[PLATF].device_type, 0, nil, @num_devices_returned);
    SetLength(device_ids, num_devices_returned);
    errcode_ret := clGetDeviceIDs(platform_devices[PLATF].platform_id, platform_devices[PLATF].device_type, num_devices_returned, @device_ids[0], @num_devices_returned);
    if (errcode_ret <> CL_SUCCESS) then
    begin
        ShowMessage('Error: Failed to create a device group');
        exit;
    end;

	// Create a compute context
	context := clCreateContext(nil, num_devices_returned, @device_ids[0], nil, nil, @errcode_ret);
    if (errcode_ret <> CL_SUCCESS) then
    begin
        ShowMessage('Error: Failed to create a compute context');
        exit;
    end;

    result := true;
end;

function TRay3_2GPU.initProgram() : boolean;
var
    sourceStr: AnsiString;
    sourceSize: size_t;
    sourcePAnsiChar: PAnsiChar;
begin
    result := false;

    // Create OpenCL program with source code
    sourceStr := convertToString(self.openCLFile);
    sourceSize := Length(sourceStr);
    sourcePAnsiChar := PAnsiChar(sourceStr);
    OpenCLProgram := clCreateProgramWithSource(context, 1, @sourcePAnsiChar, @sourceSize, @errcode_ret);
    //showmessage(tostr(sourcesize));
	if errcode_ret <> CL_SUCCESS then
    begin
        ShowMessage('Error: clCreateProgramWithSource failed ');
        clReleaseContext(context);
        exit;
    end;

    result := true;
end;

function TRay3_2GPU.buildProgram(const PLATF, DEVICE: integer; const constSSP: boolean; var globalMemBufferSize, constMemBufferSize: cl_ulong): boolean;
var
    s, error_string: AnsiString;
    returned_size: size_t;
    kernelError: cl_int;
begin
    result := false;

	// Build the program (OpenCL JIT compilation)
	if CL_SUCCESS <> clBuildProgram(OpenCLProgram, 0, nil, nil, nil, nil) then
    begin
        error_string := 'Error: clBuildProgram failed';
        clGetProgramBuildInfo(OpenCLProgram, device_ids[DEVICE], CL_PROGRAM_BUILD_LOG, 0, nil, @returned_size);
        SetLength(s, returned_size+2);
        clGetProgramBuildInfo(OpenCLProgram, device_ids[DEVICE], CL_PROGRAM_BUILD_LOG, Length(s), PAnsiChar(s), @returned_size);
        SetLength(s, Min(Pos(#0, string(s))-1, returned_size-1));
        error_string := error_string + s;

        ShowMessage(string(error_string));
        clReleaseProgram(OpenCLProgram);
        clReleaseContext(context);
        exit;
    end;

	// Create a handle to the compiled OpenCL function (Kernel)
    if ((raySolverIterationMethod = kConstSSP) and constSSP) or (raySolverIterationMethod = kRK2) then
	    kernel := clCreateKernel(OpenCLProgram, PAnsiChar(self.openCLMethodConstSSP), @kernelError)
    else
        kernel := clCreateKernel(OpenCLProgram, PAnsiChar(self.openCLMethodNotConstSSP), @kernelError);

	// Create a command-queue on the first CPU or GPU device
	self.CommandQueue := clCreateCommandQueue(context, device_ids[DEVICE], 0, nil);

    //get memory sizes
    if CL_SUCCESS <> clGetDeviceInfo(device_ids[DEVICE],
                                    CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
                                    sizeof(cl_ulong),
                                    @globalMemBufferSize,
                                    nil) then
    begin
        //
    end;

    if CL_SUCCESS <> clGetDeviceInfo(device_ids[DEVICE],
                                    CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
                                    sizeof(cl_ulong),
                                    @constMemBufferSize,
                                    nil) then
    begin
        //
    end;

    result := true;
end;

procedure TRay3_2GPU.loadInput(const nTheta : integer;
                         const rays : TRayIterator2Array;
                         const ds : double;
                         const c, dcdz, d2cdz2 : TDoubleDynArray;
                         const globalMemBufferSize: cl_ulong);
begin
    self.globalThreads[0] := nTheta;
    self.setupGPUInput(globalMemBufferSize, rays, ds, c, dcdz, d2cdz2);
    self.loadGPUMem;
end;


function TRay3_2GPU.initCL(const nTheta : integer;
                         const constSSP : boolean;
                         const rays : TRayIterator2Array;
                         const ds : double;
                         const c, dcdz, d2cdz2 : TDoubleDynArray) : boolean;
const
    PLATF = 0;
    DEVICE = 0;
var
    globalMemBufferSize, constMemBufferSize : cl_ulong;
begin
    result := false;
    if not self.initContext(PLATF,DEVICE) then exit;
    if not self.initProgram then exit;
    if not self.buildProgram(PLATF,DEVICE,constSSP,globalMemBufferSize, constMemBufferSize) then exit;

    self.loadInput(nTheta,rays,ds,c,dcdz,d2cdz2,globalMemBufferSize);
    result := true;
end;


function TRay3_2GPU.takeStep():boolean;
var
    clReturn: cl_int;
begin
	// Launch the Kernel on the GPU
    clReturn := clEnqueueNDRangeKernel(self.CommandQueue,    {command_queue}
                           self.kernel, {kernel}
                           1, {work dim: 1, 2 or 3}
                           nil, {global_work_offset, must be null}
                           @self.globalThreads,{global_work_size, array of length work_dim}
                           nil, {local_work_size: nil means opencl decides local group size}
                           0,   {num_events_in_wait_list: wait for these to be done before doing calcs}
                           nil, {event_wait_list}
                           nil);
    result := clReturn = CL_SUCCESS;
    if not result then {event: returns this event, if we need to wait for this to finish before doing something else}
    begin
        // running the command has failed, handle errors
        case clReturn of
        CL_OUT_OF_RESOURCES : ShowMessage('Out of resources error calling OpenCL');
        else ShowMessage('Error calling GPU code: ' + IntToStr(clReturn));
        end;
    end;
end;


procedure TRay3_2GPU.cleanup(const rays : TRayIterator2Array);
begin
	// Copy the output in GPU memory back to CPU memory
    clReleaseMemObject(GPURays);
    clReleaseMemObject(GPUThisPointInfo);

	clReleaseCommandQueue(self.CommandQueue);
	clReleaseKernel(self.kernel);
    clReleaseProgram(self.OpenCLProgram);
 	clReleaseContext(self.context);
end;


procedure TRay3_2GPU.getGPUOutput(const rays: TRayIterator2Array);
var
    //i : integer;
    clret : cl_int;
begin
    clret := clEnqueueReadBuffer(CommandQueue, self.GPURays, CL_TRUE, 0, sizeof(rays[0]) * length(rays), @rays[0], 0, nil, nil);
    THelper.removeWarning(clret);

//    result := '';
//    for I := 0 to length(rays) - 1 do
//        result := result + 'r' + tostr(rays[i].r) + '  z' + tostr(rays[i].z) + '  zeta' + tostr(rays[i].zeta) +
//             '  xi' + tostr(rays[i].xi) + '  q' + tostr(rays[i].q) + '  p' + tostr(rays[i].p) + #13#10;
    //showmessage(s);
end;

{$ELSE}

implementation

{$ENDIF}

end.
