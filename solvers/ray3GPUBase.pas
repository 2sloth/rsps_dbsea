unit ray3GPUBase;

interface

{$IF Defined(CLSOLVE)}

uses classes, math, dialogs, sysutils, forms, CL, CL_GL, raySolverTypes,
    generics.collections;

type

  TReplaceStringCouple = class(TObject)
      fromStr, toStr : string;
  end;
  TReplaceStringCoupleList = TObjectList<TReplaceStringCouple>;


  TRay3GPUBase = class(TObject)
  public
  const
    c_device_types : array[1..3] of cardinal = (CL_DEVICE_TYPE_CPU, CL_DEVICE_TYPE_GPU, CL_DEVICE_TYPE_ACCELERATOR);
  var
    context : cl_context; // compute context
    OpenCLProgram: cl_program;
    CommandQueue: cl_command_queue;
    kernel: cl_kernel;

    errcode_ret : cl_int; // error code returned from api calls
    platform_devices : array of t_platform_device;
  	num_devices_returned : cl_uint;
  	device_ids : array of cl_device_id; // compute device IDs

    class function convertToString(const Filename: String): AnsiString;
    procedure readOpenCLCouples;
    procedure doSetup(const PLATF, DEVICE: integer;
                      const openCLFile, methodName: string;
                      const replaceStrings: TReplaceStringCoupleList);
  end;

procedure TRay3GPUBase.readOpenCLCouples;
var
    i, j: integer;
    num_platforms_returned: cl_uint;
    platform_id: array of cl_platform_id; // platform id
    i_pl: integer;
    i_type: integer;
    device_vendor, device_name: AnsiString;
    returned_size: size_t;
begin
    if not OpenCL_loaded then
    begin
        ShowMessage('Error: OpenCL.dll not loaded!');
        exit;
    end;

    // Number of platforms
	errcode_ret := clGetPlatformIDs(0, nil, @num_platforms_returned);
	if (errcode_ret <> CL_SUCCESS) then
    begin
		ShowMessage('Error: Failed to get number of platforms!');
		exit;
    end;
    // Connect to a platform
    SetLength(platform_id, num_platforms_returned);
	errcode_ret := clGetPlatformIDs(num_platforms_returned, @platform_id[0], @num_platforms_returned);
	if (errcode_ret <> CL_SUCCESS) then
    begin
		ShowMessage('Error: Failed to find platform!');
		exit;
    end;

	// find platform+device_type
	SetLength(platform_devices, 0);

    for i_pl := 0 to num_platforms_returned - 1 do
        for i_type := 1 to 3 do
            if (CL_SUCCESS = clGetDeviceIDs(platform_id[i_pl], self.c_device_types[i_type], 0, nil, @num_devices_returned)) and (num_devices_returned>=1) then
            begin
                //if self.c_device_types[i_type] = CL_DEVICE_TYPE_CPU then
                //    showmessage('cpu');
                SetLength(platform_devices, Length(platform_devices)+1);
                platform_devices[High(platform_devices)].platform_id := platform_id[i_pl];
                platform_devices[High(platform_devices)].device_type := self.c_device_types[i_type];
            end;

	// list platform+device_type couples
	for i := 0 to High(platform_devices) do
    begin
		errcode_ret := clGetDeviceIDs(platform_devices[i].platform_id, platform_devices[i].device_type, 0, nil, @num_devices_returned);
		if (errcode_ret <> CL_SUCCESS) then
        begin
  		    ShowMessage('Error: Failed to create a device group!');
	  	    exit;
        end;

		SetLength(device_ids, num_devices_returned);
		errcode_ret := clGetDeviceIDs(platform_devices[i].platform_id, platform_devices[i].device_type, num_devices_returned, @device_ids[0], @num_devices_returned);
		for j := 0 to num_devices_returned - 1 do
        begin
            SetLength(device_name, 1024);
			clGetDeviceInfo(device_ids[j], CL_DEVICE_NAME, Length(device_name), PAnsiChar(device_name), @returned_size);
            SetLength(device_name, Min(Pos(#0, string(device_name)) - 1, returned_size-1));
            SetLength(device_vendor, 1024);
			clGetDeviceInfo(device_ids[j], CL_DEVICE_VENDOR, Length(device_vendor), PAnsiChar(device_vendor), @returned_size);
            SetLength(device_vendor, Min(Pos(#0, string(device_vendor)) - 1, returned_size-1));
		end;
		SetLength(device_ids, 0);
	end;
end;



class function TRay3GPUBase.convertToString(const Filename: String): AnsiString;
begin
    with TFileStream.Create(IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName))+Filename, fmOpenRead or fmShareDenyWrite) do
    try
        SetLength(Result, Size);
        if Size > 0 then
            read(Result[1], Size);
    finally
        Free;
    end;
end;

procedure TRay3GPUBase.doSetup(const PLATF, DEVICE : integer;
                               const openCLFile, methodName : string;
                               const replaceStrings : TReplaceStringCoupleList);
var
    sourceStr: AnsiString;
    methodStr : AnsiString;
    sourceSize: size_t;
    sourcePAnsiChar: PAnsiChar;
    OpenCLProgram: cl_program;
    s, error_string: AnsiString;
    returned_size: size_t;
    couple : TReplaceStringCouple;
begin
    ////////////////
    // Init Context

	// Get compute devices from platform
	errcode_ret := clGetDeviceIDs(platform_devices[PLATF].platform_id, platform_devices[PLATF].device_type, 0, nil, @num_devices_returned);
    SetLength(device_ids, num_devices_returned);
    errcode_ret := clGetDeviceIDs(platform_devices[PLATF].platform_id, platform_devices[PLATF].device_type, num_devices_returned, @device_ids[0], @num_devices_returned);
    if (errcode_ret <> CL_SUCCESS) then
    begin
        ShowMessage('Error: Failed to create a device group');
        exit;
    end;

	// Create a compute context
	context := clCreateContext(nil, num_devices_returned, @device_ids[0], nil, nil, @errcode_ret);
    if (errcode_ret <> CL_SUCCESS) then
    begin
        ShowMessage('Error: Failed to create a compute context');
        exit;
    end;
    // End (Init Context)
    ////////////////

    // Create OpenCL program with source code
    sourceStr := TRay3GPUBase.convertToString(openCLFile);
    if assigned(replaceStrings) then
        for couple in replaceStrings do
            sourceStr := ansistring( StringReplace(string(sourceStr), couple.fromStr, couple.toStr, [rfReplaceAll, rfIgnoreCase]) );

    sourceSize := Length(sourceStr);
    sourcePAnsiChar := PAnsiChar(sourceStr);
    OpenCLProgram := clCreateProgramWithSource(context, 1, @sourcePAnsiChar, @sourceSize, @errcode_ret);
    //showmessage(tostr(sourcesize));
	if errcode_ret <> CL_SUCCESS then
    begin
        ShowMessage('Error: clCreateProgramWithSource failed');
        clReleaseContext(context);
        exit;
    end;

	// Build the program (OpenCL JIT compilation)
	if CL_SUCCESS <> clBuildProgram(OpenCLProgram, 0, nil, nil, nil, nil) then
    begin
        error_string := 'Error: clBuildProgram failed';
        clGetProgramBuildInfo(OpenCLProgram, device_ids[DEVICE], CL_PROGRAM_BUILD_LOG, 0, nil, @returned_size);
        SetLength(s, returned_size+2);
        clGetProgramBuildInfo(OpenCLProgram, device_ids[DEVICE], CL_PROGRAM_BUILD_LOG, Length(s), PAnsiChar(s), @returned_size);
        SetLength(s, Min(Pos(#0, string(s))-1, returned_size-1));
        error_string := error_string + s;

        ShowMessage(string(error_string));
        clReleaseProgram(OpenCLProgram);
        clReleaseContext(context);
        exit;
    end;
	// Create a handle to the compiled OpenCL function (Kernel)
    methodStr := ansistring(methodName);
	self.kernel := clCreateKernel(OpenCLProgram, PAnsiChar(methodStr), nil);
end;

{$ELSE}
implementation
 {$ENDIF}
end.

