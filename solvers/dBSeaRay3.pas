unit dBSeaRay3;

interface

uses
    system.classes, system.math, system.types, system.Diagnostics,
    generics.Collections,
    dBSeaconstants, material,
    helperFunctions, Mcomplex, baseTypes,
    multiLayerReflectionCoefficient, seafloorScheme, directivity,
    mcklib, dBSeaRayArrivals, RaySolverCommon,
    {$IF Defined(CLSOLVE)}CL, CL_GL, ray3_2GPU,{$ENDIF}
    raySolverTypes,
    waterProps, solversGeneralFunctions, SmartPointer;

type

  TdBSeaRay3Solver = class(TObject)
  public
    class procedure mainMultipleFrequency(const f: TDoubleDynArray;
                                    const solveThisFreq: TBooleanDynArray;
                                    const srcZ, rmax, dr, zmax, startInitialAngle, endInitialAngle, initialStepsize: double;
                                    const aNz: integer;
                                    const azProfile2: TRangeValArray;
                                    const aCProfile3: TDepthValArrayAtRangeArray;
                                    const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp: double;
                                    const maxBottomReflections, aNTheta: integer;
                                    const returnTL: boolean;
                                    const seafloors: TSeafloorRangeArray;
                                    const directivities: TDirectivityList;
                                    const fs: double;
                                    const outputArrivals, calculateVolumeAttenuationAtEachStep: boolean;
                                    const raySolverIterationMethod: TRaySolverIterationMethod;
                                    const cancel: TWrappedBool;
                                    const pSolveriMax: TWrappedInt; {= nr, num range points}
                                    const fractionDone: TWrappedDouble;
                                    const useGPU: boolean;
                                    out tlMultiFreq: TDoubleField{f,i,j};
                                    out pFieldMultiFreq: TComplexField{f,i,j};
                                    var arrivals: TArrivalsField;
                                    //out solverErrorMessages: String;
                                    var solverErrors: TSolverErrors);
  end;

  TRay3WorkOutsideLoop = class(TObject)
  public
    class procedure initProblem(const rmax, zmax, dr, initialStepsize: double;
                                const aNTheta, nz: integer;
                                const f: TDoubleDynArray;
                                const azProfile2: TRangeValArray;
                                const aCProfile2: TDepthValArray;
                                out c0, rho0, ds, deltar, deltaz: double;
                                out nr, ntheta, nf: integer;
                                out rayPhase, rayWidth, Abeam, reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TMatrix;
                                out bPastRayWidthXOver: TBoolMatrix);
  end;

  TRay3Work = class(TObject)
  public
    class function  rayTerminateFromHitSurface(const solveFreqInds: TArray<integer>;
                                               var xnext, xprev: TRayIterator2;
                                               const nf: integer;
                                               const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag, c: TDoubleDynArray;
                                               var inAir: boolean): boolean;
    class function  rayTerminateFromHitBottom(const solveFreqInds: TArray<integer>;
                                                    var xnext, xprev: TRayIterator2;
                                                    const nf, maxBottomReflections: integer;
                                                    const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TDoubleDynArray;
                                                    const c0, cb: double;
                                                    const f, c: TDoubleDynArray;
                                                    const zProfile: TRangeValArray;
                                                    const seafloors: TSeafloorRangeArray;
                                                    const layers: TSeafloorReflectionLayers;
                                                    var seafloor: TSeafloorScheme;
                                                    var seafloorIndex: integer;
                                                    var inSeafloor: boolean;
                                                    out thisZb: double;
                                                    var nReflections: integer): boolean;
    class procedure calculateRayParameters(const solveFreqInds: TArray<integer>;
                                           const c, f: TDoubleDynArray;
                                           var xprev, xnext: TRayIterator2;
                                           const nf: integer;
                                           const delTheta, thisZb, c0, cosTheta0, waterTemp: double;
                                           var rayPhase, rayWidth, Abeam: TDoubleDynArray;
                                           const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TDoubleDynArray;
                                           const bPastRayWidthXOver: TBooleanDynArray;
                                           var delay: double;
                                           const calculateVolumeAttenuationAtEachStep: boolean);
    class procedure updateFieldOrArrivalsFromRay(const solveFreqInds: TArray<integer>;
                                      var xprev, xnext: TRayIterator2;
                                      const deltaR, deltaz, delay: double;
                                      const rayWidth, rayPhase, ABeam: TDoubleDynArray;
                                      const nf, nz, nr: integer;
                                      const outputArrivals: boolean;
                                      const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TDoubleDynArray;
                                      const arrivals: TArrivalsField;
                                      const pField: TComplexField);
    class procedure initRay(const solveFreqInds: TArray<integer>;
                            const iTheta, nf: integer;
                            const srcZ, c0: double;
                            out ray: TRayIterator2;
                            const theta: TDoubleDynArray;
                            const aDirectivity: TSingleMatrix;
                            const rayPhase: TDoubleDynArray;
                            out delay, cosTheta0: double;
                            out nReflections: integer;
                            out inSeafloor, inAir: Boolean;
                            const reflectionAmplitudePhaseReal,reflectionAmplitudePhaseImag: TDoubleDynArray;
                            const bPastRayWidthXOver: TBooleanDynArray);
    class procedure initRays(const solveFreqInds: TArray<integer>;
                             const nTheta, nf: integer;
                                  const srcZ, c0: double;
                                  out rays, lastStepRays: TRayIterator2Array;
                                  const theta: TDoubleDynArray;
                                  const aDirectivity: TSingleMatrix;
                                  out rayPhase: TMatrix;
                                  out delay, xi, cosTheta0, thisZb: TDoubleDynArray;
                                  out nReflections: TLongIntDynArray;
                                  out inSeafloor, inAir: TBooleanDynArray;
                                  out reflectionAmplitudePhaseReal,reflectionAmplitudePhaseImag: TMatrix;
                                  out bPastRayWidthXOver: TBoolMatrix);

  end;

implementation

{$B-}

class procedure TdBSeaRay3Solver.mainMultipleFrequency(const f: TDoubleDynArray;
                                                 const solveThisFreq: TBooleanDynArray;
                                                 const srcZ, rmax, dr, zmax, startInitialAngle, endInitialAngle, initialStepsize: double;
                                                 const aNz: integer;
                                                 const azProfile2: TRangeValArray;
                                                 const aCProfile3: TDepthValArrayAtRangeArray;
                                                 const firstSeafloorLayerC, firstSeafloorLayerRho, waterTemp: double;
                                                 const maxBottomReflections, aNTheta: integer;
                                                 const returnTL: boolean;
                                                 const seafloors: TSeafloorRangeArray;
                                                 const directivities: TDirectivityList;
                                                 const fs: double;
                                                 const outputArrivals, calculateVolumeAttenuationAtEachStep: boolean;
                                                 const raySolverIterationMethod: TRaySolverIterationMethod;
                                                 const cancel: TWrappedBool;
                                                 const pSolveriMax: TWrappedInt;
                                                 const fractionDone: TWrappedDouble;
                                                 const useGPU: boolean;
                                                 out tlMultiFreq: TDoubleField;
                                                 out pFieldMultiFreq: TComplexField;
                                                 var arrivals: TArrivalsField;
                                                 //out solverErrorMessages: String;
                                                 var solverErrors: TSolverErrors);
//NB kill ray after maxBottomReflections reflections from the bottom
var
    c,dc_dz,d2c_dz2,prev_c,prev_dc_dz,prev_d2c_dz2: TDoubleDynArray;
    nz, nf, nr, ntheta, itheta, iz: LongInt;
    cb, rho0, c0, deltaz, deltar, ds, delTheta: double;
    constSSP: boolean;
    theta: TDoubleDynArray;
    directivityMat: TSingleMatrix;
    //sortedUniqueDepths: TDoubleDynArray;

    rayPhase, rayWidth, Abeam, ReflectionAmplitudePhaseReal, ReflectionAmplitudePhaseImag: TMatrix;
    bPastRayWidthXOver: TBoolMatrix;
    thisZb, delay, cosTheta0: TDoubleDynArray;
    inSeafloor, inAir: TBooleanDynArray;
    nReflections: TLongIntDynArray;
    layers: ISmart<TSeafloorReflectionLayers>;

    xnext,xprev: TRayIterator2;
    rays: TRayIterator2Array;
    {$IF Defined(CLSOLVE)}
    lastStepRays: TRayIterator2Array;
    rayGPU2: ISmart<TRay3_2GPU>;
    {$ENDIF}

    seafloor: TSeafloorScheme;
    seafloorIndex: integer;
    currentCProfile: TDepthValArray;
    CProfileRangeInd, prevProfileRangeInd: integer;
    prevR, prev_dc_dr: double;
    solveFreqInds: TArray<integer>;

    procedure initArrivalsOrField;
    var
        fi, ir, iz : integer;
    begin
        if outputArrivals then
        begin
            if assigned(arrivals) then
                arrivals.reinitialise(nf,nr,1,nz,fs)
            else
                arrivals := TArrivalsField.Create(nf,nr,1,nz,fs);
        end
        else
        begin
            setlength(pFieldMultiFreq,nf,nz,nr);
            for fi := 0 to nf - 1 do
                for ir := 0 to nr - 1 do
                    for iz := 0 to nz - 1 do
                    begin
                        pFieldMultiFreq[fi,iz,ir].r := 0;
                        pFieldMultiFreq[fi,iz,ir].i := 0;
                    end;
        end;
    end;

    procedure outputTL;
    var
        fi, ir, iz : integer;
        thisModulusSquared : double;
    begin
        setlength(tlMultiFreq,nf,nz,nr);
        for fi := 0 to nf - 1 do
            for ir := 0 to nr - 1 do
                for iz := 0 to nz - 1 do
                    if solveThisFreq[fi] then
                    begin
                        thisModulusSquared := pFieldMultiFreq[fi,iz,ir].modulusSquared;
                        if thisModulusSquared > 0 then
                            tlMultiFreq[fi,iz,ir] := -10*log10(thisModulusSquared)
                        else
                            tlMultiFreq[fi,iz,ir] := 0;
                    end
                    else
                        tlMultiFreq[fi,iz,ir] := nan;
    end;

    function getSortedUniqueDepths(const zb : TDoubleDynArray) : TDoubleDynArray;
    begin
        result := zb.copy;
        result.sort;
        result := result.unique;
    end;

    function nTerminated: integer;
    var
        iter: TRayIterator2;
    begin
        result := 0;
        for iter in rays do
            if iter.terminated <> 0 then
                inc(result);
    end;

    function allTerminated: boolean;
    var
        iter: TRayIterator2;
    begin
        result := true;
        for iter in rays do
            if iter.terminated = 0 then
                exit(false);
    end;
begin
    solveFreqInds := TSolversGeneralFunctions.getSolveFreqInds(solveThisFreq);
    if solveThisFreq.allFalse() then
        include(solverErrors,kNoFrequencies);

    nz := aNz;
    cb := firstSeafloorLayerC;
    //rhob := firstSeafloorLayerRho;    //is already converted to specific density

    seafloor := seafloors.seafloorForRange(0, seafloorIndex);

    currentCProfile := aCProfile3.depthValForRange(0, CProfileRangeInd);
    layers := TSmart<TSeafloorReflectionLayers>.create(TSeafloorReflectionLayers.create(true));

    TRay3WorkOutsideLoop.initProblem(rmax, zmax, dr, initialStepsize, aNTheta, nz, f, aZProfile2, currentCProfile, c0, rho0, ds, deltar, deltaz, nr, ntheta, nf, rayPhase, rayWidth, Abeam, reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag, bPastRayWidthXOver);
    TRaySolverCommon.initCProfiles(currentCProfile,constSSP,c,dc_dz,d2c_dz2);
    prev_c := c.copy;
    prev_dc_dz := dc_dz.copy;
    prev_d2c_dz2 := d2c_dz2.copy;

    TSolversGeneralFunctions.setupRayAngles(startInitialAngle,endInitialAngle,ntheta,theta,delTheta);
    directivityMat := TSolversGeneralFunctions.getDirectivityAsMatrix(f,theta,directivities);
    initArrivalsOrField;
    pSolveriMax.i := nr;

    {$IF Defined(CLSOLVE)}
    if useGPU then
    begin
        rayGPU2 := TSmart<TRay3_2GPU>.create(TRay3_2GPU.Create);
        rayGPU2.readOpenCLCouples;

        TRay3Work.initRays(solveThisFreq, nTheta, nf, srcZ, c0, rays, lastStepRays, theta, directivityMat, rayPhase, delay, xi, cosTheta0, thisZb, nReflections, inSeafloor, inAir, reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag, bPastRayWidthXOver);

        rayGPU2.initCL(ntheta,constSSP,rays,ds,c,dcdz,d2cdz2);

        while not allTerminated() do
        begin
            if (cancel <> nil) and cancel.b then break;

            for iTheta := 0 to ntheta - 1 do
                lastStepRays[iTheta].cloneFrom(rays[iTheta]);

            if not rayGPU2.takeStep() then
            begin
                //call failed
            end;
            rayGPU2.getGPUOutput(rays);

            for iTheta := 0 to ntheta - 1 do
            begin
                if rays[iTheta].terminated <> 0  then continue;

                //check for reasons to stop iterating: out of surface, out of bottom, out of x range
                if TRay3Work.rayTerminateFromHitSurface(solveThisFreq, rays[iTheta], lastStepRays[iTheta], nf, reflectionAmplitudePhaseReal[iTheta], reflectionAmplitudePhaseImag[iTheta], c, inAir[iTheta]) or {surface}
                   TRay3Work.rayTerminateFromHitBottom(solveThisFreq, rays[iTheta], lastStepRays[iTheta], nf, maxBottomReflections, reflectionAmplitudePhaseReal[iTheta], reflectionAmplitudePhaseImag[iTheta], c0, cb, f, c, r_zb, zb, seafloor, inSeafloor[iTheta], xi[iTheta], thisZb[iTheta], nReflections[iTheta]) or {sea bottom}
                   (rays[iTheta].r < 0) or (rays[iTheta].r > rmax) then  {range checks - range < 0 means the ray exited the bounding box to the left}
                begin
                    rays[itheta].terminated := 1;
                    fractionDone.d := nTerminated()/ntheta;
                end
                else
                begin
                    TRay3Work.calculateRayParameters(solveThisFreq, c, f,
                        lastStepRays[iTheta], rays[iTheta],
                        nf, delTheta, thisZb[iTheta], c0, cosTheta0[iTheta], waterTemp,
                        rayPhase[iTheta], rayWidth[iTheta], Abeam[iTheta],
                        reflectionAmplitudePhaseReal[iTheta], reflectionAmplitudePhaseImag[iTheta],
                        bPastRayWidthXOver[iTheta],
                        delay[iTheta],
                        calculateVolumeAttenuationAtEachStep);
                    TRay3Work.updateFieldOrArrivalsFromRay(solveThisFreq, lastStepRays[iTheta], rays[iTheta], deltaR, deltaz, delay[iTheta], rayWidth[iTheta], rayPhase[iTheta], ABeam[iTheta], nf, nz, nr, outputArrivals, reflectionAmplitudePhaseReal[iTheta],reflectionAmplitudePhaseImag[iTheta], arrivals, pFieldMultiFreq);
                end;
            end;
        end; //all terminated

        rayGPU2.cleanup(rays);
    end
    else
    {$ENDIF}
    begin
        //Not GPU

        setlength(rayPhase,1,nf);
        setlength(delay,1);
        setlength(cosTheta0,1);
        setlength(nReflections,1);
        setlength(inSeafloor,1);
        setlength(inAir,1);
        setlength(reflectionAmplitudePhaseReal,1,nf);
        setlength(reflectionAmplitudePhaseImag,1,nf);
        setlength(bPastRayWidthXOver,1,nf);
        setlength(thisZb,1);

        //RAY LOOP
        for itheta := 1 to ntheta - 1 do
        begin
            if (cancel <> nil) and cancel.b then exit();

            //source can't be below the seafloor (currently)
            if srcZ > aZProfile2[0].val then
            begin
                include(solverErrors,kSourceBelowSeafloorRaySolver);
                break;
            end;

            TRay3Work.initRay(solveFreqInds, iTheta, nf, srcZ, c0, xnext, theta, directivityMat, rayPhase[0], delay[0], cosTheta0[0], nReflections[0], inSeafloor[0], inAir[0], reflectionAmplitudePhaseReal[0], reflectionAmplitudePhaseImag[0], bPastRayWidthXOver[0]);

            CProfileRangeInd := 0;
            prev_dc_dr := 0;

            while true do
            begin
                if (cancel <> nil) and cancel.b then break;

                prevR := xprev.r;
                xprev.cloneFrom(xnext);

                prevProfileRangeInd := CProfileRangeInd;
                currentCProfile := aCProfile3.depthValForRange(xnext.r, CProfileRangeInd);
                if CProfileRangeInd <> prevProfileRangeInd then
                begin
                    c.copyInPlace(prev_c);
                    dc_dz.copyInPlace(dc_dz);
                    d2c_dz2.copyInPlace(d2c_dz2);
                    TRaySolverCommon.initCProfiles(currentCProfile, constSSP, c, dc_dz, d2c_dz2);
                end;

                iz := min(length(c) - 1,max(0,saferound(xprev.z)));
                {take a step, via 2nd or 4th order runge kutta (depending on whether the SSP is const or not) // TODO smaller stepsize accuracy checking/rk-fehlberg}
                if ((raySolverIterationMethod = kConstSSP) and constSSP) or (raySolverIterationMethod = kRK2) then
                    xnext := TSolversGeneralFunctions.rk2StepMCK(xprev, ds, prevR, c[iz], dc_dz[iz], d2c_dz2[iz], prev_c[iz], prev_dc_dz[iz], prev_dc_dr)
                else
                    xnext := TSolversGeneralFunctions.rk4StepMCK(xprev, ds, prevR, c[iz], dc_dz[iz], d2c_dz2[iz], prev_c[iz], prev_dc_dz[iz], prev_dc_dr);

                //check for reasons to stop iterating: out of surface, out of bottom, out of x range
                if TRay3Work.rayTerminateFromHitSurface(solveFreqInds, xnext, xprev, nf, reflectionAmplitudePhaseReal[0], reflectionAmplitudePhaseImag[0], c, inAir[0]) or {surface}
                   TRay3Work.rayTerminateFromHitBottom(solveFreqInds, xnext, xprev, nf, maxBottomReflections, reflectionAmplitudePhaseReal[0], reflectionAmplitudePhaseImag[0], c0, cb, f, c, azProfile2, seafloors, layers, seafloor, seafloorIndex, inSeafloor[0], thisZb[0], nReflections[0]) or {sea bottom}
                   (xnext.r < 0) or (xnext.r > rmax) then  {range checks - range < 0 means the ray exited the bounding box to the left}
                begin
                    break;
                end
                else
                begin
                    TRay3Work.calculateRayParameters(solveFreqInds, c, f, xprev, xnext, nf, delTheta, thisZb[0], c0, cosTheta0[0], waterTemp, rayPhase[0], rayWidth[0], Abeam[0], reflectionAmplitudePhaseReal[0],reflectionAmplitudePhaseImag[0], bPastRayWidthXOver[0], delay[0], calculateVolumeAttenuationAtEachStep);
                    TRay3Work.updateFieldOrArrivalsFromRay(solveFreqInds, xprev, xnext, deltaR, deltaz, delay[0], rayWidth[0], rayPhase[0], ABeam[0], nf, nz, nr, outputArrivals, reflectionAmplitudePhaseReal[0],reflectionAmplitudePhaseImag[0], arrivals, pFieldMultiFreq);
                end;
            end; //while
            fractionDone.d := itheta / ntheta;
        end; //for itheta
    end;

    if (not outputArrivals) and returnTL then
        outputTL;
end;

class function TRay3Work.rayTerminateFromHitSurface(const solveFreqInds: TArray<integer>;
                                                    var xnext, xprev : TRayIterator2;
                                                    const nf : integer;
                                                    const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag, c : TDoubleDynArray;
                                                    var inAir : boolean) : boolean;
var
    stepdz, stepdr, inTheta, outTheta, cHere : double;
    fi : integer;
begin
    if (xnext.z < 0) then
    begin
        {ray hit the surface}
        result := inAir;
        inAir := true;

        stepdz := xnext.z - xprev.z;
        stepdr := xnext.r - xprev.r;
        inTheta := arctan2(stepdz,stepdr);
        outTheta := -inTheta;     //eq 3.173
        cHere := c[min(length(c) - 1,max(0,saferound(xprev.z)))]; //todo average for this ray
        xnext.xi := cos(outTheta)/cHere;
        xnext.r := xprev.r - xprev.z/stepdz*stepdr;
        xnext.z := 0;
        xnext.zeta := sin(outTheta)/cHere;    //zeta

        for fi in solveFreqInds do
        begin
            reflectionAmplitudePhaseReal[fi] := reflectionAmplitudePhaseReal[fi] * (-1); //phase change on reflection
            reflectionAmplitudePhaseImag[fi] := reflectionAmplitudePhaseImag[fi] * (-1); //phase change on reflection
        end;
    end
    else
    begin
        inAir := false;
        result := false;
    end;
end;

class function TRay3Work.rayTerminateFromHitBottom(const solveFreqInds: TArray<integer>;
                                                    var xnext, xprev: TRayIterator2;
                                                    const nf, maxBottomReflections: integer;
                                                    const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TDoubleDynArray;
                                                    const c0, cb: double;
                                                    const f, c: TDoubleDynArray;
                                                    const zProfile: TRangeValArray;
                                                    const seafloors: TSeafloorRangeArray;
                                                    const layers: TSeafloorReflectionLayers;
                                                    var seafloor: TSeafloorScheme;
                                                    var seafloorIndex: integer;
                                                    var inSeafloor: boolean;
                                                    out thisZb: double;
                                                    var nReflections: integer): boolean;
var
    stepdz, stepdr, inTheta, outTheta, cHere: double;
    fi: integer;
    theta1, mwat, cwat: double;
    rCoeff: TComplex;
    tmp: double;
    thisBottom: TGetDepthRec;
begin
    //get bottom info at this point
    thisBottom := TSolversGeneralFunctions.getdepth(zProfile,xnext.r);
    thisZb := thisBottom.z;

    if thisZb <= 0 then
    begin
        result := true; //stop ray at 0 depth
    end
    else
    if (xnext.z > thisZb) then //todo interpolate zb for range
    begin
        {hit the bottom}
        result := inSeafloor;
        inSeafloor := true;
        inc(nReflections);

        //kill ray if it has had more than the max number of reflections
        if nReflections >= maxBottomReflections then
            result := true;

        stepdz := xnext.z - xprev.z;
        stepdr := xnext.r - xprev.r;
        inTheta := arctan2(stepdz,stepdr);
        cHere := c[min(length(c) - 1,max(0,saferound(xprev.z)))]; //todo average for this ray
        theta1 := inTheta - thisBottom.ang;
        outTheta := -(inTheta - 2*thisBottom.ang);
        mwat := stepdz/stepdr;
        cwat := xprev.z - mwat*xprev.r;

        xnext.xi := cos(outTheta)/cHere;
        xnext.r := (cwat - thisBottom.c)/(thisBottom.m - mwat);
        xnext.z := mwat*xnext.r + cwat;

        seafloor := seafloors.seafloorForRange(xnext.r,seafloorIndex);

        xnext.zeta := sin(outTheta)/cHere; //zeta
        TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(seafloor,thisZb,layers);
        for fi in solveFreqInds do
        begin
            rCoeff := TMultiLayerReflectionCoefficient.calcR(f[fi], theta1, layers);
            tmp := reflectionAmplitudePhaseReal[fi];
            reflectionAmplitudePhaseReal[fi] := reflectionAmplitudePhaseReal[fi] * rCoeff.r - reflectionAmplitudePhaseImag[fi] * rCoeff.i;
            reflectionAmplitudePhaseImag[fi] := tmp                              * rCoeff.i + reflectionAmplitudePhaseImag[fi] * rCoeff.r;
        end;
    end
    else
    begin
        inSeafloor := false;
        result := false
    end;
end;

class procedure TRay3Work.calculateRayParameters(const solveFreqInds: TArray<integer>;
                                                 const c, f : TDoubleDynArray;
                                                 var xprev, xnext : TRayIterator2;
                                                 const nf : integer;
                                                 const delTheta, thisZb, c0, cosTheta0, waterTemp : double;
                                                 var rayPhase, rayWidth, Abeam : TDoubleDynArray;
                                                 const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag : TDoubleDynArray;
                                                 const bPastRayWidthXOver : TBooleanDynArray;
                                                 var delay : double;
                                                 const calculateVolumeAttenuationAtEachStep : boolean);
var
    fi, iz1, iz2 : integer;
    stepDist : double;
    cHere : double;
    localAttenuation : single;
begin
    iz1 := min(length(c) - 1,max(0,saferound(xnext.z)));
    iz2 := min(length(c) - 1,max(0,saferound(xprev.z)));
    cHere := (c[iz1] + c[iz2])/2; //average c for this step
    stepDist := hypot(xnext.r - xprev.r, xnext.z - xprev.z);
    delay := delay + stepDist / cHere; //keep track of time delay, for arrivals calc

    if calculateVolumeAttenuationAtEachStep then
        for fi in solveFreqInds do
        begin
            localAttenuation := TWaterProps.seawaterAbsorptionCoefficient(waterTemp,f[fi],(xnext.z + xprev.z)/2);
            localAttenuation := undB20(-localAttenuation*stepDist);
            reflectionAmplitudePhaseReal[fi] := reflectionAmplitudePhaseReal[fi]*localAttenuation;
            reflectionAmplitudePhaseImag[fi] := reflectionAmplitudePhaseImag[fi]*localAttenuation;
        end;

    for fi in solveFreqInds do
    begin
        rayPhase[fi] := rayPhase[fi] + (2 * pi * f[fi])/cHere*stepDist;

        if sign(xnext.q) <> sign(xprev.q) then
            rayPhase[fi] := rayPhase[fi] + pi;  //phase change at caustics

        rayWidth[fi] := abs(xnext.q*delTheta); //eq 3.74

        if not bPastRayWidthXOver[fi] then
            if rayWidth[fi] > pi*cHere/f[fi] then
                bPastRayWidthXOver[fi] := true;

        if bPastRayWidthXOver[fi] then
            rayWidth[fi] := max(pi*cHere/f[fi],rayWidth[fi]);

        Abeam[fi] := 1/power(2*pi,0.25)*sqrt(delTheta/xnext.r*cHere/c0*2*cosTheta0/rayWidth[fi]);   //eq 3.76
        rayWidth[fi] := min(thisZb,rayWidth[fi]); //reduce ray width when the channel is shallow. but keep intensity as it would have been
    end;
end;

class procedure TRay3Work.updateFieldOrArrivalsFromRay(const solveFreqInds: TArray<integer>;
        var xprev, xnext: TRayIterator2;
        const deltar, deltaz, delay: double;
        const rayWidth, rayPhase, Abeam: TDoubleDynArray;
        const nf, nz, nr: integer;
        const outputArrivals: boolean;
        const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TDoubleDynArray;
        const arrivals: TArrivalsField;
        const pField: TComplexField);
var
    fi, ir, iz, irStart, irEnd : integer;
    startR, endR, thisR, distToRay, magnitude : double;
begin
    startR := xprev.r;
    endR := xnext.r;
    //get the approx start and end range indices (much faster than looping over all range indices). could actually calculate the real indices to go to here
    irStart := max(0,safefloor(startR / deltaR) - 1);
    irEnd := min(nr - 1,safeceil(endR / deltaR) + 1);
    for ir := irStart to irEnd do
    begin
        thisR := ir * deltaR;
        if (thisR < startR) or (thisR >= endR) then continue;

        for fi in solveFreqInds do
        begin
            {check all the z points that are within the range of the raywidth}
            for iz := max(0     ,safefloor((min(xprev.z,xnext.z) - 2*rayWidth[fi])/deltaz)) to
                      min(nz - 1,safeceil ((max(xprev.z,xnext.z) + 2*rayWidth[fi])/deltaz)) do
            begin
                //todo called multiple times due to freq loop, should clean up
                distToRay := TSolversGeneralFunctions.distPointToLineSegment(thisR, iz*deltaZ, startR, xprev.z, endR, xnext.z); //takes account of line segment endpoints

                if (distToRay > 2*rayWidth[fi]) then continue; //2*waywidth gets us to 2 StdDev, which is around -30 dB

                magnitude := exp(-sqr(distToRay/rayWidth[fi])); //gaussian -- we can speed this line up //eq 3.75

                if outputArrivals then
                    {add this arrival to the arrivals list for this point}
                    arrivals.addArrival(fi, ir, 0, iz,
                                        reflectionAmplitudePhaseReal[fi]*Abeam[fi]*magnitude*cos(rayPhase[fi]),
                                        delay)
                else
                    {add the contribution from this ray at this point to the output field} //eq 3.72
                    pField[fi,iz,ir] := pField[fi,iz,ir] + ComplexEXP(makeComplex(0,rayPhase[fi])) *
                                                                        makecomplex(reflectionAmplitudePhaseReal[fi],reflectionAmplitudePhaseImag[fi]) *
                                                                        (Abeam[fi]*magnitude); //includes accumulated reflection amplitude/phase
            end; //iz
        end; //fi
    end; //ir
end;


class procedure TRay3WorkOutsideLoop.initProblem(const rmax, zmax, dr, initialStepsize: double;
                                                 const aNTheta, nz: integer;
                                                 const f: TDoubleDynArray;
                                                 const aZProfile2: TRangeValArray;
                                                 const aCProfile2: TDepthValArray;
                                                 out c0, rho0, ds, deltar, deltaz: double;
                                                 out nr, ntheta, nf: integer;
                                                 out rayPhase, rayWidth, Abeam, reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TMatrix;
                                                 out bPastRayWidthXOver: TBoolMatrix);
const
    {$IFDEF DEBUG}
    defaultNTheta = 100;
    {$ELSE}
    defaultNTheta = 1000;
    {$ENDIF}
begin
    {water properties}
    c0 := aCProfile2.average;
    rho0 := TMaterial.water.rho_g;  //water density = 1 g/cm3

    nr := max(1,saferound(rmax/dr));
    ntheta := ifthen(aNTheta = 0, defaultNTheta,aNTheta);

    ds := initialStepsize;

    //setting all needed multi-freq arrays
    nf := length(f);
    setlength(rayPhase,nTheta,nf);
    setlength(rayWidth,nTheta,nf);
    setlength(Abeam,nTheta,nf);
    setlength(bPastRayWidthXOver,nTheta,nf);
    setlength(reflectionAmplitudePhaseReal,nTheta,nf);
    setlength(reflectionAmplitudePhaseImag,nTheta,nf);

    deltaz := zmax / max(1, (nz - 1));
    deltar := dr;//rmax / (nr - 1);
    if deltar = 0 then
        deltar := 1;

//    {ranges of seafloor depths}
//    bsteps := length(aZProfile2);
//    setlength(r_zb,bsteps);
//    setlength(zb,bsteps);
//    for i := 0 to bsteps - 1 do
//    begin
//        r_zb[i] := aZProfile2[i].range;
//        zb[i]   := aZProfile2[i].val;
//    end;
end;

class procedure TRay3Work.initRay(const solveFreqInds: TArray<integer>;
    const iTheta, nf: integer;
    const srcZ, c0: double;
    out ray: TRayIterator2;
    const theta: TDoubleDynArray;
    const aDirectivity: TSingleMatrix;
    const rayPhase: TDoubleDynArray;
    out delay, cosTheta0: double;
    out nReflections: integer;
    out inSeafloor, inAir: boolean;
    const reflectionAmplitudePhaseReal, reflectionAmplitudePhaseImag: TDoubleDynArray;
    const bPastRayWidthXOver: TBooleanDynArray);
var
    fi: integer;
begin
    for fi in solveFreqInds do
    begin
        reflectionAmplitudePhaseReal[fi] := undB20(-aDirectivity[fi,itheta]); //amplitude from the directivity: NB -ve
        reflectionAmplitudePhaseImag[fi] := 0;
        rayPhase[fi] := 0;
        bPastRayWidthXOver[fi] := false;
    end;
    delay := 0; //keeping track of time taken, for arrivals calc

    {ray initial conditions: r,z,zeta,q,p //xi only changes at reflections so we don't integrate it}
    cosTheta0 := cos(theta[itheta]);
    ray.r := 0.0;
    ray.z := srcZ;
    ray.zeta := sin(theta[itheta]) / c0;    //eq 3.28
    ray.xi := cosTheta0/c0;     //eq 3.27 //NB doesn't integrate if dc/dr = 0
    ray.q := 0;
    ray.p := 1/c0;

    inSeafloor := false;
    inAir := false;
    nReflections := 0;
end;


class procedure TRay3Work.initRays(const solveFreqInds: TArray<integer>;
                                   const nTheta, nf : integer;
                                  const srcZ, c0 : double;
                                  out rays, lastStepRays : TRayIterator2Array;
                                  const theta : TDoubleDynArray;
                                  const aDirectivity : TSingleMatrix;
                                  out rayPhase : TMatrix;
                                  out delay, xi, cosTheta0, thisZb : TDoubleDynArray;
                                  out nReflections : TLongIntDynArray;
                                  out inSeafloor, inAir : TBooleanDynArray;
                                  out reflectionAmplitudePhaseReal,reflectionAmplitudePhaseImag : TMatrix;
                                  out bPastRayWidthXOver : TBoolMatrix);
var
    itheta,fi : integer;
begin
    setlength(rays,nTheta);
    setlength(lastStepRays,nTheta);
    setlength(rayPhase,nTheta,nf);
    setlength(delay,nTheta);
    setlength(xi,nTheta);
    setlength(cosTheta0,nTheta);
    setlength(thisZb,nTheta);
    setlength(nReflections,nTheta);
    setlength(inSeafloor,nTheta);
    setlength(inAir,nTheta);
    setlength(reflectionAmplitudePhaseReal,nTheta,nf);
    setlength(reflectionAmplitudePhaseImag,nTheta,nf);
    setlength(bPastRayWidthXOver,nTheta,nf);

    for itheta := 0 to nTheta - 1 do
    begin
        for fi in solveFreqInds do
        begin
            reflectionAmplitudePhaseReal[itheta,fi] := undB20(-aDirectivity[fi,itheta]); //amplitude from the directivity: NB -ve
            reflectionAmplitudePhaseImag[itheta,fi] := 0;
            rayPhase[itheta,fi] := 0;
            bPastRayWidthXOver[itheta,fi] := false;
        end;
        delay[itheta] := 0; //keeping track of time taken, for arrivals calc

        {ray initial conditions: r,z,zeta,q,p //xi only changes at reflections so we don't integrate it}
        cosTheta0[itheta] := cos(theta[itheta]);
        xi[itheta] := cosTheta0[itheta]/c0;     //eq 3.27 //NB doesn't integrate if dc/dr = 0
        rays[itheta].r := 0.0;
        rays[itheta].z := srcZ;
        rays[itheta].zeta := sin(theta[itheta]) / c0;    //eq 3.28
        rays[itheta].q := 0;
        rays[itheta].p := 1/c0;
        rays[itheta].xi := xi[itheta]; //eq 3.27
        rays[itheta].terminated := 0;

        inSeafloor[itheta] := false;
        inAir[itheta] := false;
        nReflections[itheta] := 0;
    end;
end;



end.
