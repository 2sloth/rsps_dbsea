unit StatefulBiquadFilter;

interface

uses
    system.sysutils, system.classes, system.math, system.types,
    fmx.dialogs, fmx.stdctrls, fmx.forms,
    smartPointer, baseTypes, utextendedx87, helperFunctions,
    DSP, FilterDef;

type
    TStatefulBiquadFilter = class(TObject)
    private
        prevX, prevRes: array[0..2] of TSingleDynArray;
        prevXd, prevResd: array[0..2] of TSingleDynArray;
        def: ISmart<TFilterDef>;
    public
        constructor create(const aDef: TFilterDef);

        function  applyFilter3Times(const x: TSingleDynArray): TSingleDynArray; overload;
        function  applyFilter3Times(const x: TDoubleDynArray): TDoubleDynArray; overload;
        function  applyFilter(const x: TSingleDynArray; const ind: integer): TSingleDynArray; overload;
        function  applyFilter(const x: TDoubleDynArray; const ind: integer): TDoubleDynArray; overload;
        procedure applyFilterInPlace(const x: TDoubleDynArray; const ind: integer; const result: TDoubleDynArray);
        //procedure applyFilterInPlaceDouble(const x: TDoubleDynArray);
    end;

implementation

{ TStatefulBiquadFilter }

function TStatefulBiquadFilter.applyFilter(const x: TSingleDynArray; const ind: integer): TSingleDynArray;
var
    n,m,j: integer;
begin
    setlength(result,length(x));

    //start of output
    for n := 0 to min(length(x),def.taps) - 1 do
    begin
        result[n] := def.b[0]*x[n];
        for m := 1 to length(def.b) - 1 do
        begin
            j := n - m;
            if j >= 0 then
                result[n] := result[n] + def.b[m]*x[j]
            else
                result[n] := result[n] + def.b[m]*prevX[ind, length(prevX[ind]) + j];

            if j >= 0 then
                result[n] := result[n] - def.a[m]*result[j]
            else
                result[n] := result[n] - def.a[m]*prevX[ind, length(prevX[ind]) + j];
        end;
        result[n] := result[n] / def.a[0];
    end;

    //main part of output
    for n := def.taps to length(x) - 1 do
    begin
        result[n] := def.b[0]*x[n];
        for m := 1 to length(def.b) - 1 do
            result[n] := result[n] + def.b[m]*x[n - m];
        for m := 1 to length(def.a) - 1 do
            result[n] := result[n] - def.a[m]*result[n - m];
        result[n] := result[n] / def.a[0];
    end;

    for n := 0 to length(prevX[ind]) - 1 do
        prevX[ind,n] := x[length(x) - length(prevX[ind]) + n];
end;

function TStatefulBiquadFilter.applyFilter(const x: TDoubleDynArray; const ind: integer): TDoubleDynArray;
var
    n,m,j,k: integer;
    p: pDouble;
begin
    setlength(result,length(x));

    //start of output
    p := @(result[0]);
    for n := 0 to min(length(x),def.taps) - 1 do
    begin
        p^ := def.b[0]*x[n];
        for m := 1 to length(def.b) - 1 do
        begin
            j := n - m;
            if j >= 0 then
                p^ := p^ + def.b[m]*x[j]
            else
                p^ := p^ + def.b[m]*prevXd[ind, length(prevXd[ind]) + j];

            if j >= 0 then
                p^ := p^ - def.a[m]*result[j]
            else
                p^ := p^ - def.a[m]*prevResd[ind, length(prevResd[ind]) + j];
        end;
        p^ := p^ / def.a[0];
        inc(p);
    end;

    //main part of output
    for n := def.taps to length(x) - 1 do
    begin
        p^ := def.b[0]*x[n];
        for m := 1 to length(def.b) - 1 do
            p^ := p^ + def.b[m]*x[n - m];
        for m := 1 to length(def.a) - 1 do
            p^ := p^ - def.a[m]*result[n - m];
        p^ := p^ / def.a[0];
        inc(p);
    end;

    for n := 0 to length(prevXd[ind]) - 1 do
    begin
        k := length(result) - length(prevXd[ind]) + n;
        prevXd[ind,n] := x[k];
        prevResd[ind,n] := result[k];
    end;
end;

procedure TStatefulBiquadFilter.applyFilterInPlace(const x: TDoubleDynArray; const ind: integer; const result: TDoubleDynArray);
var
    n,m,j,k,hix: integer;
    p: pDouble;
begin
    //start of output
    p := @(result[0]);
    for n := 0 to min(length(x),def.taps) - 1 do
    begin
        p^ := def.b[0]*x[n];
        for m := 1 to def.hiB do
        begin
            j := n - m;
            if j >= 0 then
                p^ := p^ + def.b[m]*x[j]
            else
                p^ := p^ + def.b[m]*prevXd[ind, length(prevXd[ind]) + j];

            if j >= 0 then
                p^ := p^ - def.a[m]*result[j]
            else
                p^ := p^ - def.a[m]*prevResd[ind, length(prevResd[ind]) + j];
        end;
        p^ := p^ * def.oneOverA0;// / def.a[0];
        inc(p);
    end;

    //main part of output
    hix := length(x) - 1;
    for n := def.taps to hix do
    begin
        p^ := def.b[0]*x[n];
        for m := 1 to def.hiB do
        begin
            //NB only ok if a taps == b taps
            j := n - m;
            p^ := p^ + def.b[m]*x[j] - def.a[m]*result[j];
        end;
        //for m := 1 to def.hiA do
        //    p^ := p^ - def.a[m]*result[n - m];
        p^ := p^ * def.oneOverA0;// / def.a[0];
        inc(p);
    end;

    for n := 0 to length(prevXd[ind]) - 1 do
    begin
        k := length(result) - length(prevXd[ind]) + n;
        prevXd[ind,n] := x[k];
        prevResd[ind,n] := result[k];
    end;
end;

function TStatefulBiquadFilter.applyFilter3Times(const x: TSingleDynArray): TSingleDynArray;
var
    tmp: TSingleDynArray;
begin
    tmp := applyFilter(x,0);
    tmp := applyFilter(tmp,1);
    result := applyFilter(tmp,2);
end;

function TStatefulBiquadFilter.applyFilter3Times(const x: TDoubleDynArray): TDoubleDynArray;
var
    tmp: TDoubleDynArray;
begin
    tmp := applyFilter(x,0);
    tmp := applyFilter(tmp,1);
    result := applyFilter(tmp,2);
end;

constructor TStatefulBiquadFilter.create(const aDef: TFilterDef);
var
    i: integer;
begin
    inherited create;

    self.def := TSmart<TFilterDef>.create(aDef);
    for i := 0 to 2 do
    begin
        setlength(prevX[i], def.taps);
        setlength(prevRes[i], def.taps);
        setlength(prevXd[i], def.taps);
        setlength(prevResd[i], def.taps);
    end;
end;

end.
