unit animat;

interface

uses
    system.classes, system.math,
    vcl.Graphics, vcl.forms, vcl.extctrls, vcl.dialogs,
    generics.collections, generics.Defaults,
    system.sysutils, system.types,
    basetypes, helperFunctions,
    dbseaconstants,
    taskbarprogress,
    soundSource, highorder;

type
    IAnimatDelegate = interface(IInterface)
      function  getStepTime: double;
    end;

    TAnimat = class(TObject)
    private
    const
        desiredseparation : double = 5.0;
        neighbordist : double = 50.0;
    var
        delegate : IAnimatDelegate;

        function  seek(const target : TFPoint3) : TFPoint3;
        function  soundGradient(const soundForce : double; const gradientAtPos : TFPoint3) : TFPoint3;
        //procedure applyForce(const force : TMyFPoint; var accel : TMyFPoint);
        procedure limitVerticalSpeed(var p : TFPoint3);
        function  simpleFleeing(const stepTime : double;
                                const depthAtPos : double;
                                const levelAtPos : double;
                                const keepPosHistory : boolean;
                                const bAnimatsReactToSoundLevels : boolean;
                                const sources: TSourceList): boolean;
    public
    const
        maxSpeed : double = 1.5;
        maxForce : double = 1.0;
    var
        dpos : TFPoint3;
        pos : TFPoint3;
        removeFromCalc : boolean;
        thresholdLevel : single;
        levelHistory : TList<single>;
        posHistory : TList<TFPoint3>;

        constructor Create(const aDelegate : IAnimatDelegate);
        destructor Destroy; override;

        function  lastLev : single;
        function  SEL : single;
        function  peakLevel : single;
    end;
    TAnimatList = TObjectList<TAnimat>;

    TAnimatHelper = class helper for TAnimat
    private
        function  separate(const animats : TAnimatList; const distances : TDoubleDynArray) : TFPoint3;
        function  align(const animats : TAnimatList; const distances : TDoubleDynArray) : TFPoint3;
        function  cohesion(const animats : TAnimatList; const distances : TDoubleDynArray) : TFPoint3;
    public
        function  runFlocking(const stepTime : double;
                             const animats : TAnimatList;
                             const distances : TDoubleDynArray;
                             const separationForce,alignmentForce,cohesionForce,soundForce : double;
                             const levelAtPos : double;
                             const gradientAtPos : TFPoint3;
                             const depthAtPos : double;
                             const keepPosHistory : boolean;
                             const bAnimatsReactToSoundLevels : boolean;
                             const bSimpleFleeing: boolean;
                             const sources: TSourceList) : boolean;
    end;

implementation

constructor TAnimat.Create(const aDelegate : IAnimatDelegate);
begin
    self.delegate := aDelegate;

    thresholdLevel := 0;
    removeFromCalc := true;

    levelHistory := TList<single>.create;
    posHistory := TList<TFPoint3>.create;

    pos.setZero;
    dpos.setZero;
end;

destructor TAnimat.Destroy;
begin
    levelHistory.clear;
    levelHistory.Free;
    posHistory.Clear;
    posHistory.Free;

    inherited;
end;

      // Separation
  // Method checks for nearby particles and steers away
function TAnimatHelper.separate(const animats : TAnimatList; const distances : TDoubleDynArray) : TFPoint3;
var
    steer,diff : TFPoint3;
    count, i : integer;
    d : double;
    other : TAnimat;
begin
    steer.setZero;
    count := 0;
    // For every animat in the system, check if it's too close
    for i := 0 to animats.Count - 1 do
    begin
        d := distances[i];
        // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
        if (not isnan(d)) and ((d > 0) and (d < desiredseparation)) then
        begin
            other := animats[i];
            // Calculate vector pointing away from neighbor
            diff := pos.subPoint(other.pos);
            diff.normalise;
            diff.divBy(d);        // Weight by distance
            steer.add(diff);
            inc(count);            // Keep track of how many
        end;
    end;

    // Average -- divide by how many
    if (count > 0) then
        steer.divBy(count);

    // As long as the vector is greater than 0
    if (steer.mag > 0) then
    begin
        // Implement Reynolds: Steering = Desired - Velocity
        steer.normalise;
        steer.multBy(self.maxSpeed);
        steer.sub(self.dpos);
        steer.limit(maxforce);
    end;
    result := steer;
end;


    // Alignment
  // For every nearby boid in the system, calculate the average velocity
function TAnimatHelper.align(const animats : TAnimatList; const distances : TDoubleDynArray) : TFPoint3;
var
    sum : TFPoint3;
    count,i : integer;
    d : double;
    other : TAnimat;
begin
    sum.setZero;

    count := 0;
    for i := 0 to animats.Count - 1 do
    begin
        d := distances[i];
        if (not isnan(d)) and ((d > 0) and (d < neighbordist)) then
        begin
            other := animats[i];
            sum.add(other.dpos);
            inc(count);
        end;
    end;

    if (count > 0) then
    begin
        sum.divBy(count);

        // Implement Reynolds: Steering = Desired - Velocity
        sum.normalise;
        sum.multBy(maxSpeed);
        result := sum.subPoint(dpos);
        result.limit(maxforce);
    end
    else
        result.setZero;
end;

  // Cohesion
  // For the average location (i.e. center) of all nearby boids, calculate steering vector towards that location
function TAnimatHelper.cohesion(const animats : TAnimatList; const distances : TDoubleDynArray) : TFPoint3;
var
    sum : TFPoint3;
    count,i : integer;
    d : double;
    other : TAnimat;
begin
    sum.setZero;
    count := 0;

    for i := 0 to animats.Count - 1 do
    begin
        d := distances[i];
        if (not isnan(d)) and ((d > 0) and (d < neighbordist)) then
        begin
            other := animats[i];
            sum.add(other.pos); // Add location
            inc(count);
        end;
    end;

    if (count > 0) then
    begin
        sum.divBy(count);
        result := seek(sum);  // Steer towards the location
    end
    else
        result.setZero;
end;

    // A method that calculates and applies a steering force towards a target
  // STEER = DESIRED MINUS VELOCITY
function TAnimat.seek(const target : TFPoint3) : TFPoint3;
var
    desired : TFPoint3;
begin
    desired := target.subPoint(pos);  // A vector pointing from the location to the target
    // Scale to maximum speed
    desired.normalise;
    desired.multBy(maxSpeed);

    // Steering = Desired minus Velocity
    result := desired.subPoint(dpos);
    result.limit(maxforce);  // Limit to maximum steering force
end;

function TAnimat.soundGradient(const soundForce : double; const gradientAtPos : TFPoint3) : TFPoint3;
begin
    result := gradientAtPos;
    result.normalise;
    result.multBy(soundForce);
    result.reverse;
    result.limit(maxForce);
end;

procedure TAnimat.limitVerticalSpeed(var p : TFPoint3);
const
    vLimit = 0.02;
var
    v : double;
    zi : double;
begin
    if abs(p.z) < vLimit * maxSpeed then exit;

    zi := ifthen(p.z > 0, vLimit * maxSpeed, -vLimit * maxSpeed);
    v := p.mag;
    p.z := 0;//vLimit * maxSpeed;
    p.normalise;
    p.multBy(v);
    p.z := zi;
end;

function TAnimat.simpleFleeing(const stepTime : double;
                               const depthAtPos : double;
                               const levelAtPos : double;
                               const keepPosHistory : boolean;
                               const bAnimatsReactToSoundLevels : boolean;
                               const sources: TSourceList) : boolean;
    function angleAwayFromSources(const sources: TSourceList): double;
    var
        source: TSource;
        dx,dy : double;
        dist : double;
        dxSum, dySum : double;
    begin
        dxSum := 0;
        dySum := 0;
        for source in sources do
        begin
            dx := self.pos.x - source.pos.x;
            dy := self.pos.y - source.pos.y;
            dist := sqrt(sqr(dx) + sqr(dy));
            dxSum := dxSum + dx / dist;
            dySum := dySum + dy / dist;
        end;
        result := ArcTan2(dySum,dxSum);
    end;
var
    cosA, sinA: double;
begin
    if self.removeFromCalc then exit(false);

    if isnan(levelAtPos) or isinfinite(levelAtPos) or (levelAtPos < lowLev) then
        levelHistory.Add(nan)
    else
        levelHistory.Add(levelAtPos);

    //if not bAnimatsReactToSoundLevels, then just ignore the sound levels/gradient
    result := bAnimatsReactToSoundLevels and (levelAtPos > thresholdLevel);

    system.SineCosine(angleAwayFromSources(sources), sinA, cosA);
    pos.x := pos.x + maxSpeed * cosA * stepTime;
    pos.y := pos.y + maxSpeed * sinA * stepTime;

    if pos.z > depthAtPos then
        pos.z := depthAtPos;

    if isnan(depthAtPos) or pos.anyNaN then
        removeFromCalc := true;

    if keepPosHistory then
        self.posHistory.Add(pos);
end;

function TAnimatHelper.runFlocking(const stepTime : double;
                             const animats : TAnimatList;
                             const distances : TDoubleDynArray;
                             const separationForce,alignmentForce,cohesionForce,soundForce : double;
                             const levelAtPos : double;
                             const gradientAtPos : TFPoint3;
                             const depthAtPos : double;
                             const keepPosHistory : boolean;
                             const bAnimatsReactToSoundLevels : boolean;
                             const bSimpleFleeing: boolean;
                             const sources: TSourceList) : boolean;
//returns true if the animat is active and the level at the animat is higher than the threshold
var
    sep,ali,coh,gradient : TFPoint3;
    ddpos : TFPoint3;
begin
    if bSimpleFleeing then
        result := self.simpleFleeing(stepTime, depthAtPos, levelAtPos, keepPosHistory, bAnimatsReactToSoundLevels, sources)
    else
    begin
        if self.removeFromCalc then exit(false);

        ddpos.setZero;

        sep := separate(animats,distances);   // Separation
        ali := align(animats,distances);      // Alignment
        coh := cohesion(animats,distances);   // Cohesion

        // Arbitrarily weight these forces
        sep.multBy(separationForce);
        ali.multBy(alignmentForce);
        coh.multBy(cohesionForce);

        // Add the force vectors to acceleration
        ddpos.add(sep);
        ddpos.add(ali);
        ddpos.add(coh);

        if isnan(levelAtPos) or isinfinite(levelAtPos) or (levelAtPos < lowLev) then
            levelHistory.Add(nan)
        else
            levelHistory.Add(levelAtPos);

        //if not bAnimatsReactToSoundLevels, then just ignore the sound levels/gradient
        result := bAnimatsReactToSoundLevels and (levelAtPos > thresholdLevel);
        if result then
        begin
            gradient := soundGradient(soundForce,gradientAtPos);
            ddpos.add(gradient);
        end;

        if ddpos.anyNaN then
        begin
            removeFromCalc := true;
            exit;
        end;

        ddpos.limit(maxForce);

        // Update velocity
        dpos.x := dpos.x + ddpos.x * stepTime;
        dpos.y := dpos.y + ddpos.y * stepTime;
        dpos.z := dpos.z + ddpos.z * stepTime;

        limitVerticalSpeed(dpos);
        // Limit speed
        dpos.limit(maxSpeed);

        //check for above/below water
        if (pos.z + dpos.z < 0)then
        begin
            if (dpos.z < 0) then
                dpos.z := -dpos.z
            else
                dpos.z := 0;
        end;

        if (pos.z + dpos.z > depthAtPos) then
        begin
            if (dpos.z > 0) then
                dpos.z := -dpos.z
            else
                dpos.z := 0;
        end;

        pos.x := pos.x + dpos.x * stepTime;
        pos.y := pos.y + dpos.y * stepTime;
        pos.z := pos.z + dpos.z * stepTime;

        if pos.z < 0 then
            pos.z := 0;
        if pos.z > depthAtPos then
            pos.z := depthAtPos;

        if isnan(depthAtPos) or pos.anyNaN or dpos.anyNaN then
            removeFromCalc := true;

        if keepPosHistory then
            self.posHistory.Add(pos);
    end;
end;

function  TAnimat.lastLev : single;
begin
    if levelHistory.Count = 0 then exit(nan);
    result := levelHistory.Last;
end;

function  TAnimat.SEL : single;
begin
    if levelHistory.Count = 0 then exit(nan);

    result := T_.reduce<single>(levelHistory, function(memo, s: single): single
    begin
        if isnan(s) then
            result := memo
        else if isnan(memo) then
            result := unDB10(s)
        else
            result := memo + unDB10(s);
    end, 0);

    if not isnan(Result) then
        result := dB10(result * delegate.getStepTime);
end;

function TAnimat.peakLevel : single;
var
    s : single;
begin
    if levelHistory.Count = 0 then exit(nan);

    result := levelHistory[0];
    for s in levelHistory do
        greater(result,s);
end;

end.
