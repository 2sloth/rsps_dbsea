unit ImportedLocationUnit;

interface

uses system.classes, system.UITypes, system.strutils, system.SysUtils, system.math,
    vcl.dialogs,
    latlong2UTM, baseTypes, nullable;

type

    TImportedLocation = class(TObject)
        utmLongZone         : nullable<integer>;
        utmLatZone          : nullable<String>;

        latitude, longitude, dx, dy: nullable<double>;

        constructor create;
        procedure clear;

        function zoneToString(): string;
        procedure setWith(const en: TEastNorth; const dx, dy: double);
        procedure setZoneWith(const s: String);
        function hasUTMData: boolean;
        function reshapeWithImportLocation(const m: TSingleMatrix): TSingleMatrix;
    end;

var
    importedLocation: TImportedLocation;

implementation

procedure TImportedLocation.clear;
begin
    utmLongZone.null;
    utmLatZone.null;

    latitude.null;
    longitude.null;
    dx.null;
    dy.null;
end;

constructor TImportedLocation.create;
begin
    inherited;

    clear;
end;

function TImportedLocation.hasUTMData: boolean;
begin
    result := utmLongZone.HasValue and utmLatZone.HasValue;
end;

procedure TImportedLocation.setWith(const en: TEastNorth; const dx, dy: double);
begin
    self.utmLatZone := TLatLong2UTM.getLatZone(en.latitude);
    self.utmLongZone := TLatLong2UTM.getLongZone(en.longitude);

    self.latitude := en.latitude;
    self.longitude := en.longitude;
    self.dx := dx;
    self.dy := dy;
end;

function TImportedLocation.reshapeWithImportLocation(const m: TSingleMatrix): TSingleMatrix;
var
    xInd, i, j: integer;
    tmpLL, tmpUTM: TEastNorth;
    extents: TExtents;
    UTMDepths: TSingleMatrix;
    xUTMEdges: array[0..1] of double;
    xLatLngEdges: array of array[0..1] of double;
    dxThisRow, dxUTM: double;
begin
    if (length(m) = 0) or (length(m[0]) = 0) then exit;

    xUTMEdges[0] := self.longitude.val - (length(m[0]) - 1) * self.dx.val / 2;
    xUTMEdges[1] := self.longitude.val + (length(m[0]) - 1) * self.dx.val / 2;

    //refZone := TLatLong2UTM.getLongZone(self.longitude.val + length(m[0]) / 2 * self.dx.val);
    setlength(xLatLngEdges, length(m));
    UTMDepths := m.copy;
    for j := 0 to length(m) - 1 do
    begin
        tmpLL.latitude := self.latitude.val - (length(m) - 1) * self.dy.val / 2 + (length(m) - 1 - j) * self.dy.val;

        tmpLL.longitude := xUTMEdges[0];
        tmpUTM := TLatLong2UTM.latlong2UTM(tmpLL, self.utmLongZone);
        xLatLngEdges[j,0] := tmpUTM.easting;

        tmpLL.longitude := xUTMEdges[1];
        tmpUTM := TLatLong2UTM.latlong2UTM(tmpLL, self.utmLongZone);
        xLatLngEdges[j,1] := tmpUTM.easting;

        if (j = 0) or (xLatLngEdges[j,0] < extents.xmin) then
            extents.xmin := xLatLngEdges[j,0];
        if (j = 0) or (xLatLngEdges[j,1] > extents.xmax) then
            extents.xmax := xLatLngEdges[j,1];
        if (j = 0) or (tmpUTM.northing < extents.ymin) then
            extents.ymin := tmpUTM.northing;
        if (j = 0) or (tmpUTM.northing > extents.ymax) then
            extents.ymax := tmpUTM.northing;
     end;

    //resample each row, to be the same dx dy
    dxUTM := (extents.height) / (length(UTMDepths) - 1);

    setlength(result, length(UTMDepths), round( extents.width / dxUTM) + 1);
    for j := 0 to length(UTMDepths) - 1 do
    begin
        dxThisRow := (xLatLngEdges[j,1] - xLatLngEdges[j,0]) / (length(UTMDepths[0]) - 1);

        //resample
        for i := 0 to length(result[0]) - 1 do
        begin
            xInd := round( (i * dxUTM - xLatLngEdges[j,0] + extents.xmin) / dxThisRow);
            if (xInd >= 0) and (xInd < length(UTMDepths[0])) then
                result[j,i] := UTMDepths[j,xInd]
            else
                result[j,i] := nan;
        end;
    end;

    self.dx := dxUTM;
    self.dy := dxUTM;

    //ESRIData.ncols := length(result[0]);
    //ESRIData.nrows := length(result);
    //ESRIData.dx := extents.width / ESRIData.ncols;
    //ESRIData.dy := extents.height / ESRIData.nrows;
    //ESRIData.cellsize := hypot(ESRIData.dx, ESRIData.dy);
end;

procedure TImportedLocation.setZoneWith(const s: String);
begin
    if not TLatLong2UTM.isValidUTMZone(s) then
    begin
        self.utmLongZone.null;
        self.utmLatZone.null;
        exit;
    end;

    self.utmLongZone := TLatLong2UTM.getLongZone(s);
    self.utmLatZone := TLatLong2UTM.getLatZone(s);
end;

function TImportedLocation.zoneToString: string;
begin
    if self.hasUTMData then
        result := inttostr(self.utmLongZone) + UpperCase(self.utmLatZone.val)
    else
        result := '';
end;

initialization
    importedLocation := TImportedLocation.create;

finalization
    importedLocation.Free;

end.

