unit MathMethod;

interface

uses MMatrix,Mcomplex,MatrixAdd;

function Seidel(z:TComplexMatrix;x:TComplexMatrix;n:integer;e:extended;var ki:integer):TComplexMatrix;
{
������������ ����� ������-������� ��� ������� ���������
 ������� ���������
 z - �������� ������� �������������
 x - ������� ��������� ������
 n - ����� ��������
  ���
 e -  ���������� �������� �������

 ������� ���������� �������-������� ��������� �������
 � �������� ki - ����� ������������� ��������
}

function FadeevInvertMatrix(z:TComplexMatrix):TComplexMatrix;
{
������������� ����� �������� ������� � ��������� ������������� ������������������� ����������
}


function SimpleIteration(a:TComplexMatrix;b:TComplexMatrix;x0:TComplexMatrix;Ni:integer;e:extended;var ki:integer):TComplexMatrix;
{
����� ������� �������� ��� ������� ����
}

function Gaus(a:TComplexMatrix;b:TComplexMatrix):TComplexMatrix;
{
����� ������ ��� ������� ����
(����� ������������� �������)
� - ������� �������������
� - ������� ��������� ������

������� ���������� �������-������� ��������� �������
}

function Tihonov(a:TComplexMatrix;b:TComplexMatrix;Delta:extended;var ki:integer):TComplexMatrix;

implementation

uses sysutils;

function Seidel(z:TComplexMatrix;x:TComplexMatrix;n:integer;e:extended;var ki:integer):TComplexMatrix;
var i,j,l,k:integer;
    s:Tcomplex;
begin
result:=TComplexMatrix.Create(1,z.rowcount);
l:=0;
k:=0;
while l=0 do
begin
for i:=1 to z.rowcount do
 begin
 s:=x[1,i];
 for j:=1 to z.rowcount do s:=SubCOmplex(s,MulComplex(z[j,i],result[1,j]));
 s:=divcomplex(s,z[i,i]);
 result[1,i]:=addcomplex(result[1,i],s);
 if e<>0 then if abs(s.r)<e then l:=1;
 end;
inc(k);
if e=0 then if k>=n then l:=1;
end;
ki:=k;
end;

function FadeevInvertMatrix(z:TComplexMatrix):TComplexMatrix;
var p:Tcomplex;
    j,i,x:integer;
    b:TComplexMatrix;
begin
x:=z.colcount;
if x<>z.rowcount then
  begin
  raise EMathError.create('Fadeev''s Method will Use only to square matrixes.');
  exit;
  end;
result:=TComplexMatrix.Create(x,x);
b:=TComplexMatrix.Create(x,x);
b.copyFromMatrix(z);
p:=z.footstep;
for i:=1 to x do b[i,i]:=SubComplex(b[i,i],p);

for j:=2 to x-1 do
 begin
  b:=MulMatrix(z,b);                //
  p:=DivComplex(b.footstep,MakeComplex(j,0));
  for i:=1 to x do b[i,i]:=SubComplex(b[i,i],p);
end;

result.copyfrommatrix(b);
//for i:=1 to z.Colcount do result[i,i]:=SubComplex(b[i,i],DivComplex(p,MakeComplex(j+1,0)));
b:=MulMatrix(z,Result);
p:=DivComplex(b.footStep,MakeComplex(x,0));

result.DivMatrixPerComplex(p);
end;

function SimpleIteration(a,b,x0:TComplexMatrix;Ni:integer;e:extended;var ki:integer):TComplexMatrix;
  var i,n,o,p,w:integer;
      X,Em,C,X1,X2:TComplexMatrix;
      C1,C2,s:TComplex;
begin
  n:=b.RowCount;
  S:=A.Determinant;
  //fmMain.Det.Caption:=FloatToStrF(s.r,ffGeneral,8,5);
  //fmMain.Det.Width:=90;
  C:=TComplexMatrix.create(1,1);
  Em:=TComplexMatrix.create(1,1);
  Em.CopyFromMatrix(a);
  Em.Transposition;
  if Not Equality(Em,a) then
  begin
    MulMatrixEx(Em,a,a);
    MulMatrixEx(Em,b,b);
  end;
  Em.FillMatrix(1);
  SubMatrixEx(Em,a,C);
  C1:=Norma(C,1);
  C2:=Norma(C,2);
  if (C1.r>1) then
  begin
    C1.r:=C1.r+1;
    C1.i:=0;
    for o:=1 to n do
      for p:=1 to n do
        C[o,p]:=DivComplex(C[o,p],C1);
      for p:=1 to n do b[1,p]:=DivComplex(b[1,p],C1);
  end;
  Em.Destroy;
  X:=TComplexMatrix.create(1,1);
  X1:=TComplexMatrix.create(1,1);
  X2:=TComplexMatrix.create(1,1);
  X:=X0;
  w:=0;
  i:=1;
  while (w=0) do
  begin
    MulMatrixEx(C,X,X1);
    AddMatrixEx(X1,b,X);
    //fmMain.gProg.Progress:=Trunc(100*i/Ni);
    i:=i+1;
    if i>Ni then w:=1;
    if e<>0 then
    begin
      MulMatrixEx(A,X,X1);
      SubMatrixEx(X1,B,X2);
      if Norma(X2,2).r<e then w:=1;
    end;
  end;
  result:=X;
  ki:=i-1;
  X1.Destroy;
  X2.Destroy;
  C.Destroy;
end;

function Gaus(a:TComplexMatrix;b:TComplexMatrix):TComplexMatrix;
  var i,j,k,n:integer;
      C:TComplexMatrix;
      s:TComplex;
begin
  n:=b.RowCount;
  result:=TComplexMatrix.create(1,n);
  C:=TComplexMatrix.create(n+1,n);
  S:=A.Determinant;
  if S.r=0 then
  begin
    //ShowMessage('������������ ����� ����. �������� �� ����� �������.');
    exit;
  end;
  for i:=1 to n do
    for k:=1 to n do C[i,k]:=a[i,k];
  for i:=1 to n do C[n+1,i]:=b[1,i];
  for k:=1 to n do begin
    s:=C[k,k];
    for j:=k to n+1 do C[j,k]:=DivComplex(C[j,k],s);
    for i:=k+1 to n do
      begin
      s:=C[k,i];
      for j:=k to n+1 do
        C[j,i]:=SubComplex(C[j,i],MulComplex(s,C[j,k]));
      end;
  end;
  result[1,n]:=C[n+1,n];
  for i:=n-1 downto 1 do
  begin
    s.r:=0;
    s.i:=0;
    for j:=i+1 to n do s:=AddComplex(s,MulComplex(C[j,i],result[1,j]));
    result[1,i]:=SubComplex(C[n+1,i],s);
  end;
  C.Destroy;
end;

function Tihonov(a:TComplexMatrix;b:TComplexMatrix;Delta:extended;var ki:integer):TComplexMatrix;
var
    i,l,n:integer;
    X,E,a1,C,b1,X1,X2:TComplexMatrix;
    s:TComplex;
begin
  n:=b.RowCount;
  //C:=TComplexMatrix.create(1,1);
  a1:=TComplexMatrix.create(1,1);
  b1:=TComplexMatrix.create(1,1);
  X:=TComplexMatrix.create(1,1);
  X1:=TComplexMatrix.create(1,1);
  X2:=TComplexMatrix.create(1,1);
  E:=TComplexMatrix.create(n,n);
  E.FillMatrix(1);
  s.i:=0;
  for i:=1 to n do
  begin
    s.r:=Delta*cos(3.1415926*i);
    b[1,i]:=AddComplex(b[1,i],s);
  end;
  s.r:=0.001;
  s.i:=0;
  l:=0;
  C:=a;
  a.Transposition;
  MulMatrixEx(C,b,b1);
  ki:=0;
  while (l=0) do
  begin
    MulMatrixEx(C,a,X1);
    MulMatrixPerComplexEx(E,s,X2);
    AddMatrixEx(X1,X2,a1);
    X:=Gaus(a1,b1);
    MulMatrixEx(a,X,X1);
    SubMatrixEx(X1,b,X2);
    if Delta=0 then break;
    if Norma(X2,2).r<Delta*sqrt(n) then l:=1 else
    begin
      X.Destroy;
      s.r:=s.r/2;
    end;
    ki:=ki+1;
  end;
  result:=X;
  E.Destroy;
  a1.Destroy;
  b1.Destroy;
  x1.Destroy;
  x2.Destroy;
end;



end.
