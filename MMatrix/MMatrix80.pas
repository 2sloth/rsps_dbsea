{ * Matrix Made Easy                                             * }
{ * Author: Andrew N. Tuseyev                                    * }
{ * Version 0.2b                                                 * }
{ * Description: Complex matrix class and function of            * }
{ *              complex variable                                * }
{ * If you have some idea contact with me                        * }
{ * E-Mail: brain@uti.ru                                         * }
{ * All file provided "as is" without  warranties of stable work * }
{ *--------------------------------------------------------------* }
{ * If this library was useful to you send me some something by  * }
{ * mail (like postcard, postmark, map of your city and etc)     * }
{ * Thanx a lot.                                                 * }

unit MMatrix80;

interface

uses system.Classes, system.Math, system.SysUtils, complex80;

type
    TComplex = TComplex80;
    PComplex = ^TComplex;

type
    TComplexMatrix = class(TObject)
    private

        Acol: integer;
        Arow: integer;
        MatrixItem: PComplex;
        AItems: Tlist;
        ErrorLog: integer;
        ReverseCount: integer;
        ReverseBit: boolean;
        FCheckPrecision: boolean;

        function getvalues(col, row: integer): TComplex;
        function GetDeterminant: TComplex;
        procedure setvaluesComplex(col, row: integer; value: TComplex);
        procedure AddZeroItem;
        function GetCheckPrecision: boolean;
        procedure SetCheckPrecision(value: boolean);
        function GetFootStep: TComplex;
        function index(const col, row: integer): integer;
    public

        constructor create(cols, rows: integer); overload;
        constructor create(m: TComplexMatrix); overload;
        destructor Destroy; override;

        procedure clear;
        function sparsecheck(col, row: integer): boolean;

        // function  multByVect(q: TComplexVect): TComplexVect;
        procedure mySetVal(const x, y: integer; const d: double); overload;
        procedure mySetValWithDenom(const x, y: integer; const d, denom: double); overload;
        procedure myAddVal(const x, y: integer; const d: double); overload;
        procedure myAddValWithDenom(const x, y: integer; const d, denom: double); overload;
        procedure myAddValWithDenom(const x, y: integer; const z: TComplex; const denom: double); overload;
        procedure mySetVal(const x, y: integer; const z: TComplex); overload;
        procedure mySetValWithDenom(const x, y: integer; const z: TComplex; const denom: double); overload;
        procedure myAddVal(const x, y: integer; const z: TComplex); overload;
        procedure setvalues(const x, y: integer; const real, imag: double);
        property  Items[col, row: integer]: TComplex read getvalues write setvaluesComplex; default;
        property  ColCount: integer read Acol; // getcolcount;
        property  RowCount: integer read Arow; // getrowcount;
        property  Determinant: TComplex read GetDeterminant; // ������������ ������� (��� ���, ��� �������)
        property  CheckPrecision: boolean read GetCheckPrecision write SetCheckPrecision; // �������� ��������
        Property  FootStep: TComplex read GetFootStep; // ����� ������������ ���������

        { Procedures and Finction }
        procedure AddRow; // �������� ���
        procedure InsertRow(x: integer); // �������� ��� �����...
        Procedure DeleteRow(x: integer); // ������� ���
        procedure AddCOl; // ���� �� ���������
        procedure InsertCol(x: integer); // ..
        procedure DeleteCol(x: integer); // ..

        procedure Exchange(x, y, x2, y2: integer); // ������ ��������� ������� �� �����������
        procedure Transposition; // ���������������� (������ ����� ���������)
        procedure LowTriangulation; // �������� ������� ����������� ������� (�� ������)
        procedure HighTriangulation; // ........ ������ .....
        procedure InvertMatrix; // �������� �������
        procedure LUDecompose(var L, U: TComplexMatrix);
        procedure SolveMatrixL(const B: TComplexMatrix; var z: TComplexMatrix);
        procedure SolveMatrixU(const B: TComplexMatrix; var z: TComplexMatrix);
        procedure RecreateMatrix(x, y: integer); // ����������������� ������� �� ����� ������ (����������, ����������, �� ���������)

        procedure AddMatrix(m: TComplexMatrix); // ������� � �������� �
        procedure SubMatrix(m: TComplexMatrix); // ������� ������� �
        procedure MulMatrixPerComplex(z: TComplex); // �������� ������� �� ����������� �����
        procedure DivMatrixPerComplex(z: TComplex); // ������ .....

        procedure FillMatrix(mode: integer); // ��������� ������� ���������� ���� ��. ����
        procedure FillMatrixWithValue(z: TComplex);
        procedure CopyFromMatrix(z: TComplexMatrix); // ��������� ������� ( � ������������������) ���������� �� ������ Z
        procedure CopyRealFromMatrix(z: TComplexMatrix);
        procedure CopyImagineFromMatrix(z: TComplexMatrix);
        procedure FlipVertical; // ���������� �����������
        Procedure FlipHorizontal; // .... �������������
        Procedure FillMatrixFromArray(const A: array of TComplex); // ��������� ������� ���������� �� ������� �� �������
        Procedure FillMatrixFromArrayE(const A: array of double);
        // Procedure FillGridWithMatrix(Grid:TStringGrid;x,y:integer); // ����� ������ � StringGrid'�
        Function MaxModuleNonDiagonalItem(var x, y: integer): TComplex; // ����� ������������ �� ������ ������� � ��� ����������
        Procedure RotateMatrix(Angle: double; mx, my: integer);
        procedure InvertSign;
        procedure TakeMatrixPart(x, y, x1, y1: integer; var A: TComplexMatrix);
        procedure PlaceMatrixPart(A: TComplexMatrix; x, y: integer);
        procedure ExchangeCols(x1, x2: integer);
        procedure ExchangeRows(y1, y2: integer);

        function Norma: double;
        procedure SaveToStream(AStream: TStream);
        procedure SaveToFile(Name: string);
        procedure LoadFromStream(AStream: TStream);
        procedure LoadFromFile(Name: string);

        function  toString(const components: integer = 0): string; reintroduce;
    end;

    // Matrix Operation

function EqualSizeMatrix(m1, m2: TComplexMatrix): boolean;

function AddMatrix(m1, m2: TComplexMatrix): TComplexMatrix;
function SubMatrix(m1, m2: TComplexMatrix): TComplexMatrix;
function MulMatrix(m1, m2: TComplexMatrix): TComplexMatrix;
function MulMatrixPerComplex(m1: TComplexMatrix; z: TComplex): TComplexMatrix;
function DivMatrixPerComplex(m1: TComplexMatrix; z: TComplex): TComplexMatrix;

procedure AddMatrixEx(m1, m2: TComplexMatrix; var r: TComplexMatrix);
procedure SubMatrixEx(m1, m2: TComplexMatrix; var r: TComplexMatrix);
procedure MulMatrixEx(m1, m2: TComplexMatrix; var r: TComplexMatrix);
procedure MulMatrixEx2(m1, m2: TComplexMatrix; var r: TComplexMatrix);
procedure MulMatrixPerComplexEx(m1: TComplexMatrix; z: TComplex; var r: TComplexMatrix);

procedure DeterminateLW(x: TComplexMatrix; var L, W: TComplexMatrix);
procedure SolveMatrixL(L, B: TComplexMatrix; var z: TComplexMatrix);
procedure SolveMatrixW(W, B: TComplexMatrix; var z: TComplexMatrix); // ������� ������� � ������� ����������� ��������
procedure GaussInvert(A: TComplexMatrix; var B: TComplexMatrix);

implementation

const
    HeaderCode = $2552;
    VersionCode = $0101;
    LabelCode = 'MME';

type
    TComplexMatrixFileHeader = packed record
        Lbl: string[3];
        Header: Word;
        Version: Word;
        FCol, FRow: LongInt;
    end;

    // System matrix operation

function EqualSizeMatrix(m1, m2: TComplexMatrix): boolean;
begin
    result := (m1.ColCount = m2.ColCount) and (m1.RowCount = m2.RowCount);
end;

// Matrix Operation

procedure DeterminateLW(x: TComplexMatrix; var L, W: TComplexMatrix);
var
    k, j, i: integer;
    // x:tcomplex;
begin
    if not EqualSizeMatrix(x, L) then
        L.RecreateMatrix(x.ColCount, x.RowCount);
    if not EqualSizeMatrix(x, W) then
        W.RecreateMatrix(x.ColCount, x.RowCount);

    W.CopyFromMatrix(x);
    { f  acol<arow then
      begin
      errorlog:=12; //������������� ���������������� ������� � ���-��� �������� ������ ����� (� ����� � ������ ���������������)
      exit;
      end; }
    for k := 1 to x.RowCount do
    begin
        { if ComplexEqualZero(Items[k,k]) then // ���� ������� ������� =0 ������ ������
          begin
          x1:=k+1;
          while (x1<Arow) and (ComplexEqualZero(Items[k,x1])) do inc(x1);     // ���� ��������� ������� ���������
          if (x1=Arow) and (ComplexEqualZero(Items[k,x1])) then               // ���� �� ������� ��������� ����
          for i:=1 to ARow do exchange(k,i,k+1,i)
          else
          for i:=1 to ACol do exchange(i,k,i,x1);
          end; }

        L[k, k] := makeComplex80(1, 0);
        for j := k + 1 to x.RowCount do
        begin
            L[k, j] := W[k, j] / W[k, k];
            // x:=DivComplex(NegativeCOmplex(Items[k,j]),Items[k,k]);
            // W[i,j]-L[k,j]*W[i,j-1]
            for i := k to x.ColCount do
                W[i, j] := W[i, j] - (L[k, j] * W[i, k]);
            // addcomplex(mulcomplex(Items[i,k],x),Items[i,j]);
        end;

    end;
end;

procedure AddMatrixEx(m1, m2: TComplexMatrix; var r: TComplexMatrix);
var
    i, j: integer;
    // ptr : PComplex;
    x: TComplex;
begin
    if not EqualSizeMatrix(m1, m2) then
    begin
        Raise EMathError.create('Operation can''t complete because first matrix column count not equal second matrix row count.');
        Exit;
    end;

    if not EqualSizeMatrix(r, m1) then
        r.RecreateMatrix(m1.ColCount, m2.RowCount);

    for j := 1 to m1.RowCount do
        for i := 1 to m1.ColCount do
        begin
            // ptr := r[i,j];
            x := m1[i, j] + m2[i, j];
            r.setvalues(i, j, x.r, x.i);
        end;
    // r[i,j]:=AddComplex(m1[i,j],m2[i,j]);

end;

procedure SubMatrixEx(m1, m2: TComplexMatrix; var r: TComplexMatrix);
var
    i, j: integer;
begin
    if EqualSizeMatrix(m1, m2) then
    begin
        if not EqualSizeMatrix(r, m1) then
            r.RecreateMatrix(m1.ColCount, m2.RowCount);
        for j := 1 to m1.RowCount do
            for i := 1 to m1.ColCount do
                r[i, j] := m1[i, j] - m2[i, j];
    end
    else
        Raise EMathError.create('Operation can''t complete because first matrix column count not equal second matrix row count.');
end;

procedure MulMatrixEx(m1, m2: TComplexMatrix; var r: TComplexMatrix);
var
    i, j, k: integer;
begin
    if m1.ColCount = m2.RowCount then
    begin
        if not EqualSizeMatrix(r, m2) then
            r.RecreateMatrix(m2.ColCount, m1.RowCount);
        for j := 1 to r.RowCount do
            for i := 1 to r.ColCount do
                for k := 1 to m1.ColCount
                  do
                    r[i, j] := r[i, j] + (m1[k, j] * m2[i, k]);

    end
    else
        Raise EMathError.create('Operation can''t complete because first matrix column count not equal second matrix row count.');
end;

function AddMatrix(m1, m2: TComplexMatrix): TComplexMatrix;
var
    i, j: integer;
    // x : TComplex;
begin
    result := TComplexMatrix.create(m1.ColCount, m1.RowCount);
    for j := 1 to m1.RowCount do
        for i := 1 to m1.ColCount do
        begin
            // AddComplexEx(m1[i,j],m2[i,j],x);
            // result.setvalues(i,j,m1[i,j].r+m2[i,j].r,m1[i,j].i+m2[i,j].i);
            result[i, j] := m1[i, j] + m2[i, j];
        end;
end;

function SubMatrix(m1, m2: TComplexMatrix): TComplexMatrix;
var
    i, j: integer;
begin
    result := TComplexMatrix.create(m1.ColCount, m1.RowCount);
    for j := 1 to m1.RowCount do
        for i := 1 to m1.ColCount do
            result[i, j] := m1[i, j] - m2[i, j];
end;

function MulMatrix(m1, m2: TComplexMatrix): TComplexMatrix;
var
    i, j, k: integer;
    x: TComplex;
begin
    if not m1.ColCount = m2.RowCount then
    begin
        Raise EMathError.create('Operation can''t complete because first matrix column count not equal second matrix row count.');
        Exit;
    end;

    result := TComplexMatrix.create(m2.ColCount, m1.RowCount);
    for j := 1 to result.RowCount do
        for i := 1 to result.ColCount do
            for k := 1 to m1.ColCount do
            begin
                if m1.sparsecheck(k, j) and m2.sparsecheck(i, k) then
                    x := m1[k, j] * m2[i, k]
                else
                    x := NullComplex80;
                result[i, j] := result[i, j] + x;
            end;
end;

procedure MulMatrixEx2(m1, m2: TComplexMatrix; var r: TComplexMatrix);
var
    i, j, k: integer;
    x: TComplex;
begin
    if not m1.ColCount = m2.RowCount then
    begin
        Raise EMathError.create('Operation can''t complete because first matrix column count not equal second matrix row count.');
        Exit;
    end;

    if not EqualSizeMatrix(r, m2) then
        r.RecreateMatrix(m2.ColCount, m1.RowCount);

    r.free;
    r := TComplexMatrix.create(m2.ColCount, m1.RowCount);
    for j := 1 to r.RowCount do
        for i := 1 to r.ColCount do
            for k := 1 to m1.ColCount do
            begin
                if m1.sparsecheck(k, j) and m2.sparsecheck(i, k) then
                    x := m1[k, j] * m2[i, k]
                else
                    x := NullComplex80;
                r[i, j] := r[i, j] + x;
            end;
end;

function MulMatrixPerComplex(m1: TComplexMatrix; z: TComplex): TComplexMatrix;
var
    i, j: integer;
begin
    result := TComplexMatrix.create(m1.ColCount, m1.RowCount);
    for j := 1 to m1.RowCount do
        for i := 1 to m1.ColCount do
            result[i, j] := m1[i, j] * z;
end;

procedure MulMatrixPerComplexEx(m1: TComplexMatrix; z: TComplex; var r: TComplexMatrix);
var
    i, j: integer;
begin
    if not EqualSizeMatrix(r, m1) then
        r.RecreateMatrix(m1.ColCount, m1.RowCount);
    for j := 1 to m1.RowCount do
        for i := 1 to m1.ColCount do
            r[i, j] := m1[i, j] * z;
end;

function DivMatrixPerComplex(m1: TComplexMatrix; z: TComplex): TComplexMatrix;
var
    i, j: integer;
begin
    if not Complex80EqualZero(z) then
    begin
        result := TComplexMatrix.create(m1.ColCount, m1.RowCount);
        for j := 1 to m1.RowCount do
            for i := 1 to m1.ColCount do
                result[i, j] := m1[i, j] / z;
    end
    else
        raise EDivByZero.create('Dividyig By Zero.');
end;

// Inside Matrix Class

procedure TComplexMatrix.AddZeroItem;
begin
    // new(MatrixItem);
    MatrixItem := Nil;
    AItems.add(MatrixItem);
end;

constructor TComplexMatrix.create(cols, rows: integer);
var
    i: integer;
begin
    Acol := cols;
    Arow := rows;
    AItems := Tlist.create;
    AItems.clear;
    AItems.capacity := Acol * Arow;
    for i := 0 to Acol * Arow - 1 do
        AddZeroItem;

    inherited create;
end;

destructor TComplexMatrix.Destroy;
var
    i: integer;
begin
    for i := 0 to AItems.count - 1 do
        FreeMem(PComplex(AItems[i]));
    AItems.clear;
    AItems.free;

    inherited Destroy;
end;

procedure TComplexMatrix.clear;
var
    i: integer;
begin
    for i := 0 to Acol * Arow - 1 do
        AddZeroItem;
end;

function TComplexMatrix.sparsecheck(col, row: integer): boolean;
begin
    result := assigned(AItems[index(col, row)]);
end;

function TComplexMatrix.getvalues(col, row: integer): TComplex;
begin
    if not((col > 0) and (col <= ColCount)) and ((row > 0) and (row <= RowCount)) then
        Exit;

    MatrixItem := AItems.Items[(row - 1) * Acol + col - 1];
    if MatrixItem = Nil then
        result := NullComplex80
    else
        result := MatrixItem^;
end;

// function TComplexMatrix.multByVect(q: TComplexVect): TComplexVect;
// var
// i,j: integer;
// begin
// setlength(result, rowcount);
// for j := 0 to rowcount - 1 do
// for i := 0 to colcount - 1 do
// result[j] := result[j] + self[i+1,j+1]*q[i];
// end;

procedure TComplexMatrix.myAddVal(const x, y: integer; const z: TComplex);
begin
    self.setvaluesComplex(x + 1, y + 1, getvalues(x + 1, y + 1) + z);
end;

procedure TComplexMatrix.myAddValWithDenom(const x, y: integer; const z: TComplex; const denom: double);
begin
    self.setvaluesComplex(x + 1, y + 1, getvalues(x + 1, y + 1) + z / denom);
end;

procedure TComplexMatrix.mySetVal(const x, y: integer; const z: TComplex);
begin
    // so I can use 0-based
    self.setvaluesComplex(x + 1, y + 1, z);
end;

procedure TComplexMatrix.mySetValWithDenom(const x, y: integer; const z: TComplex; const denom: double);
begin
    self.setvaluesComplex(x + 1, y + 1, z / denom);
end;

procedure TComplexMatrix.mySetValWithDenom(const x, y: integer; const d, denom: double);
begin
    self.setvaluesComplex(x + 1, y + 1, makeComplex80(d / denom, 0));
end;

procedure TComplexMatrix.mySetVal(const x, y: integer; const d: double);
begin
    // so I can use 0-based
    self.setvaluesComplex(x + 1, y + 1, makeComplex80(d, 0));
end;

procedure TComplexMatrix.myAddVal(const x, y: integer; const d: double);
begin
    // so I can use 0-based
    self.setvaluesComplex(x + 1, y + 1, getvalues(x + 1, y + 1) + d);
end;

procedure TComplexMatrix.myAddValWithDenom(const x, y: integer; const d, denom: double);
begin
    self.setvaluesComplex(x + 1, y + 1, getvalues(x + 1, y + 1) + d / denom);
end;

procedure TComplexMatrix.setvaluesComplex(col, row: integer; value: TComplex);
begin
    if ((col > 0) and (col <= ColCount)) and ((row > 0) and (row <= RowCount)) then
    begin
        MatrixItem := AItems.Items[(row - 1) * Acol + col - 1];
        if Complex80EqualZero(value) then
        begin
            dispose(MatrixItem);
            AItems.Items[(row - 1) * Acol + col - 1] := nil;
        end
        else
        begin
            if MatrixItem = nil then
                new(MatrixItem);
            if FCheckPrecision = true then
                if value.r < 0.00000000000000000001 then
                    value.r := 0;
            MatrixItem^ := value;
            AItems.Items[(row - 1) * Acol + col - 1] := MatrixItem;
        end;
    end;
end;

procedure TComplexMatrix.setvalues(const x, y: integer; const real, imag: double);
begin
    if ((x > 0) and (x <= ColCount)) and ((y > 0) and (y <= RowCount)) then
    begin
        MatrixItem := AItems.Items[(y - 1) * Acol + x - 1];

        if MatrixItem = nil then
            new(MatrixItem);
        // if FCheckPrecision = true then
        // if real<0.00000000000000000001 then
        // real := 0;
        MatrixItem^.r := real;
        MatrixItem^.i := imag;
        AItems.Items[(y - 1) * Acol + x - 1] := MatrixItem;
    end;
end;

procedure TComplexMatrix.AddRow;
var
    i: integer;
begin
    inc(Arow);
    AItems.capacity := Acol * Arow;
    for i := 1 to Acol do
        AddZeroItem;
end;

procedure TComplexMatrix.InsertRow(x: integer);
var
    i: integer;
begin
    if x <= ColCount then
    begin
        inc(Arow);
        AItems.capacity := Acol * Arow;
        for i := 1 to Acol do
        begin
            new(MatrixItem);
            MatrixItem := nil;
            AItems.Insert(Acol * x, MatrixItem);
        end;
    end;
end;

Procedure TComplexMatrix.DeleteRow(x: integer);
var
    i: integer;
begin
    if (x <= RowCount) and (x > 0) then
    begin
        dec(Arow);
        for i := 1 to Acol do
            AItems.delete(Acol * (x - 1));
        AItems.capacity := Acol * Arow;
    end;
end;

procedure TComplexMatrix.AddCOl;
var
    i: integer;
begin
    inc(Acol);
    AItems.capacity := Acol * Arow;
    for i := 1 to Arow - 1 do
    begin
        new(MatrixItem);
        MatrixItem := nil;
        AItems.Insert(Acol * i - 1, MatrixItem);
    end;
    AddZeroItem;
end;

procedure TComplexMatrix.InsertCol(x: integer);
Var
    i: integer;
begin
    inc(Acol);
    AItems.capacity := Acol * Arow;
    for i := 1 to Arow do
    begin
        new(MatrixItem);
        MatrixItem := nil;
        AItems.Insert(Acol * i - (Acol - x), MatrixItem);
    end;
end;

procedure TComplexMatrix.DeleteCol(x: integer);
Var
    i: integer;
begin
    dec(x);
    for i := Arow downto 1 do
        AItems.delete(Acol * i - (Acol - x));
    dec(Acol);
    AItems.capacity := Acol * Arow;
end;

procedure TComplexMatrix.Exchange(x, y, x2, y2: integer);
begin
    AItems.Exchange((Acol * (y - 1) + x) - 1, (Acol * (y2 - 1) + x2) - 1);
end;

procedure TComplexMatrix.Transposition;
var
    i, j, x, y, z: integer;

    procedure SwapInteger(var x, y: integer); assembler;
    asm
        mov  ecx, [EAX];
        xchg ecx, [EDX]
        mov  [EAX], ecx
    end;

    begin
        x := Acol;
        y := Arow;
        if (x = 1) or (y = 1) then
        begin
            SwapInteger(Acol, Arow);
            Exit;
        end;
        z := abs(x - y);
        for i := 1 to z do
            if x < y then
                AddCOl
            else if x > y then
                AddRow;
        for j := 1 to Arow do
            for i := j to Acol do
                Exchange(i, j, j, i);
        for i := 1 to z do
            if x < y then
                DeleteRow(RowCount)
            else if x > y then
                DeleteCol(ColCount);
    end;

procedure TComplexMatrix.LowTriangulation;
var
    k, j, i, x1: integer;
    x: TComplex;
begin
    if Acol < Arow then
    begin
        ErrorLog := 12; // ������������� ���������������� ������� � ���-��� �������� ������ ����� (� ����� � ������ ���������������)
        Exit;
    end;
    ReverseCount := 0; // ������� ������������
    ReverseBit := False; // ��� ������ ������������ �������� ������������ �������������
    for k := 1 to Arow - 1 do
    begin
        if Complex80EqualZero(Items[k, k]) then // ���� ������� ������� =0 ������ ������
        begin
            x1 := k + 1;
            while (x1 < Arow) and (Complex80EqualZero(Items[k, x1])) do
                inc(x1);
            if (x1 = Arow) and (Complex80EqualZero(Items[k, x1])) then
                for i := 1 to Arow do
                    Exchange(k, i, k + 1, i)
            else
                for i := 1 to Acol do
                    Exchange(i, k, i, x1);
            inc(ReverseCount);
            ReverseBit := Not ReverseBit;
        end;
        for j := k + 1 to Arow do
        begin
            x := -Items[k, j] / Items[k, k];
            for i := k to Acol do
                Items[i, j] := (Items[i, k] * x) + Items[i, j];
        end;
    end;
end;

procedure TComplexMatrix.HighTriangulation; // �� ����� �� ������������
var
    k, j, i: integer;
    x: TComplex;
begin
    {
      if  acol<>arow then
      begin
      errorlog:=11; //������� �� ����������
      exit;
      end;
    }
    for k := Arow downto 2 do
    begin
        for j := k - 1 downto 1 do
        begin
            x := -(Items[k, j]) / Items[k, k];
            for i := Acol downto 1 do
            begin
                Items[i, j] := (Items[i, k] * x) + Items[i, j];
            end;
        end;
    end;
end;

procedure TComplexMatrix.InvertMatrix;
var
    k, i, j: integer;
    s, r: TComplex;
    // useprogress:boolean;
    // counter:integer;
begin
    if Acol <> Arow then
    begin
        raise EMathError.create('Matrix Not Square.');
        Exit;
    end;

    // if ( Assigned(fInvertProgress)) then UseProgress:=true else UseProgress:=false;
    // counter:=2*Arow+(Acol div 2);

    for k := 1 to Arow do // ��������� ��������� �������
    begin
        AddCOl;
        Items[k + Arow, k] := makeComplex80(1, 0);
    end;

    for k := 1 to Arow do // ����� ��������
    begin
        s := Items[k, k];
        j := k;
        for i := k + 1 to Arow do
        begin
            r := Items[k, i];
            if abs(r.r) > abs(s.r) then
            begin
                s := r;
                j := i;
            end;
        end;
        { if useprogress then
          begin
          finvertprogress(self,counter,k+arow);
          Application.ProcessMessages;
          end; }
        if Complex80EqualZero(s) then
            break;
        if j <> k then
            for i := k to Acol do
                Exchange(i, k, i, j);
        for j := k + 1 to Acol do
            Items[j, k] := Items[j, k] / s;
        for i := k + 1 to Arow do
            for j := k + 1 to Acol do
                Items[j, i] := Items[j, i] - (Items[j, k] * Items[k, i]);
    end;
    if not Complex80EqualZero(s) then
        for j := Arow + 1 to Acol do
            for i := Arow - 1 downto 1 do
                for k := i + 1 to Arow do
                    Items[j, i] := Items[j, i] - (Items[j, k] * Items[k, i]);

    for k := 1 to (Acol div 2) do
        DeleteCol(1); // ������� �������� �������

end;

procedure TComplexMatrix.LUDecompose(var L, U: TComplexMatrix);
var
    k, j, i: integer;
    x: TComplex;
begin
    if not EqualSizeMatrix(self, L) then
        L.RecreateMatrix(self.ColCount, self.RowCount);
    if not EqualSizeMatrix(self, U) then
        U.RecreateMatrix(self.ColCount, self.RowCount);

    U.CopyFromMatrix(self);
    { f  acol<arow then
      begin
      errorlog:=12; //������������� ���������������� ������� � ���-��� �������� ������ ����� (� ����� � ������ ���������������)
      exit;
      end; }
    for k := 1 to self.RowCount do
    begin
        { if ComplexEqualZero(Items[k,k]) then // ���� ������� ������� =0 ������ ������
          begin
          x1:=k+1;
          while (x1<Arow) and (ComplexEqualZero(Items[k,x1])) do inc(x1);     // ���� ��������� ������� ���������
          if (x1=Arow) and (ComplexEqualZero(Items[k,x1])) then               // ���� �� ������� ��������� ����
          for i:=1 to ARow do exchange(k,i,k+1,i)
          else
          for i:=1 to ACol do exchange(i,k,i,x1);
          end; }

        L[k, k] := makeComplex80(1, 0);
        for j := k + 1 to self.RowCount do
        begin
            if assigned(self.AItems[index(k, j)]) then
                L[k, j] := (U[k, j] / U[k, k]);
            // self:=DivComplex(NegativeCOmplex(Items[k,j]),Items[k,k]);
            // W[i,j]-L[k,j]*W[i,j-1]
            for i := k to self.ColCount do
            begin
                if assigned(self.AItems[index(i, k)]) then
                    x := (L[k, j] * U[i, k])
                else
                    x := NullComplex80;

                U[i, j] := (U[i, j] - x);
            end;
            // addcomplex(mulcomplex(Items[i,k],self),Items[i,j]);
        end;
    end;
end;

procedure TComplexMatrix.SolveMatrixL(const B: TComplexMatrix; var z: TComplexMatrix);
var
    i, j: integer;
    x: TComplex;
begin
    z.FillMatrix(0);
    z[1, 1] := (B[1, 1] / self[1, 1]);
    for i := 2 to self.RowCount do
    begin
        for j := 1 to i - 1 do
        begin
            if assigned(self.AItems[index(j, i)]) then
                x := (z[1, j] * self[j, i]) / self[i, i]
            else
                x := NullComplex80;
            z[1, i] := (z[1, i] - x);
        end;
        z[1, i] := ((B[1, i] / self[i, i]) + z[1, i]);
    end;
end;

procedure TComplexMatrix.SolveMatrixU(const B: TComplexMatrix; var z: TComplexMatrix); // ������� ������� � ������� ����������� ��������
var
    i, j: integer;
    x: TComplex;
begin
    z.FillMatrix(0);
    z[1, z.RowCount] := (B[1, z.RowCount] / self[z.RowCount, z.RowCount]);
    for i := z.RowCount - 1 downto 1 do
    begin
        for j := z.RowCount downto i + 1 do
        begin
            if assigned(self.AItems[index(j, i)]) then
                x := ((z[1, j] * self[j, i]) / self[i, i])
            else
                x := NullComplex80;

            z[1, i] := (z[1, i] - x);
        end;
        z[1, i] := (B[1, i] / self[i, i]) + z[1, i];
    end;
end;

procedure TComplexMatrix.FillMatrixWithValue(z: TComplex);
var
    i, j: integer;
begin
    for i := 1 to RowCount do
        for j := 1 to ColCount do
            Items[j, i] := z;
end;

procedure TComplexMatrix.FillMatrix(mode: integer);
var
    i, j, x: integer;
begin
    {
      0: ������� �������
      1: ��������� �������
      2: �� 1 ����� ������� ������ ���� � ������������ �� 1
      3: �� Acol*Arow ����� ������� ������ ���� � ��������� �� 1
      4: ��������� �����
      5: ��������� ����������� �����
    }
    case mode of
        0:
            for i := 1 to Arow do
                for j := 1 to Acol do
                    Items[j, i] := NullComplex80;
        1:
            for i := 1 to Arow do
                for j := 1 to Acol do
                    if j <> i then
                        Items[j, i] := NullComplex80
                    else
                        Items[j, i] := makeComplex80(1, 0);
        2:
            begin
                x := 0;
                for i := 1 to Arow do
                    for j := 1 to Acol do
                    begin
                        inc(x);
                        Items[j, i] := makeComplex80(x, 0);
                    end;
            end;
        3:
            begin
                x := Acol * Arow + 1;
                for i := 1 to Arow do
                    for j := 1 to Acol do
                    begin
                        dec(x);
                        Items[j, i] := makeComplex80(x, 0);
                    end;
            end;
        4:
            for i := 1 to Arow do
                for j := 1 to Acol do
                    Items[j, i] := makeComplex80(Random(Acol * Arow), 0);
        5:
            for i := 1 to Arow do
                for j := 1 to Acol do
                    Items[j, i] := makeComplex80(Random(Acol * Arow), Random(Acol * Arow));
    end;
end;

function TComplexMatrix.GetDeterminant: TComplex;
var
    t: TComplexMatrix;
    i, j: integer;
begin

    t := TComplexMatrix.create(Acol, Arow);
    for j := 1 to Arow do
        for i := 1 to Acol do
            t[i, j] := Items[i, j];
    t.LowTriangulation;
    result := makeComplex80(1, 0);
    for i := 1 to Acol do
        result := (result * t[i, i]);
    if t.ReverseBit = true then
        result := -(result);
    t.free;

end;

procedure TComplexMatrix.RecreateMatrix(x, y: integer);
var
    i: integer;
begin
    for i := AItems.count - 1 downto 0 do
    begin
        MatrixItem := AItems.Items[i];
        dispose(MatrixItem);
        AItems.delete(i);
    end;

    // s := x*y - acol*arow;
    AItems.capacity := x * y;
    for i := 0 to x * y - 1 do
        AddZeroItem;

    Acol := x;
    Arow := y;
end;

procedure TComplexMatrix.CopyFromMatrix(z: TComplexMatrix);
var
    i, j: integer;
begin
    if z.RowCount * z.ColCount <> Acol * Arow then
        RecreateMatrix(z.ColCount, z.RowCount)
    else
    begin
        Acol := z.ColCount;
        Arow := z.RowCount;
    end;
    for j := 1 to Arow do
        for i := 1 to Acol do
            Items[i, j] := z[i, j];
end;

procedure TComplexMatrix.CopyRealFromMatrix(z: TComplexMatrix);
var
    i, j: integer;
begin
    if z.RowCount * z.ColCount <> Acol * Arow then
        RecreateMatrix(z.ColCount, z.RowCount)
    else
    begin
        Acol := z.ColCount;
        Arow := z.RowCount;
    end;
    for j := 1 to Arow do
        for i := 1 to Acol do
            Items[i, j] := makeComplex80(z[i, j].r, Items[i, j].i);
end;

constructor TComplexMatrix.create(m: TComplexMatrix);
var
    i: integer;
begin
    Acol := m.ColCount;
    Arow := m.RowCount;
    AItems := Tlist.create;
    AItems.clear;
    AItems.capacity := Acol * Arow;
    for i := 0 to Acol * Arow - 1 do
        AddZeroItem;

    inherited create;
end;

procedure TComplexMatrix.CopyImagineFromMatrix(z: TComplexMatrix);
var
    i, j: integer;
begin
    if z.RowCount * z.ColCount <> Acol * Arow then
        RecreateMatrix(z.ColCount, z.RowCount)
    else
    begin
        Acol := z.ColCount;
        Arow := z.RowCount;
    end;
    for j := 1 to Arow do
        for i := 1 to Acol do
            Items[i, j] := makeComplex80(Items[i, j].r, z[i, j].i);
end;

procedure TComplexMatrix.FlipVertical;
var
    j, i: integer;
begin
    for j := 1 to (Arow div 2) do
        for i := 1 to Acol do
            Exchange(i, j, i, Arow - (j - 1));
end;

Procedure TComplexMatrix.FlipHorizontal;
var
    j, i: integer;
begin
    for j := 1 to Arow do
        for i := 1 to (Acol div 2) do
            Exchange(i, j, Acol - (i - 1), j);
end;

function TComplexMatrix.GetCheckPrecision: boolean;
begin
    result := FCheckPrecision;
end;

procedure TComplexMatrix.SetCheckPrecision(value: boolean);
begin
    FCheckPrecision := value;
end;

procedure TComplexMatrix.AddMatrix(m: TComplexMatrix);
var
    i, j: integer;
begin
    if (m.ColCount = Acol) or (m.RowCount = Arow) then
    begin
        for j := 1 to Arow do
            for i := 1 to Acol do
                Items[i, j] := (Items[i, j] + m[i, j]);
    end
    else
        Raise EMathError.create('Appliyng Matrix Not Same Size');
end;

procedure TComplexMatrix.SubMatrix(m: TComplexMatrix);
var
    i, j: integer;
begin
    if (m.ColCount = Acol) or (m.RowCount = Arow) then
    begin
        for j := 1 to Arow do
            for i := 1 to Acol do
                Items[i, j] := (Items[i, j] - m[i, j]);
    end
    else
        Raise EMathError.create('Appliyng Matrix Not Same Size');
end;

procedure TComplexMatrix.MulMatrixPerComplex(z: TComplex);
var
    i, j: integer;
begin
    for j := 1 to Arow do
        for i := 1 to Acol do
            Items[i, j] := (Items[i, j] * z);
end;

procedure TComplexMatrix.DivMatrixPerComplex(z: TComplex);
var
    i, j: integer;
begin
    if not Complex80EqualZero(z) then
    begin
        for j := 1 to Arow do
            for i := 1 to Acol do
                Items[i, j] := (Items[i, j] / z);
    end
    else
        raise EDivByZero.create('Evaluting Can''t Complete Because Deviding By Zero');
end;

function TComplexMatrix.GetFootStep: TComplex; // ����� ������������ ���������
var
    i: integer;
begin
    result := NullComplex80;
    if Acol <> Arow then
        Raise EMathError.create('Matrix Not Square.')
    else
        for i := 1 to Acol do
            result := (result + Items[i, i]);
end;

function TComplexMatrix.index(const col, row: integer): integer;
begin
    result := (row - 1) * Acol + col - 1;
end;

Procedure TComplexMatrix.FillMatrixFromArray(const A: array of TComplex);
var
    i, j, x: integer;
begin
    if high(A) <> (Acol * Arow - 1) then
        Raise ERangeError.create('Comes Short elements in array.')
    else
    begin
        x := 0;
        for i := 1 to Arow do
            for j := 1 to Acol do
            begin
                Items[j, i] := A[x];
                inc(x);
            end;
    end;
end;

Procedure TComplexMatrix.FillMatrixFromArrayE(const A: array of double);
var
    i, j, x: integer;
begin
    if high(A) <> (Acol * Arow - 1) then
        Raise ERangeError.create('Comes Short elements in array.')
    else
    begin
        x := 0;
        for i := 1 to Arow do
            for j := 1 to Acol do
            begin
                Items[j, i] := makeComplex80(A[x], 0);
                inc(x);
            end;
    end;
end;

// Procedure TComplexMatrix.FillGridWithMatrix(Grid:TStringGrid;x,y:integer);
// var i,j:Integer;
// begin
// Grid.ColCount:=ACol+x;
// Grid.RowCount:=ARow+y;
// for j:=1 to RowCount do
// for i:=1 to ColCount do
// Grid.Cells[x+i-1,y+j-1]:=COmplexToStr(Items[i,j]);
// end;

Function TComplexMatrix.MaxModuleNonDiagonalItem(var x, y: integer): TComplex; // ����� ������������ �� ������ ������� � ��� ����������
var
    Temp1: double;
    i, j: integer;
begin
    Temp1 := Items[1, 1].modulus;
    for j := 1 to RowCount do
        for i := 1 to ColCount do
            if i <> j then
                if Items[i, j].modulus > Temp1 then
                begin
                    x := i;
                    y := j;
                    result := Items[i, j];
                end;
end;

Procedure TComplexMatrix.RotateMatrix(Angle: double; mx, my: integer);
Var
    c, s: TComplex;
    i: integer;
begin
    c := makeComplex80(cos(Angle), 0);
    s := makeComplex80(Sin(Angle), 0);
    for i := 1 to RowCount do
    begin
        Items[mx, i] := ((Items[mx, i] * c) - (Items[my, i] * s));
        Items[my, i] := ((Items[mx, i] * s) + (Items[my, i] * c));
    end;
    for i := 1 to ColCount do
    begin
        Items[i, mx] := ((Items[i, mx] * c) - (Items[i, my] * s));
        Items[i, my] := ((Items[i, mx] * s) + (Items[i, my] * c));
    end;
end;

procedure TComplexMatrix.InvertSign;
var
    i, j: integer;
begin
    for i := 1 to RowCount do
        for j := 1 to ColCount do
            Items[j, i] := -(Items[j, i]);
end;

procedure TComplexMatrix.TakeMatrixPart(x, y, x1, y1: integer; var A: TComplexMatrix);
var
    i, j: integer;
begin
    if (x > ColCount) or (x1 > ColCount) or (y > RowCount) or (y1 > RowCount) then
        Exit;

    if A = nil then
        A := TComplexMatrix.create(x1 - x + 1, y1 - y + 1)
    else if (A.ColCount <> (x1 - x + 1)) or (A.RowCount <> (y1 - y + 1)) then
        A.RecreateMatrix(x1 - x + 1, y1 - y + 1);

    for i := y to y1 do
        for j := x to x1 do
            A[j - x + 1, i - y + 1] := Items[j, i];
end;

function TComplexMatrix.toString(const components: integer = 0): string;
var
    i,j: integer;
begin
    result := '';
    for i := 1 to rowCount do
    begin
        for j := 1 to colCount do
        begin
            case components of
            //complex
            0: result := result + FloatToStrF(self[j,i].r, ffExponent, 6,3) + ' ' + FloatToStrF(self[j,i].i, ffExponent, 6,3) + 'i' + #09;
            //real
            1: result := result + FloatToStrF(self[j,i].r, ffExponent, 6,3) + #09;
            //image
            2: result := result + FloatToStrF(self[j,i].i, ffExponent, 6,3) + #09;
            end;
        end;
        result := result + #13#10;
    end;
end;

procedure TComplexMatrix.PlaceMatrixPart(A: TComplexMatrix; x, y: integer);
var
    i, j, x1, y1: integer;
begin
    x1 := x + A.ColCount;
    y1 := y + A.RowCount;

    if x1 > ColCount then
        x1 := ColCount;
    if y1 > ColCount then
        y1 := RowCount;

    for i := y to y1 do
        for j := x to x1 do
            Items[j, i] := A[j - x + 1, i - y + 1];

end;

function TComplexMatrix.Norma: double;
var
    i, j: integer;
begin
    result := 0;
    for i := 1 to RowCount do
        for j := 1 to ColCount do
            result := result + Items[j, i].modulus;

    result := SQRT(result);
end;

procedure TComplexMatrix.ExchangeCols(x1, x2: integer);
var
    i: integer;
begin
    for i := 1 to RowCount do
        Exchange(x1, i, x2, i);
end;

procedure TComplexMatrix.ExchangeRows(y1, y2: integer);
var
    i: integer;
begin
    for i := 1 to ColCount do
        Exchange(i, y1, i, y2);
end;

procedure SolveMatrixL(L, B: TComplexMatrix; var z: TComplexMatrix); // ������� ������� � ������ ����������� ��������
var
    i, j: integer;
    x: TComplex;
begin
    z.FillMatrix(0);
    z[1, 1] := (B[1, 1] / L[1, 1]);
    for i := 2 to L.RowCount do
    begin
        for j := 1 to i - 1 do
        begin
            if L.sparsecheck(j, i) then
                x := ((z[1, j] * L[j, i]) / L[i, i])
            else
                x := NullComplex80;
            z[1, i] := (z[1, i] - x);
            // Z[1,i]:=SubComplex(Z[1,i], DivComplex(MulComplex(Z[1,j],L[j,i]),L[i,i]) );
        end;
        z[1, i] := ((B[1, i] / L[i, i]) + z[1, i]);
    end;
end;

procedure SolveMatrixW(W, B: TComplexMatrix; var z: TComplexMatrix); // ������� ������� � ������� ����������� ��������
var
    i, j: integer;
    x: TComplex;
begin
    z.FillMatrix(0);
    z[1, z.RowCount] := (B[1, z.RowCount] / W[z.RowCount, z.RowCount]);
    for i := z.RowCount - 1 downto 1 do
    begin
        for j := z.RowCount downto i + 1 do
        begin
            if W.sparsecheck(j, i) then
                x := ((z[1, j] * W[j, i]) / W[i, i])
            else
                x := NullComplex80;

            z[1, i] := (z[1, i] - x);
            // Z[1,i]:=SubComplex(Z[1,i], DivComplex(MulComplex(Z[1,j],W[j,i]),W[i,i]) );
        end;
        z[1, i] := ((B[1, i] / W[i, i]) + z[1, i]);
    end;
end;

procedure GaussInvert(A: TComplexMatrix; var B: TComplexMatrix);
var
    i: integer;
    L, W, c, z1, z2: TComplexMatrix;

    procedure FillVectorWIthOne(var s: TComplexMatrix; i: integer);
    begin
        s.FillMatrix(0);
        s[1, i] := makeComplex80(1, 0);
    end;

begin
    if not EqualSizeMatrix(A, B) then
        B.RecreateMatrix(A.ColCount, A.RowCount);
    L := TComplexMatrix.create(A.ColCount, A.ColCount);
    W := TComplexMatrix.create(A.ColCount, A.ColCount);

    c := TComplexMatrix.create(1, A.ColCount);
    z1 := TComplexMatrix.create(1, A.ColCount);
    z2 := TComplexMatrix.create(1, A.ColCount);

    DeterminateLW(A, L, W);

    for i := 1 to A.ColCount do
    begin
        FillVectorWIthOne(c, i);
        SolveMatrixL(L, c, z1);
        SolveMatrixW(W, z1, z2);
        B.PlaceMatrixPart(z2, i, 1);
    end;

    L.free;
    W.free;
    c.free;
    z1.free;
    z2.free;
end;

procedure TComplexMatrix.SaveToStream(AStream: TStream);
var
    Hdr: TComplexMatrixFileHeader;
    i, j: integer;
    z: TComplex;
begin
    with Hdr do
    begin
        Lbl := LabelCode;
        Header := HeaderCode;
        Version := VersionCode;
        FCol := ColCount;
        FRow := RowCount;
    end;
    AStream.Write(Hdr, SizeOf(Hdr));

    for j := 1 to RowCount do
        for i := 1 to ColCount do
        begin
            z := Items[i, j];
            AStream.Write(z, SizeOf(TComplex));
        end;
end;

procedure TComplexMatrix.SaveToFile(Name: string);
var
    tmp: TFileStream;
begin
    tmp := TFileStream.create(name, fmCreate);
    try
        SaveToStream(tmp);
    finally
        tmp.free;
    end;
end;

procedure TComplexMatrix.LoadFromStream(AStream: TStream);
var
    Hdr: TComplexMatrixFileHeader;
    i, j: integer;
    z: TComplex;
begin
    AStream.Read(Hdr, SizeOf(Hdr));

    if Hdr.Lbl = LabelCode then
    begin
        if Hdr.Header = HeaderCode then
        begin
            if Hdr.Version = VersionCode then
            begin

                RecreateMatrix(Hdr.FCol, Hdr.FRow);
                FillMatrix(0);

                for j := 1 to Hdr.FRow do
                    for i := 1 to Hdr.FCol do
                    begin
                        AStream.Read(z, SizeOf(TComplex));
                        Items[i, j] := z;
                    end;

            end
            else
                raise Exception.create('Version of file not supported');
        end
        else
            raise Exception.create('Wrong header of matrix');
    end
    else
        raise Exception.create('Wrong signature of matrix');

end;

procedure TComplexMatrix.LoadFromFile(Name: string);
var
    tmp: TFileStream;
begin
    tmp := TFileStream.create(name, fmOpenRead);
    try
        LoadFromStream(tmp);
    finally
        tmp.free;
    end;
end;

end.
