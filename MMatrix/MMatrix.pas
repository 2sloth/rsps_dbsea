{* Matrix Made Easy                                             *}
{* Author: Andrew N. Tuseyev                                    *}
{* Version 0.2b                                                 *}
{* Description: Complex matrix class and function of            *}
{*              complex variable                                *}
{* If you have some idea contact with me                        *}
{* E-Mail: brain@uti.ru                                         *}
{* All file provided "as is" without  warranties of stable work *}
{*--------------------------------------------------------------*}
{* If this library was useful to you send me some something by  *}
{* mail (like postcard, postmark, map of your city and etc)     *}
{* Thanx a lot.                                                 *}

unit MMatrix;

interface

uses Classes, Math, SysUtils, Grids, mcomplex, dialogs;

type PComplex = ^Tcomplex;

type
  TComplexMatrix = class(TObject)
  private

   Acol:integer;
   Arow:integer;
   MatrixItem:PComplex;
   AItems:Tlist;
   ErrorLog:integer;
   ReverseCount:integer;
   ReverseBit:boolean;
   FCheckPrecision:Boolean;

   function  getvalues(x,y:integer):Tcomplex;
   function  GetDeterminant:Tcomplex;
   procedure setvaluesComplex(x,y:integer;value:Tcomplex);
   procedure AddZeroItem;
   function  GetCheckPrecision:Boolean;
   procedure SetCheckPrecision(value:Boolean);
   function  GetFootStep:TComplex;
    function index(const x: integer; const y: integer) : integer;
  public

   constructor create(x,y:integer); overload;
   constructor create(m: TComplexMatrix); overload;
   destructor Destroy;override;

   procedure clear;
   function  sparsecheck(x,y:integer): boolean;


   function  multByVect(q: TComplexVect): TComplexVect; overload;
   procedure multByVect(const q, result: TComplexVect); overload;
   procedure mySetVal(const x, y: integer; const d: double); overload;
   procedure mySetValWithDenom(const x, y: integer; const d,denom: double); overload;
   procedure myAddVal(const x, y: integer; const d: double); overload;
   procedure myAddValWithDenom(const x, y: integer; const d, denom: double); overload;
   procedure myAddValWithDenom(const x, y: integer; const z: TComplex; const denom: double); overload;
   procedure mySetVal(const x, y: integer; const z: TComplex); overload;
   procedure mySetValWithDenom(const x, y: integer; const z: TComplex; const denom: double); overload;
   procedure myAddVal(const x, y: integer; const z: TComplex); overload;
   procedure setvalues(const x,y : integer; const real,imag : double);
   property Items[x,y:integer]:Tcomplex read getvalues write setvaluesComplex; default;
   property ColCount:integer read acol;//getcolcount;
   property RowCount:integer read arow;//getrowcount;
   property Determinant:Tcomplex read GetDeterminant; // ������������ ������� (��� ���, ��� �������)
   property CheckPrecision:Boolean read GetCheckPrecision write SetCheckPrecision;     //�������� ��������
   Property FootStep:Tcomplex read GetFootStep; //����� ������������ ���������

   {Procedures and Finction}
   procedure AddRow;                                    // �������� ���
   procedure InsertRow(x:integer);                      // �������� ��� �����...
   Procedure DeleteRow(x:integer);                      // ������� ���
   procedure AddCOl;                                    // ���� �� ���������
   procedure InsertCol(x:integer);                      // ..
   procedure DeleteCol(x:integer);                      // ..

   procedure Exchange(x,y,x2,y2:integer);               // ������ ��������� ������� �� �����������
   procedure Transposition;                             // ���������������� (������ ����� ���������)
   procedure LowTriangulation;                          // �������� ������� ����������� ������� (�� ������)
   procedure HighTriangulation;                         // ........ ������ .....
   procedure InvertMatrix;                              // �������� �������
   procedure LUDecompose(var L,U:TComplexMatrix);
   procedure SolveMatrixL(const B : TComplexMatrix; var Z : TComplexMatrix);
   procedure SolveMatrixU(const B : TComplexMatrix; var Z : TComplexMatrix);
   procedure RecreateMatrix(x,y:integer);               // ����������������� ������� �� ����� ������ (����������, ����������, �� ���������)

   procedure AddMatrix(m:TComplexMatrix);                      // ������� � �������� �
   procedure SubMatrix(m:TComplexMatrix);                      // ������� ������� �
   procedure MulMatrixPerComplex(z:Tcomplex);           // �������� ������� �� ����������� �����
   procedure DivMatrixPerComplex(z:Tcomplex);           // ������ .....

   procedure FillMatrix(mode:integer);                  // ��������� ������� ���������� ���� ��. ����
   procedure FillMatrixWithValue(z:tcomplex);
   procedure CopyFromMatrix(z:TComplexMatrix);                 // ��������� ������� ( � ������������������) ���������� �� ������ Z
   procedure CopyRealFromMatrix(z:TComplexMatrix);
   procedure CopyImagineFromMatrix(z:TComplexMatrix);
   procedure FlipVertical;                              // ���������� �����������
   Procedure FlipHorizontal;                            // .... �������������
   Procedure FillMatrixFromArray(const A:array of Tcomplex); // ��������� ������� ���������� �� ������� �� �������
   Procedure FillMatrixFromArrayE(const A:array of double);
   Procedure FillGridWithMatrix(Grid:TStringGrid;x,y:integer); // ����� ������ � StringGrid'�
   Function  MaxModuleNonDiagonalItem(var x,y:integer):TComplex; // ����� ������������ �� ������ ������� � ��� ����������
   Procedure RotateMatrix(Angle:double;mx,my:integer);
   procedure InvertSign;
   procedure TakeMatrixPart(x,y,x1,y1:integer;var A:TComplexMatrix);
   procedure PlaceMatrixPart(A:TComplexMatrix;x,y:integer);
   procedure ExchangeCols(x1,x2:integer);
   procedure ExchangeRows(y1,y2:integer);

   function  Norma:Double;
   procedure SaveToStream(AStream:TStream);
   procedure SaveToFile(Name:string);
   procedure LoadFromStream(AStream:TStream);
   procedure LoadFromFile(Name:string);
end;

// Matrix Operation

function  EqualSizeMatrix(m1,m2:TComplexMatrix):boolean;

function  AddMatrix(m1,m2:TComplexMatrix):TComplexMatrix;
function  SubMatrix(m1,m2:TComplexMatrix):TComplexMatrix;
function  MulMatrix(m1,m2:TComplexMatrix):TComplexMatrix;
function  MulMatrixPerComplex(m1:TComplexMatrix;z:tcomplex):TComplexMatrix;
function  DivMatrixPerComplex(m1:TComplexMatrix;z:tcomplex):TComplexMatrix;

procedure AddMatrixEx(m1,m2:TComplexMatrix; var r:TComplexMatrix);
procedure SubMatrixEx(m1,m2:TComplexMatrix; var r:TComplexMatrix);
procedure MulMatrixEx(m1,m2:TComplexMatrix; var r:TComplexMatrix);
procedure MulMatrixEx2(m1,m2:TComplexMatrix; var r:TComplexMatrix);
procedure MulMatrixPerComplexEx(m1:TComplexMatrix;z:tcomplex; var r:TComplexMatrix);

procedure DeterminateLW(X:TComplexMatrix; var L,W:TComplexMatrix);
procedure SolveMatrixL(L,B:TComplexMatrix;var Z:TComplexMatrix);
procedure SolveMatrixW(W,B:TComplexMatrix;var Z:TComplexMatrix); // ������� ������� � ������� ����������� ��������
procedure GaussInvert(A:TComplexMatrix;var B:TComplexMatrix);


implementation

const HeaderCode=$2552;
      VersionCode=$0101;
      LabelCode='MME';

type TComplexMatrixFileHeader=packed record
     Lbl:string[3];
     Header:Word;
     Version:Word;
     FCol,FRow:LongInt;
end;

// System matrix operation


function EqualSizeMatrix(m1,m2:TComplexMatrix):boolean;
begin
 result := (m1.colcount=m2.colcount) and (m1.rowcount=m2.rowcount);
end;

//Matrix Operation

procedure DeterminateLW(X:TComplexMatrix; var L,W:TComplexMatrix);
var k,j,i:integer;
//    x:tcomplex;
begin
if not EqualSizeMatrix(X,L) then  L.RecreateMatrix(X.colcount,X.rowcount);
if not EqualSizeMatrix(X,W) then  W.RecreateMatrix(X.colcount,X.rowcount);

W.CopyFromMatrix(X);
{f  acol<arow then
  begin
  errorlog:=12; //������������� ���������������� ������� � ���-��� �������� ������ ����� (� ����� � ������ ���������������)
  exit;
  end;}
for k:=1 to X.RowCount do
 begin
{  if ComplexEqualZero(Items[k,k]) then // ���� ������� ������� =0 ������ ������
    begin
    x1:=k+1;
    while (x1<Arow) and (ComplexEqualZero(Items[k,x1])) do inc(x1);     // ���� ��������� ������� ���������
    if (x1=Arow) and (ComplexEqualZero(Items[k,x1])) then               // ���� �� ������� ��������� ����
      for i:=1 to ARow do exchange(k,i,k+1,i)
    else
      for i:=1 to ACol do exchange(i,k,i,x1);
    end;}

  L[k,k]:=MakeComplex(1,0);
  for j:=k+1 to X.RowCount do
   begin
     L[k,j]:=DivComplex(W[k,j],W[k,k]);
//   x:=DivComplex(NegativeCOmplex(Items[k,j]),Items[k,k]);
//   W[i,j]-L[k,j]*W[i,j-1]
   for i:=k to X.ColCount do
      W[i,j]:=SubComplex( W[i,j] , MulComplex(L[k,j],W[i,k]) );
//   addcomplex(mulcomplex(Items[i,k],x),Items[i,j]);
   end;

 end;
end;

procedure AddMatrixEx(m1,m2:TComplexMatrix; var r:TComplexMatrix);
var
    i,j:integer;
    //ptr : PComplex;
    x : TComplex;
begin
    if not EqualSizeMatrix(m1,m2) then
    begin
        Raise EMathError.Create('Operation can''t complete because first matrix column count not equal second matrix row count.');
        Exit;
    end;

    if not EqualSizeMatrix(r,m1) then
        r.RecreateMatrix(m1.colcount,m2.rowcount);

    for j:=1 to m1.Rowcount do
        for i:=1 to m1.ColCount do
         begin
            //ptr := r[i,j];
            x := AddComplex(m1[i,j],m2[i,j]);
            r.setvalues(i,j,x.r,x.i);
         end;
            //r[i,j]:=AddComplex(m1[i,j],m2[i,j]);

end;

procedure SubMatrixEx(m1,m2:TComplexMatrix; var r:TComplexMatrix);
var i,j:integer;
begin
if EqualSizeMatrix(m1,m2) then
  begin
   if not EqualSizeMatrix(r,m1) then  r.RecreateMatrix(m1.colcount,m2.rowcount);
   for j:=1 to m1.Rowcount do for i:=1 to m1.ColCount do r[i,j]:=SubComplex(m1[i,j],m2[i,j]);
  end
else Raise EMathError.Create('Operation can''t complete because first matrix column count not equal second matrix row count.');
end;

procedure MulMatrixEx(m1,m2:TComplexMatrix; var r:TComplexMatrix);
var i,j,k:integer;
begin
if m1.colcount=m2.rowcount then
  begin
   if not EqualSizeMatrix(r,m2) then r.RecreateMatrix(m2.ColCount,m1.RowCount);
   for j:=1 to R.RowCount do for i:=1 to R.Colcount do for k:=1 to m1.colcount
    do
     r[i,j]:=AddComplex( r[i,j] , MulComplex( m1[k,j],m2[i,k] ) );

  end
else Raise EMathError.Create('Operation can''t complete because first matrix column count not equal second matrix row count.');
end;

function AddMatrix(m1,m2:TComplexMatrix):TComplexMatrix;
var
    i,j:integer;
    //x : TComplex;
begin
    Result:=TComplexMatrix.Create(m1.ColCount,m1.rowCount);
    for j:=1 to m1.Rowcount do
        for i:=1 to m1.ColCount do
        begin
            //AddComplexEx(m1[i,j],m2[i,j],x);
            //result.setvalues(i,j,m1[i,j].r+m2[i,j].r,m1[i,j].i+m2[i,j].i);
            result[i,j] := AddComplex(m1[i,j],m2[i,j]);
        end;
end;

function SubMatrix(m1,m2:TComplexMatrix):TComplexMatrix;
var i,j:integer;
begin
Result:=TComplexMatrix.Create(m1.ColCount,m1.rowCount);
for j:=1 to m1.Rowcount do
 for i:=1 to m1.ColCount do
  result[i,j]:=SubComplex(m1[i,j],m2[i,j]);
end;

function MulMatrix(m1,m2:TComplexMatrix):TComplexMatrix;
var
    i,j,k:integer;
    x : TComplex;
begin
    if not m1.colcount = m2.rowcount then
    begin
        Raise EMathError.Create('Operation can''t complete because first matrix column count not equal second matrix row count.');
        Exit;
    end;

    Result:=TComplexMatrix.Create(m2.ColCount,m1.rowCount);
    for j:=1 to Result.RowCount do
        for i:=1 to Result.Colcount do
            for k:=1 to m1.colcount do
            begin
                if m1.sparsecheck(k,j) and m2.sparsecheck(i,k) then
                    x := MulComplex(m1[k,j],m2[i,k])
                else
                    x := NullComplex;
                result[i,j] := AddComplex(result[i,j],x);
            end;
end;

procedure MulMatrixEx2(m1,m2:TComplexMatrix; var r:TComplexMatrix);
var
    i,j,k:integer;
    x : TComplex;
begin
    if not m1.colcount = m2.rowcount then
    begin
        Raise EMathError.Create('Operation can''t complete because first matrix column count not equal second matrix row count.');
        Exit;
    end;

    if not EqualSizeMatrix(r,m2) then r.RecreateMatrix(m2.ColCount,m1.RowCount);

    r.free;
    r := TComplexMatrix.Create(m2.ColCount,m1.rowCount);
    for j := 1 to r.RowCount do
        for i := 1 to r.Colcount do
            for k := 1 to m1.colcount do
            begin
                if m1.sparsecheck(k,j) and m2.sparsecheck(i,k) then
                    x := MulComplex(m1[k,j],m2[i,k])
                else
                    x := NullComplex;
                r[i,j] := AddComplex(r[i,j],x);
            end;
end;


function MulMatrixPerComplex(m1:TComplexMatrix;z:tcomplex):TComplexMatrix;
var i,j:integer;
begin
Result:=TComplexMatrix.Create(m1.ColCount,m1.rowCount);
for j:=1 to m1.Rowcount do
 for i:=1 to m1.ColCount do
  result[i,j]:=MulComplex(m1[i,j],z);
end;

procedure MulMatrixPerComplexEx(m1:TComplexMatrix;z:tcomplex; var r:TComplexMatrix);
var i,j:integer;
begin
if not EqualSizeMatrix(r,m1) then r.RecreateMatrix(m1.ColCount,m1.RowCount);
for j:=1 to m1.Rowcount do
 for i:=1 to m1.ColCount do
  r[i,j]:=MulComplex(m1[i,j],z);
end;


function DivMatrixPerComplex(m1:TComplexMatrix;z:tcomplex):TComplexMatrix;
var i,j:integer;
begin
if not ComplexEqualZero(z) then
 begin
 Result:=TComplexMatrix.Create(m1.ColCount,m1.rowCount);
 for j:=1 to m1.Rowcount do for i:=1 to m1.ColCount do result[i,j]:=DivComplex(m1[i,j],z);
 end
else raise EDivByZero.Create('Dividyig By Zero.');
end;

// Inside Matrix Class

procedure TComplexMatrix.AddZeroItem;
begin
    //new(MatrixItem);
    MatrixItem := Nil;
    AItems.add(MatrixItem);
end;

constructor TComplexMatrix.create(x,y:integer);
var
    i:integer;
begin
    Acol := x;
    ARow := y;
    AItems := Tlist.create;
    AItems.clear;
    AItems.capacity := Acol*Arow;
    for i := 0 to Acol*Arow - 1 do
        AddZeroItem;

    inherited Create;
end;

destructor TComplexMatrix.Destroy;
var
    i : integer;
begin
    for i := 0 to AItems.count - 1 do
        FreeMem(PComplex(AItems[i]));
    AItems.Clear;
    AItems.free;

    inherited destroy;
end;

procedure TComplexMatrix.clear;
var
    i : integer;
begin
    for i:=0 to Acol*Arow-1 do AddZeroItem;
end;

function  TComplexMatrix.sparsecheck(x,y:integer) : boolean;
begin
    result := assigned(AItems[index(x,y)]);
end;


function TComplexMatrix.getvalues(x,y:integer):Tcomplex;
begin
    if not ((x>0) and (x<=colcount)) and ((y>0) and (y<=rowcount)) then exit;

    MatrixItem := AItems.items[(y-1)*Acol+x-1];
    if MatrixItem = Nil then
        Result := NullComplex
    else
        result := Matrixitem^;
end;

function TComplexMatrix.multByVect(q: TComplexVect): TComplexVect;
var
    i,j: integer;
begin
    setlength(result, rowcount);
    for j := 0 to rowcount - 1 do
        for i := 0 to colcount - 1 do
            result[j] := result[j] + self[i+1,j+1]*q[i];
end;

procedure TComplexMatrix.multByVect(const q, result: TComplexVect);
var
    i,j: integer;
begin
    for j := 0 to rowcount - 1 do
    begin
        result[j] := 0;
        for i := 0 to colcount - 1 do
            result[j] := result[j] + self[i+1,j+1]*q[i];
    end;
end;

procedure TComplexMatrix.myAddVal(const x, y: integer; const z: TComplex);
begin
    self.setvaluesComplex(x + 1,y + 1,getvalues(x+1,y+1) + z);
end;

procedure TComplexMatrix.myAddValWithDenom(const x, y: integer; const z: TComplex; const denom: double);
begin
    self.setvaluesComplex(x + 1,y + 1,getvalues(x+1,y+1) + z/denom);
end;

procedure TComplexMatrix.mySetVal(const x, y: integer; const z: TComplex);
begin
    //so I can use 0-based
    self.setvaluesComplex(x + 1,y + 1,z);
end;

procedure TComplexMatrix.mySetValWithDenom(const x, y: integer; const z: TComplex; const denom: double);
begin
    self.setvaluesComplex(x + 1,y + 1,z/denom);
end;

procedure TComplexMatrix.mySetValWithDenom(const x, y: integer; const d, denom: double);
begin
    self.setvaluesComplex(x + 1,y + 1,makecomplex(d/denom,0));
end;

procedure TComplexMatrix.mySetVal(const x, y: integer; const d: double);
begin
    //so I can use 0-based
    self.setvaluesComplex(x + 1,y + 1,makecomplex(d,0));
end;

procedure TComplexMatrix.myAddVal(const x, y: integer; const d: double);
begin
    //so I can use 0-based
    self.setvaluesComplex(x + 1,y + 1, getvalues(x+1,y+1) + d);
end;

procedure TComplexMatrix.myAddValWithDenom(const x, y: integer; const d, denom: double);
begin
    self.setvaluesComplex(x + 1,y + 1, getvalues(x+1,y+1) + d/denom);
end;

procedure TComplexMatrix.setvaluesComplex(x,y:integer;value:Tcomplex);
begin
if ((x>0) and (x<=colcount)) and ((y>0) and (y<=rowcount)) then
 begin
 MatrixItem:=AItems.items[(y-1)*Acol+x-1];
 if ComplexEqualZero(Value) then
   begin
   dispose(MatrixItem);
   AItems.items[(y-1)*Acol+x-1]:=nil;
   end
 else
  begin
 if MatrixItem=nil then new(MatrixItem);
  if FCheckPrecision=true then if Value.r<0.00000000000000000001 then Value.r:=0;
  Matrixitem^:=value;
  AItems.items[(y-1)*Acol+x-1]:=MatrixItem;
  end;
 end;
end;

procedure TComplexMatrix.setvalues(const x,y : integer; const real,imag : double);
begin
    if ((x>0) and (x<=colcount)) and ((y>0) and (y<=rowcount)) then
    begin
        MatrixItem:=AItems.items[(y-1)*Acol+x-1];

        if MatrixItem=nil then
            new(MatrixItem);
//        if FCheckPrecision = true then
//            if real<0.00000000000000000001 then
//                real := 0;
        Matrixitem^.r := real;
        Matrixitem^.i := imag;
        AItems.items[(y-1)*Acol+x-1] := MatrixItem;
    end;
end;


procedure TComplexMatrix.AddRow;
var i:integer;
begin
inc(Arow);
AItems.capacity:=Acol*Arow;
for i:=1 to Acol do AddZeroItem;
end;

procedure TComplexMatrix.InsertRow(x:integer);
var i:integer;
begin
if x<=ColCount then
 begin
 inc(Arow);
 AItems.capacity:=Acol*Arow;
 for i:=1 to Acol do
  begin
  new(MatrixItem);
  MatrixItem:=nil;
  AItems.Insert(Acol*x,MatrixItem);
  end;
 end;
end;

Procedure TComplexMatrix.DeleteRow(x:integer);
var i:integer;
begin
if (x<=RowCount) and (x>0) then
 begin
 dec(Arow);
 for i:=1 to Acol do AItems.delete(Acol*(x-1));
 AItems.capacity:=Acol*Arow;
 end;
end;

procedure TComplexMatrix.AddCol;
var i:integer;
begin
inc(Acol);
AItems.capacity:=Acol*Arow;
for i:=1 to Arow-1 do
 begin
 new(MatrixItem);
 MatrixItem:=nil;
 AItems.Insert(Acol*i-1,MatrixItem);
 end;
AddZeroItem;
end;

procedure TComplexMatrix.InsertCol(x:integer);
Var I:integer;
begin
inc(Acol);
AItems.capacity:=Acol*Arow;
for i:=1 to Arow do
 begin
 new(MatrixItem);
 MatrixItem:=nil;
 AItems.Insert(Acol*i-(acol-x),MatrixItem);
 end;
end;

procedure TComplexMatrix.DeleteCol(x:integer);
Var I:integer;
begin
dec(x);
for i:=Arow downto 1 do  AItems.Delete(Acol*i-(acol-x));
dec(Acol);
AItems.capacity:=Acol*Arow;
end;

procedure TComplexMatrix.Exchange(x,y,x2,y2:integer);
begin
AItems.Exchange((Acol*(y-1)+x)-1,(Acol*(y2-1)+x2)-1);
end;

procedure TComplexMatrix.Transposition;
var i,j,x,y,z:integer;

procedure SwapInteger(var x, y: Integer); assembler;
asm
  mov  ecx, [EAX];
  xchg ecx, [EDX]
  mov  [EAX], ecx
end;

begin
x:=acol;
y:=arow;
if (x=1) or (y=1) then
   begin
   SwapInteger(Acol,Arow);
   exit;
   end;
z:=abs(x-y);
for i:=1 to z do if x<y then Addcol else if x>y then AddRow;
for j:=1 to ARow do for i:=j to Acol do Exchange(i,j,j,i);
for i:=1 to z do if x<y then DeleteRow(RowCount) else if x>y then DeleteCol(ColCount);
end;

procedure TComplexMatrix.LowTriangulation;
var k,j,i,x1:integer;
    x:tcomplex;
begin
if  acol<arow then
  begin
  errorlog:=12; //������������� ���������������� ������� � ���-��� �������� ������ ����� (� ����� � ������ ���������������)
  exit;
  end;
ReverseCount:=0;  // ������� ������������
ReverseBit:=False; // ��� ������ ������������ �������� ������������ �������������
for k:=1 to Arow-1 do
 begin
   if ComplexEqualZero(Items[k,k]) then // ���� ������� ������� =0 ������ ������
    begin
    x1:=k+1;
    while (x1<Arow) and (ComplexEqualZero(Items[k,x1])) do inc(x1);
    if (x1=Arow) and (ComplexEqualZero(Items[k,x1])) then
      for i:=1 to ARow do exchange(k,i,k+1,i)
    else
    for i:=1 to ACol do exchange(i,k,i,x1);
     inc(ReverseCount);
     ReverseBit:=Not ReverseBit;
    end;
  for j:=k+1 to Arow do
   begin
   x:=DivComplex(NegativeCOmplex(Items[k,j]),Items[k,k]);
   for i:=k to Acol do Items[i,j]:=addcomplex(mulcomplex(Items[i,k],x),Items[i,j]);
   end;
 end;
end;

procedure TComplexMatrix.HighTriangulation; // �� ����� �� ������������
var k,j,i:integer;
    x:tcomplex;
begin
{
if  acol<>arow then
  begin
  errorlog:=11; //������� �� ����������
  exit;
  end;
}
for k:=Arow downto 2 do
 begin
  for j:=k-1 downto 1 do
   begin
   x:=DivComplex(NegativeCOmplex(Items[k,j]),Items[k,k]);
    for i:=Acol downto 1 do
     begin
     Items[i,j]:=addcomplex(mulcomplex(Items[i,k],x),Items[i,j]);
     end;
   end;
 end;
end;

procedure TComplexMatrix.InvertMatrix;
var k,i,j:integer;
    s,r:tcomplex;
//    useprogress:boolean;
//    counter:integer;
begin
if  acol<>arow then
  begin
  raise EMatherror.create('Matrix Not Square.');
  exit;
  end;

//if ( Assigned(fInvertProgress)) then UseProgress:=true else UseProgress:=false;
//counter:=2*Arow+(Acol div 2);

for k:=1 to Arow do // ��������� ��������� �������
 begin
 addcol;
 items[k+arow,k]:=makecomplex(1,0);
end;

for k:=1 to Arow do // ����� ��������
 begin
  s:=items[k,k];
  j:=k;
  for i:=k+1 to Arow do
   begin
   r:=items[k,i];
   if abs(r.r)>abs(s.r) then
    begin
    s:=r;
    j:=i;
    end;
   end;
{  if useprogress then
     begin
     finvertprogress(self,counter,k+arow);
     Application.ProcessMessages;
     end;}
  if ComplexEqualZero(s) then break;
  if j<>k then for i:=k to Acol do exchange(i,k,i,j);
  for j:=k+1 to ACol do items[j,k]:=divcomplex(items[j,k],s);
  for i:=k+1 to Arow do
    for j:=k+1 to acol do items[j,i]:=subcomplex(items[j,i],mulcomplex(items[j,k],items[k,i]));
end;
if not ComplexEqualZero(s) then
 for j:=Arow+1 to Acol do
  for i:=Arow-1 downto 1 do
   for k:=i+1 to arow do items[j,i]:=subcomplex(items[j,i],mulcomplex(items[j,k],items[k,i]));

for k:=1 to (Acol div 2) do
    deletecol(1); //������� �������� �������

end;

procedure TComplexMatrix.LUDecompose(var L,U:TComplexMatrix);
var k,j,i:integer;
    x:tcomplex;
begin
	if not EqualSizeMatrix(self,L) then  L.RecreateMatrix(self.colcount,self.rowcount);
	if not EqualSizeMatrix(self,U) then  U.RecreateMatrix(self.colcount,self.rowcount);

	U.CopyFromMatrix(self);
	{f  acol<arow then
	begin
	errorlog:=12; //������������� ���������������� ������� � ���-��� �������� ������ ����� (� ����� � ������ ���������������)
	exit;
	end;}
	for k:=1 to self.RowCount do
	begin
		{  if ComplexEqualZero(Items[k,k]) then // ���� ������� ������� =0 ������ ������
		begin
		x1:=k+1;
		while (x1<Arow) and (ComplexEqualZero(Items[k,x1])) do inc(x1);     // ���� ��������� ������� ���������
		if (x1=Arow) and (ComplexEqualZero(Items[k,x1])) then               // ���� �� ������� ��������� ����
		  for i:=1 to ARow do exchange(k,i,k+1,i)
		else
		  for i:=1 to ACol do exchange(i,k,i,x1);
		end;}

		L[k,k]:=MakeComplex(1,0);
		for j:=k+1 to self.RowCount do
		begin
			if assigned(self.AItems[index(k,j)]) then
				L[k,j]:=DivComplex(U[k,j],U[k,k]);
			//   self:=DivComplex(NegativeCOmplex(Items[k,j]),Items[k,k]);
			//   W[i,j]-L[k,j]*W[i,j-1]
			for i:=k to self.ColCount do
			begin
				if assigned(self.AItems[index(i,k)]) then
					x := MulComplex(L[k,j],U[i,k])
				else
					x := NullComplex;

				U[i,j]:=SubComplex( U[i,j] , x );
			end;
			//   addcomplex(mulcomplex(Items[i,k],self),Items[i,j]);
		end;
	end;
end;

procedure TComplexMatrix.SolveMatrixL(const B:TComplexMatrix;var Z:TComplexMatrix);
var
    i,j:integer;
    x : Tcomplex;
begin
    z.FillMAtrix(0);
    Z[1,1]:=DivComplex(B[1,1],self[1,1]);
    for i:=2 to self.RowCount do
    begin
        for j:=1 to i-1 do
        begin
            if assigned(self.AItems[index(j,i)]) then
                x := DivComplex(MulComplex(Z[1,j],self[j,i]),self[i,i])
            else
                x := NullComplex;
            Z[1,i]:=SubComplex(Z[1,i], x);
        end;
        Z[1,i]:=AddComplex(DivComplex(B[1,i],self[i,i]),Z[1,i]);
    end;
end;

procedure TComplexMatrix.SolveMatrixU(const B : TComplexMatrix; var Z : TComplexMatrix); // ������� ������� � ������� ����������� ��������
var
    i,j:integer;
    x : TComplex;
begin
    z.FillMAtrix(0);
    Z[1,Z.Rowcount] := DivComplex(B[1,Z.Rowcount],self[Z.Rowcount,Z.Rowcount]);
    for i:= Z.Rowcount - 1 downto 1 do
    begin
        for j:= Z.Rowcount downto i + 1 do
        begin
            if assigned(self.AItems[index(j,i)]) then
                x := DivComplex(MulComplex(Z[1,j],self[j,i]),self[i,i])
            else
                x := NullComplex;

            Z[1,i] := SubComplex(Z[1,i], x );
        end;
        Z[1,i] := AddComplex(DivComplex(B[1,i],self[i,i]),Z[1,i]);
    end;
end;

procedure TComplexMatrix.FillMatrixWithValue(z:tcomplex);
var i,j:integer;
begin
for i:=1 to rowcount do for j:=1 to colcount do Items[j,i]:=z;
end;

procedure TComplexMatrix.FillMatrix(mode:integer);
var i,j,x:integer;
begin
{
0: ������� �������
1: ��������� �������
2: �� 1 ����� ������� ������ ���� � ������������ �� 1
3: �� Acol*Arow ����� ������� ������ ���� � ��������� �� 1
4: ��������� �����
5: ��������� ����������� �����
}
case mode of
0:for i:=1 to ARow do for j:=1 to ACol do Items[j,i]:=Nullcomplex;
1:for i:=1 to ARow do for j:=1 to ACol do if j<>i then Items[j,i]:=Nullcomplex else Items[j,i]:=MakeComplex(1,0);
2:begin
  x:=0;
  for i:=1 to ARow do for j:=1 to ACol do
     begin
     inc(x);
     Items[j,i]:=makecomplex(x,0);
     end;
  end;
3:begin
  x:=Acol*Arow+1;
  for i:=1 to ARow do for j:=1 to ACol do
     begin
     dec(x);
     Items[j,i]:=makecomplex(x,0);
     end;
  end;
4:for i:=1 to ARow do for j:=1 to ACol do Items[j,i]:=MakeComplex(Random(Acol*Arow),0);
5:for i:=1 to ARow do for j:=1 to ACol do Items[j,i]:=MakeComplex(Random(Acol*Arow),Random(Acol*Arow));
end;
end;

function TComplexMatrix.GetDeterminant:Tcomplex;
var t:TComplexMatrix;
    i,j:integer;
begin

t:=TComplexMatrix.create(acol,arow);
for j:=1 to arow do for i:=1 to acol do t[i,j]:=Items[i,j];
t.lowtriangulation;
result:=makecomplex(1,0);
for i:=1 to acol do result:=mulcomplex(result,t[i,i]);
if t.ReverseBit=true then result:=NegativeComplex(result);
t.free;

end;

procedure TComplexMatrix.RecreateMatrix(x,y:integer);
var
    i : integer;
begin
    for i := AItems.count - 1 downto 0 do
    begin
        MatrixItem := AItems.items[i];
        dispose(MatrixItem);
        AItems.Delete(i);
    end;

    //s := x*y - acol*arow;
    Aitems.Capacity := x*y;
    for i := 0 to x*y - 1 do
        addzeroitem;

    acol := x;
    arow := y;
end;

procedure TComplexMatrix.CopyFromMatrix(z:TComplexMatrix);
var i,j:integer;
begin
if z.Rowcount*z.Colcount<>Acol*arow then RecreateMatrix(z.Colcount,z.Rowcount)
else
 begin
 acol:=z.colcount;
 arow:=z.rowcount;
 end;
for j:=1 to Arow do for i:=1 to acol do Items[i,j]:=z[i,j];
end;

procedure TComplexMatrix.CopyRealFromMatrix(z:TComplexMatrix);
var i,j:integer;
begin
if z.Rowcount*z.Colcount<>Acol*arow then RecreateMatrix(z.Colcount,z.Rowcount)
else
 begin
 acol:=z.colcount;
 arow:=z.rowcount;
 end;
for j:=1 to Arow do for i:=1 to acol do Items[i,j]:=Makecomplex(z[i,j].r,Items[i,j].i);
end;

constructor TComplexMatrix.create(m: TComplexMatrix);
var
    i:integer;
begin
    Acol := m.ColCount;
    ARow := m.RowCount;
    AItems := Tlist.create;
    AItems.clear;
    AItems.capacity := Acol*Arow;
    for i := 0 to Acol*Arow - 1 do
        AddZeroItem;

    inherited Create;
end;

procedure TComplexMatrix.CopyImagineFromMatrix(z:TComplexMatrix);
var i,j:integer;
begin
if z.Rowcount*z.Colcount<>Acol*arow then RecreateMatrix(z.Colcount,z.Rowcount)
else
 begin
 acol:=z.colcount;
 arow:=z.rowcount;
 end;
for j:=1 to Arow do for i:=1 to acol do Items[i,j]:=Makecomplex(Items[i,j].r,z[i,j].i);
end;



procedure TComplexMatrix.FlipVertical;
var j,i:integer;
begin
for j:=1 to (Arow div 2) do  for i:=1 to Acol do exchange(i,j,i,Arow-(j-1));
end;

Procedure TComplexMatrix.FlipHorizontal;
var j,i:integer;
begin
for j:=1 to Arow do for i:=1 to (Acol div 2) do exchange(i,j,Acol-(i-1),j);
end;

function TComplexMatrix.GetCheckPrecision:Boolean;
begin
result:=FCheckPrecision;
end;

procedure TComplexMatrix.SetCheckPrecision(value:Boolean);
begin
FCheckPrecision:=Value;
end;

procedure TComplexMatrix.AddMatrix(m:TComplexMatrix);
var i,j:integer;
begin
if (m.ColCount=Acol) or (m.rowCount=Arow) then
 begin
 for j:=1 to Arow do for i:=1 to acol do Items[i,j]:=Addcomplex(Items[i,j],m[i,j]);
 end
else
 Raise EMathError.create('Appliyng Matrix Not Same Size');
end;

procedure TComplexMatrix.SubMatrix(m:TComplexMatrix);
var i,j:integer;
begin
if (m.ColCount=Acol) or (m.rowCount=Arow) then
 begin
 for j:=1 to Arow do for i:=1 to acol do Items[i,j]:=Subcomplex(Items[i,j],m[i,j]);
 end
else
 Raise EMathError.create('Appliyng Matrix Not Same Size');
end;

procedure TComplexMatrix.MulMatrixPerComplex(z:Tcomplex);
var i,j:integer;
begin
for j:=1 to Arow do for i:=1 to acol do Items[i,j]:=Mulcomplex(Items[i,j],z);
end;

procedure TComplexMatrix.DivMatrixPerComplex(z:Tcomplex);
var i,j:integer;
begin
if not ComplexEqualZero(z) then
 begin
 for j:=1 to Arow do for i:=1 to acol do Items[i,j]:=Divcomplex(Items[i,j],z);
 end
else
  raise EDivByZero.Create('Evaluting Can''t Complete Because Deviding By Zero');
end;

function TComplexMatrix.GetFootStep:TComplex; // ����� ������������ ���������
var i:integer;
begin
result:=nullcomplex;
if acol<>arow then Raise EMathError.create('Matrix Not Square.')
else
 for i:=1 to acol do result:=AddComplex(result,Items[i,i]);
end;

function TComplexMatrix.index(const x: integer; const y: integer) : integer;
begin
    result := (y-1)*Acol+x-1;
end;


Procedure TComplexMatrix.FillMatrixFromArray(const A:array of Tcomplex);
var i,j,x:integer;
begin
if high(a)<>(acol*arow-1) then Raise ERangeError.Create('Comes Short elements in array.')
else
begin
  x:=0;
  for i:=1 to ARow do
  for j:=1 to ACol do
     begin
     Items[j,i]:=a[x];
     inc(x);
     end;
end;
end;

Procedure TComplexMatrix.FillMatrixFromArrayE(const A:array of double);
var i,j,x:integer;
begin
if high(a)<>(acol*arow-1) then Raise ERangeError.Create('Comes Short elements in array.')
else
begin
  x:=0;
  for i:=1 to ARow do
  for j:=1 to ACol do
     begin
     Items[j,i]:=MakeComplex(a[x],0);
     inc(x);
     end;
end;
end;

Procedure TComplexMatrix.FillGridWithMatrix(Grid:TStringGrid;x,y:integer);
var i,j:Integer;
begin
  Grid.ColCount:=ACol+x;
  Grid.RowCount:=ARow+y;
  for j:=1 to RowCount do
   for i:=1 to ColCount do
    Grid.Cells[x+i-1,y+j-1]:=COmplexToStr(Items[i,j]);
end;

Function TComplexMatrix.MaxModuleNonDiagonalItem(var x,y:integer):TComplex; // ����� ������������ �� ������ ������� � ��� ����������
var Temp1:double;
    i,j:integer;
begin
Temp1:= Items[1,1].modulus;
for j:=1 to RowCount do
 for i:=1 to Colcount do
  if i<>j then if Items[i,j].modulus > Temp1 then
    begin
    x:=i;
    y:=j;
    Result:=Items[i,j];
    end;
end;

Procedure TComplexMatrix.RotateMatrix(Angle:double;mx,my:integer);
Var c,s:Tcomplex;
    i:integer;
begin
c:=MakeComplex(cos(Angle),0);
s:=MakeCOmplex(Sin(Angle),0);
for i:=1 to RowCount do
  begin
  Items[mx,i]:=SubComplex( MulComplex(Items[mx,i],c) , MulCOmplex(Items[my,i],s) );
  Items[my,i]:=AddCOmplex( MulComplex(Items[mx,i],s) , MulComplex(Items[my,i],c) );
  end;
for i:=1 to ColCount do
  begin
  Items[i,mx]:=SubComplex( MulComplex(Items[i,mx],c) , MulComplex(Items[i,my],s) );
  Items[i,my]:=AddCOmplex( MulComplex(Items[i,mx],s) , MulComplex(Items[i,my],c) );
  end;
end;

procedure TComplexMatrix.InvertSign;
var i,j:integer;
begin
for i:=1 to RowCount do
 for j:=1 to ColCount do
   Items[j,i]:=NegativeComplex(Items[j,i]);
end;

procedure TComplexMatrix.TakeMatrixPart(x,y,x1,y1:integer;var A:TComplexMatrix);
var i,j:integer;
begin
if (x>Colcount) or (x1>Colcount) or (y>Rowcount) or (y1>Rowcount) then exit;

if A=nil then A:=TComplexMatrix.Create(x1-x+1,y1-y+1)
 else if (A.ColCOunt<>(x1-x+1)) or (A.RowCOunt<>(y1-y+1)) then A.RecreateMatrix(x1-x+1,y1-y+1);

for i:=y to y1 do
 for j:=x to x1 do
  A[j-x+1,i-y+1]:=Items[j,i];
end;

procedure TComplexMatrix.PlaceMatrixPart(A:TComplexMatrix;x,y:integer);
var i,j,x1,y1:integer;
begin
x1:=x+A.ColCount;
y1:=y+A.RowCount;

if x1>ColCount then x1:=ColCount;
if y1>ColCount then y1:=RowCount;

for i:=y to y1 do
 for j:=x to x1 do
  Items[j,i]:=A[j-x+1,i-y+1];

end;

function TComplexMatrix.Norma:Double;
var i,j:integer;
begin
result:=0;
for i:=1 to RowCount do
 for j:=1 to ColCount do
  result:=result + Items[j,i].modulus;

Result:=SQRT(result);
end;

procedure TComplexMatrix.ExchangeCols(x1,x2:integer);
var i:integer;
begin
for i:=1 to rowcount do Exchange(x1,i,x2,i);
end;

procedure TComplexMatrix.ExchangeRows(y1,y2:integer);
var i:integer;
begin
for i:=1 to colcount do Exchange(i,y1,i,y2);
end;


procedure SolveMatrixL(L,B:TComplexMatrix;var Z:TComplexMatrix); // ������� ������� � ������ ����������� ��������
var
    i,j:integer;
    x : TComplex;
begin
    z.FillMAtrix(0);
    Z[1,1]:=DivComplex(B[1,1],L[1,1]);
    for i:=2 to L.RowCount do
    begin
        for j:=1 to i-1 do
        begin
            if L.sparsecheck(j,i) then
                x := DivComplex(MulComplex(Z[1,j],L[j,i]),L[i,i])
            else
                x := NullComplex;
            Z[1,i]:=SubComplex(Z[1,i], x);
            //Z[1,i]:=SubComplex(Z[1,i], DivComplex(MulComplex(Z[1,j],L[j,i]),L[i,i]) );
        end;
        Z[1,i]:=AddComplex(DivComplex(B[1,i],L[i,i]),Z[1,i]);
    end;
end;

procedure SolveMatrixW(W,B:TComplexMatrix;var Z:TComplexMatrix); // ������� ������� � ������� ����������� ��������
var
    i,j:integer;
    x : TComplex;
begin
    z.FillMAtrix(0);
    Z[1,Z.Rowcount]:=DivComplex(B[1,Z.Rowcount],W[Z.Rowcount,Z.Rowcount]);
    for i:=Z.Rowcount-1 downto 1 do
    begin
        for j:=Z.Rowcount downto i+1 do
        begin
            if W.sparsecheck(j,i) then
                x := DivComplex(MulComplex(Z[1,j],W[j,i]),W[i,i])
            else
                x := NullComplex;

            Z[1,i]:=SubComplex(Z[1,i], x);
            //Z[1,i]:=SubComplex(Z[1,i], DivComplex(MulComplex(Z[1,j],W[j,i]),W[i,i]) );
        end;
        Z[1,i]:=AddComplex(DivComplex(B[1,i],W[i,i]),Z[1,i]);
    end;
end;


procedure GaussInvert(A:TComplexMatrix;var B:TComplexMatrix);
var i:integer;
    l,w,c,z1,z2:TComplexMatrix;

    procedure FillVectorWIthOne(var s:TComplexMatrix;i:integer);
    begin
    s.fillMatrix(0);
    s[1,i]:=MakeComplex(1,0);
    end;
begin
if not EqualSizeMatrix(A,B) then B.RecreateMatrix(A.colcount,A.rowcount);
l:=TComplexMatrix.Create(A.colcount,A.colcount);
w:=TComplexMatrix.Create(A.colcount,A.colcount);

c:=TComplexMatrix.Create(1,A.colcount);
z1:=TComplexMatrix.Create(1,A.colcount);
z2:=TComplexMatrix.Create(1,A.colcount);

DeterminateLW(A,l,w);

for i:=1 to A.colcount do
 begin
 FillVectorWithOne(c,i);
 SolveMatrixL(l,c,z1);
 solveMatrixW(w,z1,z2);
 b.PlaceMatrixPart(z2,i,1);
 end;

l.free;
w.free;
c.free;
z1.free;
z2.free;
end;

procedure TComplexMatrix.SaveToStream(AStream:TStream);
var Hdr:TComplexMatrixFileHeader;
    i,j:integer;
    z:TComplex;
begin
 with hdr do
  begin
  lbl:=LabelCode;
  Header:=HeaderCode;
  Version:=VersionCode;
  FCol:=ColCount;
  FRow:=RowCount;
  end;
 AStream.Write(hdr,SizeOf(hdr));

 for j:=1 to rowcount do
  for i:=1 to colcount do
    begin
    z:=Items[i,j];
    AStream.Write(z,SizeOf(TComplex));
    end;
end;

procedure TComplexMatrix.SaveToFile(Name:string);
var tmp:TFileStream;
begin
  tmp:=TFileStream.Create(name,fmCreate);
  try
    SaveToStream(tmp);
  finally
    tmp.Free;
  end;
end;

procedure TComplexMatrix.LoadFromStream(AStream:TStream);
var hdr:TComplexMatrixFileHeader;
    i,j:integer;
    Z:Tcomplex;
begin
  AStream.Read(hdr,SizeOf(hdr));

  if hdr.lbl=LabelCode then
   begin
    if hdr.Header=HeaderCode then
     begin
      if hdr.Version=VersionCode then
       begin

       RecreateMatrix(hdr.Fcol,hdr.frow);
       FillMatrix(0);

       for j:=1 to hdr.Frow do
        for i:=1 to hdr.FCol do
         begin
         Astream.read(z,sizeof(TComplex));
         Items[i,j]:=z;
         end;

       end
      else raise Exception.Create('Version of file not supported');
     end
    else raise Exception.Create('Wrong header of matrix');
   end
  else raise Exception.Create('Wrong signature of matrix');

end;

procedure TComplexMatrix.LoadFromFile(Name:string);
var tmp:TFileStream;
begin
  tmp:=TFileStream.Create(name,fmOpenRead);
  try
    LoadFromStream(tmp);
  finally
    tmp.Free;
  end;
end;


end.

