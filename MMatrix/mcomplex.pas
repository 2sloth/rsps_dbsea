unit MComplex;

interface

uses system.SysUtils, system.math, system.strutils;

type
    TComplex = packed record
      r,i: double;

      function modulus: double;
      function modulusSquared: double;
      function isNaN: boolean;
      function isNil: boolean;
      function argument: double;
      function conjugate: TComplex;
      function discriminant: double;
      function normalise(): TComplex;
      function mulByI: TComplex;

      class operator Negative(a: TComplex): Tcomplex;
      class operator Equal(a, b: TComplex): boolean;
      class operator Add(a, b: TComplex): Tcomplex;
      class operator Add(a: TComplex; b: double): Tcomplex;
      class operator Add(b: double; a: TComplex): Tcomplex;
      class operator Subtract(a, b: TComplex): Tcomplex;
      class operator Subtract(a: TComplex; b: double): TComplex;
      class operator Subtract(b: double; a: TComplex): TComplex;
      class operator Multiply(a, b: TComplex): Tcomplex;
      class operator Multiply(a: TComplex; b:double): Tcomplex;
      class operator Multiply(b:double; a: TComplex): Tcomplex;
      class operator Divide(a, b: TComplex): Tcomplex;
      class operator Divide(a: TComplex; b:double): TComplex;
      class operator Divide(b:double; a: TComplex): TComplex;
      class operator implicit(d: double): TComplex;

      procedure setWithDenom(val: TComplex; denom: double); overload;
      procedure setWithDenom(val, denom: double); overload;
      procedure add(val: TComplex); overload;
      procedure add(val: double); overload;
      procedure addWithDenom(val: TComplex; denom: double); overload;
      procedure addWithDenom(val, denom: double); overload;

      function toString(const forOctave: boolean = false): string;
     end;

TComplexVect = array of TComplex;
//pComplexVect = ^TComplexVect;
TComplexVectHelper = record helper for TComplexVect
private
    function getJ(const i,j,n: integer): integer;
    function getDivisor(const i,n: integer): integer;
public
    function get1stDeriv(const i,n: integer; const dz: double): TComplex;
    function copy(): TComplexVect;
    function toString(const forOctave: boolean = false): string;
    procedure zero;
    function modulusSum: double;
end;

TComplexMat = array of TComplexVect;
TComplexMatHelper = record helper for TComplexMat
    procedure mySetValWithDenom(const x, row, rowXFiddle, y: integer; const z: TComplex; const denom: double); overload;
    procedure mySetValWithDenom(const x, row, rowXFiddle, y: integer; const d, denom: double); overload;
    procedure myAddValWithDenom(const x, row, rowXFiddle, y: integer; const z: TComplex; const denom: double); overload;
    procedure myAddValWithDenom(const x, row, rowXFiddle, y: integer; const d, denom: double); overload;
    function  multByVectHeptaDiag(const v: TComplexVect; const n: integer): TComplexVect;
    function  multByVectTriDiag(const v: TComplexVect; const n: integer): TComplexVect;
    //procedure add1Ord(const i, halfn: integer; const val: TComplex; const dz: double);
    //procedure add2Ord(const i, halfn: integer; const val: TComplex; const dz: double);
    function  copy: TComplexMat;
end;
TComplexField = array of TComplexMat;
//pComplexField = ^TComplexField;
TComplexFieldArray = array of TComplexField;
pComplexFieldArray = ^TComplexFieldArray;

function ComplexToStr(z : tcomplex):String;
function StrToComplex(z : string):Tcomplex;

function ComplexEqualZero(z:Tcomplex):Boolean;
function AddComplex(z1,z2:Tcomplex):Tcomplex; overload;
function SubComplex(z1,z2:Tcomplex):Tcomplex; overload;
function MulComplex(z1,z2:Tcomplex):Tcomplex; overload;
function DivComplex(z1,z2:Tcomplex):Tcomplex; overload;
function AddComplex(z1:Tcomplex;r2:double):Tcomplex; overload;
function SubComplex(z1:Tcomplex;r2:double):Tcomplex; overload;
function MulComplex(z1:Tcomplex;r2:double):Tcomplex; overload;
function DivComplex(z1:Tcomplex;r2:double):Tcomplex; overload;

procedure AddComplexEx(const z1,z2:Tcomplex; var zout : TComplex);

function NegativeComplex(z : Tcomplex) : Tcomplex;
function ModulusComplex(z : Tcomplex) : double;
function InversComplex(z : Tcomplex) : Tcomplex;
function descriminantComplex(z : TComplex) : double;
function NullComplex : Tcomplex;
function nanComplex : TComplex;
function MakeComplex(z1,z2 : double) : Tcomplex;

function PolarComplex(z1,z2:double):Tcomplex;


function MulComplexParI(z1:Tcomplex):Tcomplex;
function MulComplexParNegI(z1:Tcomplex):Tcomplex;

function cpower(z: Tcomplex; d: double): TComplex;
function ComplexEXP(z:Tcomplex):Tcomplex;
function ComplexLN(z:Tcomplex):Tcomplex;
function ComplexSQRT(z:Tcomplex):TComplex;
function ComplexSQR(z:Tcomplex):TComplex;

function ComplexCOS(z:Tcomplex):Tcomplex;
function ComplexSIN(z:Tcomplex):Tcomplex;
function ComplexTAN(z:TComplex):Tcomplex;
function ComplexCOTAN(z:TComplex):Tcomplex;
function ComplexSEC(z:TComplex):Tcomplex;
function ComplexCOSEC(z:TComplex):Tcomplex;

function SaferComplexARCCOS(z:Tcomplex):TComplex;
function ComplexARCCOS(z:Tcomplex):TComplex;
function ComplexARCSIN(z:Tcomplex):TComplex;
function ComplexARCTAN(z:Tcomplex):TComplex;

function ComplexARGCOSH(z:Tcomplex):Tcomplex; // argch(z)
function ComplexARGSINH(z:Tcomplex):Tcomplex; // argsh(z)
function ComplexARGTANH(z:Tcomplex):Tcomplex; // argth(z)

function ComplexCOSH(z:Tcomplex):Tcomplex; // Cosh(z)
function ComplexSINH(z:Tcomplex):Tcomplex; // Sinh(z)
function ComplexTANH(z:Tcomplex):Tcomplex; // Tanh(z)
function ComplexCOTANH(z:TComplex):Tcomplex;
function ComplexSECH(z:TComplex):Tcomplex;
function ComplexCOSECH(z:TComplex):Tcomplex;

function ComplexVectModulusSum(za:TComplexVect): double;


//function TakeRealPart(z:TComplex):double;
//function TakeImagePart(z:TComplex):double;

implementation

{$IFNDEF HASP_DIAGNOSTIC}
uses bandsolver;
{$ENDIF}

class operator TComplex.Negative(a: TComplex): Tcomplex;
begin
    result.r := -a.r;
    result.i := -a.i;
end;

function Tcomplex.normalise;
begin
    result := self/self.modulus;
end;

procedure Tcomplex.setWithDenom(val: TComplex; denom: double);
begin
    self.r := val.r / denom;
    self.i := val.i / denom;
end;

procedure Tcomplex.setWithDenom(val, denom: double);
begin
    self.r := val / denom;
    self.i := 0;
end;

class operator TComplex.Equal(a, b: TComplex): boolean;
begin
    result := (a.r = b.r) and (a.i = b.i);
end;

class operator TComplex.Add(a, b: TComplex): TComplex;
begin
    result.r := a.r + b.r;
    result.i := a.i + b.i;
end;

class operator TComplex.Add(a:TComplex; b: double): TComplex;
begin
    result.r := a.r + b;
    result.i := a.i;
end;

class operator TComplex.Add(b: double; a: TComplex): Tcomplex;
begin
    result := a + b;
end;

class operator TComplex.Subtract(a, b: TComplex): TComplex;
begin
    result.r := a.r - b.r;
    result.i := a.i - b.i;
end;

class operator TComplex.Subtract(a:TComplex; b: double): TComplex;
begin
    result.r := a.r - b;
    result.i := a.i;
end;

class operator TComplex.Subtract(b: double; a: TComplex): TComplex;
begin
    result := makeComplex(b,0) - a;
end;


function Tcomplex.toString(const forOctave: boolean = false): string;
begin
    if forOctave then
        result := floattostr(self.r) + ifthen(self.i >= 0,'+','') + floattostr(self.i) + 'i'
    else
        result := floattostr(self.r) + #09 + floattostr(self.i);
end;

class operator TComplex.Multiply(a, b: TComplex): TComplex;
begin
    result.r := a.r*b.r - a.i*b.i;
    result.i := a.r*b.i + a.i*b.r;
end;

class operator TComplex.Multiply(a: TComplex; b:double): TComplex;
begin
    result.r := a.r*b;
    result.i := a.i*b;
end;

class operator TComplex.Multiply(b:double; a: TComplex): Tcomplex;
begin
    result := a*b;
end;

class operator TComplex.Divide(a, b: TComplex): TComplex;
var
    descrim : double;
begin
    descrim := b.r*b.r+b.i*b.i;
    if descrim <> 0 then
    begin
        result.r := (a.r*b.r+a.i*b.i)/descrim;
        result.i := (a.i*b.r-a.r*b.i)/descrim;
    end
    else
    begin
        raise EDivByZero.CreateFmt('Unable to divide because descriminant of %s = 0',[ComplexToStr(b)]);
        result := NullComplex;
    end;
end;

class operator TComplex.Divide(a: TComplex; b:double): TComplex;
begin
    result.r := a.r/b;
    result.i := a.i/b;
end;

class operator TComplex.Divide(b:double; a: TComplex): TComplex;
begin
    result := makeComplex(b,0) / a;
end;


//function TakeRealPart(z:TComplex):double;
//begin
// Result:=Z.r;
//end;
//
//function TakeImagePart(z:TComplex):double;
//begin
// Result:=Z.i;
//end;


function ComplexToStr(z:tcomplex):String;
begin
if z.r<>0 then result:=FloatTostr(z.r) else result:='';
if (z.i>0) and (z.r<>0) then result:=result+'+';
if z.i<>0 then result:=result+FloatTostr(z.i)+'i' else if result='' then result:='0';
end;

function StrToComplex(z:string):Tcomplex;
var i:integer;
    sr,si:string;
begin
if charinset(z[Length(z)],['i','I']) then
  begin
  i:=Length(z)-1;
  while (not (charinset(z[i],['+','-']))) and (i>1) do dec(i);
  if charinset(z[i-1],['E','e']) then
     begin
     dec(i);
     while not (charinset(z[i],['+','-'])) do dec(i);
     end;
  sr:=copy(z,1,i-1); if sr='' then sr:='0';
  si:=copy(z,i,length(z)-i);
  result.r:=StrToFLoat(sr);
  result.i:=StrToFLoat(si);
  end
else
  begin
  result.r:=StrTOFloat(z);
  result.i:=0;
  end;
end;


// Working With Complex Variables

//function ComplexEqual(z1,z2:Tcomplex):Boolean;
//begin
//result:=(z1.r = z2.r) and (z1.i = z2.i);
//end;

function ComplexEqualZero(z:Tcomplex):Boolean;
begin
result:=( z.r = 0 ) and ( z.i = 0 );
end;

//function ComplexEqualOne(z:Tcomplex):Boolean;
//begin
//result:=( z.r = 1 ) and ( z.i = 0 );
//end;


function AddComplex(z1,z2:Tcomplex):Tcomplex;// z1+z2
begin
    result.r:=z1.r+z2.r;
    result.i:=z1.i+z2.i;
end;

function AddComplex(z1:Tcomplex;r2:double):Tcomplex;
begin
    result.r:=z1.r+r2;
    result.i:=z1.i;
end;

function SubComplex(z1:Tcomplex;r2:double):Tcomplex;
begin
    result.r:=z1.r-r2;
    result.i:=z1.i;
end;

function MulComplex(z1:Tcomplex;r2:double):Tcomplex;
begin
    result.r:=z1.r*r2;
    result.i:=z1.i*r2;
end;

function DivComplex(z1:Tcomplex;r2:double):Tcomplex;
begin
    result.r:=z1.r/r2;
    result.i:=z1.i/r2;
end;

procedure AddComplexEx(const z1,z2:Tcomplex; var zout : TComplex);// z1+z2
begin
//    if zout = nil then

    zout.r := z1.r+z2.r;
    zout.i := z1.i+z2.i;
end;

function SubComplex(z1,z2:Tcomplex):Tcomplex;// z1-z2
begin
    result.r:=z1.r-z2.r;
    result.i:=z1.i-z2.i;
end;

function MulComplex(z1,z2:Tcomplex):Tcomplex;// z1*z2
begin
result.r:=z1.r*z2.r-z1.i*z2.i;
result.i:=z1.r*z2.i+z1.i*z2.r;
end;

function DivComplex(z1,z2:Tcomplex):Tcomplex;// z1/z2
var descrim:double;
begin
 descrim:=z2.r*z2.r+z2.i*z2.i;
 if descrim<>0 then
   begin
   result.r:=(z1.r*z2.r+z1.i*z2.i)/descrim;
   result.i:=(z1.i*z2.r-z1.r*z2.i)/descrim;
   end
 else
   begin
   raise EDivByZero.CreateFmt('Can''t evaluate, descriminant of %s = 0',[ComplexToStr(z2)]);
   result:=NullComplex;
   end;
end;




function NegativeComplex(z:Tcomplex):Tcomplex; // -z
begin
  result.r:=-z.r;
  result.i:=-z.i;
end;

function ModulusComplex(z:Tcomplex):double; // |z|
begin
  result:=sqrt(z.r*z.r+z.i*z.i);
end;

function TComplex.modulus : double;
begin
    result := hypot(r,i);
end;

function TComplex.modulusSquared : double;
begin
    result := sqr(r) + sqr(i);
end;

function Tcomplex.mulByI: TComplex;
begin
    result.r := -self.i;
    result.i := self.r;
end;

class operator Tcomplex.implicit(d: double): TComplex;
begin
    result.r := d;
    Result.i := 0;
end;

function TComplex.isNaN : boolean;
begin
    result := system.math.isnan(r) or system.math.isnan(i);
end;

function TComplex.isNil : boolean;
begin
    result := (r = 0) or (i = 0);
end;

procedure Tcomplex.Add(val: TComplex);
begin
    self.r := self.r + val.r;
    self.i := self.i + val.i;
end;

procedure Tcomplex.Add(val: double);
begin
    self.r := self.r + val;
end;

procedure Tcomplex.addWithDenom(val: TComplex; denom: double);
begin
    self.r := self.r + val.r / denom;
    self.i := self.i + val.i / denom;
end;

procedure Tcomplex.addWithDenom(val, denom: double);
begin
    self.r := self.r + val / denom;
end;

function TComplex.argument : double;
begin
    result := arctan2(i,r);
end;

function TComplex.conjugate : TComplex;
begin
    result.r := r;
    result.i := -i;
end;

function TComplex.discriminant : double;
begin
    result := sqr(r) + sqr(i);
end;


function InversComplex(z:Tcomplex):Tcomplex; // 1/z
var descrim:double;
begin
    descrim := z.discriminant;
    if descrim<>0 then
    begin
        result.r := z.r/descrim;
        result.i := -z.i/descrim;
    end
    else
    begin
        raise EDivByZero.CreateFmt('Can''t evaluate, descriminant of %s = 0',[ComplexToStr(z)]);
            result:=NullComplex;
    end;
end;

function descriminantComplex(z : TComplex) : double;
begin
    result := sqr(z.r) + sqr(z.i);
end;

function NullComplex:Tcomplex;
begin
    result.r := 0;
    result.i := 0;
end;

function nanComplex:Tcomplex;
begin
   result.r := nan;
   result.i := nan;
end;


function MakeComplex(z1,z2:double):Tcomplex;
begin
 result.r:=z1;
 result.i:=z2;
end;

//function ArgumentComplex(z1:Tcomplex):double; // ???? ??????
//begin
//    result := arctan2(z1.i,z1.r);
//end;

//function ConjugeComplex(z1:Tcomplex):Tcomplex; // ??????????????? ???????????
//begin
// result.r:=z1.r;
// result.i:=-z1.i;
//end;

function MulComplexParI(z1:Tcomplex):Tcomplex; // z*i
begin {z=x+y*i -> -y+x*i}
result.r:=-z1.i;
result.i:=z1.r;
end;

function MulComplexParNegI(z1:Tcomplex):Tcomplex; // z*(-i)
begin {z=x+y*i -> y-x*i}
result.r:=z1.i;
result.i:=-z1.r;
end;

function ComplexEXP(z:Tcomplex):Tcomplex; // exp(z)
var
    temp : double;
    cosA, sinA: double;
begin //{e xp(x+y*i)=exp(x)*(cos(y)+i*sin(y)) }
    temp := exp(z.r);// for Speed UP
    system.SineCosine(z.i, sinA, cosA);
    result.r := temp*cosA;
    result.i := temp*sinA;
end;

function ComplexLN(z:Tcomplex):Tcomplex;
var Temp:double;
begin
temp:=z.r*z.r+z.i*z.i;
if temp=0 then
  begin
  raise EMathError.CreateFmt('Can''t evaluate, descriminant of %s = 0',[ComplexToStr(z)]);
  //  result.error:=3; // Zero Logarithme raise
  result:=NullComplex;
  end
else
  begin
  result.r:=ln(temp);
  result.i:= z.argument;
  end;
end;

function ComplexSQRT(z:Tcomplex):TComplex; // sqrt(z)
var
    temp1,temp2:double;
begin
    if (z.r <> 0) or (z.i <> 0) then
    begin
        temp1 := sqrt(0.5*(abs(z.r) + z.modulus));
        temp2 := z.i/(2*temp1);
        if (z.r >= 0) then
        begin
            result.r := temp1;
            result.i := temp2;
        end
        else
        if (z.i < 0) then
        begin
            result.r := -temp2;
            result.i := -temp1;
        end
        else
        begin
            result.r := temp2;
            result.i := temp1;
        end;
    end
    else
        result := z;
end;

function ComplexSQR(z:Tcomplex):TComplex;
begin
    result := z*z;
end;

function ComplexSIN(z:Tcomplex):Tcomplex;
begin
 { sin(x+i*y)=sin(x)*cos(i*y)+cos(x)*sin(i*y) sin(i*y)=i*sh(y) cos(i*x)=ch(x) }
 result.r:=sin(z.r)*cosh(z.i);
 result.i:=cos(z.r)*sinh(z.i);
end;

function ComplexCOS(z:Tcomplex):Tcomplex;
begin
 { cos(x+y*i)=cos(x)*cos(i*y)-sin(x)*sin(i*y) Look Up Man!!!}
 result.r:=cos(z.r)*cosh(z.i);
 result.i:=-sin(z.r)*sinh(z.i);
end;

function ComplexTAN(z:TComplex):Tcomplex;
var T1,t2:Tcomplex;
begin
t1:=ComplexCOS(z);
if ComplexEqualZero(t1) then
   begin
   raise EMathError.CreateFmt('Can''t evaluate, descriminant of %s = 0',[ComplexToStr(z)]);
   result:=NullComplex;
   end
else
   begin
   t2:=ComplexSIN(z);
   result:=DivComplex(t2,t1);
   end;
end;

function ComplexCOTAN(z:TComplex):Tcomplex;
begin
result:=InversComplex(ComplexTAN(z));
end;

function ComplexSEC(z:TComplex):Tcomplex;
begin
result:=InversComplex(ComplexCOS(z));
end;

function ComplexCOSEC(z:TComplex):Tcomplex;
begin
result:=InversComplex(ComplexSIN(z));
end;

function ComplexARGCOSH(z:Tcomplex):Tcomplex; // argch(z);
var t:tcomplex;
begin
//{argch(z)=-ln(z+i*sqrt(1-z*z)) }
t:=SubComplex(MakeComplex(1,0),MulComplex(z,z));
//{t.r:=1-z.r*z.r+z.i*z.i;
//t.i:=-2*z.r*z.i;}
result:=NegativeComplex(ComplexLN(AddComplex(z,MulComplexParI(ComplexSQRT(t)))));
end;

function ComplexARGSINH(z:Tcomplex):Tcomplex; // argch(z);
var t:tcomplex;
begin {argsh(z)=ln(z+i*sqrt(1+z*z)) }
t:=AddComplex(MakeComplex(1,0),MulComplex(z,z));
{t.r:=1+z.r*z.r-z.i*z.i;
t.i:=2*z.r*z.i;}
result:=ComplexLN(AddComplex(z,ComplexSQRT(t)));
end;

function ComplexARGTANH(z:Tcomplex):Tcomplex; // argth(z)
begin {argth(z)=(1/2)*ln((z+1)/(1-z)) }
if ComplexEqualZero(SubComplex(MakeComplex(1,0),z)) then
   begin
   raise EDivByZero.Create('Can''t evaluate due to divide by 0');
   result:=NullComplex;
   end
else
   Result:=DivComplex(ComplexLN(DivComplex(AddComplex(z,MakeComplex(1,0)),SubComplex(MakeComplex(1,0),z))),MakeComplex(2,0));
end;

function ComplexCOSH(z:Tcomplex):Tcomplex; // Cosh(z)
var
    cosA, sinA: double;
begin { ch(x+i*y)=ch(x)*ch(i*y)+sh(x)*sh(i*y) }
system.SineCosine(z.i, sinA, cosA);
result.r:=cosh(z.r)*cosA;
result.i:=sinh(z.r)*sinA;
end;

function ComplexSINH(z:Tcomplex):Tcomplex; // Sinh(z)
var
    cosA, sinA: double;
begin { sh(x+i*y)=sh(x)*ch(i*y)+ch(x)*sh(i*y) }
system.SineCosine(z.i, sinA, cosA);
result.r:=sinh(z.r)*cosA;
result.i:=cosh(z.r)*sinA;
end;

function ComplexTANH(z:Tcomplex):Tcomplex; // Tanh(z)
begin
result:=DivComplex(ComplexSINH(z),ComplexCOSH(z))
end;

function ComplexCOTANH(z:TComplex):Tcomplex;
begin
result:=InversComplex(ComplexTANH(z));
end;

function ComplexSECH(z:TComplex):Tcomplex;
begin
result:=InversComplex(ComplexCOSH(z));
end;

function ComplexCOSECH(z:TComplex):Tcomplex;
begin
result:=InversComplex(ComplexSINH(z));
end;

function SaferComplexARCCOS(z:Tcomplex):TComplex;
begin
    //MCK added case, for when z.i = 0 and z.r is very large
    if (z.i = 0) and (z.r > 1e7) then
        z.r := 1e7;

    result := MulComplexParI(ComplexARGCOSH(z)); //MCK changed to positive
end;

function ComplexARCCOS(z:Tcomplex):TComplex;
begin { arccos(z)=-i*argch(z) }
result:=MulComplexParI(ComplexARGCOSH(z)); //MCK changed to positive
end;

function ComplexARCSIN(z:Tcomplex):TComplex;
begin { arcsin(z)=-i*argsh(i*z) }
result:=MulComplexParNegI(ComplexARGSINH(MulComplexParI(z)));
end;

function ComplexARCTAN(z:Tcomplex):TComplex;
begin { arctg(z)=-i*argth(i*z) }
result:=MulComplexParNegI(ComplexARGTANH(MulComplexParI(z)));
end;

function PolarComplex(z1,z2:double):Tcomplex;
var
    cosA, sinA: double;
begin
    System.SineCosine(z2, sinA, cosA);
    result.r := z1*cosA;
    result.i := z1*sinA;
end;

function ComplexVectModulusSum(za:TComplexVect) : double;
var
    i : integer;
begin
    result := 0;
    for i := 0 to length(za) - 1 do
        result := result + za[i].modulus;
end;

{ TComplexVectHelper }

//function TComplexVectHelper.pFirst: pDouble;
//begin
//    result := pDouble(@(self[0]));
//end;

{ TComplexMatHelper }

function TComplexMatHelper.copy: TComplexMat;
var
    i,j: integer;
begin
    setlength(result,length(self),length(self[0]));
    for i := 0 to length(self) - 1 do
        for j := 0 to length(self[0]) - 1 do
            result[i,j] := self[i,j];
end;

function TComplexMatHelper.multByVectHeptaDiag(const v: TComplexVect; const n: integer): TComplexVect;
begin
    setlength(result,length(v));
    {$IFNDEF HASP_DIAGNOSTIC}
    TBandSolver.banmulOrig(self,n,3,3,v,result);
    {$ENDIF}
end;

function TComplexMatHelper.multByVectTriDiag(const v: TComplexVect; const n: integer): TComplexVect;
begin
    setlength(result,length(v));
    {$IFNDEF HASP_DIAGNOSTIC}
    TBandSolver.banmulOrig(self,n,1,1,v,result);
    {$ENDIF}
end;

//procedure TComplexMatHelper.add1Ord(const i, halfn: integer; const val: TComplex; const dz: double);
//begin
//    if i = 0 then
//    begin
//        self[i+1+0,i+1] := -val/dz;
//        self[i+1+1,i+1] :=  val/dz;
//    end
//    else if i = halfn - 1 then
//    begin
//        self[i+1-1,i+1] := -val/dz;
//        self[i+1+0,i+1] :=  val/dz;
//    end
//    else
//    begin
//        self[i+1-1,i+1] := -val/2/dz;
//        self[i+1+1,i+1] :=  val/2/dz;
//    end;
//end;
//
//procedure TComplexMatHelper.add2Ord(const i, halfn: integer; const val: TComplex; const dz: double);
//begin
//    if i = 0 then
//    begin
//        self[i+1+0,i+1] := -2*val / dz/dz;
//        self[i+1+1,i+1] := val / dz/dz;
//    end
//    else if i = halfn - 1 then
//    begin
//        self[i+1-1,i+1] := val / dz/dz;
//        self[i+1+0,i+1] := -2*val / dz/dz;
//    end
//    else
//    begin
//        self[i+1-1,i+1] := val / dz/dz;
//        self[i+1+0,i+1] := -2*val / dz/dz;
//        self[i+1+1,i+1] := val / dz/dz;
//    end;
//end;

procedure TComplexMatHelper.myAddValWithDenom(const x, row, rowXFiddle, y: integer; const z: TComplex; const denom: double);
var
    c,r: integer;
begin
    c := 3+2*x+rowXFiddle; //column
    r := 2*y+row; //row
    self[r,c] := self[r,c] + z/denom;
end;

procedure TComplexMatHelper.myAddValWithDenom(const x, row, rowXFiddle, y: integer; const d, denom: double);
var
    c,r: integer;
begin
    c := 3+2*x+rowXFiddle; //column
    r := 2*y+row; //row
    self[r,c] := self[r,c] + MakeComplex(d/denom,0);
end;

procedure TComplexMatHelper.mySetValWithDenom(const x, row, rowXFiddle, y: integer; const d, denom: double);
begin
    self[2*y+row,3+2*x+rowXFiddle] := MakeComplex(d/denom,0);
end;

procedure TComplexMatHelper.mySetValWithDenom(const x, row, rowXFiddle, y: integer; const z: TComplex; const denom: double);
begin
    self[2*y+row,3+2*x+rowXFiddle] := z/denom;
end;



function TComplexVectHelper.getDivisor(const i,n: integer): integer;
begin
    result := ifthen((i = 0) or (i = n - 1),1,2);
end;

function TComplexVectHelper.getJ(const i, j, n: integer): integer;
begin
    if i = 0 then
        result := ifthen(j = -1,0,1)
    else if i = n - 1 then
        result := ifthen(j = -1,-1,0)
    else
        result := j;
end;

function TComplexVectHelper.copy: TComplexVect;
var
    i: integer;
begin
    setlength(result, length(self));
    for i := 0 to length(self) - 1 do
        result[i] := self[i];
end;

function TComplexVectHelper.toString(const forOctave: boolean = false): string;
var
    z: TComplex;
begin
    result := '';
    for z in self do
        result := result + z.toString(forOctave) + ifthen(forOctave,';','') + #13#10;
end;

procedure TComplexVectHelper.zero;
var
    i: integer;
begin
    for i := 0 to length(self) - 1 do
        self[i] := 0;
end;

function TComplexVectHelper.modulusSum: double;
var
    z: Tcomplex;
begin
    result := 0;
    for z in self do
        result := result + z.modulus;
end;

function TComplexVectHelper.get1stDeriv(const i,n: integer; const dz: double): TComplex;
begin
    result := (self[i + getJ(i,1,n)] - self[i + getJ(i,-1,n)]) / self.getDivisor(i,n) / dz;
end;

function cpower(z: Tcomplex; d: double): TComplex;
var
    nrm, ang: double;
    cosA, sinA: double;
begin
    if ComplexEqualZero(z) then exit(z);

    nrm := system.math.power(z.r*z.r + z.i*z.i,d/2);
    ang := system.math.ArcTan2(z.i,z.r);// arctanExtX87(z.i/z.r);
    system.SineCosine(d*ang, sinA, cosA);
    result.r := nrm * cosA;
    result.i := nrm * sinA;
end;


end.

