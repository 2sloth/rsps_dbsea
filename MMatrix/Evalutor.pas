unit evalutor;

interface

function Evaluate(const A:array of extended;Express:string):extended;
function CheckFormula(Express:string):boolean;

implementation

uses Math,SysUtils,dialogs;

function CheckFormula(Express:string):boolean;
var i,c:integer;
begin
c:=0;
for i:=1 to Length(Express) do
 if Express[i]='(' then inc(c) else
  if Express[i]=')' then dec(c);
if c=0 then Result:=true else Result:=false;
end;

function Evaluate(const A:array of extended;Express:string):extended;
var
    position:integer;
    w,ar:string;

    //helper functions
    function CutBeforeDelemiter(Express:string;position:integer):string;
    begin
    result:=copy(Express,1,position-1);
    end;

    function CutAfterDelemiter(Express:string;position:integer):string;
    begin
    result:=copy(Express,position+1,length(Express)-position);
    end;

    function FindNextBracketDR(Express:string;position:integer):integer;
    var count:integer;
    begin
    count:=0;
    inc(position);
    while (count>=0) and ((position<=length(Express)) or (position<>0)) do
     begin
     if Express[position]='(' then inc(count) else if Express[position]=')' then dec(count);
     inc(position);
     end;
    //if count=0 then
     result:=position-1

    //else Raise EMathError.Create('�������������� ���������� �������� � �������� ������.');
    end;

    function FindNextBracketDL(Express:string;position:integer):integer;
    var count:integer;
    begin
    count:=0;
    dec(position);
    while (count>=0) and ((position<=length(Express)) or (position<>0)) do
     begin
     if Express[position]=')' then inc(count) else if Express[position]='(' then dec(count);
     dec(position);
     end;
    //if count=0 then
    result:=position+1
    //else Raise EMathError.Create('�������������� ���������� �������� � �������� ������.');
    end;

    function CheckLimitBrackets(Express:string):boolean;
    begin
    result:=false;
    if Express[1]='(' then
       if FindNextBracketDR(Express,1)=length(Express) then
          result:=true
       else
          result:=false;

    end;

    function DeleteLimitBrackets(Express:string):string;
    begin
    if CheckLimitBrackets(Express) then
       result:=copy(Express,2,length(Express)-2)
    else
       result:=Express;
    end;

    function Find1stLevelDelemiter(Express:string):integer;
    var position,
        len:integer;
    begin
    position:=1;
    len:=length(Express);
    if charinset(express[position],['-','+']) then inc(position) else if express[position]='(' then position:=FindNextBracketDR(Express,position);
    while not(charinset(Express[position],['+','-'])) and (position<=len) do
     begin
     inc(position);
     if charinset(Express[position],['-','+']) then
      begin
      if charinset(Express[position-1],['+','-','*','/']) then inc(position);
      end
     else if Express[position]='(' then position:=FindNextBracketDR(Express,position);
     end;
    if position<len then result:=position else result:=0;
    end;

    function Find2ndLevelDelemiter(Express:string):integer;
    var position:integer;
    begin
    position:=length(Express);
    if Express[position]=')' then position:=FindNextBracketDL(Express,position);
    while not(charinset(Express[position],['*','/','^'])) and (position>1) do
     begin
     dec(position);
     if Express[position]=')' then position:=FindNextBracketDL(Express,position);
     end;
    if position>1 then result:=position else result:=0;
    end;

    function Findword(Express:string;var arguments:string):string;
    var position,
        len:integer;
    begin
    position:=1;
    result:='';
    len:=length(Express);
    while (charinset(Express[position],['0'..'9','A'..'Z','a'..'z'])) and (position<=len) do
     begin
     result:=result+Express[position];
     inc(position);
     end;
    if position<len then arguments:=CutAfterDelemiter(Express,position-1) else arguments:='';
    end;

//start of evaluate function
begin
result := 0;
Express:=DeleteLimitBrackets(Express);
position:=Find1stLevelDelemiter(Express);
if position>0 then
 begin
  if Express[position]='+' then
     result:=evaluate(A,CutBeforeDelemiter(Express,position))+evaluate(A,CutAfterDelemiter(Express,position))
  else if Express[position]='-' then
     result:=evaluate(A,CutBeforeDelemiter(Express,position))+evaluate(A,CutAfterDelemiter(Express,position-1));
 end
else
 begin
 position:=Find2ndLevelDelemiter(Express);
 if position>0 then
  begin
  if Express[position]='*' then
      result:=evaluate(A,CutBeforeDelemiter(Express,position))*evaluate(A,CutAfterDelemiter(Express,position))
  else if Express[position]='/' then
      result:=evaluate(A,CutBeforeDelemiter(Express,position))/evaluate(A,CutAfterDelemiter(Express,position))
  else if Express[position]='^' then
      result:=power(evaluate(A,CutBeforeDelemiter(Express,position)),evaluate(A,CutAfterDelemiter(Express,position)));
  end
 else
  begin
  if CheckLimitBrackets(Express) then result:=Evaluate(A,DeleteLimitBrackets(Express))
  else
   begin
   if Express[1]='-' then result:=-Evaluate(A,CutAfterDelemiter(Express,1))
   else
     begin
     w:=uppercase(findword(Express,ar));
     if w='SIN' then result:=sin(evaluate(A,ar)) else
     if w='COS' then result:=cos(evaluate(A,ar)) else
     if w='TAN' then result:=tan(evaluate(A,ar)) else
     if w='EXP' then result:=exp(evaluate(A,ar)) else
     if w='ABS' then result:=abs(evaluate(A,ar)) else
     if w='ARCTAN' then result:=arctan(evaluate(A,ar)) else
     if w='ARCSIN' then result:=arcsin(evaluate(A,ar)) else
     if w='LN' then result:=ln(evaluate(A,ar)) else
     if w='LOG' then result:=log10(evaluate(A,ar)) else
     if w='SQR' then result:=sqr(evaluate(A,ar)) else
     if w='SQRT' then result:=sqrt(evaluate(A,ar)) else
     if w='INT' then result:=int(evaluate(A,ar)) else
     if w='FRAC' then result:=frac(evaluate(A,ar)) else
     if w='PI' then result:=pi else
      if UpperCase(w)='R' then result:=a[0] else
      if UpperCase(w)='X' then result:=a[1] else
      if UpperCase(w)='G' then result:=a[2] else
      if UpperCase(w)='B' then result:=a[3] else
      if UpperCase(w)='E' then result:=a[4] else
      if UpperCase(w)='EF' then result:=a[5] else
      if UpperCase(w)='KR' then result:=a[6] else
      if UpperCase(w)='KI' then result:=a[7] else
      if UpperCase(w)='SUM' then result:=a[8] else
      if Uppercase(w[1])='P' then result:=a[9+strtoint(copy(w,2,length(w)-1))] else
     {if findvariable(w,ar)<>-1 then result:=evaluate(ar) else}
      begin
       try
       result:=strtofloat(Express);
       except
         on E: EConvertError do
            begin
            ShowMessage(E.ClassName + #13 + E.Message);
            result:=0;
            end;
       end;
      end;
     end;
   end;
  end;
 end;
end;

end.
