unit MatrixAdd;

interface

uses Classes,Math,SysUtils,Grids,mcomplex, mmatrix;

function Norma(A:TComplexMatrix;x:integer):TComplex;
function Equality(A,B:TComplexMatrix):Boolean;

implementation

function Norma(A:TComplexMatrix;x:integer):Tcomplex;
var i,j:integer;
    s:TComplex;
begin
  s.r:=0;
  s.i:=0;
  if x=1 then
  begin
    for i:=1 to A.ColCount do
      for j:=1 to A.RowCount do if (abs(A[i,j].r)>abs(s.r)) and (abs(A[i,j].i)>=abs(s.i)) then
      begin
        s.r:=abs(A[i,j].r);
        s.i:=abs(A[i,j].i);
      end;
   result:=s;
  end
  else
  begin
    for i:=1 to A.ColCount do
      for j:=1 to A.RowCount do s:=AddComplex(s,MulComplex(A[i,j],A[i,j]));
    result:=ComplexSQRT(s);
  end;
end;

function Equality(A,B:TComplexMatrix):Boolean;
  var i,j:integer;
begin
  result:=True;
  for i:=1 to A.ColCount do
    for j:=1 to B.ColCount do
      if (A[i,j].r<>B[i,j].r) then result:=False;
end;


end.
