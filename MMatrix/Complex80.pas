unit Complex80;

interface
uses system.SysUtils, system.math, MComplex, uTExtendedX87;

type
    TComplex80 = record
        r,i: TExtendedX87;

        class operator Add(a, b: TComplex80): TComplex80;
        class operator Add(a: TExtendedX87; b: TComplex80): TComplex80;
        class operator Add(b: TComplex80; a: TExtendedX87): TComplex80;
        class operator Subtract(a,b: TComplex80): TComplex80;
        class operator Subtract(a: TExtendedX87; b: TComplex80): TComplex80;
        class operator Multiply(a, b: TComplex80): Tcomplex80;
        class operator Multiply(a: TExtendedX87; b: TComplex80): Tcomplex80;
        class operator Multiply(b: TComplex80; a: TExtendedX87): Tcomplex80;
        class operator Divide(a, b: TComplex80): TComplex80;
        class operator Divide(a: TComplex80; b: double): TComplex80;
        class operator negative(a: TComplex80): TComplex80;

        class operator implicit(d: TExtendedX87): TComplex80;
        class operator implicit(d: double): TComplex80;
        class operator implicit(a: TComplex80): TComplex;

        function  modulus: TExtendedX87;
        function  mulByI: TComplex80;
        function  toString: string;
    end;
    TComplex80Vect = array of TComplex80;
    TComplex80Mat = array of TComplex80Vect;

    function  makeComplex80(r,i: TExtendedX87): TComplex80;
    function  NullComplex80: TComplex80;
    function  complex80EXP(z: Tcomplex80): Tcomplex80;
    function  c80power(z: Tcomplex80; d: TExtendedX87): TComplex80;
    function  complex80SQRT(z: TComplex80): TComplex80;
    function  Complex80EqualZero(z: TComplex80): Boolean;

type
    TComplex80VectHelper = record helper for TComplex80Vect
        function  toComplexVect: TComplexVect;
    end;

    TComplex80MatHelper = record helper for TComplex80Mat
        function  transpose: TComplex80Mat;
        function  mult(b: TComplex80Mat): TComplex80Mat; overload;
        function  mult(b: TComplex80Vect): TComplex80Vect; overload;
    end;

implementation

{ TComplex80 }

function  makeComplex80(r,i: TExtendedX87): TComplex80;
begin
    result.r := r;
    result.i := i;
end;

function  NullComplex80: TComplex80;
begin
    result.r := 0;
    result.i := 0;
end;

function complex80EXP(z:Tcomplex80): Tcomplex80; // exp(z)
var
    temp: TExtendedX87;
begin
    //{e xp(x+y*i)=exp(x)*(cos(y)+i*sin(y)) }
    temp := expExtX87(z.r);
    result.r := temp * cosExtX87(z.i);
    result.i := temp * sinExtX87(z.i);
end;


class operator TComplex80.Add(a, b: TComplex80): TComplex80;
begin
    result.r := a.r + b.r;
    result.i := a.i + b.i;
end;

class operator TComplex80.Multiply(a, b: TComplex80): Tcomplex80;
begin
    result.r := a.r*b.r - a.i*b.i;
    result.i := a.r*b.i + a.i*b.r;
end;

function c80power(z: Tcomplex80; d: TExtendedX87): TComplex80;
var
    nrm, ang: TExtendedX87;
begin
    if Complex80EqualZero(z) then exit(z);

    nrm := system.math.power(z.r*z.r + z.i*z.i,d/2);
    ang := system.math.ArcTan2(z.i,z.r);// arctanExtX87(z.i/z.r);
    result.r := nrm * cosExtX87(d*ang);
    result.i := nrm * sinExtX87(d*ang);
end;

function complex80SQRT(z: TComplex80): TComplex80;
var
    temp1,temp2: TExtendedX87;
begin
    if (z.r <> 0) or (z.i <> 0) then
    begin
        temp1 := sqrtExtX87(0.5*(abs(z.r) + z.modulus));
        temp2 := z.i/(2*temp1);
        if (z.r >= 0) then
        begin
            result.r := temp1;
            result.i := temp2;
        end
        else
        if (z.i < 0) then
        begin
            result.r := -temp2;
            result.i := -temp1;
        end
        else
        begin
            result.r := temp2;
            result.i := temp1;
        end;
    end
    else
        result := z;
end;

function Complex80EqualZero(z: TComplex80): Boolean;
begin
    result:=( z.r = 0 ) and ( z.i = 0 );
end;


class operator TComplex80.Subtract(a, b: TComplex80): TComplex80;
begin
    result.r := a.r - b.r;
    result.i := a.i - b.i;
end;

class operator TComplex80.Add(a: TExtendedX87; b: TComplex80): TComplex80;
begin
    result.r := a + b.r;
    result.i := b.i;
end;

class operator TComplex80.Add(b: TComplex80; a: TExtendedX87): TComplex80;
begin
    result.r := a + b.r;
    result.i := b.i;
end;

class operator TComplex80.Divide(a: TComplex80; b: double): TComplex80;
begin
    result.r := a.r / b;
    result.i := a.i / b;
end;

class operator TComplex80.implicit(d: TExtendedX87): TComplex80;
begin
    result.r := d;
    result.i := 0;
end;

class operator TComplex80.implicit(d: double): TComplex80;
begin
    result.r := d;
    result.i := 0;
end;

class operator TComplex80.implicit(a: TComplex80): TComplex;
begin
    result.r := a.r;
    result.i := a.i;
end;

class operator TComplex80.Divide(a, b: TComplex80): TComplex80;
var
    descrim: TExtendedX87;
begin
    descrim := b.r*b.r+b.i*b.i;
    if descrim <> 0 then
    begin
        result.r := (a.r*b.r+a.i*b.i)/descrim;
        result.i := (a.i*b.r-a.r*b.i)/descrim;
    end
    else
    begin
        //raise EDivByZero.CreateFmt('Unable to divide because descriminant of %s = 0',[ComplexToStr(b)]);
        result := NullComplex80;
    end;
end;


function TComplex80.modulus: TExtendedX87;
begin
    result := sqrtExtX87(r*r + i*i);
end;

function TComplex80.mulByI: TComplex80;
begin
    result.r := -self.i;
    result.i := self.r;
end;

class operator TComplex80.Multiply(b: TComplex80; a: TExtendedX87): Tcomplex80;
begin
    result.r := a*b.r;
    result.i := a*b.i;
end;

class operator TComplex80.negative(a: TComplex80): TComplex80;
begin
    result.r := -a.r;
    result.i := -a.i;
end;

class operator TComplex80.Subtract(a: TExtendedX87; b: TComplex80): TComplex80;
begin
    result.r := a - b.r;
    result.i := -b.i;
end;

function TComplex80.toString: string;
begin
    result := floattostr(r) + ' ' + floattostr(i) + 'i';
end;

class operator TComplex80.Multiply(a: TExtendedX87; b: TComplex80): Tcomplex80;
begin
    result.r := a*b.r;
    result.i := a*b.i;
end;

{ TComplex80MatHelper }

function TComplex80MatHelper.mult(b: TComplex80Mat): TComplex80Mat;
var
    i,j,k: integer;
    z: TComplex80;
begin
    setlength(result, length(self), length(b[0]));
    for I := 0 to length(self) - 1 do
        for j := 0 to length(b[0]) - 1 do
        begin
            z := NullComplex80;
            for k := 0 to length(self[0]) - 1 do
                z := z + self[i,k] * b[k,j];
            result[i,j] := z;
        end;
end;

function TComplex80MatHelper.mult(b: TComplex80Vect): TComplex80Vect;
var
    i,k: integer;
    z: TComplex80;
begin
    setlength(result, length(self));
    for I := 0 to length(self) - 1 do
    begin
        z := NullComplex80;
        for k := 0 to length(self[0]) - 1 do
            z := z + self[i,k] * b[k];
        result[i] := z;
    end;
end;

function TComplex80MatHelper.transpose: TComplex80Mat;
var
    i,j: integer;
begin
    setlength(result, length(self[0]), length(self));
    for i := 0 to length(self) - 1 do
        for j := 0 to length(self[0]) - 1 do
            result[j,i] := self[i,j];
end;

{ TComplex80VectHelper }

function TComplex80VectHelper.toComplexVect: TComplexVect;
var
    i: integer;
begin
    setlength(result, length(self));
    for i := 0 to length(result) - 1 do
        result[i] := self[i];
end;

end.

