unit MarchingSquares;

interface

uses system.types, system.generics.collections, basetypes, helperFunctions, pos3;

type
    TIntPair = array[0..1] of integer;
    TIntPairMatrix = array of array of TIntPair;

    TPosPair = class(TObject)
        p1,p2: TfPoint;

        constructor create(const x1,y1,x2,y2: double);
    end;
    TPosPairList = class( TObjectList<TPosPair>)
        function  toPos3s: TPos3ListList;
    end;

    TMarchingSquares = class(TObject)
    class function MarchingSquaresBits(const bmIn: TBoolMatrix): TIntegerMatrix;
    class function MarchingSquaresTrits(const imIn: TIntegerMatrix): TIntegerMatrix;
    class function MarchingTrisBits(const bmIn: TBoolMatrix): TIntPairMatrix;
    class function MarchingTrisTrits(const imIn: TIntegerMatrix): TIntPairMatrix;

    class function generateMarchingSquaresLines(const miMSLines: TIntegerMatrix; const xs,ys: TDoubleDynArray): TPosPairList;
    class function drawMarchingTrisLines(const miMSLines: TIntPairMatrix; const xs,ys: TDoubleDynArray): TPosPairList;
    end;

implementation

class function TMarchingSquares.drawMarchingTrisLines(const miMSLines: TIntPairMatrix; const xs,ys: TDoubleDynArray): TPosPairList;
{the problem with having the triangles with a right angle, is that we don't get
sloping lines in 1 direction. looks a bit weird. on the other hand, if the
triangles meander, then the edge looks a bit funny}
const
    cc: array[0..1,1..9,0..1] of integer = (((0,0),(1,0),(2,0),
                                                (0,1),(1,1),(2,1),
                                                (0,2),(1,2),(2,2)),
                                                ((2,2),(1,2),(0,2),
                                                (2,1),(1,1),(0,1),
                                                (2,0),(1,0),(0,0)));
var
    i,j,leni,lenj,q: integer;
    i2,j2: TIntegerDynArray;
begin
    {each square is made up of 2 triangles
    first triangle :
    4
    | \
    1- 2
    second triangle :
    2 - 1
      \ |
        4
    now we can use the same code for both triangles.
    }

    result := TPosPairList.create(true);

    leni := length(miMSLines);
    lenj := length(miMSLines[0]);
    {create 2*i and 2*j vectors for later quicker accessing.}
    setlength(i2,leni);
    for i := 0 to leni - 1 do
        i2[i] := 2*i;
    setlength(j2,lenj);
    for j := 0 to lenj - 1 do
        j2[j] := 2*j;

    for i := 0 to leni - 1 do
        for j := 0 to lenj - 1 do
        begin

            {this is the marching tris algorithm drawing part.
            based on the values we got from our boolean corners of
            each triangle, choose where to draw lines. (NB my y (j)
            direction is reversed to the normal sense)
            do nothing for 0 and 7, ie all true or all false}

            for q := 0 to 1 do
            begin
                case miMSLines[i,j,q] of
                    1,6: result.add(TPosPair.create(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]])); //across 1 corner
                    2,5: result.add(TPosPair.create(xs[i2[i] + cc[q,2,0]],ys[j2[j] + cc[q,2,1]],xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]])); //across 2 corner - vertical
                    3,4: result.add(TPosPair.create(xs[i2[i] + cc[q,4,0]],ys[j2[j] + cc[q,4,1]],xs[i2[i] + cc[q,5,0]],ys[j2[j] + cc[q,5,1]])); //across 4 corner - horizontal
                end; //case
            end;//q
        end;//i,j
end;

class function TMarchingSquares.generateMarchingSquaresLines(const miMSLines: TIntegerMatrix; const xs,ys: TDoubleDynArray): TPosPairList;
var
    i,j: integer;
begin
    result := TPosPairList.create(true);

    for i := 0 to high(miMSLines) do
        for j := 0 to high(miMSLines[0]) do
        begin

            {this is the marching squares algorithm drawing part.
            based on the values we got from our boolean corners of
            each square, choose where to draw lines. (NB my y (j)
            direction is reversed to the normal sense)
            do nothing for 0 and 15, ie all true or all false}

            case miMSLines[i,j] of
                1, 14: result.add(TPosPair.create(xs[2*i + 0],ys[2*j + 1],xs[2*i + 1],ys[2*j + 0]));
                2, 13: result.add(TPosPair.create(xs[2*i + 1],ys[2*j + 0],xs[2*i + 2],ys[2*j + 1]));
                3, 12: result.add(TPosPair.create(xs[2*i + 0],ys[2*j + 1],xs[2*i + 2],ys[2*j + 1])); //horizontal
                4, 11: result.add(TPosPair.create(xs[2*i + 1],ys[2*j + 2],xs[2*i + 2],ys[2*j + 1]));
                5: begin
                    {saddle point}
                    result.add(TPosPair.create(xs[2*i + 0],ys[2*j + 1],xs[2*i + 1],ys[2*j + 2]));
                    result.add(TPosPair.create(xs[2*i + 1],ys[2*j + 0],xs[2*i + 2],ys[2*j + 1]));
                end;
                6, 9: result.add(TPosPair.create(xs[2*i + 1],ys[2*j + 2],xs[2*i + 1],ys[2*j + 0])); //vertical
                7, 8: result.add(TPosPair.create(xs[2*i + 0],ys[2*j + 1],xs[2*i + 1],ys[2*j + 2]));
                10: begin
                    {saddle point}
                    result.add(TPosPair.create(xs[2*i + 0],ys[2*j + 1],xs[2*i + 1],ys[2*j + 0]));
                    result.add(TPosPair.create(xs[2*i + 1],ys[2*j + 2],xs[2*i + 2],ys[2*j + 1]));
                end;
            end; //case miMarchingSquares

        end;//i,j
end;


class function    TMarchingSquares.MarchingSquaresBits(const bmIn: TBoolMatrix): TIntegerMatrix;
var
    i,j,hi,hj: integer;
    SquareCorners: TBooleanDynArray;
begin
    hi := high(bmIn);
    hj := high(bmIn[0]);

    setlength(result,hi,hj);
    setlength(SquareCorners,4);
    for i := 0 to hi - 1 do
        for j := 0 to hj - 1 do
        begin
            {get an integer representing the binary value for the 4 corners}
            SquareCorners[3] := bmIn[i,j];
            SquareCorners[2] := bmIn[i+1,j];
            SquareCorners[1] := bmIn[i+1,j+1];
            SquareCorners[0] := bmIn[i,j+1];

            result[i,j] := SquareCorners.toInt; //big endian
        end; //i and j
end;

class function    TMarchingSquares.MarchingSquaresTrits(const imIn: TIntegerMatrix): TIntegerMatrix;
var
    i,j,hi,hj: integer;
    thisInt: integer;
begin
    if (length(imIn) = 0) or (length(imIn[0]) = 0) then
    begin
        setlength(result,0,0);
        exit;
    end;

    hi := high(imIn);
    hj := high(imIn[0]);

    setlength(result,hi,hj);
    for i := 0 to hi - 1 do
        for j := 0 to hj - 1 do
        begin
            {get an integer representing the ternary value for the 4 corners}
            thisInt := imIn[i,j] * 27;
            thisInt := thisInt + imIn[i+1,j] * 9;
            thisInt := thisInt + imIn[i+1,j+1] * 3;
            thisInt := thisInt + imIn[i,j+1];

            result[i,j] := thisInt;
        end; //i and j
end;

class function    TMarchingSquares.MarchingTrisBits(const bmIn: TBoolMatrix): TIntPairMatrix;
var
    i,j,hi,hj: integer;
    TriCorners: TBooleanDynArray;
begin
    {each square is made up of 2 triangles   (values are the 2^x value)
    first triangle :
    4
    | \
    1- 2
    second triangle :
    2 - 1
      \ |
        4}

    hi := high(bmIn);
    hj := high(bmIn[0]);

    setlength(result,hi,hj); //2 triangles for each index
    setlength(TriCorners,3);
    for i := 0 to hi - 1 do
        for j := 0 to hj - 1 do
        begin
            {get an integer representing the binary value for the 3 vertices}
            {first triangle}
            TriCorners[0] := bmIn[i  ,j+1];
            TriCorners[1] := bmIn[i+1,j  ];
            TriCorners[2] := bmIn[i  ,j  ];
            result[i,j,0] := TriCorners.toInt; //big endian!
            {second triangle}
            TriCorners[0] := bmIn[i+1,j  ];
            TriCorners[1] := bmIn[i  ,j+1];
            TriCorners[2] := bmIn[i+1,j+1];
            result[i,j,1] := TriCorners.toInt; //big endian!
        end; //i and j
end;

class function    TMarchingSquares.MarchingTrisTrits(const imIn: TIntegerMatrix): TIntPairMatrix;
var
    i,j,hi,hj,thisInt: integer;
begin
    {each square is made up of 2 triangles   (values are the 2^x value)
    first triangle :
    4
    | \
    1- 2
    second triangle :
    2 - 1
      \ |
        4}
    hi := high(imIn);
    hj := high(imIn[0]);

    setlength(result,hi,hj); //2 triangles for each index
    for i := 0 to hi - 1 do
        for j := 0 to hj - 1 do
        begin
            {get an integer representing the ternary value for the 3 vertices}
            {first triangle}
            thisInt := imIn[i,j+1] * 9;
            thisInt := thisInt + imIn[i+1,j] * 3;
            result[i,j,0] := thisInt + imIn[i,j];

            {second triangle}
            thisInt := imIn[i+1,j] * 9;
            thisInt := thisInt + imIn[i,j+1] * 3;
            result[i,j,1] := thisInt + imIn[i+1,j+1];
        end; //i and j
end;


{ TPosPair }

constructor TPosPair.create(const x1, y1, x2, y2: double);
begin
    inherited create;

    self.p1.x := x1;
    self.p1.y := y1;
    self.p2.x := x2;
    self.p2.y := y2;
end;

{ TPosPairList }

function TPosPairList.toPos3s: TPos3ListList;
var
    pp: TPosPair;
    pl: TPos3List;
begin
    result := TPos3ListList.create(true);
    for pp in self do
    begin
        pl := TPos3List.create(true);
        pl.add(TPos3.create(pp.p1.x, pp.p1.y, 0));
        pl.add(TPos3.create(pp.p2.x, pp.p2.y, 0));
        result.add(pl);
    end;
end;

end.
