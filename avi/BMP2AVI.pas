//
// Raw mjpeg avi writer
//
// This takes a stream of TBitmap images, compresses them to jpeg and saves them into an mjpeg .avi file - on the fly
//
// no API calls here thank you very much :)
//
// written by C.Moss using Delphi 6  ....  30th March 2003
// tab setting 3'
//
// note, they seem to recommend you add regular JUNK chunks at around every 20Kbytes - something to do with when reading it off CD ??? (beats me)
// This is done for you in the routine that saves the jpeg into the avi ... you could always comment it out it if you wish
//
// ALL chunks in the AVI must start on a 2-byte alignment
//
// ALL images MUST be an even width AND even height size - and ALL the same size, standard AVI do not support changes in image width/height size
//
// note: unless you use the extended avi chunks your file must be limited to the 2GByte size.
//       The extended format is just basically more RIFF chunks than just the normal one - you'll find info on the net about this
//
// Prolly best to make this an object but couldn't be arsed
//
// Freeware, do as you see fit with this code

unit BMP2AVI;

interface

uses Windows, SysUtils, Classes, Graphics;

const
 //CC_DIB = 'DIB ';
 CC_DIB = #0#0#0#0;
 BI_RGB = 0;
 CC_vids = 'vids';
 MaxIndexes = 1000000;
 
 // avih flags
 AVIF_HASINDEX = $00000010;
 AVIF_MUSTUSEINDEX = $00000020;
 AVIF_ISINTERLEAVED = $00000100;
 AVIF_TRUSTCKTYPE = $00000800;
 AVIF_WASCAPTUREFILE = $00010000;
 AVIF_COPYRIGHTED = $00020000;
 
 // strh flags
 AVISF_DISABLED = $00000001;
 AVISF_VIDEO_PALCHANGES = $00010000;
 
 // idx1 flags
 AVIIF_LIST = $00000001;
 AVIIF_KEYFRAME = $00000010;
 AVIIF_FIRSTPART = $00000020;
 AVIIF_LASTPART = $00000040;
 AVIIF_MIDPART = $00000060;
 AVIIF_NOTIME = $00000100;
 AVIIF_COMPUSE = $0FFF0000;
 
 // audio format tag
 WAVE_FORMAT_UNKNOWN = $0000; // Microsoft Corporation
 WAVE_FORMAT_PCM = $0001; // Microsoft Corporation
 WAVE_FORMAT_ADPCM = $0002; // Microsoft Corporation
 WAVE_FORMAT_IBM_CVSD = $0005; // IBM Corporation
 WAVE_FORMAT_ALAW = $0006; // Microsoft Corporation
 WAVE_FORMAT_MULAW = $0007; // Microsoft Corporation
 WAVE_FORMAT_OKI_ADPCM = $0010; // OKI
 WAVE_FORMAT_DVI_ADPCM = $0011; // Intel Corporation
 WAVE_FORMAT_IMA_ADPCM = $0011; // Intel Corporation
 WAVE_FORMAT_MEDIASPACE_ADPCM = $0012; // Videologic
 WAVE_FORMAT_SIERRA_ADPCM = $0013; // Sierra Semiconductor Corp
 WAVE_FORMAT_G723_ADPCM = $0014; // Antex Electronics Corporation
 WAVE_FORMAT_DIGISTD = $0015; // DSP Solutions, Inc.
 WAVE_FORMAT_DIGIFIX = $0016; // DSP Solutions, Inc.
 WAVE_FORMAT_DIALOGIC_OKI_ADPCM = $0017; // Dialogic Corporation
 WAVE_FORMAT_YAMAHA_ADPCM = $0020; // Yamaha Corporation of America
 WAVE_FORMAT_SONARC = $0021; // Speech Compression
 WAVE_FORMAT_DSPGROUP_TRUESPEECH = $0022; // DSP Group, Inc
 WAVE_FORMAT_ECHOSC1 = $0023; // Echo Speech Corporation
 WAVE_FORMAT_AUDIOFILE_AF36 = $0024; //
 WAVE_FORMAT_APTX = $0025; // Audio Processing Technology
 WAVE_FORMAT_AUDIOFILE_AF10 = $0026; //
 WAVE_FORMAT_DOLBY_AC2 = $0030; // Dolby Laboratories
 WAVE_FORMAT_GSM610 = $0031; // Microsoft Corporation
 WAVE_FORMAT_MSN = $0032; // Microsoft Corporation
 WAVE_FORMAT_ANTEX_ADPCME = $0033; // Antex Electronics Corporation
 WAVE_FORMAT_CONTROL_RES_VQLPC = $0034; // Control Resources Limited
 WAVE_FORMAT_DIGIREAL = $0035; // DSP Solutions, Inc.
 WAVE_FORMAT_DIGIADPCM = $0036; // DSP Solutions, Inc.
 WAVE_FORMAT_CONTROL_RES_CR10 = $0037; // Control Resources Limited
 WAVE_FORMAT_NMS_VBXADPCM = $0038; // Natural MicroSystems
 WAVE_FORMAT_CS_IMAADPCM = $0039; // Crystal Semiconductor IMA ADPCM
 WAVE_FORMAT_G721_ADPCM = $0040; // Antex Electronics Corporation
 WAVE_FORMAT_MPEG = $0050; // Microsoft Corporation
 WAVE_FORMAT_MPEG3 = $0055; // Microsoft Corporation
 WAVE_FORMAT_CELP48 = $0070; // Lernout & Hauspie
 WAVE_FORMAT_SBC8 = $0071; // Lernout & Hauspie
 WAVE_FORMAT_SBC12 = $0072; // Lernout & Hauspie
 WAVE_FORMAT_SBC16 = $0073; // Lernout & Hauspie
 WAVE_FORMAT_VOXRT = $0074; // Voxware Audio
 WAVE_FORMAT_VOXAC = $0075; // Voxware Audio
 WAVE_FORMAT_CREATIVE_ADPCM = $0200; // Creative Labs, Inc
 WAVE_FORMAT_CREATIVE_FASTSPEECH8 = $0202; // Creative Labs, Inc
 WAVE_FORMAT_CREATIVE_FASTSPEECH10 = $0203; // Creative Labs, Inc
 WAVE_FORMAT_FM_TOWNS_SND = $0300; // Fujitsu Corp.
 WAVE_FORMAT_OLIGSM = $1000; // Ing C. Olivetti & C., S.p.A.
 WAVE_FORMAT_OLIADPCM = $1001; // Ing C. Olivetti & C., S.p.A.
 WAVE_FORMAT_OLICELP = $1002; // Ing C. Olivetti & C., S.p.A.
 WAVE_FORMAT_OLISBC = $1003; // Ing C. Olivetti & C., S.p.A.
 WAVE_FORMAT_OLIOPR = $1004; // Ing C. Olivetti & C., S.p.A.
 
type
 FOURCC = array[0..3] of char;
 
 TCHUNK = record
  fcc: FOURCC;
  cb: Cardinal;
 end;
 
 TAVIH = record
  dwMicroSecPerFrame: dword;
  dwMaxBytesPerSec: dword;
  dwPaddingGranularity: dword;
  dwFlags: dword;
  dwTotalFrames: dword;
  dwInitialFrames: dword;
  dwStreams: dword; // 1 for just video, 2 for video and audio
  dwSuggestedBufferSize: dword;
  dwWidth: dword;
  dwHeight: dword;
  dwReserved: array[0..3] of dword;
 end;
 
 TSTRH = record
  fccType: FOURCC;
  fccHandler: FOURCC;
  dwFlags: dword;
  wPriority: word;
  wLanguage: word;
  dwInitialFrames: dword;
  dwScale: dword;
  dwRate: dword;
  dwStart: dword;
  dwLength: dword;
  dwSuggestedBufferSize: dword;
  dwQuality: dword;
  dwSampleSize: dword;
  rcFrame: record
   Left: integer; // short int
   Top: integer;
   Right: integer;
   Bottom: integer;
  end;
 end;
 
 TPAL = record
  bFirstEntry: byte;
  bNumEntries: byte;
  wFlags: short;
 end;
 
 TIDX1 = packed record
  ckid: FOURCC;
  dwFlags: dword;
  dwChunkOffset: dword;
  dwChunkLength: dword;
 end;
 
 TIDX2 = packed record
  fcc: FOURCC;
  cb: dword;
  wLongsPerEntry: word;
  bIndexSubType: byte;
  bIndexType: byte;
  nEntriesInUse: dword;
  dwChunkId: FOURCC;
  dwReserved: array[0..2] of dword;
 end;
 
 TWaveFormatEx = packed record // audio format
  wFormatTag: word;
  nChannels: word;
  nSamplesPerSec: dword;
  nAvgBytesPerSec: dword;
  nBlockAlign: word;
  wBitsPerSample: word;
  cbSize: word;
 end;
 
 TAVIHDR = packed record // this is a basic ready made avi header (video only atm)
  riff: TCHUNK;
  avi: FOURCC;

  list1: TCHUNK;

  hdrl: FOURCC;

  avihhdr: TCHUNK;
  avih: TAVIH;
 end;
 
 TIndexTableRec = packed record
  Offset: dword; // address in avi file of the jpeg image header
  Length: dword; // size of jpeg image data
  fcc: fourcc;
 end;
 
 TIndexTable = packed array[0..MaxIndexes - 1] of TIndexTableRec;
 PIndexTable = ^TIndexTable;
 
 TAVICreator = class(TObject)
 private
  FIndexes: integer;
  FIndexTable: PIndexTable;
  FOpened: boolean; // true if we have the .avi file open
  FUncFilename: string;
  FUncFile: file;
  FStreamBytes: Cardinal;
  FTotalFrames: Cardinal;
  FMicroSecPerFrame: Cardinal;
  FBitmapStream: TMemoryStream;
  FAviHdr: TAVIHDR;
  FMoviChunk: TChunk;
  FMoviStart: Cardinal;
  FLastJunkChunkAddr: Cardinal;
  FEndOfHeaderAddr: Cardinal;
  FVFormat: record // video format
   list: TCHUNK;
   strl: FOURCC;
   strhhdr: TCHUNK;
   strh: TSTRH;
   strfhdr: TCHUNK;
  end;
  FAFormat: record // audio format
   list: TCHUNK;
   strl: FOURCC;
   strhhdr: TCHUNK;
   strh: TSTRH;
   strfhdr: TCHUNK;
  end;
  procedure CleanExit;
  function AddJUNKChunk(Strg: string; MinimumSize: integer): boolean;
  function AddToIndexTable(Offset, Size: dword; fcc: fourcc): boolean;
  function AllocateIndexTable(NumIndexes: integer): boolean;
  function FreeIndexTable: boolean;
 public
  function CreateMJPEGfile(Filename: string;
   Width,
   Height,
   AMicroSecPerFrame: Cardinal;
   Quality: integer;
   Bitmap: TBitmap;
   AudioWaveFormat: pointer;
   AudioWaveFormatSize: Cardinal): boolean;
  function AddAudio(AudioData: pointer; AudioDataSize: dword): boolean;
  function AddBitmap(Bitmap: TBitmap): boolean;
  function CloseMJPEGFile(DeleteIt: boolean): boolean;
 end;
 
implementation

{$I-} // stop file errors popping up

function GetLastErrorStr(error: dword): string;
var
 lpMsgBuf: cardinal;
begin
 FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM,
  nil,
  error,
  (SUBLANG_DEFAULT shl 10) or LANG_NEUTRAL, // Default language
  @lpMsgBuf,
  0,
  nil);
 result := '[' + inttostr(error) + '] ' + pchar(lpMsgBuf);
 LocalFree(lpMsgBuf);
end;

{ TAVICreator }

function TAVICreator.AddAudio(AudioData: pointer; AudioDataSize: dword): boolean;
// Add a block of audio data
var
 ior: integer;
 BytesWrote: Cardinal;
 aBytes: integer;
 j: integer;
 i: Cardinal;
 Chunk: TChunk;
begin
 result := false;
 
 if (not FOpened) then
  exit;
 if (AudioData = nil) then
  exit;
 if (AudioDataSize = 0) then
  exit;
 
 //******************************
 // setup the audio chunk header
 
 Chunk.fcc := '01wb';
 Chunk.cb := AudioDataSize;
 
 //******************************
 // update our index
 
 i := FilePos(FUncFile);
 ior := ioresult;
 if (ior <> 0) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFilename);
  FreeIndexTable;
  exit;
 end;
 
 AddToIndexTable(i, Chunk.cb, Chunk.fcc); // update the index table
 
 //******************************
 
 aBytes := 0;
 
 //******************************
 // write the audio chunk header
 
 BlockWrite(FUncFile, Chunk, sizeof(Chunk), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(Chunk)) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFilename);
  FreeIndexTable;
  exit;
 end;
 inc(aBytes, BytesWrote);
 
 //******************************
 // save the audio data into the AVI file
 
 BlockWrite(FUncFile, AudioData^, Chunk.cb, BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> Chunk.cb) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFilename);
  FreeIndexTable;
  exit;
 end;
 inc(aBytes, BytesWrote);
 
 i := aBytes and $00000001;
 if (i > 0) then
 begin // next chunk must be on 2-byte boundry .. pad it with zeros
  j := 0;
  BlockWrite(FUncFile, j, i, BytesWrote);
  ior := ioresult;
  if (ior <> 0) or (BytesWrote <> i) then
  begin
   CloseMJPEGFile(true);
   DeleteFile(FUncFilename);
   FreeIndexTable;
   exit;
  end;
  inc(aBytes, BytesWrote);
 end;
 
 inc(FStreamBytes, aBytes);
 inc(FTotalFrames);
 
 i := FilePos(FUncFile);
 ior := ioresult;
 if (ior <> 0) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFileName);
  FreeIndexTable;
  exit;
 end;
 
 //*************
 // add a JUNK chunk is need be
 
 if ((i - FLastJUNKChunkAddr) > 20000) then
  if (not AddJUNKChunk('', 1)) then
   exit;
 
 //*************
 // check to see if we are near the file size limit
 
 if (i >= 2000000000) then
 begin // yes we are ! ... either start a new file or use the extended format
  Windows.Beep(1500, 5);
 end;
 
 result := true;
end;

function TAVICreator.AddBitmap(Bitmap: TBitmap): boolean;
// jpeg compress a TBitmap then add it to the avi file
var
 ior: integer;
 BytesWrote: Cardinal;
 jBytes: integer;
 j: integer;
 i: Cardinal;
 Chunk: TChunk;
 DIB: TDIBSection;
begin
 result := false;
 
 if (not FOpened) then
  exit;
 if (Bitmap = nil) then
  exit;
 if (FBitmapStream = nil) then
  exit;
 
 //******************************
 // compress the bitmap to a jpeg
 
 FBitmapStream.Position := 0;
 FBitmapStream.SetSize(0);
 
 FillChar(DIB, SizeOf(DIB), 0);
 if GetObject(Bitmap.Handle, SizeOf(DIB), @DIB) = 0 then
  Exit;
 
 FBitmapStream.WriteBuffer(DIB.dsBm.bmBits^, DIB.dsbmih.biSizeImage);
 
 //******************************
 // setup the jpeg chunk header
 
 Chunk.fcc := '00db';
 Chunk.cb := FBitmapStream.Position; // size of the jpeg image data
 
 //******************************
 // update our index
 
 i := FilePos(FUncFile);
 ior := ioresult;
 if (ior <> 0) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFilename);
  FreeIndexTable;
  exit;
 end;
 AddToIndexTable(i, Chunk.cb, Chunk.fcc); // update the index table
 jBytes := 0;
 //******************************
 // write the jpeg chunk header
 BlockWrite(FUncFile, Chunk, sizeof(Chunk), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(Chunk)) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFilename);
  FreeIndexTable;
  exit;
 end;
 inc(jBytes, BytesWrote);
 
 //******************************
 // save the jpeg image data into the AVI file
 
 BlockWrite(FUncFile, FBitmapStream.Memory^, Chunk.cb, BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> Chunk.cb) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFilename);
  FreeIndexTable;
  exit;
 end;
 inc(jBytes, BytesWrote);
 
 i := jBytes and $00000001;
 if (i > 0) then
 begin // next chunk must be on 2-byte boundry .. pad it with zeros
  j := 0;
  BlockWrite(FUncFile, j, i, BytesWrote);
  ior := ioresult;
  if (ior <> 0) or (BytesWrote <> i) then
  begin
   CloseMJPEGFile(true);
   DeleteFile(FUncFilename);
   FreeIndexTable;
   exit;
  end;
  inc(jBytes, BytesWrote);
 end;
 
 inc(FStreamBytes, jBytes);
 inc(FTotalFrames);
 
 i := FilePos(FUncFile);
 ior := ioresult;
 if (ior <> 0) then
 begin
  CloseMJPEGFile(true);
  DeleteFile(FUncFilename);
  FreeIndexTable;
  exit;
 end;
 
 // add a JUNK chunk is need be
 
 if ((i - FLastJUNKChunkAddr) > 20000) then
  if (not AddJUNKChunk('', 1)) then
   exit;
 
 // check to see if we are near the file size limit
 
 if (i >= 2000000000) then
 begin // yes we are ! ... either start a new file or use the extended format
  Windows.Beep(1500, 5);
 end;
 
 result := true;
end;

function TAVICreator.AddJUNKChunk(Strg: string; MinimumSize: integer): boolean;
// adds a JUNK chunk :)
var
 PrevErrorMode: Word;
 err: dword;
 BytesWrote, i, j: integer;
 ior: integer;
 Chunk: TCHUNK;
 Buffer: pchar;
 s: string;
begin
 result := false;
 
 i := length(Strg) + 1;
 if (i < MinimumSize) then
  i := MinimumSize;
 if (i < 0) then
 begin
  result := true;
  exit;
 end;
 
 j := (i + 1) and $FFFFFFFE; // 2-byte align
 
 Buffer := nil;
 PrevErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS); // ignore critical errors
 try
  GetMem(Buffer, j);
 except
  err := GetLastError;
  s := GetLastErrorStr(err);
  Buffer := nil;
 end;
 SetErrorMode(PrevErrorMode); // go back to previous error mode
 
 if (Buffer = nil) then
 begin
  result := true; // don't bother stopping if it's just memory we can't get
  exit;
 end;
 
 Chunk.fcc := 'JUNK';
 Chunk.cb := i;
 
 BlockWrite(FUncFile, Chunk, sizeof(Chunk), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(Chunk)) then
 begin
  CleanExit;
  PrevErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  try
   FreeMem(Buffer);
  finally
   Buffer := nil;
   SetErrorMode(PrevErrorMode);
  end;
  exit;
 end;
 
 fillchar(Buffer^, j, 0);
 StrPCopy(Buffer, Strg);
 
 BlockWrite(FUncFile, Buffer^, j, BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> j) then
 begin
  CleanExit;
  exit;
 end;
 
 FLastJunkChunkAddr := FilePos(FUncFile);
 ior := ioresult;
 if (ior <> 0) then
 begin
  CleanExit;
  exit;
 end;
 
 result := true;
end;

function TAVICreator.AddToIndexTable(Offset, Size: dword; fcc: fourcc): boolean;
// Add a new entry to the index table
// Offset if the image chunk address if the AVI file
// Size is the number of bytes to the image data
begin
 result := false;
 
 if (FIndexes >= MaxIndexes) then
 begin // we're full up :(
  CloseMJPEGFile(true);
  FreeIndexTable;
  Exit;
 end;
 
 FIndexTable[FIndexes].Offset := Offset;
 FIndexTable[FIndexes].Length := Size;
 FIndexTable[FIndexes].fcc := fcc;
 inc(FIndexes);
 
 result := true;
end;

function TAVICreator.AllocateIndexTable(NumIndexes: integer): boolean;
// Get the memory the index table needs
var
 PrevErrorMode: Word;
begin
 result := false;
 
 if (not FreeIndexTable) then
  exit;
 
 PrevErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS); // ignore critical errors
 try
  GetMem(FIndexTable, sizeof(TIndexTableRec) * NumIndexes);
 except
  FIndexTable := nil;
  FIndexes := 0;

  CloseMJPEGFile(true);
  FreeIndexTable;
 end;
 SetErrorMode(PrevErrorMode); // go back to previous error mode
 
 result := (FIndexTable <> nil);
end;

procedure TAVICreator.CleanExit;
begin
 CloseFile(FUncFile);
 DeleteFile(FUncFilename);
 FreeIndexTable;
end;

function TAVICreator.CloseMJPEGFile(DeleteIt: boolean): boolean;
// call this when your done
var
 PrevErrorMode: Word;
 ior, i: integer;
 BytesWrote: integer;
 Chunk: TChunk;
 Index: TIDX1;
 IndexStart: dword;
begin
 result := false;
 
 if (FBitmapStream <> nil) then
 begin
  PrevErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  try
   FBitmapStream.Free;
  finally
   FBitmapStream := nil;
   SetErrorMode(PrevErrorMode);
  end;
 end;
 
 if (not FOpened) then
 begin
  FreeIndexTable;
  exit;
 end;
 
 if (DeleteIt) then
 begin
  CleanExit;
  FOpened := false;
  exit;
 end;
 
 // add the index
 
 IndexStart := FilePos(FUncFile);
 ior := ioresult; // remember the current file position
 if (ior <> 0) then
 begin
  DeleteFile(FUncFilename);
  FreeIndexTable;
  FOpened := false;
  exit;
 end;
 
 if (FIndexes > 0) and (FIndexTable <> nil) then
 begin
  Chunk.fcc := 'idx1';
  Chunk.cb := sizeof(TIDX1) * FIndexes;

  BlockWrite(FUncFile, Chunk, sizeof(Chunk), BytesWrote);
  ior := ioresult;
  if (ior <> 0) or (BytesWrote <> sizeof(Chunk)) then
  begin
   CleanExit;
   FOpened := false;
   exit;
  end;

  i := 0;
  while (i < FIndexes) do
  begin
   Index.ckid := FIndexTable[i].fcc;
   Index.dwFlags := AVIIF_KEYFRAME;
   Index.dwChunkOffset := FIndexTable[i].Offset;
   Index.dwChunkLength := FIndexTable[i].Length;

   BlockWrite(FUncFile, Index, sizeof(Index), BytesWrote);
   ior := ioresult;
   if (ior <> 0) or (BytesWrote <> sizeof(Index)) then
   begin
    CleanExit;
    FOpened := false;
    exit;
   end;

   inc(i);
  end;
 end;
 
 // update the header
 
 if (FTotalFrames > 0) and (FMicroSecPerFrame > 0) then
 begin
  Seek(FUncFile, 0);
  ior := ioresult;
  if (ior <> 0) then
  begin
   CleanExit;
   FOpened := false;
   exit;
  end;

  i := FileSize(FUncFile);
  ior := ioresult; // get the entire file size
  if (ior <> 0) then
  begin
   CleanExit;
   FOpened := false;
   exit;
  end;

  FAviHdr.riff.cb := i - sizeof(FAviHdr.riff);

  FAviHdr.avih.dwMaxBytesPerSec := round(((FStreamBytes / FTotalFrames) * FVFormat.strh.dwRate) / FMicroSecPerFrame);
  FAviHdr.avih.dwTotalFrames := FTotalFrames;

  BlockWrite(FUncFile, FAviHdr, sizeof(FAviHdr), BytesWrote);
  ior := ioresult;
  if (ior <> 0) or (BytesWrote <> sizeof(FAviHdr)) then
  begin
   CleanExit;
   FOpened := false;
   exit;
  end;

  Seek(FUncFile, sizeof(FAviHdr));
  ior := ioresult;
  if (ior <> 0) then
  begin
   CleanExit;
   FOpened := false;
   exit;
  end;

  FVFormat.strh.dwLength := FTotalFrames;

  BlockWrite(FUncFile, FVFormat, sizeof(FVFormat), BytesWrote);
  ior := ioresult;
  if (ior <> 0) or (BytesWrote <> sizeof(FVFormat)) then
  begin
   CleanExit;
   FOpened := false;
   exit;
  end;
 end;
 
 // update the movi section
 
 FMoviChunk.cb := IndexStart - FMoviStart;
 
 Seek(FUncFile, FMoviStart - sizeof(FMoviChunk));
 ior := ioresult;
 if (ior <> 0) then
 begin
  CleanExit;
  FOpened := false;
  exit;
 end;
 
 BlockWrite(FUncFile, FMoviChunk, sizeof(FMoviChunk), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(FMoviChunk)) then
 begin
  CleanExit;
  FOpened := false;
  exit;
 end;
 CloseFile(FUncFile);
 FreeIndexTable;
 FOpened := false;
 result := true;
end;

function TAVICreator.CreateMJPEGfile(Filename: string; Width, Height,
 AMicroSecPerFrame: Cardinal; Quality: integer; Bitmap: TBitmap;
 AudioWaveFormat: pointer; AudioWaveFormatSize: Cardinal): boolean;
// call this to start with
var
 PrevErrorMode: Word;
 err: dword;
 ior: integer;
 BytesWrote: Cardinal;
 Chunk: TCHUNK;
 s: string;
 BitmapInfoHeaderSize: Cardinal;
 BitmapInfoHeader: ^TBitmapInfoHeader;
 ImageSize: Cardinal;
 Image: pointer;
begin
 result := false;
 
 CloseMJPEGFile(false); // just incase
 
 FUncFilename := Filename;
 FStreamBytes := 0;
 FTotalFrames := 0;
 FOpened := false;
 FMicroSecPerFrame := AMicroSecPerFrame;
 FBitmapStream := nil;
 FMoviStart := 0;
 FLastJunkChunkAddr := 0;
 
 if (Bitmap <> nil) then
 begin
  if (Bitmap.Width <= 0) then
   exit; // Doh !
  if (Bitmap.Height <= 0) then
   exit;
  if ((Bitmap.Width and 1) <> 0) then
   exit; // what have I said ????
  if ((Bitmap.Height and 1) <> 0) then
   exit; // ditto !
 end
 else
 begin
  if (Width <= 0) then
   exit; // Doh !
  if (Height <= 0) then
   exit;
  if ((Width and 1) <> 0) then
   exit; // uneven width
  if ((Height and 1) <> 0) then
   exit; // uneven height
 end;
 
 if (not AllocateIndexTable(MaxIndexes)) then
  exit; // get memory to hold the index table
 
 FileMode := fmOpenReadWrite;
 AssignFile(FUncFile, FUncFilename);
 ReWrite(FUncFile, 1);
 ior := ioresult;
 if (ior <> 0) then
  exit;
 
 // Build the header up
 
 fillchar(FAviHdr, sizeof(FAviHdr), 0); // wipe it clean to start with
 
 FAviHdr.riff.fcc := 'RIFF';
 FAviHdr.riff.cb := 0; // update this when finished .. the size of the entire file - 8
 FAviHdr.avi := 'AVI ';
 FAviHdr.list1.fcc := 'LIST';
 FAviHdr.list1.cb := 0; // we'll update this in a bit
 FAviHdr.hdrl := 'hdrl';
 FAviHdr.avihhdr.fcc := 'avih';
 FAviHdr.avihhdr.cb := sizeof(FAviHdr.avih);
 FAviHdr.avih.dwMicroSecPerFrame := FMicroSecPerFrame;
 FAviHdr.avih.dwMaxBytesPerSec := 0; // update this when finished
 FAviHdr.avih.dwPaddingGranularity := 0;
 FAviHdr.avih.dwFlags := AVIF_WASCAPTUREFILE or AVIF_HASINDEX;
 FAviHdr.avih.dwTotalFrames := 0; // update this when finished
 FAviHdr.avih.dwInitialFrames := 0;
 FAviHdr.avih.dwStreams := 1; // 1 = video only, 2 = video and audio
 FAviHdr.avih.dwSuggestedBufferSize := 0;
 if (Bitmap = nil) then
 begin
  FAviHdr.avih.dwWidth := Width;
  FAviHdr.avih.dwHeight := Height;
 end
 else
 begin
  FAviHdr.avih.dwWidth := Bitmap.Width;
  FAviHdr.avih.dwHeight := Bitmap.Height;
 end;
 
 BlockWrite(FUncFile, FAviHdr, sizeof(FAviHdr), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(FAviHdr)) then
 begin
  CleanExit;
  exit;
 end;
 
 // add the video format chunks
 
 FVFormat.list.fcc := 'LIST';
 FVFormat.list.cb := (sizeof(FVFormat) - 8);
 
 FVFormat.strl := 'strl';
 
 FVFormat.strhhdr.fcc := 'strh';
 FVFormat.strhhdr.cb := sizeof(FVFormat.strh);
 FVFormat.strh.fccType := 'vids';
 FVFormat.strh.fccHandler := CC_DIB;
 FVFormat.strh.dwFlags := 0;
 FVFormat.strh.wPriority := 0;
 FVFormat.strh.wLanguage := 0;
 FVFormat.strh.dwInitialFrames := 0;
 FVFormat.strh.dwScale := FMicroSecPerFrame;
 FVFormat.strh.dwRate := 1000;
 FVFormat.strh.dwStart := 0;
 FVFormat.strh.dwLength := 0; // update this when finished
 FVFormat.strh.dwSuggestedBufferSize := 0;
 FVFormat.strh.dwQuality := Quality * 100; // 0 to 10000 .. can just leave this at '0' or '-1' if you wish
 FVFormat.strh.dwSampleSize := 0;
 
 BitmapInfoHeaderSize := 0;
 BitmapInfoHeader := nil;
 ImageSize := 0;
 Image := nil;
 
 if (Bitmap = nil) then
 begin // fill the BitmapInfoHeader in ourself - assume it's a 24-bit rgb bitmap
  BitmapInfoHeaderSize := sizeof(TBitmapInfoHeader);
  GetMem(BitmapInfoHeader, BitmapInfoHeaderSize);
  if (BitmapInfoHeader = nil) then
  begin
   CleanExit;
   Exit;
  end;
  BitmapInfoHeader.biSize := sizeof(TBitmapInfoHeader);
  BitmapInfoHeader.biWidth := Width;
  BitmapInfoHeader.biHeight := Height;
  BitmapInfoHeader.biPlanes := 1;
  BitmapInfoHeader.biBitCount := 24;
  BitmapInfoHeader.biSizeImage := (Width * Height * 3 * BitmapInfoHeader.biPlanes);
  BitmapInfoHeader.biXPelsPerMeter := 0;
  BitmapInfoHeader.biYPelsPerMeter := 0;
  BitmapInfoHeader.biClrUsed := 0;
  BitmapInfoHeader.biClrImportant := 0;
 end
 else
 begin
  // create the TBitmapInfo
  GetDIBSizes(Bitmap.Handle, BitmapInfoHeaderSize, ImageSize);
  if (BitmapInfoHeaderSize = 0) or (ImageSize = 0) then
  begin
   CleanExit;
   exit;
  end;
  GetMem(BitmapInfoHeader, BitmapInfoHeaderSize);
  GetMem(Image, ImageSize);
  if (BitmapInfoHeader = nil) or (Image = nil) then
  begin
   CleanExit;
   if (BitmapInfoHeader <> nil) then
    FreeMem(BitmapInfoHeader);
   if (Image <> nil) then
    FreeMem(Image);
   exit;
  end;
  if (not GetDIB(Bitmap.Handle, Bitmap.Palette, BitmapInfoHeader^, Image^)) then
  begin
   if (BitmapInfoHeader <> nil) then
    FreeMem(BitmapInfoHeader);
   if (Image <> nil) then
    FreeMem(Image);
   CleanExit;
   exit;
  end;
 end;
 
 FVFormat.list.cb := FVFormat.list.cb + BitmapInfoHeaderSize;
 FVFormat.strfhdr.fcc := 'strf';
 FVFormat.strfhdr.cb := BitmapInfoHeaderSize;
 
 BlockWrite(FUncFile, FVFormat, sizeof(FVFormat), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(FVFormat)) then
 begin
  CleanExit;
  if (BitmapInfoHeader <> nil) then
   FreeMem(BitmapInfoHeader);
  if (Image <> nil) then
   FreeMem(Image);
  exit;
 end;
 
 BitmapInfoHeader.biCompression := BI_RGB;
 
 BlockWrite(FUncFile, BitmapInfoHeader^, BitmapInfoHeaderSize, BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> BitmapInfoHeaderSize) then
 begin
  CleanExit;
  if (BitmapInfoHeader <> nil) then
   FreeMem(BitmapInfoHeader);
  if (Image <> nil) then
   FreeMem(Image);
  exit;
 end;
 
 if (BitmapInfoHeader <> nil) then
  FreeMem(BitmapInfoHeader);
 if (Image <> nil) then
  FreeMem(Image);
 
 // add the audio format chunks
 
 if (AudioWaveFormat <> nil) and (AudioWaveFormatSize > 0) then
 begin
  FAFormat.list.fcc := 'LIST';
  FAFormat.list.cb := sizeof(FAFormat) - 8;

  FAFormat.strl := 'strl';

  FAFormat.strhhdr.fcc := 'strh';
  FAFormat.strhhdr.cb := sizeof(FAFormat.strh);
  FAFormat.strh.fccType := 'auds';
  FAFormat.strh.fccHandler := '    ';
  FAFormat.strh.dwFlags := 0;
  FAFormat.strh.wPriority := 0;
  FAFormat.strh.wLanguage := 0;
  FAFormat.strh.dwInitialFrames := 1;
  FAFormat.strh.dwScale := 1152; /// you'll have set this according
  FAFormat.strh.dwRate := TWaveFormatEx(AudioWaveFormat^).nSamplesPerSec;
  FAFormat.strh.dwStart := 0;
  FAFormat.strh.dwLength := 0; // update this when finished
  FAFormat.strh.dwSuggestedBufferSize := 0;
  FAFormat.strh.dwQuality := 0;
  FAFormat.strh.dwSampleSize := 0;

  FAFormat.list.cb := FAFormat.list.cb + AudioWaveFormatSize;
  FAFormat.strfhdr.fcc := 'strf';
  FAFormat.strfhdr.cb := AudioWaveFormatSize;

  BlockWrite(FUncFile, FAFormat, sizeof(FAFormat), BytesWrote);
  ior := ioresult;
  if (ior <> 0) or (BytesWrote <> sizeof(FAFormat)) then
  begin
   CleanExit;
   exit;
  end;

  BlockWrite(FUncFile, AudioWaveFormat^, AudioWaveFormatSize, BytesWrote);
  ior := ioresult;
  if (ior <> 0) or (BytesWrote <> AudioWaveFormatSize) then
  begin
   CleanExit;
   exit;
  end;
 end;
 
 FEndOfHeaderAddr := FilePos(FUncFile);
 ior := ioresult;
 if (ior <> 0) then
 begin
  CleanExit;
  exit;
 end;
 
 FAviHdr.list1.cb := FEndOFHeaderAddr - sizeof(FAviHdr.riff) - sizeof(FAviHdr.avi) - sizeof(FAviHdr.list1);
 
 FMoviChunk.fcc := 'LIST';
 FMoviChunk.cb := 0; // update this when finished
 
 BlockWrite(FUncFile, FMoviChunk, sizeof(FMoviChunk), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(FMoviChunk)) then
 begin
  CleanExit;
  exit;
 end;
 
 // add the 'movi' string
 
 FMoviStart := FilePos(FUncFile);
 ior := ioresult; // remember where we are in the file
 if (ior <> 0) then
 begin
  CleanExit;
  exit;
 end;
 
 Chunk.fcc := 'movi';
 
 BlockWrite(FUncFile, Chunk.fcc, sizeof(Chunk.fcc), BytesWrote);
 ior := ioresult;
 if (ior <> 0) or (BytesWrote <> sizeof(Chunk.fcc)) then
 begin
  CleanExit;
  exit;
 end;
 
 // create a memory stream - this is used to save the jpeg data into before inserting into the avi file
 
 PrevErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS); // ignore critical errors
 try
  FBitmapStream := TMemoryStream.Create;
 except
  err := GetLastError;
  s := GetLastErrorStr(err);
  FBitmapStream := nil;
 end;
 SetErrorMode(PrevErrorMode); // go back to previous error mode
 
 if (FBitmapStream = nil) then
 begin
  CleanExit;
  exit;
 end;
 FOpened := true;
 Result := true;
end;

function TAVICreator.FreeIndexTable: boolean;
// free the memory the index table has used
var
 PrevErrorMode: Word;
begin
 if (FIndexTable <> nil) then
 begin
  PrevErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS); // ignore critical errors
  try
   FreeMem(FIndexTable);
  finally
   FIndexTable := nil;
   FIndexes := 0;
   SetErrorMode(PrevErrorMode); // go back to previous error mode
  end;
 end
 else
  FIndexes := 0;
 Result := not Assigned(FIndexTable);
end;

end.

