unit webGetHTTP;

interface

uses
    syncommons,
    winapi.windows, winapi.wininet, system.sysutils, forms, dbseaconstants,
    strutils, system.Types, vcl.dialogs, mcklib,
    system.threading,
    vcl.controls,
    shellApi, system.UITypes, system.classes, myRegistry, eventLogging, nullable,
    OtlTask, OtlTaskControl;

type

    TMyVersion = record
        major,minor,release: integer;
    end;

    TWebGetHTTP = class(TObject)
    private
      class function  versionStringToMyVersion(const version:  string; out outV: TMyVersion): boolean;
      class procedure checkForNewVersion(const haveNofitiedStr: String; const forceShowNotification: boolean);
    public
      class procedure webGetData(const UserAgent: string; const URL: string; out success: boolean; out result: string);
      class function  checkForNewVersionInBackground(const forceShowNotification: boolean; const onTerminate: TOmniOnTerminatedFunction): IOmniTaskControl;
      class function  internetConnectionExists: boolean;
      class function  checkDB(const keySerialNumber, salt: string): string;
    end;

implementation


class function TWebGetHTTP.internetConnectionExists : boolean;
var
    origin : cardinal;
begin
    result := InternetGetConnectedState(@origin,0);

    //connections origins by origin value
    //NO INTERNET CONNECTION              = 0;
    //INTERNET_CONNECTION_MODEM           = 1;
    //INTERNET_CONNECTION_LAN             = 2;
    //INTERNET_CONNECTION_PROXY           = 4;
    //INTERNET_CONNECTION_MODEM_BUSY      = 8;
end;

class procedure TWebGetHTTP.webGetData(const UserAgent: string; const URL: string; out success: boolean; out result: string);
var
  hInet: HINTERNET;
  hURL: HINTERNET;
  Buffer: array[0..1023] of AnsiChar;
  BufferLen: cardinal;
begin
  success := false;
  result := '';
  hInet := InternetOpen(PChar(UserAgent), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if hInet = nil then RaiseLastOSError;
  try
    hURL := InternetOpenUrl(hInet, PChar(URL), nil, 0, 0, 0);
    //InternetGetLastResponseInfo();
    //if hURL = nil then RaiseLastOSError;
    if hURL = nil then exit;

    try
      repeat
        if not InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) then
          RaiseLastOSError;
        result := result + UTF8ToString(rawutf8(Copy(Buffer, 1, BufferLen)))
      until BufferLen = 0;
      success := true;
    finally
      InternetCloseHandle(hURL);
    end;
  finally
    InternetCloseHandle(hInet);
  end;
end;

class function  TWebGetHTTP.checkForNewVersionInBackground(const forceShowNotification: boolean; const onTerminate: TOmniOnTerminatedFunction): IOmniTaskControl;
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'checkForNewVersionInBackground');

    try
        if not internetConnectionExists then exit;

        //ITask(TTask.Create(procedure
        result := CreateTask( procedure(const task: IOmniTask)
        var
            haveNotifiedStr: String;
            date : TDateTime;
        begin
            reg.getHaveNotifiedNewVersionAvailable(haveNotifiedStr,date);
            TWebGetHTTP.checkForNewVersion(haveNotifiedStr, forceShowNotification);
        end).OnTerminated(onTerminate).run;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

class function TWebGetHTTP.checkDB(const keySerialNumber, salt: string): string;
var
    str: string;
    success: boolean;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'checkDB');

    try
        if not internetConnectionExists then exit;

        eventLog.log('Check license server');
        TWebGetHTTP.webGetData(application.title,sLicenseCheckDBURL + keySerialNumber + '/' + salt, success, str);
        if not success then exit; //no internet connection
        result := str;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

class procedure TWebGetHTTP.checkForNewVersion(const haveNofitiedStr: String; const forceShowNotification: boolean);
const
    needle = 'Latest dBSea version ';
var
    str, newVersionStr: string;
    q: integer;
    currentVersionStr: String;
    current, latest: TMyVersion;
    p: integer;
    success: boolean;

    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'checkForNewVersion');

    try
        TWebGetHTTP.webGetData(application.title, sLatestVersionURL, success, str);
        if not success then exit; //no internet connection

        //str := #13#10'other text '#13#10'other text Latest dBSea version: 1.1.2 other text other text';
        //str := '';

        q := AnsiPos(needle , str);
        if q = 0 then exit;

        currentVersionStr := fileVersion(application.ExeName);
        currentVersionStr := AnsiMidStr(currentVersionStr,2,20);
        newVersionStr := AnsiMidStr(str,q + length(needle), 20);
        //trim extra stuff from latestStr
        p := pos('-',newVersionStr);
        if p > 0 then
            setlength(newVersionStr,p - 1);

        //check if a message was already shown about this new version
        if (not forceShowNotification) and (AnsiCompareText(haveNofitiedStr,newVersionStr) = 0) then exit;

        if not TWebGetHTTP.versionStringToMyVersion(currentVersionStr, current) then exit;
        if not TWebGetHTTP.versionStringToMyVersion(newVersionStr, latest) then exit;

        if (latest.major > current.major) or
           ((latest.major = current.major) and (latest.minor > current.minor)) or
           ((latest.major = current.major) and (latest.minor = current.minor) and (latest.release > current.release)) then
        begin
            if messageDlg('A new version of dBSea is available: ' +
                         inttostr(latest.major) + '.' +
                         inttostr(latest.minor) + '.' +
                         inttostr(latest.release) + #13#10 +
                         'Open download page in browser?',
                         mtInformation ,
                         [mbYes,mbNo],
                         0) = mrYes then
            begin
                ShellExecute(0, 'OPEN', PChar(sDownloadLatestVersionURL), '', '', SW_SHOWNORMAL);
            end;
            reg.setHaveNotifiedNewVersionAvailable(newVersionStr,now());
        end
        else if forceShowNotification then
            ShowMessage('You are using the latest version');
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

class function TWebGetHTTP.versionStringToMyVersion(const version : string; out outV : TMyVersion) : boolean;
var
    strs: TStringDynarray;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'versionStringToMyVersion');

    try
        strs := StrUtils.SplitString(version,'. <-');
        if length(strs) < 3 then exit(false);

        outV.major := StrToInt(strs[0]);
        outV.minor := StrToInt(strs[1]);
        outV.release := StrToInt(strs[2]);
        result := true;
    except on e: Exception do
    begin
        eventLog.logException(log, e.ToString);
        exit(false);
    end;
    end;
end;


end.
