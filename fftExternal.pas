unit fftExternal;

interface

uses windows, dialogs, helperFunctions, math, basetypes, types, system.UITypes;

type
    TFFT = class
    private
        dBSeaCLib: Thandle;
        fnFFTInit: procedure (aNfft : integer); cdecl;
        fnFFTFree: procedure (); cdecl;
        fnFFTRealInput: procedure (reIn, reOut, imOut : pInteger); cdecl;
        fnFFT: procedure (reIn, imIn, reOut, imOut : pInteger); cdecl;
        fnIFFT: procedure (reIn, imIn, reOut, imOut : pInteger); cdecl;
    public
        nfft : integer;
        reIn, imIn, reOut, imOut : TIntegerDynArray;

        constructor Create(const aNfft : integer);
        destructor  Destroy; override;

        procedure setNfft(const aNfft : integer);
        procedure FFTReal;
        procedure FFT;
        procedure iFFT;

        function  f(const i : integer; const fs : double) : double;
        function  fArray(const fs : double) : TDoubleDynArray;
    end;

implementation


constructor TFFT.Create(const aNfft : integer);
const
    dllName : string = 'dBSeaCLib.dll';
begin
    inherited Create;

    if aNfft = 0 then exit;

    dBSeaCLib := LoadLibrary(PWideChar(dllName));
    if dBSeaCLib >= 32 then { success }
    begin
        fnFFTInit := GetProcAddress(dBSeaCLib, 'FFTInit');
        fnFFTFree := GetProcAddress(dBSeaCLib, 'FFTFree');
        fnFFTRealInput := GetProcAddress(dBSeaCLib, 'FFTRealInput');
        fnFFT := GetProcAddress(dBSeaCLib, 'FFT');
        fnIFFT := GetProcAddress(dBSeaCLib, 'iFFT');

        nfft := nextPow2(aNfft);
        if nfft < 4096 then
            nfft := 4096;
        if nfft > 32768 then
            nfft := 32768;

        fnFFTInit(nfft);

        setlength(reIn,nfft);
        setlength(imIn,nfft);
        setlength(reOut,nfft);
        setlength(imOut,nfft);
    end
    else
        messageDlg('Error: could not find ' + dllName, mtError, [mbOk], 0)
end;

destructor  TFFT.Destroy;
begin
    fnFFTFree;
    freeLibrary(dBSeaCLib);

    inherited;
end;

procedure TFFT.setNfft(const aNfft : integer);
begin
    if aNfft = nfft then exit;

    fnFFTFree;

    nfft := nextPow2(aNfft);
    if nfft < 4096 then
        nfft := 4096;
    if nfft > 32768 then
        nfft := 32768;

    fnFFTInit(nfft);

    setlength(reIn,nfft);
    setlength(imIn,nfft);
    setlength(reOut,nfft);
    setlength(imOut,nfft);
end;

procedure TFFT.FFTReal;
begin
    fnFFTRealInput(pInteger(reIn),pInteger(reOut),pInteger(imOut));
end;

procedure TFFT.FFT;
begin
    fnFFT(pInteger(reIn),pInteger(imIn),pInteger(reOut),pInteger(imOut));
end;

procedure TFFT.iFFT;
begin
    fnIFFT(pInteger(reIn),pInteger(imIn),pInteger(reOut),pInteger(imOut));
end;

function  TFFT.f(const i : integer; const fs : double) : double;
begin
    if i >= nfft then exit(nan);
    if i < 0 then exit(nan);

    if i <= nfft div 2 then
        result := fs * i / nfft
    else
        result := fs* (nfft - i) / nfft;
end;


function  TFFT.fArray(const fs : double) : TDoubleDynArray;
var
    i : integer;
begin
    for i := 0 to nfft div 2 do
        result[i] := fs * i / nfft;
    for i := (nfft div 2) + 1 to nfft - 1 do
        result[i] := result[nfft - i];
end;


end.
