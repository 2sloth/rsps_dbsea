unit MyShapefileWriter;

interface

uses System.SysUtils, system.types, System.json, ShpAPI129;

type

    TMyShape = record
        padfX, padfY, padfZ: TDoubleArray;
        level: double;

        function valid: boolean;
    end;
    TMyShapeArray = array of TMyShape;
    TMyShapeArrayHelper = record helper for TMyShapeArray
        function anyValid: boolean;
    end;
    TMyShapeArrayArray = array of TMyShapeArray;
    TMyShapeArrayArrayHelper = record helper for TMyShapeArrayArray
        function anyValid: boolean;
    end;

    TMyShapefileWriter = class(TObject)
        class procedure write(const filename: string; const shapess: TMyShapeArrayArray);
    end;

    TDoubleArrayHelper = record helper for TDoubleArray
        procedure setAllVal(const d: double);
    end;

implementation

{ TMyShapefileWriter }

class procedure TMyShapefileWriter.write(const filename: string; const shapess: TMyShapeArrayArray);
var
    handle: PSHPInfo;
    shape: TMyShape;
    shapes: TMyShapeArray;
    dbfinfo: PDBFInfo;
    levelFieldInd, shapeInd: integer;
    obj: PShpObject;
    i: integer;
begin
    handle := SHPCreate(PAnsiChar(ansistring(filename)), SHPT_POLYGON);
    dbfinfo := DBFCreate(PAnsiChar(ansistring(filename)));
    levelFieldInd := DBFAddField(dbfinfo, PAnsiChar(ansistring('level')), DBFFieldType.FTDouble, 5, 1);

    for shapes in shapess do
        for shape in shapes do
            if shape.valid then
            begin
                obj := SHPCreateSimpleObject(
                  SHPT_POLYGON,
                  length(shape.padfX),
                  @shape.padfX[0],
                  @shape.padfY[0],
                  @shape.padfZ[0]);

                for i := 0 to length(shape.padfX) - 1 do
                    obj.padfM[i] := shape.level;

                shapeInd := SHPWriteObject(handle, -1, obj);

                if levelFieldInd >= 0 then
                    DBFWriteDoubleAttribute(dbfinfo, shapeInd, levelFieldInd, shape.level);
            end;

    SHPClose(handle);
    DBFClose(dbfinfo);
end;

{ TMyShape }

function TMyShape.valid: boolean;
begin
    result := (length(padfX) > 0) and (length(padfX) = length(padfY)) and (length(padfX) = length(padfZ));
end;

{ TMyShapeArrayHelper }

function TMyShapeArrayHelper.anyValid: boolean;
var
    shape: TMyShape;
begin
    for shape in self do
        if shape.valid then
            exit(true);

    result := false;
end;

{ TDoubleArrayHelper }

procedure TDoubleArrayHelper.setAllVal(const d: double);
var
    i: integer;
begin
    for i := 0 to length(self) - 1 do
        self[i] := d;
end;

{ TMyShapeArrayArrayHelper }

function TMyShapeArrayArrayHelper.anyValid: boolean;
var
    shapes: TMyShapeArray;
begin
    for shapes in self do
        if shapes.anyValid then
            exit(true);

    result := false;
end;

end.
