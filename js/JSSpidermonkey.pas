unit JSSpidermonkey;

interface
uses jsintf;

type

  [JSClassName('App')]
  TJSApp = class(TJSClass)
  public
    var
    str: string;
    procedure getStr(const s: string);
  end;

  TJS = class(TObject)
  private
  const
    cryptoAES = '/*CryptoJS v3.1.2 code.google.com/p/crypto-js (c) 2009-2013 by Jeff Mott. All rights reserved. code.google.com/p/crypto-js/wiki/License */var CryptoJS=CryptoJS||function(u,p)' +
    '{var d={},l=d.lib={},s=function(){},t=l.Base={extend:function(a){s.prototype=this;var c=new s;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,ar' +
    'guments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a)a' +
    '.hasOwnProperty(c)&&(this[c]=a[c]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},r=l.WordArray=t.extend({init' +
    ':function(a,c){a=this.words=a||[];this.sigBytes=c!=p?c:4*a.length},toString:function(a){return(a||v).stringify(this)},concat:function(a){var c=this.words,e=a.words,j=this.sigBytes;' +
    'a=a.sigBytes;this.clamp();if(j%4)for(var k=0;k<a;k++)c[j+k>>>2]|=(e[k>>>2]>>>24-8*(k%4)&255)<<24-8*((j+k)%4);else if(65535<e.length)for(k=0;k<a;k+=4)c[j+k>>>2]=e[k>>>2];else c.push' +
    '.apply(c,e);this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<32-8*(c%4);a.length=u.ceil(c/4)},clone:function(){var a=t.clone.ca' +
    'll(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],e=0;e<a;e+=4)c.push(4294967296*u.random()|0);return new r.init(c,a)}}),w=d.enc={},v=w.Hex={stringify:' +
    'function(a){var c=a.words;a=a.sigBytes;for(var e=[],j=0;j<a;j++){var k=c[j>>>2]>>>24-8*(j%4)&255;e.push((k>>>4).toString(16));e.push((k&15).toString(16))}return e.join("")},parse:f' +
    'unction(a){for(var c=a.length,e=[],j=0;j<c;j+=2)e[j>>>3]|=parseInt(a.substr(j,2),16)<<24-4*(j%8);return new r.init(e,c/2)}},b=w.Latin1={stringify:function(a){var c=a.words;a=a.sigB' +
    'ytes;for(var e=[],j=0;j<a;j++)e.push(String.fromCharCode(c[j>>>2]>>>24-8*(j%4)&255));return e.join("")},parse:function(a){for(var c=a.length,e=[],j=0;j<c;j++)e[j>>>2]|=(a.charCodeA' +
    't(j)&255)<<24-8*(j%4);return new r.init(e,c)}},x=w.Utf8={stringify:function(a){try{return decodeURIComponent(escape(b.stringify(a)))}catch(c){throw Error("Malformed UTF-8 data");}}' +
    ',parse:function(a){return b.parse(unescape(encodeURIComponent(a)))}},q=l.BufferedBlockAlgorithm=t.extend({reset:function(){this._data=new r.init;this._nDataBytes=0},_append:functio' +
    'n(a){"string"==typeof a&&(a=x.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var c=this._data,e=c.words,j=c.sigBytes,k=this.blockSize,b=j/(4*k),b' +
    '=a?u.ceil(b):u.max((b|0)-this._minBufferSize,0);a=b*k;j=u.min(4*a,j);if(a){for(var q=0;q<a;q+=k)this._doProcessBlock(e,q);q=e.splice(0,a);c.sigBytes-=j}return new r.init(q,j)},clon' +
    'e:function(){var a=t.clone.call(this);a._data=this._data.clone();return a},_minBufferSize:0});l.Hasher=q.extend({cfg:t.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.re' +
    'set()},reset:function(){q.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doF' +
    'inalize()},blockSize:16,_createHelper:function(a){return function(b,e){return(new a.init(e)).finalize(b)}},_createHmacHelper:function(a){return function(b,e){return(new n.HMAC.init' +
    '(a,e)).finalize(b)}}});var n=d.algo={};return d}(Math);(function(){var u=CryptoJS,p=u.lib.WordArray;u.enc.Base64={stringify:function(d){var l=d.words,p=d.sigBytes,t=this._map;d.cla' +
    'mp();d=[];for(var r=0;r<p;r+=3)for(var w=(l[r>>>2]>>>24-8*(r%4)&255)<<16|(l[r+1>>>2]>>>24-8*((r+1)%4)&255)<<8|l[r+2>>>2]>>>24-8*((r+2)%4)&255,v=0;4>v&&r+0.75*v<p;v++)d.push(t.charA' +
    't(w>>>6*(3-v)&63));if(l=t.charAt(64))for(;d.length%4;)d.push(l);return d.join("")},parse:function(d){var l=d.length,s=this._map,t=s.charAt(64);t&&(t=d.indexOf(t),-1!=t&&(l=t));for(' +
    'var t=[],r=0,w=0;w<l;w++)if(w%4){var v=s.indexOf(d.charAt(w-1))<<2*(w%4),b=s.indexOf(d.charAt(w))>>>6-2*(w%4);t[r>>>2]|=(v|b)<<24-8*(r%4);r++}return p.create(t,r)},_map:"ABCDEFGHIJ' +
    'KLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="}})();(function(u){function p(b,n,a,c,e,j,k){b=b+(n&a|~n&c)+e+k;return(b<<j|b>>>32-j)+n}function d(b,n,a,c,e,j,k){b=b+(n&c|a' +
    '&~c)+e+k;return(b<<j|b>>>32-j)+n}function l(b,n,a,c,e,j,k){b=b+(n^a^c)+e+k;return(b<<j|b>>>32-j)+n}function s(b,n,a,c,e,j,k){b=b+(a^(n|~c))+e+k;return(b<<j|b>>>32-j)+n}for(var t=Cr' +
    'yptoJS,r=t.lib,w=r.WordArray,v=r.Hasher,r=t.algo,b=[],x=0;64>x;x++)b[x]=4294967296*u.abs(u.sin(x+1))|0;r=r.MD5=v.extend({_doReset:function(){this._hash=new w.init([1732584193,40232' +
    '33417,2562383102,271733878])},_doProcessBlock:function(q,n){for(var a=0;16>a;a++){var c=n+a,e=q[c];q[c]=(e<<8|e>>>24)&16711935|(e<<24|e>>>8)&4278255360}var a=this._hash.words,c=q[n' +
    '+0],e=q[n+1],j=q[n+2],k=q[n+3],z=q[n+4],r=q[n+5],t=q[n+6],w=q[n+7],v=q[n+8],A=q[n+9],B=q[n+10],C=q[n+11],u=q[n+12],D=q[n+13],E=q[n+14],x=q[n+15],f=a[0],m=a[1],g=a[2],h=a[3],f=p(f,m' +
    ',g,h,c,7,b[0]),h=p(h,f,m,g,e,12,b[1]),g=p(g,h,f,m,j,17,b[2]),m=p(m,g,h,f,k,22,b[3]),f=p(f,m,g,h,z,7,b[4]),h=p(h,f,m,g,r,12,b[5]),g=p(g,h,f,m,t,17,b[6]),m=p(m,g,h,f,w,22,b[7]),f=p(f' +
    ',m,g,h,v,7,b[8]),h=p(h,f,m,g,A,12,b[9]),g=p(g,h,f,m,B,17,b[10]),m=p(m,g,h,f,C,22,b[11]),f=p(f,m,g,h,u,7,b[12]),h=p(h,f,m,g,D,12,b[13]),g=p(g,h,f,m,E,17,b[14]),m=p(m,g,h,f,x,22,b[15' +
    ']),f=d(f,m,g,h,e,5,b[16]),h=d(h' +
    ',f,m,g,t,9,b[17]),g=d(g,h,f,m,C,14,b[18]),m=d(m,g,h,f,c,20,b[19]),f=d(f,m,g,h,r,5,b[20]),h=d(h,f,m,g,B,9,b[21]),g=d(g,h,f,m,x,14,b[22]),m=d(m,g,h,f,z,20,b[23]),f=d(f,m,g,h,A,5,b[24]),h' +
    '=d(h,f,m,g,E,9,b[25]),g=d(g,h,f,m,k,14,b[26]),m=d(m,g,h,f,v,20,b[27]),f=d(f,m,g,h,D,5,b[28]),h=d(h,f,m,g,j,9,b[29]),g=d(g,h,f,m,w,14,b[30]),m=d(m,g,h,f,u,20,b[31]),f=l(f,m,g,h,r,4,b[32' +
    ']),h=l(h,f,m,g,v,11,b[33]),g=l(g,h,f,m,C,16,b[34]),m=l(m,g,h,f,E,23,b[35]),f=l(f,m,g,h,e,4,b[36]),h=l(h,f,m,g,z,11,b[37]),g=l(g,h,f,m,w,16,b[38]),m=l(m,g,h,f,B,23,b[39]),f=l(f,m,g,h,D,' +
    '4,b[40]),h=l(h,f,m,g,c,11,b[41]),g=l(g,h,f,m,k,16,b[42]),m=l(m,g,h,f,t,23,b[43]),f=l(f,m,g,h,A,4,b[44]),h=l(h,f,m,g,u,11,b[45]),g=l(g,h,f,m,x,16,b[46]),m=l(m,g,h,f,j,23,b[47]),f=s(f,m,' +
    'g,h,c,6,b[48]),h=s(h,f,m,g,w,10,b[49]),g=s(g,h,f,m,E,15,b[50]),m=s(m,g,h,f,r,21,b[51]),f=s(f,m,g,h,u,6,b[52]),h=s(h,f,m,g,k,10,b[53]),g=s(g,h,f,m,B,15,b[54]),m=s(m,g,h,f,e,21,b[55]),f=' +
    's(f,m,g,h,v,6,b[56]),h=s(h,f,m,g,x,10,b[57]),g=s(g,h,f,m,t,15,b[58]),m=s(m,g,h,f,D,21,b[59]),f=s(f,m,g,h,z,6,b[60]),h=s(h,f,m,g,C,10,b[61]),g=s(g,h,f,m,j,15,b[62]),m=s(m,g,h,f,A,21,b[6' +
    '3]);a[0]=a[0]+f|0;a[1]=a[1]+m|0;a[2]=a[2]+g|0;a[3]=a[3]+h|0},_doFinalize:function(){var b=this._data,n=b.words,a=8*this._nDataBytes,c=8*b.sigBytes;n[c>>>5]|=128<<24-c%32;var e=u.floor(' +
    'a/4294967296);n[(c+64>>>9<<4)+15]=(e<<8|e>>>24)&16711935|(e<<24|e>>>8)&4278255360;n[(c+64>>>9<<4)+14]=(a<<8|a>>>24)&16711935|(a<<24|a>>>8)&4278255360;b.sigBytes=4*(n.length+1);this._pr' +
    'ocess();b=this._hash;n=b.words;for(a=0;4>a;a++)c=n[a],n[a]=(c<<8|c>>>24)&16711935|(c<<24|c>>>8)&4278255360;return b},clone:function(){var b=v.clone.call(this);b._hash=this._hash.clone(' +
    ');return b}});t.MD5=v._createHelper(r);t.HmacMD5=v._createHmacHelper(r)})(Math);(function(){var u=CryptoJS,p=u.lib,d=p.Base,l=p.WordArray,p=u.algo,s=p.EvpKDF=d.extend({cfg:d.extend({ke' +
    'ySize:4,hasher:p.MD5,iterations:1}),init:function(d){this.cfg=this.cfg.extend(d)},compute:function(d,r){for(var p=this.cfg,s=p.hasher.create(),b=l.create(),u=b.words,q=p.keySize,p=p.it' +
    'erations;u.length<q;){n&&s.update(n);var n=s.update(d).finalize(r);s.reset();for(var a=1;a<p;a++)n=s.finalize(n),s.reset();b.concat(n)}b.sigBytes=4*q;return b}});u.EvpKDF=function(d,l,' +
    'p){return s.create(p).compute(d,l)}})();CryptoJS.lib.Cipher||function(u){var p=CryptoJS,d=p.lib,l=d.Base,s=d.WordArray,t=d.BufferedBlockAlgorithm,r=p.enc.Base64,w=p.algo.EvpKDF,v=d.Cip' +
    'her=t.extend({cfg:l.extend(),createEncryptor:function(e,a){return this.create(this._ENC_XFORM_MODE,e,a)},createDecryptor:function(e,a){return this.create(this._DEC_XFORM_MODE,e,a)},ini' +
    't:function(e,a,b){this.cfg=this.cfg.extend(b);this._xformMode=e;this._key=a;this.reset()},reset:function(){t.reset.call(this);this._doReset()},process:function(e){this._append(e);retur' +
    'n this._process()},finalize:function(e){e&&this._append(e);return this._doFinalize()},keySize:4,ivSize:4,_ENC_XFORM_MODE:1,_DEC_XFORM_MODE:2,_createHelper:function(e){return{encrypt:fu' +
    'nction(b,k,d){return("string"==typeof k?c:a).encrypt(e,b,k,d)},decrypt:function(b,k,d){return("string"==typeof k?c:a).decrypt(e,b,k,d)}}}});d.StreamCipher=v.extend({_doFinalize:functio' +
    'n(){return this._process(!0)},blockSize:1});var b=p.mode={},x=function(e,a,b){var c=this._iv;c?this._iv=u:c=this._prevBlock;for(var d=0;d<b;d++)e[a+d]^=c[d]},q=(d.BlockCipherMode=l.ext' +
    'end({createEncryptor:function(e,a){return this.Encryptor.create(e,a)},createDecryptor:function(e,a){return this.Decryptor.create(e,a)},init:function(e,a){this._cipher=e;this._iv=a}})).' +
    'extend();q.Encryptor=q.extend({processBlock:function(e,a){var b=this._cipher,c=b.blockSize;x.call(this,e,a,c);b.encryptBlock(e,a);this._prevBlock=e.slice(a,a+c)}});q.Decryptor=q.extend' +
    '({processBlock:function(e,a){var b=this._cipher,c=b.blockSize,d=e.slice(a,a+c);b.decryptBlock(e,a);x.call(this,e,a,c);this._prevBlock=d}});b=b.CBC=q;q=(p.pad={}).Pkcs7={pad:function(a,' +
    'b){for(var c=4*b,c=c-a.sigBytes%c,d=c<<24|c<<16|c<<8|c,l=[],n=0;n<c;n+=4)l.push(d);c=s.create(l,c);a.concat(c)},unpad:function(a){a.sigBytes-=a.words[a.sigBytes-1>>>2]&255}};d.BlockCip' +
    'her=v.extend({cfg:v.cfg.extend({mode:b,padding:q}),reset:function(){v.reset.call(this);var a=this.cfg,b=a.iv,a=a.mode;if(this._xformMode==this._ENC_XFORM_MODE)var c=a.createEncryptor;e' +
    'lse c=a.createDecryptor,this._minBufferSize=1;this._mode=c.call(a,this,b&&b.words)},_doProcessBlock:function(a,b){this._mode.processBlock(a,b)},_doFinalize:function(){var a=this.cfg.pa' +
    'dding;if(this._xformMode==this._ENC_XFORM_MODE){a.pad(this._data,this.blockSize);var b=this._process(!0)}else b=this._process(!0),a.unpad(b);return b},blockSize:4});var n=d.CipherParam' +
    's=l.extend({init:function(a){this.mixIn(a)},toString:function(a){return(a||this.formatter).stringify(this)}}),b=(p.format={}).OpenSSL={stringify:function(a){var b=a.ciphertext;a=a.salt' +
    ';return(a?s.create([1398893684,1701076831]).concat(a).concat(b):b).toString(r)},parse:function(a){a=r.parse(a);var b=a.words;if(1398893684==b[0]&&1701076831==b[1]){var c=s.create(b.sli' +
    'ce(2,4));b.splice(0,4);a.sigBytes-=16}return n.create({ciphertext:a,salt:c})}},a=d.SerializableCipher=l.extend({cfg:l.extend({format:b}),encrypt:function(a,b,c,d){d=this.cfg.extend(d);' +
    'var l=a.createEncryptor(c,d);b=l.finalize(b);l=l.cfg;return n.create({ciphertext:b,key:c,iv:l.iv,algorithm:a,mode:l.mode,padding:l.padding,blockSize:a.blockSize,formatter:d.format})},d' +
    'ecrypt:function(a,b,c,d){d=this.cfg.extend(d);b=this._parse(b,d.format);return a.createDecryptor(c,d).finalize(b.ciphertext)},_parse:function(a,b){return"string"==typeof a?b.parse(a,th' +
    'is):a}}),p=(p.kdf={}).OpenSSL={execute:function(a,b,c,d){d||(d=s.random(8));a=w.create({keySize:b+c}).compute(a,d);c=s.create(a.words.slice(b),4*c);a.sigBytes=4*b;return n.create({key:' +
    'a,iv:c,salt:d})}},c=d.PasswordBasedCipher=a.extend({cfg:a.cfg.extend({kdf:p}),encrypt:function(b,c,d,l){l=this.cfg.extend(l);d=l.kdf.execute(d,b.keySize,b.ivSize);l.iv=d.iv;b=a.encrypt' +
    '.call(this,b,c,d.key,l);b.mixIn(d);return b},decrypt:function(b,c,d,l){l=this.cfg.extend(l);c=this._parse(c,l.format);d=l.kdf.execute(d,b.keySize,b.ivSize,c.salt);l.iv=d.iv;return a.de' +
    'crypt.call(this,b,c,d.key,l)}})}();(function(){for(var u=CryptoJS,p=u.lib.BlockCipher,d=u.algo,l=[],s=[],t=[],r=[],w=[],v=[],b=[],x=[],q=[],n=[],a=[],c=0;256>c;c++)a[c]=128>c?c<<1:c<<1' +
    '^283;for(var e=0,j=0,c=0;256>c;c++){var k=j^j<<1^j<<2^j<<3^j<<4,k=k>>>8^k&255^99;l[e]=k;s[k]=e;var z=a[e],F=a[z],G=a[F],y=257*a[k]^16843008*k;t[e]=y<<24|y>>>8;r[e]=y<<16|y>>>16;w[e]=y<' +
    '<8|y>>>24;v[e]=y;y=16843009*G^65537*F^257*z^16843008*e;b[k]=y<<24|y>>>8;x[k]=y<<16|y>>>16;q[k]=y<<8|y>>>24;n[k]=y;e?(e=z^a[a[a[G^z]]],j^=a[a[j]]):e=j=1}var H=[0,1,2,4,8,16,32,64,128,27' +
    ',54],d=d.AES=p.extend({_doReset:function(){for(var a=this._key,c=a.words,d=a.sigBytes/4,a=4*((this._nRounds=d+6)+1),e=this._keySchedule=[],j=0;j<a;j++)if(j<d)e[j]=c[j];else{var k=e[j-1' +
    '];j%d?6<d&&4==j%d&&(k=l[k>>>24]<<24|l[k>>>16&255]<<16|l[k>>>8&255]<<8|l[k&255]):(k=k<<8|k>>>24,k=l[k>>>24]<<24|l[k>>>16&255]<<16|l[k>>>8&255]<<8|l[k&255],k^=H[j/d|0]<<24);e[j]=e[j-d]^k' +
    '}c=this._invKeySchedule=[];for(d=0;d<a;d++)j=a-d,k=d%4?e[j]:e[j-4],c[d]=4>d||4>=j?k:b[l[k>>>24]]^x[l[k>>>16&255]]^q[l[k>>>8&255]]^n[l[k&255]]},encryptBlock:function(a,b){this._doCryptB' +
    'lock(a,b,this._keySchedule,t,r,w,v,l)},decryptBlock:function(a,c){var d=a[c+1];a[c+1]=a[c+3];a[c+3]=d;this._doCryptBlock(a,c,this._invKeySchedule,b,x,q,n,s);d=a[c+1];a[c+1]=a[c+3];a[c+' +
    '3]=d},_doCryptBlock:function(a,b,c,d,e,j,l,f){for(var m=this._nRounds,g=a[b]^c[0],h=a[b+1]^c[1],k=a[b+2]^c[2],n=a[b+3]^c[3],p=4,r=1;r<m;r++)var q=d[g>>>24]^e[h>>>16&255]^j[k>>>8&255]^l' +
    '[n&255]^c[p++],s=d[h>>>24]^e[k>>>16&255]^j[n>>>8&255]^l[g&255]^c[p++],t=d[k>>>24]^e[n>>>16&255]^j[g>>>8&255]^l[h&255]^c[p++],n=d[n>>>24]^e[g>>>16&255]^j[h>>>8&255]^l[k&255]^c[p++],g=q,' +
    'h=s,k=t;q=(f[g>>>24]<<24|f[h>>>16&255]<<16|f[k>>>8&255]<<8|f[n&255])^c[p++];s=(f[h>>>24]<<24|f[k>>>16&255]<<16|f[n>>>8&255]<<8|f[g&255])^c[p++];t=(f[k>>>24]<<24|f[n>>>16&255]<<16|f[g>>' +
    '>8&255]<<8|f[h&255])^c[p++];n=(f[n>>>24]<<24|f[g>>>16&255]<<16|f[h>>>8&255]<<8|f[k&255])^c[p++];a[b]=q;a[b+1]=s;a[b+2]=t;a[b+3]=n},keySize:8});u.AES=p._createHelper(d)})();';

//    cryptoRabbit = '/*CryptoJS v3.1.2 code.google.com/p/crypto-js (c) 2009-2013 by Jeff Mott. All rights reserved. code.google.com/p/crypto-js/wiki/License */ var CryptoJS=CryptoJS||function(q,k)' +
//    '{var e={},l=e.lib={},p=function(){},c=l.Base={extend:function(a){p.prototype=this;var b=new p;a&&b.mixIn(a);b.hasOwnProperty("init")||(b.init=function(){b.$super.init.apply(this,arguments)})' +
//    ';b.init.prototype=b;b.$super=this;return b},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var b in a)a.hasOwnProperty(b)&&' +
//    '(this[b]=a[b]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},s=l.WordArray=c.extend({init:function(a,b){a=this.words=a|' +
//    '|[];this.sigBytes=b!=k?b:4*a.length},toString:function(a){return(a||d).stringify(this)},concat:function(a){var b=this.words,m=a.words,n=this.sigBytes;a=a.sigBytes;this.clamp();if(n%4)for(var' +
//    ' r=0;r<a;r++)b[n+r>>>2]|=(m[r>>>2]>>>24-8*(r%4)&255)<<24-8*((n+r)%4);else if(65535<m.length)for(r=0;r<a;r+=4)b[n+r>>>2]=m[r>>>2];else b.push.apply(b,m);this.sigBytes+=a;return this},clamp:fu' +
//    'nction(){var a=this.words,b=this.sigBytes;a[b>>>2]&=4294967295<<32-8*(b%4);a.length=q.ceil(b/4)},clone:function(){var a=c.clone.call(this);a.words=this.words.slice(0);return a},random:functi' +
//    'on(a){for(var b=[],m=0;m<a;m+=4)b.push(4294967296*q.random()|0);return new s.init(b,a)}}),b=e.enc={},d=b.Hex={stringify:function(a){var b=a.words;a=a.sigBytes;for(var m=[],n=0;n<a;n++){var r' +
//    '=b[n>>>2]>>>24-8*(n%4)&255;m.push((r>>>4).toString(16));m.push((r&15).toString(16))}return m.join("")},parse:function(a){for(var b=a.length,m=[],n=0;n<b;n+=2)m[n>>>3]|=parseInt(a.substr(n,2)' +
//    ',16)<<24-4*(n%8);return new s.init(m,b/2)}},a=b.Latin1={stringify:function(a){var b=a.words;a=a.sigBytes;for(var m=[],n=0;n<a;n++)m.push(String.fromCharCode(b[n>>>2]>>>24-8*(n%4)&255));retur' +
//    'n m.join("")},parse:function(a){for(var b=a.length,m=[],n=0;n<b;n++)m[n>>>2]|=(a.charCodeAt(n)&255)<<24-8*(n%4);return new s.init(m,b)}},u=b.Utf8={stringify:function(b){try{return decodeURIC' +
//    'omponent(escape(a.stringify(b)))}catch(c){throw Error("Malformed UTF-8 data");}},parse:function(b){return a.parse(unescape(encodeURIComponent(b)))}},t=l.BufferedBlockAlgorithm=c.extend({rese' +
//    't:function(){this._data=new s.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=u.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var b=th' +
//    'is._data,m=b.words,n=b.sigBytes,r=this.blockSize,c=n/(4*r),c=a?q.ceil(c):q.max((c|0)-this._minBufferSize,0);a=c*r;n=q.min(4*a,n);if(a){for(var t=0;t<a;t+=r)this._doProcessBlock(m,t);t=m.spli' +
//    'ce(0,a);b.sigBytes-=n}return new s.init(t,n)},clone:function(){var a=c.clone.call(this);a._data=this._data.clone();return a},_minBufferSize:0});l.Hasher=t.extend({cfg:c.extend(),init:functio' +
//    'n(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){t.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&th' +
//    'is._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(b,m){return(new a.init(m)).finalize(b)}},_createHmacHelper:function(a){return function(b,m){re' +
//    'turn(new w.HMAC.init(a,m)).finalize(b)}}});var w=e.algo={};return e}(Math);(function(){var q=CryptoJS,k=q.lib.WordArray;q.enc.Base64={stringify:function(e){var l=e.words,p=e.sigBytes,c=this.' +
//    '_map;e.clamp();e=[];for(var k=0;k<p;k+=3)for(var b=(l[k>>>2]>>>24-8*(k%4)&255)<<16|(l[k+1>>>2]>>>24-8*((k+1)%4)&255)<<8|l[k+2>>>2]>>>24-8*((k+2)%4)&255,d=0;4>d&&k+0.75*d<p;d++)e.push(c.charA' +
//    't(b>>>6*(3-d)&63));if(l=c.charAt(64))for(;e.length%4;)e.push(l);return e.join("")},parse:function(e){var l=e.length,p=this._map,c=p.charAt(64);c&&(c=e.indexOf(c),-1!=c&&(l=c));for(var c=[],s' +
//    '=0,b=0;b<l;b++)if(b%4){var d=p.indexOf(e.charAt(b-1))<<2*(b%4),a=p.indexOf(e.charAt(b))>>>6-2*(b%4);c[s>>>2]|=(d|a)<<24-8*(s%4);s++}return k.create(c,s)},_map:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcd' +
//    'efghijklmnopqrstuvwxyz0123456789+/="}})();(function(q){function k(a,b,c,d,m,n,r){a=a+(b&c|~b&d)+m+r;return(a<<n|a>>>32-n)+b}function e(a,b,c,d,m,n,r){a=a+(b&d|c&~d)+m+r;return(a<<n|a>>>32-n)' +
//    '+b}function l(a,b,c,d,m,n,r){a=a+(b^c^d)+m+r;return(a<<n|a>>>32-n)+b}function p(a,b,c,d,m,n,r){a=a+(c^(b|~d))+m+r;return(a<<n|a>>>32-n)+b}for(var c=CryptoJS,s=c.lib,b=s.WordArray,d=s.Hasher,' +
//    's=c.algo,a=[],u=0;64>u;u++)a[u]=4294967296*q.abs(q.sin(u+1))|0;s=s.MD5=d.extend({_doReset:function(){this._hash=new b.init([1732584193,4023233417,2562383102,271733878])},_doProcessBlock:func' +
//    'tion(b,c){for(var d=0;16>d;d++){var s=c+d,m=b[s];b[s]=(m<<8|m>>>24)&16711935|(m<<24|m>>>8)&4278255360}var d=this._hash.words,s=b[c+0],m=b[c+1],n=b[c+2],r=b[c+3],x=b[c+4],u=b[c+5],q=b[c+6],y=' +
//    'b[c+7],z=b[c+8],A=b[c+9],B=b[c+10],C=b[c+11],D=b[c+12],E=b[c+13],F=b[c+14],G=b[c+15],f=d[0],g=d[1],h=d[2],j=d[3],f=k(f,g,h,j,s,7,a[0]),j=k(j,f,g,h,m,12,a[1]),h=k(h,j,f,g,n,17,a[2]),g=k(g,h,j' +
//    ',f,r,22,a[3]),f=k(f,g,h,j,x,7,a[4]),j=k(j,f,g,h,u,12,a[5]),h=k(h,j,f,g,q,17,a[6]),g=k(g,h,j,f,y,22,a[7]),f=k(f,g,h,j,z,7,a[8]),j=k(j,f,g,h,A,12,a[9]),h=k(h,j,f,g,B,17,a[10]),g=k(g,h,j,f,C,22' +
//    ',a[11]),f=k(f,g,h,j,D,7,a[12]),j=k(j,f,g,h,E,12,a[13]),h=k(h,j,f,g,F,17,a[14]),g=k(g,h,j,f,G,22,a[15]),f=e(f,g,h,j,m,5,a[16]),j=e(j,f,g,h,q,9,a[17]),h=e(h,j,f,g,C,14,a[18]),g=e(g,h,j,f,s,20,' +
//    'a[19]),f=e(f,g,h,j,u,5,a[20]),j=e(j,f,g,h,B,9,a[21]),h=e(h,j,f,g,G,14,a[22]),g=e(g,h,j,f,x,20,a[23]),f=e(f,g,h,j,A,5,a[24]),j=e(j,f,g,h,F,9,a[25]),h=e(h,j,f,g,r,14,a[26]),g=e(g,h,j,f,z,20,a[' +
//    '27]),f=e(f,g,h,j,E,5,a[28]),j=e(j,f,g,h,n,9,a[29]),h=e(h,j,f,g,y,14,a[30]),g=e(g,h,j,f,D,20,a[31]),f=l(f,g,h,j,u,4,a[32]),j=l(j,f,g,h,z,11,a[33]),h=l(h,j,f,g,C,16,a[34]),g=l(g,h,j,f,F,23,a[3' +
//    '5]),f=l(f,g,h,j,m,4,a[36]),j=l(j,f,g,h,x,11,a[37]),h=l(h,j,f,g,y,16,a[38]),g=l(g,h,j,f,B,23,a[39]),f=l(f,g,h,j,E,4,a[40]),j=l(j,f,g,h,s,11,a[41]),h=l(h,j,f,g,r,16,a[42]),g=l(g,h,j,f,q,23,a[4' +
//    '3]),f=l(f,g,h,j,A,4,a[44]),j=l(j,f,g,h,D,11,a[45]),h=l(h,j,f,g,G,16,a[46]),g=l(g,h,j,f,n,23,a[47]),f=p(f,g,h,j,s,6,a[48]),j=p(j,f,g,h,y,10,a[49]),h=p(h,j,f,g,F,15,a[50]),g=p(g,h,j,f,u,21,a[5' +
//    '1]),f=p(f,g,h,j,D,6,a[52]),j=p(j,f,g,h,r,10,a[53]),h=p(h,j,f,g,B,15,a[54]),g=p(g,h,j,f,m,21,a[55]),f=p(f,g,h,j,z,6,a[56]),j=p(j,f,g,h,G,10,a[57]),h=p(h,j,f,g,q,15,a[58]),g=p(g,h,j,f,E,21,a[5' +
//    '9]),f=p(f,g,h,j,x,6,a[60]),j=p(j,f,g,h,C,10,a[61]),h=p(h,j,f,g,n,15,a[62]),g=p(g,h,j,f,A,21,a[63]);d[0]=d[0]+f|0;d[1]=d[1]+g|0;d[2]=d[2]+h|0;d[3]=d[3]+j|0},_doFinalize:function(){var a=this.' +
//    '_data,b=a.words,c=8*this._nDataBytes,d=8*a.sigBytes;b[d>>>5]|=128<<24-d%32;var m=q.floor(c/4294967296);b[(d+64>>>9<<4)+15]=(m<<8|m>>>24)&16711935|(m<<24|m>>>8)&4278255360;b[(d+64>>>9<<4)+14]' +
//    '=(c<<8|c>>>24)&16711935|(c<<24|c>>>8)&4278255360;a.sigBytes=4*(b.length+1);this._process();a=this._hash;b=a.words;for(c=0;4>c;c++)d=b[c],b[c]=(d<<8|d>>>24)&16711935|(d<<24|d>>>8)&4278255360;' +
//    'return a},clone:function(){var a=d.clone.call(this);a._hash=this._hash.clone();return a}});c.MD5=d._createHelper(s);c.HmacMD5=d._createHmacHelper(s)})(Math);(function(){var q=CryptoJS,k=q.li' +
//    'b,e=k.Base,l=k.WordArray,k=q.algo,p=k.EvpKDF=e.extend({cfg:e.extend({keySize:4,hasher:k.MD5,iterations:1}),init:function(c){this.cfg=this.cfg.extend(c)},compute:function(c,e){for(var b=this.' +
//    'cfg,d=b.hasher.create(),a=l.create(),k=a.words,p=b.keySize,b=b.iterations;k.length<p;){q&&d.update(q);var q=d.update(c).finalize(e);d.reset();for(var v=1;v<b;v++)q=d.finalize(q),d.reset();a.' +
//    'concat(q)}a.sigBytes=4*p;return a}});q.EvpKDF=function(c,e,b){return p.create(b).compute(c,e)}})();CryptoJS.lib.Cipher||function(q){var k=CryptoJS,e=k.lib,l=e.Base,p=e.WordArray,c=e.Buffered' +
//    'BlockAlgorithm,s=k.enc.Base64,b=k.algo.EvpKDF,d=e.Cipher=c.extend({cfg:l.extend(),createEncryptor:function(a,b){return this.create(this._ENC_XFORM_MODE,a,b)},createDecryptor:function(a,b){re' +
//    'turn this.create(this._DEC_XFORM_MODE,a,b)},init:function(a,b,c){this.cfg=this.cfg.extend(c);this._xformMode=a;this._key=b;this.reset()},reset:function(){c.reset.call(this);this._doReset()},' +
//    'process:function(a){this._append(a);return this._process()},finalize:function(a){a&&this._append(a);return this._doFinalize()},keySize:4,ivSize:4,_ENC_XFORM_MODE:1,_DEC_XFORM_MODE:2,_createH' +
//    'elper:function(a){return{encrypt:function(b,c,d){return("string"==typeof c?H:v).encrypt(a,b,c,d)},decrypt:function(b,c,d){return("string"==typeof c?H:v).decrypt(a,b,c,d)}}}});e.StreamCipher=' +
//    'd.extend({_doFinalize:function(){return this._process(!0)},blockSize:1});var a=k.mode={},u=function(a,b,c){var d=this._iv;d?this._iv=q:d=this._prevBlock;for(var e=0;e<c;e++)a[b+e]^=d[e]},t=(' +
//    'e.BlockCipherMode=l.extend({createEncryptor:function(a,b){return this.Encryptor.create(a,b)},createDecryptor:function(a,b){return this.Decryptor.create(a,b)},init:function(a,b){this._cipher=' +
//    'a;this._iv=b}})).extend();t.Encryptor=t.extend({processBlock:function(a,b){var c=this._cipher,d=c.blockSize;u.call(this,a,b,d);c.encryptBlock(a,b);this._prevBlock=a.slice(b,b+d)}});t.Decrypt' +
//    'or=t.extend({processBlock:function(a,b){var c=this._cipher,d=c.blockSize,e=a.slice(b,b+d);c.decryptBlock(a,b);u.call(this,a,b,d);this._prevBlock=e}});a=a.CBC=t;t=(k.pad={}).Pkcs7={pad:functi' +
//    'on(a,b){for(var c=4*b,c=c-a.sigBytes%c,d=c<<24|c<<16|c<<8|c,e=[],k=0;k<c;k+=4)e.push(d);c=p.create(e,c);a.concat(c)},unpad:function(a){a.sigBytes-=a.words[a.sigBytes-1>>>2]&255}};e.BlockCiph' +
//    'er=d.extend({cfg:d.cfg.extend({mode:a,padding:t}),reset:function(){d.reset.call(this);var a=this.cfg,b=a.iv,a=a.mode;if(this._xformMode==this._ENC_XFORM_MODE)var c=a.createEncryptor;else c=a' +
//    '.createDecryptor,this._minBufferSize=1;this._mode=c.call(a,this,b&&b.words)},_doProcessBlock:function(a,b){this._mode.processBlock(a,b)},_doFinalize:function(){var a=this.cfg.padding;if(this' +
//    '._xformMode==this._ENC_XFORM_MODE){a.pad(this._data,this.blockSize);var b=this._process(!0)}else b=this._process(!0),a.unpad(b);return b},blockSize:4});var w=e.CipherParams=l.extend({init:fu' +
//    'nction(a){this.mixIn(a)},toString:function(a){return(a||this.formatter).stringify(this)}}),a=(k.format={}).OpenSSL={stringify:function(a){var b=a.ciphertext;a=a.salt;return(a?p.create([13988' +
//    '93684,1701076831]).concat(a).concat(b):b).toString(s)},parse:function(a){a=s.parse(a);var b=a.words;if(1398893684==b[0]&&1701076831==b[1]){var c=p.create(b.slice(2,4));b.splice(0,4);a.sigByt' +
//    'es-=16}return w.create({ciphertext:a,salt:c})}},v=e.SerializableCipher=l.extend({cfg:l.extend({format:a}),encrypt:function(a,b,c,d){d=this.cfg.extend(d);var e=a.createEncryptor(c,d);b=e.fina' +
//    'lize(b);e=e.cfg;return w.create({ciphertext:b,key:c,iv:e.iv,algorithm:a,mode:e.mode,padding:e.padding,blockSize:a.blockSize,formatter:d.format})},decrypt:function(a,b,c,d){d=this.cfg.extend(' +
//    'd);b=this._parse(b,d.format);return a.createDecryptor(c,d).finalize(b.ciphertext)},_parse:function(a,b){return"string"==typeof a?b.parse(a,this):a}}),k=(k.kdf={}).OpenSSL={execute:function(a' +
//    ',c,d,e){e||(e=p.random(8));a=b.create({keySize:c+d}).compute(a,e);d=p.create(a.words.slice(c),4*d);a.sigBytes=4*c;return w.create({key:a,iv:d,salt:e})}},H=e.PasswordBasedCipher=v.extend({cfg' +
//    ':v.cfg.extend({kdf:k}),encrypt:function(a,b,c,d){d=this.cfg.extend(d);c=d.kdf.execute(c,a.keySize,a.ivSize);d.iv=c.iv;a=v.encrypt.call(this,a,b,c.key,d);a.mixIn(c);return a},decrypt:function' +
//    '(a,b,c,d){d=this.cfg.extend(d);b=this._parse(b,d.format);c=d.kdf.execute(c,a.keySize,a.ivSize,b.salt);d.iv=c.iv;return v.decrypt.call(this,a,b,c.key,d)}})}();(function(){function q(){for(var' +
//    ' b=this._X,d=this._C,a=0;8>a;a++)p[a]=d[a];d[0]=d[0]+1295307597+this._b|0;d[1]=d[1]+3545052371+(d[0]>>>0<p[0]>>>0?1:0)|0;d[2]=d[2]+886263092+(d[1]>>>0<p[1]>>>0?1:0)|0;d[3]=d[3]+1295307597+(d' +
//    '[2]>>>0<p[2]>>>0?1:0)|0;d[4]=d[4]+3545052371+(d[3]>>>0<p[3]>>>0?1:0)|0;d[5]=d[5]+886263092+(d[4]>>>0<p[4]>>>0?1:0)|0;d[6]=d[6]+1295307597+(d[5]>>>0<p[5]>>>0?1:0)|0;d[7]=d[7]+3545052371+(d[6]' +
//    '>>>0<p[6]>>>0?1:0)|0;this._b=d[7]>>>0<p[7]>>>0?1:0;for(a=0;8>a;a++){var e=b[a]+d[a],k=e&65535,l=e>>>16;c[a]=((k*k>>>17)+k*l>>>15)+l*l^((e&4294901760)*e|0)+((e&65535)*e|0)}b[0]=c[0]+(c[7]<<16' +
//    '|c[7]>>>16)+(c[6]<<16|c[6]>>>16)|0;b[1]=c[1]+(c[0]<<8|c[0]>>>24)+c[7]|0;b[2]=c[2]+(c[1]<<16|c[1]>>>16)+(c[0]<<16|c[0]>>>16)|0;b[3]=c[3]+(c[2]<<8|c[2]>>>24)+c[1]|0;b[4]=c[4]+(c[3]<<16|c[3]>>>' +
//    '16)+(c[2]<<16|c[2]>>>16)|0;b[5]=c[5]+(c[4]<<8|c[4]>>>24)+c[3]|0;b[6]=c[6]+(c[5]<<16|c[5]>>>16)+(c[4]<<16|c[4]>>>16)|0;b[7]=c[7]+(c[6]<<8|c[6]>>>24)+c[5]|0}var k=CryptoJS,e=k.lib.StreamCipher' +
//    ',l=[],p=[],c=[],s=k.algo.Rabbit=e.extend({_doReset:function(){for(var b=this._key.words,c=this.cfg.iv,a=0;4>a;a++)b[a]=(b[a]<<8|b[a]>>>24)&16711935|(b[a]<<24|b[a]>>>8)&4278255360;for(var e=t' +
//    'his._X=[b[0],b[3]<<16|b[2]>>>16,b[1],b[0]<<16|b[3]>>>16,b[2],b[1]<<16|b[0]>>>16,b[3],b[2]<<16|b[1]>>>16],b=this._C=[b[2]<<16|b[2]>>>16,b[0]&4294901760|b[1]&65535,b[3]<<16|b[3]>>>16,b[1]&4294' +
//    '901760|b[2]&65535,b[0]<<16|b[0]>>>16,b[2]&4294901760|b[3]&65535,b[1]<<16|b[1]>>>16,b[3]&4294901760|b[0]&65535],a=this._b=0;4>a;a++)q.call(this);for(a=0;8>a;a++)b[a]^=e[a+4&7];if(c){var a=c.w' +
//    'ords,c=a[0],a=a[1],c=(c<<8|c>>>24)&16711935|(c<<24|c>>>8)&4278255360,a=(a<<8|a>>>24)&16711935|(a<<24|a>>>8)&4278255360,e=c>>>16|a&4294901760,k=a<<16|c&65535;b[0]^=c;b[1]^=e;b[2]^=a;b[3]^=k;b' +
//    '[4]^=c;b[5]^=e;b[6]^=a;b[7]^=k;for(a=0;4>a;a++)q.call(this)}},_doProcessBlock:function(b,c){var a=this._X;q.call(this);l[0]=a[0]^a[5]>>>16^a[3]<<16;l[1]=a[2]^a[7]>>>16^a[5]<<16;l[2]=a[4]^a[1' +
//    ']>>>16^a[7]<<16;l[3]=a[6]^a[3]>>>16^a[1]<<16;for(a=0;4>a;a++)l[a]=(l[a]<<8|l[a]>>>24)&16711935|(l[a]<<24|l[a]>>>8)&4278255360,b[c+a]^=l[a]},blockSize:4,ivSize:2});k.Rabbit=e._createHelper(s)' +
//    '})();';

    //passphraseAES = 'bgifrennvcgreflgfdsi';
    //passphraseRabbit = 'bgifrennvcgreflgfdsi' + {salty!}'a';
    //sDecryptAES = 'U2FsdGVkX19GTAjBnwOwqK3V3wIM7AqxImLzLLWtreT0Lua9y35K6P0zY1bEI0zRPTR1vL1gYAK3aAgcn6IPFcS90hBA55idGJSoL3bTY1oosD6D33CeVRIzFyEA8Kk4pmjQ/4rmK0xmsc4sq0kYdwgu/BovZO82bDPL7v6A7l1kd6eNr9Crg7uBpemXj8qC';
    //sDecryptRabbit = 'U2FsdGVkX19IsY++ll1qoeicSCya6ynbTMbviYvYvbwN9livmlmfCoe2QcyW8SChF0hS8WKPY18iyupOOgcIgzhQ2OVd5n/nm/EjQmv0YfrjI803bcizxLPSo86wSA088y5LIuNgCvaya8krn1igvvzyzI7i3wIfeMauz3wYJajGFAfWRqU=';

    passphrase = 'bgifrennvcgreflgfdsi';
  var
    FJSEngine: TJSEngine;
  public
    App: TJSApp;

    constructor Create;
    destructor Destroy; override;

    function  decrypt(const encrypted, salt: string): string;
  end;

var
    js: TJS;

implementation

constructor TJS.Create;
begin
    inherited;

    FJSEngine := TJSEngine.Create;
    App:= TJSApp.CreateJSObject(FJSEngine, 'App') ;
    FJSEngine.Evaluate(cryptoAES);
    //FJSEngine.Evaluate(cryptoRabbit);
end;

function TJS.decrypt(const encrypted, salt: string): string;
begin
    FJSEngine.Evaluate('try {App.getStr(CryptoJS.AES.decrypt("' + encrypted + '","' + passphrase + salt + '").toString(CryptoJS.enc.Utf8)); } catch(err) {App.getStr("")}');
    //FJSEngine.Evaluate('App.getStr(CryptoJS.Rabbit.decrypt("' + sDecryptRabbit + '","' + passphraseRabbit + '").toString(CryptoJS.enc.Utf8));');
    result := app.str;
end;

destructor TJS.Destroy;
begin
    FJSEngine.free;
    inherited;
end;

procedure TJSApp.getStr(const s: string);
begin
    self.str := s;
end;

initialization
    js := TJS.create;
finalization
    js.free;

end.

