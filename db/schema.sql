CREATE TABLE FISHtable (DataSource , ID INTEGER PRIMARY KEY, Name VARCHAR (255), Category1 VARCHAR (127), Category2 VARCHAR (127), Bandwidth VARCHAR (127), Comments BLOB, Picture BLOB, Level REAL, LO16 REAL, LO31_5 REAL, LO63 REAL, LO125 REAL, LO250 REAL, LO500 REAL, LO1k REAL, LO2k REAL, LO4k REAL, LO8k REAL, LO16k REAL, LO32k REAL, LO64k REAL, LO128k REAL, L3O12_5 REAL, L3O16 REAL, L3O20 REAL, L3O25 REAL, L3O31_5 REAL, L3O40 REAL, L3O50 REAL, L3O63 REAL, L3O80 REAL, L3O100 REAL, L3O125 REAL, L3O160 REAL, L3O200 REAL, L3O250 REAL, L3O315 REAL, L3O400 REAL, L3O500 REAL, L3O630 REAL, L3O800 REAL, L3O1k REAL, L3O1_2k REAL, L3O1_6k REAL, L3O2k REAL, L3O2_5k REAL, L3O3_2k REAL, L3O4k REAL, L3O5k REAL, L3O6_3k REAL, L3O8k REAL, L3O10k REAL, L3O12_5k REAL, L3O16k REAL, L3O20k REAL, L3O25k REAL, L3O32k REAL, L3O40k REAL, L3O50k REAL, L3O64k REAL, L3O80k REAL, L3O100k REAL, L3O128k REAL, L3O160k REAL, L3O200k REAL);
CREATE TABLE equiptable (
    "DataSource" TEXT,
    "ID" INTEGER,
    "Name" VARCHAR (255),
    "Category1" VARCHAR (127),
    "Category2" VARCHAR (127),
    "Bandwidth" VARCHAR (127),
    "Comments" BLOB,
    "Picture" BLOB,
    "Level" REAL,
    "L3O12_5" REAL,
    "L3O16" REAL,
    "L3O20" REAL,
    "L3O25" REAL,
    "L3O31_5" REAL,
    "L3O40" REAL,
    "L3O50" REAL,
    "L3O63" REAL,
    "L3O80" REAL,
    "L3O100" REAL,
    "L3O125" REAL,
    "L3O160" REAL,
    "L3O200" REAL,
    "L3O250" REAL,
    "L3O315" REAL,
    "L3O400" REAL,
    "L3O500" REAL,
    "L3O630" REAL,
    "L3O800" REAL,
    "L3O1k" REAL,
    "L3O1_2k" REAL,
    "L3O1_6k" REAL,
    "L3O2k" REAL,
    "L3O2_5k" REAL,
    "L3O3_2k" REAL,
    "L3O4k" REAL,
    "L3O5k" REAL,
    "L3O6_3k" REAL,
    "L3O8k" REAL,
    "L3O10k" REAL,
    "L3O12_5k" REAL,
    "L3O16k" REAL,
    "L3O20k" REAL,
    "L3O25k" REAL,
    "L3O32k" REAL,
    "L3O40k" REAL,
    "L3O50k" REAL,
    "L3O64k" REAL,
    "L3O80k" REAL,
    "L3O100k" REAL,
    "L3O128k" REAL,
    "L3O160k" REAL,
    "L3O200k" REAL,
    "RefDistance" REAL
);
CREATE TABLE watertable (
    "ID" INTEGER PRIMARY KEY NOT NULL,
    "Name" TEXT,
    "Normalised" TEXT,
    "Comments" TEXT,
    "Depths" TEXT,
    "SoundSpeed" TEXT
, "DataSource" TEXT);
CREATE TABLE sedimenttable (
    "ID" INTEGER PRIMARY KEY NOT NULL,
    "Name" TEXT,
    "Comments" TEXT,
    "DataSource" TEXT,
    "SoundSpeed" REAL,
    "Density" REAL,
    "Attenuation" REAL
);
CREATE TABLE sqlite_stat1(tbl,idx,stat);




CREATE TABLE EQUIPtable2 (
    "ID" INTEGER PRIMARY KEY NOT NULL,
	"DataSource" TEXT,
    "Name" TEXT,
    "Category1" TEXT,
    "Category2" TEXT,
    "Bandwidth" TEXT,
    "Comments" TEXT,
    "Picture" BLOB,
    "Level" REAL,
	"RefDistance" REAL,
	"RefSourceDepth" REAL,
    "DepthUnderRefSource" REAL,
	"LevelsSerial" TEXT,
	"FreqsSerial" TEXT,
    "L12_5" REAL,
    "L16" REAL,
    "L20" REAL,
    "L25" REAL,
    "L31_5" REAL,
    "L40" REAL,
    "L50" REAL,
    "L63" REAL,
    "L80" REAL,
    "L100" REAL,
    "L125" REAL,
    "L160" REAL,
    "L200" REAL,
    "L250" REAL,
    "L315" REAL,
    "L400" REAL,
    "L500" REAL,
    "L630" REAL,
    "L800" REAL,
    "L1k" REAL,
    "L1_2k" REAL,
    "L1_6k" REAL,
    "L2k" REAL,
    "L2_5k" REAL,
    "L3_2k" REAL,
    "L4k" REAL,
    "L5k" REAL,
    "L6_3k" REAL,
    "L8k" REAL,
    "L10k" REAL,
    "L12_5k" REAL,
    "L16k" REAL,
    "L20k" REAL,
    "L25k" REAL,
    "L32k" REAL,
    "L40k" REAL,
    "L50k" REAL,
    "L64k" REAL,
    "L80k" REAL,
    "L100k" REAL,
    "L128k" REAL,
    "L160k" REAL
);

CREATE TABLE FISHtable2 (
    "ID" INTEGER PRIMARY KEY NOT NULL,
	"DataSource" TEXT,
    "Name" TEXT,
    "Category1" TEXT,
    "Category2" TEXT,
    "Bandwidth" TEXT,
    "Comments" TEXT,
    "Picture" BLOB,
    "Level" REAL,
	"LevelsSerial" TEXT,
	"FreqsSerial" TEXT,
    "L12_5" REAL,
    "L16" REAL,
    "L20" REAL,
    "L25" REAL,
    "L31_5" REAL,
    "L40" REAL,
    "L50" REAL,
    "L63" REAL,
    "L80" REAL,
    "L100" REAL,
    "L125" REAL,
    "L160" REAL,
    "L200" REAL,
    "L250" REAL,
    "L315" REAL,
    "L400" REAL,
    "L500" REAL,
    "L630" REAL,
    "L800" REAL,
    "L1k" REAL,
    "L1_2k" REAL,
    "L1_6k" REAL,
    "L2k" REAL,
    "L2_5k" REAL,
    "L3_2k" REAL,
    "L4k" REAL,
    "L5k" REAL,
    "L6_3k" REAL,
    "L8k" REAL,
    "L10k" REAL,
    "L12_5k" REAL,
    "L16k" REAL,
    "L20k" REAL,
    "L25k" REAL,
    "L32k" REAL,
    "L40k" REAL,
    "L50k" REAL,
    "L64k" REAL,
    "L80k" REAL,
    "L100k" REAL,
    "L128k" REAL,
    "L160k" REAL
);

procedure TsourceSpectrumForm.btInitSQLClick(Sender: TObject);
var
    sltb        : TSQLIteTable;
    sSQL        : String;
    fi          : integer;
    OctSpect,ThirdOctSpect : TSpectrum;
    bw          : TBandwidth;
begin
    bw := prob.bandwidth;
    if sldb.TableExists(tblName) then
    begin
        sSQL := 'DROP TABLE ' + tblName;   //deletes any existing testtable
        sldb.execsql(sSQL);
    end;

    sSQL := 'CREATE TABLE ' + tblName + ' ([ID] INTEGER PRIMARY KEY';
    sSQL := sSQL + ', [Name] VARCHAR(MAX)';
    sSQL := sSQL + ', [Category1] VARCHAR(MAX)';
    sSQL := sSQL + ', [Category2] VARCHAR(MAX)';
    sSQL := sSQL + ', [Bandwidth] VARCHAR(MAX)';
    sSQL := sSQL + ', [Comments] VARCHAR(MAX)';
    sSQL := sSQL + ', [Picture] BLOB';
    sSQL := sSQL + ', [Datasource] VARCHAR(MAX)';
    sSQL := sSQL + ', [Level] REAL';
    //now add all the freq indices, however make sure to replace '.' with '_' as '.' causes problems for SQLite
    OctSpect := TSpectrum.create(Octave);
    ThirdOctSpect := TSpectrum.create(ThirdOctave);
    //for fi := 0 to OctSpect.getLength - 1 do
    //    sSQL := sSQL + ', [LO' + OctSpect.getFStringUnderscorebyInd(fi,bw) + '] REAL';
    for fi := 0 to ThirdOctSpect.getLength - 1 do
        sSQL := sSQL + ', [L3O' + ThirdOctSpect.getFStringUnderscorebyInd(fi,bw) + '] REAL';
    sSQL := sSQL + ');';
    sldb.execsql(sSQL);

    sldb.execsql('CREATE INDEX ' + tblName + 'Name ON [' + tblName + ']([Name]);');
end;

