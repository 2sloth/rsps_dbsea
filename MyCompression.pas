unit MyCompression;

interface

uses system.classes, system.types, system.sysutils,
    MemoryStreamHelper,
    mcklib,
    synlz, SynLZO,
    SmartPointer;

type

    TMyCompression = class(TObject)
        class function  compressTSingleDynArrayToBytes(const sa: TSingleDynArray): TBytes;
        class function  decompressTSingleDynArrayFromTBytes(const compressedBytes : TBytes): TSingleDynArray;
        class function  decompressTSingleDynArrayFromTBytesUsingZLib(const compressedBytes : TBytes) : TSingleDynArray;
    end;

implementation

class function TMyCompression.compressTSingleDynArrayToBytes(const sa: TSingleDynArray): TBytes;
var
    size, compressedSize: integer;
begin
    if length(sa) = 0 then exit;

    size := Length(sA) * SizeOf(single);
    setlength(result, SynLZcompressdestlen(size));
    compressedSize := SynLZcompress1pas(@sA[0], size, @result[0]);
    {//SynLZ seems to be faster than SynLZO, approx 3x faster on compress
    setlength(result, SynLZO.lzopas_compressdestlen(size));
    compressedSize := SynLZO.lzopas_compress(@sA[0], size, @result[0]);}
    setlength(result, compressedSize);
end;

class function TMyCompression.decompressTSingleDynArrayFromTBytes(const compressedBytes : TBytes) : TSingleDynArray;
begin
    if length(compressedBytes) = 0 then Exit;

    setlength(result, SynLZdecompressdestlen(@compressedBytes[0]) div sizeof(single));
    SynLZdecompress1pas(@compressedBytes[0], length(compressedBytes), @result[0]);
end;

class function TMyCompression.decompressTSingleDynArrayFromTBytesUsingZLib(const compressedBytes : TBytes) : TSingleDynArray;
var
    compressedStream,uncompressedStream: ISmart<TMemorystream>;
    targetLength : int64;
begin
    if length(compressedBytes) = 0 then Exit;

    compressedStream := TSmart<TMemorystream>.create(TMemorystream.create);
    compressedStream.Write(compressedBytes, Length(compressedBytes));
    uncompressedStream := TSmart<TMemorystream>.create(compressedStream.decompress());

    targetLength := uncompressedStream.Size div sizeof(single);
    if not enoughMemoryForAssignment(result,targetLength) then exit;
    setlength(result,uncompressedStream.Size div sizeof(single));
    uncompressedStream.Read(result[0],uncompressedStream.Size);
end;


end.
