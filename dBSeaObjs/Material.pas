unit material;

{$B-}

interface

uses system.sysutils, system.rtti, system.classes, system.types, system.json,
    xml.xmldom, xml.XMLIntf,
    DBSeaObject, objectCopier, baseTypes,
    helperFunctions;

type

{$RTTI INHERIT}
    TMaterialRec = record
      var
        c, rho, attenuation: double;

        function rho_g: double;
    end;

    TMaterial = class(TDBSeaObject)
        { class for the different sediment materials. just bulk properties available.
          c=speed of sound (m/s)
          rho=density (kg/m3)
          attenuation is in dB/wavelength for ramgeo, bellhop and kraken (kraken actually ignores it. krakenc uses it)
          the 10 and 20 log solvers ignore the sediment completely.}
    private
    var
        fName       : string;
        fComments   : string;
        fID         : integer;
        fDataSource : TDataSource;  //either MDA or User

        fC, fRho, fAttenuation: double;
    public
        constructor Create(const aName: string; aC, aRho, aAttenuation: double; aID: integer; aDataSource: TDataSource);
        procedure cloneFrom(const source: TMaterial);

        procedure setProps(const aC, aRho, aAttenuation: double);
        function rho_g: double;

        class function water: TMaterialRec;
        function toRec: TMaterialRec;

        procedure setWithListAndWeightings(const arr: TArray<TMaterial>; const weightings: TDoubleDynArray);

        property name: string read fName write fName;
        property comments: string read fComments write fComments;
        property dataSource: TDataSource read fDataSource write fDataSource;
        property id: integer read fID write fID;

        property c: double read fC;
        property rho: double read fRho;
        property attenuation: double read fAttenuation;

        class function createFromJSON(v: TJSONValue): TMaterial;
    end;

var
    water: TMaterial;

implementation

constructor TMaterial.Create(const aName: string; aC, aRho, aAttenuation: double; aID: integer; aDataSource: TDataSource);
begin
    inherited Create;

    self.SaveBlacklist := '';

    {create a material given the 3 necessary properties. comments field leave blank for now}
    fName := aName;
    setProps(aC,aRho,aAttenuation);
    fComments := '';
    fID := aID;
    fDataSource := aDataSource;

    THelper.forceReferenceToClass(TMaterial);
end;

class function TMaterial.createFromJSON(v: TJSONValue): TMaterial;
var
    o: TJSONObject;
begin
    try
        o := TJSONObject(v);

        result := TMaterial.create('',0,0,0,0,TDataSource.dsUser);

        if not assigned(o.getValue('c')) or not tryStrToFloat(o.getValue('c').ToString, result.fc) then
            raise EJSONParsing.Create('Cannot parse material c');

        if not assigned(o.getValue('density')) or not tryStrToFloat(o.getValue('density').ToString, result.frho) then
            raise EJSONParsing.Create('Cannot parse material density');

        if not assigned(o.getValue('attenuation')) or not tryStrToFloat(o.getValue('attenuation').ToString, result.fattenuation) then
            raise EJSONParsing.Create('Cannot parse material attenuation');

        if assigned(o.getValue('name')) then
            o.getValue('name').TryGetValue<string>(result.fName);
        if assigned(o.getValue('comments')) then
            o.getValue('comments').TryGetValue<string>(result.fComments);

    except on e: Exception do
        exit(nil);
    end;
end;

procedure TMaterial.cloneFrom(const source : TMaterial);
begin
    TObjectCopier.CopyObject(source,self);
end;

procedure TMaterial.setProps(const aC, aRho, aAttenuation : double);
begin
    self.fC := aC;
    self.fRho := aRho;
    self.fAttenuation := aAttenuation;
end;

procedure TMaterial.setWithListAndWeightings(const arr: TArray<TMaterial>; const weightings: TDoubleDynArray);
var
    i: integer;
begin
    fC := 0;
    fRho := 0;
    fAttenuation := 0;

    for i := 0 to length(arr) - 1 do
    begin
        fc := fc + weightings[i] * arr[i].fc;
        fRho := fRho + weightings[i] * arr[i].fRho;
        fAttenuation := fAttenuation + weightings[i] * arr[i].fAttenuation;
    end;
end;

function TMaterial.toRec: TMaterialRec;
begin
    result.c := self.c;
    result.rho := self.rho;
    result.attenuation := self.attenuation;
end;

function  TMaterial.rho_g : double;
begin
    result := rho / 1000.0; //convert to g/cm3
end;

class function TMaterial.water: TMaterialRec;
begin
    result.c := 1500.0;
    Result.rho := 1000.0;
    Result.attenuation := 0.0;
end;

{ TMaterialRec }

function TMaterialRec.rho_g: double;
begin
    result := rho / 1000; //convert to g/cm3
end;

initialization
    water := TMaterial.create('water',1500,1000,0,0,TDataSource.dsMDA);
finalization
    water.free;

end.
