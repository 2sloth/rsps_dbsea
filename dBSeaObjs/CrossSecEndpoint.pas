unit CrossSecEndpoint;

{$B-}
interface

uses system.sysutils,
    vcl.graphics,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, objectCopier, baseTypes, pos3, helperFunctions;

type

{$RTTI INHERIT}
    TCrossSecEndpoint = class(TDBSeaObject)
        { class for the two endpoints which define the cross section. mostly needed
        so that we can easily set colours etc}
    private
    var
        fVisible   : boolean; // queried on graphics draw. this is set true and never altered, but let's keep it in case we want it in future
        fPos       : TPos3;
        fColor     : TColor;
        fAlpha     : double; // opacity
        fEnabled   : boolean;
    public
    const
        Origcolor: TColor = $1EC8C8;
        AltColor: TColor = $FF141E ;
        DisabledColor: TColor = $00463C;
    var
        constructor Create;
        destructor Destroy; override;
        procedure cloneFrom(const source: TCrossSecEndpoint);
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;


        function  setPos(const aX, aY: double): boolean;
        //procedure setColor1(const aCol: TColor);
        function  setColor(const aCol: TColor): boolean;
        procedure reset;

        property  Visible: boolean read fVisible write fVisible;
        property  Pos: TPos3 read fPos;
        property  Color: TColor read fColor;
        property  Alpha: Double read fAlpha write fAlpha;
        property  Enabled: boolean read fEnabled write fEnabled;
    End;

implementation

constructor TCrossSecEndpoint.Create;
begin
    inherited Create;

    self.SaveBlacklist := 'fPos';

    self.fPos := TPos3.Create(0, 0, 0); //Z is ignored
    self.reset;
end;

destructor TCrossSecEndpoint.Destroy;
begin
    fPos.Free;

    inherited;
end;

procedure TCrossSecEndpoint.cloneFrom(const source: TCrossSecEndpoint);
begin
    TObjectCopier.CopyObject(source,self);

    Pos.cloneFrom(source.Pos);
end;


//procedure TCrossSecEndpoint.setPos(const aX, aY: double);
//var
    //RA: TReceiverArea;
    //tmpX, tmpY: double;
//begin
    {check that the probe is only being moved within the RA and not outside it -
    the forcerange function makes sure it is within those bounds}
    //tmpX := aX;
    //tmpY := aY;
    //forceRange(tmpX, prob.theRA.Pos1.X, prob.theRA.Pos2.X);
    //forceRange(tmpY, prob.theRA.Pos1.Y, prob.theRA.Pos2.Y);
    //self.Pos.setPos(tmpX, tmpY, 0);
//end;

function TCrossSecEndpoint.setPos(const aX, aY: double): boolean;
begin
    {it is expected that the positions given are already checked by the caller}
    result := self.Pos.setPos(aX, aY, 0);
end;

procedure TCrossSecEndpoint.reset;
begin
    self.fPos.setPos(0, 0, 0); //Z is ignored
    self.fColor := self.origcolor;
    self.alpha := 1.0;
    self.visible := true;
    self.Enabled := true;
end;


//procedure TCrossSecEndpoint.setColor1(const aCol: TColor);
//begin
//    if Color <> aCol then
//    begin
//        fColor := aCol;
//        if assigned(GLWin) then
//            GLWin.setRedraw; //forcedrawnow causes flashing
//    end;
//end;

function TCrossSecEndpoint.setColor(const aCol: TColor): boolean;
begin
    //return true if the color has changed, and therefore the GLWin needs to redraw
    result := false;
    if Color <> aCol then
    begin
        fColor := aCol;
        result := true;
    end;
end;

procedure TCrossSecEndpoint.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;
    if not assigned(parentNode) then exit;

    self.Pos.writeDataToXMLNode(parentNode.AddChild('Pos'));
end;

procedure TCrossSecEndpoint.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    childnode := (parentNode as IDOMNodeSelect).selectNode('Pos');
    if assigned(childnode) then
        self.Pos.readDataFromXMLNode(childNode);
end;

end.
