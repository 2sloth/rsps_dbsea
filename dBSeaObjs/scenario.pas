﻿unit scenario;

{$B-}
interface

uses syncommons,
    system.Diagnostics, system.Math, system.Classes, system.SysUtils, system.strutils, system.types,
    vcl.Dialogs, vcl.Forms, vcl.stdctrls,
    Winapi.Windows, Winapi.Messages,
    Generics.Collections,
    xml.xmldom, xml.XMLIntf,
    dBSeaColours, baseTypes, spectrum, fishCurve,
    helperFunctions, dBSeaObject, objectCopier, srcPoint, material, waterProps,
    crossSecEndpoint, probe, soundSource, receiverArea, MCKLib, CSVDelimiters,
    dBSeaconstants, seafloorScheme, flock, levelConverter, soundSpeedProfile,
    eventlogging, globalsUnit, solveTimeEstimator,
    commentUnit, bathymetry, crossSec, BathymetryInterface,
    solveReturn, nullable, SmartPointer, ProbeTimeSeries,
    LocationProperties, RunExecutable, myshapefilewriter,
    marchingSquares, pos3;

type

{$RTTI INHERIT}
    {$SCOPEDENUMS ON}
    TSolveReturnVal = (NoError,ErrorGoToSourcePage,ErrorGoToSetupPage);
    {$SCOPEDENUMS OFF}

    IScenarioDelegate = interface(IInterface)
      function  isCurrentScenario(const scen: TDBSeaObject): boolean;
      function  getEasting: double;
      function  getNorthing: double;
      function  diagonalLength3D(const worldscale: double): double;
      function  worldscale: double;
      function  makeXString(const aVal: double): String;
      function  makeYString(const aVal: double): String;
      function  getCrossSec: TCrossSec;
      function  getPCancelSolve: TWrappedBool;
      function  getXMax: double;
      function  getYMax: double;
      function  getWorldscale: double;
      function  g2w(const a: double): double;
    end;

    TScenario = class(TdBSeaObject, IReceiverAreaDelegate, ISourceDelegate, IFlockDelegate)
    private
        fName                  : string;
        fComments              : string;
        fStartfi               : integer;
        fEndfi                 : integer; // freq indices and length
        fDMax                  : integer; //deepest depth index (at deepest location, ie index 0 is at surface
        fSourcesNSlices        : integer;
        fSourcesIMax           : integer;
        fBandwidth             : TBandwidth; // master problem bandwidth
        fSplitSolver           : boolean; //if yes, then split solvers between low/high freqs
        fSplitSolverfi         : integer; //highest index for low freq solver, when f > fSplitSolverFreqInd, use high freq solver
        fSolver                : TCalculationMethod;
        fLowSolver             : TCalculationMethod;
        fHighSolver            : TCalculationMethod;
        fUserSolverLogPart     : double;
        fUserSolverLinPart     : double;
        //fSingleSeafloorScheme  : boolean;
        //fWater                 : TWaterProps; // current water properties
        fWeighting             : TFishcurve; //current weighting spectrum
        fWeightingInd          : integer; //current index to the fishcurve DB table
        fSolveEnabled          : boolean;
        //fCancelSolve           : boolean;
        fDBSeaModesMaxModes    : integer;
        fBellhopNRays          : integer;
        fBellhopNRaysAzimuth : integer;
        fNFreqsPerBand         : integer;
        fRayTracingCoherent    : boolean;
        fRayTracingCalculateAttenuationAtEachStep: boolean;
        fRayTracingStartAngle  : double;
        fRayTracingEndAngle    : double;
        fDBSeaRayMaxBottomReflections: integer;
        fDBSeaPEzOversampling  : double;
        fDBSeaPErOversampling  : double;
        fDBSeaPEPhiOversampling: double;
        fKrakenCoupled         : boolean;
        fNaNSeafloorLevels     : boolean;
        fiSmoothTLRadial       : integer;
        fISmoothDistToExlusionZone: integer;
        fMakeTLMonotonic       : boolean;
        fCoherent              : boolean;
        fLevelType             : TLevelType;
        fAssessmentPeriod      : double;

        //fExclusionZoneLevel    : double;
        //fExclusionZoneCleared  : boolean;
        fMovingSourcesUseGriddedTL   : boolean;

        delegate: IScenarioDelegate;
        bathyDelegate: IBathymetryDelegate;

        function  isCurrent: boolean;
        procedure setSmoothDistToExclusionZone(aVal: integer);

        function  getAssessmentPeriod(): double;
        procedure setAssessmentPeriod(const d: double);
        procedure updateWeighting(const curve: TFishCurve);
        procedure setSourcesUseGriddedTL(const val: boolean);
        function  getpCancelSolve: TWrappedBool;
        function  formatOutputLevelFunc(const lev: single; const sNaN: string): string;
        function  allSourcesSpectrumAllNan(const startFi, lenFi: integer): boolean;
        function  levelsString(const thisZInd: integer; const thisLevelsDisplay: TLevelsDisplay; const sNaN: string; const cDelim: char): string;

        //delegate methods
        function  getSources: TSourceList;
        function  getCoherent: boolean;
        function  getStartfi: integer;
        function  getEndfi: integer;
        function  getBandwidth: TBandwidth;
        function  getLenfi: integer;
        function  getWeighting: TFishCurve;
        function  getDepths(const d: integer): double;
        function  getdz: double;
        function  getdmax: integer;
        function  getWater: TWaterProps;
        function  getSeabed: TSeafloorScheme;
        function  getRayTracingStartAngle: double;
        function  getRayTracingEndAngle: double;
        function  getRayTracingInitialStepsize: double;
        function  getDBSeaRayMaxBottomReflections: integer;
        function  getBellhopNRays: integer;
        function  getBellhopNRaysAzimuth: integer;
        function  getSplitSolver: boolean;
        function  getdBSeaModesMaxModes: integer;
        function  getdBSeaModesZOversampling: double;
        function  getdBSeaModesrOversampling: double;
        function  getdBSeaPEZOversampling: double;
        function  getdBSeaPErOversampling: double;
        function  getdBSeaPEPhiOversampling: double;
        function  getSolver: TCalculationMethod;
        function  getLowSolver: TCalculationMethod;
        function  getHighSolver: TCalculationMethod;
        function  getMakeTLMonotonic: boolean;
        function  getiSmoothTLRadial: integer;
        function  getLevelType: TLevelType;
        function  getMovingSourcesUseGriddedTL: boolean;
        function  getMaxSourceMovingTime: double;
        function  getLevAtInds(const d, i, j, p: integer; const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean): single;
        function  levelsDifferBetweenTimes(const time1, time2: double): boolean;
        function  getFieldAtTime(const time: double): TDoubleField;
        function  getSeafloorNaNs: boolean;
        function  getStopMarchingSolverAtLand: boolean;
        function  getGridIMax: integer;
        function  getGridJMax: integer;
        function  getXMax: double;
        function  getyMax: double;
        function  getStartTime: TDateTime;
        function  getCProfilesAtAngle3(const aPos: TFPoint3; const aAngle, rmax, dr: double): TDepthValArrayAtRangeArray;
        function  getLocationProps(const aPos: TFPoint3; const aAngle, rmax, dr: double): TLPRangeList;
        function  getSeabedsAtAngle(const aPos: TFPoint3; const aAngle, rmax, dr: double): TSeafloorRangeArray;
    function ESRIHeaderForLevels(const showErrorMessages: boolean): string;
    public
    const
        dBSeaPEzOversamplingMax = 40.0;
        dBSeaPErOversamplingMax = 2.5;
    var
        sources: TSourceList;
        //seabed: TSeabed;
        //ssp: TSSP;
        theRA: TReceiverArea;
        flock: TFlock;
        comments: TCommentList;
        rayTracingInitialStepsize: double; //meters
        nPadeTerms, nPadeTermsHorizontal: integer;
        stopMarchingSolverAtLand: boolean;
        dBSeaModeszOversampling, dBSeaModesrOversampling: double;
        startTime: TDateTime;
        propsMap: TLocationPropertiesList;
        raySolverIterationMethod: TRaySolverIterationMethod;
        tide: double;

        constructor Create(const aDelegate: IScenarioDelegate; const aBathyDelegate: IBathymetryDelegate);
        destructor Destroy(); Override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode; const srcLevels: TStringArray; const sSaveVersion: string; const scenInd: integer; const stream: TMemoryStream); reintroduce;
        procedure cloneFrom(const source: TScenario);

        procedure setName(const aName: String);
        procedure createRAAndSource();
        procedure initSampleScenario();
        procedure debugInit(); //only used when initialising the scenario when debugging

        function  printInfo (const cDelim: char; const maxSourceCount, maxProbeCount: integer): TStringlist;
        function  solversText: String;
        procedure readSourcesFromFile(const sCSVFile: String);
        function  getCProfileAtAngle(const aAngle: double): TMatrix;
        function  getCProfileAtAngle2(const aAngle: double): TDepthValArray;
        procedure scaleSourcePositions(const scaleby: double);
        function  convertLevel(const lev: single): single;
        function  unconvertLevel(const lev: single): single;
        function  formatLevel(const lev: single): string;
        function  formatLevelForSource(const src: TSource): string;
        function  anySourceMoving: boolean;
        function  numberOfMovingSources: integer;

        procedure resetSourcesandRAs();
        procedure resetCProfile();
        function  SourceNameMatch(const s: string): boolean;
        function  addSource(const atX: double = nan; const atY: double = nan; const atZ: double = nan): TSource;
        procedure deleteSource(const ind: integer);
        function  lenfi(): integer;
        procedure setWeightingInd(const aVal: integer; const curve: TFishCurve);
        function  setSmoothTLRadial(aVal: integer): boolean;
        procedure setLevelType(const levType: TLevelType);

        procedure setstartfi(const val: integer);
        procedure setendfi(const val: integer);
        procedure setSplitSolverfi(const val: integer);
        procedure setbandwidth(const val: TBandwidth);
        function  setMakeTLMonotonic(const aVal: boolean): boolean;
        function  resultsExist(): boolean;

        function  headerForExport(const cDelim: char): string;
        procedure writeLevelsToFile(const filename: String; const cDelim: char; const bWriteSpectra: boolean);
        procedure writeXSecLevelsToFile(const filename: String; const cDelim: char; const bWriteSpectra: boolean);
        procedure writeMaxLevelsToFile(const filename: String; const cDelim: char);
        procedure writeLevelsToESRIFile(const filename: String; const thisZInd: integer; const thisLevelsDisplay: TLevelsDisplay; const showErrorMessages: boolean);
        procedure writeExclusionZoneToESRIFile(const filename: String);
        procedure writeExclusionZoneToShapefile(const filename: String; const src: TSource);
        procedure writeContoursToShapefile(const filename: String);

        procedure setTheRAToEntireArea();
        procedure checkSourcesInRA();
        function  weightingIsNone(): boolean;

        function  solveNow(const scenarioIndex: integer; const return: TSolveReturn; const solveTimeEstimator: TSolveTimeEstimator; const lbSolving,lbSolveExtraInfo,lbSliceNumber,lbSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const setProgressBars: TProcedureWithDouble; const mmSolverMessages: TMemo; const updateRemainingSolveTime: TProcedureWithBoolBool): TSolveReturnVal;

        function  exclusionZoneAtPos(const exclusionZoneLevel: single; const src: TSource; const movingPosIndex, soundFieldAtMovingPosIndex: integer; const stopAtLand: boolean; const onlyForSource: boolean = false): TExclusionZonePointArray;
        procedure setDMax(const aVal: integer; const resizeNow: boolean = true);
        procedure setSourcesNSlices(const aVal: integer; const resizeNow: boolean = true);
        procedure setSourcesIMax(const aVal: integer; const resizeNow: boolean = true);
        function  estimateMemUsage(): int64;

        function  depths(const ind: integer): double;
        function  depthsArray(): TDoubleDynArray;
        function  dz(): double;
        function  maxSourceMovingTime(): double;
        function  maxSourceMovingPos(): integer;
        procedure clearLevelsCache;
        function  canShowProbeTimeSeries: boolean;

        //procedure addDefaultSeafloorScheme;
        //function  getSeafloorSchemeAtPoint(const aX,aY: double): TSeafloorScheme;
        function  topLayerMaterial(const aX,aY: double): TMaterial;
        procedure commentsForFrame(const frame: TFrame; const commentsList: TCommentlist);
        //procedure readCompressedSourceLevels(const scenInd: integer; const stream: TMemoryStream);
        procedure writeCompressedSourceLevels(const scenInd: integer; const stream: TMemoryStream);

        //-------------------------------------------------------------------------
        property  name: string read fName write setName;
        property  comment: string read fComments write fComments;
        property  dMax: integer read fDMax;
        property  sourcesNSlices: integer read fSourcesNSlices;
        property  sourcesIMax: integer read fSourcesIMax;
        property  bandwidth: TBandwidth read fBandwidth;
        property  startfi: integer read fstartfi;
        property  endfi: integer read fendfi;
        property  splitSolver: boolean read fSplitSolver write fSplitSolver;
        property  splitSolverfi: integer read fSplitSolverfi write fSplitSolverfi;
        property  solver: TCalculationMethod read fSolver write fSolver;
        property  lowSolver: TCalculationMethod read fLowSolver write fLowSolver;
        property  highSolver: TCalculationMethod read fHighSolver write fHighSolver;
        property  userSolverLogPart: double read fUserSolverLogPart write fUserSolverLogPart;
        property  userSolverLinPart: double read fUserSolverLinPart write fUserSolverLinPart;
        //property  water: TWaterProps read fWater write fWater;
        property  weighting: TFishcurve read fWeighting;
        property  weightingInd: integer read fWeightingInd;
        property  iSmoothTLRadial: integer read fiSmoothTLRadial;
        property  iSmoothDistToExlusionZone: integer read fISmoothDistToExlusionZone write setSmoothDistToExclusionZone;
        property  makeTLMonotonic: boolean read fMakeTLMonotonic;
        //property  singleSeafloorScheme: boolean read fSingleSeafloorScheme write fSingleSeafloorScheme;
        property  coherent: boolean read fCoherent write fCoherent;

        property  solveEnabled: boolean read fSolveEnabled write fSolveEnabled;
        property  pcancelSolve: TWrappedBool read getpCancelSolve;
        property  bellhopNRays: integer read fBellhopNRays write fBellhopNRays;
        property  bellhopNRaysAzimuth: integer read fBellhopNRaysAzimuth write fBellhopNRaysAzimuth;
        property  dBSeaModesMaxModes: integer read fDBSeaModesMaxModes write fDBSeaModesMaxModes;
        property  nOversampleFreqsPerBand: integer read fNFreqsPerBand write fNFreqsPerBand;
        property  dBSeaRayMaxBottomReflections: integer read fDBSeaRayMaxBottomReflections write fDBSeaRayMaxBottomReflections;
        property  rayTracingCoherent: boolean read fRayTracingCoherent write fRayTracingCoherent;
        property  rayTracingCalculateAttenuationAtEachStep: boolean read fRayTracingCalculateAttenuationAtEachStep write fRayTracingCalculateAttenuationAtEachStep;
        property  rayTracingStartAngle: double read fRayTracingStartAngle write fRayTracingStartAngle;
        property  rayTracingEndAngle: double read fRayTracingEndAngle write fRayTracingEndAngle;
        property  dBSeaPEzOversampling: double read fDBSeaPEZOversampling write fDBSeaPEZOversampling;
        property  dBSeaPErOversampling: double read fDBSeaPEROversampling write fDBSeaPEROversampling;
        property  dBSeaPEPhiOversampling: double read fDBSeaPEPhiOversampling write fDBSeaPEPhiOversampling;
        property  normalModesCoupledModes: boolean read fKrakenCoupled write fKrakenCoupled;
        property  NANSeafloorLevels: boolean read fNaNSeafloorLevels write fNaNSeafloorLevels;
        property  levelType: TLevelType read flevelType;// write setLevelType;
        property  assessmentPeriod: double  read getAssessmentPeriod write setAssessmentPeriod;
        property  movingSourcesUseGriddedTL: boolean read fMovingSourcesUseGriddedTL write setSourcesUseGriddedTL;
    end;
    TScenarioList = class(TMyObjectList<TScenario>)
        procedure writeDataToXMLNode(const parentNode: IXMLNode);
        procedure readDataFromXMLNode(const parentNode: IDOMNode; const srcLevels: TStringArray; const sSaveVersion: string; const scenInd: integer; const stream: TMemoryStream);
    end;

implementation

uses dBSeaOptions, GLWindow;

constructor TScenario.Create(const aDelegate: IScenarioDelegate;
                             const aBathyDelegate: IBathymetryDelegate);
var
    log: ISynlog;
begin
    inherited Create;

    log := TSynlogLevs.Enter(self,'Create');
    eventLog.log('Create scenario');

    self.SaveBlacklist := 'delegate,sourceBathyDelegate,sources' +
                            ',seafloorSchemes,theRA,fSediment' +
                            ',fWater,fCriteria,fWeighting' +
                            ',flock,seabed,ssp,comments,propsMap' +
                            ',bathyDelegate';

    self.delegate := aDelegate;
    self.bathyDelegate := aBathyDelegate;
    fName := 'Scenario';
    fComments := '';

    Sources := TSourceList.Create(true);  //(true) means the list owns the objects and frees them when deleting
    //seabed := TSeabed.create();
    //self.fSingleSeafloorScheme := true;

    {$IFDEF DEBUG}
    self.setDMax(40);
    self.setSourcesIMax(40);
    self.setSourcesNSlices(36);
    {$ELSE}
    self.setDMax(50);
    self.setSourcesIMax(100);
    self.setSourcesNSlices(100);
    {$ENDIF}

    fiSmoothTLRadial := 2;
    fISmoothDistToExlusionZone := 1;

    {create flat cprofile at 1500 m/s}
    //ssp := TSSP.create;
    //ssp.reset(bathyDelegate.getzMax);

    {create the spectrum we will be analysing at}
    setBandwidth(bwOctave);
    {$IFDEF DEBUG}
    fstartfi := 1;
    fendfi := 1;
    {$ELSE}
    fstartfi := 1;
    fendfi := 5;
    {$ENDIF}

    {choose solvers etc}
    SplitSolver         := false;
    {$IFDEF DEBUG}
    SplitSolverfi       := 2;
    {$ELSE}
    SplitSolverfi       := 3;
    {$ENDIF}

    {$IFDEF DEBUG}
    //Solver              := cmdBSeaRay;
    solver              := cm10log;
    {$ELSEIF Defined(LITE)}
    solver              := cm10log;
    {$ELSE}
    Solver              := cm10log;
    {$ENDIF}
    LowSolver           := cmdBSeaModes;
    HighSolver          := cmdBSeaRay;

    fUserSolverLogPart      := 15;
    fUserSolverLinPart      := 0;

    fLevelType              := kSPL;
    fAssessmentPeriod       := 3600.0;
    fBellhopNRays           := 0; // 0 = default, let solver choose
    fBellhopNRaysAzimuth    := 0; // 0 = default, let solver choose
    nOversampleFreqsPerBand := 1;
    DBSeaModesMaxModes      := 0;
    DBSeaRayMaxBottomReflections := 1000;
    fSolveEnabled           := true;
    //CancelSolve             := false;
    normalModesCoupledModes := false; //adiabatic by default
    nanSeafloorLevels       := true;
    fMakeTLMonotonic        := true;
    fCoherent               := false;
    fMovingSourcesUseGriddedTL    := true;
    frayTracingCoherent      := true;
    fRayTracingCalculateAttenuationAtEachStep := false;
    fRayTracingStartAngle   := -89;
    fRayTracingEndAngle     := 89;
    fdBSeaPEZOversampling   := 4;
    fdBSeaPEROversampling   := 0.5;
    fDBSeaPEPhiOversampling := 1;
    dBSeaModeszOversampling := 1;
    dBSeaModesrOversampling := 2;
    rayTracingInitialStepsize := 5;
    tide := 0;
    nPadeTerms := 0;
    nPadeTermsHorizontal := 0;
    stopMarchingSolverAtLand := true;
    raySolverIterationMethod := kConstSSP;

    //addDefaultSeafloorScheme;
    // TODO should probably grab sediment and water from sldb
    //Water := TWaterProps.Create('Default',8,35,0,TDataSource.dsMDA);

    propsMap := TLocationPropertiesList.create(true);
    propsMap.add(TLocationProperties.create(self.bathyDelegate.getzmax));
    propsMap.selected := propsMap.First;
    propsMap.selected.name := 'Default';

    {create the weighting spectrum, all 0. Can't use the fishedit frame yet as it is created after the problemdescription.
    Make sure to use bwThirdOctave}
    fWeightingInd := 0;
    fWeighting := TFishCurve.Create('None',bwThirdOctave,0,TDataSource.dsMDA);
    fWeighting.Spectrum.zeroAmp;

    startTime := now;

    theRA := TReceiverArea.Create(self, bathyDelegate);

    addSource; //won't work on intial create due to no container object existing

    flock := TFlock.Create(self,self.bathyDelegate);
    comments := TCommentList.create(true);
end;

destructor TScenario.Destroy;
begin
    comments.free;
    flock.Free;
    //Sources.Clear;
    Sources.Free;
    //seabed.free;
    //fWater.Free;
    //ssp.Free;
    fWeighting.Free;
    theRA.Free;
    propsMap.free;

    inherited;
end;

procedure TScenario.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    inherited;
    if not assigned(parentNode) then exit;

    //Water.writeDataToXMLNode(parentNode.AddChild('WaterProps'));
    //ssp.writeDataToXMLNode(parentNode.AddChild('SSP'));

    writeIntegerField(parentNode,'SourcesCount',sources.Count);
    ChildNode := writeListField(parentNode,'SourcesList');
    for i := 0 to Sources.Count - 1 do
        Sources[i].writeDataToXMLNode(ChildNode.AddChild('Source' + tostr(i)));

    //seabed.writeDataToXMLNode(parentNode.AddChild('Seabed'));
    theRA.writeDataToXMLNode(parentNode.AddChild('theRA'));
    weighting.writeDataToXMLNode(parentNode.AddChild('Weighting'));

    writeIntegerField(parentNode,'PropsMapCount', propsMap.Count);
    ChildNode := writeListField(parentNode,'PropsMapList');
    for i := 0 to propsMap.Count - 1 do
        propsMap[i].writeDataToXMLNode(ChildNode.AddChild('LocationProperties' + tostr(i)));
end;

procedure TScenario.readDataFromXMLNode(const parentNode: IDOMNode; const srcLevels: TStringArray; const sSaveVersion: string; const scenInd: integer; const stream: TMemoryStream);
var
    childNode: IDOMNode;
    Sel: IDOMNodeSelect;
    src: TSource;
    delimitedSrcLevels: string;
    log: ISynLog;
    lp: TLocationProperties;
begin
    log := TSynLogLevs.Enter(self,'readDataFromXMLNode');

    inherited readDataFromXMLNode(parentNode);

    {$IFDEF NON_MDA}
        {For the non-MDA version that has no external solvers, check whether one of those solvers is chosen,
        and if so, change to default. NB the user shouldn't be able to choose an incorrect solver,
        but they could potentially open a file that would choose the wrong solver.}

        if self.Solver.MDAOnly then
            self.Solver := cm10log;
        if self.HighSolver.MDAOnly then
            self.HighSolver := cm10log;
        if self.LowSolver.MDAOnly then
            self.LowSolver := cm10log;
    {$ENDIF}

    if runningmode = rmUnlicensed then
    begin
        if not (self.Solver.availableInUnlicensed) then
            self.Solver := cm10log;
        if not (self.HighSolver.availableInUnlicensed) then
            self.HighSolver := cm10log;
        if not (self.LowSolver.availableInUnlicensed) then
            self.LowSolver := cm10log;
    end;

    {$IF Defined(LITE)}
    if not self.solver.availableInLite then
        self.solver := cm10log;
    self.splitSolver := false;
    {$ENDIF}

    if not assigned(parentNode) then exit;

    Sel := parentNode as IDOMNodeSelect;

    {the receiver area. can do this now as it doesn't get levels from sources till later}
    childNode := Sel.selectNode('theRA');
    if assigned(childNode) then
        theRA.readDataFromXMLNode(childNode);  //also inits sound levels

//    {read the number of sources}
//    srccnt := 0;
//    childNode := Sel.selectNode('SourcesCount');
//    if assigned(childNode) then
//        srccnt := toint(childNode.firstChild.nodeValue);
//

    {versions pre 1.0.2 did not use gridded tl, but had no flag to show that}
    if isFloat(sSaveVersion) and (tofl(sSaveVersion) < 5.0) then
        movingSourcesUseGriddedTL := false;

    sources.Clear;
    repeat
        childNode := Sel.selectNode('SourcesList/Source' + tostr(sources.count));
        if assigned(childNode) then
        begin
            //create a source, then overwrite if with the correct data.
            src := TSource.Create(self, bathyDelegate, 0, 0, 2, 0, 0, 1, 1, 1, theRA);
            delimitedSrcLevels := '';
            if (sources.count < length(srcLevels)) then {takes care of >= 7.0}
                delimitedSrcLevels := srcLevels[sources.count];

            src.readDataFromXMLNode(childNode,delimitedSrcLevels,sSaveVersion,sources.count,scenInd,stream);
            sources.Add(src);
        end;
    until not assigned(childNode);

//    if isFloat(sSaveVersion) and (tofl(sSaveVersion) < 6.0) then
//    begin
//        {read the number of seafloor schemes}
//        schemescnt := 0;
//        childNode := Sel.selectNode('SeafloorSchemesCount');
//        if assigned(childNode) then
//            schemescnt := toint(childNode.firstChild.nodeValue);
//
//        seabed.seafloorSchemes.Clear;
//        for i := 0 to schemescnt - 1 do
//        begin
//            childNode := Sel.selectNode('SeafloorSchemesList/SeafloorScheme' + tostr(i));
//            if assigned(childNode) then
//            begin
//                //create a scheme, then overwrite if with the correct data.
//                seabed.seafloorSchemes.Add(TSeafloorScheme.Create);
//                seabed.seafloorSchemes.Last.readDataFromXMLNode(childNode);
//            end;
//        end;
//        if seabed.seafloorSchemes.Count = 0 then
//            seabed.seafloorSchemes.add(TSeafloorscheme.defaultScheme);
//    end
//    else
//    begin
//        {reading the seabed}
//        childNode := Sel.selectNode('Seabed');
//        if assigned(childNode) then
//            seabed.readDataFromXMLNode(childNode);
//    end;

    //{reading the SSP}
    //childNode := Sel.selectNode('SSP');
    //if assigned(childNode) then
    //    ssp.readDataFromXMLNode(childNode);

    //{reading the water properties}
    //childNode := Sel.selectNode('WaterProps');
    //if assigned(childNode) then
    //    water.readDataFromXMLNode(childNode);

    propsMap.Clear;
    if isFloat(sSaveVersion) and (tofl(sSaveVersion) < 8.0) then
    begin
        //get single properties for older versions
        lp := TLocationProperties.Create(self.bathyDelegate.getzmax);

        childNode := Sel.selectNode('SSP');
        if assigned(childNode) then
            lp.ssp.readDataFromXMLNode(childNode);

        childNode := Sel.selectNode('WaterProps');
        if assigned(childNode) then
            lp.water.readDataFromXMLNode(childNode);

        if isFloat(sSaveVersion) and (tofl(sSaveVersion) < 6.0) then
            childNode := Sel.selectNode('SeafloorSchemesList/SeafloorScheme0')
        else
            childNode := Sel.selectNode('Seabed/SeafloorSchemesList/SeafloorScheme0');
        if assigned(childNode) then
            lp.seabed.readDataFromXMLNode(childNode);

        propsMap.Add(lp);
    end
    else
    repeat
        childNode := Sel.selectNode('PropsMapList/LocationProperties' + tostr(propsMap.count));
        if assigned(childNode) then
        begin
            //create a source, then overwrite if with the correct data.
            lp := TLocationProperties.Create(self.bathyDelegate.getzmax);
            lp.readDataFromXMLNode(childNode);
            propsMap.Add(lp);
        end;
    until not assigned(childNode);
    if propsMap.Count = 0 then
        propsMap.add(TLocationProperties.create(self.bathyDelegate.getzmax));
    propsMap.selected := propsMap.First;

    { weighting }
    //old name: criteria
    ChildNode := Sel.selectNode('Criteria');
    if assigned(childNode) then
        weighting.readDataFromXMLNode(ChildNode);
    //new name: weighting
    ChildNode := Sel.selectNode('Weighting');
    if assigned(childNode) then
        weighting.readDataFromXMLNode(ChildNode);

    log.log(sllInfo,'Opened savefile, startfi = %, endfi = %',[self.startfi, self.endfi]);
    case self.bandwidth of
    bwOctave: log.log(sllInfo,'Opened savefile bandwidth = bwOctave');
    bwThirdOctave: log.log(sllInfo,'Opened savefile bandwidth = bwThirdOctave');
    else ;
    end;
    log.log(sllInfo,'Opened savefile, dmax = %',[dmax]);
end;

procedure TScenario.clearLevelsCache;
begin
    theRA.clearLevelsCache;
end;

procedure TScenario.cloneFrom(const source: TScenario);
begin
    TObjectCopier.CopyObject(source,self);

    theRA.cloneFrom(source.theRA);

    sources.cloneFrom(source.sources, self, bathyDelegate, theRA);
    if sources.Count = 0 then
        addSource;

    weighting.cloneFrom(source.weighting);
    propsMap.cloneFrom(source.propsMap, self.bathyDelegate.getzmax);
end;

procedure  TScenario.setName(const aName: String);
begin
    if aName = fName then exit;

    fName := aName;
end;

procedure TScenario.createRAAndSource;
{create a single RA, set to entire area. Create a source in the centre.}
begin
    setTheRAToEntireArea;
    self.addSource;
end;

procedure TScenario.resetSourcesandRAs;
var
    src: TSource;
begin
    theRA.clearLevelsCache;
    for src in Sources do
        src.initTL;
end;

procedure TScenario.resetCProfile;
var
    lp: TLocationProperties;
begin
    for lp in self.propsMap do
        lp.ssp.reset(bathyDelegate.getzMax);
end;

procedure TScenario.setstartfi(const val: integer);
var
    val2: integer;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'setstartfi');
    Log.Log(sllInfo,'setting startfi %',[val]);

    if startfi <> val then
    begin
        val2 := val; //val is const
        forceRange(val2, 0, endfi);
        fstartfi := val2;

        if startfi = endfi then
            fSplitSolverfi := startfi
        else
            forceRange(fSplitSolverfi,startfi,endfi - 1);

        {clear current results}
        resetSourcesandRAs;
        //clearGLObjs; //clear stored gl levels and marching squares arrays
    end;
end;

procedure TScenario.setendfi(const val: integer);
var
    val2: integer;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'setendfi');
    Log.Log(sllInfo,'setting endfi %',[val]);

    if endfi <> val then
    begin
        val2 := val; //val is const
        forceRange(val2, startfi, TSpectrum.getLength(self.bandwidth) - 1);
        fendfi := val2;

        if startfi = endfi then
            fSplitSolverfi := startfi
        else
            forceRange(fSplitSolverfi,startfi,endfi - 1);

        {clear current results}
        resetSourcesandRAs;
        //clearGLObjs; //clear stored gl levels and marching squares arrays
    end;
end;

procedure TScenario.setSplitSolverfi(const val: integer);
var
    val2: integer;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'setSplitSolverfi');
    Log.Log(sllInfo,'setting splitsolverfi %',[val]);

    val2 := val; //val is const
    if startfi = endfi then
        val2 := startfi
    else
        forceRange(val2, startfi, endfi - 1);
    if val2 <> self.SplitSolverfi then
    begin
        self.SplitSolverfi := val2;

        {clear current results}
        resetSourcesandRAs;
        //clearGLObjs; //clear stored gl levels and marching squares arrays
    end;
end;

function TScenario.lenfi: integer;
begin
    result := endfi - startfi + 1;
end;

function TScenario.levelsDifferBetweenTimes(const time1, time2: double): boolean;
var
    src: TSource;
begin
    result := false;
    for src in sources do
        if src.levelsDifferBetweenTimes(time1, time2) then
            exit(true);
end;

procedure TScenario.setbandwidth(const val: TBandwidth);
var
    //oldLenfi: integer;
    splitInd: integer;
    //src: TSource;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'setbandwidth');
    case val of
    bwOctave: Log.Log(sllInfo,'setting bandwidth to octaves');
    bwThirdOctave: Log.Log(sllInfo,'setting bandwidth to third octaves');
    else ;
    end;

    // change over the bandwidth, and also set the start and end indices to appropriate values
    // that should be the same or close to where they were
    if val <> Bandwidth then
    begin
        //oldLenfi := self.lenfi;
        fBandwidth := val;
        case Bandwidth of
            bwOctave :
                begin
                    // MasterSpectrum := TSpectrum.create(Octave); //make new spect first, otherwise the set statements can go wrong
                    setstartfi(safetrunc((startfi - 1) / 3)); // NB move this first, otherwise setendfi can be limited by startfi
                    setendfi(safetrunc((endfi - 1) / 3));
                    splitInd := safetrunc((SplitSolverfi - 1) / 3);
                    forceRange(splitInd,startfi,endfi);
                    setSplitSolverfi(splitInd);
                end;
            bwThirdOctave :
                begin
                    // MasterSpectrum := TSpectrum.create(ThirdOctave); //make new spect first, otherwise the set statements can go wrong
                    setendfi(endfi * 3 + 1); // NB move this first, otherwise setstartfi can be limited by endfi
                    setstartfi(startfi * 3 + 1);
                    splitInd := SplitSolverfi * 3 + 1;
                    forceRange(splitInd,startfi,endfi);
                    setSplitSolverfi(splitInd);
                end;
        end;
        //for src in sources do
        //    src.convertToBandwidth(val, oldlenfi);
        resetSourcesandRAs;
        //TODO Need to do something about source spectrum here - maybe srcSpectrum is ok, but equipreturn may have problems
        //clearGLObjs; //clear stored gl levels and marching squares arrays
    end;
end;

function TScenario.setSmoothTLRadial(aVal: integer): boolean;
var
    src: TSource;
begin
    result := false;
    if aVal < 0 then exit;

    if fiSmoothTLRadial <> aVal then
    begin
        fiSmoothTLRadial := aVal;

        for src in Sources do
            if src.SrcSolved then
            begin
                result := true;
                src.decompressSrcSPLf();
                src.postprocessLevels();
            end;
    end;
end;

procedure TScenario.setSmoothDistToExclusionZone(aVal: integer);
begin
    if aVal < 0 then exit;
    fISmoothDistToExlusionZone := aVal;
end;

function TScenario.setMakeTLMonotonic(const aVal: boolean): boolean;
var
    src: TSource;
begin
    result := false;
    if fMakeTLMonotonic <> aVal then
    begin
        fMakeTLMonotonic := aVal;

        for src in Sources do
            if src.SrcSolved then
            begin
                result := true;
                src.decompressSrcSPLf();
                src.postprocessLevels();
            end;
    end;
end;

function  TScenario.isCurrent: boolean;
begin
    result := delegate.isCurrentScenario(self);
end;


procedure TScenario.updateWeighting(const curve: TFishCurve);
begin
    if not assigned(fWeighting) then
        fWeighting := TFishcurve.Create('',bwOctave,0,TDataSource.dsMDA); //create with temp data
    fWeighting.cloneFrom(curve);

    {now we need to update the levels as the overall level is calculated based on weighted}
    if resultsExist() then
        UWAOpts.updateCMaxCMinandLevelColors();
end;


// return values: 0 - solve ran properly, go to results page
//        1: cant solve, go to sources page
//        2: cant solve, go to world setup page
function TScenario.solveNow(const scenarioIndex: integer;
                            const return: TSolveReturn;
                            const solveTimeEstimator: TSolveTimeEstimator;
                            const lbSolving,lbSolveExtraInfo,lbSliceNumber,lbSolveSourcePosition: TLabel;
                            const stepProgressBars: TProcedureWithInt;
                            const setProgressBars: TProcedureWithDouble;
                            const mmSolverMessages: TMemo;
                            const updateRemainingSolveTime: TProcedureWithBoolBool): TSolveReturnVal;
    procedure addNSlicesForThisSourceToTotal(const src: TSource);
    var
        fi: integer;
    begin
        if src.SrcSpectrumSet then
        begin
            //for splitsolver, keep track of both low and high freq solvers
            //dBSeaRay is a special case, as it solves all frequencies at once
            if SplitSolver then
            begin
                for fi := 0 to lenfi - 1 do
                begin
                    if fi <= splitsolverfi then
                    begin
                        //low solver
                        if ((self.LowSolver = cmDBSeaRay) and (fi = 0)) or (self.LowSolver <> cmDBSeaRay) then
                            solveTimeEstimator.iLowTotSlicesTimesFreq := saferound(solveTimeEstimator.iLowTotSlicesTimesFreq + SourcesNSlices * TSpectrum.getFbyInd(startfi + fi,Bandwidth));
                    end
                    else
                    begin
                        //high solver
                        if ((self.HighSolver = cmDBSeaRay) and (fi = splitSolverfi + 1)) or (self.HighSolver <> cmDBSeaRay) then
                            solveTimeEstimator.iHighTotSlicesTimesFreq := saferound(solveTimeEstimator.iHighTotSlicesTimesFreq + SourcesNSlices * TSpectrum.getFbyInd(startfi + fi,Bandwidth));
                    end; //low/high solver
                end; //fi
            end //splitsolver
            else
            begin
                //not splitsolver
                for fi := 0 to lenfi - 1 do
                begin
                    if ((self.Solver = cmDBSeaRay) and (fi = 0)) or (self.Solver <> cmDBSeaRay) then
                        solveTimeEstimator.iLowTotSlicesTimesFreq := saferound(solveTimeEstimator.iLowTotSlicesTimesFreq + SourcesNSlices * TSpectrum.getFbyInd(startfi + fi,Bandwidth));
                end;
            end; //splitsolver
        end; //src.SrcSpectrumSet
    end;
var
    src: TSource;
    Stopwatch: TStopwatch;
    log: ISynLog;
    allErrorMessages: ISmart<TStringlist>;
    errorMessage: string;

    procedure logAndMessageBuffer(const s: String);
    begin
        return.messageBuffer.Add(s);
        eventLog.log(s);
        Log.Log(sllInfo,RawUTF8(s));
    end;
begin
    log := TSynLogLevs.enter(self,'solveNow');
    Log.Log(sllInfo,'Checking if scenario % can solve. Solver = %, lowSolver = %, highSolver = %, splitsolver = %',[scenarioIndex, self.solver.toStr, self.lowSolver.toStr, self.highSolver.toStr, self.splitSolver]);
    {check if some sources exist}
    if sources.Count = 0 then
    begin
        logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' create at least one source before solving');
        return.errors.add(TSolveError.create(kScenarioNoSources, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' create at least one source before solving', scenarioIndex, -1));
        Exit(TSolveReturnVal.ErrorGoToSourcePage);
    end;

    {$IF Defined(LITE)}
    splitSolver := false;
    if not solver.availableInLite then
        solver := cm10log;
    {$ENDIF}

    {getting the total number of slices*frequencies to solve, for the progress bar.
    Also check that sources are not placed on land, and that sources are
    above the seafloor}
    for src in sources do
    begin
        if src.Enabled then
        begin
            {check whether source has had arrays properly allocated}
            if not src.ArraysCorrectlyAssigned then
            begin
                logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' is not ready to solve due to memory limitations');
                return.errors.add(TSolveError.create(kArraysNotCorrectlyAssigned,'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' is not ready to solve due to memory limitations', scenarioIndex, sources.IndexOf(src)));
                exit(TSolveReturnVal.ErrorGoToSetupPage);
            end;

            {check whether the source is on land}
            if (not src.IsMovingSource) and (isnan(bathyDelegate.getBathymetryAtPos(src.Pos.X,src.Pos.Y)) or
                                            (bathyDelegate.getBathymetryAtPos(src.Pos.X,src.Pos.Y) <= 0.0)) then
            begin
                logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' is placed on land or unsurveyed area, cannot solve');
                return.errors.add(TSolveError.create(kStationarySourceOnLand, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' is placed on land or unsurveyed area, cannot solve', scenarioIndex, sources.IndexOf(src)));
                Exit(TSolveReturnVal.ErrorGoToSourcePage);
            end;

            {check whether the source is below the seafloor}
            {$IF not (defined (DEBUG) or defined (VER2))}
            if (not src.IsMovingSource) and (not src.checkSourceDepth) then
            begin
                logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' is below seafloor, check z depth before solving');
                return.errors.add(TSolveError.create(kStationarySourceBelowSeabed, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' is below seafloor, check z depth before solving', scenarioIndex, sources.IndexOf(src)));
                Exit(TSolveReturnVal.ErrorGoToSourcePage);
            end;
            {$ENDIF}

            //if src.SrcSpectrumSet and not singleSeafloorScheme then
            //begin
            //    logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' varying sediment scheme with range not currently supported, using first scheme');
            //    return.errors.add(TSolveError.create(kNotSingleSeafloorScheme, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' varying sediment scheme with range not currently supported, using first scheme', scenarioIndex, sources.IndexOf(src)));
            //end;

            if src.srcSpectrumSet then
                addNSlicesForThisSourceToTotal(src);
        end; //src.Enabled
    end;  //src

    {at least one of the sources needs to have a spectrumset before we can solve}
    if not ((solveTimeEstimator.iLowTotSlicesTimesFreq <> 0) or (solveTimeEstimator.iHighTotSlicesTimesFreq <> 0)) then
    begin
        logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' set source spectra or time series before solving');
        return.errors.add(TSolveError.create(kNoSpectrumOrTimeSeries, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' set source spectra or time series before solving', scenarioIndex, -1));
        Exit(TSolveReturnVal.ErrorGoToSourcePage);
    end;

    if (not TLevelConverter.isTimeSeries(self.levelType)) and allSourcesSpectrumAllNan(startFi, lenFi) then
    begin
        logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' set source spectra before solving');
        return.errors.add(TSolveError.create(kNoSourceSpectrum, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' set source spectra before solving', scenarioIndex, -1));
        Exit(TSolveReturnVal.ErrorGoToSourcePage);
    end;

    //check that for multiple locationproperties, the number of layers matches
    {$IF defined (DEBUG) or defined (VER2)}
    if not self.propsMap.allSeabedsHaveSameNumberOfLayers then
    begin
        logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' must have the same number of seabed layers at every location');
        return.errors.add(TSolveError.create(kNoSourceSpectrum, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' must have the same number of seabed layers at everz location', scenarioIndex, -1));
        Exit(TSolveReturnVal.ErrorGoToSetupPage);
    end;
    {$ENDIF}

    {there is at least 1 source to be solved, but also show a message for any source that has no spectrum set}
    for src in sources do
        if src.Enabled and (not src.SrcSpectrumSet) then
        begin
            logAndMessageBuffer('Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' has no spectrum or time series set, ignoring for this solve');
            return.errors.add(TSolveError.create(kIgnoringScenario, 'Scenario ' + tostr(scenarioIndex) + ' ' + name + ' source ' + src.Name + ' has no spectrum or time series set, ignoring for this solve', scenarioIndex, sources.IndexOf(src)));
        end;

    {now we are ready to solve as we have sources, receivers, and the sources have spectra}
    eventLog.log('Begin solve of scenario ' + tostr(scenarioIndex) + ' ' + self.name);
    Log.Log(sllInfo,'Beginning solve of scenario % %',[scenarioIndex, self.name]);

    Stopwatch := TStopwatch.StartNew;

    allErrorMessages := TSmart<TStringList>.create(TStringlist.create);
    for src in sources do
        if src.Enabled and src.SrcSpectrumSet then
        begin
            //make sure the source has the correct number of slices and imax
            src.setNSlices(sourcesNSlices);
            src.setiMax(sourcesIMax);

            src.solveNow(solver,
                         lowSolver,
                         highSolver,
                         startfi,
                         splitSolverfi,
                         lenfi,
                         endfi,
                         nOversampleFreqsPerBand,
                         dMax,
                         nPadeTerms,
                         nPadeTermsHorizontal,
                         splitSolver,
                         coherent,
                         rayTracingCalculateAttenuationAtEachStep,
                         raySolverIterationMethod,
                         levelType,
                         userSolverLogPart, userSolverLinPart,
                         allErrorMessages,
                         solveTimeEstimator,
                         lbSolving,
                         lbSolveExtraInfo,
                         lbSliceNumber,
                         lbSolveSourcePosition,
                         stepProgressBars,
                         setProgressBars,
                         mmSolverMessages,
                         updateRemainingSolveTime);
        end;

    if allErrorMessages.Count > 0 then
    begin
        ShowMessage(allErrorMessages.DelimitedText);
        for errorMessage in allErrorMessages do
            logAndMessageBuffer(errorMessage);
    end;

    lbSolving.caption := 'Updating levels cache';
    application.processMessages;
    theRA.updateLevelsCache(-1, setProgressBars, self.pcancelSolve);

    eventLog.log('Finish solve of scenario ' + tostr(scenarioIndex) + ' ' + self.name + ', elapsed time (s): ' + tostrWithTestAndRound1(Stopwatch.Elapsed.TotalSeconds));
    Log.Log(sllInfo,'Finish solve of scenario % %',[scenarioIndex, self.name]);
    result := TSolveReturnVal.NoError;
end;

function TScenario.allSourcesSpectrumAllNan(const startFi, lenFi: integer): boolean;
var
    src: TSource;
begin
    for src in sources do
        if (not src.spectrumAllNan(startFi,lenfi)) then
            exit(false);

    result := true;
end;

function TScenario.anySourceMoving: boolean;
var
    src: TSource;
begin
    for src in self.sources do
        if src.isMovingSource then
            exit(true);
    result := false;
end;

function TScenario.numberOfMovingSources: integer;
var
    src: TSource;
begin
    result := 0;
    for src in self.sources do
        if src.isMovingSource then
            inc(result);
end;

function TScenario.exclusionZoneAtPos(const exclusionZoneLevel: single; const src: TSource; const movingPosIndex, soundFieldAtMovingPosIndex: integer; const stopAtLand: boolean; const onlyForSource: boolean = false): TExclusionZonePointArray;
type
    TAngleInfo = record
        distPastLimit: double;
        levPastLimit: single;
        distBeforeLimit: double;
        levBeforeLimit: single;
        distToLimit: double;
        inds: integer;
        exclusionZoneIsAtBorder: boolean;
        isLand: boolean;
        lastWaterDist: double;
    end;
    TAngleInfoArray = array of TAngleInfo;
var
    angs: TAngleInfoArray; //info about the exclusion zone at each nslice

    procedure testAndDoUpdate(const isBorder: boolean; const r, Lev: double; var aInfoInds: TIntegerDynArray);
    var
        aInfoInd: integer;
    begin
        for aInfoInd in aInfoInds do
        begin
            if aInfoInd <0 then break;

            {x and y are our current grid location. r is distance to source. lev is the level here}
            if (not isnan(Lev)) and
                (isBorder or (Lev < exclusionZoneLevel)) and
                (isnan(angs[aInfoInd].distPastLimit) or (r < angs[aInfoInd].distPastLimit)) then
                {points that are on the border,
                or the level is less than the current exclusion zone,
                ie outside exclusion zone.
                nan = no current point past for this angle,
                so use this point. r < dist this point is closer than the current point for this angle}
            begin
                angs[aInfoInd].distPastLimit := r;
                angs[aInfoInd].levPastLimit  := Lev;
                angs[aInfoInd].exclusionZoneIsAtBorder := isBorder;
            end;
        end;
    end;

    procedure nanSmoothing(const theAngs: TAngleInfoArray; const beforeLimit: boolean);
    var
        v: TDoubleDynArray;
        i: integer;
    begin
        {sometimes, there will not be a point within certain angles (sparse grid, lots of slices) so smooth across the NaN values. get the required values out of the record, smooth, and replace}
        setlength(v,length(theAngs));

        if beforeLimit then
        begin
            for I := 0 to length(theAngs) - 1 do
                v[i] := theAngs[i].distBeforeLimit;
            TMCKLib.arrayLoopSmoothNaNs(v);
            for I := 0 to length(theAngs) - 1 do
                theAngs[i].distBeforeLimit := v[i];

            for I := 0 to length(theAngs) - 1 do
                v[i] := theAngs[i].levBeforeLimit;
            TMCKLib.arrayLoopSmoothNaNs(v);
            for I := 0 to length(theAngs) - 1 do
                theAngs[i].levBeforeLimit := v[i];
        end
        else
        begin
            for I := 0 to length(theAngs) - 1 do
                v[i] := theAngs[i].distPastLimit;
            TMCKLib.arrayLoopSmoothNaNs(v);
            for I := 0 to length(theAngs) - 1 do
                theAngs[i].distPastLimit := v[i];

            for I := 0 to length(theAngs) - 1 do
                v[i] := theAngs[i].LevPastLimit;
            TMCKLib.arrayLoopSmoothNaNs(v);
            for I := 0 to length(theAngs) - 1 do
                theAngs[i].LevPastLimit := v[i];
        end;
    end;
const
    angleSearchFactor = 3;
var
    i,j,k,n: integer;
    dx,dy: double;
    distToLimit: TDoubleDynArray;
    inds: array of TIntegerMatrix; //the relevant nSlice indices for each point in i,j (may be 1,2 or 3 relevant slices as we expand the search zone a little)
    rs: TMatrix;
    Levs: TSingleMatrix;
    q, psi, sinA, cosA: double; {psi = angle of index to source}
    movingPos: TFPoint3;

    //{$DEFINE EXCLUSIONOUTPUT}
    {$IF Defined(DEBUG) and Defined(EXCLUSIONOUTPUT)}
    s: string;
    {$ENDIF}
begin
    setlength(angs,src.NSlices);
    setlength(result,src.nSlices);

    {clearing working array}
    for n := 0 to src.NSlices - 1 do
    begin
        angs[n].distPastLimit := nan;
        angs[n].levPastLimit := nan;
        angs[n].exclusionZoneIsAtBorder := true;
        angs[n].distBeforeLimit := nan;
        angs[n].levBeforeLimit := nan;
        angs[n].isLand := false;
    end;

    setlength(rs  ,theRA.imax,theRA.jmax);
    setlength(inds,theRA.imax,theRA.jmax,3);
    setlength(Levs,theRA.imax,theRA.jmax);

    {NB Levs[] and self.fExclusionZoneLevel are kSPL, not converted to user Leveltype}
    for i := 0 to theRA.imax - 1 do
        for j := 0 to theRA.jmax - 1 do
            if onlyForSource then
                Levs[i,j] := theRA.getLevAtInds(UWAOpts.DisplayZInd,i,j,soundFieldAtMovingPosIndex,UWAOpts.LevelsDisplay,NaNSeafloorLevels,src)
            else
                Levs[i,j] := theRA.getLevAtInds(UWAOpts.DisplayZInd,i,j,soundFieldAtMovingPosIndex,UWAOpts.LevelsDisplay,NaNSeafloorLevels);

    {get position (includes non-moving source)}
    movingPos := src.getMovingPos(movingPosIndex);

    {get tmpDistToLimit for just this source. We have the levels over theRA, and they have been made monotonic
    going in towards each source. so for each point we evaluate
    whether it is less than the exclusion level. then, we find which 'angle' index (given in inds[i,j]) around
    the given source this point is. if this point is closer in to the source than the current closest point within this
    angle index, then update the tmpDistToLimit value for this point.
    The old version looked for the furthest out point that was greater than the exclusion level, however this causes
    problems when there are 2 sources widely separated, and the area between them should NOT be coloured. The
    problem with doing it this way around, is that the given point is now actually too far out from the source -
    ie it is the point closest to the source that shouldn't be coloured.
    Now we also need to look for points along the edges, as we could have the possibility of finding no points between
    the source and the edge that was below the exclusion level, in which case we would just have a NaN for that angle}

    //get the nearest point outside the exclusion zone
    for i := 0 to theRA.imax - 1 do
    begin
        dx := theRA.xs(i) - movingPos.x;
        for j := 0 to theRA.jmax - 1 do
        begin
            dy := theRA.ys(j) - movingPos.y;
            rs[i,j] := hypot(dx,dy); //distance to source
            psi := arctan2(dy,dx);
            if not isnan(psi) and (psi < 0) then
                psi := psi + 2*pi;
            inds[i,j,0] := integerWrap(saferound(psi/src.angleSpacing),src.nSlices - 1); //nslices index

            if (inds[i,j,0] <> 0) and
                (abs(psi - src.angles(inds[i,j,0] - 1)) < (angleSearchFactor * pi / sourcesNSlices)) then
                inds[i,j,1] := inds[i,j,0] - 1
            else
                inds[i,j,1] := -1;

            if (inds[i,j,0] < sourcesNSlices - 1) and
                (abs(psi - src.angles(inds[i,j,0] + 1)) < (angleSearchFactor * pi / sourcesNSlices)) then
                inds[i,j,2] := inds[i,j,0] + 1
            else
                inds[i,j,2] := -1;

            testAndDoUpdate((i = 0) or (i = theRA.imax - 1) or (j = 0) or (j = theRA.jmax - 1),
                            rs[i,j],
                            Levs[i,j],
                            inds[i,j]);
        end;//i,j
    end;

    nanSmoothing(angs, false);

    {as we have the nearest point outside the exclusion zone, now we go through and find the nearest
    point inside the exclusion zone}
    for i := 0 to theRA.imax - 1 do
        for j := 0 to theRA.jmax - 1 do
            for k in inds[i,j] do
                if (k >= 0) and (rs[i,j] < angs[k].distPastLimit) then
                    if isnan(angs[k].distBeforeLimit) or (rs[i,j] > angs[k].distBeforeLimit) then
                    begin
                        angs[k].distBeforeLimit := rs[i,j];
                        angs[k].levBeforeLimit  := levs[i,j];
                    end;

    nanSmoothing(angs, true);

    {Now we have the distances to points on either side of the exclusion zone, and the levels at those points.
    Interpolate to find the distance to the zone limit}
    setlength(distToLimit,src.NSlices);
    for n := 0 to src.NSlices - 1 do
    begin
        //need to work out how far between the before and after limit points the limit sits
        if angs[n].exclusionZoneIsAtBorder or (angs[n].levBeforeLimit = angs[n].levPastLimit) then
            q := 1.0
        else
            q := (angs[n].levBeforeLimit - exclusionZoneLevel)/(angs[n].levBeforeLimit - angs[n].levPastLimit); //get ratio of sound levels before and after

        distToLimit[n] := (1 - q)*angs[n].distBeforeLimit + q*angs[n].distPastLimit;
    end;

    //Here we can do smoothing of angs[i].distToLimit
    TMCKLib.arrayLoopSmoothNaNs(distToLimit);  //get rid of nans

    //make sure we aren't going past land
    if stopAtLand then
        for n := 0 to src.nSlices - 1 do
        begin
            angs[n].lastWaterDist := src.getLastWaterDist(movingPosIndex,n);
            if distToLimit[n] > angs[n].lastWaterDist then
                distToLimit[n] := angs[n].lastWaterDist;
        end;

    if self.iSmoothDistToExlusionZone > 0 then
        TMCKLib.loopSmoothArray(TMCKLib.triangleDistribution(self.iSmoothDistToExlusionZone),distToLimit,true); //smooth around the array (looping)

    for n := 0 to src.nSlices - 1 do
    begin
        angs[n].isLand := stopAtLand and (distToLimit[n] > angs[n].lastWaterDist);
        if angs[n].isLand then
            angs[n].distToLimit := angs[n].lastWaterDist
        else
            angs[n].distToLimit := distToLimit[n];
    end;

    //pass the at limit values through to the source
    for n := 0 to src.NSlices - 1 do
    begin
        system.SineCosine(src.angles(n), sinA, cosA);
        result[n].p.X := movingPos.X + angs[n].distToLimit*cosA;
        result[n].p.Y := movingPos.Y + angs[n].distToLimit*sinA;
        result[n].isLand := angs[n].isLand;
    end;
end;

function  TScenario.estimateMemUsage: int64;
begin
    if not assigned(theRA) then exit(0);

    result := 4*(theRA.iMax*theRA.jMax + sources.Count*SourcesIMax*SourcesNSlices)*dMax*(lenfi + 1);
end;

function TScenario.weightingIsNone: Boolean;
var
    i: integer;
begin
    {if all the weighting at all freqs is zero, then returns true, i.e. no weighting set}
    result := true;
    for i := 0 to TSpectrum.getlength(weighting.bandwidth) - 1 do
        if weighting.Spectrum.getAmpbyInd(i,weighting.bandwidth) <> 0 then
            exit(false);
end;

procedure TScenario.readSourcesFromFile(const sCSVFile: String);
var
    src: TSource;
    sM: TStringMatrix;
    i: integer;
    aX, aY, aZ: double;
begin
    {file exists and is not locked by another process. proceed with reading data.}
    if CSVDelimitersForm.setFileName(sCSVFile) then
        CSVDelimitersForm.showmodal;
    {only show the form if it can't figure out by itself which is the correct delimiter.
    it seems to work ok without needing to be shown,
    and it is more confusing than helpful. if there are still problems with file import,
    the user will have to look at the input file. Do use the form though, to get the
    delimiters.}

    sM.fromDelimitedString(THelper.readfiletostring(sCSVFile),CSVDelimitersForm.fieldDelimiter);

    if length(sM) = 0 then
    begin
        showmessage('Unable to parse CSV file data');
        Exit;
    end;

    if not (length(sM[0]) = 4) then
    begin
        showmessage('Unable to parse CSV file data');
        Exit;
    end;

    i := 0;
    if ansicomparetext(sm[0,0],'name') = 0 then
        inc(i); //ignore header row

    while i < length(sM) do
    begin
        if (isFloat(sm[i,1]) and isFloat(sm[i,2]) and isFloat(sm[i,3])) then
        begin
            src := AddSource();
            src.Name := sM[i,0];
            aX := f2m(tofl(sm[i,1]),UWAOpts.DispFeet) - delegate.getEasting;
            aY := f2m(tofl(sm[i,2]),UWAOpts.DispFeet) - delegate.getNorthing;
            aZ := f2m(tofl(sm[i,3]),UWAOpts.DispFeet);
            forceRange(aX, theRA.Pos1.X, theRA.Pos2.X);
            forceRange(aY, theRA.Pos1.Y, theRA.Pos2.Y);
            src.setPos(aX,aY,aZ)
        end
        else
            showmessage('Row ' + tostr(i) + ' ignored');

        inc(i);
    end;
end;

function  TScenario.getCProfileAtAngle(const aAngle: double): TMatrix;
var
    water: TWaterProps;
begin
    water := propsMap.selected.water;
    result := propsMap.selected.ssp.getCProfileAtAngle(aAngle, water.Velocity, water.Direction);
end;

function  TScenario.getCProfileAtAngle2(const aAngle: double): TDepthValArray;
var
    water: TWaterProps;
begin
    water := propsMap.selected.water;
    result := propsMap.selected.ssp.getCProfileAtAngle2(aAngle, water.Velocity, water.Direction);
end;

function TScenario.getCProfilesAtAngle3(const aPos: TFPoint3; const aAngle, rmax, dr: double): TDepthValArrayAtRangeArray;
var
    r, cosA, sinA: double;
    i: integer;
    lp, oldLP: TLocationProperties;
begin
    lp := propsMap.closest(aPos.x, aPos.y);
    oldLP := lp;

    setlength(result, 1);
    result[0].range := 0;
    result[0].depthVal := lp.ssp.getCProfileAtAngle2(aAngle, lp.water.Velocity, lp.water.Direction);

    system.SineCosine(aAngle, sinA, cosA);
    for i := 1 to ceil(rmax / dr) - 1 do
    begin
        r := i * dr;
        lp := propsMap.closest(aPos.x + r * cosA, aPos.y + r * sinA);
        if lp <> oldLP then
        begin
            oldLP := lp;
            setlength(result, length(result) + 1);
            result[length(result) - 1] := TDepthValArrayAtRange.create(r,lp.ssp.getCProfileAtAngle2(aAngle, lp.water.Velocity, lp.water.Direction));
        end;
    end;
end;

function TScenario.getLocationProps(const aPos: TFPoint3; const aAngle, rmax, dr: double): TLPRangeList;
var
    r, cosA, sinA: double;
    i: integer;
begin
    result := TLPRangeList.create(true);

    {$IF not (defined(DEBUG) or defined(VER2))}
        result.add(TLPRange.create(0, propsMap.copyFirst(self.bathyDelegate.getzmax)));
        exit;
    {$ENDIF}

    if propsMap.count = 1 then
    begin
        result.add(TLPRange.create(0, propsMap.copyFirst(self.bathyDelegate.getzmax)));
        exit;
    end;

    system.SineCosine(aAngle, sinA, cosA);
    for i := 0 to ceil(rmax / dr) - 1 do
    begin
        r := i * dr;
        result.add(TLPRange.create(r, propsMap.interp(aPos.x + r * cosA, aPos.y + r * sinA, self.bathyDelegate.getzmax)));
    end;
end;

function TScenario.getdBSeaModesMaxModes: integer;
begin
    result := dBSeaModesMaxModes;
end;

function TScenario.getdBSeaModesrOversampling: double;
begin
    result := self.dBSeaModesrOversampling;
end;

function TScenario.getdBSeaModesZOversampling: double;
begin
    result := self.dBSeaModeszOversampling;
end;

function TScenario.getdBSeaPEPhiOversampling: double;
begin
    result := self.dBSeaPEPhiOversampling;
end;

function TScenario.getdBSeaPErOversampling: double;
begin
    result := self.dBSeaPErOversampling;
end;

function TScenario.getdBSeaPEZOversampling: double;
begin
    result := self.dBSeaPEzOversampling;
end;

function TScenario.getDBSeaRayMaxBottomReflections: integer;
begin
    result := self.dBSeaRayMaxBottomReflections;
end;

function TScenario.getDepths(const d: integer): double;
begin
    result := self.depths(d);
end;

function TScenario.getdmax: integer;
begin
    result := self.dMax;
end;

function TScenario.getdz: double;
begin
    result := self.dz;
end;

function TScenario.getEndfi: integer;
begin
    result := self.endfi;
end;

function TScenario.getFieldAtTime(const time: double): TDoubleField;
var
    i,j,d: integer;
begin
    setlength(result,theRA.iMax,theRA.jMax,self.dMax);
    for i := 0 to theRA.imax - 1 do
        for j := 0 to theRA.jMax - 1 do
            for d := 0 to self.dMax - 1 do
                result[i,j,d] := self.theRA.getLevAtIndsAndTime(d,i,j,time);
end;

function TScenario.getGridIMax: integer;
begin
    result := theRA.iMax;
end;

function TScenario.getGridJMax: integer;
begin
    result := theRA.jMax;
end;

function TScenario.getHighSolver: TCalculationMethod;
begin
    result := self.highSolver;
end;

function TScenario.getiSmoothTLRadial: integer;
begin
    result := self.iSmoothTLRadial;
end;

function TScenario.getLenfi: integer;
begin
    result := self.lenfi;
end;

function TScenario.getLevAtInds(const d, i, j, p: integer;
  const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean): single;
begin
    result := self.theRA.getLevAtInds(d,i,j,p,levelsDisplay,bSeafloorNaNs);
end;

function TScenario.getLevelType: TLevelType;
begin
    result := self.levelType;
end;

function TScenario.getLowSolver: TCalculationMethod;
begin
    result := self.lowSolver;
end;

function TScenario.getMakeTLMonotonic: boolean;
begin
    result := self.makeTLMonotonic;
end;

function TScenario.getMaxSourceMovingTime: double;
begin
    result := self.maxSourceMovingTime;
end;

function TScenario.getMovingSourcesUseGriddedTL: boolean;
begin
    result := self.movingSourcesUseGriddedTL;
end;

function TScenario.resultsExist;
var
    src: TSource;
begin
    result := false;
    for src in sources do
        if src.srcSolved then
            exit(true);
end;

function  TScenario.SourceNameMatch(const s: string): boolean;
var
    src: TSource;
begin
    for src in sources do
        if ansiCompareText(s,src.Name) = 0 then
            exit(true);
    result := false;
end;


function TScenario.addSource(const atX: double = nan; const atY: double = nan; const atZ: double = nan): TSource;
var
    aDepth: double;
    aX, aY: double;
begin
    if not assigned(theRA) then exit(nil);

    if isnan(atX) then
        aX := (theRA.Pos2.X - theRA.Pos1.X)/2
    else
        aX := atX;

    if isnan(atY) then
        aY := (theRA.Pos2.Y - theRA.Pos1.Y)/2
    else
        aY := atY;

    if isnan(atZ) then
    begin
        aDepth := 5;
        if not isnan(bathyDelegate.getBathymetryAtPos(aX,aY)) then
            aDepth := smaller(aDepth,bathyDelegate.getBathymetryAtPos(aX,aY)/2);
    end
    else
        aDepth := atZ;

    sources.Add(TSource.Create(self,
                               bathyDelegate,
                               aX,
                               aY,
                               aDepth,
                               delegate.diagonalLength3D(delegate.worldscale),
                               bathyDelegate.getzmax,
                               SourcesIMax,
                               SourcesNSlices,
                               sources.count + 1,
                               theRA));
    result := sources.Last;
end;

procedure TScenario.deleteSource(const ind: integer);
begin
    if checkRange(ind,0,sources.Count - 1) then
    begin
        eventLog.log('Delete source ' + tostr(ind) + ' in  scenario' + self.name);
        sources.Delete(ind);
    end;
end;

procedure TScenario.scaleSourcePositions(const scaleby: double);
var
    src: TSource;
begin
    for src in sources do
        src.setPos(src.Pos.X * scaleBy,
                   src.Pos.Y * scaleBy,
                   src.Pos.Z);
end;

function TScenario.headerForExport(const cDelim: char): string;
{create the header to be attached at the start of all exported levels files}
var
    orderedKeys: ISmart<TStringlist>;
    i: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);

    orderedKeys := TSmart<TStringlist>.create(self.printInfo(cDelim, self.sources.count, self.theRA.probes.count));
    sb.append(CurrentProjectName + #13#10#13#10);
    for i := 0 to orderedKeys.count - 1 do
        sb.append(orderedKeys[i] + cDelim + TWrappedString(orderedKeys.Objects[i]).str + #13#10);
    if UWAOpts.exportFreeText <> '' then
        sb.append('Export info' + cDelim + UWAOpts.exportFreeText + #13#10);
    sb.append(#13#10#13#10);
    result := sb.ToString;
end;

procedure TScenario.writeLevelsToFile(const filename: String; const cDelim: char; const bWriteSpectra: boolean);
var
    //RA: TReceiverArea;
    sl: ISmart<TStringlist>;
    fi,d,i,j: integer;
    spect: ISmart<TSpectrum>;
    bw: TBandwidth;
    sUnits: string;
    thisLev: double;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    {have already established that file can be written to}
    bw := Bandwidth;
    sUnits := ifthen(UWAOpts.dispFeet,'ft','m');

    spect := TSmart<TSpectrum>.create(TSpectrum.create);
    sl := TSmart<TStringlist>.create(TStringList.Create);

    sl.Delimiter := cDelim; // delimiter char
    sb.append(HeaderForExport(cDelim));

    if resultsExist() then
    begin
        //s := s + 'Receiver Area ' + tostr(RA.RAId + 1) + ': "' + RA.Name + '"' + #13#10#13#10;

        {write the total level}
        sb.append('Total level' + #13#10 + '==========='#13#10#13#10);
        for d := 0 to dMax - 1 do
        begin
            {depth line}
            sb.append('Depth = ' + tostr(round1(m2f(depths(d),UWAOpts.DispFeet))) + sUnits + #13#10#13#10);
            {y dir line}
            sl.Clear;
            sl.Add('x,y');//spacer

            for i := 0 to theRA.iMax - 1 do
                sl.Add(delegate.makeXString(theRA.xs(i)));
            sb.append(sl.DelimitedText + #13#10);

            {use downto, so that y direction is reversed}
            for j := theRA.jMax - 1 downto 0 do
            begin
                sl.Clear;
                sl.Add(delegate.makeYString(theRA.ys(j))); //x line
                //sl.Add('');//spacer
                {add the levels at that point}
                for i := 0 to theRA.iMax - 1 do
                begin
                    thisLev := theRA.getLevAtInds(d,i,j,-1,
                                                  ldSingleLayer,
                                                  NaNSeafloorLevels);
                    thisLev := convertLevel(thisLev);
                    if isNaN(thisLev) then
                        sl.Add('n')
                    else
                        sl.Add(tostr(round1(thisLev)));
                end;
                sb.append(sl.DelimitedText + #13#10);
            end; //i
            sb.append(#13#10);
        end;//d

        if bWriteSpectra then
        begin
            {now write out the individual freq components.}
            sb.append('Frequency components'#13#10'===================='#13#10);
            for fi := 0 to lenfi - 1 do
            begin
                {we don't use the getfstring function to get the freq string, due to the space between 63 hz
                but 1khz - doesn't work.}
                sb.append(#13#10'Frequency = ' + tostr(TSpectrum.getFbyInd(startfi + fi,bw)) + ' Hz'#13#10);
                for d := 0 to dMax - 1 do
                begin
                    {depth line}
                    sb.append(#13#10 + 'Depth = ' + tostr(round1(m2f(depths(d),UWAOpts.DispFeet))) + sUnits + #13#10#13#10);
                    {y dir line}
                    sl.Clear;
                    sl.Add('x,y');//spacer
                    for i := 0 to theRA.iMax - 1 do
                        sl.Add(delegate.makeXString(theRA.xs(i)));
                    sb.append(sl.DelimitedText + #13#10);

                    {use downto, so that y direction is reversed}
                    for j := theRA.jMax - 1 downto 0 do
                    begin
                        sl.Clear;
                        sl.Add(delegate.makeYString(theRA.ys(j))); //y line
                        //sl.Add('');//spacer
                        {add the freq components at that point}
                        for i := 0 to theRA.iMax - 1 do
                        begin
                            thisLev := theRA.getSPLfAtInds(d,i,j,-1,startfi + fi,bw);
                            thisLev := convertLevel(thisLev);
                            if isNaN(thisLev) then
                                sl.Add('n')
                            else
                                sl.Add(tostr(round1(thisLev)));
                        end;
                        sb.append(sl.DelimitedText + #13#10);
                    end; //i
                end;//d
            end;//fi
        end; //bWriteSpectra
    end; //RA in RAs, RA.solved

    THelper.WriteStringToFile(filename,sb.ToString);
end;

function TScenario.formatOutputLevelFunc(const lev: single; const sNaN: string): string;
begin
    if isNaN(lev) or (lev < LowLev) then
        result := sNaN
    else
        result := forceDotSeparator(tostr(round1(convertLevel(lev))));
end;

procedure TScenario.writeXSecLevelsToFile(const filename: String; const cDelim: char; const bWriteSpectra: boolean);
var
    amCrossSecSpectra: array of TMatrix;
    maCrossSecLevels: TMatrix;
    zProfile: TRangeValArray;
    i, d, iLenDepths: integer;
    spect: ISmart<TSpectrum>;
    range, angle, maxDepth: double;
    xs, ys: TDoubleDynArray;
    sl: ISmart<TStringlist>;
    fi: integer;
    bw: TBandwidth;
    sUnits: string;
    iLenRange, iLenRangeX, iLenRangeY: integer;
    dx, dy, distX, distY: double;
    dDistBetweenPoints: double;
    iInd, jInd: integer;
    tmpXs, tmpYs: TDoubleDynArray;
    RAdx, RAdy: double;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    {have already established that file can be written to}
    bw := Bandwidth;
    sl := nil;
    spect := TSmart<TSpectrum>.create(TSpectrum.create);
    sl := TSmart<TStringList>.create(TStringList.Create);
    sUnits := ifthen(UWAOpts.dispFeet,'ft','m');

    sl.Delimiter := cDelim; // delimiter char
    sb.append(HeaderForExport(cDelim));

    {GET SEA BOTTOM PROFILE}
    range := TMCKLib.Distance3D(delegate.getCrossSec.endpoints[0].Pos.X, delegate.getCrossSec.endpoints[1].Pos.X,
                        delegate.getCrossSec.endpoints[0].Pos.Y, delegate.getCrossSec.endpoints[1].Pos.Y,
                        delegate.getCrossSec.endpoints[0].Pos.Z, delegate.getCrossSec.endpoints[1].Pos.Z);
    if range = 0 then
    begin
        showmessage('Cross section must be defined first');
        Exit;
    end;
    distX := delegate.getCrossSec.endpoints[1].Pos.X - delegate.getCrossSec.endpoints[0].Pos.X;
    distY := delegate.getCrossSec.endpoints[1].Pos.Y - delegate.getCrossSec.endpoints[0].Pos.Y;
    angle := arctan2(distY,distX);

    {get the maximum depth along the given xsec vector}
    zProfile := bathyDelegate.getBathymetryVector2(delegate.getCrossSec.endpoints[0].Pos.X, delegate.getCrossSec.endpoints[0].Pos.Y, angle, range, 0);
    maxDepth := zProfile.max;
    if maxDepth < 0 then
        maxDepth := 0;

    dx := theRA.xs(1) - theRA.xs(0);
    dy := theRA.ys(1) - theRA.ys(0);
    iLenRangeX := safeceil(distX/dx);
    iLenRangeY := safeceil(distY/dy);
    iLenRange := safeceil(sqrt(iLenRangeX*iLenRangeX + iLenRangeY*iLenRangeY));
    dDistBetweenPoints := range/iLenRange;

    setlength(xs,iLenRange);
    setlength(ys,iLenRange);
    for i := 0 to iLenRange - 1 do
    begin
        xs[i] := delegate.getCrossSec.endpoints[0].Pos.X + i*dDistBetweenPoints*cos(angle);
        ys[i] := delegate.getCrossSec.endpoints[0].Pos.Y + i*dDistBetweenPoints*sin(angle);
    end;

    if resultsExist() then
    begin
        if UWAOpts.FullXSecDepth then
            iLenDepths := dMax
        else
        begin
            iLenDepths := 0;
            for i := 0 to dMax - 1 do
                if depths(i) < maxDepth then
                    iLenDepths := i + 1;
        end;

        {Get levels matrix - at every point in the z profile and every depth}
        setlength(maCrossSecLevels,iLenRange,iLenDepths);
        {if we are writing spectra, also get the levels at every freq and put in a separate array of matrix}
        if bWriteSpectra then
            setlength(amCrossSecSpectra,lenfi,iLenRange,iLenDepths);

        {put xs and ys into temp arrays so we can use arrayFind}
        tmpXs := theRA.getXsArray;
        tmpYs := theRA.getYsArray;

        RAdx := tmpXs[1] - tmpXs[0];
        RAdy := tmpYs[1] - tmpYs[0];

        for i := 0 to iLenRange - 1 do
        begin
            iInd := saferound(xs[i]/RAdx);
            forceRange(iInd,0,high(tmpXs));
            jInd := saferound(ys[i]/RAdy); //yes this really should be i in ys[i], i is stepping along the xsec
            forceRange(jInd,0,high(tmpYs));

            for d := 0 to iLenDepths - 1 do
            begin
                {need to keep using the spectrum call here, as we sometimes need
                the full spectrum. this function doesn't pick up the assessment weighting}
                theRA.getSpectAtInds(spect,iInd,jInd,ldSingleLayer,d);
                maCrossSecLevels[i,d] := spect.Level;

                if bWriteSpectra then
                    for fi := 0 to lenfi - 1 do
                        amCrossSecSpectra[fi,i,d] := spect.getAmpbyInd(fi + startfi,bw);
            end; //d
        end; //i

        {write the total level}
        sb.append('Total level' + #13#10 + '==========='#13#10#13#10);

        {x and y dir lines}
        {split the lines up to make life a bit easier in spreadsheets}
        sl.Clear;
        sl.Add('x');
        for i := 0 to iLenRange - 1 do
            sl.Add(delegate.makeXString(xs[i]));
        sb.append(sl.DelimitedText + #13#10);
        sl.Clear;
        sl.Add('y');
        for i := 0 to iLenRange - 1 do
            sl.Add(delegate.makeYString(ys[i]));
        sb.append(sl.DelimitedText + #13#10);

        for d := 0 to iLenDepths - 1 do
        begin
            sb.append(#13#10 + tostr(round1(m2f(depths(d),UWAOpts.DispFeet))) + cDelim); {depth}

            sl.Clear;
            for i := 0 to iLenRange - 1 do
                sl.add(self.formatOutputLevelFunc(convertLevel(maCrossSecLevels[i,d]),'n')); {levels}
            sb.append(sl.DelimitedText);
        end;//d

        if bWriteSpectra then
        begin
            {write out the individual freq components.}
            sb.append(#13#10#13#10 + 'Frequency components'#13#10'====================');
            for fi := 0 to lenfi - 1 do
            begin
                {we don't use the getfstring function to get the freq string, due to the space between 63 hz
                but 1khz - doesn't work.}
                sb.append(#13#10#13#10 + 'Frequency = ' + tostr(TSpectrum.getFbyInd(startfi + fi,bw)) + ' Hz'#13#10);

                for d := 0 to iLenDepths - 1 do
                begin
                    sb.append(#13#10 + tostr(round1(m2f(depths(d),UWAOpts.DispFeet))) + cDelim); //depth

                    sl.Clear;
                    for i := 0 to iLenRange - 1 do
                        sl.add(self.formatOutputLevelFunc(convertLevel(amCrossSecSpectra[fi,i,d]),'n')); {spectra levels}
                    sb.append(sl.DelimitedText);
                end;//d
            end;//fi
        end; //bWriteSpectra
    end; //RA in RAs, RA.solved

    THelper.WriteStringToFile(filename,sb.ToString);
end;

function  TScenario.levelsString(const thisZInd: integer; const thisLevelsDisplay: TLevelsDisplay; const sNaN: string; const cDelim: char): string;
var
    i,j: integer;
    sl: ISmart<TStringList>;
    thisLev: single;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);

    sl := TSmart<TStringList>.create(TStringList.Create);
    sl.Delimiter := cDelim; // delimiter char

    {use downto, so that y direction is reversed}
    for j := theRA.jMax - 1 downto 0 do
    begin
        sl.Clear;
        for i := 0 to theRA.iMax - 1 do
        begin
            thisLev := theRA.getLevAtInds(thisZInd,i,j,-1,
                                          thisLevelsDisplay,
                                          NaNSeafloorLevels);
            sl.add(self.formatOutputLevelFunc(thisLev,sNaN));
        end;//i
        sb.Append(sl.DelimitedText + #13#10);
    end; //j
    result := sb.ToString;
end;

procedure TScenario.writeMaxLevelsToFile(const filename: String; const cDelim: char);
begin
    if not resultsExist() then
        showmessage('No levels to write')
    else if (theRA.iMax < 2) or (theRA.jMax < 2) or (self.dMax < 2) then
        showmessage('X, Y or Z grid is to small')
    else
        THelper.WriteStringToFile(filename,HeaderForExport(cDelim) + self.levelsString(0, ldMax, 'n', cDelim));
end;

function TScenario.ESRIHeaderForLevels(const showErrorMessages: boolean): string;
const
    cDelim: char = ' ';
    sNODATA = '-9999';
    dMaxAspectRatio = 1.05;
var
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    sb.append('NCOLS        ' + tostr(theRA.iMax) + #13#10);
    sb.append('NROWS        ' + tostr(theRA.jMax) + #13#10);
    sb.append('XLLCORNER    ' + forceDotSeparator( delegate.makeXString(theRA.pos1.X)) + #13#10); //XLLCENTER?
    sb.append('YLLCORNER    ' + forceDotSeparator( delegate.makeYString(theRA.pos1.Y)) + #13#10); //YLLCENTER?
    if (max(theRA.dx,theRA.dy) / min(theRA.dx,theRA.dy)) < dMaxAspectRatio then
        sb.append('CELLSIZE     ' + forceDotSeparator(tostr(sqrt(theRA.dx * theRA.dy))) + #13#10)
    else
    begin
        if showErrorMessages then
            showmessage(wraptext('DX/DY grid aspect ratio too high, using non-standarrd DX and DY parameters in place of CELLSIZE parameter. These parameters may not be fully supported by all GIS software'));
        sb.append('DX           ' + forceDotSeparator(tostr(theRA.dx)) + #13#10);
        sb.append('DY           ' + forceDotSeparator(tostr(theRA.dy)) + #13#10);
    end;
    sb.append('NODATA_VALUE ' + sNODATA + #13#10);
    result := sb.ToString;
end;

procedure TScenario.writeLevelsToESRIFile(const filename: String; const thisZInd: integer; const thisLevelsDisplay: TLevelsDisplay; const showErrorMessages: boolean);
const
    cDelim: char = ' ';
    sNODATA = '-9999';
begin
    {have already established that file can be written to}
    if not resultsExist() then
    begin
        if showErrorMessages then
            showmessage('No levels to write')
    end
    else if (theRA.iMax < 2) or (theRA.jMax < 2) or (self.dMax < 2) then
    begin
        if showErrorMessages then
            showmessage('X, Y or Z grid is to small')
    end
    else
        THelper.WriteStringToFile(filename,self.ESRIHeaderForLevels(showErrorMessages) + self.levelsString(thisZInd, thisLevelsDisplay, sNODATA, cDelim));
end;

procedure TScenario.writeContoursToShapefile(const filename: String);

    function getLevsRA(): TSingleMatrix;
    const
        updateNSteps = 25;
    var
        i,j: integer;
        convertedMax, convertedMin: single;
        levDisp: TLevelsDisplay;
    begin
        levDisp := UWAOpts.levelsDisplay;

        convertedMin := convertLevel(UWAOpts.levelLimits.Min) + 0.01;
        convertedMax := convertLevel(UWAOpts.levelLimits.Max);

        setlength(result, theRA.imax, theRA.jmax);
        for i := 0 to theRA.imax - 1 do
            for j := 0 to theRA.jmax - 1 do
                if UWAOpts.levelLimits.MaxOrMinNaN then
                    result[i,j] := nan
                else
                    result[i,j] := TGLForm.levelChecking(getLevAtInds(UWAOpts.displayZInd,i,j,-1,levDisp,NaNSeafloorLevels), convertedMax, convertedMin);
    end;

var
    pss: TPos3ListList;
    ps: TPosPairList;
    xs, ys: TDoubleDynArray;
    i,j,k: integer;
    mbLRA: TBoolMatrix;
    LevsRA: TSingleMatrix;
    shapess: TMyShapeArrayArray;
    prs: TPair<integer,TPos3List>;
    pr: TPair<integer,TPos3>;
    contourLevels: TDoubleDynArray;
begin
        xs := TGLForm.getMarchingSquaresXorYGrid(theRA.imax,
                                          theRA.xs(0),
                                          theRA.xMax);
        ys := TGLForm.getMarchingSquaresXorYGrid(theRA.jmax,
                                          theRA.ys(0),
                                          theRA.yMax);

        for i := 0 to length(xs) - 1 do
            xs[i] := self.delegate.g2w(xs[i]) + self.delegate.getEasting;
        for i := 0 to length(ys) - 1 do
            ys[i] := self.delegate.g2w(ys[i]) + self.delegate.getNorthing;

        LevsRA := getLevsRA();
        setlength(mbLRA,length(LevsRA), length(LevsRA[0]));
        contourLevels := UWAOpts.getLevelContours;
        setlength(shapess, length(contourLevels));
        for k := 0 to length(contourLevels) - 1 do
        begin
            for i := 0 to length(LevsRA) - 1 do
                for j := 0 to length(LevsRA[0]) - 1 do
                    mbLRA[i,j] := LevsRA[i,j] > contourLevels[k];

            if UWAOpts.ResultsTris then
                ps := TMarchingSquares.drawMarchingTrisLines(TMarchingSquares.MarchingTrisBits(mbLRA),xs,ys)
            else
                ps := TMarchingSquares.generateMarchingSquaresLines(TMarchingSquares.MarchingSquaresBits(mbLRA),xs,ys);

            pss := nil;
            try
                pss := ps.toPos3s;
                //pss.connectShapes;

                setlength(shapess[k], pss.count);
                for prs in THelper.iterate<TPos3List>(pss) do
                begin
                    if prs.Value.count < 2 then continue;

                    shapess[k][prs.Key].level := contourLevels[k];

                    setlength(shapess[k][prs.Key].padfX, prs.Value.count);
                    setlength(shapess[k][prs.Key].padfY, prs.Value.count);
                    setlength(shapess[k][prs.Key].padfZ, prs.Value.count);

                    for pr in THelper.iterate<TPos3>(prs.Value) do
                    begin
                        shapess[k][prs.Key].padfX[pr.Key] := pr.Value.x;
                        shapess[k][prs.Key].padfY[pr.Key] := pr.Value.y;
                        shapess[k][prs.Key].padfZ[pr.Key] := 0;
                    end;
                end;

            finally
                ps.free;
                pss.Free;
            end;
        end;

        if not shapess.anyValid then
        begin
            showmessage('No valid shapes found for export');
            exit;
        end;

        TMyShapefileWriter.write(filename, shapess);
end;

procedure TScenario.writeExclusionZoneToShapefile(const filename: String; const src: TSource);
var
    p,j: integer;
    points: TExclusionZonePointArray;
    shapes: TMyShapeArray;
begin
    if (self.theRA.iMax = 0) or (self.theRA.jMax = 0) then exit;
    if not src.srcSolved then exit;

    setlength(points,src.NSlices);

    SetLength(shapes,src.getMovingPosMax);
    for p := 0 to src.getMovingPosMax - 1 do
    begin
        shapes[p].level := UWAOpts.levelLimits.exclusionZoneLevel;

        points := exclusionZoneAtPos(UWAOpts.levelLimits.exclusionZoneLevel,src,p,-1,UWAOpts.exclusionZoneStopAtLand);

        //extend by 1 so loop is complete
        setlength(shapes[p].padfX, length(points) + 1);
        setlength(shapes[p].padfY, length(points) + 1);
        setlength(shapes[p].padfZ, length(points) + 1);
        if length(points) > 0 then
        begin
            for j := 0 to length(points) - 1 do
            begin
                shapes[p].padfX[j] := points[j].p.x + delegate.getEasting;
                shapes[p].padfY[j] := points[j].p.y + delegate.getNorthing;
                shapes[p].padfZ[j] := 0;
            end;

            //last point loops around
            shapes[p].padfX[length(points)] := points[0].p.x + delegate.getEasting;
            shapes[p].padfY[length(points)] := points[0].p.y + delegate.getNorthing;
            shapes[p].padfZ[length(points)] := 0;
        end;
    end;

    if not shapes.anyValid then
    begin
        showmessage('No valid shapes found for export');
        exit;
    end;

    TMyShapefileWriter.write(filename, [shapes]);
end;

procedure TScenario.writeExclusionZoneToESRIFile(const filename: String);
const
    cDelim: CHAR = ' ';
var
    sl: ISmart<TStringList>;
    i,j: integer;
    src: TSource;
    p: integer;
    aExclusionZonePoints: TExclusionZonePointArray;
    points: TFPointArray;
    bm: TBoolMatrix;
    sb: ISmart<TStringBuilder>;
begin
    if (self.theRA.iMax = 0) or (self.theRA.jMax = 0) then exit;

    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);

    setlength(bm, self.theRA.iMax, self.theRA.jMax);
    bm.setAllVal(false);

    for src in sources do
    begin
        if src.SrcSolved then
        begin
            setlength(aExclusionZonePoints,src.NSlices);

            for p := 0 to src.getMovingPosMax - 1 do {loop through for each source position, if it is moving, or just static pos if not}
            begin
                {get a local temp version of the src exclusion zone, to check against theRA limits}
                aExclusionZonePoints := exclusionZoneAtPos(UWAOpts.levelLimits.exclusionZoneLevel,src,p,-1,UWAOpts.exclusionZoneStopAtLand);
                setlength(points, length(aExclusionZonePoints));
                for I := 0 to length(points) - 1 do
                    points[i] := aExclusionZonePoints[i].p;

                for i := 0 to length(bm) - 1 do
                    for j := 0 to length(bm[0]) - 1 do
                        if not bm[i,j] then
                            if TMCKLib.pointInPolygon(points,self.theRA.xs(i),self.theRA.ys(j)) then
                                bm[i,j] := true;
            end; //p
        end; //srcSolved
    end; //src

    sl := TSmart<TStringList>.create(TStringList.Create);
    sl.Delimiter := cDelim; // delimiter char
    sb.Append(self.ESRIHeaderForLevels(false));

    for j := length(bm[0]) - 1 downto 0 do
    begin
        sl.Clear;
        for i := 0 to length(bm) - 1 do
            sl.add(ifthen(bm[i,j],'1','0'));
        sb.Append(sl.DelimitedText + #13#10);
    end;

    THelper.WriteStringToFile(filename,sb.ToString);
end;

procedure TScenario.setWeightingInd(const aVal: integer; const curve: TFishCurve);
begin
    if aVal <> weightingInd then
    begin
        {range checking of the val is done in the updateweighting function}

        fWeightingInd := aVal;
        updateWeighting(curve);
        theRA.clearLevelsCache;
    end;
end;

procedure TScenario.setSourcesUseGriddedTL(const val: boolean);
begin
    if val <> fMovingSourcesUseGriddedTL then
    begin
        //TODO can we change this at any time?
        fMovingSourcesUseGriddedTL := val;
    end;
end;

function TScenario.getBandwidth: TBandwidth;
begin
    result := self.bandwidth;
end;

function TScenario.getBellhopNRays: integer;
begin
    result := self.fBellhopNRays;
end;

function TScenario.getBellhopNRaysAzimuth: integer;
begin
    result := self.fBellhopNRaysAzimuth;
end;

function TScenario.getCoherent: boolean;
begin
    result := self.coherent;
end;

function TScenario.getpCancelSolve: TWrappedBool;
begin
    result := delegate.getPCancelSolve;
end;

function TScenario.getRayTracingEndAngle: double;
begin
    result := self.rayTracingStartAngle;
end;

function TScenario.getRayTracingInitialStepsize: double;
begin
    result := self.rayTracingInitialStepsize;
end;

function TScenario.getRayTracingStartAngle: double;
begin
    result := self.rayTracingEndAngle;
end;

procedure TScenario.setTheRAToEntireArea;
begin
    theRA.setPos(0,0,delegate.getxMax,delegate.getyMax,theRA.Pos1.Z);
end;

function TScenario.canShowProbeTimeSeries: boolean;
begin
    result := self.anySourceMoving and not self.movingSourcesUseGriddedTL;
end;

procedure TScenario.checkSourcesInRA;
var
    src: TSource;
    tmpX, tmpY: double;
begin
    for src in self.Sources do
    begin
        src.setRMax(delegate.getWorldscale); //also clears all levels

        tmpX := src.Pos.X;
        tmpY := src.Pos.Y;
        forceRange(tmpX, theRA.Pos1.X, theRA.Pos2.X);
        forceRange(tmpY, theRA.Pos1.Y, theRA.Pos2.Y);

        src.setPos(tmpX,tmpY,src.Pos.Z);
    end;
end;

procedure TScenario.setDMax(const aVal: integer; const resizeNow: boolean = true);
var
    src: TSource;
    log: ISynLog;
    val: integer;
begin
    log := TSynLogLevs.Enter(self,'setDMax');
    val := max(5, aVal);
    log.log(sllInfo,'Setting scenario.dMax to %',[val]);
    if val <> dMax then
    begin
        fDMax := val;
        if assigned(theRA) then
            theRA.clearLevelsCache;

        if resizeNow then
        begin
            for src in self.Sources do
                src.initSoundLevs();
        end;
    end;
end;

procedure TScenario.setSourcesNSlices(const aVal: integer; const resizeNow: boolean = true);
var
    src: TSource;
begin
    //if aVal < 5 then
    //    aVal := 5;
    if aVal <> SourcesNSlices then
    begin
        fSourcesNSlices := aVal;
        for src in Sources do
            src.setnslices(SourcesNSlices,resizeNow);
    end;
end;

procedure TScenario.setSourcesIMax(const aVal: integer; const resizeNow: boolean = true);
var
    src: TSource;
begin
    //if aVal < 5 then
    //    aVal := 5;
    if aVal <> SourcesIMax then
    begin
        fSourcesIMax := aVal;
        for src in Sources do
            src.setiMax(SourcesIMax, resizeNow);
    end;
end;

function TScenario.dz: double;
begin
    if dmax < 2 then
        result := 0
    else
        result := bathyDelegate.getzMax / (dMax - 1); //not sure if this should be (DMax - 1)
end;

function  TScenario.depths(const ind: integer):double;
begin
    result := ind*dz;
end;

function  TScenario.depthsArray(): TDoubleDynArray;
var
    d: integer;
begin
    setlength(result,self.dMax);
    for d := 0 to self.dMax - 1 do
        result[d] := self.depths(d);
end;

procedure TScenario.initSampleScenario;
begin
    sources.Clear;
    self.addSource(111566, 111515, 5);
    debugInit; //only does something if we are in debug mode
    resetCProfile;

    //center the probe in theRA so it looks nice
    if theRA.probes.Count > 0 then
        theRA.probes.Last.setPos((theRA.Pos2.X - theRA.Pos1.X) / 2 , (theRA.Pos2.Y - theRA.Pos1.Y) / 2, 0);
end;

procedure TScenario.debugInit;
{$IFDEF DEBUG}
var
    src: TSource;
    fi: integer;
begin
    {make the source spectrum all 200 dB - NB this isn't the correct way to do it,
    as the equipreturn is not set, and so saving and loading won't pick up the spectrum}
    src := sources.last;

    //this will correctly set the src.equipreturn so that saving, loading works
    src.srcDBData.EquipmentReturns[0].name := 'Debug test';
    src.srcDBData.EquipmentReturns[0].count := 1;
    src.srcDBData.EquipmentReturns[0].duty := 1;
    for fi := 0 to TSpectrum.getLength(bwOctave) - 1 do
        src.srcDBData.EquipmentReturns[0].spectrum.setAmpbyInd(fi,200,bwOctave);
    //src.srcDBData.EquipmentReturns[length(src.equipmentReturns) - 1].spectrum.cloneFrom(src.EquipmentReturns[0].spectrum);
    src.spectrumFromEquipmentReturn;
    src.SrcSpectrumSet := true;
end;
{$ELSE}
begin
end;
{$ENDIF}

function TScenario.printInfo(const cDelim: char; const maxSourceCount, maxProbeCount: integer): TStringlist;
    procedure myAdd(const key,value: string);
    begin
        result.AddObject(key,TWrappedString.create);
        TWrappedString(result.Objects[result.Count - 1]).str := value;
    end;
var
    src: TSource;
    i,fi: integer;
    prb: TProbe;
    probeSpect: ISmart<TSpectrum>;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    result := TStringlist.Create(true);

    myAdd('Name',name);
    myAdd('Active',ifthen(delegate.isCurrentScenario(self),'Yes',''));
    myAdd('Comments',comment);
    myAdd('Grid x points',tostr(theRa.iMax));
    myAdd('Grid y points',tostr(theRa.jMax));
    myAdd('Depth z points',tostr(dMax));
    myAdd('Source radial slices',tostr(SourcesNSlices));
    myAdd('Source range points',tostr(SourcesIMax));
    myAdd('Level type',TLevelConverter.levelTypeName(levelType));
    myAdd('Uses time series',ifthen(TLevelConverter.isTimeSeries(levelType),'Yes','No'));

    if self.SplitSolver then
    begin
        myAdd('Solvers',self.LowSolver.toStr + ' and ' + self.HighSolver.toStr);
        myAdd('Freq range',TSpectrum.getFStringbyInd(startfi,bandwidth) + 'Hz: ' +
                           TSpectrum.getFStringbyInd(SplitSolverfi,bandwidth) + 'Hz: ' +
                           TSpectrum.getFStringbyInd(endfi,bandwidth) + 'Hz');
    end
    else
    begin
        myAdd('Solvers',self.Solver.toStr);
        myAdd('Freq range',TSpectrum.getFStringbyInd(startfi,bandwidth) + 'Hz: ' +
                           TSpectrum.getFStringbyInd(endfi,bandwidth) + 'Hz');
    end;
    myAdd('Bandwidth',TSpectrum.bandwidthString(bandwidth));

    myAdd('Weighting name',weighting.Name);
    for fi := 0 to lenfi - 1 do
        sb.Append(ifthen(isNaN(weighting.Spectrum.getMitigationAmpbyInd(startfi + fi,bandwidth)),'-' + cDelim,
                        tostr(saferound(weighting.Spectrum.getMitigationAmpbyInd(startfi + fi,bandwidth))) +
                        ' dB' + ifthen(fi < lenfi - 1,cDelim,'')));
    myAdd('Weighting',sb.ToString);

    //myAdd('No. sources',tostr(self.Sources.Count));
    for i := 0 to maxSourceCount - 1 do
    begin
        sb.Clear;
        if i < sources.count then
        begin
            src := Sources[i];
            sb.Append(src.name);

            if not src.Enabled then
                sb.Append('inactive')
            else if not isnan(src.srcLevel) then
                sb.Append(': ' + self.formatLevelForSource(src));

            if src.isMovingSource then
                sb.Append(' moving, ' + tostr(src.getMovingPosMax) + ' points')
            else
                sb.Append(' static');
            //s := s + ifthen(i = Sources.count - 1,'',', ');
        end;

        if i = 0 then
            myAdd('Sources',sb.ToString)
        else
            myAdd('',sb.ToString);
    end;


    //myAdd('No. probes',tostr(self.theRA.probes.Count));
    //theRa.updateProbes;
    for i := 0 to maxProbeCount - 1 do
    begin
        sb.Clear;
        if i < theRA.probes.count then
        begin
            prb := theRa.probes[i];
            sb.Append(prb.Name);
            if prb.Enabled then
            begin
                probeSpect := TSmart<TSpectrum>.create(TSpectrum.create);
                if UWAOpts.LevelsDisplay = ldMax then
                    theRA.getSpectAtXY(probeSpect,prb.Pos.x,prb.Pos.y,UWAOpts.LevelsDisplay,UWAOpts.DisplayZInd,true)
                else
                    theRA.getSpectAtXY(probeSpect,prb.Pos.x,prb.pos.y,UWAOpts.LevelsDisplay,saferound(prb.Pos.Z / self.dz),true);

                if not isnan(probeSpect.Level) then
                    sb.Append(': ' + self.formatLevel(probeSpect.Level));
            end
            else
                sb.Append('inactive');
            //s := s + ifthen(i = theRa.probes.count - 1,'',', ');
        end;
        if i = 0 then
            myAdd('Probes',sb.ToString)
        else
            myAdd('',sb.ToString);
    end;
end;

function TScenario.solversText: String;
begin
    if self.SplitSolver then
        result := self.LowSolver.toStr + ' and ' + self.HighSolver.toStr
    else
        result := self.Solver.toStr;
end;

function TScenario.maxSourceMovingPos: integer;
var
    src: TSource;
    p: integer;
begin
    result := 0;
    for src in Sources do
    begin
        p := src.getMovingPosMax;
        if p > result then
            result := p;
    end;
end;

function TScenario.maxSourceMovingTime: double;
var
    src: TSource;
    T: double;
begin
    result := NaN;
    for src in Sources do
    begin
        T := src.getMaxMovingTime;
        if not isNan(T) then
        begin
            if isnan(result) then
                result := T
            else
                if T > result then
                    result := T;
        end;
    end;
end;

function TScenario.getSeabed: TSeafloorScheme;
begin
    result := self.propsMap.selected.seabed;
end;

function TScenario.getSeabedsAtAngle(const aPos: TFPoint3; const aAngle, rmax, dr: double): TSeafloorRangeArray;
var
    r, cosA, sinA: double;
    i: integer;
    lp, oldLP: TLocationProperties;
begin
    lp := propsMap.closest(aPos.x, aPos.y);
    oldLP := lp;

    setlength(result, 1);
    result[0].range := 0;
    result[0].seafloor := lp.seabed;

    cosA := cos(aAngle);
    sinA := sin(aAngle);
    for i := 1 to ceil(rmax / dr) - 1 do
    begin
        r := i * dr;
        lp := propsMap.closest(aPos.x + r * cosA, aPos.y + r * sinA);
        if lp <> oldLP then
        begin
            oldLP := lp;
            setlength(result, length(result) + 1);
            result[length(result) - 1] := TSeafloorRange.create(r,lp.seabed);
        end;
    end;
end;

function TScenario.getSeafloorNaNs: boolean;
begin
    result := self.fNaNSeafloorLevels;
end;

//function  TScenario.getSeafloorSchemeAtPoint(const aX,aY: double): TSeafloorScheme;
//begin
//    result := self.propsMap.closest(aX, aY).seabed;
//end;

function TScenario.getSolver: TCalculationMethod;
begin
    result := self.solver;
end;

function TScenario.getSources: TSourceList;
begin
    result := self.sources;
end;

function TScenario.getSplitSolver: boolean;
begin
    result := self.splitSolver;
end;

function TScenario.getStartfi: integer;
begin
    result := self.startfi;
end;

function TScenario.getStartTime: TDateTime;
begin
    Result := startTime;
end;

function TScenario.getStopMarchingSolverAtLand: boolean;
begin
    result := stopMarchingSolverAtLand;
end;

function TScenario.getWater: TWaterProps;
begin
    result := self.propsMap.selected.water;
end;

function TScenario.getWeighting: TFishCurve;
begin
    result := self.weighting;
end;

function TScenario.getXMax: double;
begin
    result := theRA.xMax;
end;

function TScenario.getyMax: double;
begin
    result := theRA.yMax;
end;

function  TScenario.topLayerMaterial(const aX,aY: double): TMaterial;
begin
    result := self.propsMap.closest(aX,aY).seabed.layers.First.material;
end;

//procedure TScenario.addDefaultSeafloorScheme;
//begin
//    seabed.seafloorSchemes.add(TSeafloorscheme.defaultScheme);
//    fSingleSeafloorScheme := true;
//end;

function TScenario.getAssessmentPeriod(): double;
begin
    if anySourceMoving then
        result := maxSourceMovingTime
    else
        result := fAssessmentPeriod;
end;

procedure  TScenario.setAssessmentPeriod(const d: double);
var
    src: TSource;
begin
    if fassessmentPeriod <> d then
    begin
        fassessmentPeriod := d;
        for src in self.sources do
            src.displayAssessmentPeriod := fassessmentPeriod;
        if isCurrent then
            UWAOpts.levelLimits.updateCmaxCmin();
    end;
end;

procedure TScenario.setLevelType(const levType: TLevelType);
begin
    if flevelType <> levType then
    begin
        flevelType := levType;

        if isCurrent() then
            UWAOpts.levelLimits.updateCmaxCmin();
    end;
end;

function  TScenario.convertLevel(const lev: single): single;
begin
    if TLevelConverter.isTimeSeries(levelType) then
    begin
        result := lev; //currently, the source discards the timeseries info and returns levels in the freq band slots that represent whatever leveltype was used during the calc. so we can't convert from one type to another
    end
    else
        result := TLevelConverter.convert(lev, kSPL, levelType, assessmentPeriod);
end;

function  TScenario.unconvertLevel(const lev: single): single;
begin
    if TLevelConverter.isTimeSeries(levelType) then
    begin
        result := lev; //currently, the source discards the timeseries info and returns levels in the freq band slots that represent whatever leveltype was used during the calc. so we can't convert from one type to another
    end
    else
        result := TLevelConverter.convert(lev, levelType, kSPL, assessmentPeriod);
end;

function TScenario.formatLevel(const lev: single): string;
begin
    result := tostrWithTestAndRound1(self.convertLevel(lev)) +
              ' dB' + TLevelConverter.levelTypeName(self.levelType);
end;

function TScenario.formatLevelForSource(const src: TSource): string;
begin
    if IsNan(src.srcLevel) or (src.srcLevel < lowLev) then
        result := '-'
    else
    begin
        result := tostrWithTestAndRound1(TLevelConverter.convertSourceLevel(src.srcLevel,
                                                                            kSL,
                                                                            src.displayLevelType,
                                                                            assessmentPeriod,
                                                                            src.srcSpectrumCrestFactor)) +
                  ' dB' + TLevelConverter.sourceLevelTypeName(src.displayLevelType);
    end;
end;

procedure TScenario.commentsForFrame(const frame: TFrame; const commentsList: TCommentlist);
var
    comment: TComment;
begin
    commentsList.Clear;
    for comment in self.comments do
        if comment.frameClassName = frame.ClassName then
            commentsList.add(comment);
end;

//procedure TScenario.readCompressedSourceLevels(const scenInd: integer; const stream: TMemoryStream);
//var
//    i: integer;
//begin
//    for i := 0 to sources.count - 1 do
//        sources[i].readCompressedFromStream(i, scenInd, stream);
//end;

procedure TScenario.writeCompressedSourceLevels(const scenInd: integer; const stream: TMemoryStream);
var
    p: TPair<integer,TSource>;
begin
    for p in THelper.iterate<TSource>(self.sources) do
        p.Value.writeCompressedToStream(p.Key, scenInd, stream);
end;

procedure TScenarioList.readDataFromXMLNode(const parentNode: IDOMNode; const srcLevels: TStringArray; const sSaveVersion: string; const scenInd: integer; const stream: TMemoryStream);
begin

end;

procedure TScenarioList.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    TDBSeaObject.writeIntegerField(parentNode,'ScenariosCount',self.count);
    ChildNode := TDBSeaObject.writeListField(parentNode,'ScenariosList');
    for i := 0 to self.Count - 1 do
        self[i].writeDataToXMLNode(ChildNode.AddChild('Scenario' + tostr(i)));
end;

end.
