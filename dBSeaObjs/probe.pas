unit probe;

{$B-}
interface

uses system.strutils, system.math,
    xml.XMLIntf, xml.xmldom,
    vcl.graphics,
    generics.collections,
    dBSeaObject, helperFunctions, baseTypes, pos3,
    spectrum,
      objectCopier, ProbeTimeSeries;

type

{$RTTI INHERIT}
    TProbe = class(TdBSeaObject)
        { class for a 'probe' which is placed somewhere within a receiver area, so as
          to give more control over looking at results. it would be nice to get rid of this
          and be querying the levels in the RA directly, however there is a fair amount of other
          data (colours, visibility, name) that it would be rather annoying to hold within
          the RA as there are a variable number of probes. It would also be nice to get levels
          directly from the parent RA instead of having probe.spectrum updated from the RA, but that
          would mean reaching back to the RA from within the probe}
    private
        fName      : String;
        fVisible   : boolean; // queried on graphics draw. this is set true and never altered, but let's keep it in case we want it in future
        fPos       : TPos3;
        fColor     : TColor;
        bAltColor  : boolean;
        fAlpha     : double; // opacity

        fEnabled   : boolean;
        fUndoPos   : TMatrix;
    public
    const
        Origcolor: TColor = $4d4d6a;//$FF1496;
        AltColor: TColor = $32FF32;
        DisabledColor: TColor = $00466E;
    var
        constructor Create(const aX, aY, aZ: double; ID, iUndoMovesMax: integer);
        destructor Destroy; override;
        procedure cloneFrom(const source: TProbe);
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;

        function  setPos(const aX, aY, aZ: double): boolean;
        function  dragPos(const aX, aY: double): boolean;
        function  UndoMove: boolean;
        procedure rotateUndoMoves(const isForward: boolean);

        function  setColor(const aCol: TColor): boolean;
        function  setAltColor(const aVal: boolean): boolean;
        function  Color: TColor;

        property  Name: String read fName write fName;
        property  Visible: boolean read fVisible write fVisible;
        property  Pos: TPos3 read fPos;
        property  Alpha: Double read fAlpha write fAlpha;
        property  Enabled: boolean read fEnabled write fEnabled;
    End;
    TProbeList = class(TObjectList<TProbe>)
        procedure writeDataToXMLNode(const parentNode: IXMLNode);
        procedure readDataFromXMLNode(const parentNode: IDOMNode; const iUndoMovesMax: integer);
        procedure cloneFrom(const source: TProbeList; const iUndoMovesMax: integer);
    end;

implementation

constructor TProbe.Create(const aX, aY, aZ: double; ID, iUndoMovesMax: integer);
var
    i: integer;
begin
    inherited Create;

    self.SaveBlacklist := 'fPos,fUndoPos';

    self.Name := 'Probe ' + tostr(ID + 1);
    self.fPos := TPos3.Create(aX, aY, aZ);
    self.fColor := self.origcolor;
    self.alpha := 1.0;
    self.visible := true; // now we are always showing probes
    self.Enabled := true;

    setlength(fUndoPos,3,iUndoMovesMax);
    for i := 0 to iUndoMovesMax - 1 do
    begin
        fUndoPos[0,i] := pos.X;
        fUndoPos[1,i] := pos.Y;
        fUndoPos[2,i] := pos.Z;
    end;
end;

destructor TProbe.Destroy;
begin
    fPos.Free;
    inherited;
end;

function TProbe.setPos(const aX, aY, aZ: double): boolean;
var
    tmpX, tmpY, tmpZ: double;
begin
    tmpX := pos.X;
    tmpY := pos.Y;
    tmpZ := pos.Z;

    //checking whether position is within theRA is done by the caller
    result := fPos.setPos(aX, aY, aZ);
    if result then
    begin
        rotateUndoMoves(true);
        fUndoPos[0,0] := tmpX;
        fUndoPos[1,0] := tmpY;
        fUndoPos[2,0] := tmpZ;
    end;
end;

function TProbe.dragPos(const aX, aY: double): boolean;
begin
    //checking whether position is within theRA is done by the caller

    {check that the probe is only being moved within the RA and not outside it -
    the forcerange function makes sure it is within those bounds}
//    tmpX := aX;
//    tmpY := aY;
//    forceRange(tmpX, prob.theRA.Pos1.X, prob.theRA.Pos2.X);
//    forceRange(tmpY, prob.theRA.Pos1.Y, prob.theRA.Pos2.Y);
    result := fPos.setPos(aX,aY,self.Pos.Z);
end;

function TProbe.UndoMove: boolean;
begin
    result := fPos.setPos(fUndoPos[0,1],fUndoPos[1,1],fUndoPos[2,1]);

    if result then
        rotateUndoMoves(false);
end;

procedure TProbe.rotateUndoMoves(const isForward: boolean);
var
    i,j: integer;
begin
    if isForward then
    begin
        for i := (length(fUndoPos[0]) - 1) downto 1 do
            for j := 0 to length(fUndoPos) - 1 do
                fUndoPos[j,i] := fUndoPos[j,i - 1]
    end
    else
    begin
        for i := 0 to (length(fUndoPos[0]) - 2) do
            for j := 0 to length(fUndoPos) - 1 do
                fUndoPos[j,i] := fUndoPos[j,i + 1];
    end;
end;


function TProbe.setColor(const aCol: TColor): boolean;
begin
    result := false;
    if Color <> aCol then
    begin
        fColor := aCol;
        result := true;
//        if assigned(GLWin) then
//            GLWin.setRedraw; //forcedrawnow causes flashing
    end;
end;

function TProbe.setAltColor(const aVal: boolean): boolean;
begin
    result := false;
    if aVal <> self.bAltColor then
    begin
        self.bAltColor := aVal;
        result := true;
//        if assigned(GLWin) then
//            GLWin.setRedraw; //forceDrawnow causes flashing
    end;
end;

function  TProbe.Color: TColor;
begin
    result := ifthen(bAltColor,AltColor,fColor);
end;

procedure TProbe.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;
    if not assigned(parentNode) then exit;

    self.Pos.writeDataToXMLNode(parentNode.AddChild('Pos'));
end;

procedure TProbe.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    //read TPos3 info
    childnode := (parentNode as IDOMNodeSelect).selectNode('Pos');
    if assigned(childnode) then
        self.Pos.readDataFromXMLNode(childNode);
end;

procedure TProbe.cloneFrom(const source: TProbe);
begin
    TObjectCopier.CopyObject(source,self);

    Pos.cloneFrom(source.Pos);
end;

{ TProbeList }

procedure TProbeList.cloneFrom(const source: TProbeList; const iUndoMovesMax: integer);
var
    p: TProbe;
begin
    self.Clear;
    for p in source do
    begin
        self.Add(TProbe.Create(0.0, 0.0, 0.0, 0, iUndoMovesMax));
        self.Last.cloneFrom(p);
    end;
end;

procedure TProbeList.readDataFromXMLNode(const parentNode: IDOMNode; const iUndoMovesMax: integer);
var
    childNode: IDOMNode;
    Sel: IDOMNodeSelect;
begin
    if not assigned(parentNode) then exit;
    Sel := parentNode as IDOMNodeSelect;

    self.Clear;
    repeat
        childNode := Sel.selectNode('ProbesList/Probe' + tostr(self.count));
        if assigned(childNode) then
        begin
            self.Add(TProbe.Create(0.0, 0.0, 0.0, 0, iUndoMovesMax));
            self.Last.readDataFromXMLNode(childNode);
        end;
    until not assigned(childNode);
end;

procedure TProbeList.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    inherited;

    if not assigned(parentNode) then exit;

    TDBSeaObject.writeIntegerField(parentNode,'ProbesCount',self.Count);
    ChildNode := TDBSeaObject.writeListField(parentNode,'ProbesList');
    for i := 0 to self.Count - 1 do
        self[i].writeDataToXMLNode(ChildNode.AddChild('Probe' + tostr(i)));
end;


end.
