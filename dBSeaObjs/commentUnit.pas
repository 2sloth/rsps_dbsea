unit commentUnit;

{$B-}
interface

uses SynCommons,
    system.SysUtils, system.math,
    xml.XMLIntf, xml.xmldom,
    vcl.forms,
    generics.collections,
    dBSeaObject, objectCopier, basetypes, MCKLib,
    helperFunctions, CSVDelimiters, dBSeaconstants, esriReader,
    eventlogging;

type
{$RTTI INHERIT}
    TComment = Class(TdBSeaObject)
    private
    public
        x,y: integer;
        comment: String;
        frameClassName: String;

        constructor Create(const aComment:String; const aX, aY: integer; const aFrame: TFrame);
        destructor Destroy; Override;
        procedure cloneFrom(const source: TComment);
    End;
    TCommentList = TObjectList<TComment>;

implementation

{$B-}

constructor TComment.Create(const aComment:String; const aX, aY: integer; const aFrame: TFrame);
begin
    inherited Create;

    self.SaveBlacklist := '';

    self.x := ax;
    self.y := ay;
    self.comment := aComment;
    self.frameClassName := aFrame.ClassName;
end;

destructor TComment.Destroy;
begin
    inherited;
end;

procedure TComment.cloneFrom(const source: TComment);
begin
    TObjectCopier.CopyObject(source,self);
end;


end.
