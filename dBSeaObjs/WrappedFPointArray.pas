unit WrappedFPointArray;

interface

uses system.SysUtils,
    system.generics.collections,
    xml.XMLIntf, xml.xmldom,
    basetypes, dbseaobject, objectcopier;

type

    TWrappedFPointArray = class(TDBSeaObject)
    public
        points: TFPointArray;

        constructor Create();
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TWrappedFPointArray);

        function center: TFPoint;
    end;
    TWrappedFPointArrayList = class(TObjectList<TWrappedFPointArray>)
        procedure writeDataToXMLNode(const parentNode: IXMLNode; const fieldName: string);
        procedure readDataFromXMLNode(const parentNode: IDOMNode; const fieldName: string);
        procedure cloneFrom(const source: TWrappedFPointArrayList);
    end;

implementation

function TWrappedFPointArray.center: TFPoint;
var
    p: TFPoint;
begin
    if length( points) = 0 then exit;

    for p in self.points do
    begin
        result.x := result.x + p.x;
        result.y := result.y + p.y;
    end;

    result.x := result.x / length(points);
    result.y := result.y / length(points);
end;

procedure TWrappedFPointArray.cloneFrom(const source: TWrappedFPointArray);
var
    i: integer;
begin
    setlength(self.points, 0);
    setlength(self.points, length(source.points));
    for i := 0 to length(source.points) - 1 do
    begin
        self.points[i].x := source.points[i].x;
        self.points[i].y := source.points[i].y;
    end;
end;

constructor TWrappedFPointArray.Create;
begin
    inherited;

    self.SaveBlacklist := 'points';
end;

procedure TWrappedFPointArray.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
    Sel: IDOMNodeSelect;
begin
    inherited;

    setlength(points,0);

    if not assigned(parentNode) then exit;

    Sel := parentNode as IDOMNodeSelect;

    childNode := Sel.selectNode('points');
    if assigned(childNode) then
        self.points.deserialise(readStringField(sel, 'points'));
end;

procedure TWrappedFPointArray.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;

    if not assigned(parentNode) then exit;

    writeStringField(parentNode, 'points', points.serialise);
end;

{ TWrappedFPointArrayList }

procedure TWrappedFPointArrayList.cloneFrom(const source: TWrappedFPointArrayList);
var
    i: integer;
begin
    self.Clear;
    for i := 0 to source.Count - 1 do
    begin
        self.Add(TWrappedFPointArray.create());
        self.Last.cloneFrom(source[i]);
    end;
end;

procedure TWrappedFPointArrayList.readDataFromXMLNode(const parentNode: IDOMNode; const fieldName: string);
var
    childNode: IDOMNode;
    Sel: IDOMNodeSelect;
begin
    if not assigned(parentNode) then exit;

    Sel := parentNode as IDOMNodeSelect;

    self.Clear;
    repeat
        childNode := Sel.selectNode(fieldName +  'sList/' + fieldName + inttostr(self.count));
        if assigned(childNode) then
        begin
            self.Add(TWrappedFPointArray.create());
            self.Last.readDataFromXMLNode(childNode);
        end;
    until not assigned(childNode);
end;

procedure TWrappedFPointArrayList.writeDataToXMLNode(const parentNode: IXMLNode; const fieldName: string);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    TDBSeaObject.writeIntegerField(parentNode, fieldName + 'sCount',self.Count);
    ChildNode := TDBSeaObject.writeListField(parentNode, fieldName + 'sList');
    for i := 0 to self.Count - 1 do
        self[i].writeDataToXMLNode(ChildNode.AddChild(fieldName + inttostr(i)));
end;

end.
