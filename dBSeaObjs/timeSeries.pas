unit timeSeries;

{$B-}
interface

uses system.SysUtils, system.Math, system.Classes, system.types, system.strutils,
    generics.Collections,
    xml.xmldom, xml.XMLIntf,
    baseTypes,
    helperFunctions, dBSeaObject, objectCopier, dBSeaconstants, mckLib,
    levelconverter, DSP, FilterDef, uTextendedX87, wavReader;


type

{$RTTI INHERIT}

    EDownsamplingMismatch = class(Exception);

    TSparseSample = class(TObject)
    public
        amplitude: double;
        sample: integer;  //delay from t = 0, in samples

        constructor Create(const aSample: integer; const aAmp: double);
    end;
    TSparseSampleList = class(TObjectList<TSparseSample>)
    public
        fs: double; //nb downsampling
        downsampledOctaves: integer;

        constructor create(const ownsObjects: boolean; const fs: double; const downsampledOctaves: integer);

        procedure multiplyBy(const d: double);
    end;

    TTimeSeries = class(TdBSeaObject)
    public
        fs: double;      //sampling freq Hz
        samples: TDoubleDynArray;  //pressure Pa
        count: double;
        downsampledOctaves: integer;

        constructor Create(const aFs: double; const downsampledOctaves: integer);
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TTimeSeries);

        procedure readSamplesFromString(const s: string; const fadeInLength: integer); //values on separate lines
        procedure setWithWavReader(const wr: TWAVReader);

        function  isZero: boolean;
        function  SPLpeak: single; //dB re 1uPa
        function  SPLpeaktopeak: single; //dB re 1uPa
        function  SEL: single; overload;
        function  level(const levType: TLevelType): single;

        function  directConvole(const otherSamples: TDoubleDynArray): TDoubleDynArray;
        function  sparseTimeSeries(const lowAmpCutoff: double): TSparseSampleList;

        procedure clearSamples();
        procedure addSamples(const ts: TTimeSeries);
        function  downsampleTo(const newFS: double): TTimeSeries;
        procedure downsample1Octave;
        procedure upsample1Octave;

        class function SEL(const samples: TDoubleDynArray; const fs: double): single; overload;
    end;


implementation

constructor TSparseSample.Create(const aSample: integer; const aAmp: double);
begin
    sample := aSample;
    amplitude := aAmp;
end;

constructor TTimeSeries.Create(const aFs: double; const downsampledOctaves: integer);
begin
    inherited Create;

    self.fs := aFs;
    self.clearSamples;
    self.count := 1;
    self.downsampledOctaves := downsampledOctaves;
end;

procedure TTimeSeries.readDataFromXMLNode(const parentNode: IDOMNode);
var
    ChildNode: IDomNode;
begin
    inherited;

    //explicitly read samples, as RTTI doesn't work well with dynamic arrays
    childnode := (parentNode as IDOMNodeSelect).selectNode('samples');
    if assigned(childnode) then
        samples.deserialise(childnode.firstChild.nodeValue);
end;

procedure TTimeSeries.clearSamples;
begin
    setlength(samples,1);
    samples[0] := 0; //null
end;

procedure TTimeSeries.cloneFrom(const source: TTimeSeries);
begin
    TObjectCopier.CopyObject(source,self);
    //samples := arrayCopy(source.samples); //not necessary
end;

function  TTimeSeries.isZero: boolean;
var
    i: integer;
begin
    result := true;
    for i := 0 to length(samples) - 1 do
        if samples[i] <> 0 then
            exit(false);
end;

procedure TTimeSeries.readSamplesFromString(const s: string; const fadeInLength: integer);
var
    m: TMatrix;
    i: integer;
begin
    m.fromDelimitedString(s,',',true);
    samples := m.row(0);
    for i := 0 to fadeInLength - 1 do
        samples[i] := samples[i] * (i + 1) / fadeInLength;
end;

function TTimeSeries.SPLpeak: single;
var
    mx,mn: single;
begin
    mx := max(0,samples.max);
    mn := min(0,samples.min);
    if isnan(mx) or isnan(mn) or ((mx = 0) and (mn = 0)) then
        result := nan
    else
        result := 20*log10( max(mx,-mn) * 1e6 ); //re 1 uPa
end;

function TTimeSeries.SPLpeaktopeak: single;
var
    mx,mn: single;
begin
    mx := max(0,samples.max);
    mn := min(0,samples.min);
    if isnan(mx) or isnan(mn) or (mx = mn) then
        result := nan
    else
        result := 20*log10( (mx - mn) * 1e6 ); //re 1 uPa
end;

procedure TTimeSeries.upsample1Octave;
begin
    self.samples := TDSP.upsampleBy2(self.samples);
    self.fs := 2*self.fs;
    dec(self.downsampledOctaves);
end;

function  TTimeSeries.SEL: single;
begin
    if isnan(count) or (count = 0) then
        result := nan
    else
        result := TTimeseries.SEL(samples,fs) + dB10(count);
end;

function  TTimeSeries.level (const levType: TLevelType): single;
begin
    if isZero then
        exit(nan);

    case levType of
        kSPL,kSELfreq,kSPLCrestFactor: result := nan;
        kSELtime: result := SEL;
        kSPLpk: result := SPLpeak;
        kSPLpkpk: result := SPLpeaktopeak;
    else
        result := nan;
    end;
end;

function  TTimeSeries.directConvole(const otherSamples: TDoubleDynArray): TDoubleDynArray;
var
    i: integer;
    j: integer;
begin
    setlength(result, length(samples) + length(otherSamples) + 1);
    for I := 0 to length(samples) - 1 do
        for j := 0 to length(otherSamples) - 1 do
            result[i + j] := result[i + j] + samples[i] * otherSamples[j];
end;


procedure TTimeSeries.downsample1Octave;
var
    tmpFilt, antialiasFilter: TFilterDef;
begin
    tmpFilt := TDSP.LPF(self.fs,self.fs / 2 * 0.85,0.77);
    antialiasFilter := TDSP.cubeFilter(tmpFilt);
    try
        self.fs := self.fs / 2;
        self.samples := TDSP.downsampleBy2(self.samples, antialiasFilter);
        inc(self.downsampledOctaves);
    finally
        tmpFilt.Free;
        antialiasFilter.Free;
    end;
end;

function TTimeSeries.downsampleTo(const newFS: double): TTimeSeries;
var
    tmpFilt, antialiasFilter: TFilterDef;
begin
    if isnan(newFS) then exit(nil);

    result := TTimeSeries.create(self.fs, self.downsampledOctaves);
    result.samples := self.samples.copy;
    result.count := self.count;

    tmpFilt := TDSP.LPF(self.fs,self.fs / 2 * 0.85,0.77);
    antialiasFilter := TDSP.cubeFilter(tmpFilt);
    try
        repeat
            result.fs := result.fs / 2;
            result.samples := TDSP.downsampleBy2(result.samples, antialiasFilter);
            inc(result.downsampledOctaves);
        until result.fs < newFS * 2;
    finally
        tmpFilt.Free;
        antialiasFilter.Free;
    end;
end;

procedure TTimeSeries.setWithWavReader(const wr: TWAVReader);
begin
    self.fs := wr.fs;
    self.samples := wr.weighted.toDoubleArray;
end;

function  TTimeSeries.sparseTimeSeries(const lowAmpCutoff: double): TSparseSampleList;
var
    i: integer;
    maxAmp: double;
begin
    maxAmp := 0;
    for i := 0 to length(samples) - 1 do
        if abs(samples[i]) > maxAmp then
            maxAmp := abs(samples[i]);

    result := TSparseSampleList.create(true, self.fs, self.downsampledOctaves);
    for i := 0 to length(samples) - 1 do
        if abs(samples[i]) > (maxAmp * lowAmpCutoff) then
            result.Add(TSparseSample.Create(i,samples[i]));
end;

procedure TTimeSeries.addSamples(const ts: TTimeSeries);
var
    i: integer;
    upsampled: TTimeSeries;
begin
    if ts.downsampledOctaves = self.downsampledOctaves then
        upsampled := ts
    else if ts.downsampledOctaves > self.downsampledOctaves then
    begin
        upsampled := TTimeSeries.create(ts.fs, ts.downsampledOctaves);
        upsampled.cloneFrom(ts);

        for I := upsampled.downsampledOctaves - 1 downto self.downsampledOctaves do
            upsampled.upsample1Octave;
    end
    else
        raise EDownsamplingMismatch.Create('Downsampling before adding samples not implemented');


    //if v is longer, extend out internal samples vector
    if length(upsampled.samples) > length(samples) then
        setlength(samples,length(upsampled.samples));

    for i := 0 to min(length(upsampled.samples),length(samples)) - 1 do //v may be shorter than samples
        samples[i] := samples[i] + upsampled.samples[i];

    if upsampled <> ts then
        upsampled.free;
end;

class function TTimeSeries.SEL(const samples: TDoubleDynArray; const fs: double): single;
var
    i: integer;
    sum: TextendedX87;
begin
    if isnan(fs) or (fs = 0) then exit(nan);

    sum := 0;
    for i := 0 to length(samples) - 1 do
        if not isnan(samples[i]) then
            sum := sum + samples[i]*samples[i];

    if sum <= 0 then
        result := nan
    else
        result := 10*log10( sum * 1e12 / fs ); //re 1 uPa, 1 sec T0
end;



constructor TSparseSampleList.create(const ownsObjects: boolean; const fs: double; const downsampledOctaves: integer);
begin
    inherited Create(ownsObjects);

    self.fs := fs;
    self.downsampledOctaves := downsampledOctaves;
end;

procedure TSparseSampleList.multiplyBy(const d: double);
var
    s: TSparseSample;
begin
    if isnan(d) or (d = 1) then exit;
    for s in self do
        s.amplitude := s.amplitude * d;
end;

end.
