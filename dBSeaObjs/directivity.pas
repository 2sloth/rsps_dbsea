unit directivity;

{$B-}
interface

uses system.rtti, system.math, system.types,
    generics.collections,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, baseTypes, objectCopier, MCKLib;

type

{$RTTI INHERIT}

    TDirectivity = class(TDBSeaObject)
    public
        f: double; //TODO careful here, if solvers use multi freqs per band
        //maybe start and end angle?
        directivity: TSingleDynArray;

        constructor Create();
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TDirectivity);
    end;
    TDirectivityList = TObjectList<TDirectivity>;

implementation

constructor TDirectivity.Create();
begin
    inherited Create;

    self.SaveBlacklist := '';

    f := nan;
end;

procedure TDirectivity.readDataFromXMLNode(const parentNode: IDOMNode);
var
    ChildNode: IDomNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    //explicitly read directivity, as RTTI doesn't work well with dynamic arrays
    childnode := (parentNode as IDOMNodeSelect).selectNode('directivity');
    if assigned(childnode) then
        directivity.deserialise(childnode.firstChild.nodeValue);
end;

procedure TDirectivity.cloneFrom(const source: TDirectivity);
begin
    TObjectCopier.CopyObject(source,self);
end;

end.

