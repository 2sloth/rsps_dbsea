unit problemContainer;

{$B-}
interface

uses syncommons,
    system.sysutils, system.classes, system.math, system.types, system.strutils,
    vcl.stdctrls, vcl.comctrls, vcl.dialogs, vcl.forms,
    Generics.Collections,
    xml.xmldom, xml.XMLIntf,
    dBSeaObject, basetypes, objectCopier,
    crossSecEndpoint, scenario, dBSeaconstants,
    MCKLib, helperfunctions, bathymetry, geooverlay,
    soundSource, geoSetup, crossSec,
    levelServer, eventLogging,
    dBSeaOptions, globalsunit, Pos3, solveTimeEstimator,
    receiverArea, flock, BathymetryInterface,
    GeoSetupAndBathyDelegate,
    solveReturn, SmartPointer, imageOverlay;

type

{$RTTI INHERIT}

    //TSolveReturn = (kSuccess,kNoScenariosEnabled);

    TProblemContainer = Class(TdBSeaObject, IBathymetryDelegate, IGeoSetupAndBathyDelegate, ILevelServer, IScenarioDelegate, IImageOverlayDelegate)
    private
    const
        DelimZDepthsStart = '___ZDEPTHS___' + #13#10;
        DelimZDepthsEnd = '___ENDZDEPTHS___';
        DelimScenarioStart = '___SCENARIO';
        DelimScenarioEnd = '___ENDSCENARIO';
        DelimSourceStart = '___SOURCE';
        DelimSourceEnd = '___ENDSOURCE';
        DelimSPLF = 'SPLF___';
    var
        [TNoSave] fBathymetry        : TBathymetry;
        fCurrentScenario   : integer; //todo convert to object
        fIsSolving         : boolean;
        fCancelSolve       : TWrappedBool;

        //delegate methods
        function  getLevAtInds(const d, i, j, p: integer; const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean): single;
        procedure resetCProfile();
        procedure afterBathyOrWorldscaleChange(const scaleBy: double = 1);
        function  depthIsAboveBathymetryAtXY(const d, aX, aY: double): boolean;
        function  getTide: double;

        function  getzMax: double;
        function  isCurrentScenario(const scen: TDBSeaObject): boolean;
        function  getEasting: double;
        function  getNorthing: double;
        function  diagonalLength3D(const worldscale: double): double;
        function  worldscale: double;
        function  g2w(const a: double): double;
        function  makeXString(const aVal: double): String;
        function  makeYString(const aVal: double): String;
        function  getCrossSec: TCrossSec;
        function  getPCancelSolve: TWrappedBool;
        function  getxMax: double;
        function  getYMax: double;
        function  getWorldscale: double;
        function  getDepthsMatrix: TSingleMatrix;

    public
        [TNoSave] crossSec: TCrossSec;
        [TNoSave] savedCrossSecs: array [0..8] of TCrossSec;
        [TNoSave] geoSetup: TGeoSetup;
        [TNoSave] scenarios: TScenarioList;
        [TNoSave] levelServer: TLevelServer;
        [TNoSave] geoOverlays: TGeoOverlays;
        [TNoSave] image: TImageOverlay;
        [TNoSave] serialisedOnInit: String;
        //scenarioDiff: TScenarioDiff; //don't need to cache

        constructor Create();
        destructor Destroy(); Override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode; const sUncompressed: String; const sSaveVersion: string; const stream: TMemoryStream); reintroduce;

        procedure clearAndMakeNew(const aDepth: double = nan);
        class function  delimitedBathymetryFromUncompressedString(const sUncompressed: String):String;
        class function  searchBetweenStrings(const sStart,sEnd, sHaystack: string): string;

        function  scenario(): TScenario;
        procedure addScenario();
        procedure duplicateCurrentScenario();
        procedure deleteScenario(ind: integer);
        procedure deleteCurrentScenario();
        procedure setCurrentScenario(const ind: integer);
        procedure initSampleProblem(const lblSolving: TLabel; const pbSolveProgress: TProgressbar);

        procedure resetCrossSecs();
        procedure makeCrossSecInRA();
        procedure saveXSec(const ind: integer);
        procedure recallXSec(const ind: integer);
        procedure nameXSec(const ind: integer; const aName: string);
        procedure setRAsToEntireArea();
        procedure checkSourcesInRAs();
        procedure checkProbesInRAs();
        function  getBathymetryVector(const aX, aY, angle, rmax: double; const extendPastEdge: integer): TMatrix;
        function  getBathymetryVector2(const aX, aY, angle, rmax: double; const extendPastEdge: integer): TRangeValArray;
        function  getLastWaterDist(const aX, aY, angle, rmax: double): double;
        function  getBathymetryAtPos(const ax,ay: double; const logging: boolean = true): single;
        procedure bathymetryFromFile(const filename: TFilename; const lblSolving: TLabel; const pbSolveProgress: TProgressbar);
        procedure clearLevelsCaches;

        function  crossSecReady(out shouldRedrawGL: boolean): boolean;
        function  numberScenariosSolveEnabled(): integer;
        function  estimateMemUsage(): int64;

        //procedure readCompressedSourceLevels(const stream: TMemoryStream);
        procedure writeCompressedSourceLevels(const stream: TMemoryStream);
        procedure writeCompressedBathyToStream(const stream: TMemoryStream);
        //function  getBathyStringForCompressionBeforeVersion7(const suppliedBathymetryString: pstring): string;
        //function  getStringForCompression(const suppliedBathymetryString: pstring): string;
        procedure ESRIHeader(out header, sNoData: String);
        procedure writeBathyToESRIFile(const filename: String);

        procedure solveNow(const infoMemo: TMemo; const infoLabel: TLabel; const solveTimeEstimator: TSolveTimeEstimator; const lbSolving,lbSolveExtraInfo,lbSliceNumber,lbSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const setProgressBars: TProcedureWithDouble; const mmSolverMessages: TMemo; const updateRemainingSolveTime: TProcedureWithBoolBool; const return: TSolveReturn);

        //-------properties------------------------------
        property  currentScenario: integer read fCurrentScenario write setCurrentScenario;
        property  isSolving: boolean read fIsSolving write fIsSolving;
        property  bathymetry: TBathymetry read fBathymetry;
        property  cancelSolve: TWrappedBool read fCancelSolve;
    end;

var
    container: TProblemContainer;

implementation

uses ImportedLocationUnit;

constructor TProblemContainer.Create;
var
    i: integer;
    log: ISynLog;
begin
    inherited Create;

    log := TSynLogLevs.Enter(self,'Create');

    self.SaveBlacklist := 'crossSec,savedCrossSecs,CrossSecEndpoints,fBathymetry,geoSetup,scenarios' +
        ',scenarioDiff,levelServer,fIsSolving,fCancelSolve';

    scenarios := TScenarioList.Create(true);

    geoSetup := TGeoSetup.Create(self);
    fBathymetry := TBathymetry.Create(self);

    addScenario;

    crossSec := TCrossSec.Create;
    for i := 0 to length(savedCrossSecs) - 1 do
        savedCrossSecs[i] := TCrossSec.Create;

    levelServer := TLevelServer.create(self);

    fIsSolving := false;
    fCancelSolve := TWrappedBool.create();
    fCancelSolve.b := false;

    geoOverlays := TGeoOverlays.create(true);

    image := TImageOverlay.create(self);
end;

destructor TProblemContainer.Destroy;
var
    xsec: TCrossSec;
begin
    scenarios.Clear;
    scenarios.Free;
    crossSec.Free;
    for xsec in savedCrossSecs do
        xsec.Free;
    levelServer.free;
    geoSetup.Free;
    bathymetry.Free;
    fCancelSolve.free;
    geoOverlays.Free;
    image.free;

    inherited;
end;


procedure TProblemContainer.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
    sourcesCountList: TIntegerDynArray;
begin
    inherited;
    if not assigned(parentNode) then exit;

    scenarios.writeDataToXMLNode(parentNode);
//    {writing scenarios}
//    writeIntegerField(parentNode,'ScenariosCount',scenarios.count);
//    ChildNode := writeListField(parentNode,'ScenariosList');
//    for i := 0 to scenarios.Count - 1 do
//        scenarios[i].writeDataToXMLNode(ChildNode.AddChild('Scenario' + tostr(i)));

    //as we are saving the calced levels separately, we need to note down how many sources each scenario has
    setlength(sourcesCountList,scenarios.Count);
    for i := 0 to scenarios.Count - 1 do
        sourcesCountList[i] := scenarios[i].Sources.Count;
    ChildNode := parentNode.AddChild('SourcesCountList');
    ChildNode.Attributes['type'] := 'string';
    ChildNode.Text := sourcesCountList.serialise;

    crossSec.writeDataToXMLNode(parentNode.AddChild('CrossSec'));

    writeIntegerField(parentNode,'SavedCrossSecsCount',length(savedCrossSecs));
    ChildNode := writeListField(parentNode,'SavedCrossSecsList');
    for i := 0 to length(savedCrossSecs) - 1 do
        savedCrossSecs[i].writeDataToXMLNode(ChildNode.AddChild('SavedCrossSec' + tostr(i)));

    geoSetup.writeDataToXMLNode(parentNode.AddChild('GeoSetup'));
    bathymetry.writeDataToXMLNode(parentNode.AddChild('Bathymetry'));
    geoOverlays.writeDataToXMLNode(parentNode);

    image.writeDataToXMLNode(parentNode.AddChild('Image'));
end;

procedure TProblemContainer.readDataFromXMLNode(const parentNode: IDOMNode; const sUncompressed: String; const sSaveVersion: string; const stream: TMemoryStream);
var
    childNode: IDOMNode;
    Sel: IDOMNodeSelect;
    i,iScenario,iSource: integer;
    scenariocnt: integer;
    srcLevels: TStringArray;
    tmpCurrentScenario: integer;
    sourceCounts: TIntegerDynArray;
    sScenarioSourceLevels: String;
    //delimitedBathymetry: string;
    log: ISynLog;
begin
    inherited readDataFromXMLNode(parentNode);

    log := TSynLogLevs.Enter(self,'readDataFromXMLNode');

    if not assigned(parentNode) then exit;

    Sel := parentNode as IDOMNodeSelect;

    childNode := Sel.selectNode('GeoSetup');
    if assigned(childNode) then
        geoSetup.readDataFromXMLNode(childNode);

    childNode := Sel.selectNode('Bathymetry');
    if assigned(childNode) then
        bathymetry.readDataFromXMLNode(childNode);

    childNode := Sel.selectNode('Image');
    if assigned(childNode) then
        image.readDataFromXMLNode(childNode);

    //get bathymetry from the delimitedBathymetry string
    log.log(sllInfo,'Reading bathymetry from decompressed string');
    if isFloat(sSaveVersion) and (tofl(sSaveVersion) >= 7.0) then
        bathymetry.readCompressedFromStream(stream)
    else
        bathymetry.BathymetryFromString(self.delimitedBathymetryFromUncompressedString(sUncompressed));

    crossSec.resetEndpoints();
    bathymetry.updateZMax();

    childNode := Sel.selectNode('CrossSec');
    if assigned(childNode) then
        crossSec.readDataFromXMLNode(childNode);

    geoOverlays.readDataFromXMLNode(parentNode);

    for I := 0 to length(savedCrossSecs) - 1 do
    begin
        childNode := Sel.selectNode('SavedCrossSecsList/SavedCrossSec' + tostr(i));
        if assigned(childNode) then
            savedCrossSecs[i].readDataFromXMLNode(childNode);
    end;

    {read the number of scenarios}
    scenariocnt := 0;
    childNode := Sel.selectNode('ScenariosCount');
    if assigned(childNode) then
        scenariocnt := toint(childNode.firstChild.nodeValue);

    {read the numbers of sources}
    childNode := Sel.selectNode('SourcesCountList');
    if assigned(childNode) then
        sourceCounts.deserialise(childNode.firstChild.nodeValue);

    {write the correct data to src, then add it to the list of sources}
    tmpCurrentScenario := fCurrentScenario;  //store this away, but set the current scenario to 0 as it can be called during all the resetting that goes on
    fCurrentScenario := 0;
    //remove all sources except for 1
    while scenarios.Count > 1 do
        deleteCurrentScenario;

    {deal out each scenario}
    log.log(sllInfo,'Reading % scenarios',[scenariocnt]);
    for iScenario := 0 to scenariocnt - 1 do
    begin
        childNode := Sel.selectNode('ScenariosList/Scenario' + tostr(iScenario));
        if assigned(childNode) then
        begin
            //we already have 1 scenario, but for more scenarios, create a scenario
            if iScenario > (scenarios.Count - 1) then
                scenarios.Add(TScenario.Create(self,self));

            if (isfloat(sSaveVersion) and (tofl(sSaveVersion) < 7.0)) or not (isFloat(sSaveVersion)) then
            begin
                sScenarioSourceLevels := searchBetweenStrings(DelimScenarioStart + tostr(iScenario) + #13#10,
                                                  DelimScenarioEnd + tostr(iScenario),sUncompressed);

                setlength(srcLevels,sourceCounts[iScenario]);
                for iSource := 0 to sourceCounts[iScenario] - 1 do
                    srcLevels[iSource] := searchBetweenStrings(DelimSourceStart + tostr(iSource) + DelimSPLF + #13#10,
                                                               DelimSourceEnd + tostr(iSource) + DelimSPLF,sScenarioSourceLevels);
            end
            else
                setlength(srcLevels, 0);

            //overwrite the scenario with the correct data
            scenarios.Last.readDataFromXMLNode(childNode,srcLevels,sSaveVersion,iScenario,stream);
        end;
    end;

    fCurrentScenario := tmpCurrentScenario;
end;

procedure TProblemContainer.clearAndMakeNew(const aDepth: double = nan);
begin
    //remove all scenarios but 1
    while scenarios.Count > 1 do
        deleteCurrentScenario;

    self.geoSetup.init;
    importedLocation.clear;
    self.bathymetry.init(aDepth);

    self.addScenario; //add a new blank scenario to the end of the list
    self.deleteScenario(0); //delete the old scenario from first position

    self.scenario.debugInit;
end;

class function TProblemContainer.delimitedBathymetryFromUncompressedString(const sUncompressed: String):String;
begin
    result := searchBetweenStrings(DelimZDepthsStart,DelimZDepthsEnd,sUncompressed);
end;

class function TProblemContainer.searchBetweenStrings(const sStart,sEnd, sHaystack: string): string;
var
    aPos1,aPos2: int64;
begin
    Result := '';
    aPos1 := Pos(sStart,sHaystack);
    aPos2 := Pos(sEnd,sHaystack);
    if aPos2 <= (aPos1 + length(sStart)) then Exit;

    setlength(result,aPos2 - aPos1);

    result := midStr(sHaystack,aPos1 + length(sStart),aPos2 - (aPos1 + length(sStart)));
end;


function TProblemContainer.scenario: TScenario;
begin
    if (scenarios.Count < 1) then
    begin
        //showmessage('Error: no scenarios found. Creating default scenario');
        eventlog.log('Error: no scenarios found. Creating default scenario');
        scenarios.Add(TScenario.Create(self,self));
        exit(scenarios.First);
    end;

    if (currentScenario < 0) or (currentScenario >= scenarios.Count) then
    begin
        //showmessage('Error: incorrect scenario index passed to main_container. Showing first scenario');
        eventlog.log('Error: incorrect scenario index ' + tostr(currentScenario) + ' passed to main_container. Showing first scenario');
        exit(scenarios.First);
    end;

    result := scenarios[currentScenario];
end;

procedure TProblemContainer.addScenario;
var
    scen: TScenario;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'addScenario');

    scenarios.Add(TScenario.Create(self,self));
    scenarios.Last.setTheRAToEntireArea;
    fCurrentScenario := scenarios.Count - 1;
    //set source to the middle of the area
    if scenario.Sources.Count > 0 then
        scenario.Sources.First.setPos((scenario.theRA.Pos2.X - scenario.theRA.Pos1.X) / 2,
                                      (scenario.theRA.Pos2.Y - scenario.theRA.Pos1.Y) / 2,
                                      scenario.Sources.First.Pos.Z);
    if scenario.theRA.probes.Count > 0 then
        scenario.theRA.probes.First.setPos((scenario.theRA.Pos2.X - scenario.theRA.Pos1.X) / 2,
                                           (scenario.theRA.Pos2.Y - scenario.theRA.Pos1.Y) / 2,
                                           scenario.theRA.probes.First.Pos.Z);

    //try to avoid name clashes
    for scen in scenarios do
        if scen <> scenario then
            if scen.Name = scenario.Name then
                scenario.Name := 'New ' + scenario.Name;
end;

function TProblemContainer.diagonalLength3D(const worldscale: double): double;
begin
    result := self.bathymetry.diagonalLength3D(worldscale);
end;

procedure TProblemContainer.duplicateCurrentScenario;
var
    sourceScenario: TScenario;
begin
    sourceScenario := scenario;
    scenarios.Add(TScenario.Create(self,self));
    scenarios.Last.cloneFrom(sourceScenario);
    scenarios.Last.Name := scenarios.Last.Name + ' copy';
    fCurrentScenario := scenarios.Count - 1;
    eventLog.log('Duplicate scenario ' + sourceScenario.name);
end;

procedure TProblemContainer.deleteScenario(ind: integer);
var
    newInd: integer; // the scenario that will be selected after the delete
    log: ISynLog;
begin
    if scenarios.Count < 2 then exit;
    if (ind < 0) or (ind >= scenarios.Count) then exit;

    log := TSynLogLevs.Enter(self,'deleteScenario');

    if ind = 0 then
        newInd := 0
    else
        newInd := ind - 1;

    eventLog.log('Delete scenario ' + self.scenarios[ind].name);
    if scenarios.Count > 1 then
    begin
        scenarios.Delete(ind);
        fCurrentScenario := newInd;
    end;

    if assigned(UWAOpts) then
        UWAOpts.updateCMaxCMinandLevelColors();
end;

procedure TProblemContainer.deleteCurrentScenario;
begin
    deleteScenario(currentScenario);
end;

procedure TProblemContainer.InitSampleProblem(const lblSolving: TLabel; const pbSolveProgress: TProgressbar);
var
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'InitSampleProblem');
    //Log.Log(sllInfo,'setting startfi %',[val]);

    eventLog.log('Create sample scenario');
    {create a bathymetry depth map}
    if fileExists(TempPath + defaultBathymetryFile) then
        bathymetry.BathymetryFromFile(TempPath + defaultBathymetryFile,lblSolving,pbSolveProgress)
    else
        showmessage('Unable to find default bathymetry file at ' + TempPath + defaultBathymetryFile +
                        ', leaving blank bathymetry in place');

    scenario.createRAAndSource;
    scenario.initSampleScenario;
end;

function TProblemContainer.isCurrentScenario(const scen: TDBSeaObject): boolean;
begin
    result := TScenario(scen) = self.scenario;
end;

procedure TProblemContainer.resetCrossSecs;
var
    i: integer;
begin
    crossSec.resetEndpoints;
    for i := 0 to length(savedCrossSecs) - 1 do
        savedCrossSecs[i].resetEndpoints;
end;

procedure TProblemContainer.makeCrossSecInRA;
var
    i: integer;
begin
    for i := 0 to 1 do
    begin
        if crossSec.endpoints[i].Pos.X < 0 then
            crossSec.endpoints[i].Pos.setPos(0,crossSec.endpoints[i].Pos.Y,crossSec.endpoints[i].Pos.Z);
        if crossSec.endpoints[i].Pos.X > scenario.theRA.xMax then
            crossSec.endpoints[i].Pos.setPos(scenario.theRA.xMax,crossSec.endpoints[i].Pos.Y,crossSec.endpoints[i].Pos.Z);
        if crossSec.endpoints[i].Pos.Y < 0 then
            crossSec.endpoints[i].Pos.setPos(crossSec.endpoints[i].Pos.X,0,crossSec.endpoints[i].Pos.Z);
        if crossSec.endpoints[i].Pos.Y > scenario.theRA.yMax then
            crossSec.endpoints[i].Pos.setPos(crossSec.endpoints[i].Pos.X,scenario.theRA.yMax,crossSec.endpoints[i].Pos.Z);
    end;
end;

function TProblemContainer.makeXString(const aVal: double): String;
begin
    result := self.geoSetup.makeXString(aVal);
end;

function TProblemContainer.makeYString(const aVal: double): String;
begin
    result := self.geoSetup.makeYString(aVal);
end;

procedure TProblemContainer.saveXSec(const ind: integer);
var
    aName: string;
begin
    if ind < 0 then exit;
    if ind >= length(savedCrossSecs) then exit;

    aname := savedCrossSecs[ind].Name;
    savedCrossSecs[ind].cloneFrom(crossSec);
    savedCrossSecs[ind].Name := aname;
end;

procedure TProblemContainer.recallXSec(const ind: integer);
begin
    if ind < 0 then exit;
    if ind >= length(savedCrossSecs) then exit;

    if not savedCrossSecs[ind].crossSecReady then
    begin
        showmessage('Cross section not ready');
        exit;
    end;

    crossSec.cloneFrom(savedCrossSecs[ind]);
end;

procedure TProblemContainer.nameXSec(const ind: integer; const aName: string);
begin
    if ind < 0 then exit;
    if ind >= length(savedCrossSecs) then exit;

    savedCrossSecs[ind].Name := aName;
end;

procedure TProblemContainer.resetCProfile;
var
    scen: TScenario;
begin
    for scen in scenarios do
        scen.resetCProfile;
end;

procedure TProblemContainer.setRAsToEntireArea;
var
    scen: TScenario;
begin
    for scen in scenarios do
        scen.setTheRAToEntireArea;
end;

procedure TProblemContainer.checkSourcesInRAs;
var
    scen: TScenario;
begin
    for scen in scenarios do
        scen.checkSourcesInRA;
end;

procedure TProblemContainer.checkProbesInRAs;
var
    scen: TScenario;
begin
    for scen in scenarios do
        scen.theRA.checkProbesInRA;
end;


procedure TProblemContainer.setCurrentScenario(const ind: integer);
var
    log: ISynLog;
begin
    if ind = currentScenario then exit;
    if ind < 0 then exit;
    if ind >= scenarios.Count then exit;

    log := TSynLogLevs.enter(self,'setCurrentScenario');

    fCurrentScenario := ind;

    if assigned(UWAOpts) then
        UWAOpts.updateCMaxCMinandLevelColors();
end;

procedure TProblemContainer.afterBathyOrWorldscaleChange(const scaleBy: double = 1);
var
    scen: TScenario;
begin
    setRAsToEntireArea;
    for scen in  scenarios do
    begin
        scen.clearLevelsCache;
        scen.scaleSourcePositions(scaleBy);
        scen.theRA.scaleProbePositions(scaleBy);
    end;
    checkSourcesInRAs;
    checkProbesInRAs;
end;

function  TProblemContainer.crossSecReady(out shouldRedrawGL: boolean): boolean;
var
    src: TSource;
    tmpX1,tmpX2,tmpY: double;
begin
    {if the cross sec points are not ready (ie have the same position), then
    set them apart - point 1 at source 0, point 2 straight out to the right}
    shouldRedrawGL := false;
    result := crossSec.crossSecReady;

    if not result then
    begin
        if scenario.sources.Count < 1 then
        begin
            tmpX1 := scenario.theRa.Pos1.X;
            tmpX2 := scenario.theRa.Pos1.X + geoSetup.worldScale;
            tmpY := scenario.theRA.Pos1.Y;
        end
        else
        begin
            src := scenario.Sources.First;
            tmpX1 := src.Pos.X;
            tmpX2 := src.Pos.X + geoSetup.worldScale;
            tmpY := src.Pos.Y;
        end;
        forceRange(tmpY, scenario.theRA.Pos1.Y, scenario.theRA.Pos2.Y);

        forceRange(tmpX1, scenario.theRA.Pos1.X, scenario.theRA.Pos2.X);
        if crossSec.endpoints[0].setPos(tmpX1,tmpY) then
            shouldRedrawGL := true;

        forceRange(tmpX2, scenario.theRA.Pos1.X, scenario.theRA.Pos2.X);
        if crossSec.endpoints[1].setPos(tmpX2,tmpY) then
            shouldRedrawGL := true;
    end;
end;

procedure TProblemContainer.SolveNow(const infoMemo: TMemo;
                                     const infoLabel: TLabel;
                                     const solveTimeEstimator: TSolveTimeEstimator;
                                     const lbSolving,lbSolveExtraInfo,lbSliceNumber,lbSolveSourcePosition: TLabel;
                                     const stepProgressBars: TProcedureWithInt;
                                     const setProgressBars: TProcedureWithDouble;
                                     const mmSolverMessages: TMemo;
                                     const updateRemainingSolveTime: TProcedureWithBoolBool;
                                     const return: TSolveReturn);
var
    scen: TScenario;
    prevActiveScenario: integer;
    s: string;
    Log: ISynLog;
begin
    Log := TSynLogLevs.Enter(self,'SolveNow');

    fCancelSolve.b := false;

    if numberScenariosSolveEnabled = 0 then
    begin
        //showmessage('No scenarios are set to solve');
        return.errors.add(TSolveError.create(kNoScenariosEnabled, 'No scenarios are set to solve', -1, -1));
        exit;
    end;

    isSolving := true;
    try
        infoLabel.Caption := '';
        infoLabel.Visible := true;
        infoMemo.Text := '';

        prevActiveScenario := currentScenario;
        for scen in scenarios do
        begin
            if scen.solveEnabled then
            begin
                infoLabel.Caption := 'Scenario ' + tostr(scenarios.IndexOf(scen) + 1) + ': ' + scen.Name +
                                                            '   (' + tostr(scenarios.IndexOf(scen) + 1) + ' of ' +
                                                            tostr(scenarios.Count) + ', ' + tostr(numberScenariosSolveEnabled) + ' solving)';

                fCurrentScenario := self.scenarios.IndexOf(scen);
                scen.SolveNow(self.scenarios.IndexOf(scen),
                    return,
                    solveTimeEstimator,
                    lbSolving,lbSolveExtraInfo,lbSliceNumber,lbSolveSourcePosition,
                    stepProgressBars,
                    setProgressBars,
                    mmSolverMessages,
                    updateRemainingSolveTime);

                if return.messageBuffer.Count > 0 then
                begin
                    infoMemo.Visible := true;
                    for s in return.messageBuffer do
                        infoMemo.Text := infoMemo.Text + s + #13#10;
                    return.messageBuffer.Clear;
                    application.processmessages;
                end;
            end;
        end;
        fcurrentScenario := prevActiveScenario;
        infoMemo.Visible := length(infoMemo.Text) <> 0;

        if assigned(UWAOpts) then
        begin
            UWAOpts.levelLimits.DeleteUserSetLimits();
            UWAOpts.updateCMaxCMinAndLevelColors();
        end;

        //cleanupOldPools;
    finally
        isSolving := false;
        infoLabel.Caption := '';
        infoLabel.Visible := false;
    end;
end;

function  TProblemContainer.numberScenariosSolveEnabled: integer;
var
    scen: TScenario;
begin
    result := 0;
    for scen in scenarios do
        if scen.solveEnabled then
            inc(result);
end;


function  TProblemContainer.estimateMemUsage: int64;
var
    scen: TScenario;
begin
    result := 0;
    for scen in scenarios do
        result := result + scen.estimateMemUsage;
end;

//procedure TProblemContainer.readCompressedSourceLevels(const stream: TMemoryStream);
//var
//    i: integer;
//begin
//    for i := 0 to scenarios.count - 1 do
//        scenarios[i].readCompressedSourceLevels(i, stream);
//end;

procedure TProblemContainer.writeCompressedSourceLevels(const stream: TMemoryStream);
var
    i: integer;
begin
    for i := 0 to scenarios.count - 1 do
        scenarios[i].writeCompressedSourceLevels(i, stream);
end;

//function  TProblemContainer.getBathyStringForCompressionBeforeVersion7(const suppliedBathymetryString: pstring): string;
//begin
//    result := #13#10 + DelimZDepthsStart;
//    if assigned(suppliedBathymetryString) then
//        result := result + suppliedBathymetryString^;
//    result := result + #13#10 + DelimZDepthsEnd + #13#10;
//end;

//function  TProblemContainer.getStringForCompression(const useSuppliedBathymetryString: boolean; const suppliedBathymetryString: pstring): string;
////var
////    i,iScenario: integer;
////    src: TSource;
//begin
//    {create the string that will be compressed, containing zdepths and each source splf}
//    result := self.getBathyStringForCompression(suppliedBathymetryString);
//
////    for iScenario := 0 to scenarios.Count - 1 do
////    begin
////        result := result + #13#10 + '___SCENARIO' + tostr(iScenario) + #13#10;
////        for i := 0 to scenarios[iScenario].sources.Count - 1 do
////        begin
////            result := result + #13#10 + '___SOURCE' + tostr(i) + 'SPLF___' + #13#10;
////
////            if includeResults then
////            begin
////                src := scenarios[iScenario].sources[i];
////                result := result + src.getSerialisedTL;
////            end;
////
////            result := result + #13#10 + '___ENDSOURCE' + tostr(i) + 'SPLF___' + #13#10;
////        end;
////        result := result + #13#10 + '___ENDSCENARIO' + tostr(iScenario) + #13#10;
////    end;
//end;

function TProblemContainer.getWorldscale: double;
begin
    result := self.geoSetup.worldScale;
end;

function TProblemContainer.getxMax: double;
begin
    if bathymetry.ZDepthsLen = 0 then
        result := 0
    else
        result := geoSetup.worldscale * bathymetry.xAspectRatio;
end;

function TProblemContainer.getYMax: double;
begin
    if bathymetry.ZDepthsLen = 0 then
        result := 0
    else
        result := geoSetup.worldscale * bathymetry.yAspectRatio;
end;

function TProblemContainer.getzMax: double;
begin
    result := self.bathymetry.zMax;
end;

procedure TProblemContainer.ESRIHeader(out header, sNoData: String);
const
    NODATA: string = '-9999';
begin
    header := '';
    header := header + 'NCOLS        ' + tostr(bathymetry.iMax) + #13#10;
    header := header + 'NROWS        ' + tostr(bathymetry.jMax) + #13#10;
    header := header + 'XLLCORNER    ' + forceDotSeparator( geoSetup.makeXString(0) ) + #13#10; //XLLCENTER?
    header := header + 'YLLCORNER    ' + forceDotSeparator( geoSetup.makeYString(0) ) + #13#10; //YLLCENTER?
    header := header + 'CELLSIZE     ' + forceDotSeparator( tostr(geoSetup.worldScale/bathymetry.ZDepthsLen) ) + #13#10;
    header := header + 'NODATA_VALUE ' + NODATA + #13#10;
    sNoData := NODATA;
end;

function TProblemContainer.g2w(const a: double): double;
begin
    result := self.geoSetup.g2w(a);
end;

function TProblemContainer.worldscale: double;
begin
    result := self.geoSetup.worldScale;
end;

procedure TProblemContainer.writeBathyToESRIFile(const filename: String);
const
    cDelim: CHAR = ' ';
var
    s: string;
    sl: ISmart<TStringList>;
    i,j: integer;
    thisLev: double;
    sNoData: string;
    zDepths: TSingleMatrix;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    {have already established that file can be written to}
    sl := TSmart<TStringList>.create(TStringList.Create);

    sl.Delimiter := cDelim; // delimiter char
    self.ESRIHeader(s,sNodata);
    sb.append(s);

    zDepths := bathymetry.zDepths;
    for j := 0 to bathymetry.jMax - 1 do
    begin
        sl.Clear;
        {add the levels at that point}
        for i := 0 to bathymetry.iMax - 1 do
        begin
            thisLev := zDepths[j,i];
            if isNaN(thisLev) then
                sl.Add(sNODATA)
            else
                sl.Add(forceDotSeparator(tostr(round1(thisLev))));
        end;//i
        sb.append(sl.DelimitedText + #13#10);
    end; //j
    //s := s + #13#10;

    THelper.WriteStringToFile(filename,sb.ToString);
end;

procedure TProblemContainer.writeCompressedBathyToStream(const stream: TMemoryStream);
begin
    bathymetry.writeCompressedToStream(stream);
end;

function  TProblemContainer.getLastWaterDist(const aX, aY, angle, rmax: double): double;
var
    i, li, lj, leni, qi, qj, largerDim: integer;
    xlen, ylen, dist, px, py: single;
    sinAng, cosAng: double;
begin
    {note 'reversed' direction order - ie column first -> j. and also that the y/j direction is in reverse order}
    largerDim := bathymetry.ZDepthsLen;
    li := bathymetry.iMax;
    lj := bathymetry.jMax;

    system.SineCosine(anglewrap(angle), sinAng, cosAng);
    sinAng := -sinAng;

    {get the length of the solution vector in 'bathymetry matrix pixels'}
    xlen := cosAng * rmax / geoSetup.worldscale * largerDim;
    ylen := sinAng * rmax / geoSetup.worldscale * largerDim;
    dist := hypot(xlen,ylen);
    leni := safetrunc(dist); // truncated length to get integer
    if leni < 1 then // if source rmax is too short, we need to get at least 1 'pixel'
        leni := 1;

    px := aX / geoSetup.worldscale * largerDim;
    py := (lj - 1) - aY / geoSetup.worldscale * largerDim;
    { reversed y direction indices }

    for i := 0 to leni - 1 do
    begin
        px := px + cosAng;
        qi := saferound(px);
        if (qi < 0) or (qi >= li) then
            exit(rmax * i / dist);

        py := py + sinAng;
        qj := saferound(py); // reversed direction in y
        if (qj < 0) or (qj >= lj) then
            exit(rmax * i / dist);

        if (bathymetry[qj, qi] <= 0) then
            exit(rmax * i / dist);
    end;
    result := rmax;
end;

function TProblemContainer.getLevAtInds(const d, i, j, p: integer;
  const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean): single;
begin
    result := scenario.theRA.getLevAtInds(d,i,j,p,levelsdisplay,bseafloornans);
end;

function TProblemContainer.getNorthing: double;
begin
    result := self.geoSetup.Northing;
end;

function TProblemContainer.getPCancelSolve: TWrappedBool;
begin
    result := self.fCancelSolve;
end;

function TProblemContainer.getTide: double;
begin
    if scenarios.count < 1 then exit(0);
    result := scenario.tide;
end;

function  TProblemContainer.getBathymetryVector(const aX, aY, angle, rmax: double; const extendPastEdge: integer): TMatrix;
var
    i, li, lj, leni, qi, qj, largerDim: integer;
    xlen, ylen, dist, invDist, px, py: single;
    extendPointsCount: integer;
    sinAng, cosAng: double;
begin
    {return a matrix containing range from source, and the depth under that range
    when going out along angle. blindly returns all depths, doesn't stop for 0 or anything}

    //NB we could use getBathymetryAtPos, but this version accounts for extending past the edge of the map

    extendPointsCount := 0;

    {note 'reversed' direction order - ie column first -> j
    // and also that the y/j direction is in reverse order}
    largerDim := bathymetry.ZDepthsLen;
    li := bathymetry.iMax;
    lj := bathymetry.jMax;

    {get the length of the solution vector in 'bathymetry matrix pixels'}
    // TODO fix these - ie not sure about LargerDim business
    // I'm getting range check errors
    system.SineCosine(anglewrap(angle), sinAng, cosAng);
    xlen := cosAng * rmax / geoSetup.worldscale * largerDim;
    ylen := -sinAng * rmax / geoSetup.worldscale * largerDim;
    dist := hypot(xlen,ylen);
    invDist := 1/dist;
    leni := safetrunc(dist); // truncated length to get integer
    if leni < 1 then // if source rmax is too short, we need to get at least 1 'pixel'
        leni := 1;

    px := aX / geoSetup.worldscale * largerDim;
    py := (lj - 1) - aY / geoSetup.worldscale * largerDim;
    { reversed y direction indices }

    setlength(result, 2, leni);
    for i := 0 to leni - 1 do
    begin
        result[0, i] := rmax * i * invDist; // distance in m from the source
        qi := saferound(px + cosAng * i);
        qj := saferound(py - sinAng * i); // reversed direction in y

        {get depth at that distance.
         range check to make sure we don't go past the edges of the matrix}
        //if ((qi<0) or (qi>(li-1)) or (qj<0) or (qj>(lj-1))) then
        if (not checkRange(qi,0,(li - 1))) or (not checkRange(qj,0,(lj - 1))) then
        begin
            if extendPointsCount < extendPastEdge then
            begin
                if i = 0 then
                    result[1, i] := 0
                else
                begin
                    {extendPastEdge lets us extend the last point out over the edge of theRA - this is useful
                    for some of the solvers, where due to the way the angles of the source slices meet the edge
                    of theRA it can sometimes look strange if the results are cut off exactly at the edge.}
                    result[1, i] := result[1, i - 1];
                    inc(extendPointsCount);
                end;
            end
            else
                result[1, i] := 0;
        end
        else
            result[1, i] := bathymetry[qj, qi];
        {NB reversed indexing on bathymetry!}
    end;
    //i := 0;
end;

function TProblemContainer.getBathymetryVector2(const aX, aY, angle, rmax: double; const extendPastEdge: integer): TRangeValArray;
var
    i, li, lj, leni, qi, qj, largerDim: integer;
    xlen, ylen, dist, invDist, px, py: single;
    extendPointsCount: integer;
    sinAng, cosAng: double;
begin
    {return a matrix containing range from source, and the depth under that range
    when going out along angle. blindly returns all depths, doesn't stop for 0 or anything}

    //NB we could use getBathymetryAtPos, but this version accounts for extending past the edge of the map

    extendPointsCount := 0;

    {note 'reversed' direction order - ie column first -> j
    // and also that the y/j direction is in reverse order}
    largerDim := bathymetry.ZDepthsLen;
    li := bathymetry.iMax;
    lj := bathymetry.jMax;

    {get the length of the solution vector in 'bathymetry matrix pixels'}
    // TODO fix these - ie not sure about LargerDim business
    // I'm getting range check errors
    system.SineCosine(angleWrap(angle), sinAng, cosAng);
    xlen := cosAng * rmax / geoSetup.worldscale * largerDim;
    ylen := -sinAng * rmax / geoSetup.worldscale * largerDim;
    dist := hypot(xlen,ylen);
    invDist := 1/dist;
    leni := safetrunc(dist); // truncated length to get integer
    if leni < 1 then // if source rmax is too short, we need to get at least 1 'pixel'
        leni := 1;

    px := aX / geoSetup.worldscale * largerDim;
    py := (lj - 1) - aY / geoSetup.worldscale * largerDim;
    { reversed y direction indices }

    setlength(result, leni);
    for i := 0 to leni - 1 do
    begin
        result[i].range := rmax * i * invDist; // distance in m from the source
        qi := saferound(px + cosAng * i);
        qj := saferound(py - sinAng * i); // reversed direction in y

        {get depth at that distance.
         range check to make sure we don't go past the edges of the matrix}
        //if ((qi<0) or (qi>(li-1)) or (qj<0) or (qj>(lj-1))) then
        if (not checkRange(qi,0,(li - 1))) or (not checkRange(qj,0,(lj - 1))) then
        begin
            if (extendPointsCount < extendPastEdge) and (i <> 0) then
            begin
                {extendPastEdge lets us extend the last point out over the edge of theRA - this is useful
                for some of the solvers, where due to the way the angles of the source slices meet the edge
                of theRA it can sometimes look strange if the results are cut off exactly at the edge.}
                result[i].val := result[i - 1].val;
                inc(extendPointsCount);
            end
            else
                result[i].val := nan;
        end
        else
            result[i].val := bathymetry[qj, qi]; {NB reversed indexing on bathymetry!}
    end;
end;

function TProblemContainer.getCrossSec: TCrossSec;
begin
    result := self.crossSec;
end;

function TProblemContainer.getEasting: double;
begin
    result := self.geoSetup.Easting;
end;

function TProblemContainer.getDepthsMatrix: TSingleMatrix;
begin
    result := bathymetry.zDepths;
end;

function TProblemContainer.getBathymetryAtPos(const ax,ay: double; const logging: boolean = true): single;
var
    qi, qj, largerDim: integer;
    px, py: double;
    log: ISynLog;
begin
    if isnan(ax) or isnan(ay) then exit(0);
    if (bathymetry.iMax = 0) or (bathymetry.jMax = 0) then exit(0);
    if geoSetup.worldScale = 0 then exit(0);

    {note 'reversed' direction order - ie opposite to what you might at first think
    // and also that the y/j direction is in reverse order}
    largerDim := bathymetry.ZDepthsLen;
    if (largerDim < 2) then exit(0);

    px := aX / geoSetup.worldscale * (largerDim - 1);
    py := (bathymetry.jMax - 1) - aY / geoSetup.worldscale * (largerDim - 1);
    { reversed y direction indices }

    if isnan(px) or isnan(py) or IsInfinite(px) or IsInfinite(py) then exit(0);
    qi := saferound(px);
    qj := saferound(py); // reversed direction in y

    { depth at that point.
    // range check to make sure we don't go past the edges of the matrix}
    //if (not checkRange(qi,0,(li - 1))) or (not checkRange(qj,0,(lj - 1))) then
    if not ((qj > 0) and (qj < bathymetry.jMax) and (qi > 0) and (qi < bathymetry.iMax)) then// checkRange(qj,qi,zDepths)) then
    begin
        if logging then
        begin
            log := TSynLogLevs.Enter(self,'getBathymetryAtPos');
            log.log(sllInfo,'Error: attempted to read depth outside bathymetry area, x: %  y: %',[ax,ay]);
            eventLog.log('Error: attempted to read depth outside bathymetry area, x:' + tostr(ax) + '  y:' + tostr(ay));
        end;
        result := 0;
    end
    else
        result := bathymetry[qj, qi];
    // NB reversed indexing on bathymetry!
end;

function  TProblemContainer.depthIsAboveBathymetryAtXY(const d, aX, aY: double): boolean;
var
    thisDepth: single;
begin
    thisDepth := getBathymetryAtPos(aX,aY);
    if isnan(thisDepth) then exit(false);
    result := d < thisDepth;
end;

procedure TProblemContainer.bathymetryFromFile(const filename: TFilename; const lblSolving: TLabel; const pbSolveProgress: TProgressbar);
var
    scen: TScenario;
begin
    self.geoSetup.utmZone := '';
    bathymetry.bathymetryFromFile(filename,lblSolving,pbSolveProgress);
    self.geoSetup.utmZone := importedLocation.zoneToString;
    resetCrossSecs();
    clearLevelsCaches;
    for scen in self.scenarios do
        scen.setTheRAToEntireArea;
end;

procedure TProblemContainer.clearLevelsCaches;
var
    scen: TScenario;
begin
    for scen in scenarios do
        scen.clearLevelsCache;
end;


end.

