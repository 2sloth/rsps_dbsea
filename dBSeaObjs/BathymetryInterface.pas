unit BathymetryInterface;

interface

uses baseTypes;

type

    IBathymetryDelegate = interface(IInterface)
        function  getzmax: double;
        function  getLastWaterDist(const aX, aY, angle, rmax: double): double;
        function  getBathymetryAtPos(const ax,ay: double; const logging: boolean = true): single;
        function  getBathymetryVector(const aX, aY, angle, rmax: double; const extendPastEdge: integer): TMatrix;
        function  getBathymetryVector2(const aX, aY, angle, rmax: double; const extendPastEdge: integer): TRangeValArray;
        function  depthIsAboveBathymetryAtXY(const d, aX, aY: double): boolean;
        function  getDepthsMatrix: TSingleMatrix;
    end;

implementation

end.
