unit geoSetup;

{$B-}

interface

uses system.math, system.types,
    vcl.dialogs,
    dBSeaObject, objectCopier, helperFunctions,
    scenario, MCKLib, basetypes, dBSeaOptions,
    GeoSetupAndBathyDelegate;

type
{$RTTI INHERIT}
    TGeoSetup = class(TDBSeaObject)
    private
        fWorldScale: double; // scaling from real world (m) to openGL
        fNorthing: double;
        fEasting: double; // used for coordinate systems

        delegate: IGeoSetupAndBathyDelegate;
    public
        utmZone: String;

        constructor Create(const aDelegate: IGeoSetupAndBathyDelegate);
        //destructor Destroy; Override;

        procedure init;

        function makeXString(const aVal: double): string;
        function makeYString(const aVal: double): string;
        function makeZString(const aVal: double): string;
        function getXfromString(const s: string): double;
        function getYfromString(const s: string): double;
        function getZfromString(const s: string): double;

        function g2w(const a: double): double;
        function w2g(const a: double): double;

        procedure setEasting(const aVal: double);
        procedure setNorthing(const aVal: double);
        procedure setWorldScale(const aVal: double); overload;
        procedure setWorldScaleWithPoints(const P1, P2: TFPoint; const aDist: double); overload;
        function  centerToTEastNorth(const nrows, ncols: integer): TEastNorth;

        property worldScale: double read fWorldScale;
        property Northing: double read fNorthing;
        property Easting: double read fEasting;
    end;

implementation

constructor TGeoSetup.Create(const aDelegate: IGeoSetupAndBathyDelegate);
begin
    inherited Create;

    self.SaveBlacklist := 'delegate';

    self.delegate := aDelegate;

    init;
end;

//destructor TGeoSetup.Destroy;
//begin
//    inherited;
//end;

procedure TGeoSetup.init;
begin
    fWorldScale := 10000.0; // converting from world to openGL coordinates
    self.utmZone := '';
end;

procedure TGeoSetup.setWorldScale(const aVal: double);
var
    scaleBy: double;
begin
    if isnan(aVal) then
        exit;
    if aVal = 0 then
        exit;

    if aVal = fWorldScale then
        exit;

    scaleBy := aVal / worldScale;

    fWorldScale := aVal;

    delegate.afterBathyOrWorldscaleChange(scaleBy);
end;

procedure TGeoSetup.setWorldScaleWithPoints(const P1, P2: TFPoint; const aDist: double);
var
    GLDist: double;
    gx1, gx2, gy1, gy2: double;
begin
    { convert to 'gl coords' but with pan and zoom taken out, ie each is then an idealised
      gl pos from -1 to 1. Therefore the easting and northing origin point is at -1,-1 }
    gx1 := w2g(P1.X);
    gx2 := w2g(P2.X);
    gy1 := w2g(P1.Y);
    gy2 := w2g(P2.Y);

    { a GLDist of 2 would be along one entire edge, ie the same as largerDim }
    GLDist := TMCKLib.Distance2D(gx1, gx2, gy1, gy2);

    if GLDist = 0 then
        showmessage('Zero distance between points. Unable to set scale')
    else
        setWorldScale(aDist / (GLDist / 2));
end;

function TGeoSetup.centerToTEastNorth(const nrows, ncols: integer): TEastNorth;
begin
    result.Easting := self.Easting + self.worldScale / 2 * ncols / max(nrows,ncols);
    result.Northing := self.Northing + self.worldScale / 2 * nrows / max(nrows,ncols);
end;

procedure TGeoSetup.setEasting(const aVal: double);
begin
    fEasting := aVal;
end;

procedure TGeoSetup.setNorthing(const aVal: double);
begin
    fNorthing := aVal;
end;

function TGeoSetup.makeXString(const aVal: double): string;
begin
    result := tostr(saferound(m2f(aVal + Easting, UWAOpts.dispFeet)))
end;

function TGeoSetup.makeYString(const aVal: double): string;
begin
    result := tostr(saferound(m2f(aVal + Northing, UWAOpts.dispFeet)))
end;

function TGeoSetup.makeZString(const aVal: double): string;
begin
    result := tostr(saferound(m2f(aVal, UWAOpts.dispFeet)))
end;

function TGeoSetup.getXfromString(const s: string): double;
begin
    if not isFloat(s) then
        exit(NaN);
    result := f2m(tofl(s), UWAOpts.dispFeet) - Easting;
end;

function TGeoSetup.getYfromString(const s: string): double;
begin
    if not isFloat(s) then
        exit(NaN);
    result := f2m(tofl(s), UWAOpts.dispFeet) - Northing;
end;

function TGeoSetup.getZfromString(const s: string): double;
begin
    if not isFloat(s) then
        exit(NaN);
    result := f2m(tofl(s), UWAOpts.dispFeet);
end;

function TGeoSetup.g2w(const a: double): double;
{ openGL to World scale conversion }
begin
    result := (a + 1) * 0.5 * worldScale;
end;

function TGeoSetup.w2g(const a: double): double;
{ World to openGL scale conversion }
begin
    result := ((a / worldScale) * 2) - 1;
end;

end.
