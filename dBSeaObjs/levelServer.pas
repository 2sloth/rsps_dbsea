unit levelServer;

interface

uses
    system.SysUtils, system.Math, system.Classes, system.strutils,
    dBSeaColours, baseTypes, spectrum, fishCurve,
    helperFunctions, dBSeaObject, objectCopier, srcPoint,
    crossSecEndpoint, probe, soundSource, receiverArea, MCKLib,
    dBSeaconstants, pos3, scenario, scenarioDiff;

type

{$RTTI INHERIT}
    ILevelServer = interface(IInterface)
        function getLevAtInds(const d, i, j, p: integer; const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean): single;
    end;

    TLevelServer = class(TdBSeaObject)
    private
        fUseDiff: boolean;
        fIsValid: boolean;
        delegate: ILevelServer;
    public
        diff: TScenarioDiff;

        constructor Create(const aParent: ILevelServer);
        destructor Destroy; Override;

        procedure setDiffScenarios(const s1, s2: TScenario);
        procedure updateDiff(const levelsDisplay: TLevelsDisplay);
        procedure invalidateDiff;
        function isDiff: boolean;

        function getLev(const d, i, j, p: integer; const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean): single;

        property useDiff: boolean read fUseDiff write fUseDiff;
        property isValid: boolean read fIsValid;
    end;


implementation

constructor TLevelServer.Create(const aParent : ILevelServer);
begin
    inherited create;

    self.SaveBlacklist := 'delegate,diff,fUseDiff,fIsValid';

    self.delegate := aParent;
    fUseDiff := false;
    fIsValid := false;
    diff := TScenarioDiff.Create;
end;

destructor TLevelServer.Destroy;
begin
    diff.Free;

    inherited;
end;

function  TLevelServer.getLev(const d, i, j, p: integer;
                              const levelsDisplay: TLevelsDisplay;
                              const bSeafloorNaNs: boolean): single;
begin
    if useDiff then
    begin
        if isValid and diff.scenariosAssigned and diff.diffReady and (i < length(diff.diff)) and (j < length(diff.diff[0])) then
            result := diff.diff[i,j]
        else
            result := nan;
    end
    else
    begin
        //go to the delegate to get the normal level, not the diff
        result := delegate.getLevAtInds(d,i,j,p,levelsDisplay,bSeafloorNaNs);
    end;
end;

procedure  TLevelServer.setDiffScenarios(const s1, s2 : TScenario);
begin
    self.diff.s1 := s1;
    self.diff.s2 := s2;
end;

procedure  TLevelServer.updateDiff(const levelsDisplay : TLevelsDisplay);
begin
    diff.updateDiff(levelsDisplay);
    self.fIsValid := true;
end;

procedure TLevelServer.invalidateDiff;
begin
    self.fIsValid := false;
end;

function TLevelServer.isDiff: boolean;
begin
    result := assigned(self.diff) and self.useDiff;
end;

end.
