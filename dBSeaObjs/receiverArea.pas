unit receiverArea;

{$B-}
interface

uses syncommons,
    system.math, system.sysutils, system.types, system.dateUtils,
    xml.XMLIntf, xml.xmldom,
    vcl.dialogs, vcl.forms, vcl.graphics,
    generics.Collections,
    dBSeaObject, helperFunctions, baseTypes, pos3,
    soundSource, probe, gltypes, spectrum, objectCopier,
    dBSeaconstants, mcomplex, eventLogging, mcklib, griddedTL,
    fishcurve, BathymetryInterface, SmartPointer, ProbeTimeSeries,
    OtlParallel;

type

{$RTTI INHERIT}
    IReceiverAreaDelegate = interface(IInterface)
      function  getSources: TSourceList;
      function  getCoherent: boolean;
      function  getStartfi: integer;
      function  getBandwidth: TBandwidth;
      function  getLenfi: integer;
      function  getWeighting: TFishCurve;
      function  getDepths(const d: integer): double;
      function  getdz: double;
      function  getdmax: integer;
      function  getSeafloorNaNs: boolean;
      function  getStartTime: TDateTime;
    end;

    TLevelAndDepthInd = record
        level: Single;
        di: integer;
    end;

    TReceiverArea = class(TDBSeaObject, IGriddedTLRADelegate)
        { class for a receiver area - the RA is used to sum the sound level contributions
          from all the sources. ie there could be a lot of sources (each with their own levels), and the level that the
          RA ends up with is the sum of all levels from all sources. }
    private
    const
        gridMin: integer = 5;
    var
        fName              : String;
        fPos1              : TPos3; // top left and bottom right corners
        fPos2              : TPos3; // top left and bottom right corners
        fiMax              : integer;  // number of grid points in x
        fjMax              : integer;  // number of grid points in y
        fEnabled           : boolean;

        fLevelsCache: TDoubleField;
        fCacheMovingPosInd: integer;

        [TNoSave] delegate: IReceiverAreaDelegate;
        [TNoSave] bathyDelegate: IBathymetryDelegate;

        function  getLevbyInd(const d, i, j, p: integer; const weightingSpect: TSpectrum; const onlyForSource: TSource): single;
        function  getMaxLevAndDepthIndAtInd(const i,j,p: integer; const bSeafloorNaNs: boolean; const onlyForSource: TSource): TLevelAndDepthInd;
        function  getIMax: integer;
        function  getJMax: integer;
        function  getPos1: TPos3;
        function  getPos2: TPos3;
        procedure getSpectAtXYNoInterp(const theSpect: TSpectrum; const i,j: integer; const LevDisp: TLevelsDisplay; const depthInd: integer);
    public
    const
        Origcolor: TColor = $00FFFF;
        AltColor: TColor = $80FF00;
        DisabledColor: TColor = $00466E;
    var
        probes: TProbeList;

        constructor Create(const aDelegate: IReceiverAreaDelegate; const aBathyDelegate: IBathymetryDelegate);
        destructor Destroy; Override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TReceiverArea);

        procedure setPos(const aX1, aY1, aX2, aY2, Z: double);

        function  getLevAtInds(const d, i, j, p: integer; const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean; const onlyForSource: TSource = nil): single;
        function  getLevAtIndsAndTime(const d, i, j: integer; const time: double): single;
        function  getLevAtXY(const ax,ay: double; const LevDisp: TLevelsDisplay; const depthInd: integer; const interpolate: boolean): double;
        procedure getSpectAtInds(const theSpect: TSpectrum; const i,j: integer; const LevDisp: TLevelsDisplay; const depthInd: integer);
        procedure getSpectAtXY(const theSpect: TSpectrum; const aX,aY: double; const LevDisp: TLevelsDisplay; const depthInd: integer; const interpolate: boolean);
        function  getSPLfAtInds(const d, i, j, p, fi: integer; const bw: TBandwidth; const onlyForSource: TSource = nil): single;

        procedure setIMax(const aVal: integer);
        procedure setJMax(const aVal: integer);
        function  xs(const ind: integer): double;
        function  ys(const ind: integer): double;
        function  xMax: double;
        function  yMax: double;
        function  dx: double;
        function  dy: double;
        function  getXsArray: TDoubleDynArray;
        function  getYsArray: TDoubleDynArray;

        function  checkProbesInRA: boolean;
        procedure scaleProbePositions(const scaleby: double);
        function  addProbe: TProbe;
        procedure deleteProbe(const ind: integer);

        function  probeTimeSeries(const pos: TPos3): TProbeTimeSeries;

        function  levelsCached: boolean;
        procedure clearLevelsCache;
        procedure updateLevelsCache(const p: integer; const setProgressBars: TProcedureWithDouble = nil; const pCancelSolve: TWrappedBool = nil);

        //------------------------------------------------------------------
        property  name: String read fName write fName;
        property  pos1: TPos3 read fPos1 write fPos1;
        property  pos2: TPos3 read fPos2 write fPos2;
        property  iMax: integer read fIMax;
        property  jMax: integer read fJMax;
        property  enabled: boolean read fEnabled write fEnabled;
    End;

implementation

constructor TReceiverArea.Create(const aDelegate: IReceiverAreaDelegate;  const aBathyDelegate: IBathymetryDelegate);
{x1 y1 etc are leftovers from the old way of calling this function,
you can now safely ignore them as the RA is just set to the full size
of the problem area}
begin
    inherited Create;

    self.SaveBlacklist := 'delegate,bathyDelegate' +
                          ',probes' +
                          ',fSPLf,fLev,fMaxLev,fAvgLev' +
                          ',fMaxLevDepth,fPos1,fPos2,fSolved' +
                          ',fLevelsCache,fCacheMovingPosInd'; //removed

    self.delegate := aDelegate;
    self.bathyDelegate := aBathyDelegate;
    self.probes := TProbeList.Create(true);
    self.Name := 'Receiver Area';

    {we create our fPos1 and fPos2 before setting them to the correct xyz}
    self.fPos1 := TPos3.Create(0, 0, -2);
    self.fPos2 := TPos3.Create(0, 0, -2);

    {$IFDEF DEBUG}
    self.fIMax := 20;
    self.fJMax := 20;
    {$ELSE}
    self.fIMax := 100;
    self.fJMax := 100;
    {$ENDIF}

    fCacheMovingPosInd := -1;
    self.Enabled := true;
    self.addProbe;
    checkProbesInRA;
end;

destructor TReceiverArea.Destroy;
begin
    Probes.Clear;
    Probes.Free;
    fpos1.Free;
    fpos2.Free;

    inherited;
end;

procedure TReceiverArea.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;

    if not assigned(parentNode) then exit;

    {write the TPos3 info}
    Pos1.writeDataToXMLNode(parentNode.AddChild('Pos1'));
    Pos2.writeDataToXMLNode(parentNode.AddChild('Pos2'));

    probes.writeDataToXMLNode(parentNode);
end;

procedure TReceiverArea.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
    Sel: IDOMNodeSelect;
begin
    clearLevelsCache;

    inherited;

    //updateGridBathymetry; //the prob has new bathy, but updateGridBathymetry was already called too soon, before theRA had it's new imax and jmax

    if not assigned(parentNode) then exit;
    Sel := parentNode as IDOMNodeSelect;

    {read TPos info}
    childNode := Sel.selectNode('Pos1');
    if assigned(childNode) then
        pos1.readDataFromXMLNode(childNode);

    childNode := Sel.selectNode('Pos2');
    if assigned(childNode) then
        pos2.readDataFromXMLNode(childNode);

    {read in the number of probes and probe data for the TReceiverArea}
    probes.readDataFromXMLNode(parentNode, iUndoMovesMax);
    if probes.Count = 0 then
        addProbe;
end;

procedure TReceiverArea.clearLevelsCache;
begin
    setlength(fLevelsCache,0);
end;

procedure TReceiverArea.cloneFrom(const source: TReceiverArea);
begin
    clearLevelsCache;

    TObjectCopier.CopyObject(source,self);

    {copy TPos }
    Pos1.cloneFrom(source.Pos1);
    Pos2.cloneFrom(source.Pos2);

    {copy the probes}
    probes.cloneFrom(source.probes, iUndoMovesMax);
    if probes.Count < 1 then
        addProbe;
end;

procedure TReceiverArea.setPos(const aX1, aY1, aX2, aY2, Z: double);
var
    tmpX1, tmpX2, tmpY1, tmpY2: double;
begin
    {make sure that x1y1 is low left, x2y2 is high right}
    tmpX1 := aX1;
    tmpX2 := aX2;
    tmpY1 := aY1;
    tmpY2 := aY2;
    if tmpX1 > tmpX2 then
    begin
        tmpX1 := aX2;
        tmpX2 := aX1;
    end;
    if tmpY1 > tmpY2 then
    begin
        tmpY1 := aY2;
        tmpY2 := aY1;
    end;

    {now we can set the two TPos3}
    {$B+} //evaluate both sides of boolean expression
    if fPos1.setPos(tmpX1, tmpY1, Z) or self.fPos2.setPos(tmpX2, tmpY2, Z) then
        checkProbesInRA();
    {$B-}
end;

function TReceiverArea.checkProbesInRA: boolean;
var
    tmpX, tmpY: double;
    prb: TProbe;
begin
    result := false;
    {make sure all the probes fit inside the RA}
    for prb in probes do
        if assigned(prb) then
        begin
            tmpX := prb.Pos.X;
            tmpY := prb.Pos.Y;
            forceRange(tmpX, self.Pos1.X, self.Pos2.X);
            forceRange(tmpY, self.Pos1.Y, self.Pos2.Y);

            if prb.setPos(tmpX,tmpY,prb.Pos.Z) then
                result := true;
        end;
    //updateProbes; // NB by now the Levels are probably wiped
end;

procedure TReceiverArea.scaleProbePositions(const scaleby: double);
var
    prb: TProbe;
begin
    for prb in probes do
        prb.setPos(prb.Pos.X * scaleBy,
                   prb.Pos.Y * scaleBy,
                   prb.Pos.Z);
end;

function TReceiverArea.getSPLfAtInds(const d, i, j, p, fi: integer; const bw: TBandwidth; const onlyForSource: TSource = nil): single;
var
    thisLev: single;
    src: TSource;
    complexLev: TComplex;
    sources: TArray<TSource>;
begin
    result := nan;

    if assigned(onlyForSource) then
        sources := [onlyForSource]
    else
        sources := delegate.getSources.ToArray;

    for src in sources do
        if src.SrcSolved then
        begin
            if delegate.getCoherent then
            begin
                complexLev := src.getFieldAndSourceLevelAtPos(xs(i),ys(j),d,p,fi,bw);
                thisLev := db10(complexLev.modulusSquared);
            end
            else
                thisLev := src.getLevelAtPos(xs(i),ys(j),d,p,fi,bw); {getLevelAtPos returns -NullLev if there is land between the source and the current point}

            if isNaN(thisLev) then continue;
            if isnan(result) then
                result := thisLev
            else
                result := dBAdd(result,thisLev);
        end; //fi
end;

function TReceiverArea.getLevbyInd(const d, i, j, p: integer; const weightingSpect: TSpectrum; const onlyForSource: TSource): single;
//var
//    thisL, weightingL: double;
//    fi: integer;
//    startfi: integer;
//    bw: TBandwidth;
begin
    if (not levelsCached) or (p <> fCacheMovingPosInd) then
        updateLevelsCache(p);

    exit(self.fLevelsCache[i,j,d]);
//
//    result := nan;
//
//    startfi := delegate.getStartfi;
//    bw := delegate.getBandwidth;
//    for fi := 0 to delegate.getLenfi - 1 do
//    begin
//        thisL := getSPLfAtInds(d, i, j, fi + startfi,bw, onlyForSource);
//        weightingL := weightingSpect.getMitigationAmpbyInd(fi + startfi,bw);
//        if not(isNaN(weightingL) or isNaN(thisL)) then
//            if isNaN(result) then
//                result := thisL - weightingL
//            else
//                result := dBAdd(result, thisL - weightingL);
//    end; //fi
end;

function TReceiverArea.getIMax: integer;
begin
    result := fimax;
end;

function TReceiverArea.getJMax: integer;
begin
    result := fjmax;
end;

function TReceiverArea.getLevAtInds(const d, i, j, p: Integer; const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean; const onlyForSource: TSource = nil):single;
begin
    //p i the moving source position
    {the caller may want to either know the projected to surface max or average level, or the level at a particular depth. }

    {if we are below the seafloor, exit and return NaN. this is irrelevant if we are doing ldMax}
    if bSeafloorNaNs and (LevelsDisplay <> ldMax) and (not bathyDelegate.depthIsAboveBathymetryAtXY(delegate.getDepths(d),xs(i),ys(j))) then
        exit(nan);

    case LevelsDisplay of
        ldMax: result := getMaxLevAndDepthIndAtInd(i,j,p,bSeafloorNaNs, onlyForSource).level;
        ldSingleLayer, ldAllLayers: result := getLevbyInd(d,i,j,p, delegate.getWeighting.spectrum, onlyForSource);
    else
        result := NaN;
    end;
end;

function TReceiverArea.getLevAtIndsAndTime(const d, i, j: integer; const time: double): single;
var
    src: TSource;
    lev: single;
    energy: double;
    fi, startfi: integer;
    movingPositionIndex: integer;
    bw: TBandwidth;
begin
    energy := 0;
    startfi := delegate.getStartfi;
    bw := delegate.getBandwidth;
    for src in delegate.getSources do
    begin
        movingPositionIndex := src.movingPosForTime(time);
        for fi := 0 to delegate.getLenfi - 1 do
        begin
            lev := src.srcSpectrum.getAmpbyInd(fi + startfi,bw,nil) - src.getMovingTLWithSourceAtPos(xs(i),
                                                                                                 ys(j),
                                                                                                 movingPositionIndex,
                                                                                                 d,
                                                                                                 fi + startfi,
                                                                                                 false);
            energy := energy + power(10, 0.1 * lev);
        end;
    end;
    result := 10 * log10(energy);
end;

function  TReceiverArea.getMaxLevAndDepthIndAtInd(const i,j,p: integer; const bSeafloorNaNs: boolean; const onlyForSource: TSource): TLevelAndDepthInd;
var
    dmax: integer;
    d: integer;
    dz, thisZ: double;
    tmp: double;
    thisLev: single;
begin
    result.level := nan;
    result.di := -1;

    //dmax := 0;
    if bSeafloorNaNs then
    begin
        dz := delegate.getdz;
        if isnan(dz) or (dz <= 0) then exit;

        //expanding this out as I suspect that it was causing crashes
        thisZ := self.bathyDelegate.getBathymetryAtPos(xs(i),ys(j));
        tmp := thisZ / dz;
        if isnan(tmp) or IsInfinite(tmp) or (tmp < low(integer)) or (tmp > high(integer)) then exit;
        dmax := saferound(tmp);
    end
    else
    begin
        //log.log(sllInfo,'Not bSeafloorNaNs: get dMax from parent dmax');
        dmax := delegate.getdmax;
    end;

    for d := 0 to dmax - 1 do
    begin
        thisLev := getLevbyInd(d, i, j, p, delegate.getWeighting.spectrum, onlyForSource);
        if isnan(thisLev) then continue;
        if isnan(result.level) then
        begin
            result.level := thisLev;
            result.di := d;
        end
        else if thisLev > result.level then
        begin
            result.level := thisLev;
            result.di := d;
        end;
    end;
end;

function TReceiverArea.getPos1: TPos3;
begin
    result := fPos1;
end;

function TReceiverArea.getPos2: TPos3;
begin
    result := fPos2;
end;

procedure TReceiverArea.getSpectAtXY(const theSpect: TSpectrum; const aX,aY: double; const LevDisp: TLevelsDisplay; const depthInd: integer; const interpolate: boolean);
{return the spectrum at a given location.
also need to get the correct depth index if
our current levelsDisplay is ldMax, in which case depthInd is ignored}
var
    i, j, m, n, k, startfi, fi: integer;
    spectra: TArray<ISmart<TSpectrum>>;
    weightings: array of double;
    bw: TBandwidth;
    divisor, val, d: double;
begin
    theSpect.clear;

    if interpolate then
    begin
        i := floor((aX - pos1.x) / dx);
        j := floor((aY - pos1.y) / dy);
        setlength(spectra, 4);
        setlength(weightings, 4);
        for k := 0 to 3 do
        begin
            m := i + ifthen(k mod 2 = 0, 0, 1);
            n := j + ifthen(k < 2, 0 , 1);
            spectra[k] := TSmart<TSpectrum>.create(TSpectrum.create);
            self.getSpectAtXYNoInterp(TSmart<TSpectrum>(spectra[k]).Invoke,m,n,LevDisp,depthInd);
            weightings[k] := (1 - abs(m - ((aX - pos1.x) / dx))) * (1 - abs(n - ((aY - pos1.y) / dy)));
        end;

        bw := delegate.getBandwidth;
        startfi := delegate.getstartfi;
        for fi := 0 to delegate.getlenfi - 1 do
        begin
            val := 0;
            divisor := 0;
            for k := 0 to 3 do
            begin
                d := TSmart<TSpectrum>(spectra[k]).Invoke.getAmpbyInd(fi + startfi, bw);
                if isnan(d) then continue;
                val := val + d*weightings[k];
                divisor := divisor + weightings[k];
            end;
            if divisor <> 0 then
                theSpect.setAmpbyInd(fi + startfi, val / divisor,bw);
        end;
    end
    else
    begin
        i := round((aX - pos1.x) / dx);
        j := round((aY - pos1.y) / dy);
        getSpectAtXYNoInterp(theSpect, i, j, LevDisp, depthInd);
    end;
end;

procedure TReceiverArea.getSpectAtXYNoInterp(const theSpect: TSpectrum; const i,j: integer; const LevDisp: TLevelsDisplay; const depthInd: integer);
{return the spectrum at a given location.
also need to get the correct depth index if
our current levelsDisplay is ldMax, in which case depthInd is ignored}
var
    ldi: TLevelAndDepthInd;
    fi, startfi: integer;
    bw: TBandwidth;
begin
    theSpect.clear;
    if (i < 0) or (j < 0) or (i >= iMax) or (j >= jMax) then exit();

    {if we are doing ldMax, then we need to look up the depth ind where the
    greatest level is at this x,y pos. Otherwise, get the depth ind from UWAOpts}
    if LevDisp = ldMax then
        ldi := self.getMaxLevAndDepthIndAtInd(i,j,-1,delegate.getSeafloorNaNs,nil)
    else
        ldi.di := depthInd;

    bw := delegate.getBandwidth;
    startfi := delegate.getstartfi;
    for fi := 0 to delegate.getlenfi - 1 do
        if ldi.di = -1 then
            theSpect.setAmpbyInd(fi + startfi,NaN, bw)//d=-1 only NaNs for all depths at that location
        else
            theSpect.setAmpbyInd(fi + startfi,getSPLfAtInds(ldi.di, i, j, -1, fi + startfi,bw), bw) //returns NaN if indices out of bounds
end;

function  TReceiverArea.getLevAtXY(const ax,ay: double; const LevDisp: TLevelsDisplay; const depthInd: integer; const interpolate: boolean): double;
var
    spect: ISmart<TSpectrum>;
begin
    spect := TSmart<TSpectrum>.create( TSpectrum.Create);
    getSpectAtXY(spect,ax,ay,LevDisp,depthInd,interpolate);
    result := spect.Level;
end;


procedure TReceiverArea.getSpectAtInds(const theSpect: TSpectrum; const i,j: integer; const LevDisp: TLevelsDisplay; const depthInd: integer);
{theSpect is the spectrum at given indices. this is needed because  getSpectAtPos takes up
a lot of time regenerating the x and y arrays when imax and jmax are large.

This function is only ever called with ldSingleLayer

NB this is a procedure and not a function, to reduce the chances of memory leaks}
var
    d, fi, startfi, len: integer;
    bw: TBandwidth;
begin
    theSpect.clear;
    len := delegate.getlenfi;
    bw := delegate.getBandwidth;
    startfi := delegate.getstartfi;

    if levDisp <> ldSingleLayer then
        raise Exception.Create('Error: expected ldSingleLayer was not received');
    d := depthInd;

    for fi := 0 to len - 1 do
    begin
        if d = -1 then
            theSpect.setAmpbyInd(fi + startfi,NaN, bw)//d=-1 only NaNs for all depths at that location
        else
            theSpect.setAmpbyInd(fi + startfi, getSPLfAtInds(d, i, j, -1, fi + startfi,bw), bw)//d=-1 only NaNs for all depths at that location
    end;
end;

procedure TReceiverArea.setIMax(const aVal: integer);
begin
    if (aval < gridMin) or (aval = fiMax) then exit;

    clearLevelsCache;
    fImax := aval;
end;

procedure TReceiverArea.setJMax(const aVal: integer);
begin
    if (aval < gridMin) or (aval = fjMax) then exit;

    clearLevelsCache;
    fJmax := aval;
end;

function TReceiverArea.xs(const ind: integer): double;
begin
    result := Pos1.X + (dx * ind);
end;

function TReceiverArea.ys(const ind: integer): double;
begin
    result := Pos1.Y + (dy * ind);
end;

function  TReceiverArea.xMax: double;
begin
    result := Pos2.X;
end;

function  TReceiverArea.yMax: double;
begin
    result := Pos2.y;
end;

function  TReceiverArea.dx: double;
begin
    result := (Pos2.X - Pos1.X) / (imax - 1);
end;

function  TReceiverArea.dy: double;
begin
    result := (Pos2.Y - Pos1.Y) / (jmax - 1);
end;

function TReceiverArea.getXsArray: TDoubleDynArray;
var
    i: integer;
begin
    setlength(result,iMax);
    for i := 0 to iMax - 1 do
        result[i] := xs(i);
end;

function TReceiverArea.getYsArray: TDoubleDynArray;
var
    j: integer;
begin
    setlength(result,jMax);
    for j := 0 to jMax - 1 do
        result[j] := ys(j);
end;

function TReceiverArea.addProbe: TProbe;
begin
    {probe z is ignored so we can set it to whatever}
    probes.Add(TProbe.Create((Pos2.X - Pos1.X)/2, (Pos2.Y - Pos1.Y)/2, 0.0, probes.Count, iUndoMovesMax));
    result := probes.Last;
end;

procedure TReceiverArea.deleteProbe(const ind: integer);
begin
    if checkRange(ind,0,self.probes.Count - 1) then
    begin
        self.probes.Delete(ind);
        //self.updateProbeIDs;
    end;
end;

function TReceiverArea.levelsCached: boolean;
begin
    Result := length(fLevelsCache) > 0;
end;


function TReceiverArea.probeTimeSeries(const pos: TPos3): TProbeTimeSeries;
var
    srcs: TArray<TSource>;
    src: TSource;
    p,fi, startfi: integer;
    spect: ISmart<TSpectrum>;
    //energy: double;
    bw: TBandwidth;
begin
    bw := delegate.getBandwidth;
    startfi := delegate.getStartfi;
    spect := TSmart<TSpectrum>.create(TSpectrum.create);
    srcs := delegate.getSources.ToArray;
    for src in srcs do
    begin
        if src.isMovingSource then
        begin
            setlength(Result, src.getMovingPosMax);
            for p := 0 to length(result) - 1 do
            begin
                result[p].time := IncMilliSecond(delegate.getStartTime, round(src.getMovingPosTime(p) * 1000));

                for fi := 0 to delegate.getLenfi - 1 do
                    spect.setAmpbyInd(fi,
                        src.srcSpectrum.getAmpbyInd(fi + startfi,bw) - src.getMovingTLWithSourceAtPos(pos.x, pos.y, p, 0, fi + startfi, false), bw);

                result[p].level := spect.Level;
            end;
            exit; //todo what to do with multiple moving sources?
        end;
    end;
end;

procedure TReceiverArea.updateLevelsCache(const p: integer; const setProgressBars: TProcedureWithDouble = nil; const pCancelSolve: TWrappedBool = nil);
var
    fi: integer;
    startfi: integer;
    bw: TBandwidth;
    weightingSpect: TDoubleDynArray;
    fractionDone: ISmart<TDoubleList>;
    finished: TBooleanDynArray;
begin
    clearLevelsCache;
    SetLength(fLevelsCache, imax, jmax, delegate.getdmax);
    self.fCacheMovingPosInd := p;

    startfi := delegate.getStartfi;
    bw := delegate.getBandwidth;

    SetLength(weightingSpect, delegate.getLenfi);
    for fi := 0 to delegate.getLenfi - 1 do
        weightingSpect[fi] :=  delegate.getWeighting.spectrum.getMitigationAmpbyInd(fi + startfi,bw);

    setlength(finished, delegate.getdmax);
    fractionDone := TSmart<TDoubleList>.create(TDoubleList.Create());
    fractionDone.init(delegate.getdmax);

    parallel.for(0, delegate.getdmax - 1)
        .NoWait //background
        .Execute(procedure (d: integer)
    var
        i,j,fi: integer;
        thisL, weightingL, outLev: double;
    begin
        for i := 0 to imax - 1 do
            for j := 0 to jmax - 1 do
            begin
                outLev := nan;
                for fi := 0 to delegate.getLenfi - 1 do
                begin
                    thisL := getSPLfAtInds(d, i, j, p, fi + startfi,bw);
                    weightingL := weightingSpect[fi];
                    if not(isNaN(weightingL) or isNaN(thisL)) then
                        if isNaN(outLev) then
                            outLev := thisL - weightingL
                        else
                            outLev := dBAdd(outLev, thisL - weightingL);
                end; //fi

                fLevelsCache[i,j,d] := outLev;

                fractionDone[d].d := (i + (j/jmax)) / imax;
            end;

        finished[d] := true;
    end);

    while not (finished.allTrue or ((pCancelSolve <> nil) and pCancelSolve.b)) do
    begin
        sleep(200);
        if not assigned(setProgressBars) then continue;
        setProgressBars(fractionDone.totalFractionDone);
        //Application.ProcessMessages;
    end;
end;


end.
