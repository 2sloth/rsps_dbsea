unit dBSeaObject;
{$M+}
{$B-}

interface

uses system.classes, system.strutils, system.sysutils, system.rtti, system.types,
    system.Math, system.TypInfo,
    generics.collections,
    vcl.graphics,
    xml.xmldom, xml.XMLIntf,
    helperFunctions, baseTypes, LevelConverter,
    dbSeaConstants, smartPointer;

type

{$RTTI INHERIT} //NB include this directive in every unit that subclasses TDBSeaObject, so they will have rtti information

    TFieldInfo = class(TObject)
    public
        fieldName,fieldType,fieldValue: string;
    end;
    TFieldInfos = TObjectList<TFieldInfo>;

    TNoSaveAttribute = class(TCustomAttribute);
    TNoCloneAttribute = class(TCustomAttribute);
    TNodeNameAttribute = class(TCustomAttribute)
    public
        name: string;
        constructor create(const aName: string = '');
    end;
    TSaveListAttribute = class(TCustomAttribute);

    TDBSeaObject = Class(TInterfacedPersistent)
    private
        //class function  underlyingTypeFromList(const lfield: TrttiField): TRttiType;
        function  readInodetoValue(const inode: IDOMNode; const fieldtype: string): TValue;
    protected
    const
        nullString: string = '__NULL__';
    var
        [TNoSave] fSaveBlacklist: string;

        class function noSave(const f: TRttiField): boolean;
        class function noClone(const f: TRttiField): boolean;
        class function saveList(const f: TRttiField): boolean;
        class function nodeName(const f: TRttiField): string;
    published
        property  SaveBlacklist: string read fSaveBlacklist write fSaveBlacklist;
        function  variableSaveBlacklist(const variableName: string): boolean;
    public
        class function  readIntegerField(const Sel: IDOMNodeSelect; const nodename: string): integer;
        class function  readStringField(const Sel: IDOMNodeSelect; const nodeName: string): string;
        class procedure writeIntegerField(const parentNode: IXMLNode; const nodeName: String; const val : integer);
        class procedure writeStringField(const parentNode: IXMLNode; const nodeName, val: string);
        class function  writeListField(const parentNode: IXMLNode; const nodeName: string): IXMLNode;

        procedure writeDataToXMLNode(const parentNode: IXMLNode); virtual;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); virtual;
    End;

implementation

class function TDBSeaObject.saveList(const f: TRttiField): boolean;
var
    a: TCustomAttribute;
begin
    for a in f.GetAttributes do
        if (a is TSaveListAttribute) then
            exit(true);

    exit(false);
end;

class function TDBSeaObject.noSave(const f: TRttiField): boolean;
var
    a: TCustomAttribute;
begin
    for a in f.GetAttributes do
        if (a is TNoSaveAttribute) or (a is TSaveListAttribute) then
            exit(true);

    exit(false);
end;

class function TDBSeaObject.noClone(const f: TRttiField): boolean;
var
    a: TCustomAttribute;
begin
    for a in f.GetAttributes do
        if (a is TNoCloneAttribute) or (a is TSaveListAttribute) then
            exit(true);

    exit(false);
end;

class function TDBSeaObject.nodeName(const f: TRttiField): string;
var
    a: TCustomAttribute;
begin
    for a in f.GetAttributes do
    begin
        if (a is TNodeNameAttribute) then
        begin
            if TNodeNameAttribute(a).name.IsEmpty then
                exit(f.Name)
            else
                exit(TNodeNameAttribute(a).name);
        end
        else if (a is TSaveListAttribute) then
            exit(f.Name);
    end;

    exit('');
end;

class procedure TDBSeaObject.writeIntegerField(const parentNode: IXMLNode; const nodeName: String; const val : integer);
var
    ChildNode : IXMLNode;
begin
    ChildNode := parentNode.AddChild(nodeName);
    ChildNode.Attributes['type'] := 'Integer';
    ChildNode.Text := intTostr(val);
end;

class function TDBSeaObject.writeListField(const parentNode: IXMLNode; const nodeName: string): IXMLNode;
begin
    result := parentNode.AddChild(nodeName);
    result.Attributes['type'] := 'List';
end;

class procedure TDBSeaObject.writeStringField(const parentNode: IXMLNode; const nodeName, val: string);
var
    ChildNode : IXMLNode;
begin
    ChildNode := parentNode.AddChild(nodeName);
    ChildNode.Attributes['type'] := 'String';
    if val = '' then
        ChildNode.Text := nullString
    else
        ChildNode.Text := val;
end;

class function TDBSeaObject.readIntegerField(const Sel: IDOMNodeSelect; const nodename: string): integer;
begin
    result := StrToInt(Sel.selectNode(nodename).firstChild.nodeValue);
end;

class function TDBSeaObject.readStringField(const Sel: IDOMNodeSelect; const nodeName: string): string;
begin
    result := sel.selectNode(nodeName).firstChild.nodeValue;
end;

function    TDBSeaObject.variableSaveBlacklist(const variableName : string) : boolean;
{blacklist any variables that we don't want to save. remember to put correct array length for consts.
case insensitive}
var
    SA: TStringArray;
    bl: string;
begin
    {return true if string variable name is to be blacklisted and not saved
    NB ansicomparetext = case insensitive, ansicomparestr is case sensitive}
    result := false;
    //check against the actual blacklist variable itself
    if ansicomparetext(variableName,'fSaveBlacklist') = 0 then
        result := true;

    SA.deserialise(self.SaveBlacklist);
    for bl in SA do
        if ansicomparetext(variableName,trim(bl)) = 0 then
            result := true;
end;

procedure TDBSeaObject.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    ctx: TRttiContext;
    rField: TRttiField;
    fieldInfos: ISmart<TFieldInfos>;
    fieldInfo: TFieldInfo;
begin
    if not assigned(parentNode) then exit;

    //set the parent node type from the class name
    parentNode.Attributes['type'] := self.ClassName;

    fieldInfos := TSmart<TFieldInfos>.create(TFieldInfos.create(true));
    if assigned(ctx.GetType(self.ClassType)) then
    begin
        for rField in ctx.GetType(self.ClassType).GetDeclaredFields do
        begin
            if (not self.variableSaveBlacklist(rField.Name)) and (not noSave(rField)) then
            begin
                fieldInfo := TFieldInfo.create();
                fieldInfos.add(fieldInfo);
                fieldInfo.fieldName := rField.Name;
                if rField.FieldType = nil then
                    fieldInfo.fieldType := 'null' //rtti has a problem with array[..] of TCrossSecEndPoint
                else
                    fieldInfo.fieldType := rField.FieldType.ToString;

                {getting field values. if the field is something singular, like integer, float, string, enum etc, then GetValue.Tostring
                will work. however for arrays and matrices, we need to specifically provide a way to serialise it into a string.
                (the same is true of deserialising).}
                case THelper.caseOfText(fieldInfo.fieldType,['TDoubleDynArray','TMatrix','TStringMatrix','TIntegerDynArray','TBooleanDynArray',
                                             'TSingleDynArray','TSingleMatrix','TIntegerMatrix']) of
                    0 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TDoubleDynArray>.serialise(true);
                    1 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TMatrix>.serialise(true);
                    2 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TStringMatrix>.serialise;
                    3 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TIntegerDynArray>.serialise;
                    4 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TBooleanDynArray>.serialise;
                    5 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TSingleDynArray>.serialise(true);//NB the values get round1'ed
                    6 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TSingleMatrix>.serialise(true);//NB the values get round1'ed
                    7 : fieldInfo.fieldvalue := rField.GetValue(self).AsType<TIntegerMatrix>.serialise;
                else
                    begin
                        if rField.FieldType = nil then
                            fieldInfo.fieldvalue := ''
                        else
                        begin
                            fieldInfo.fieldvalue := rField.GetValue(self).ToString;
                            if MatchStr(lowercase(fieldInfo.fieldType), ['extended','double','single']) then
                                fieldInfo.fieldvalue := forceDotSeparator(fieldInfo.fieldvalue);
                        end;
                    end;
                end;
                {empty strings mean there is no field data written to the node, which causes problems later}
                if fieldInfo.fieldvalue = '' then
                    fieldInfo.fieldvalue := nullString;
            end;
        end;
    end;

    for fieldInfo in fieldInfos do
    begin
        if not self.variableSaveBlacklist(fieldInfo.fieldName) then
        begin
            ChildNode := parentNode.AddChild(fieldInfo.fieldName);
            ChildNode.Attributes['type'] := fieldInfo.fieldType;
            ChildNode.Text := fieldInfo.fieldValue;
        end;
    end;
end;

procedure TDBSeaObject.readDataFromXMLNode(const parentNode: IDOMNode);
var
    value: TValue;
    inode: IDOMNode;
    Sel: IDOMNodeSelect;
    lType: TRttiType;
    lContext: TRttiContext;
    lField: TRttiField;
    fieldName: string;
    fieldType: string;
begin
    {NB Sel.selectnode is case sensitive.
    To get inode text, actually call inode.firstchild.nodevalue}

    if not assigned(parentNode) then exit;

    Sel := parentNode as IDOMNodeSelect;

    {write the values back to this object}
    lType := lContext.GetType(self.ClassType);

    if assigned(lType) then
    begin
        for lField in lType.GetFields do
        begin
            fieldname := lField.Name; // get field name
            if not self.variableSaveBlacklist(fieldname) then
            begin
                FieldType := lField.FieldType.ToString;
                iNode := Sel.selectNode(fieldname);
                //if the iNode doesn't exist, then ignore and move on
                if assigned(iNode) then
                begin
                    value := readInodetoValue(iNode, FieldType);
                    if not value.IsEmpty then
                        lField.setvalue(self, value);
                    //null strings are stored as nullString, so set them back to ''
                    if (ansicompareText(FieldType,'String') = 0) or (ansicompareText(FieldType,'TFileName') = 0) then
                        if ansicompareStr(lField.GetValue(self).ToString,nullString) = 0 then
                            lField.SetValue(self, TValue.From(''));
                end;
            end;
        end; // for lfield
    end;
end;

//class function TDBSeaObject.underlyingTypeFromList(const lfield: TrttiField): TRttiType;
//var
//    s: string;
//    openBracket, closeBracket: integer;
//    lContext: TRttiContext;
//begin
//    s := lfield.FieldType.ToString;
//    OpenBracket := Pos('<', s);
//    CloseBracket := PosEx('>', s, OpenBracket+1);
//    s := Copy(s, OpenBracket+1, CloseBracket-OpenBracket-1);
//    result := lContext.FindType(s);
//end;

function TDBSeaObject.readInodetoValue(const inode : IDOMNode; const fieldtype : string) : TValue;
{used during save and open operations. read an xml inode and return the value}
var
    //Value       : TValue;
    SM          : TStringMatrix;
    BA          : TBooleanDynArray;
    IA          : TIntegerDynArray;
    IM          : TIntegerMatrix;
    //ERId        : TEquipReturnID;
    d           : double;
    sing        : single;
    c           : Cardinal;
    w           : Word;
    lw          : Longword;
    col         : TColor;
    s           : String;
    i           : integer;
    b           : boolean;
    //vis         : TVisibility;
    calcmethod  : TCalculationMethod;
    bw          : TBandwidth;
    levType     : TLevelType;
    srcLevType  : TSourceLevelType;
begin
    if not Assigned(iNode) then Exit; //TODO if fieldtype = node attribute

    //NB on READING we have to work with TVect, TMatrix, TSingleArray, TSingleMatrix explicitly, not via this func.
    //Saving is fine
    //as TValue doesn't work well with dynamic arrays as these are all done explicitly
    //
    //however, the clone functions are fine so don't need to do it explicitly

    case THelper.caseOfText(fieldtype,['string','Integer','Double','Single','Word','LongWord','Cardinal',
                                'Boolean','TColor','TStringMatrix','TIntegerArray','TIntegerDynArray',
                                'TIntegerMatrix',
                                'TBoolArray','TBooleanDynArray','TCalculationMethod',
                                'TBandwidth','TFileName','TEquipReturnID',
                                'TVisibility','TLevelType','TSourceLevelType']) of
        0 : begin   //string
            try
                s := inode.firstChild.nodeValue;
            except on e: Exception do
                s := NullString;
            end;
            TValue.Make(@s,TypeInfo(string),result);
        end;
        1 : begin //int
            try
                i := toint(inode.firstChild.nodeValue);
            except on e: exception do
                i := 0;
            end;
            TValue.Make(@i,TypeInfo(integer),result);
        end;
        2 : begin  //double
            try
                if (ansiCompareText(inode.firstChild.nodeValue,nanString) <> 0) and
                   (ansiCompareText(inode.firstChild.nodeValue,'-' + nanString) <> 0) and
                   isFloat(inode.firstChild.nodeValue) then
                        d := tofl(inode.firstChild.nodeValue)
                else
                    d := nan;
            except on e: exception do
                d := nan;
            end;
            TValue.Make(@d,TypeInfo(Double),result);
        end;
        3 : begin  //single
            try
                if (ansiCompareText(inode.firstChild.nodeValue,nanString) <> 0) and
                   (ansiCompareText(inode.firstChild.nodeValue,'-' + nanString) <> 0) and
                   isFloat(inode.firstChild.nodeValue) then
                    sing := tofl(inode.firstChild.nodeValue)
                else
                    sing := nan;
            except on e: exception do
                sing := nan;
            end;
            TValue.Make(@sing,TypeInfo(Single),result);
        end;
        4 : begin  //Word
            try
                w := toint(inode.firstChild.nodeValue);
            except on e: exception do
                w := 0;
            end;
            TValue.Make(@w,TypeInfo(Word),result);
        end;
        5 : begin  //LongWord
            try
                lw := toint(inode.firstChild.nodeValue);
            except on e: exception do
                lw := 0;
            end;
            TValue.Make(@lw,TypeInfo(LongWord),result);
        end;
        6 : begin  //cardinal
            try
                c := toint(inode.firstChild.nodeValue);
            except on e: exception do
                c := 0;
            end;
            TValue.Make(@c,TypeInfo(Cardinal),result);
        end;
        7 : begin   //bool
            try
                b := tobool(inode.firstChild.nodeValue);
            except on e: exception do
                b := false;
            end;
            TValue.Make(@b,TypeInfo(boolean),result);
        end;
        8 : begin   //color
            try
                col := StringToColor(inode.firstChild.nodeValue);
            except on e: exception do
                col := 0;
            end;
            TValue.Make(@col,TypeInfo(TColor),result);
        end;
        9 : begin   //TStringMatrix
            try
                SM.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                SM := [];
            end;
            TValue.Make(@SM,TypeInfo(TStringMatrix),result);
        end;
        10 : begin   //TIntegerArray  //legacy, use TIntegerDynArray going forward
            try
                IA.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                IA := [];
            end;
            TValue.Make(@IA,TypeInfo(TIntegerDynArray),result);
        end;
        11 : begin   //TIntegerDynArray
            try
                IA.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                IA := [];
            end;
            TValue.Make(@IA,TypeInfo(TIntegerDynArray),result);
        end;
        12 : begin   //TIntegerMatrix
            try
                IM.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                IM := [];
            end;
            TValue.Make(@IM,TypeInfo(TIntegerMatrix),result);
        end;
        13 : begin  //TBoolArray     //legacy, use TBooleanDynArray going forward
            try
                BA.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                BA := [];
            end;
            TValue.Make(@BA,TypeInfo(TBooleanDynArray),result);
        end;
        14 : begin  //TBooleanDynArray
            try
                BA.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                BA := [];
            end;
            TValue.Make(@BA,TypeInfo(TBooleanDynArray),result);
        end;
        15 : begin  //TCalculationMethod
            try
                s := inode.firstChild.nodeValue;
                calcmethod := TCalculationMethod(GetEnumValue(TypeInfo(TCalculationMethod),s));
            except on e: exception do
                calcmethod := cm10log;
            end;
            TValue.Make(@calcmethod,TypeInfo(TCalculationMethod),result);
        end;
        16 : begin  //TBandwidth
            try
                s := inode.firstChild.nodeValue;
                bw := TBandwidth(GetEnumValue(TypeInfo(TBandwidth),s));
            except on e: exception do
                bw := bwOctave;
            end;
            TValue.Make(@bw,TypeInfo(TBandwidth),result);
        end;
        17 : begin //TFileName
            try
                s := inode.firstChild.nodeValue;
            except on e: exception do
                s := nullString;
            end;
            TValue.Make(@s,TypeInfo(TFileName),result);
        end;
        18 : begin   //TEquipReturnID
            //convert to integerarray to support legacy save files from beta test period
            try
                IA.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                IA := [];
            end;
            TValue.Make(@IA,TypeInfo(TIntegerDynArray),result);
        end;
        19 : begin  //TVisibility
            //convert to TBoolArray to support legacy save files from beta test period
            try
                BA.deserialise(inode.firstChild.nodeValue);
            except on e: exception do
                BA := [];
            end;
            TValue.Make(@BA,TypeInfo(TBooleanDynArray),result);
        end;
        20 : begin  //TLevelType
            try
                s := inode.firstChild.nodeValue;
                levType := TLevelType(GetEnumValue(TypeInfo(TLevelType),s));
            except on e: exception do
                levType := TLevelType.kSPL;
            end;
            TValue.Make(@levType,TypeInfo(TLevelType),result);
        end;
        21 : begin  //TSourceLevelType
            try
                s := inode.firstChild.nodeValue;
                {support legacy files where TLevelType and TSourceLevelType were combined. I suspect there are not actually any such files out there in the wild}
                if ansicompareText(s,'kSPL') = 0 then
                    srcLevType := kSL
                else if ansicompareText(s,'kSELfreq') = 0 then
                    srcLevType := kSELfreqSrc
                else
                    srcLevType := TSourceLevelType(GetEnumValue(TypeInfo(TSourceLevelType),s));
            except on e: exception do
                srcLevType := TSourceLevelType.kSL;
            end;

            TValue.Make(@srcLevType,TypeInfo(TSourceLevelType),result);
        end;
    else
        result := TValue.Empty;
    end; //case
end;



{ TNodeNameAttribute }

constructor TNodeNameAttribute.create(const aName: string);
begin
    inherited create;

    self.name := aName;
end;

end.

