unit fishCurve;

{$B-}
interface

uses system.math, system.sysutils, system.strutils, system.rtti, system.classes,
    system.generics.collections,
    vcl.dialogs, vcl.stdctrls,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, baseTypes, SmartPointer,
     helperFunctions, objectCopier, spectrum, SQLiteTable3, dbSeaConstants;

type
{$RTTI INHERIT}

    TFishCurve = class(TDBSeaObject)
        { class for representing the db threshold or weighting values for different species of fish }
    private
        fName       : string;
        fComments   : string;
        fID         : integer;
        fDataSource : TDataSource;  //either MDA or User
        fBandWidth  : TBandWidth;

        fSpectrum   : TSpectrum;
    public
        constructor Create(const aName: string; const aBW: TBandWidth; const aID: integer; const aDataSource: TDataSource);
        destructor Destroy; override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TFishCurve);

        property Name: string read fName write fName;
        property Comments: string read fComments write fComments;
        property ID: integer read fID write fID;
        property DataSource: TDataSource read fDataSource write fDataSource;

        property spectrum: TSpectrum read fSpectrum write fSpectrum;
        property bandwidth: TBandWidth read fBandWidth;

        class procedure addEntries(const sltb: TSQLIteTable; const curves: TObjectList<TFishCurve>);
    end;

implementation

class procedure TFishCurve.addEntries(const sltb: TSQLIteTable; const curves: TObjectList<TFishCurve>);
var
    fi, len  : integer;
    bwStr2      : string;
    bw          : TBandwidth;
    levelsLabel : string;
    baseFreq    : double;
    sDataSource : TDataSource;
    fc          : TFishCurve;
    sl: ISmart<TStringlist>;
begin
    sl := TSmart<TStringList>.create(TStringList.create);

    sltb.MoveFirst;
    while not sltb.EOF do
    begin
        bwStr2 :=  sltb.FieldAsString(sltb.FieldIndex['Bandwidth']);

        {select correct bandwidth}
        if ansicomparetext(bwStr2,TSpectrum.bandwidthString(bwOctave)) = 0 then
        begin
            bw := bwOctave;
            baseFreq := BaseFreqOctave;
            levelsLabel := 'OctaveLevelsSerial';
        end
        else
        begin
            bw := bwThirdOctave;
            baseFreq := BaseFreqThirdOctave;
            levelsLabel := 'ThirdOctaveLevelsSerial';
        end;

        {select correct data source - mda or user}
        if sltb.FieldAsString(sltb.FieldIndex['DataSource']) = sUserDB then
            sDataSource := TDataSource.dsUser
        else
            sDataSource := TDataSource.dsMDA;

        {create fish curve with the same bandwidth as what is stored in the database. fill all amplitudes}
        fc := TFishCurve.create(sltb.FieldAsString(sltb.FieldIndex['Name']),
            bw,
            sltb.FieldAsInteger(sltb.FieldIndex['ID']),
            sDataSource);

        len := TSpectrum.getLength(bw); //TFishCurve(weightingSelectBox.Items.Objects[i]).Spectrum.getLength;
        if (sltb.FieldAsDouble(sltb.FieldIndex['baseFreq']) <> baseFreq) then
        begin
            showmessage('DB error: Unexpected base frequency');
            Exit;
        end;
        sl.CommaText := sltb.FieldAsString(sltb.FieldIndex[levelsLabel]);
        if (sl.Count <> len) then
        begin
            showmessage('DB error: Expected ' + tostr(len) + ' comma delimited level values, received ' + tostr(sl.Count));
            Exit;
        end;
        for fi := 0 to sl.Count - 1 do
        begin
            {ignore NaNs - nb the spectrum is initialised with nans also}
            if isFloat(sl[fi]) then
                fc.Spectrum.setAmpbyInd(fi,tofl(sl[fi]),bw);
        end;
        fc.Comments := sltb.FieldAsString(sltb.FieldIndex['Comments']);

        if assigned(curves) then
            curves.Add(fc)
        else
            fc.free;

        sltb.Next;
    end;
end;

constructor TFishCurve.Create(const aName : string; const aBW : TBandwidth; const aID : integer; const aDataSource : TDataSource);
begin
    inherited Create;

    self.SaveBlacklist := 'fSpectrum';

    self.name := aName;
    self.ID := aID;
    self.Spectrum := TSpectrum.Create;
    self.fBandWidth := aBW;
    self.DataSource := aDataSource;
end;

destructor TFishCurve.Destroy;
begin
    fSpectrum.Free;

    inherited;
end;

procedure TFishCurve.writeDataToXMLNode(const parentNode : IXMLNode);
begin
    inherited;
    if not assigned(parentNode) then exit;

    spectrum.writeDataToXMLNode(parentNode.AddChild('Spectrum'));
end;

procedure TFishCurve.readDataFromXMLNode(const parentNode : IDOMNode);
var
    childNode : IDOMNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    childnode := (parentNode as IDOMNodeSelect).selectNode('Spectrum');
    if assigned(childnode) then
        spectrum.readDataFromXMLNode(childNode);
end;

procedure TFishCurve.cloneFrom(const source : TFishCurve);
begin
    TObjectCopier.CopyObject(source,self);

    fSpectrum.CloneFrom(source.spectrum);
end;

end.
