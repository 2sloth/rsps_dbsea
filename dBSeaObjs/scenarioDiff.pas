unit scenarioDiff;

{$B-}

interface

uses
    system.SysUtils, system.Math, system.Classes, system.strutils, system.types,
    vcl.Dialogs, vcl.Forms,
    Winapi.Windows, Winapi.Messages,
    Generics.Collections,
    xml.xmldom, xml.XMLIntf,
    dBSeaColours, baseTypes,
    helperFunctions, dBSeaObject, objectCopier,
    receiverArea, MCKLib, dBSeaOptions,
    dBSeaconstants,  pos3, scenario, SmartPointer;

type

{$RTTI INHERIT}
    TScenarioDiff = class(TdBSeaObject)
    private
        fDiff : TMatrix;

        function  getDiffAtPos(const aPos : TPos3; const levDisp : TLevelsDisplay) : double;
        function  updateDiff2(const levDisp : TLevelsDisplay) : boolean;
    public
        s1, s2 : TScenario;

        constructor Create;
        destructor Destroy; Override;

        function  scenariosAssigned : boolean;
        function  diffReady : boolean;
        function  updateDiff(const levDisp : TLevelsDisplay) : boolean; //returns true if success

        function  scenariosCanBePaired: boolean;
        function  isValid: boolean;

        property  diff : TMatrix read fDiff;
    end;


implementation

constructor TScenarioDiff.Create;
begin
    inherited;

    self.SaveBlacklist := 'fDiff,s1,s2,xs,ys';
end;

destructor TScenarioDiff.Destroy;
begin
    inherited;
end;

function TScenarioDiff.scenariosAssigned;
begin
    result := assigned(s1) and assigned(s2);
end;

function TScenarioDiff.isValid: boolean;
begin
    result := scenariosAssigned and (s1 <> s2) and s1.resultsExist and s2.resultsExist and self.scenariosCanBePaired;
end;

function TScenarioDiff.scenariosCanBePaired: boolean;
begin
    if not self.scenariosAssigned then exit(false);
    if s1 = s2 then exit(false);

    result := (s1.theRA.iMax = s2.theRA.iMax) and (s1.theRA.jMax = s2.theRA.jMax)
                and (s1.dMax = s2.dMax) and (s1.startfi = s2.startfi) and (s1.endfi = s2.endfi);
end;

function  TScenarioDiff.diffReady : boolean;
begin
    if length(diff) = 0 then exit(false);
    result := length(diff[0]) > 0;
end;

function TScenarioDiff.getDiffAtPos(const aPos: TPos3; const levDisp: TLevelsDisplay): double;
var
    lev1, lev2: double;
    d1, d2: integer;
begin
    if not self.isValid then exit(nan);
    if aPos.Z < 0 then exit(nan);

    //d1 and d2 may be ignored if levdisp = ldMax
    d1 := min(s1.dMax, saferound(aPos.Z / s1.dz) );
    d2 := min(s2.dMax, saferound(aPos.Z / s2.dz) );

    lev1 := s1.theRa.getLevAtXY(aPos.x,aPos.y,levDisp,d1,false);
    lev2 := s2.theRa.getLevAtXY(aPos.x,aPos.y,levDisp,d2,false);

    result := lev2 - lev1;
end;

function  TScenarioDiff.updateDiff(const levDisp : TLevelsDisplay) : boolean;
//the diff is calculated on the grid of s1
var
    xs, ys: TDoubleDynArray;
    i, j: integer;
    p: ISmart<TPos3>;
begin
    if not self.isValid then exit(false);

    if self.scenariosCanBePaired then
        result := self.updateDiff2(levDisp) //quicker
    else
    begin
        p := TSmart<TPos3>.create(TPos3.Create(0,0,0));
        xs := s1.theRA.getXsArray;
        ys := s1.theRA.getYsArray;

        setlength(fDiff,length(xs),length(ys));
        for i := 0 to length(xs) - 1 do
            for j := 0 to length(ys) - 1 do
            begin
                p.setPos(xs[i],ys[j],0);
                fDiff[i,j] := self.getDiffAtPos(p,levDisp);
            end;
        result := true;
    end;
end;

function  TScenarioDiff.updateDiff2(const levDisp: TLevelsDisplay): boolean;
var
    i, j, d: integer;
    l1, l2: single;
begin
    if not self.isValid then exit(false);

    d := uwaopts.displayZInd;

    setlength(fDiff,s1.theRA.iMax,s1.theRA.jMax);
    for i := 0 to s1.theRA.iMax - 1 do
        for j := 0 to s1.theRA.jMax - 1 do
        begin
            l1 := s1.theRA.getLevAtInds(d,i,j,-1,levdisp,true);
            l2 := s2.theRA.getLevAtInds(d,i,j,-1,levdisp,true);
            fDiff[i,j] := l1 - l2;
        end;
    result := true;
end;

end.
