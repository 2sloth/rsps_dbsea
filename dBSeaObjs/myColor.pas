unit myColor;

{$B-}

interface

uses system.math,
    vcl.graphics,
    winapi.windows,
    generics.Collections,
    dBSeaObject;

type

{$RTTI INHERIT}

    TColorHelper = record helper for TColor
    public
        function r: byte;
        function g: byte;
        function b: byte;
        class function random: TColor; static;
    end;

    TMyColorRec = record
        color: TColor;
        visible: boolean;

        procedure makeRandom();
    end;

    TMyColor = Class(TdBSeaObject)
        { I'm creating this class so that i can make a tlist of it, containing some colors.
          when i make a tlist just containing colors, the tlist thinks
          it should be pointers and then that doesn't work. }
    private
        fColor      : TColor;
        fVisible    : boolean;
    public
        constructor Create;
        constructor CreateWithColor(const aCol: TColor; const isVisible: boolean);

        function asTMyColorRec(): TMyColorRec;

        property color: TColor read fColor write fColor;
        property visible: boolean read fVisible write fVisible;
    End;
    TMyColorList = TObjectList<TMyColor>;

implementation

procedure TMyColorRec.makeRandom();
begin
    self.color := RGB(Byte(RandomRange(0,255)),
                      Byte(RandomRange(0,255)),
                      Byte(RandomRange(0,255)));
    self.visible := true;
end;

constructor TMyColor.Create;
begin
    inherited Create;

    self.SaveBlacklist := '';
    self.Color := RGB(0, 0, 0);
    self.fVisible := true;
end;

constructor TMyColor.CreateWithColor(const aCol : TColor; const isVisible : boolean);
begin
    inherited Create;

    self.SaveBlacklist := '';
    self.Color := aCol;
    self.fVisible := isVisible;
end;

function  TMyColor.asTMyColorRec() : TMyColorRec;
begin
    result.color := self.Color;
    result.visible := self.Visible;
end;

{ TColorHelper }

function TColorHelper.b: byte;
begin
    result := GetBValue(self);
end;

function TColorHelper.g: byte;
begin
    result := GetGValue(self);
end;

function TColorHelper.r: byte;
begin
    result := GetRValue(self);
end;

class function TColorHelper.random(): TColor;
begin
    result := RGB(Byte(RandomRange(0,255)),
                      Byte(RandomRange(0,255)),
                      Byte(RandomRange(0,255)));
end;

end.
