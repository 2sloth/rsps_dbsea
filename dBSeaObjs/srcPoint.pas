unit srcPoint;

{$B-}
interface

uses generics.collections, dBSeaObject, objectCopier, basetypes;

type

{$RTTI INHERIT}

    TMovingSourceTiming = record
        dT, divs: double;
    end;

    TSrcPoint = Class(TDBSeaObject)
        {would like to do this as a record, but tobjectlist only works with classes}
    public
        X: double;
        Y: double;
        Z: double;
        dT: double;  //time between this and previous pos
        divs: integer; //number of points to evaluate between this and previous pos

        constructor Create;
        procedure cloneFrom(const source: TSrcPoint);

        function  asFPoint3: TFPoint3;
        function  asFPoint: TFPoint;
        function  dragMovingPos(const aX, aY: double): boolean;
    end;
    TSrcPointList = TObjectList<TSrcPoint>;

implementation

constructor TSrcPoint.Create;
begin
    inherited;

    self.SaveBlacklist := '';
end;

procedure TSrcPoint.cloneFrom(const source : TSrcPoint);
begin
    TObjectCopier.CopyObject(source,self);
end;

function TSrcPoint.asFPoint: TFPoint;
begin
    result.x := self.x;
    result.y := self.y;
end;

function  TSrcPoint.asFPoint3 : TFPoint3;
begin
    result.x := self.x;
    result.y := self.y;
    result.z := self.z;
end;

function TSrcPoint.dragMovingPos(const aX, aY : double) : boolean;
begin
    result := (ax <> self.X) or (ay <> self.y);
    if result then
    begin
        self.x := ax;
        self.y := ay;
    end;
end;

end.
