unit equipReturn;

{$B-}
interface

uses system.sysutils, system.rtti, system.math,
     xml.XMLIntf, xml.xmldom,
    generics.Collections,
    dBSeaObject, helperFunctions, baseTypes, spectrum,
    objectCopier, material, mcklib;

type

{$RTTI INHERIT}
    TEquipReturn = class(TDBSeaObject)
    private
        fName          : string;
        fCategory      : string;
        fCount         : integer;
        fDuty          : double;  //expressed as fraction, not percent. ie for 50% on time, use duty = 0.5
        fSpectrum      : TSpectrum;
        fEquipID       : integer;
        fDataSource    : TDataSource;
        fIsMitigation  : boolean;
        //fCrestFactordB : double;

        function  getCountDutyDB: double;
    public
        constructor Create;
        destructor Destroy; override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TEquipReturn);

        procedure clear;
        function  getAmpCountDutyByInd(const fi: integer; const aBW: TBandWidth): double;
        function  isActive: boolean;
        function  level(const startfi,endfi: integer; const bw: TBandwidth): double;

        property  name: string read fName write fName;
        property  category: string read fCategory write fCategory;
        property  count: integer read fCount write fCount;
        property  duty: double read fDuty write fDuty;
        property  spectrum: TSpectrum read fSpectrum write fSpectrum;
        property  equipID: integer read fEquipID write fEquipID;
        property  dataSource: TDataSource read fDataSource write fDataSource;
        property  isMitigation: boolean read fIsMitigation write fIsMitigation;
        //property  crestFactordB: double read fCrestFactordB write fCrestFactordB;
    end;

implementation

constructor TEquipReturn.Create;
begin
    inherited Create;

    saveBlacklist := 'fSpectrum';

    spectrum := TSpectrum.Create;
    clear;
end;

destructor TEquipReturn.Destroy;
begin
    fSpectrum.Free;

    inherited;
end;

procedure TEquipReturn.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;
    if not assigned(parentNode) then exit;

    self.spectrum.writeDataToXMLNode(parentNode.AddChild('Spectrum'));
end;

procedure TEquipReturn.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    childnode := (parentNode as IDOMNodeSelect).selectNode('Spectrum');
    if assigned(childnode) then
        self.spectrum.readDataFromXMLNode(childNode);
end;

procedure TEquipReturn.cloneFrom(const source: TEquipReturn);
begin
    TObjectCopier.CopyObject(source,self);

    fSpectrum.CloneFrom(source.spectrum);
end;

procedure TEquipReturn.clear;
begin
    fname := '';
    fcategory := '';
    fduty := nan;
    fcount := -1;
    fEquipID := -1;
    fDataSource := TDataSource.dsMDA;
    fspectrum.clear;
    fIsMitigation := false;
    //fCrestFactordB := 12.0;
end;

//function  TEquipReturn.level: double;
//begin
//    result := spectrum.Level;
//end;


function  TEquipReturn.getCountDutyDB: double;
begin
    if isnan(count) or isnan(duty) then exit(nan);

    result := dB10(self.count) + dB10(self.duty);
end;

function TEquipReturn.getAmpCountDutyByInd(const fi: integer; const aBW: TBandWidth): double;
begin
    result := spectrum.getAmpbyInd(fi,aBW) + getCountDutyDB;
end;

function  TEquipReturn.isActive: boolean;
begin
    result := (not isnan(duty)) and
              (count > 0) and
              (not isnan(spectrum.Level));
end;

function  TEquipReturn.level(const startfi,endfi: integer; const bw: TBandwidth): double;
begin
    result := spectrum.Level(startfi,endfi,bw) + getCountDutyDB;
end;

end.
