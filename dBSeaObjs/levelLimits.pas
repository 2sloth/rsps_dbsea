unit levelLimits;

{$B-}

interface

uses syncommons,
    system.sysutils, system.math, system.rtti,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, helperFunctions, baseTypes,
    dBSeaColours, dBSeaconstants,
    objectCopier, levelConverter, eventlogging;

type

{$RTTI INHERIT}
    ILevelLimitsDelegate = interface(IInterface)
      function  getLevelsDisplay: TLevelsDisplay;
      function  getDisplayZInd: integer;
    end;

    TLevelLimits = class(TdBSeaObject)
    private
        fCMax, fCMin           : single; // colormap results max and min
        fUMax, fUMin           : single; // user set max and min
        fExclusionZoneLevel    : single;
        fbUserSetLimits        : boolean;
        fLevelSpacing          : single; // spacing to use if not user set
        fShowExclusionZone     : boolean;
        fLockLimits            : boolean;

        procedure setLims(const aMin, aMax: single; const bSetUserLimitsOrFalseForCLimits: boolean);
        procedure setLevelSpacing(const spacing: single);
        procedure maxMinRounding(const inMin, inMax: single; out outMin, outMax: single);
    public
        delegateOrNil: ILevelLimitsDelegate;

        constructor Create(const aDelegate: ILevelLimitsDelegate);
        procedure cloneFrom(const source: TLevelLimits);

        function  max: single;
        function  min: single;
        function  maxOrMinNaN: boolean;

        procedure deleteUserSetLimits;
        procedure setLimsWithSpacing(const aMin, aMax, spacing: single; const bSetUserLimitsOrFalseForCLimits: boolean);
        procedure updateCMaxCMin();

        property  cMax: single read fCMax;
        property  cMin: single read fCMin;
        property  exclusionZoneLevel: single read fExclusionZoneLevel write fExclusionZoneLevel;
        property  showExclusionZone: boolean read fShowExclusionZone write fShowExclusionZone;
        property  levelSpacing: single read fLevelSpacing write setLevelSpacing;
        property  bUserSetLimits: boolean read fbUserSetLimits;
        property  lockLimits: boolean read flockLimits write flockLimits;
    end;

implementation

uses problemContainer;

constructor TLevelLimits.Create(const aDelegate: ILevelLimitsDelegate);
begin
    inherited Create;

    self.SaveBlacklist := 'delegateOrNil';

    self.delegateOrNil := aDelegate;
    fLevelSpacing := 5;  //5 dB spacing
    fCMax := NaN; // max and min level from results for resultscolors.
    fCMin := NaN;
    fUMax := NaN; // max and min level from user for resultscolors.
    fUMin := NaN;
    fExclusionZoneLevel := nan;
    fShowExclusionZone := false;
    fbUserSetLimits := false;
    fLockLimits := false;
end;

procedure  TLevelLimits.cloneFrom(const source: TLevelLimits);
begin
    TObjectCopier.CopyObject(source,self);
end;

procedure TLevelLimits.updateCMaxCMin();
var
    mx, mn, thisLev: single;
    i,j: integer;
    log: ISynLog;
    levDisp: TLevelsDisplay;
begin
    if lockLimits then exit;
    if not assigned(delegateOrNil) then exit;

    log := TSynLogLevs.Enter(self,'updateCMaxCMin');

    levDisp := delegateOrNil.getLevelsDisplay;

    {ldAllLayers, let's set limits based on ldmax. will mean we don't see some low levels but is a good speed compromise}
    if levDisp = ldAllLayers then
        levDisp := ldMax;

    mx := NaN;
    mn := NaN;
    for i := 0 to container.scenario.theRA.imax - 1 do
        for j := 0 to container.scenario.theRA.jmax - 1 do
        begin
            thisLev := container.scenario.theRA.getLevAtInds(delegateOrNil.getDisplayZInd,i,j,-1,
                                                             levDisp,
                                                             container.scenario.NaNSeafloorLevels);

            if (not isNaN(thisLev)) and (thisLev > LowLev) then
            begin
                greater(mx,thisLev);
                lesser(mn,thisLev);
            end;
        end; //i,j

    if self.fLevelSpacing = 0 then
        self.fLevelSpacing := 5;

    {these values are the raw max and min, so now set to the spacing}
    setLimsWithSpacing(mn,mx,LevelSpacing,false);
end;


function  TLevelLimits.Max: single;
begin
    result := ifthen(fbUserSetLimits,fUMax,fCMax);
end;

function  TLevelLimits.Min: single;
begin
    result := ifthen(fbUserSetLimits,fUMin,fCMin);
end;

function  TLevelLimits.MaxOrMinNaN: boolean;
begin
    result := isnan(Min) or isnan(Max);
end;

procedure TLevelLimits.DeleteUserSetLimits;
{just to clear the usersetlimits flag. you still need to recalc the limits explicitly}
begin
    fbUserSetLimits := false;
end;

procedure TLevelLimits.setLevelSpacing(const spacing: single);
var
    mx, mn: single;
begin
    if spacing = LevelSpacing then Exit;
    if spacing <= 0 then Exit;

    fLevelSpacing := spacing;
    maxMinRounding(self.Min,self.Max,mn,mx);
    setLims(mn,mx,bUserSetLimits);
end;

procedure TLevelLimits.setLims(const aMin, aMax: single; const bSetUserLimitsOrFalseForCLimits: boolean);
begin
    if bSetUserLimitsOrFalseForCLimits then
    begin
        fUMin := aMin;
        fUMax := aMax;
        fbUserSetLimits := true;
    end
    else
    begin
        fCMin := aMin;
        fCMax := aMax;
    end;
end;

procedure TLevelLimits.setLimsWithSpacing(const aMin, aMax, spacing: single; const bSetUserLimitsOrFalseForCLimits: boolean);
{results limits, with spacing of spacing dB. Inputs MUST be in kSPL}
var
    mn,mx: single;
begin
    if isnan(spacing) or (spacing <= 0) then exit;

    if isNaN(aMin) or isNaN(aMax) then
        setLims(NaN,NaN,bSetUserLimitsOrFalseForCLimits)
    else
    begin
        fLevelSpacing := spacing;
        maxMinRounding(aMin,aMax,mn,mx);
        setLims(mn,mx,bSetUserLimitsOrFalseForCLimits);    {the limits are in kSPL}
    end;
end;


procedure TLevelLimits.maxMinRounding(const inMin, inMax: single; out outMin, outMax: single);
begin
    //convert to user levels, so that we can have the user see max and min mod spacing = 0
    outMin := container.scenario.convertLevel(inMin);
    outMax := container.scenario.convertLevel(inMax);

    outMin := floorx(outMin,self.fLevelSpacing);
    outMax := ceilx(outMax,self.fLevelSpacing);

    {check to make sure we always set at least 1 division between max and min}
    if ((outMax - outMin) < self.fLevelSpacing) or (outMax < outMin) then
        outMax := outMin + self.fLevelSpacing;

    //convert back to kSPL
    outMin := container.scenario.unconvertLevel(outMin);
    outMax := container.scenario.unconvertLevel(outMax);
end;




end.
