unit ProbeTimeSeries;

interface

uses system.math, system.types;

type
    TProbeTimeSeriesPoint = record
        time: TDateTime;
        level: double;
    end;
    TProbeTimeSeries = array of TProbeTimeSeriesPoint;

implementation

end.
