unit LocationProperties;

{$B-}
interface

uses system.math, system.SysUtils, system.types, system.json,
    xml.XMLIntf, xml.xmldom,
    vcl.graphics, VCL.StdCtrls,
    winapi.windows,
    generics.collections,
    dBSeaObject, objectCopier, basetypes, MCKLib,
    helperFunctions, dBSeaconstants, waterprops,
    seafloorScheme, soundSpeedProfile,
    dBSeaColours;

type
{$RTTI INHERIT}
    TLocationProperties = Class(TdBSeaObject)
    public
        easting, northing: double;
        name, comments: string;
        seabed: TSeafloorScheme;
        ssp: TSSP;
        water: TWaterProps;
        color: TColor;

        constructor Create(const zMax: double);
        destructor Destroy; Override;

        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TLocationProperties);

        function distanceSquaredTo(const east, north: double): double;

        class function createFromJSON(v: TJSONValue; const zmax: double): TLocationProperties;
    End;
    TLocationPropertiesList = class(TMyObjectList<TLocationProperties>)
    private
    public
        procedure cloneFrom(const source: TLocationPropertiesList; const zmax: double);

        function  closest(const east, north: double): TLocationProperties;
        function  interp(const east, north, zmax: double): TLocationProperties;
        function  interpColor(const east, north: double): TColor;
        function  copyFirst(const zmax: double): TLocationProperties;
        function  allSeabedsHaveSameNumberOfLayers: boolean;
        class function importFromJSON(const s: String; const zmax: double): TLocationPropertiesList;

        procedure initComboBox(const cb: TComboBox);
    end;
    TLPRange = class(TObject)
    public
        range: double;
        lp: TLocationProperties;

        constructor create(const r: double; const lp: TLocationProperties);
        destructor Destroy; override;
    end;
    TLPRangeList = class(TObjectList<TLPRange>)
        function getCProfiles(const aAngle: double): TDepthValArrayAtRangeArray;
        function getSeabeds: TSeafloorRangeArray;
    end;

implementation

uses problemContainer;

constructor TLocationProperties.Create(const zMax: double);
begin
    inherited Create;

    self.SaveBlacklist := 'delegate,location,seabed,ssp,water,color';

    self.name := 'New properties';
    self.color := TDBSeaColours.randomSaturatedColor;
    self.seabed := TSeafloorScheme.defaultScheme;
    self.ssp := TSSP.create;
    ssp.reset(zMax);
    self.water := TWaterProps.Create('Default',8,35,0,TDataSource.dsMDA);
end;

destructor TLocationProperties.Destroy;
begin
    seabed.free;
    ssp.free;
    water.free;

    inherited;
end;

function TLocationProperties.distanceSquaredTo(const east, north: double): double;
begin
    result := sqr(east - self.easting) + sqr(north - self.northing);
end;

procedure TLocationProperties.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;
    if not assigned(parentNode) then exit;

    Water.writeDataToXMLNode(parentNode.AddChild('WaterProps'));
    ssp.writeDataToXMLNode(parentNode.AddChild('SSP'));
    seabed.writeDataToXMLNode(parentNode.AddChild('Seabed'));
end;

procedure TLocationProperties.readDataFromXMLNode(const parentNode: IDOMNode);
var
    ChildNode: IDOMNode;
    Sel: IDOMNodeSelect;
begin
    inherited;
    if not assigned(parentNode) then exit;

    Sel := parentNode as IDOMNodeSelect;

    childNode := Sel.selectNode('Seabed');
    if assigned(childNode) then
        seabed.readDataFromXMLNode(childNode);

    {reading the SSP}
    childNode := Sel.selectNode('SSP');
    if assigned(childNode) then
        ssp.readDataFromXMLNode(childNode);

    {reading the water properties}
    childNode := Sel.selectNode('WaterProps');
    if assigned(childNode) then
        water.readDataFromXMLNode(childNode);
end;


procedure TLocationProperties.cloneFrom(const source: TLocationProperties);
begin
    TObjectCopier.CopyObject(source,self);

    seabed.cloneFrom(source.seabed);
    ssp.cloneFrom(source.ssp);
    water.cloneFrom(source.water);
end;

class function TLocationProperties.createFromJSON(v: TJSONValue; const zmax: double): TLocationProperties;
var
    o: TJSONObject;
    colourStr: String;
begin
    try
        o := TJSONObject(v);

        result := TLocationProperties.create(zmax);
        result.water.free;
        result.ssp.free;
        result.seabed.free;

        if not assigned(o.getValue('easting')) or not isFloat(o.getValue('easting').ToString) then
            raise EJSONParsing.Create('Could not parse easting');
        if not assigned(o.getValue('northing')) or not isFloat(o.getValue('northing').ToString) then
            raise EJSONParsing.Create('Could not parse northing');

        result.easting := container.geoSetup.getXfromString(o.getValue('easting').ToString);
        result.northing := container.geoSetup.getYfromString(o.getValue('northing').ToString);

        if assigned(o.getValue('name')) then
            o.getValue('name').TryGetValue<string>(result.name);
        if assigned(o.getValue('comments')) then
            o.getValue('comments').TryGetValue<string>(result.comments);

        if assigned(o.getValue('colour')) then
        begin
            try
                o.getValue('colour').TryGetValue<string>(colourStr);
                result.color := TDBSeaColours.HtmlToColor(colourStr, result.color);
            except
            end;
        end;

        if not assigned(o.getValue('water')) then
            raise EJSONParsing.Create('Water field not found');
        if not assigned(o.getValue('ssp')) then
            raise EJSONParsing.Create('SSP field not found');
        if not assigned(o.getValue('seabed')) then
            raise EJSONParsing.Create('Seabed field not found');

        result.water := TWaterProps.createFromJSON(o.getValue('water'));
        result.ssp := TSSP.createFromJSON(o.getValue('ssp'));
        result.seabed := TSeafloorScheme.createFromJSON(o.getValue('seabed'));

        if not assigned(result.water) then
            raise EJSONParsing.Create('Could not parse water');
        if not assigned(result.SSP) then
            raise EJSONParsing.Create('Could not parse SSP');
        if not assigned(result.seabed) then
            raise EJSONParsing.Create('Could not parse seabed');

    except on e: Exception do
        exit(nil);
    end;
end;

procedure TLocationPropertiesList.cloneFrom(const source: TLocationPropertiesList; const zmax: double);
var
    i: integer;
begin
    self.clear;
    for i := 0 to source.Count - 1 do
    begin
        self.Add(TLocationProperties.Create(zmax));
        self.Last.cloneFrom(source[i]);
    end;
    if self.Count = 0 then
        self.add(TLocationProperties.create(zmax));

    if assigned(source.selected) then
        self.selected := Self[source.IndexOf(source.selected)]
    else
        self.selected := self.First;
end;

function TLocationPropertiesList.closest(const east, north: double): TLocationProperties;
var
    minDistSquared, thisDistSquared: double;
    i: integer;
begin
    if self.count = 0 then exit(nil);
    result := self.First;
    if self.count = 1 then exit();

    minDistSquared := result.distanceSquaredTo(east,north);
    for I := 1 to self.count - 1 do
    begin
        thisDistSquared := self[i].distanceSquaredTo(east, north);
        if thisDistSquared < minDistSquared then
        begin
            minDistSquared := thisDistSquared;
            result := self[i];
        end;
    end;
end;

function TLocationPropertiesList.copyFirst(const zmax: double): TLocationProperties;
begin
    result := TLocationProperties.create(zmax);
    result.cloneFrom(self.First);
end;

class function TLocationPropertiesList.importFromJSON(const s: String; const zmax: double): TLocationPropertiesList;
var
    arr: TJSONArray;
    i: integer;
    lp: TLocationProperties;
begin
    result := nil;

    arr := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(s),0) as TJSONArray;
    try
        if not assigned(arr) then
            raise EJSONParsing.Create('JSON parsing failed, check input data formatting');

        if arr.Count = 0 then
            raise EJSONParsing.Create('Main array length = 0');

        result := TLocationPropertiesList.create(true);

        for i := 0 to arr.count - 1 do
        begin
            lp := TLocationProperties.createFromJSON(arr.items[i],zmax);
            if assigned(lp) then
                result.add(lp);
        end;

        if result.count = 0 then
            raise EJSONParsing.Create('No entries were successfully parsed')
        else if not result.allSeabedsHaveSameNumberOfLayers then
            raise EJSONParsing.Create('All imported seabeds must have the same number of layers');

        result.selected := result.first;
    finally
        arr.free;
    end;
end;

function TLocationPropertiesList.allSeabedsHaveSameNumberOfLayers: boolean;
var
    lp: TLocationProperties;
begin
    if self.count < 2 then exit(true);
    for lp in self do
        if lp.seabed.layers.count <> self.first.seabed.layers.count then
            exit(false);
    result := true;
end;

procedure TLocationPropertiesList.initComboBox(const cb: TComboBox);
var
    p: TPair<integer, TLocationProperties>;
begin
    cb.Clear;

    for p in self.enumerate do
        cb.AddItem(inttostr(1 + p.key) + ': ' + p.Value.name, nil);

    cb.ItemIndex := self.IndexOf(self.selected);
end;

function TLocationPropertiesList.interp(const east, north, zmax: double): TLocationProperties;
var
    invDists, weightings: TDoubleDynArray;
    totalInvDist, dx, dy, invDist: double;
    i: integer;
    lp: TLocationProperties;
    waters: TArray<TWaterProps>;
    SSPs: TArray<TSSP>;
    seabeds: TArray<TSeafloorScheme>;
begin
    setlength(waters, self.count);
    setlength(SSPs, self.count);
    setlength(seabeds, self.count);
    setlength(invDists, self.count);
    totalInvDist := 0;
    for i := 0 to self.count - 1 do
    begin
        lp := self[i];
        waters[i] := lp.water;
        SSPs[i] := lp.ssp;
        seabeds[i] := lp.seabed;

        dx := east - lp.easting;
        dy := north - lp.northing;
        if (dx = 0) and (dy = 0) then
            exit(lp);

        invDist := 1/hypot(dx,dy);
        invDists[i] := invDist;
        totalInvDist := totalInvDist + invDist;
    end;

    setlength(weightings, length(invDists));
    for i := 0 to length(invDists) - 1 do
        weightings[i] := invDists[i] / totalInvDist;

    result := TLocationProperties.create(zmax);

    result.water.setWithListAndWeightings(waters, weightings);
    result.ssp.setWithListAndWeightings(SSPs, weightings);
    result.seabed.setWithListAndWeightings(seabeds, weightings);
end;

function TLocationPropertiesList.interpColor(const east, north: double): TColor;
var
    invDists, weightings: TDoubleDynArray;
    totalInvDist, dx, dy, invDist: double;
    i: integer;
    lp: TLocationProperties;
    r,g,b: double;
begin
    setlength(invDists, self.count);
    totalInvDist := 0;
    for i := 0 to self.count - 1 do
    begin
        lp := self[i];

        dx := east - lp.easting;
        dy := north - lp.northing;
        if (dx = 0) and (dy = 0) then
            exit(lp.color);

        invDist := 1/hypot(dx,dy);
        invDists[i] := invDist;
        totalInvDist := totalInvDist + invDist;
    end;

    setlength(weightings, length(invDists));
    for i := 0 to length(invDists) - 1 do
        weightings[i] := invDists[i] / totalInvDist;

    r := 0;
    g := 0;
    b := 0;

    for i := 0 to self.count - 1 do
    begin
        lp := self[i];

        r := r + getRValue(lp.color) * weightings[i];
        g := g + getGValue(lp.color) * weightings[i];
        b := b + getBValue(lp.color) * weightings[i];
    end;
    r := min(255,r);
    g := min(255,g);
    b := min(255,b);
    result := rgb(round(r),round(g),round(b));
end;

constructor TLPRange.create(const r: double; const lp: TLocationProperties);
begin
    inherited create;

    self.range := r;
    self.lp := lp;
end;


function TLPRangeList.getCProfiles(const aAngle: double): TDepthValArrayAtRangeArray;
var
    i: integer;
    lp: TLocationProperties;

begin
    setlength(result, self.count);
    for i := 0 to self.count - 1 do
    begin
        lp := TLPRange(self[i]).lp;
        result[i] := TDepthValArrayAtRange.create(TLPRange(self[i]).range,lp.ssp.getCProfileAtAngle2(aAngle, lp.water.Velocity, lp.water.Direction));
        TSSP.removeDuplicateDepths2(result[i].depthVal);
    end;
end;

function TLPRangeList.getSeabeds: TSeafloorRangeArray;
var
    i: integer;
begin
    setlength(result, self.count);
    for i := 0 to self.count - 1 do
        result[i] := TSeafloorRange.create(TLPRange(self[i]).range,TLPRange(self[i]).lp.seabed);
end;

destructor TLPRange.Destroy;
begin
    lp.free;

  inherited;
end;

end.


