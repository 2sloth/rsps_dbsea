unit seabedObject;

{$B-}
interface

uses system.math,
    xml.XMLIntf, xml.xmldom,
    generics.collections,
    dBSeaObject, objectCopier, basetypes, MCKLib,
    helperFunctions, CSVDelimiters, dBSeaconstants, esriReader,
    seafloorScheme;

type
{$RTTI INHERIT}
//    ISeabedDelegate = interface(IInterface)
//        //
//    end;

//    TXYPair = TPair<double,double>;
//    TSeafloorSchemeMap = class(TDictionary<TXYPair,TSeafloorscheme>)
//    public
//    var
//        default: TSeafloorScheme;
//        constructor Create(const aDefault: TSeafloorScheme);
//    end;

    TSeabed = Class(TdBSeaObject)
    private
        //[TNoSave] delegate: ISeabedDelegate;
    public
        seafloorSchemes: TSeafloorSchemesList;
        //[TNoSave] schemesMap: TSeafloorSchemeMap;

        constructor Create();
        destructor Destroy; Override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TSeabed);
    End;

implementation

constructor TSeabed.Create();
begin
    inherited Create;

    self.SaveBlacklist := 'delegate,fSchemeIndices,seafloorSchemes';

    //self.delegate := aDelegate;
    seafloorSchemes := TSeafloorSchemesList.create(true);
    seafloorSchemes.add(TSeafloorScheme.defaultScheme);

    //schemesMap := TSeafloorSchemeMap.create(seafloorSchemes.First);
end;

destructor TSeabed.Destroy;
begin
    //schemesMap.free;
    seafloorSchemes.Free;

    inherited;
end;

procedure TSeabed.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    inherited;
    if not assigned(parentNode) then exit;

    {writing seafloor schemes}
    writeIntegerField(parentNode,'SeafloorSchemesCount',seafloorschemes.Count);
    ChildNode := writeListField(parentNode,'SeafloorSchemesList');
    for i := 0 to seafloorschemes.Count - 1 do
        seafloorSchemes[i].writeDataToXMLNode(ChildNode.AddChild('SeafloorScheme' + tostr(i)));
end;

procedure TSeabed.readDataFromXMLNode(const parentNode: IDOMNode);
var
    ChildNode: IDOMNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    seafloorSchemes.Clear;
    repeat
        childNode := (parentNode as IDOMNodeSelect).selectNode('SeafloorSchemesList/SeafloorScheme' + tostr(seafloorSchemes.Count));
        if assigned(childNode) then
        begin
            //create a scheme, then overwrite if with the correct data.
            seafloorSchemes.Add(TSeafloorScheme.Create);
            seafloorSchemes.Last.readDataFromXMLNode(childNode);
        end;
    until not assigned(ChildNode);
    if seafloorSchemes.Count = 0 then
        seafloorSchemes.add(TSeafloorscheme.defaultScheme);
end;


procedure TSeabed.cloneFrom(const source: TSeabed);
var
    scheme: TSeafloorScheme;
begin
    TObjectCopier.CopyObject(source,self);

    {get the seafloor schemes}
    seafloorSchemes.Clear;
    for scheme in source.seafloorSchemes do
    begin
        seafloorSchemes.Add(TSeafloorScheme.Create);
        seafloorSchemes.Last.cloneFrom(scheme);
    end;
    if seafloorSchemes.Count = 0 then
        seafloorSchemes.add(TSeafloorscheme.defaultScheme);
end;

{ TSeafloorSchemeMap }

//constructor TSeafloorSchemeMap.Create(const aDefault: TSeafloorScheme);
//begin
//    inherited Create();
//
//    self.default := aDefault;
//end;

end.


