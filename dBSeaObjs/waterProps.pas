unit waterProps;

{$B-}
interface

uses system.types, system.SysUtils, system.math, system.JSON,
    DBSeaObject, objectCopier, baseTypes;

type

{$RTTI INHERIT}
    TWaterProps = class(TDBSeaObject)
        { class for the different water properties.  }
    private
        fName       : string;
        fComments   : string;
        fID         : integer;
        fDataSource : TDataSource;  //either MDA or User

        fTemperature, fSalinity: double;
        fVelocity, fDirection: double; // direction in radians, acw from right = 0 (x = +1 y = 0)
    public
        constructor Create(const aName: string; aTemperature, aSalinity: double; aID: integer; aDataSource: TDataSource);
        destructor Destroy; override;
        // function  Clone : TWaterProps;
        procedure cloneFrom(const source: TWaterProps);
        procedure setProps(const aTemperature, aSalinity: double);
        function seawaterAbsorptionCoefficient(const dFreq, dDepth: double): double; overload;

        procedure setWithListAndWeightings(const arr: TArray<TWaterProps>; const weightings: TDoubleDynArray);

        property Name: string read fName write fName;
        property Comments: string read fComments write fComments;
        property ID: integer read fID write fID;
        property DataSource: TDataSource read fDataSource write fDataSource;
        property Temperature: double read fTemperature write fTemperature;
        property Salinity: double read fSalinity write fSalinity;
        property Velocity: double read fVelocity write fVelocity;
        property Direction: double read fDirection write fDirection;
        // property  pH : Double read fpH write fpH;

        class function seawaterAbsorptionCoefficient(const dTemp, dFreq, dDepth: double): double; overload;
        class function createFromJSON(v: TJSONValue): TWaterProps;
    end;

    TWaterRange = record
        range: double;
        water: TWaterProps;

        class function create(r: double; wp: TWaterProps): TWaterRange; static;
    end;
    TWaterRangeArray = array of TWaterRange;
    TWaterRangeArrayHelper = record helper for TWaterRangeArray
        function  waterForRange(const range: double; out index: integer): TWaterProps;
    end;

implementation


constructor TWaterProps.Create(const aName: string; aTemperature, aSalinity: double; aID: integer; aDataSource: TDataSource);
begin
    inherited Create;

    self.SaveBlacklist := '';

    {create a material given the 3 necessary properties. comments field leave blank for now}
    self.fName := aName;
    setProps(aTemperature,aSalinity);
    self.fComments := '';
    self.fVelocity := 0;
    self.fDirection := 0;
    self.fID := aID;
    self.fDataSource := aDataSource;
end;

destructor TWaterProps.Destroy;
begin
    inherited;
end;

//function TWaterProps.Clone : TWaterProps;
//begin
//    result := TWaterProps.Create(self.Name,self.Temperature,self.Salinity,self.ID,self.DataSource);
//    result.fComments := self.Comments;
//    result.Velocity := self.Velocity;
//    result.Direction := self.Direction;
//end;

procedure TWaterProps.cloneFrom(const source: TWaterProps);
begin
    TObjectCopier.CopyObject(source,self);
end;

procedure TWaterProps.setProps(const aTemperature, aSalinity: double);
begin
    self.Temperature := aTemperature;
    self.Salinity := aSalinity;
    //self.pH := apH;
end;

procedure TWaterProps.setWithListAndWeightings(const arr: TArray<TWaterProps>; const weightings: TDoubleDynArray);
var
    i: integer;
    vx,vy: double;
begin
    fTemperature := 0;
    fSalinity := 0;

    vx := 0;
    vy := 0;

    if length(arr) <> length(weightings) then
        raise Exception.Create(format('Can''t set water properties, array lengths don''t match: %d %d',[length(arr), length(weightings)]));

    for i := 0 to length(arr) - 1 do
    begin
        fTemperature := fTemperature + weightings[i] * arr[i].fTemperature;
        fSalinity := fSalinity + weightings[i] * arr[i].fSalinity;

        vx := vx + weightings[i] * arr[i].fVelocity * cos(arr[i].fVelocity);
        vy := vy + weightings[i] * arr[i].fVelocity * sin(arr[i].fVelocity);
    end;
    self.fVelocity := hypot(vx,vy);
    self.fDirection := ArcTan2(vy,vx);
end;

class function TWaterProps.createFromJSON(v: TJSONValue): TWaterProps;
var
    o: TJSONObject;
begin
    try
        o := TJSONObject(v);

        result := TWaterProps.create('',0,0,0,TDataSource.dsUser);

        if not assigned(o.getValue('temperature')) or not tryStrToFloat(o.getValue('temperature').ToString, result.fTemperature) then
            raise EJSONParsing.Create('Could not parse water temperature');
        if not assigned(o.getValue('salinity')) or not tryStrToFloat(o.getValue('salinity').ToString, result.fSalinity) then
            raise EJSONParsing.Create('Could not parse water salinity');
        if not assigned(o.getValue('velocity')) or not tryStrToFloat(o.getValue('velocity').ToString, result.fVelocity) then
            raise EJSONParsing.Create('Could not parse water velocity');
        if not assigned(o.getValue('direction')) or not tryStrToFloat(o.getValue('direction').ToString, result.fDirection) then
            raise EJSONParsing.Create('Could not parse water direction');

        if assigned(o.getValue('name')) then
            o.getValue('name').TryGetValue<string>(result.fName);
        if assigned(o.getValue('comments')) then
            o.getValue('comments').TryGetValue<string>(result.fComments);

        if (result.Salinity < 0) then
            raise EJSONParsing.Create('Water salinity cannot be negative');

    except on e: Exception do
        exit(nil);
    end;
end;

function TWaterProps.seawaterAbsorptionCoefficient(const dFreq,dDepth: double): double;
begin
    result := TWaterProps.seawaterAbsorptionCoefficient(self.Temperature,dFreq,dDepth);
end;

class function TWaterProps.seawaterAbsorptionCoefficient(const dTemp,dFreq,dDepth: double): double;
{seawater absorption in dB/m. From Fisher and Simmons 1977 I think?}
var
    Tk,P,A1,P1,f1,A2,P2,f2,A3,P3,Boric,MgSO4,H2O : double;
    dTemp2, dFreq2 : double;
begin
    //dTemp Temperature (degC)%
    //Salinity := aWater.Salinity; //Salinity (ppt)
    //Depth := 10; //Depth (m)
    //pH := 8; //Acidity

    Tk := 273.1 + dTemp;
    P := dDepth / 10.0;

    dTemp2 := dTemp*dTemp;
    dFreq2 := dFreq*dFreq;

    // Boric acid contribution
    A1 := 1.03E-8 + 2.36E-10 * dTemp - 5.22E-12 * dTemp2;
    P1 := 1;
    f1 := 1.32E3 * Tk * exp(-1700 / Tk);
    Boric := (A1 * P1 * f1 * dFreq2)/(dFreq2 + f1*f1);
    // MgSO4 contribution
    A2 := 5.62E-8 + 7.52E-10 * dTemp;
    P2 := 1 - 10.3E-4 * P + 3.7E-7 * P*P;
    f2 := 1.55E7 * Tk * exp(-3052 / Tk);
    MgSO4 := (A2 * P2 * f2 * dFreq2)/(dFreq2 + f2*f2);

    // Pure water contribution
    A3 := (55.9 - 2.37 * dTemp + 4.77E-2 * dTemp2 - 3.48E-4 * dTemp*dTemp2 ) * 1E-15;
    P3 := 1 - 3.84E-4 * P + 7.57E-8 * P*P;
    H2O := A3 * P3 * dFreq2;

    // Total absorption (dB/km) ( * 8.686 converts to dB/m)
    result := (Boric + MgSO4 + H2O) * 8.686;
end;


{ TWaterRange }

class function TWaterRange.create(r: double; wp: TWaterProps): TWaterRange;
begin
    result.range := r;
    result.water := wp;
end;

{ TWaterRangeArrayHelper }

function TWaterRangeArrayHelper.waterForRange(const range: double; out index: integer): TWaterProps;
var
    i: integer;
begin
    result := TWaterRange(self[0]).water;
    index := 0;
    for i := 1 to length(self) - 1 do
        if TWaterRange(self[i]).range <= range then
        begin
            result := TWaterRange(self[i]).water;
            index := i;
        end
        else
            exit();
end;

end.
