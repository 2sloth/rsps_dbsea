unit SolveReturn;

interface

uses
    system.types, system.classes,
    generics.collections;

type

    TSolveErrorType = (kNoError,
                       kNoScenariosEnabled,
                       kNoSpectrumOrTimeSeries,
                       kNoSourceSpectrum,
                       kStationarySourceBelowSeabed,
                       kScenarioNoSources,
                       kArraysNotCorrectlyAssigned,
                       kStationarySourceOnLand,
                       kNotSingleSeafloorScheme,
                       kIgnoringScenario);

    TSolveErrorTypeHelper = record helper for TSolveErrorType
        function isWarning: boolean;
        function isError: boolean;
    end;

    TSolveError = class(TObject)
        error: TSolveErrorType;
        scenarioInd, sourceInd: integer;
        freqs: TDoubleDynArray;
        body: string;

        constructor create(const aError: TSolveErrorType; const aBody: string; const scenInd, srcInd: integer);
        destructor Destroy; override;

        function  isError: boolean;
    end;
    TSolveErrorList = TObjectList<TSolveError>;

    TSolveReturn = class(TObject)
        errors: TSolveErrorList;
        messageBuffer: TStringlist;

        constructor create;
        destructor Destroy; override;

        function  success: boolean;
        function  hasError(const aError: TSolveErrorType):boolean;
    end;

implementation

constructor TSolveReturn.create;
begin
    inherited;

    errors := TSolveErrorList.create(true);
    messageBuffer := TStringlist.create;
end;

destructor TSolveReturn.Destroy;
begin
    errors.Free;
    messageBuffer.Free;

    inherited;
end;

function TSolveReturn.hasError(const aError: TSolveErrorType): boolean;
var
    err: TSolveError;
begin
    for err in errors do
        if err.error = aError then
            exit(true);

    result := false;
end;

function TSolveReturn.success: boolean;
var
    err: TSolveError;
begin
    if errors.count > 0 then exit(false);

    for err in errors do
        if err.isError then
            exit(false);

    result := true;
end;

constructor TSolveError.create(const aError: TSolveErrorType; const aBody: string; const scenInd, srcInd: integer);
begin
    inherited Create;

    self.error := aError;
    self.body := aBody;
    self.scenarioInd := scenInd;
    self.sourceInd := srcInd;
end;

destructor TSolveError.Destroy;
begin
    inherited;
end;

function TSolveError.isError: boolean;
begin
    result := error.isError;
end;

function TSolveErrorTypeHelper.isError: boolean;
begin
    result := (self <> kNoError) and (not self.isWarning);
end;

function TSolveErrorTypeHelper.isWarning: boolean;
begin
    result := self in [kNotSingleSeafloorScheme,kIgnoringScenario];
end;

end.
