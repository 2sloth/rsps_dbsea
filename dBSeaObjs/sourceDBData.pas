unit sourceDBData;

{$B-}
interface

uses system.rtti, system.math, system.sysutils,
    generics.Collections,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, helperFunctions, baseTypes, spectrum,
    objectCopier, material, mcklib, equipReturn;

type

{$RTTI INHERIT}
    TSourceDBData = class(TDBSeaObject)
    public
        equipmentReturns: array [0..5] of TEquipReturn; //0..3 = equipment, 4 = mitigation, 5 = total

        constructor Create;
        destructor Destroy; override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TSourceDBData);

        function  totalEquipReturn: TEquipReturn;
        function  mitigationEquipReturn: TEquipReturn;
        function  isActive(const ind: integer): boolean;

        procedure clear;
        procedure updateTotalLev;
        function  sumLevelsByFreq(const fi: integer; const aBW: TBandwidth): double;
    end;

implementation

constructor TSourceDBData.Create;
var
    i: integer;
begin
    inherited Create;

    saveBlacklist := 'equipmentReturns';

    for i := 0 to length(equipmentReturns) - 1 do
        equipmentReturns[i] := TEquipReturn.create;

    self.mitigationEquipReturn.count := 1;
    self.mitigationEquipReturn.duty := 1;

    clear;
end;

destructor TSourceDBData.Destroy;
var
    i: integer;
begin
    for i := 0 to length(equipmentReturns) - 1 do
        equipmentReturns[i].Free;

    inherited;
end;

procedure TSourceDBData.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    inherited;
    if not assigned(parentNode) then exit;

    //equipment returns
    writeIntegerField(parentNode,'EquipmentReturnCount',length(equipmentReturns));
    ChildNode := parentNode.AddChild('EquipmentReturnArray');
    ChildNode.Attributes['type'] := TEquipReturn.ClassName;
    for i := 0 to length(equipmentReturns) - 1 do
        equipmentReturns[i].writeDataToXMLNode(ChildNode.AddChild('EquipmentReturn' + tostr(i)));
end;

procedure TSourceDBData.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
    i: integer;
begin
    inherited;
    if not assigned(parentNode) then exit;

    for i := 0 to length(equipmentReturns) - 1 do
    begin
        equipmentReturns[i].clear;
        childNode := (parentNode as IDOMNodeSelect).selectNode('EquipmentReturnArray/EquipmentReturn' + tostr(i));
        if assigned(childNode) then
            equipmentReturns[i].readDataFromXMLNode(childNode);
    end;
end;

procedure TSourceDBData.cloneFrom(const source: TSourceDBData);
var
    i: integer;
begin
    TObjectCopier.CopyObject(source,self);

    for i := 0 to length(equipmentReturns) - 1 do
        equipmentReturns[i].cloneFrom(source.equipmentReturns[i]);

    updateTotalLev;
end;

function  TSourceDBData.totalEquipReturn: TEquipReturn;
begin
    result := equipmentReturns[length(equipmentReturns) - 1];
end;

function  TSourceDBData.mitigationEquipReturn: TEquipReturn;
begin
    result := equipmentReturns[length(equipmentReturns) - 2];
end;

function  TSourceDBData.isActive(const ind: integer): boolean;
begin
    if (ind < length(equipmentReturns) - 2) then
        result := self.equipmentReturns[ind].isActive
    else if (ind = length(equipmentReturns) - 2) then
        result := (not isnan(equipmentReturns[ind].spectrum.Level))
    else
        result := false;
end;

procedure TSourceDBData.clear;
var
    i: integer;
begin
    for i := 0 to length(equipmentReturns) - 1 do
        equipmentReturns[i].clear;
end;

procedure TSourceDBData.updateTotalLev;
var
    fi: integer;
begin
    for fi := 0 to TSpectrum.maxLength - 1 do
        totalEquipReturn.spectrum.setAmpbyInd(fi,sumLevelsByFreq(fi,bwThirdOctave),bwThirdOctave);
end;

function  TSourceDBData.sumLevelsByFreq(const fi: integer; const aBW: TBandwidth): double;
var
    i: integer;
    thisLev: double;
begin
    result := NaN;

    {sum over all the equipmentReturns. also multiply by the count for each piece of equip (=log10(count))
    NB it has to be the string values in the cells, as we can be picking up what the user typed}
    for i := 0 to length(equipmentReturns) - 3 do
    begin
        thisLev := equipmentReturns[i].getAmpCountDutyByInd(fi,aBW);

        //add lev to total for this freq
        if not isnan(thisLev) then
            if isNaN(result) then
                result := thisLev
            else
                result := dbAdd(result,thisLev);
    end;

    //mitigation doesnt use count or duty
    thisLev := mitigationEquipReturn.spectrum.getMitigationAmpByInd(fi,aBW);
    if not isnan(thisLev) then
        result := result + thisLev;
end;



end.
