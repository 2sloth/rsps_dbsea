unit ImageOverlay;

{$B-}

interface

uses system.sysutils, system.rtti, system.classes, system.types, system.json,
    system.math, system.NetEncoding, system.IOUtils,
    Soap.EncdDecd,
    vcl.graphics, vcl.imaging.jpeg,
    xml.xmldom, xml.XMLIntf,
    DBSeaObject, objectCopier, baseTypes,
    helperFunctions, nullable;

type

{$RTTI INHERIT}

    IImageOverlayDelegate = interface(IInterface)
    ['{8443A7C5-351A-471C-A768-5BFB70DEE712}']

        function getEasting: double;
        function getNorthing: double;
        function  getxMax: double;
        function  getYMax: double;
    end;

    TImageOverlay = class(TDBSeaObject)
    private
        function tryGetWorldFile(const filename: TFilename): string;
    public
        [TNoSave] image: TBytes;
        [TNoSave] width, height: nullable<integer>;
        [TNoSave] delegate: IImageOverlayDelegate;

        anchorWorldX1, anchorWorldY1{, anchorImageX1, anchorImageY1}: double;
        anchorWorldX2, anchorWorldY2{, anchorImageX2, anchorImageY2}: double;
        lockAspectRatio: boolean;

        constructor Create(const delegate: IImageOverlayDelegate);

        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TImageOverlay);

        procedure clearImage;
        function setImageHasWorldFile(const filename: TFilename): boolean;
        function readWorldFile(const filename: TFilename): boolean;
        function toBMP: TBitmap;
        function p1Valid: boolean;
        function p2Valid: boolean;
        function valid: boolean;
        procedure calcImageDims;
        function cornerPos(const corner: TRectControl): TFPoint;
        procedure setCornerPos(const x,y: double; const corner: TRectControl);
        procedure resetScale;

        function textureS(const x: double): double;
        function textureT(const y: double): double;
    end;

implementation

{ TImageOverlay }

function TImageOverlay.textureS(const x: double): double;
var
    m: double;
begin
    m := {(anchorImageX2 - anchorImageX1)} 1 /(anchorWorldX2 - anchorWorldX1);
    result := {self.anchorImageX1 + }m * (x - anchorWorldX1);
end;

function TImageOverlay.textureT(const y: double): double;
var
    m: double;
begin
    m := {(anchorImageY2 - anchorImageY1)} 1 /(anchorWorldY2 - anchorWorldY1);
    result := {self.anchorImageY1 + }m * (y - anchorWorldY1);
end;

function TImageOverlay.toBMP: TBitmap;
var
    //bmp: TBitmap;
    jpeg: TJpegimage;
    stream: TBytesStream;
begin
    result := TBitmap.create;

    //bmp := nil;
    stream := nil;
    //stream2 := nil;
    jpeg := TJPEGImage.create;
    try
        stream := TBytesStream.Create;
        stream.Write(self.image[0], length(self.image));
        stream.Seek(0,TSeekOrigin.soBeginning);
        jpeg.LoadFromStream(stream);

        //bmp := TBitmap.create;
        result.SetSize(jpeg.Width,jpeg.Height);
        result.Canvas.Draw(0, 0, jpeg);

        //stream2 := TBytesStream.create;
        //bmp.SaveToStream(stream2);
        //stream2.Seek(0,TSeekOrigin.soBeginning);
        //result.LoadFromStream(stream2);

    finally
        jpeg.free;
        stream.free;
        //stream2.Free;
        //bmp.free;
    end;
end;

function TImageOverlay.valid: boolean;
begin
    result := (length(self.image) > 0) and self.p1Valid and self.p2Valid;
end;

procedure TImageOverlay.calcImageDims;
var
    bmp: TBitmap;
begin
    if length(self.image) = 0 then
    begin
        self.width.null;
        self.height.null;
        exit;
    end;

    bmp := nil;
    try
        try
            bmp := self.toBMP;
        except
            self.width.null;
            self.height.null;
            exit;
        end;
        self.width := bmp.Width;
        self.height := bmp.Height;
    finally
        bmp.free;
    end;
end;

procedure TImageOverlay.clearImage;
begin
    setlength(self.image, 0);

    width.null;
    height.null;

    anchorWorldX1 := nan;
    anchorWorldY1 := nan;
    //anchorImageX1 := 0;
    //anchorImageY1 := 0;
    anchorWorldX2 := nan;
    anchorWorldY2 := nan;
    //anchorImageX2 := 1;
    //anchorImageY2 := 1;
end;

procedure TImageOverlay.cloneFrom(const source: TImageOverlay);
begin
    TObjectCopier.CopyObject(source,self);

    setlength(self.image, length(source.image));
    if length(source.image) > 0 then
        move(source.image[0], self.image[0], length(source.image));

    self.width := source.width;
    self.height := source.height;
end;

function TImageOverlay.cornerPos(const corner: TRectControl): TFPoint;
begin
    case corner of
    kLL, kUL: result.x := self.anchorWorldX1;
    kLR, kUR: result.x := self.anchorWorldX2;
    kCenter: result.x := (self.anchorWorldX1 + self.anchorWorldX2) / 2;
    end;
    case corner of
    kLL, kLR: result.y := self.anchorWorldY1;
    kUL, kUR: result.y := self.anchorWorldY2;
    kCenter: result.y := (self.anchorWorldY1 + self.anchorWorldY2) / 2;
    end;
end;

constructor TImageOverlay.Create(const delegate: IImageOverlayDelegate);
begin
    inherited create;

    self.fSaveBlacklist := 'image,width,height';

    lockAspectRatio := true;
    self.delegate := delegate;
    self.clearImage;
end;

function TImageOverlay.p1Valid: boolean;
begin
    result := not (isnan(self.anchorWorldX1) or
        isnan(self.anchorWorldY1) {or
        isnan(self.anchorImageX1) or
        isnan(self.anchorImageY1)});
end;

function TImageOverlay.p2Valid: boolean;
begin
    result := not (isnan(self.anchorWorldX2) or
        isnan(self.anchorWorldY2) {or
        isnan(self.anchorImageX2) or
        isnan(self.anchorImageY2)});
end;

procedure TImageOverlay.readDataFromXMLNode(const parentNode: IDOMNode);
var
    sel: IDOMNodeSelect;
begin
    inherited;
    if not assigned(parentNode) then exit;

    sel := parentNode as IDOMNodeSelect;

    if assigned(sel.selectNode('image')) and not self.readStringField(sel, 'image').Equals(NullString) then
        self.image := DecodeBase64(ansistring(self.readStringField(sel, 'image')))
    else
        setlength(self.image, 0);

    self.calcImageDims;
end;

function TImageOverlay.readWorldFile(const filename: TFilename): boolean;
var
    ss: TArray<string>;
    dx,dy,x0,y0: double;
begin
    ss := THelper.readFileToString(filename).Replace(#13#10, #10).Split([#10]);
    if length(ss) <> 6 then exit(false);
    if not TryStrToFloat(ss[0], dx) then exit(false);
    if not TryStrToFloat(ss[3], dy) then exit(false);
    if not TryStrToFloat(ss[4], x0) then exit(false);
    if not TryStrToFloat(ss[5], y0) then exit(false);
    if not width.HasValue or not height.HasValue then exit(false);

    self.anchorWorldX1 := x0 - delegate.getEasting;
    self.anchorWorldX2 := self.anchorWorldX1 + dx * width.val;
    self.anchorWorldY2 := y0 - delegate.getNorthing; //because dbsea uses lower left, but world file uses upper left
    self.anchorWorldY1 := self.anchorWorldY2 + dy * height.val;

    //self.anchorImageX1 := 0;
    //self.anchorImageY1 := 0;
    //self.anchorImageX2 := 1;
    //self.anchorImageY2 := 1;

    result := true;
end;

procedure TImageOverlay.resetScale;
begin
    self.anchorWorldX1 := 0;
    self.anchorWorldY1 := 0;
    if (not (self.width.HasValue and self.height.HasValue)) or (self.width.val = self.height.val) then
    begin
        self.anchorWorldX2 := delegate.getxMax;
        self.anchorWorldY2 := delegate.getYMax;
        exit;
    end;

    if self.width.val > self.height.val then
    begin
        self.anchorWorldX2 := delegate.getxMax;
        self.anchorWorldY2 := delegate.getxMax * self.height.val / self.width.val;
    end
    else
    begin
        self.anchorWorldX2 := delegate.getyMax * self.width.val / self.height.val;
        self.anchorWorldY2 := delegate.getYMax;
    end;
end;

procedure TImageOverlay.writeDataToXMLNode(const parentNode: IXMLNode);
    function getImageString(): string;
    begin
        if length(self.image) = 0 then
            result := NullString
        else
            result := string( EncodeBase64(self.image, length(self.image)));
    end;
begin
    inherited;
    if not assigned(parentNode) then exit;

    self.writeStringField( parentNode, 'image', getImageString);
end;

procedure TImageOverlay.setCornerPos(const x, y: double; const corner: TRectControl);
var
    currentWidth, currentHeight, widthRatio, heightRatio, ratio: double;
begin
    if corner = kCenter then
    begin
        currentWidth := anchorWorldX2 - anchorWorldX1;
        currentHeight := anchorWorldY2 - anchorWorldY1;
        anchorWorldX1 := x - currentWidth / 2;
        anchorWorldX2 := x + currentWidth / 2;
        anchorWorldY1 := y - currentHeight / 2;
        anchorWorldY2 := y + currentHeight / 2;
        exit;
    end;

    if lockAspectRatio then
    begin
        currentWidth := abs(self.anchorWorldX2 - self.anchorWorldX1);
        currentHeight := abs(self.anchorWorldY2 - self.anchorWorldY1);

        case corner of
            kLL,kUL: widthRatio := abs(self.anchorWorldX2 - x) / currentWidth;
            kLR,kUR: widthRatio := abs(x - self.anchorWorldX1) / currentWidth;
            else widthRatio := 1;
        end;
        case corner of
            kLL,kLR: heightRatio := abs(self.anchorWorldY2 - y) / currentHeight;
            kUL,kUR: heightRatio := abs(y - self.anchorWorldY1) / currentHeight;
            else heightRatio := 1;
        end;

        if (widthRatio < 1) and (heightRatio < 1) then
            ratio := min(widthRatio, heightRatio)
        else
            ratio := max(widthRatio, heightRatio);

        case corner of
            kLL,kUL: anchorWorldX1 := anchorWorldX2 - ratio * currentWidth;
            kLR,kUR: anchorWorldX2 := anchorWorldX1 + ratio * currentWidth;
        end;
        case corner of
            kLL,kLR: anchorWorldY1 := anchorWorldY2 - ratio * currentHeight;
            kUL,kUR: anchorWorldY2 := anchorWorldY1 + ratio * currentHeight;
        end;
    end
    else
    begin
        case corner of
            kLL,kUL: anchorWorldX1 := x;
            kLR,kUR: anchorWorldX2 := x;
        end;
        case corner of
            kLL,kLR: anchorWorldY1 := y;
            kUL,kUR: anchorWorldY2 := y;
        end;
    end;
end;

function TImageOverlay.setImageHasWorldFile(const filename: TFilename): boolean;
var
    jgwFile: String;
begin
    //self.picture.LoadFromFile(filename);
    self.image := THelper.ReadFileToBytes(filename);
    self.calcImageDims;

    jgwFile := tryGetWorldFile(filename);
    result := (not jgwFile.isempty) and FileExists(jgwFile) and self.readWorldFile(jgwFile);
    if not result then
        self.resetScale;
end;

function TImageOverlay.tryGetWorldFile(const filename: TFilename): string;
var
    s: string;
begin
    s := IncludeTrailingPathDelimiter(TPath.GetDirectoryName(filename)) + TPath.GetFileNameWithoutExtension(filename) + '.jgw';
    if FileExists(s) then
        exit(s);

    s := IncludeTrailingPathDelimiter(TPath.GetDirectoryName(filename)) + TPath.GetFileNameWithoutExtension(filename) + '.jpgw';
    if FileExists(s) then
        exit(s);

    s := IncludeTrailingPathDelimiter(TPath.GetDirectoryName(filename)) + TPath.GetFileNameWithoutExtension(filename) + '.jpegw';
    if FileExists(s) then
        exit(s);

    result := '';
end;

end.
