unit GeoOverlay;

interface

uses system.math, system.SysUtils, system.UITypes,
    system.Generics.collections,
    xml.XMLIntf, xml.xmldom,
    dbseaobject, objectCopier, baseTypes, WrappedFPointArray,
    ShpAPI129, latlong2utm;

type

    TGeoOverlay = class(TDBSeaObject)
    private
    public
        name: string;
        comments: string;
        sourceFile: string;
        z, fillOpacity: double;
        visible, fillVisible: boolean;
        color: TColor;
        [TNoSave] loops: TWrappedFPointArrayList;

        constructor Create();
        destructor Destroy; override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TGeoOverlay);

        procedure removeEastingNorthing(const easting, northing: double);
        procedure convertFromLatLng();
        function  isProbablyLatLng: boolean;
        class function overlayFromShapeFile(const filename: TFilename): TGeoOverlay;
    end;
    TGeoOverlays = class(TMyObjectList<TGeoOverlay>)
        procedure writeDataToXMLNode(const parentNode: IXMLNode);
        procedure readDataFromXMLNode(const parentNode: IDOMNode);
        procedure cloneFrom(const source: TGeoOverlays);
    end;

implementation

procedure TGeoOverlay.cloneFrom(const source: TGeoOverlay);
begin
    TObjectCopier.CopyObject(source,self);
    loops.cloneFrom(source.loops);
end;

procedure TGeoOverlay.convertFromLatLng;
var
    i, refZone: integer;  //can't loop for p, as then p would be const
    loop: TWrappedFPointArray;
    tmp: TEastNorth;
begin
    refZone := -1;
    for loop in loops do
        for i := 0 to length(loop.points) - 1 do
        begin
            if refZone = -1 then
                refZone := TLatLong2UTM.getLongZone(TFPoint(loop.points[i]).x);

            tmp.longitude := TFPoint(loop.points[i]).x;
            tmp.latitude := TFPoint(loop.points[i]).y;
            tmp := TLatLong2UTM.latlong2UTM(tmp, refZone);
            TFPoint(loop.points[i]).setXY(tmp.easting,tmp.northing);
        end;
end;

constructor TGeoOverlay.Create();
begin
    inherited create;

    self.name := 'New overlay';
    self.loops := TWrappedFPointArrayList.create(true);
    self.visible := true;
    self.fillVisible := false;
    self.fillOpacity := 0.3;
end;

destructor TGeoOverlay.Destroy;
begin
    loops.Free;

  inherited;
end;

function TGeoOverlay.isProbablyLatLng: boolean;
var
    p: TFPoint;
    loop: TWrappedFPointArray;
begin
    for loop in loops do
        for p in loop.points do
            if (p.x < -360) or (p.x > 360) or (p.y < -90) or (p.y > 90) then
                exit(false);
    result := true;
end;

class function TGeoOverlay.overlayFromShapeFile(const filename: TFilename): TGeoOverlay;
var
    i, j: integer;
    hSHP: PSHPInfo;
    psShape: PSHPObject;
    nShapeType, nEntities: LongInt;
    adfMinBound, adfmaxBound: Array [0 .. 3] Of Double;
    ansiPath: AnsiString;
    loop: TWrappedFPointArray;
begin
    ansiPath := AnsiString(filename);
    hShp := SHPOpen(PAnsiChar(@ansiPath[1]),'rb');
    ShpGetInfo(hShp,@nEntities,@nShapeType,@adfMinBound,@adfMaxBound);

    result := TGeoOverlay.create();
    result.sourceFile := filename;
    for i := 0 to nEntities - 1 do
    begin
        psShape := SHPReadObject(hSHP, i);
        loop := TWrappedFPointArray.create;

        setlength(loop.points, psShape^.nVertices);
        for j := 0 to psShape^.nVertices - 1 do
        begin
            loop.points[j].x := psShape^.padfX[j];
            loop.points[j].y := psShape^.padfY[j];
        end;
        SHPDestroyObject( psShape );

        result.loops.add(loop);
    end;
    SHPClose( hSHP );
end;

procedure TGeoOverlay.readDataFromXMLNode(const parentNode: IDOMNode);
begin
    inherited;
    loops.readDataFromXMLNode(parentNode, 'Loop');
end;

procedure TGeoOverlay.removeEastingNorthing(const easting, northing: double);
var
    i: integer;
    loop: TWrappedFPointArray;
begin
    for loop in loops do
        for i := 0 to length(loop.points) - 1 do
            TFPoint(loop.points[i]).setXY(TFPoint(loop.points[i]).x - easting,TFPoint(loop.points[i]).y - northing);
end;

procedure TGeoOverlay.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;

    if not assigned(parentNode) then exit;

    loops.writeDataToXMLNode(parentNode, 'Loop');
end;

{ TGeoOverlays }

procedure TGeoOverlays.cloneFrom(const source: TGeoOverlays);
var
    o: TGeoOverlay;
begin
    self.clear;

    for o in source do
    begin
        self.add(TGeoOverlay.create());
        self.last.cloneFrom(o);
    end;
end;

procedure TGeoOverlays.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
    Sel: IDOMNodeSelect;
begin
    inherited;

    if not assigned(parentNode) then exit;
    Sel := parentNode as IDOMNodeSelect;

    self.Clear;
    repeat
        childNode := Sel.selectNode('OverlaysList/Overlay' + inttostr(self.count));
        if assigned(childNode) then
        begin
            self.Add(TGeoOverlay.create());
            self.Last.readDataFromXMLNode(childNode);
        end;
    until not assigned(childNode);
end;

procedure TGeoOverlays.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    inherited;

    if not assigned(parentNode) then exit;

    TDBSeaObject.writeIntegerField(parentNode,'OverlaysCount',self.Count);
    ChildNode := TDBSeaObject.writeListField(parentNode,'OverlaysList');
    for i := 0 to self.Count - 1 do
        self[i].writeDataToXMLNode(ChildNode.AddChild('Overlay' + inttostr(i)));
end;

end.
