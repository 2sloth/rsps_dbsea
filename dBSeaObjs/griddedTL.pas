unit griddedTL;

{$B-}

interface

uses system.math, system.types, system.sysutils, system.classes,
    vcl.dialogs, vcl.clipbrd, vcl.graphics, vcl.forms,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, basetypes, helperFunctions, objectCopier,
    dBSeaconstants, mcomplex, pos3,
    spectrum, eventlogging, memoryStreamHelper, MyCompression, SmartPointer;

type
    EGriddedTLWrongSize = class(Exception);

    IGriddedTLDelegate = interface(IInterface)
      function  startfiFromParent: integer;
      function  endfiFromParent: integer;
      function  lenfiFromParent: integer;
      function  dmaxFromParent: integer;
    end;

    IGriddedTLRADelegate = interface(IInterface)
      function  getimax: integer;
      function  getjmax: integer;
      function  getPos1: TPos3;
      function  getPos2: TPos3;
    end;

    TGriddedTL = class(TDBSeaObject)
      private
        TL             : array{fi, same as spectrum fi} of array{i} of array{j} of TSingleDynArray{d};
        delegate       : IGriddedTLDelegate;
        RADelegate     : IGriddedTLRADelegate;
        eventlog       : TEventLog;

        class function  compressionStart(const fi: integer): string;
        class function  compressionEnd(const fi: integer): string;
        function  checkIndices(const fi: integer; const aX, aY: double; const d: integer): boolean; overload;
        function  checkIndices(const fi, i, j, d: integer): boolean; overload;

        function  pos1: TPos3;
        function  pos2: TPos3;
        function  dx: double;
        function  dy: double;
      public
        constructor Create(const aDelegate: IGriddedTLDelegate; const aRADelegate: IGriddedTLRADelegate; const aeventlog: TEventlog);
        procedure cloneFrom(const source: TGriddedTL);

        procedure setTLLengthZero();
        procedure setLengthTL0;
        function  iMax: integer;
        function  jMax: integer;
        function  dMax: integer;
        procedure updateStorageSizeAndNaN;

        procedure readCompressedFromStream(const stream: TMemoryStream; const mustBeAfterPos: cardinal);
        procedure writeCompressedToStream(const stream: TMemoryStream);
        function  getSerialisedTLString: string;
        procedure deserialiseTLFromString(const s: string);
        function  xs(const ind: integer): double;
        function  ys(const ind: integer): double;

        function  getTL(const aX, aY: double; const d, fi: integer): single;
        function  getTLComplex(const aX, aY: double; const d, fi: integer): TComplex;
        procedure setTLMinByInd(const newTL: single; const fi,i,j,d: integer);
        procedure invAddToTLByInd(const aLev: single; const fi,i,j,d: integer);
        function  bSizeIsAtPlannedSize: boolean;

        procedure outputToClipboard;
    end;

implementation

constructor TGriddedTL.Create(const aDelegate: IGriddedTLDelegate; const aRADelegate: IGriddedTLRADelegate; const aeventlog: TEventlog);
begin
    inherited Create;

    self.SaveBlacklist := 'delegate,TL,bSizeIsAtPlannedSize' +
                          ',fPos1,fPos2,theRA,eventlog' +
                          ',RADelegate';

    self.delegate := aDelegate;
    self.RADelegate := aRADelegate;
    self.eventlog := aeventlog;

    self.setLengthTL0;
end;

class function TGriddedTL.compressionStart(const fi: integer): string;
begin
    result := '_GriddedTLFreq' + inttostr(fi) + 'Start_';
end;

class function TGriddedTL.compressionEnd(const fi: integer): string;
begin
    result := '_GriddedTLFreq' + inttostr(fi) + 'End_';
end;

procedure TGriddedTL.writeCompressedToStream(const stream: TMemoryStream);
var
    fi, i, j, d: integer;
    s: TSingleDynArray;
begin
     if not self.bSizeIsAtPlannedSize then exit;

    setlength(s, 2 + self.iMax * self.jMax * self.dMax);
    s[0] := self.iMax;
    s[1] := self.jMax;

    // fi i j d
    for fi := 0 to length(TL) - 1 do
    begin
        stream.myWrite(compressionStart(fi));
        if length(TL[fi]) > 0 then
        begin
            for i := 0 to length(TL[fi]) - 1 do
                for j := 0 to length(TL[fi,i]) - 1 do
                    for d := 0 to length(TL[fi,i,j]) - 1 do
                        s[2 + i * jmax * dmax + j * dmax + d] := TL[fi,i,j,d];
            stream.myWrite( TMyCompression.compressTSingleDynArrayToBytes(s) );
        end;
        stream.myWrite(compressionEnd(fi));
    end;
end;

procedure TGriddedTL.readCompressedFromStream(const stream: TMemoryStream; const mustBeAfterPos: cardinal);
var
    fi, i, j, d: integer;
    s: TSingleDynArray;
    aPos1,aPos2: int64;
begin
    self.updateStorageSizeAndNaN;

    // fi i j d
    for fi := 0 to length(TL) - 1 do
    begin
        aPos1 := THelper.BytePos(TEncoding.Unicode.GetBytes(compressionStart(fi)), stream.Memory, stream.Size, mustBeAfterPos);
        aPos1 := aPos1 + length(compressionStart(fi))*sizeof(Char);
        aPos2 := THelper.BytePos(TEncoding.Unicode.GetBytes(compressionEnd(fi)), stream.Memory, stream.Size, mustBeAfterPos);
        if aPos2 <= aPos1 then continue;

        s := TMyCompression.decompressTSingleDynArrayFromTBytes( stream.getBytes(aPos1, aPos2) );

        if length(s) > 0 then
            for i := 0 to length(TL[fi]) - 1 do
                for j := 0 to length(TL[fi,i]) - 1 do
                    for d := 0 to length(TL[fi,i,j]) - 1 do
                        TL[fi,i,j,d] := s[2 + i * jmax * dmax + j * dmax + d];
    end;
end;

procedure TGriddedTL.cloneFrom(const source: TGriddedTL);
begin
    TObjectCopier.CopyObject(source,self);
    //TL?
end;

procedure TGriddedTL.setLengthTL0;
begin
    setlength(TL,TSpectrum.maxLength);
    //bSizeIsAtPlannedSize := false;
end;

procedure TGriddedTL.setTLLengthZero;
begin
    setlength(self.TL, 0);
end;

procedure TGriddedTL.updateStorageSizeAndNaN;
var
    fi,i,j: integer;
begin
    if bSizeIsAtPlannedSize then exit;

    setlength(TL,0);
    setlength(TL,TSpectrum.maxLength);
    for fi := 0 to length(TL) - 1 do
    begin
        if (fi < delegate.startfiFromParent) or (fi > delegate.endfiFromParent) then
            setLength(TL[fi],0)
        else
        begin
            setlength(TL[fi], RADelegate.getiMax, RADelegate.getjMax, delegate.dmaxFromParent);
            for i := 0 to RADelegate.getiMax - 1 do
                for j := 0 to RADelegate.getjMax - 1 do
                    TL[fi,i,j].setAllVal(nan);
        end;
    end;
end;

procedure TGriddedTL.setTLMinByInd(const newTL: single; const fi,i,j,d: integer);
var
    existingTL: double;
begin
    if not bSizeIsAtPlannedSize then
    begin
        showmessage('Error: gridded TL storage not at correct size on add to TL');
        eventlog.log('Error: gridded TL storage not at correct size on add to TL');
        exit;
    end;
    if isnan(newTL) or not checkIndices(fi,i,j,d) then exit;

    existingTL := TL[fi,i,j,d];
    if isnan(existingTL) then
        TL[fi,i,j,d] := newTL
    else if newTL < existingTL then
         TL[fi,i,j,d] := newTL;
end;

procedure TGriddedTL.invAddToTLByInd(const aLev: single; const fi,i,j,d: integer);
begin
    if not bSizeIsAtPlannedSize then
    begin
        showmessage('Error: gridded TL storage not at correct size on add to TL');
        eventlog.log('Error: gridded TL storage not at correct size on add to TL');
        exit;
    end;

    if isnan(aLev) or not checkIndices(fi,i,j,d) then exit;
    if isnan(TL[fi,i,j,d]) then
        TL[fi,i,j,d] := aLev
    else
        TL[fi,i,j,d] := invdBAdd(TL[fi,i,j,d], aLev);
end;

function  TGriddedTL.iMax: integer;
var
    fi: integer;
begin
    result := 0;
    if (length(TL) = 0) then exit;
    for fi := 0 to length(TL) - 1 do
        if (length(TL[fi]) > 0) then
            exit(length(TL[fi]));
end;

function  TGriddedTL.jMax: integer;
Var
    fi: integer;
begin
    result := 0;
    if (length(TL) = 0) then exit;
    for fi := 0 to length(TL) - 1 do
        if (length(TL[fi]) > 0) then
            exit(length(TL[fi,0]));
end;

function  TGriddedTL.dMax: integer;
Var
    fi: integer;
begin
    result := 0;
    if (length(TL) = 0) then exit;
    for fi := 0 to length(TL) - 1 do
        if (length(TL[fi]) > 0) and (length(TL[fi,0]) > 0) then
            exit(length(TL[fi,0,0]));
end;

//procedure TGriddedTL.setExtents(const pos1,pos2: TPos3);
//begin
//    fPos1.cloneFrom(pos1);
//    fPos2.cloneFrom(pos2);
//end;

function  TGriddedTL.getSerialisedTLString: string;
const
    delim: char = ',';
var
    fi,i,j,d: integer;
    sb: ISmart<TStringBuilder>;
begin
    if (not bSizeIsAtPlannedSize) then exit('');

    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);

    for fi := 0 to length(TL) - 1 do
    begin
        for i := 0 to length(TL[fi]) - 1 do
        begin
            for j := 0 to length(TL[fi,i]) - 1 do
            begin
                for d := 0 to length(TL[fi,i,j]) - 1 do
                    if isnan(TL[fi,i,j,d]) then
                        sb.Append('n' + delim)
                    else
                        sb.Append(forceDotSeparator( tostr(round1(TL[fi,i,j,d])) ) + delim);
            end;
        end;
    end;
    result := sb.ToString;
end;

procedure  TGriddedTL.deserialiseTLFromString(const s: string);
const
    delim: char = ',';
var
    fi,i,j,d: integer;
    ind: integer;
    tmp: string;
begin
    updateStorageSizeAndNaN;
    ind := 1;

    for fi := 0 to length(TL) - 1 do
        for i := 0 to length(TL[fi]) - 1 do
            for j := 0 to length(TL[fi,i]) - 1 do
                for d := 0 to length(TL[fi,i,j]) - 1 do
                begin
                    tmp := '';
                    while (ind <= length(s)) and (s[ind] <> delim) do   //B- needed here, shortcircuit if outside range
                    begin
                        tmp := tmp + s[ind];
                        inc(ind);
                    end;
                    if isfloat(tmp) then
                    begin
                        TL[fi,i,j,d] := tofl(tmp);
                        //if tofl(tmp) < 800 then     {testing}
                        //    TL[fi,i,j,d] := tofl(tmp);
                    end
                    else
                        TL[fi,i,j,d] := nan;
                    inc(ind);
                end;
end;

function TGriddedTL.xs(const ind: integer): double;
begin
    result := pos1.X + round1(dx * ind);
end;

function TGriddedTL.ys(const ind: integer): double;
begin
    result := pos1.Y + (dy * ind);
end;

function  TGriddedTL.dx: double;
begin
    result := (pos2.X - pos1.X) / (imax - 1);
end;

function  TGriddedTL.pos1: TPos3;
begin
    result := RADelegate.getpos1;
end;

function  TGriddedTL.pos2: TPos3;
begin
    result := RADelegate.getpos2;
end;

function  TGriddedTL.bSizeIsAtPlannedSize: boolean;
begin
    if (length(TL) <> TSpectrum.maxLength) then exit(false);
    if (self.iMax <> RADelegate.getiMax) then exit(false);
    if (self.jMax <> RADelegate.getjMax) then exit(false);
    if (self.dMax <> delegate.dmaxFromParent) then exit(false);
    result := true;
end;

function  TGriddedTL.dy: double;
begin
    result := (pos2.Y - pos1.Y) / (jmax - 1);
end;

function TGriddedTL.checkIndices(const fi: integer; const aX, aY: double; const d: integer): boolean;
var
    i,j: integer;
begin
    if (aX < pos1.x) or (aY < pos1.y) or (aX > pos2.x) or (aY > pos2.y) or (dx = 0) or (dy = 0) then exit(false);
    i := saferound(aX / dx);
    j := saferound(aY / dy);
    if (fi >= length(TL)) or (i >= length(TL[fi])) or (j >= length(TL[fi,i])) or (d >= length(TL[fi,i,j])) then exit(false);
    result := true;
end;

function  TGriddedTL.checkIndices(const fi, i, j, d: integer): boolean;
begin
    result := not ( (fi >= length(TL)) or (i >= length(TL[fi])) or (j >= length(TL[fi,i])) or (d >= length(TL[fi,i,j])) );
end;

function  TGriddedTL.getTL(const aX, aY: double; const d, fi: integer): single;
begin
    if not bSizeIsAtPlannedSize then
        raise EGriddedTLWrongSize.Create('Source gridded TL storage not at correct size on get TL');

    if not checkIndices(fi,aX,aY,d) then
    begin
        eventlog.log('Error: gridded tl attempt to read TL outside range');
        exit(nan);
    end;
    result := TL[fi,saferound(aX / dx),saferound(aY / dy),d];
end;

function  TGriddedTL.getTLComplex(const aX, aY: double; const d, fi: integer): TComplex;
begin
    if not bSizeIsAtPlannedSize then
        raise EGriddedTLWrongSize.Create('Source gridded TL storage not at correct size on get TL');

    if not checkIndices(fi,aX,aY,d) then
    begin
        eventlog.log('Error: gridded tl attempt to read TL complex outside range');
        exit(nanComplex);
    end;
    //TODO
    result := nanComplex; //TL[fi,saferound(aX / dx),saferound(aY / dy),d];
end;

procedure  TGriddedTL.outputToClipboard;
var
    fi,i,j,d: integer;
    m: double;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    for fi := 0 to length(TL) - 1 do
    begin
        for i := 0 to length(TL[fi]) - 1 do
        begin
            for j := 0 to length(TL[fi,i]) - 1 do
            begin
                m := TL[fi,i,j,0];
                for d := 0 to length(TL[fi,i,j]) - 1 do
                    greater(m,TL[fi,i,j,d]);
                sb.append(tostr(m) + ';');
            end;
            sb.append(#13#10);
        end;

        sb.Append(#13#10#13#10#13#10);
    end;

    THelper.setClipboard(sb.ToString);
end;

end.
