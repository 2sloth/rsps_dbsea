unit spectrum;

{$B-}
interface

uses system.math, system.sysutils, system.strutils, system.types, system.rtti,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, baseTypes,
     helperFunctions, objectCopier,
     eventlogging, mcklib, SmartPointer;

type
{$RTTI INHERIT}


    TSpectrum = class(TDBSeaObject)
    {all spectra use this class. each object contains the full range from low to hi freq,
    but you can also request a level between any 2 freqs (and based on either 1/3 octave
    or octave bandwidth. still need to do a little work on how 1/3 oct vs octave data is
    handled, if the user changes from one to the other.
    Note that length(fAmp) = OctaveBandsLength when octave bands, and
    length(fAmp) = ThirdOctaveBandsLength when third octave bands}
    private
    const
        OctaveBandsLength = 14;
        ThirdOctaveBandsLength = 42;
        fFreq: array [0..ThirdOctaveBandsLength - 1] of double = (12.5,16,20,25,31.5,40,50,63,80,100,125,
                                                                   160,200,250,315,400,500,630,800,1000,
                                                                   1250,1600,2000,2500,3150,4000,5000,6300,
                                                                   8000,10000,12500,16000,20000,25000,32000,
                                                                   40000,50000,64000,80000,100000,128000,160000);

    var
        fAmp: TSingleDynArray; // amplitude and frequency. TODO change to fixed length, and make separate for oct/third oct?

        class function  getInd(const fi: integer; const bw: TBandwidth; const aEventLog: TEventLog = nil): integer;
        //function getIndbyfreq(const fget: double; bw: TBandwidth): integer;
    public
        constructor Create;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TSpectrum);

        function  getAmpbyFreq(const fget: double; const bw: TBandwidth): single;
        procedure setAmpbyInd(const fi: integer; const Anew: single; const bw: TBandwidth);
        procedure addAmpbyInd(const fi: integer; const Anew: single; const bw: TBandwidth);
        function  getAmpbyInd(const fi: integer; const accessorBW: TBandwidth; const aEventlog: TEventLog = nil): single;

        procedure setMitigationAmpbyInd(const fi: integer; const Anew: single; const bw: TBandwidth; const aEventlog: TEventLog = nil);
        function  getMitigationAmpbyInd(const fi: integer; const accessorBW: TBandwidth; const aEventlog: TEventLog = nil): single;

        function  Level: single; overload;
        function  Level(const startfi, endfi: integer; const accessorBW: TBandwidth): single; overload;
        procedure clear;
        procedure zeroAmp;

        class function evenlySpacedFrequenciesInBand(const f: double; const bw: TBandwidth; const n: integer): TDoubleDynArray;
        class function getFbyInd(const fi: integer; const bw: TBandwidth; const aEventlog: TEventLog = nil): double;
        class function getFStringbyInd(const fi: integer; const bw: TBandwidth; const aEventlog: TEventLog = nil): String;
        class function convertSpectrum(const inLevs: TSingleDynArray; const fromBW, toBW: TBandWidth): TSingleDynArray;
        class function maxLength: integer;
        class function getLength(const aBW: TBandwidth): integer;
        class function bandwidthString(const aBW: TBandwidth): string;
        class function bandwidthFromString(const s: string; out bw: TBandwidth): boolean;
    end;


implementation

constructor TSpectrum.Create;
begin
    inherited Create;

    self.saveBlacklist := '';

    setlength(fAmp, ThirdOctaveBandsLength);
    clear;
end;

class function TSpectrum.evenlySpacedFrequenciesInBand(const f: double;
  const bw: TBandwidth; const n: integer): TDoubleDynArray;
var
    lowF,k: double;
    i: integer;
begin
    setlength(result, n);
    if n = 0 then
        exit
    else if n = 1 then
    begin
        setlength(result,1);
        result[0] := f;
    end
    else
    begin
        if bw = bwThirdOctave then
        begin
            lowF := f / power(2,1/6);
            k := power(power(2,1/3),1/n);
        end
        else
        begin
            lowF := f / power(2,1/2);
            k := power(2,1/n);
        end;

        result[0] := lowF * sqrt(k);
        for I := 1 to n - 1 do
            result[i] := result[i - 1] * k;
    end;
end;

procedure TSpectrum.readDataFromXMLNode(const parentNode: IDOMNode);
var
    ChildNode: IDomNode;
    SA: TSingleDynArray;
begin
    inherited;
    if not assigned(parentNode) then exit;

    //explicitly read fAmp, as RTTI doesn't work well with dynamic arrays
    childnode := (parentNode as IDOMNodeSelect).selectNode('fAmp');
    if assigned(childnode) then
    begin
        SA.deserialise(childnode.firstChild.nodeValue);
        if length(SA) = length(fAmp) then
            fAmp := SA.copy;
    end;
end;

procedure TSpectrum.cloneFrom(const source: TSpectrum);
begin
    TObjectCopier.CopyObject(source,self);

    self.fAmp := source.fAmp.copy;
end;

function TSpectrum.getAmpbyFreq(const fget: double; const bw: TBandwidth): single;
var
    fi: integer;
    tmpArray: TDoubleDynArray;
    ind: integer;
begin
    result := NaN;

    {can't call arrayfind on an array of double, only on a TVect so
    copy all the values of fFreq over to a temp array before using find}
    setlength(tmpArray,length(fFreq));
    for fi := 0 to length(fFreq) - 1 do
        tmpArray[fi] := fFreq[fi];
    fi := tmpArray.find(fget);

    if (fi >= 0) and (fi < ThirdOctaveBandsLength) then
    begin
        // if we are using a different bandwidth than that in which it is stored, then we use these cases
        case bw of
            bwOctave: begin
                ind := getInd(fi, bw);
                if not (checkRange(ind - 1, 0, ThirdOctaveBandsLength - 1) and
                        checkRange(ind + 1, 0, ThirdOctaveBandsLength - 1)) then
                begin
                    {$IFDEF DEBUG}
                    Raise Exception.Create('Frequency index out of bounds. ' + tostr(fi) + ' ' + tostr
                          (result) + ' ' + tostr( low(fAmp)) + ' ' + tostr( high(fAmp)));
                    {$ENDIF}
                    exit(nan);
                end;

                result := dBAdd( fAmp[ind - 1],   //sum the energy of the 3 third oct bands
                                dBAdd( fAmp[ind],
                                       fAmp[ind + 1]));
            end;
            bwThirdOctave: result := fAmp[fi];
        end;
    end
    else
    begin
        {$IFDEF DEBUG}
        raise Exception.Create('Error: Incorrect frequency selected');
        {$ENDIF}
        result := nan;
    end;
end;

procedure TSpectrum.setAmpbyInd(const fi: integer; const Anew: single; const bw: TBandwidth);
//call with fi + startf
const
    db3: single = 4.771216;
var
    ind: integer;
begin
    if bw = bwThirdOctave then
    begin
        if not checkrange(fi,0,ThirdOctaveBandsLength - 1) then exit;

        fAmp[fi] := Anew;
    end
    else
    begin
        //need to split the energy over 3 bands. When we add them back together, we get ANew
        if fi < 0 then exit;

        ind := fi * 3;

        if (ind + 2) >= ThirdOctaveBandsLength then exit;

        fAmp[ind    ] := Anew - db3;
        fAmp[ind + 1] := Anew - db3;
        fAmp[ind + 2] := Anew - db3;

//        if not isnan(ANew) then
//        begin
//            ashow(ANew);
//            ashow( dBAdd(fAmp[ind    ], dBAdd(fAmp[ind + 1],fAmp[ind + 2]) ) );
//            ashow( self.getAmpbyInd(fi,bw));
//        end;
    end;

//    ind := getInd(fi, bw);
//    if ind < 0 then
//    begin
//        showmessage('Error on fi = ' + tostr(fi));
//        exit; //getInd returns -1 for invalid fi
//    end;
//
//    self.fAmp[ind] := Anew;
end;

procedure TSpectrum.addAmpbyInd(const fi: integer; const Anew: single; const bw: TBandwidth);
//call with fi + startfi
var
    currentAmp: single;
begin
    if not isNaN(ANew) then
    begin
        currentAmp := getAmpbyInd(fi,bw);
        if isNaN(currentAmp) then
            setAmpbyInd(fi,Anew,bw)
        else
            setAmpbyInd(fi,dBAdd(Anew,currentAmp),bw);
    end;
end;


function TSpectrum.getAmpbyInd(const fi: integer; const accessorBW: TBandwidth; const aEventlog: TEventLog = nil): single;
//call with fi + startfi
var
    L: array [0..2] of single;
    ind,i: integer;
begin
    {this version is for normal, positive amplitudes.
    for subtractive amplitudes, ie
    mitigation or weighting, where the calculations are different, use getMitigationAmpbyInd}
    ind := getInd(fi, accessorBW, aEventlog);
    if ind < 0 then
        exit(nan);

    case accessorBW of
        bwOctave: begin
            //add the three bands together
            L[0] := fAmp[ind - 1];
            L[1] := fAmp[ind];
            L[2] := fAmp[ind + 1];
            result := NaN;
            for i := 0 to 2 do
                if not isNaN(L[i]) then
                begin
                    if isnan(result) then
                        result := L[i]
                    else
                        result := dBAdd(result,L[i]);
                end;
        end;
        bwThirdOctave: result := self.fAmp[ind];
    else
        result := nan;
    end;
end;

procedure TSpectrum.setMitigationAmpbyInd(const fi: integer; const Anew: single; const bw: TBandwidth; const aEventlog: TEventLog = nil);
var
    ind: integer;
begin
    if bw = bwThirdOctave then
    begin
        if not checkrange(fi,0,ThirdOctaveBandsLength - 1) then
        begin
            aEventlog.log('Error: setMitigationAmpByInd with index out of range, third octave');
            exit;
        end;

        fAmp[fi] := Anew;
    end
    else
    begin
        //just copy the value to the three bands
        if fi < 0 then exit;

        ind := fi * 3;

        if (ind + 2) >= ThirdOctaveBandsLength then
        begin
            aEventlog.log('Error: setMitigationAmpByInd with index out of range, octave');
            exit;
        end;

        fAmp[ind    ] := Anew;
        fAmp[ind + 1] := Anew;
        fAmp[ind + 2] := Anew;
    end;
end;

function TSpectrum.getMitigationAmpbyInd(const fi: integer; const accessorBW: TBandwidth; const aEventlog: TEventLog = nil): single;
var
    i: integer;
    nonNanBands: integer;
    bandsum, L: single;
begin
    {subtractive amplitudes, i.e. weighting or mitigation}

//        the formula is:
//       - 1/3 oct to oct: attenuation = 10*log10(3/sum(1/10^(0.1*band[i]))), sum over 3 bands}

    if (accessorBW = bwThirdOctave) then
        result := getAmpbyInd(fi,accessorBW,aeventlog)
    else
    begin
        {doing the calc for accessor = octave and DB = 1/3 Oct. Also letting NaNs propagate}
        //hadNaN := false;
        {accessor is in octaves but self is in 1/3 octaves}
        bandsum := 0;
        nonNanBands := 0;
        for i := 0 to 2 do
        begin
            //TODO check that this is indeed getting the correct indices
            L := getAmpbyInd(3*fi + i,bwThirdOctave, aEventlog);
            if not isNaN(L) then
            begin
                bandsum := bandsum + 1/power(10,0.1*L);
                inc(nonNanBands);
            end;
        end;
        if nonNanBands = 0 then
            result := NaN
        else
            result := 10*log10(nonNanBands/bandsum);
    end;
end;

//function TSpectrum.getFbyInd(const fi: integer; bw: TBandwidth): double;
//var
//    ind: integer;
//begin
//    //return nan if index ends up out of range
//    ind := getInd(fi, bw);
//    if ind < 0 then
//        result := nan
//    else
//        result := self.fFreq[ind];
//end;

class function  TSpectrum.getInd(const fi: integer; const bw: TBandwidth; const aEventLog: TEventLog = nil): integer;
begin
    case bw of
        bwOctave :
            result := 3 * fi + 1;
        bwThirdOctave :
            result := fi;
    else
        result := -1;
    end;

    if not checkRange(result, 0, TSpectrum.maxLength - 1) then
    begin
        {$IFDEF DEBUG}
        Raise Exception.Create('Frequency index out of bounds. ' + tostr(fi) + ' ' + tostr
              (result) + ' ' + tostr( 0) + ' ' + tostr( TSpectrum.maxLength - 1 ));
        {$ENDIF}
        if Assigned(aEventLog) then
            aEventlog.log('Frequency index out of bounds. ' + tostr(fi) + ' ' + tostr
                (result) + ' ' + tostr( 0) + ' ' + tostr( TSpectrum.maxLength - 1 ));
        result := -1; //return nonsense index
    end;
end;

class function TSpectrum.getFbyInd(const fi: integer; const bw: TBandwidth; const aEventlog: TEventLog = nil): double;
var
    ind: integer;
begin
    //return nan if index ends up out of range
    ind := TSpectrum.getInd(fi, bw, aEventlog);
    if ind < 0 then
        result := nan
    else
        result := fFreq[ind];
end;

class function TSpectrum.getFStringbyInd(const fi: integer; const bw: TBandwidth; const aEventlog: TEventLog = nil): String;
var
    f: double;
begin
    f := TSpectrum.getFbyInd(fi, bw, aEventLog);
    if isnan(f) then
        exit('');

    if f < 1000 then
        result := tostr(f)
    else
        result := ifthen((fmod(f, 1000) = 0),tostr(saferound(f / 1000)) + 'k',tostr(0.1 * saferound(f / 100)) + 'k');

    // if we are in a country with , as decimal separator, replace it with .
    //result := stringreplace(result, ',', '.', [rfReplaceAll, rfIgnoreCase]);
end;

class function TSpectrum.bandwidthString(const aBW: TBandwidth): string;
begin
    case aBW of
        bwOctave :
            result := 'Octave';
        bwThirdOctave :
            result := 'ThirdOctave';
    end;
end;

class function TSpectrum.bandwidthFromString(const s: string; out bw: TBandwidth): boolean;
begin
    result := true;
    case THelper.caseOfText(s,[TSpectrum.bandwidthString(bwOctave),TSpectrum.bandwidthString(bwThirdOctave)]) of
        0: bw := bwOctave;
        1: bw := bwThirdOctave;
    else
        begin
            bw := bwOctave;
            result := false;
        end;
    end;
end;

function TSpectrum.Level: single;
var
    fi: integer;
    L: single;
begin
    {return the total level for this spectrum (all components).
    NB if all freq components are NaN, then
    NaN is returned.}
    result := NaN;

    for fi := 0 to TSpectrum.maxLength - 1 do
    begin
        L := getAmpbyInd(fi,bwThirdOctave);

        {check if L is NaN. if not, add to result. if result
        is still NaN, then just set result to L.}
        if not isNaN(L) then
        begin
            if isNaN(result) then
                result := L
            else
                result := dBAdd(result, L);
        end;
    end;
end;

function TSpectrum.Level(const startfi, endfi: integer; const accessorBW: TBandwidth): single;
var
    fi: integer;
    L: single;
begin
    {return the level between startfi and endfi for this spectrum.
    NB if all freq components are NaN, then
    NaN is returned.}
//    result := -1000;
//
//    for fi := startfi to endfi do
//        result := dBAdd(result, fAmp[getInd(fi, bw)]);
    result := NaN;

    for fi := startfi to endfi do
    begin
        L := getAmpbyInd(fi,accessorBW);
        {check if L is NaN. if not, add to result. if result
        is still NaN, then just set result to L.}
        if not isNaN(L) then
        begin
            if isNaN(result) then
                result := L
            else
                result := dBAdd(result, L);
        end;
    end;
end;

procedure TSpectrum.clear;
const
    initAmp: single = NaN;
var
    i: integer;
begin
    for i := 0 to length(fAmp) - 1 do
        self.fAmp[i] := initAmp;
end;

procedure TSpectrum.zeroAmp;
const
    initAmp: single = 0;
var
    i: integer;
begin
    for i := 0 to length(fAmp) - 1 do
        self.fAmp[i] := initAmp;
end;


class function  TSpectrum.getLength(const aBW: TBandwidth): integer;
begin
    case aBW of
        bwOctave: result := OctaveBandsLength;
        bwThirdOctave: result := ThirdOctaveBandsLength;
    else
        result := 0;
    end;
end;

class function TSpectrum.convertSpectrum(const inLevs: TSingleDynArray; const fromBW, toBW: TBandWidth): TSingleDynArray;
var
    spect: ISmart<TSpectrum>;
    fi: integer;
begin
    setlength(result,0);
    case fromBW of
        bwOctave: if length(inLevs) <> OctaveBandsLength then exit;
        bwThirdOctave: if length(inLevs) <> thirdOctaveBandsLength then exit;
    end;

    spect := TSmart<TSpectrum>.create(TSpectrum.Create);
    for fI := 0 to length(inLevs) - 1 do
        spect.setAmpbyInd(fi,inLevs[fi],fromBW);

    case toBW of
        bwOctave: setlength(result,OctaveBandsLength);
        bwThirdOctave: setlength(result,thirdOctaveBandsLength);
    end;

    for fi := 0 to length(result) - 1 do
        result[fi] := spect.getAmpbyInd(fi,toBW);
end;


class function TSpectrum.maxLength: integer;
begin
    result := ThirdOctaveBandsLength;
end;

end.
