unit crossSec;

{$B-}
interface

uses system.typinfo, system.rtti, system.sysutils,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, objectCopier, baseTypes, pos3, helperFunctions,
    crossSecEndpoint;

type
{$RTTI INHERIT}
    TCrossSec = class(TDBSeaObject)
    private
        fName      : string;
        fVisible   : boolean;
    public
        endpoints  : array [0..1] of TCrossSecEndpoint;

        constructor Create;
        destructor Destroy; override;
        procedure cloneFrom(const source: TCrossSec);
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;

        function  setPos(const aX1, aY1, aX2, aY2: double): boolean;
        procedure resetEndpoints;
        function  crossSecReady: boolean;

        function  dx: double;
        function  dy: double;

        property  Name: String read fName write fName;
        property  Visible: boolean read fVisible write fVisible;
    end;


implementation

{$B-}

constructor TCrossSec.Create;
begin
    inherited Create;

    self.SaveBlacklist := 'endpoints';

    self.Name := '';
    self.endpoints[0] := TCrossSecEndpoint.Create;
    self.endpoints[1] := TCrossSecEndpoint.Create;
    self.visible := true;
end;

destructor TCrossSec.Destroy;
begin
    endpoints[0].Free;
    endpoints[1].Free;

    inherited;
end;

procedure TCrossSec.cloneFrom(const source: TCrossSec);
begin
    TObjectCopier.CopyObject(source,self);

    endpoints[0].cloneFrom(source.endpoints[0]);
    endpoints[1].cloneFrom(source.endpoints[1]);
end;

procedure TCrossSec.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    inherited;

    if not assigned(parentNode) then exit;

    {cross section end points}
    ChildNode := writeListField(parentNode,'CrossSecEndpoints');
    for i := 0 to length(endpoints) - 1 do
        endpoints[i].writeDataToXMLNode(ChildNode.AddChild('CrossSecEndpoint' + tostr(i)));
end;

procedure TCrossSec.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
    i: integer;
begin
    inherited;
    if not assigned(parentNode) then exit;

    for i := 0 to length(endpoints) - 1 do
    begin
        childNode := (parentNode as IDOMNodeSelect).selectNode('CrossSecEndpoints/CrossSecEndpoint' + tostr(i));
        if assigned(childNode) then
            endpoints[i].readDataFromXMLNode(childNode);
    end;
end;

function  TCrossSec.setPos(const aX1, aY1, aX2, aY2: double): boolean;
begin
    //return true if either point moved. it is expected that the positions given are already checked by the caller
    {$B+} //evaluate both sides of boolean expression
    result := self.endpoints[0].setPos(aX1,aY1) or self.endpoints[1].setPos(aX2,aY2);
    {$B-}
end;

procedure TCrossSec.resetEndpoints;
begin
    endpoints[0].reset;
    endpoints[1].reset;
end;

function  TCrossSec.crossSecReady: boolean;
begin
    result := (endpoints[0].Pos.X <> endpoints[1].Pos.X) or
              (endpoints[0].Pos.Y <> endpoints[1].Pos.Y);
end;

function  TCrossSec.dx: double;
begin
    result := endpoints[1].Pos.x - endpoints[0].Pos.x;
end;

function  TCrossSec.dy: double;
begin
    result := endpoints[1].Pos.y - endpoints[0].Pos.y;
end;

end.
