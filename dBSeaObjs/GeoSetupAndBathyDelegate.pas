unit GeoSetupAndBathyDelegate;

interface

type
    IGeoSetupAndBathyDelegate = interface(IInterface)
      procedure resetCProfile();
      procedure afterBathyOrWorldscaleChange(const scaleBy: double = 1);
      function  getTide: double;
    end;

implementation

end.
