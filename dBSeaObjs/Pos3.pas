unit Pos3;

{$B-}

interface

uses system.generics.collections, dBSeaObject, objectCopier, basetypes;

type

{$RTTI INHERIT}
    TPos3 = Class(TdBSeaObject)
        { class used to represent positions in the problem. note that Z is actually
          a depth so the direction is reversed from normal z axis right hand rule.
          everything is stored in meters internally, so if the user is working in feet,
          all conversion happens just before presenting data to the user. also
          easting and northing aren't stored here, they are only shown when
          displaying info onscreen/getting numbers back from the user }
    private
    var
        fx, fy, fz: double;
    public
        constructor Create(const aX, aY, aZ: double);
        destructor Destroy; Override;
        procedure cloneFrom(const source: TPos3);

        function distanceToTPos3(const Pos2: TPos3): double;

        function setPos(const aX, aY, aZ: double): boolean;
        procedure setPosNoRedraw(const aX, aY, aZ: double);
        function asFPoint3: TFPoint3;
        function asFPoint: TFPoint;
        function equalsPos(const p: TPos3): boolean;

        property x: double read fx write fx;
        property y: double read fy write fy;
        property z: double read fz write fz;
    End;
    TPos3List = class( TObjectList<TPos3>)
    end;
    TPos3ListList = class( TObjectList<TPos3List>)
        procedure connectShapes;
    end;

implementation

constructor TPos3.Create(const aX, aY, aZ : double);
begin
    inherited Create;

    self.SaveBlacklist := '';
    self.setPos(aX, aY, aZ);
end;

destructor TPos3.Destroy;
begin
    inherited;
end;

procedure TPos3.cloneFrom(const source: TPos3);
begin
    TObjectCopier.CopyObject(source,self);
end;

function TPos3.setPos(const aX, aY, aZ: double): boolean;
begin
    //returns true if the position moved
    result := (X <> aX) or (Y <> aY) or (Z <> aZ);
    if result then
        setPosNoRedraw(aX, aY, aZ);
end;


procedure TPos3.setPosNoRedraw(const aX, aY, aZ: double);
begin
    fX := aX;
    fY := aY;
    fZ := aZ;
end;


function TPos3.asFPoint: TFPoint;
begin
    result.x := self.x;
    result.y := self.y;
end;

function  TPos3.asFPoint3 : TFPoint3;
begin
    result.x := self.x;
    result.y := self.y;
    result.z := self.z;
end;

function    TPos3.DistanceToTPos3(const Pos2: TPos3): Double;
//function Distance3D(const Pos1, Pos2: TPos3): Double;
{distance between 2x TPos3 in 3D}
begin
    Result := sqrt(sqr(self.X - Pos2.X) + sqr(self.Y - Pos2.Y) + sqr(self.Z - Pos2.Z));
end;



function TPos3.equalsPos(const p: TPos3): boolean;
begin
    result := (self.fx = p.fx) and (self.fy = p.fy) and (self.fz = p.fz);
end;

{ TPos3ListList }

procedure TPos3ListList.connectShapes;
var
    i,j: integer;
begin
    for i := 0 to self.count - 1 do
        self[i].OwnsObjects := false;

    for i := 0 to self.count - 1 do
    begin
        if self[i].count = 0 then continue;

        for j := i + 1 to self.count - 1 do
        begin
            if self[j].count = 0 then continue;

            if self[j].First.equalsPos(self[i].First) then
            begin
                while self[j].count > 0 do
                    self[i].insert(0,self[j].Extract(self[j].first));
                continue;
            end;

            if self[j].last.equalsPos(self[i].First) then
            begin
                while self[j].count > 0 do
                    self[i].insert(0,self[j].Extract(self[j].last));
                continue;
            end;

            if self[j].first.equalsPos(self[i].last) then
            begin
                self[j].Remove(self[j].First);
                while self[j].count > 0 do
                    self[i].add(self[j].Extract(self[j].first));
                continue;
            end;

            if self[j].last.equalsPos(self[i].last) then
            begin
                self[j].Remove(self[j].last);
                while self[j].count > 0 do
                    self[i].add(self[j].Extract(self[j].last));
                continue;
            end;
        end;
    end;

    for i := self.count - 1 downto 0 do
    begin
        if self[i].count = 0 then
            self.Delete(i)
        else
            self[i].OwnsObjects := true;
    end;
end;

end.
