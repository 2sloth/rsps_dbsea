unit bathymetry;

{$B-}
interface

uses SynCommons,
    system.classes, system.types,
    system.SysUtils, system.math,
    xml.XMLIntf, xml.xmldom,
    vcl.dialogs, vcl.comctrls, vcl.stdctrls, vcl.forms,
    dBSeaObject, objectCopier, basetypes, MCKLib,
    helperFunctions, CSVDelimiters, dBSeaconstants, esriReader,
    eventlogging, GeoSetupAndBathyDelegate,
    MyCompression, MemoryStreamHelper;

type
{$RTTI INHERIT}

    TBathymetry = Class(TdBSeaObject)
    private
    const
        compressionStart = '_BathymetryCompressedBegin_';
        compressionEnd = '_BathymetryCompressedEnd_';
    var
        fZDepths: TSingleMatrix; // 2d raster array representing water depths over a domain
        fZMax   : double; // maximum depth. NB I had it calling matrixmax(zdepths) but this was quite slow for something that gets called a lot
        fBathymetryFile    : String; // filename where we get csv bathymetry data from

        delegate: IGeoSetupAndBathyDelegate;

        procedure BathymetryErrorChecking;
        function getZMax: double;
        function getZDepths: TSingleMatrix;
        procedure setZDepths(const m: TSingleMatrix);
        function getZDepth(j,i: integer): single;
    public
        constructor Create(const aDelegate: IGeoSetupAndBathyDelegate);
        destructor Destroy; Override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TBathymetry);

        procedure init(const aDepth: double = nan);
        procedure BathymetryFromFile(const Filename: TFilename; const lbSolving: TLabel; const pbSolveProgress: TProgressbar);
        procedure BathymetryFromString(const s: String);
        class function  getCompressedBytesFromStream(const stream: TMemoryStream): TBytes;
        procedure readCompressedFromStream(const stream: TMemoryStream);
        procedure writeCompressedToStream(const stream: TMemoryStream);

        function  iMax: integer;
        function  jMax: integer;
        function  ZDepthsLen: integer;
        function  xAspectRatio: double;
        function  yAspectRatio: double;
        procedure updateZMax;
        function  gDepths: TSingleMatrix;
        function  diagonalLength3D(const worldscale: double): double;
        function  diagonalLength2D(const worldscale: double): double;
        procedure reverseDepths;

        property  zDepth[j,i: integer]: single read getZDepth; default;
        property  zDepths: TSingleMatrix read getZDepths write setZDepths;
        property  zMax: Double read getZMax;
        property  bathymetryFile: string read fBathymetryFile;
    End;

implementation

{$B-}
uses dBSeaOptions;

constructor TBathymetry.Create(const aDelegate: IGeoSetupAndBathyDelegate);
begin
    inherited Create;

    self.SaveBlacklist := 'fZDepths,fZMax,delegate';

    self.delegate := aDelegate;

    init;
end;

destructor TBathymetry.Destroy;
begin
    inherited;
end;

procedure TBathymetry.writeCompressedToStream(const stream: TMemoryStream);
begin
    stream.myWrite(TEncoding.Unicode.GetBytes(self.compressionStart));
    stream.myWrite(TMyCompression.compressTSingleDynArrayToBytes(self.fZDepths.toSingleDynArray));
    stream.myWrite(TEncoding.Unicode.GetBytes(self.compressionEnd));
end;

procedure TBathymetry.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;

    if not assigned(parentNode) then exit;
end;

procedure TBathymetry.readDataFromXMLNode(const parentNode: IDOMNode);
var
    Sel: IDOMNodeSelect;
begin
    inherited;

    if not assigned(parentNode) then exit;

    sel := parentNode as IDOMNodeSelect;
end;


procedure TBathymetry.reverseDepths;
var
    i,j: integer;
begin
    for i := 0 to length(fZDepths) - 1 do
        for j := 0 to length(fZDepths[0]) - 1 do
            fZDepths[i,j] := -fZDepths[i,j];
end;

procedure TBathymetry.setZDepths(const m: TSingleMatrix);
begin
    self.fZDepths := m.add(-delegate.getTide);
end;

procedure TBathymetry.cloneFrom(const source: TBathymetry);
begin
    TObjectCopier.CopyObject(source,self);
end;

procedure TBathymetry.init(const aDepth: double = nan);
begin
    {clear all the depths data and start fresh}
    fBathymetryFile := '';
    setlength(fZDepths, 0, 0);
    setlength(fZDepths, 10, 10);
    fZDepths.setAllVal(ifthen(isnan(aDepth) or (aDepth <= 0),defaultZ0, aDepth));
    updateZMax;
end;

procedure TBathymetry.updateZMax;
begin
    {It would be possible to have zmax as a function calling
            matrixmax(zdepths) each time, but this turns out to be too slow for something
            that is called a lot during the solve phase.}
    fZMax := fZDepths.max;
end;

function  TBathymetry.iMax: integer;
begin
    if length(fZDepths) = 0 then
        result := 0
    else
        result := length(fZDepths[0]);
end;

function  TBathymetry.jMax: integer;
begin
    result := length(fZDepths);
end;

function  TBathymetry.gDepths: TSingleMatrix;
{convert bathymetry from world scale to normalised openGL scale, including decimation/downsampling step}
var
    i,j : integer;
    tmp: TSingleMatrix;
    mmax, mmin: double;
    invZClip: double;
begin
    if not assigned(UWAOpts) then Exit;
    if length(fZDepths) = 0 then exit;

    {downsample the matrix, ie reduce the amount of data by the factor 'decimate' in each direction}
    //not sure why, but the prog was occasionally crashing (not in debug mode) if we call
    //UWAOpts.BathymetryDecimate within the matrixDownsample line, so we first grab the integer then use it.
    //this seems to work

    tmp := fZDepths.downsample(UWAOpts.BathymetryDecimate);

    {it is possible that there were just a few spurious 'depth' entries in zdepths, but actually the file needs to
    be height/depth swapped, and we can only check that now that we have downsampled}
    mmax := tmp.max;
    mmin := tmp.min;
    if ((mmax = 0) or (mmax = -0)) and ((mmin = 0) or (mmin = -0)) then
    begin
        showmessage('Input file could not be correctly parsed, please check input format');
    end
    else
    if not (mmax > 0) then
        if (mmin < -0) then
        begin
            //showmessage('Swapping heights and depths for this input file, as no entries with depth > 0 were found');
            reverseDepths;
            updateZMax;
            tmp := fZDepths.downsample(UWAOpts.BathymetryDecimate);
        end;

    tmp := tmp.add(delegate.getTide);
    setlength(result,length(tmp),length(tmp[0]));

    {convert from world to gl scale}
    if tmp.max <= 0 then
        invZClip := 1
    else
        invZClip := 1 / tmp.max;
    for i := 0 to length(result) - 1 do
        for j := 0 to length(result[0]) - 1 do
        begin
            result[i,j] := tmp[i,j]*invZClip;
            if result[i,j] > 1 then
                result[i,j] := 1; //clipping anything that is too deep
        end;
end;

procedure TBathymetry.BathymetryFromFile(const Filename: TFilename; const lbSolving: TLabel; const pbSolveProgress: TProgressbar);
const
    iTestChars: integer = 1000;
var
    s: string;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'BathymetryFromFile');

    {Note: all files are read in as if they were in meters. ESRI data that is in degrees, is converted by TESRIReader.
    If the current units are imperial, then bathymetryErrorChecking will convert depth to meters from feet.}

    fBathymetryFile := filename;
    {get bathymetry from a CSV or ArcGIS ascii grid file. round data to 0.1m }

    {testing for ESRI grid ascii - check for the magic words at the start of the file}
    s := THelper.ReadNBytesOfFileToString(Filename,iTestChars);
    if (SubStringOccurencesi('ncols', s) = 1) and
        (SubStringOccurencesi('nrows', s) = 1) and
        ((SubStringOccurencesi('xllcorner', s) = 1) or (SubStringOccurencesi('xllcenter', s) = 1)) and
        ((SubStringOccurencesi('yllcorner', s) = 1) or (SubStringOccurencesi('yllcenter', s) = 1)) then
    begin
        fZDepths := TESRIReader.readESRIascii(Filename, lbSolving, pbSolveProgress)
    end
    else
    begin
        if CSVDelimitersForm.setFileName(filename) then
            CSVDelimitersForm.showmodal();
        {only show the form if it can't figure out by itself which is the correct delimiter.
        it seems to work ok without needing to be shown,
        and it is more confusing than helpful. if there are still problems with file import,
        the user will have to look at the input file. Do use the form though, to get the
        delimiters.}

        fZDepths.fromDelimitedFile(Filename,CSVDelimitersForm.fieldDelimiter,lbSolving,pbSolveProgress);
        if length(fZDepths) = 0 then
            log.log(sllInfo,'Read bathy from CSV: no data: fZDepths iMax = 0')
        else
            log.log(sllInfo,'Read bathy from CSV: fZDepths iMax = %, jMax = %',[length(fZDepths),length(fZDepths[0])]);
    end;

    BathymetryErrorChecking(); //includes round1ing at this step
end;

procedure TBathymetry.BathymetryErrorChecking;
var
    largerdim: integer;
    i,j: integer;
    //bInputReadErrorFlag: boolean;
    zMin: double;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'BathymetryErrorChecking');

    {guard functions}
    if not length(fZDepths) > 0 then
    begin
        fZMax := 0;
        Exit();
    end;

    if not length(fZDepths[0]) > 0 then
    begin
        fZMax := 0;
        Exit();
    end;

    //bInputReadErrorFlag := false;

    {check for any NaNs (but keep them, as this means unsurveyed land), and do conversion ft to m if needed}
    for i := 0 to length(fZDepths) - 1 do
        for j := 0 to length(fZDepths[0]) - 1 do
            if not isNaN(fZDepths[i,j]) then
                fZDepths[i,j] := round1(f2m(fZDepths[i,j],UWAOpts.DispFeet)); //NB round1ing!

    //if bInputReadErrorFlag then
    //    showmessage('Some data may have been incorrectly parsed, check for inconsistencies');

    {check if we need to decimate the data - which is done later, after we set the decimation factor here
    On my test machine, around about 400x400 bathymetry 'pixels' causes the graphics to slow, so we
    reduce the data when it gets over this amount.
    TODO maybe add the ability to tweak maxGLBathyPixels, and also to save it to
    registry so that each particular machine can tweak to suit}
    largerDim := ZDepthsLen;

    UWAOpts.bathymetryDecimate := safeceil(largerdim/UWAOpts.maxGLBathyPixels);

    updateZMax();
    if (zMax <= 0) and (fZDepths.min < 0) then
    begin
        //showmessage('Swapping heights and depths for this input file, as no entries with depth > 0 were found');
        reverseDepths;
        updateZMax();
    end;

//    //Testing coping with nan
//    for i := 0 to length(ZDepths) - 1 do
//        for j := 0 to length(ZDepths[0]) - 1 do
//            if RandomRange(0,1000) < 10 then
//                ZDepths[i,j] := nan;

    log.Log(sllInfo,'Checked imported bathymetry. zmax = %',[self.zMax]);

    //set land scale different from bathy scale
    if assigned(UWAOpts) then
    begin
        zMin := fZDepths.min;
        if (zMax > 0) and (zMin < 0) then
        begin
            zMin := -zMin;
            UWAOpts.zLandFactor := UWAOpts.ZFactor * 0.5 * zMax / zMin;
        end;
    end;

    delegate.resetCProfile();
    delegate.afterBathyOrWorldscaleChange(1.0); {theRA, set it to the entire area}
end;

class function  TBathymetry.getCompressedBytesFromStream(const stream: TMemoryStream): TBytes;
var
    aPos1,aPos2: int64;
begin
    aPos1 := THelper.BytePos(TEncoding.Unicode.GetBytes(compressionStart), stream.Memory, stream.Size, 0);
    aPos1 := aPos1 + length(compressionStart)*sizeof(Char);
    aPos2 := THelper.BytePos(TEncoding.Unicode.GetBytes(compressionEnd), stream.Memory, stream.Size, 0);
    if aPos2 <= aPos1 then exit;
    result := stream.getBytes(aPos1, aPos2);
end;

function TBathymetry.getZDepth(j, i: integer): single;
begin
    result := self.fZDepths[j,i] + delegate.getTide;
end;

function TBathymetry.getZDepths: TSingleMatrix;
begin
    if delegate.getTide = 0 then
        result := fZDepths
    else
        result := self.fZDepths.add(delegate.getTide);
end;

function TBathymetry.getZMax: double;
begin
    result := fZMax + delegate.getTide;
end;

procedure TBathymetry.readCompressedFromStream(const stream: TMemoryStream);
begin
    self.fZDepths.fromSingleDynArray(TMyCompression.decompressTSingleDynArrayFromTBytes(self.getCompressedBytesFromStream(stream)));
    if length(self.fZDepths) = 0 then
        setlength(fZDepths, 1, 1);
end;

procedure TBathymetry.BathymetryFromString(const s: String);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'BathymetryFromString');

    if length(s) = 0 then exit;

    fZDepths.fromDelimitedString(s,',',true);
    if length(fZDepths) = 0 then
        log.log(sllInfo,'Read bathy from string: no data: fZDepths iMax = 0')
    else
        log.log(sllInfo,'Read bathy from string: fZDepths iMax = %, jMax = %',[length(fZDepths),length(fZDepths[0])]);

    BathymetryErrorChecking();
end;


function  TBathymetry.ZDepthsLen: integer;
{get the length of the larger of the 2 dimensions of the depths matrix}
begin
    if length(fZDepths) = 0 then
        result := 0
    else
        result := larger(iMax,jMax);
end;

function  TBathymetry.diagonalLength3D(const worldscale: double): double;
var
    xmax, ymax: double;
begin
    if self.ZDepthsLen = 0 then exit(1);
    xmax := self.xAspectRatio() * worldscale;
    ymax := self.yAspectRatio() * worldscale;
    result := sqrt( sqr(xMax) + sqr(yMax) + sqr(zmax) );
end;

function  TBathymetry.diagonalLength2D(const worldscale: double): double;
var
    xmax, ymax: double;
begin
    if self.ZDepthsLen = 0 then exit(1);
    xmax := self.xAspectRatio() * worldscale;
    ymax := self.yAspectRatio() * worldscale;
    result := sqrt( sqr(xMax) + sqr(yMax));
end;

function  TBathymetry.xAspectRatio: double;
begin
    if ZDepthsLen = 0 then exit(1);
    result := iMax / ZDepthsLen;
end;

function  TBathymetry.yAspectRatio: double;
begin
    if ZDepthsLen = 0 then exit(1);
    result := jMax / ZDepthsLen;
end;

end.
