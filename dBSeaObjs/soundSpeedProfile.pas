unit soundSpeedProfile;

{$B-}
interface

uses system.math, system.sysutils, system.types, system.json,
    generics.collections, generics.defaults,
    vcl.graphics, vcl.forms,
    xml.XMLIntf, xml.xmldom,
    dBSeaObject, basetypes, helperFunctions,
    objectCopier, mcklib, nullable, SmartPointer;


type
    TSSP = class(TDBSeaObject)
    public
        cProfile: TMatrix; // sound speed with depth variation
        name: string;
        comments: string;

        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TSSP);

        procedure reset(const zMax: double);
        function  getCProfileAtAngle(const aAngle, currentVelocity, currentDirection: double): TMatrix;
        function  getCProfileAtAngle2(const aAngle, currentVelocity, currentDirection: double): TDepthValArray;
        function  getCProfile: TDepthValArray;
        class function  removeDuplicateDepths(const ssp: TMatrix): TMatrix;
        class function  removeDuplicateDepths2(const ssp: TDepthValArray): TDepthValArray;

        procedure setWithListAndWeightings(const arr: TArray<TSSP>; const weightings: TDoubleDynArray);
        class function createFromJSON(v: TJSONValue): TSSP;
    end;

implementation

uses solversGeneralFunctions;

procedure TSSP.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    childNode := (parentNode as IDOMNodeSelect).selectNode('cProfile');
    if assigned(childNode) then
        cprofile.deserialise(childNode.firstChild.nodeValue);
end;

class function TSSP.removeDuplicateDepths(const ssp: TMatrix): TMatrix;
var
    i: integer;
    prevDepth: double;
    q: TMatrix;
begin
    result := [];
    if length(ssp) = 0 then exit;

    q := ssp.rotate90;

    prevDepth := nan;
    for I := 0 to length(q) - 1 do
    begin
        if q[i,0] <> prevDepth then
        begin
            setlength(result,length(result) + 1, 2);
            result[length(result) - 1,0] := q[i,0];
            result[length(result) - 1,1] := q[i,1];
            prevDepth := q[i,0];
        end;
    end;

    result := result.rotate90().rotate90().rotate90();    //todo make this not suck
end;

class function TSSP.removeDuplicateDepths2(const ssp: TDepthValArray): TDepthValArray;
var
    i: integer;
    prevDepth: Nullable<double>;
begin
    result := [];
    if length(ssp) = 0 then exit;

    for I := 0 to length(ssp) - 1 do
    begin
        if not prevDepth.HasValue or (ssp[i].depth <> prevDepth) then
        begin
            setlength(result,length(result) + 1);
            result[length(result) - 1] := ssp[i];
            prevDepth := ssp[i].depth;
        end;
    end;
end;

procedure TSSP.cloneFrom(const source: TSSP);
begin
    TObjectCopier.CopyObject(source,self);

    cProfile := source.CProfile.copy;
end;


procedure TSSP.reset(const zMax: double);
begin
    {create flat SSP at 1500 m/s}
    setlength(cProfile, 2, 2);
    cProfile[0, 0] := 0;
    cProfile[0, 1] := zMax;
    cProfile[1, 0] := 1500.0;
    cProfile[1, 1] := 1500.0;
    name := 'Default';
    comments := '';
end;

procedure TSSP.setWithListAndWeightings(const arr: TArray<TSSP>; const weightings: TDoubleDynArray);
var
    i,j: integer;
    depthVals: array of TDepthValArray;
    allDepths: TDoubleList;
begin
    if length(arr) <> length(weightings) then
        raise Exception.Create(format('Can''t set SSP properties, array lengths don''t match: %d %d',[length(arr), length(weightings)]));

    //get all the depths in all input SSPs
    allDepths := {TSmart<TDoubleList>.create(}TDoubleList.create(true);
    setlength(depthVals,length(arr));
    for i := 0 to length(arr) - 1 do
    begin
        depthVals[i] := arr[i].getCProfile;
        for j := 0 to length(depthVals[i]) - 1 do
            allDepths.add(TWrappedDouble.create(depthVals[i][j].depth));
    end;

    //sort depths (min 0.1 m spacing)
    allDepths.Sort(IComparer<TWrappedDouble>(TDelegatedComparer<TWrappedDouble>.create(function(const Left, Right: TWrappedDouble): Integer begin
        Result := round((Left.d - Right.d) * 10);
    end)));

    //remove duplicate depths (min 0.1 m spacing)
    for j := allDepths.count - 1 downto 1 do
        if abs(allDepths[j].d - allDepths[j - 1].d) < 0.1 then
            allDepths.Delete(j);

    // set depths in output
    setlength(self.cProfile,0);
    setlength(self.cProfile,2,allDepths.count);
    for j := 0 to allDepths.count - 1 do
        self.cProfile[0,j] := allDepths[j].d;

    //add weighted profiles at each output depth
    for i := 0 to length(arr) - 1 do
        for j := 0 to allDepths.count - 1 do
            self.cProfile[1,j] := self.cProfile[1,j] + weightings[i] * TSolversGeneralFunctions.interpolationFunPE(depthVals[i],allDepths[j].d);

    allDepths.free;
end;

class function TSSP.createFromJSON(v: TJSONValue): TSSP;
var
    o, oCProfile: TJSONObject;
    depthArr, cArr: TJSONArray;
    i: integer;
    d,c: double;
begin
    try
        o := TJSONObject(v);

        result := TSSP.create();

        if not assigned(o.getValue('cProfile')) then
            raise EJSONParsing.Create('SSP cProfile field not found');

        oCProfile := o.getValue('cProfile') as TJSONObject;

        if not assigned(oCProfile.getValue('depth')) then
            raise EJSONParsing.Create('SSP cProfile depth field not found');
        if not assigned(oCProfile.getValue('c')) then
            raise EJSONParsing.Create('SSP cProfile c field not found');

        depthArr := oCProfile.getValue('depth') as TJSONArray;
        cArr := oCProfile.getValue('c') as TJSONArray;

        if depthArr.Count <> cArr.Count then
            raise EJSONParsing.Create('Depth and speed array lengths must match');

        if depthArr.Count = 0 then
            raise EJSONParsing.Create('Depth and speed array lengths cannot be 0');

        setlength(result.cProfile, 2, depthArr.Count);
        for i := 0 to depthArr.count - 1 do
        begin
            if not TryStrToFloat(depthArr.items[i].ToString, d) then
                raise EJSONParsing.Create('Cannot parse depth value');

            if not TryStrToFloat(cArr.items[i].ToString, c) then
                raise EJSONParsing.Create('Cannot parse speed value');

            if (i = 0) and (d <> 0)  then
                raise EJSONParsing.Create('First SSP depth must be 0');
            if (i > 0) and (d <= result.cProfile[0,i-1]) then
                raise EJSONParsing.Create('SSP depths must be in increasing order');
            if (d < 0) then
                raise EJSONParsing.Create('SSP depths must be positive');
            if (c <= 0) then
                raise EJSONParsing.Create('SSP speeds must be positive');

            result.cProfile[0,i] := d;
            result.cProfile[1,i] := c;
        end;

        if assigned(o.getValue('name')) then
            o.getValue('name').TryGetValue<string>(result.name);
        if assigned(o.getValue('comments')) then
            o.getValue('comments').TryGetValue<string>(result.comments);

    except on e: Exception do
        exit(nil);
    end;
end;

function  TSSP.getCProfileAtAngle(const aAngle, currentVelocity, currentDirection: double): TMatrix;
var
    i: integer;
begin
    result := cProfile.copy;
    for i := 0 to length(result[0]) - 1 do
        result[1,i] := result[1,i] - currentVelocity*cos(currentDirection - aAngle);
end;

function  TSSP.getCProfileAtAngle2(const aAngle, currentVelocity, currentDirection: double): TDepthValArray;
var
    i: integer;
begin
    if length(cProfile) = 0 then
    begin
        setlength(result,0);
        exit;
    end;

    setlength(result, length(cProfile[0]));
    for i := 0 to length(result) - 1 do
    begin
        result[i].depth := cProfile[0,i];
        result[i].val := cProfile[1,i] - currentVelocity*cos(currentDirection - aAngle);
    end;
end;

function  TSSP.getCProfile(): TDepthValArray;
var
    i: integer;
begin
    if length(cProfile) = 0 then
    begin
        setlength(result,0);
        exit;
    end;

    setlength(result, length(cProfile[0]));
    for i := 0 to length(result) - 1 do
    begin
        result[i].depth := cProfile[0,i];
        result[i].val := cProfile[1,i];
    end;
end;




end.
