unit dBSeaOptions;

{$B-}

interface

uses syncommons,
    system.sysutils, system.math, system.rtti, system.types,
    winapi.windows,
    vcl.graphics,
    xml.XMLIntf, xml.xmldom,
    generics.Collections,
    eventlogging, dBSeaObject, helperFunctions, baseTypes,
    dBSeaColours, dBSeaconstants,
    levelLimits, levelConverter, mycolor, mcklib;

type

{$RTTI INHERIT}
    TdBSeaOptions = class(TdBSeaObject, ILevelLimitsDelegate)
        { class of openGL and other visual options. things that don't affect the physical problem -
          no affect on results, just the way they are displayed. this is all saved in the save file.
          Note some colours and opacities are stored with the individual objects, and can be set singly. }
    private
    const
        TargetResultsColorMapLength = 2048; //if this is too low, then there is visible stepping in colours in the GL display
    var
        fResultsColors         : TMyColorList; // list containing the colormap colours for results display (can be any length)
        fResultsShadowColor    : TMyColor;
        fLandColor, fBathymetryColor, fWaterSurfaceColor: TColor;
        fExclusionZoneColor    : TColor;
        fBackgroundColor       : TColor;
        fGLTextColor, fGLXSecTextColor, fGL3DBorderTextColor: TColor;

        fbUserSetColors        : boolean;
        fZFactor               : double; // z depth display factor - used for underwater range, and also all non land items
        fZLandFactor           : double; // z depth display factor, exclusively for Land

        fWaterSurfaceAlpha, fxsecWaterSurfaceAlpha, fBathymetryAlpha: double; // opacity of water and bathymetry
        fResultsAlpha, fResultsContoursAlpha: double;
        fResultsContoursDarkening: double;
        fResultsContoursWidth  : integer;

        fDisplayZInd           : integer; // index in z of results layer to display, if in single layer mode
        fBathymetryDecimate    : integer; // we use this when we wish to downsample bathymetry data if it is too large (ie slowing down graphics display)
        fBathymetryColorMapLength: integer;
        fResultsColorMapLength : integer;
        fMaxGLBathyPixels      : integer; //the maximum number of gl bathymetry input pixels we want (controls bathymetry downsampling)

        fResultsTris           : boolean; // whether to use meandering triangles or maching squares for contour plots
        fResultsColormapSmooth : boolean; // whether to interpolate between result colours in the resultcolourmap
        fLevelsDisplay         : TLevelsDisplay; //whether to show max projected level, average or just 1 layer at a time

        fGLShowLandinXSec      : boolean;
        fGLUseTopoLandColors   : boolean;
        fbHideProbesOnExport   : boolean;
        fDispFeet              : boolean; // whether we are using feet or meters
        fFullXSecDepth         : boolean; //whether to export the full xsec depth, or just up to the max depth in that xsec
        fXSecTruncateAtWaterSurface: boolean; //whether to chop off anything above the water level in xsec
        fComponentVisibility   : TBooleanDynArray; // boolean list of what is currently wisible in window

        fSourceSize            : double;
        fProbeSize             : double;
        fSourceLineWidth       : integer;

        fAutosaveAfterSolve    : boolean;
        fmaxProcessorsForSolve : integer;

        fExportLogoFile        : TFilename;
        fExportFreeText        : String;
        fExportDelimiter       : char;

        { stores the commatext specifying our custom colours. gets copied to the customcolors.commatext
          property of any colordialog we create, then copied back after the dialog is closed }
        fCustomColors          : String;

        fUTMZone               : integer;

        function  getMmaxProcessorsForSolve: integer;

        procedure setGLShowLandInXSec(const val: boolean);
        procedure setResultsColormapSmooth(const val: boolean);
        procedure setXSecTruncateAtWaterSurface(const val: boolean);

        procedure setBackgroundColor(const aCol: TColor);
        procedure setLandColor(const aCol: TColor);
        procedure setBathymetryColor(const aCol: TColor);
        procedure setExclusionZoneColor(const aCol: TColor);

        procedure setResultsTris(const val: boolean);

        procedure setResultsAlpha(const aAlpha: double);
        procedure setResultsContoursAlpha(const aAlpha: double);
        procedure setZFactor(const aZFactor: double);
        procedure setZLandFactor(const aZLandFactor: double);
        procedure setBathymetryDecimate(const val: integer);
        //procedure setZClip(const val: double);
        procedure setDispFeet(const val: boolean);
        procedure setFullXSecDepth(const val: boolean);

        //delegate methods
        function  getLevelsDisplay: TLevelsDisplay;
        function  getDisplayZInd: integer;

    public
    const
        MaxSourceSize = 0.05;
        MaxProbeSize = 0.05;
    var
        levelLimits: TLevelLimits;
        NaNColor: TColor;
        canSelectProbes, canSelectSources, exclusionZoneStopAtLand: boolean;

        constructor Create;
        destructor Destroy; override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode; const sSaveVersion: string); reintroduce;

        function  bathymetryColormap: TMatrix;
        function  landColormap: TMatrix;

        procedure updateCMaxCMinandLevelColors();
        procedure updateResultsLevelColors;
        function  resultsColorMap: TMatrix;
        function  getResultsColorsSteps: integer;
        function  getLevelContours: TDoubleDynArray;
        function  getLevelContour(const ind: integer): double;
        function  resultsColorsRec(const ind: integer): TMyColorRec;
        function  resultsColorsCount: integer;
        procedure resultsColorsClear;
        procedure resultsColorsAdd(const aColor: TColor; const aVisible: boolean);

        //procedure DeleteUserSetLimits;
        function  getDepthViewInd: Integer;
        procedure setVisibility(const ind: integer; const val: boolean);
        procedure setLevelsDisplay(const val: TLevelsDisplay; const dmax: integer);
        function  setMaxGLBathyPixels(const val: integer; const zDepthsLen: integer): boolean;
        procedure setDisplayZInd(const val: integer; const dmax: integer);

        //------------------------------------------------------------------

        property  bUserSetColors: boolean read fbUserSetColors write fbUserSetColors;
        property  zFactor: double read fZFactor write setZFactor;
        property  zLandFactor: double read fZLandFactor write setZLandFactor;
        property  displayZInd: integer read fDisplayZInd;
        property  levelsDisplay: TLevelsDisplay read fLevelsDisplay;
        property  bathymetryDecimate: integer read fBathymetryDecimate write setBathymetryDecimate;
        property  maxGLBathyPixels: integer read fMaxGLBathyPixels;
        property  resultsColormapSmooth: boolean read fResultsColormapSmooth write setResultsColormapSmooth;
        property  resultsTris: boolean read fResultsTris write setResultsTris;
        property  GLShowLandinXSec: boolean read fGLShowLandinXSec write setGLShowLandInXSec;
        property  GLUseTopoLandColors: boolean read fGLUseTopoLandColors write fGLUseTopoLandColors;
        property  bHideProbesOnExport: boolean read fbHideProbesOnExport write fbHideProbesOnExport;
        property  dispFeet: boolean read fDispFeet write setDispFeet;
        property  fullXSecDepth: boolean read fFullXSecDepth write setFullXSecDepth;
        property  xSecTruncateAtWaterSurface: boolean read fXSecTruncateAtWaterSurface write setXSecTruncateAtWaterSurface;

        property  resultsShadowColor: TMyColor read fResultsShadowColor write fResultsShadowColor;
        property  landColor: TColor read fLandColor write setLandColor;
        property  waterColor: TColor read fBathymetryColor write setBathymetryColor;
        property  waterSurfaceColor: TColor read fWaterSurfaceColor;
        property  backgroundColor: TColor read fBackgroundColor write setBackgroundColor;
        property  exclusionZoneColor: TColor read fExclusionZoneColor write setExclusionZoneColor;
        property  GLTextColor: TColor read fGLTextColor;
        property  GLXSecTextColor: TColor read fGLXSecTextColor;
        property  GL3DBorderTextColor: TColor read fGL3DBorderTextColor write fGL3DBorderTextColor;
        property  customColors: String read fCustomColors write fCustomColors;

        property  sourceSize: double read fSourceSize write fSourceSize;
        property  probeSize: double read fProbeSize write fProbeSize;
        property  sourceLineWidth: integer read fSourceLineWidth write fSourceLineWidth;

        property  exportLogoFile: TFilename read fExportLogoFile write fExportLogoFile;
        property  exportFreeText: String read fExportFreeText write fExportFreeText;

        property  xsecWaterSurfaceAlpha: Double read fxsecWaterSurfaceAlpha;
        property  waterSurfaceAlpha: Double read fWaterSurfaceAlpha;
        property  bathymetryAlpha: Double read fBathymetryAlpha;
        property  resultsAlpha: Double read fResultsAlpha write setResultsAlpha;
        property  resultsContoursAlpha: Double read fResultsContoursAlpha write setResultsContoursAlpha;
        property  resultsContoursDarkening: double read fResultsContoursDarkening write fResultsContoursDarkening;
        property  resultsContoursWidth: integer read fResultsContoursWidth write fResultsContoursWidth;
        property  componentVisibility: TBooleanDynArray read fComponentVisibility;

        property  bathymetryColorMapLength: integer read fBathymetryColorMapLength write fBathymetryColorMapLength;
        property  resultsColorMapLength: integer read fResultsColorMapLength write fResultsColorMapLength;
        property  autosaveAfterSolve: boolean read fAutosaveAfterSolve write fAutosaveAfterSolve;
        property  maxProcessorsForSolve: integer read getMmaxProcessorsForSolve write fmaxProcessorsForSolve;
        property  exportDelimiter: char read fExportDelimiter write fExportDelimiter;
        property  depthViewInd: integer read getDepthViewInd;
        property  UTMZone: integer read fUTMZone write fUTMZone;
    const
        ZFactMax: double = 1.0;
    end;

var
    UWAOpts: TdBSeaOptions;

implementation

constructor TdBSeaOptions.Create;
var
    i: integer;
begin
    inherited;

    self.SaveBlacklist := 'fResultsColors,fResultsShadowColor,levelLimits';

    {top and bottom color were previously used when we had a shaded background. now only flat colour is used}
    fBackgroundColor := TDBSeaColours.darkBlue;
    fLandColor := TDBSeaColours.defaultLand;
    fBathymetryColor := TDBSeaColours.midBlue;
    fWaterSurfacecolor := TDBSeaColours.midBlue;
    fGLTextColor := TDBSeaColours.GLtext;  //text displayed in gl with glPrint
    fGLXSecTextColor := TDBSeaColours.GLXSectext; //text display with glPrint, in cross section
    fGL3DBorderTextColor  := TDBSeaColours.GL3DBorderText;
    fExclusionZoneColor := TDBSeaColours.ExclusionZone;
    NaNColor := TDBSeaColours.white(0.7);
    fsourceSize  := 0.013; //default GL size to draw Sources
    fprobeSize   := 0.010; //default GL size to draw Probes
    fSourceLineWidth := 4;

    fWaterSurfaceAlpha := 0.075; // transparency of water surface
    fXSecWaterSurfaceAlpha := 0.3; // transparency of water surface in XSec view
    fBathymetryAlpha := 1.0; // transparency of bathymetry
    fResultsAlpha := 0.6; //default, as per Andy's request. this gets saved to registry though
    fResultsContoursAlpha := 1.0;
    fResultsContoursDarkening := 0.5;
    fResultsContoursWidth := 2;
    fBathymetryDecimate := 1;
    fMaxGLBathyPixels := 800;

    fGLShowLandinXSec := true;
    fXSecTruncateAtWaterSurface := true;
    fbHideProbesOnExport := true;
    fGLUseTopoLandColors := true;
    fExportDelimiter := ',';

    fbathymetryColorMapLength := 2048;

    fResultsTris := false; //use squares not triangles to draw levels as default
    fResultsColormapSmooth := false; //false means use marching squares to draw levels (colours not blended)
    fResultsColors := TMyColorList.Create(true);
    TDBSeaColours.resetResultsColorList(fResultsColors,-1,0); //set default colors
    fbUserSetColors := false;
    fResultsShadowColor := TMyColor.CreateWithColor(TDBSeaColours.defaultResultsShadowZone(),true);
    fResultsShadowColor.Visible := true;

    fmaxProcessorsForSolve := CPUCount - 1;
    fAutosaveAfterSolve  := true;
    fFullXSecDepth := false;

    fResultsColorMapLength := TargetResultsColorMapLength;
    {set some random colours to start with - for our customcolours that show up when using the colordialog}
    fcustomColors := TDBSeaColours.defaultCustomColorsString;

    fDisplayZInd := 0; // default z index sectors result to display
    fLevelsDisplay := ldMax; //whether to show projected, ALL z layers of results or just 1 at a time

    levelLimits := TLevelLimits.Create(self);
    levelLimits.delegateOrNil := self;

    fZFactor := -30 * ZFactMax / 100;
    fZLandFactor := -30 * ZFactMax / 100;

    {the individual visibility indices are listed in the enum}
    setlength(fComponentVisibility,integer(high(TVisibilityEnum)) + 1);
    for i := 0 to length(fComponentVisibility) - 1 do
        fComponentVisibility[i] := true;
    fComponentVisibility[ord(veAxes)] := false; //don't currently show axes
    fComponentVisibility[ord(veEndpoints)] := false; //don't currently show cross sec endpoints
    fComponentVisibility[ord(veRAs)] := false; //don't show RA
    fComponentVisibility[ord(veContours)] := false; //don't show lines between contours
    fComponentVisibility[ord(veScaleBar)] := false;
    fComponentVisibility[ord(veGrid)] := false;
    fComponentVisibility[ord(veRays)] := false;
    canSelectProbes := true;
    canSelectSources := true;
    exclusionZoneStopAtLand := true;

    fDispFeet := false;
    fUTMZone := -1;
end;

destructor TdBSeaOptions.Destroy;
begin
    fResultsColors.Free;
    fResultsShadowColor.Free;
    levelLimits.Free;

    inherited;
end;

procedure TdBSeaOptions.writeDataToXMLNode(const parentNode: IXMLNode);
var
    childNode: IXMLNode;
    i: integer;
begin
    inherited;
    if not assigned(parentNode) then exit;

    writeIntegerField(parentNode,'ResultsColorsCount',resultsColorsCount);
    ChildNode := writeListField(parentNode,'ResultsColorsList');
    for i := 0 to resultsColorsCount - 1 do
        self.fResultsColors[i].writeDataToXMLNode(ChildNode.AddChild('MyColor' + tostr(i)));

    levelLimits.writeDataToXMLNode(parentNode.AddChild('LevelLimits'));
end;

procedure TdBSeaOptions.readDataFromXMLNode(const parentNode: IDOMNode; const sSaveVersion: string);
var
    childNode, grandChildNode: IDOMNode;
    sel: IDOMNodeSelect;
    i: integer;
    log: ISynlog;
begin
    inherited readDataFromXMLNode(parentNode);

    //fix for opening files from older versions
    if length(fComponentVisibility) <> (integer(high(TVisibilityEnum)) + 1) then
        setlength(fComponentVisibility,integer(high(TVisibilityEnum)) + 1);

    log := TSynLogLevs.Enter(self,'readDataFromXMLNode');

    if not assigned(parentNode) then exit;

    sel := parentNode as IDOMNodeSelect;

    childnode := sel.selectNode('LevelLimits');
    if assigned(childNode) then
        levelLimits.readDataFromXMLNode(childNode);

    resultsColorsClear;
    childNode := Sel.selectNode('ResultsColorsList');
    if assigned(childNode) then
        for i := 0 to childNode.childNodes.Length - 1 do
        begin
            GrandChildNode := childNode.childNodes[i];
            if assigned(GrandChildNode) then
            begin
                self.fResultsColors.add(TMyColor.create);
                self.fResultsColors.last.readDataFromXMLNode(grandChildNode);
            end;
        end;
end;

function TdBSeaOptions.bathymetryColorMap: TMatrix;
{create multiarray containing a linear gradient of 3 doubles defining colours for the bathymetry. length should
be quite long to accommodate possible depths with can be km or more}
var
    i: integer;
    aCol: TColor;
begin
    if BathymetryColorMapLength < 1 then exit;

    setlength(result,BathymetryColorMapLength,3);
    for i := 0 to length(result) - 1 do
    begin
        //result[i, 0] := getRValue(colWater) / 255 * (1 - i / BathymetryColorMapLength);
        //result[i, 1] := getGValue(colWater) / 255 * (1 - i / BathymetryColorMapLength);
        //result[i, 2] := getBValue(colWater) / 255 * (1 - i / BathymetryColorMapLength); // water - to black
        aCol := THelper.TColorMix(waterColor,clBlack,i/(BathymetryColorMapLength - 1));
        result[i, 0] := getRValue(aCol) / 255;
        result[i, 1] := getGValue(aCol) / 255;
        result[i, 2] := getBValue(aCol) / 255; // water - to black
    end;
end;

function TdBSeaOptions.landColorMap: TMatrix;
{create multiarray containing a linear gradient of 3 doubles defining colours for the bathymetry. length should
be quite long to accommodate possible depths with can be km or more}
var
    i: integer;
    aCol: TColor;

    LandColorsArray: TColorArray;
    entriesPerColor, j: integer;
    Color1, Color2: TColor;
    theColor       : TColor;
begin
    setlength(result,bathymetryColorMapLength,3);

    if GLUseTopoLandColors then
    begin
        //nice topo colour scheme
        TDBSeaColours.defaultTopoLandColors(LandColorsArray);
        entriesPerColor := bathymetryColorMapLength div (length(LandColorsArray) - 1);
        for i := 0 to length(LandColorsArray) - 2 do
        begin
            Color1 := LandColorsArray[i];
            Color2 := LandColorsArray[i + 1];
            for j := 0 to entriesPerColor - 1 do
            begin
                theColor := THelper.TColorMix(Color1,Color2,j/(entriesPerColor - 1));
                result[i * entriesPerColor + j, 0] := getRValue(theColor)/255;
                result[i * entriesPerColor + j, 1] := getGValue(theColor)/255;
                result[i * entriesPerColor + j, 2] := getBValue(theColor)/255;
            end;
        end;
        Color2 := LandColorsArray[high(LandColorsArray)];
        result[bathymetryColorMapLength - 1, 0] := getRValue(Color2) / 255;
        result[bathymetryColorMapLength - 1, 1] := getGValue(Color2) / 255;
        result[bathymetryColorMapLength - 1, 2] := getBValue(Color2) / 255;
    end
    else
    begin
        for i := 0 to length(result) - 1 do
        begin
    //        result[i, 0] := getRValue(colLand) / 255 * (1 - i / iColormapLength);
    //        result[i, 1] := getGValue(colLand) / 255 * (1 - i / iColormapLength);
    //        result[i, 2] := getBValue(colLand) / 255 * (1 - i / iColormapLength); // land - to black
            aCol := THelper.TColorMix(LandColor,clBlack,i/(BathymetryColorMapLength - 1));
            result[i, 0] := getRValue(aCol) / 255;
            result[i, 1] := getGValue(aCol) / 255;
            result[i, 2] := getBValue(aCol) / 255; // water - to black
        end;
    end;
end;

function  TdBSeaOptions.getMmaxProcessorsForSolve: integer;
begin
    //shouldn't try using more processors than exist
    fmaxProcessorsForSolve := min(CPUCount, fmaxProcessorsForSolve);
    result := fmaxProcessorsForSolve;
end;

procedure TdBSeaOptions.setBackgroundColor(const aCol: TColor);
begin
    fBackgroundColor := aCol;
end;

procedure TdBSeaOptions.setLandColor(const aCol: TColor);
begin
    if LandColor <> aCol then
    begin
        fLandColor := aCol;
        GLUseTopoLandColors := false;
    end;
end;

procedure TdBSeaOptions.setBathymetryColor(const aCol: TColor);
begin
    fBathymetryColor := aCol;
end;

procedure TdBSeaOptions.setExclusionZoneColor(const aCol: TColor);
begin
    fExclusionZoneColor := aCol;
end;

procedure TdBSeaOptions.setResultsAlpha(const aAlpha: double);
begin
    fResultsAlpha := aAlpha;
end;

procedure TdBSeaOptions.setResultsContoursAlpha(const aAlpha: double);
begin
    fResultsContoursAlpha := aAlpha;
end;

procedure TdBSeaOptions.setZFactor(const aZFactor: double);
begin
    fZFactor := aZFactor;
end;

procedure TdBSeaOptions.setZLandFactor(const aZLandFactor: double);
begin
    fZLandFactor := aZLandFactor;
end;

function TdBSeaOptions.resultsColorMap: TMatrix;
{create multiarray containing linear gradients of 3 doubles defining colours for results display. it can
be smooth (ie linear gradient) or not smoothed, in which case we hold each colour constant
until the next colour is reached. the colours are given in a TObjectList, and can therefore
be of any length >= 2. note that entriesPerColor is different for the 2 cases (smooth or not)}
var
    entriesPerColor, i, j: integer;
    iColorCount: integer;
    color1, color2: TColor;
    myCol: TMyColorRec;
begin
    iColorCount := ResultsColorsCount;
    if iColorCount < 1 then
    begin
        setlength(result,TargetResultsColorMapLength,3);
        exit;
    end;

    {make sure the colormaplength can be divided evenly into the separate colours. note that
    this differs between smooth or not colormap}
    if resultsColormapSmooth then
    begin
        if iColorCount = 1 then
            resultsColorMapLength := TargetResultsColorMapLength
        else
            resultsColorMapLength := (iColorCount - 1) * safefloor(TargetResultsColorMapLength/(iColorCount - 1))
    end
    else
        resultsColorMapLength := (iColorCount) * safefloor(TargetResultsColorMapLength/(iColorCount));

    if resultsColorMapLength < 1 then
        resultsColorMapLength := TargetResultsColorMapLength;

    setlength(result,resultsColorMapLength,3);

    if iColorCount = 1 then
    begin
        myCol := resultsColorsRec(0);
        color1 := myCol.Color;
        for j := 0 to resultsColorMapLength - 1 do
        begin
            result[j, 0] := getRValue(Color1) / 255;
            result[j, 1] := getGValue(Color1) / 255;
            result[j, 2] := getBValue(Color1) / 255;
        end;
    end
    else
    begin
        {blending smoothly from one color to the next version}
        entriesPerColor := resultsColorMapLength div (iColorCount - 1);
        for i := 0 to iColorCount - 2 do
        begin
            myCol := resultsColorsRec(i);
            color1 := myCol.Color;
            myCol := resultsColorsRec(i + 1);
            color2 := myCol.Color;
            {linear interpolation, multiply color1 by (k-j)/k and color2 by j/k}
            for j := 0 to entriesPerColor - 1 do
            begin
                result[i * entriesPerColor + j, 0] :=
                  (getRValue(Color1)*(entriesPerColor - j)/entriesPerColor +
                  getRValue(Color2)*j/entriesPerColor)/255;
                result[i * entriesPerColor + j, 1] :=
                  (getGValue(Color1)*(entriesPerColor - j)/entriesPerColor +
                  getGValue(Color2)*j/entriesPerColor)/255;
                result[i * entriesPerColor + j, 2] :=
                  (getBValue(Color1)*(entriesPerColor - j)/entriesPerColor +
                  getBValue(Color2)*j/entriesPerColor)/255;
            end;
        end;
        { fill in the last row on the colormap}
        myCol := resultsColorsRec(iColorCount - 1);
        Color2 := myCol.Color;
        result[resultsColorMapLength - 1, 0] := getRValue(Color2) / 255;
        result[resultsColorMapLength - 1, 1] := getGValue(Color2) / 255;
        result[resultsColorMapLength - 1, 2] := getBValue(Color2) / 255;
    end;
end;

procedure TdBSeaOptions.updateResultsLevelColors;
begin
    {if the user has made alterations to the colors, then we need to keep
    those colors at the start of the list (as many as are needed, some may be
    discarded if the results now cover a smaller range). If the
    user has never touched the list, then reset it with the current limits}
    if levelLimits.maxOrMinNaN then Exit;

    if not bUserSetColors then
        TDBSeaColours.resetResultsColorList(fResultsColors,saferound((levelLimits.Max - levelLimits.Min)/levelLimits.levelSpacing,1),0)
    else
        TDBSeaColours.updateResultsLevelColorsAnyList(fResultsColors,saferound((levelLimits.Max - levelLimits.Min)/levelLimits.levelSpacing,1));
end;

procedure TdBSeaOptions.updateCMaxCMinandLevelColors();
begin
    {do a complete reset on the max and min levels, and the interpolated contour levels.
    The LevDisp parameter is set separately from whatever is the TUWAOpts default, because
    when we are plotting a cross section we need to be able to set it to ldAllLayers
    so we have the overall max and min CMax and CMin}

    levelLimits.updateCMaxCMin();
    updateResultsLevelColors;
end;

procedure TdBSeaOptions.setBathymetryDecimate(const val: integer);
begin
    if val > 0 then
        fBathymetryDecimate := val;
end;

procedure TdBSeaOptions.setDispFeet(const val: boolean);
begin
    fdispFeet := val;
end;

procedure TdBSeaOptions.setFullXSecDepth(const val: boolean);
begin
    fFullXSecDepth := val;
end;

procedure TdBSeaOptions.setXSecTruncateAtWaterSurface(const val: boolean);
begin
    fXSecTruncateAtWaterSurface := val;
end;

procedure TdBSeaOptions.setDisplayZInd(const val: integer; const dmax: integer);
begin
    if val < 0 then exit;
    if val >= dmax then exit;

    fDisplayZInd := val;

    updateCMaxCMinandLevelColors();
end;

procedure TdBSeaOptions.setLevelsDisplay(const val: TLevelsDisplay; const dmax: integer);
begin
    if (val <> flevelsDisplay) then
    begin
        fLevelsDisplay := val;

        if (fLevelsDisplay = ldSingleLayer) then
            fdisplayZInd := max(0,min(dMax - 1,fdisplayZInd));

        updateCMaxCMinandLevelColors();
    end;
end;

function  TdBSeaOptions.getDepthViewInd: Integer;
begin
    case LevelsDisplay of
        ldMax: result := 0;  //we only plot at the surface
        ldSingleLayer, ldAllLayers: result := DisplayZInd;  //we only plot the zInd  iDisplayZInd
    else
        result := 0;
    end;
end;

function TdBSeaOptions.getDisplayZInd: integer;
begin
    result := displayZInd;
end;

procedure TdBSeaOptions.setVisibility(const ind: integer; const val: boolean);
begin
    fComponentVisibility[ind] := val;
end;

procedure TdBSeaOptions.setResultsColormapSmooth(const val: boolean);
begin
    fResultsColormapSmooth := val;
end;

procedure TdBSeaOptions.setResultsTris(const val: boolean);
begin
    fResultsTris := val;
end;

procedure TdBSeaOptions.setGLShowLandInXSec(const val: boolean);
begin
    fGLShowLandinXSec := val;
end;

function TdBSeaOptions.setMaxGLBathyPixels(const val: integer; const zDepthsLen: integer):boolean;
begin
    result := fMaxGLBathyPixels <> val;
    fMaxGLBathyPixels := val;
    self.setBathymetryDecimate(safeceil(zDepthsLen/maxGLBathyPixels));
end;

function  TdBSeaOptions.getResultsColorsSteps: integer;
{the target number of results colors, based on the spacing between the max and min.
there is 1 more color than this, due to fencepost error}
begin
    if levelLimits.maxOrMinNaN or isNaN(levelLimits.levelSpacing) then
        result := 0
    else
        result := safetrunc((levelLimits.max - levelLimits.min) / levelLimits.levelSpacing);
end;

function  TdBSeaOptions.getLevelContours: TDoubleDynArray;
var
    len: integer;
    i: integer;
begin
    len := getResultsColorsSteps;
    setlength(result,len + 1);
    for i := 0 to len do
        result[i] := getLevelContour(i);
end;

function TdBSeaOptions.getLevelsDisplay: TLevelsDisplay;
begin
    result := levelsDisplay;
end;

function  TdBSeaOptions.getLevelContour(const ind: integer): double;
{get the sound level in kSPL at a given contour index. note starts at min and goes up to max. }
begin
    if checkRange(ind,0,getResultsColorsSteps) then
        result := levelLimits.min + ind*levelLimits.levelSpacing
    else
        Raise Exception.Create('Requested level index out of range');
end;

function  TdBSeaOptions.ResultsColorsRec(const ind: integer): TMyColorRec;
begin
    if (ind >= 0) and (ind < fResultsColors.Count) then
        result := fResultsColors[ind].asTMyColorRec
    else
        result.makeRandom();
end;

function  TdBSeaOptions.ResultsColorsCount: integer;
begin
    result := self.fResultsColors.Count;
end;

procedure TdBSeaOptions.ResultsColorsClear;
begin
    self.fResultsColors.Clear;
end;

procedure TdBSeaOptions.ResultsColorsAdd(const aColor: TColor; const aVisible: boolean);
begin
    self.fResultsColors.Add(TMyColor.CreateWithColor(aColor,aVisible));
end;

initialization
    UWAOpts := TdBSeaOptions.Create();
finalization
    UWAOpts.free;

end.

