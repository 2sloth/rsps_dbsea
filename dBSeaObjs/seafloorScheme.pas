unit seafloorScheme;

{$B-}
interface

uses system.rtti, system.classes, system.math, system.types, system.SysUtils, system.JSON,
    generics.Collections,
    xml.xmldom, xml.XMLIntf,
    DBSeaObject, objectCopier, baseTypes,
    material,
    helperFunctions, globalsUnit;

type
{$RTTI INHERIT}
    TSeafloorLayer = class(TDBSeaObject)
      private
      var
        fName                       : string;
        fComments                   : string;
        fDepth                      : double;  //where this layer starts
      public
      var
        material                    : TMaterial;

        constructor Create;
        destructor Destroy; override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TSeafloorLayer);

        property  name: string read fName write fName;
        property  comments: string read fComments write fComments;
        property  depth: double read fDepth write fDepth;

        procedure setWithListAndWeightings(const arr: TArray<TSeafloorLayer>; const weightings: TDoubleDynArray);

        class function defaultLayer: TSeafloorLayer;
        class function createFromJSON(v: TJSONValue): TSeafloorLayer;
    end;
    TSeaFloorLayersList = class(TObjectList<TSeaFloorLayer>)
        procedure setWithJSON(arr: TJSONArray);
    end;

    TSeafloorScheme = class(TDBSeaObject)
      private
      var
        fName: string;
        fComments: String;
        fIsReferencedFromSeafloor: boolean;
      public
      var
        layers: TSeaFloorLayersList;

        constructor Create;
        destructor Destroy; override;
        procedure writeDataToXMLNode(const parentNode: IXMLNode); override;
        procedure readDataFromXMLNode(const parentNode: IDOMNode); override;
        procedure cloneFrom(const source: TSeafloorScheme);

        procedure addDefaultLayer;
        procedure addLayer;
        procedure deleteLayer(const i: integer);
        function  setLayerDepth(const ind: integer; const aDepth: double): boolean;
        function  layerThickness(const i: integer): double; overload;
        function  layerThickness(const layer: TSeafloorLayer): double; overload;

        function  getLayerAtDepthBelowSurface(const aDepth, localSeafloorDepth: double): TSeaFloorLayer;
        function  getMaterialAtDepthBelowSurface(const aDepth, localSeafloorDepth: double): TMaterial;
        procedure getLayerDensityList(const localDepth, rhoWater: double; var result: TDepthValArray);
        function  getMinimumSpeed(): double;

        property  name: string read fName write fName;
        property  comments: string read fComments write fComments;
        property  isReferencedFromSeafloor: boolean read fIsReferencedFromSeafloor write fIsReferencedFromSeafloor;

        procedure setWithListAndWeightings(const arr: TArray<TSeafloorScheme>; const weightings: TDoubleDynArray);

        class function defaultScheme: TSeafloorScheme;
        class function createFromJSON(v: TJSONValue): TSeafloorScheme;
    end;
    //pSeafloorScheme = ^TSeafloorScheme;
    TSeafloorSchemesList = TObjectList<TSeafloorScheme>;

    TSeafloorRange = record
        range: double;
        seafloor: TSeafloorScheme;

        class function create(r: double; sf: TSeafloorScheme): TSeafloorRange; static;
    end;
    TSeafloorRangeArray = array of TSeafloorRange;
    TSeafloorRangeArrayHelper = record helper for TSeafloorRangeArray
        function  seafloorForRange(const range: double; out index: integer): TSeafloorScheme;
    end;

implementation

constructor TSeafloorLayer.Create;
begin
    inherited Create;

    self.SaveBlacklist := 'material';

    fName := '';
    fDepth := nan;

    material := TMaterial.Create('',0,0,0,0,TDataSource.dsMDA);
end;

class function TSeafloorLayer.createFromJSON(v: TJSONValue): TSeafloorLayer;
var
    o: TJSONObject;
begin
    try
        o := TJSONObject(v);

        result := TSeafloorLayer.create();
        result.material.free;

        if not assigned(o.getValue('depth')) or not tryStrToFloat(o.getValue('depth').ToString, result.fdepth) then
            raise EJSONParsing.Create('Cannot parse depth');

        if assigned(o.getValue('name')) then
            o.getValue('name').TryGetValue<string>(result.fName);
        if assigned(o.getValue('comments')) then
            o.getValue('comments').TryGetValue<string>(result.fComments);

        if not assigned(o.getValue('material')) then
            raise EJSONParsing.Create('Seabed layer material field not found');

        result.material := TMaterial.createFromJSON(o.getValue('material'));

        if not assigned( result.material) then
            raise EJSONParsing.Create('Cannot parse material');

    except on e: Exception do
        exit(nil);
    end;
end;

destructor TSeafloorLayer.Destroy;
begin
    material.Free;

    inherited;
end;

procedure TSeafloorLayer.writeDataToXMLNode(const parentNode: IXMLNode);
begin
    inherited;
    if not assigned(parentNode) then exit;

    material.writeDataToXMLNode(parentNode.AddChild('Material'));
end;

procedure TSeafloorLayer.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
begin
    inherited;
    if not assigned(parentNode) then exit;

    childNode := (parentNode as IDOMNodeSelect).selectNode('Material');
    if assigned(childNode) then
        material.readDataFromXMLNode(childNode);
end;

procedure TSeafloorLayer.setWithListAndWeightings(const arr: TArray<TSeafloorLayer>; const weightings: TDoubleDynArray);
var
    i: integer;
    materialsArr: TArray<TMaterial>;
begin
    setlength(materialsArr, length(arr));
    self.depth := 0;
    for i := 0 to length(arr) - 1 do
    begin
        self.depth := self.depth + weightings[i] * arr[i].depth;
        materialsArr[i] := arr[i].material;
    end;
    self.material.setWithListAndWeightings(materialsArr, weightings);
end;

procedure TSeafloorLayer.cloneFrom(const source: TSeafloorLayer);
begin
    TObjectCopier.CopyObject(source,self);

    material := TMaterial.Create('',0,0,0,0,TDataSource.dsMDA); //dummy values
    material.cloneFrom(source.material);
end;

class function TSeafloorLayer.defaultLayer: TSeafloorLayer;
begin
    result := TSeafloorLayer.Create;
    result.material.setProps(1650, 1900, 0.8);
    result.material.Name := 'Sand';
    result.material.ID := 0;
    result.material.DataSource := TDataSource.dsMDA;
    result.name := 'Layer';
    result.depth := 0;
end;



constructor TSeafloorScheme.Create;
begin
    inherited Create;

    self.SaveBlacklist := 'layers';

    fName := '';
    fIsReferencedFromSeafloor := true;
    layers := TSeaFloorLayersList.create(true);
end;

class function TSeafloorScheme.createFromJSON(v: TJSONValue): TSeafloorScheme;
var
    o: TJSONObject;
begin
    try
        result := TSeafloorScheme.create();

        o := v as TJSONObject;

        if assigned(o.getValue('name')) then
            o.getValue('name').TryGetValue<string>(result.fName);
        if assigned(o.getValue('comments')) then
            o.getValue('comments').TryGetValue<string>(result.fComments);

        if not assigned(o.GetValue('isReferencedFromSeafloor')) then
            raise EJSONParsing.Create('Seabed isReferencedFromSeafloor field not found');
        o.GetValue('isReferencedFromSeafloor').TryGetValue<boolean>(result.fIsReferencedFromSeafloor);

        if not assigned(o.GetValue('layers')) then
            raise EJSONParsing.Create('Seabed layers field not found');

        result.layers.setWithJSON(o.GetValue('layers') as TJSONArray);

        if result.isReferencedFromSeafloor and (result.layers.First.depth <> 0) then
            raise EJSONParsing.Create('First seafloor layer must have depth = 0 if isReferencedFromSeafloor = true');

    except on e: Exception do
        exit(nil);
    end;
end;

destructor TSeafloorScheme.Destroy;
begin
    layers.free;

    inherited;
end;

procedure TSeafloorScheme.writeDataToXMLNode(const parentNode: IXMLNode);
var
    ChildNode: IXMLNode;
    i: integer;
begin
    inherited;
    if not assigned(parentNode) then exit;

    {writing layers}
    writeIntegerField(parentNode,'LayersCount',layers.Count);
    ChildNode := writeListField(parentNode,'LayersList');
    for i := 0 to layers.Count - 1 do
        layers[i].writeDataToXMLNode(ChildNode.AddChild('Layer' + tostr(i)));
end;

procedure TSeafloorScheme.readDataFromXMLNode(const parentNode: IDOMNode);
var
    childNode: IDOMNode;
begin
    inherited;
   if not assigned(parentNode) then exit;

    layers.Clear;
    repeat
        childNode := (parentNode as IDOMNodeSelect).selectNode('LayersList/Layer' + tostr(layers.count));
        if assigned(childNode) then
        begin
            layers.Add(TSeafloorLayer.Create);
            layers.Last.readDataFromXMLNode(childNode);
        end;
    until not assigned(childNode);
    if layers.Count = 0 then
        addDefaultLayer;
end;

procedure TSeafloorScheme.cloneFrom(const source: TSeafloorScheme);
var
    i: integer;
begin
    TObjectCopier.CopyObject(source,self);

    layers.Clear;
    for i := 0 to source.layers.Count - 1 do
    begin
        layers.Add(TSeafloorLayer.Create);
        layers.Last.cloneFrom(source.layers[i]);
    end;
    if layers.Count = 0 then
        addDefaultLayer;
end;

procedure TSeafloorScheme.addLayer;
var
    layer, copyLayer: TSeafloorLayer;
begin
    copyLayer := layers.Last;
    layers.Add(TSeafloorLayer.Create);
    layer := layers.Last;
    layer.cloneFrom(copyLayer);
    if layer.depth = 0 then
        layer.depth := 1.0
    else
        layer.depth := layer.depth * 2.0;
end;

procedure TSeafloorScheme.addDefaultLayer;
begin
    layers.add(TSeafloorLayer.defaultLayer);
end;

procedure TSeafloorScheme.deleteLayer(const i: integer);
begin
    if (layers.Count < 2) or (i >= layers.Count) then exit;
    layers.Delete(i);
end;

function  TSeafloorScheme.setLayerDepth(const ind: integer; const aDepth: double): boolean;
var
    layer, prevLayer, nextLayer: TSeafloorLayer;
begin
    if ind < 0 then exit(false);
    if isReferencedFromSeafloor and (ind = 0) then exit(false); //should always be 0 for this case
    if ind >= layers.Count then exit(false);

    result := true;
    layer := self.layers[ind];

    if ind < self.layers.Count - 1 then
    begin
        //we have a next layer, check depth is between both next and prev layer
        nextLayer := self.layers[ind + 1];
        if aDepth >= nextLayer.depth then
            result := false;
    end;

    if ind > 0 then
    begin
        prevLayer := self.layers[ind - 1];
        //no next layer, just check if depth is greater than previous
        if aDepth <= prevLayer.depth then
            result := false;
    end;

    if result then //now ok to set the layer to this depth
        layer.depth := aDepth;
end;

function  TSeafloorScheme.layerThickness(const i: integer): double;
begin
    if i >= layers.count then exit(nan);
    if i < 0 then exit(nan);
    if i = layers.Count - 1 then exit(10000); //some large number for last layer
    result := layers[i + 1].depth - layers[i].depth;
end;

function  TSeafloorScheme.getLayerAtDepthBelowSurface(const aDepth, localSeafloorDepth: double): TSeaFloorLayer;
var
    layer: TSeafloorLayer;
begin
    if layers.Count = 0 then exit(nil);
    if (aDepth < localSeafloorDepth) then exit(nil);

    result := layers.first;
    for layer in layers do
    begin
        if isReferencedFromSeafloor then
        begin
            if (aDepth - localSeafloorDepth) > layer.depth then
                result := layer;
        end
        else
        begin
            if aDepth > layer.depth then
                result := layer;
        end;
    end;
end;


function  TSeafloorScheme.getMaterialAtDepthBelowSurface(const aDepth, localSeafloorDepth: double): TMaterial;
begin
    if layers.Count = 0 then exit(nil);
    if (aDepth < localSeafloorDepth) then exit(water);

    result := self.getLayerAtDepthBelowSurface(aDepth, localSeafloorDepth).material;
end;

procedure TSeafloorScheme.getLayerDensityList(const localDepth, rhoWater: double; var result: TDepthValArray);
var
    i, j: integer;
    layer: TSeafloorLayer;
begin
    if IsNan(localDepth) then //nodata
    begin
        setlength(result, 1);
        result[0].depth := nan;
        result[0].val := rhoWater;
    end;

    if length(result) <> layers.count + 1 then
        setlength(result, layers.Count + 1);
    result[0].depth := 0.0;
    result[0].val := rhoWater;
    j := 0;
    for i := 0 to layers.Count - 1 do
    begin
        layer := layers[i];
        //if isReferencedFromSeafloor then all layers get added. if not, then only layers that are greater than the local depth
        if isReferencedFromSeafloor or (layer.depth > localDepth) then
        begin
            inc(j);
            if isReferencedFromSeafloor then
                result[j].depth := localDepth + layer.depth
            else
                result[j].depth := layer.depth;
            result[j].val := layer.material.rho_g;
        end;
    end;

    //if needed, trim the empty space at end of array
    if not isReferencedFromSeafloor then
    begin
        setlength(result, j + 1);

        //special case, when all the layers are too shallow: still need at least sediment 1 entry
        if j = 0 then
        begin
            layer := layers.Last;
            inc(j);
            setlength(result, j + 1);
            result[j].depth := localDepth;
            result[j].val := layer.material.rho_g;
        end;
    end;
end;

function  TSeafloorScheme.getMinimumSpeed(): double;
var
    layer: TSeafloorLayer;
begin
    result := nan;
    for layer in self.layers do
        lesser(result,layer.material.c);
end;

function TSeafloorScheme.layerThickness(const layer: TSeafloorLayer): double;
begin
    result := layerThickness(layers.IndexOf(layer));
end;

class function TSeafloorScheme.defaultScheme: TSeafloorScheme;
begin
    result := TSeafloorScheme.Create;
    result.name := 'Default scheme';
    result.isReferencedFromSeafloor := true;
    result.addDefaultLayer;
end;

procedure TSeafloorScheme.setWithListAndWeightings(const arr: TArray<TSeafloorScheme>; const weightings: TDoubleDynArray);
var
    i,j: integer;
    layer: TSeafloorLayer;
    layersArr: TArray<TSeafloorLayer>;
begin
    self.layers.clear;

    if length(arr) <> length(weightings) then
        raise Exception.Create(format('Can''t set seabed properties, array lengths don''t match: %d %d',[length(arr), length(weightings)]));

    setlength(layersArr, length(arr));
    for i := 0 to length(arr) - 1 do
    begin
        if arr[i].layers.count <> arr[0].layers.count then
            raise Exception.Create(format('Can''t set seabed properties, seabeds don''t have the same number of layers: %d %d',[arr[0].layers.Count, arr[i].layers.count]));

        if arr[i].IsReferencedFromSeafloor <> arr[0].IsReferencedFromSeafloor then
            raise Exception.Create('Can''t set seabed properties, all seabeds must have same value for ''referenced from seafloor''');
    end;

    self.isReferencedFromSeafloor := arr[0].isReferencedFromSeafloor;
    for j := 0 to arr[0].layers.count - 1 do
    begin
        layer := TSeafloorLayer.create();

        for i := 0 to length(arr) - 1 do
           layersArr[i] := arr[i].layers[j];

        layer.setWithListAndWeightings(layersArr, weightings);
        self.layers.add(layer);
    end;
end;


{ TSeafloorRangeArrayHelper }

function TSeafloorRangeArrayHelper.seafloorForRange(const range: double; out index: integer): TSeafloorScheme;
var
    i: integer;
begin
    result := TSeafloorRange(self[0]).seafloor;
    index := 0;
    for i := 1 to length(self) - 1 do
        if TSeafloorRange(self[i]).range <= range then
        begin
            result := TSeafloorRange(self[i]).seafloor;
            index := i;
        end
        else
            exit();
end;

{ TSeafloorRange }

class function TSeafloorRange.create(r: double; sf: TSeafloorScheme): TSeafloorRange;
begin
    result.range := r;
    result.seafloor := sf;
end;

{ TSeaFloorLayersList }

procedure TSeaFloorLayersList.setWithJSON(arr: TJSONArray);
var
    i: integer;
begin
    self.clear;

    if arr.count < 1 then
        raise EJSONParsing.Create('Seabed array cannot have length = 0');

    for i := 0 to arr.count - 1 do
        self.add(TSeafloorLayer.createFromJSON(arr.items[i]));

    if self.count = 0 then
        raise EJSONParsing.Create('No seafloor layers imported');
end;

end.
