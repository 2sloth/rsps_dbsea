// functions that require no knowledge of non-Delphi types

unit helperFunctions;

interface

uses system.SysUtils, system.UITypes, system.Math, system.Strutils, system.classes,
    system.IniFiles, system.rtti, System.Typinfo,
    generics.collections,
    vcl.Dialogs,
    vcl.Forms, vcl.clipbrd,
    vcl.menus,
    winapi.Windows,
    winapi.ShlObj,
    winapi.TlHelp32,
    eventlogging,
    SmartPointer,
    otlParallel;

type
    THelper = class(TObject)
        class function  readFileToString(const FileName: TFileName): String;
        class function  readFileToBytes(const FileName: TFileName): TBytes;
        class function  readNBytesOfFileToString(const FileName: TFileName; const n: int64): String;
        class function  writeStringToFile(const FileName: TFileName; const str: string): boolean;
        class procedure fixForVCLOnclickProblemWithTComboBox;

        class function  findMenuItem(const mainitem: TMenuItem; const s:string): TMenuItem;
        class function  caseOfText(const s: string; const a: array of string): Integer;
        class function  getImageType(const FileName: String): string;

        class function  TColorToHex(const Color: TColor): string;
        class function  TColorMix(const color1, color2: TColor; const mix: double): TColor;
        class function  TColorToLuminosity(const Color: TColor): integer;

        class function  processExists(const sProcessName: string): Boolean;
        class function  killProcess(const sProcessName: string): Integer;
        class function  testAndKillProcess(const sProcessName: string): integer;
        class procedure RemoveWarning(const x); inline;

        class function  GetLocalComputerName: string;
        class function  getLocalAppDataPath: string;
        class function  getMyDocumentsPath: string;
        class function  currentMemoryUsage: cardinal;
        class procedure saveINI;

        class function  isValidFileName(const fileName: string): boolean;
        class function  isFileInUse(const fName: TFileName): Boolean;
        class function  getSubDirectories(const directory: string): TStringlist;
        class function  getFilenamesInDir(const Path: string): TStringList;
        class function  deleteMatchingFilesInDir(const thePath, matchString: string): boolean;

        class procedure forceReferenceToInt(i: integer);
        class procedure forceReferenceToClass(C: TClass);
        class procedure patchInstanceClass(Instance: TObject; NewClass: TClass);

        class function  bytePos(const Pattern: TBytes; const Buffer: PByte; const BufLen, mustBeAfterByte: cardinal): int64;
        class function  resourceFileUnbundle(const targetDir, filename, resID: string; const forceOverwrite: boolean = false): Boolean;
        class function  LinkerTimeStamp(): TDateTime;
        class procedure asyncWait(const timeout: cardinal; onMainThread: TProc);

        class function  factorial(n: Integer): Integer;
        class procedure setClipboard(const s: string);
        class function  getClipboard: string;
        class procedure append<T>(const val: T; var arr: TArray<T>);
        class function  iterate<T>(const arr: TArray<T>): TArray<TPair<integer,T>>; overload;
        class function  iterate<T>(const list: TList<T>): TArray<TPair<integer,T>>; overload;
        class function  twoDigitIntToStr(const i: integer): string;
    end;

    function    forceDotSeparator(const s: string): string;
    function    tostr(const val: int64):string; overload;
    function    tostr(const val: extended):string; overload;
    function    tostrWithTestAndRound1(const val: extended):string;
    function    tostr(const val: boolean):string; overload;
    function    tryTofl(const s: string): double;
    function    tofl(const s: string; const initialise: boolean = false):double;
    function    toint(const s: string):integer;
    function    tobool(const s: string): boolean;
    procedure   greater(var existingVal: double; const comparisonVal: double); overload;
    procedure   greater(var existingVal: single; const comparisonVal: single); overload;
    procedure   lesser(var existingVal: double; const comparisonVal: double); overload;
    procedure   lesser(var existingVal: single; const comparisonVal: single); overload;
    procedure   lesserGreater(var existingMin, existingMax: double; const comparisonVal: double);
    function    smaller(const a,b :double):double; overload;
    function    smaller(const a,b :integer):integer; overload;
    function    smaller(const a,b :int64):int64; overload;
    function    larger(const a,b :double):double; overload;
    function    larger(const a,b :integer):integer; overload;
    function    larger(const a,b: int64): int64; overload;

    function    f2m(const val: double):double; overload;
    function    m2f(const val: double):double; overload;
    function    f2m(const val: double; const displayFeet: boolean):double; overload;
    function    m2f(const val: double; const displayFeet: boolean):double; overload;
    function    makeMunkSSP(const maxDepth: double; const depthAtMinSp: double; const minSp: double):string;

    function    dB10(const val: double): single;
    function    dB20(const val: double): single;
    function    undB20(const val: single):double;
    function    undB10(const val: single):double;
    function    dbAdd(const a,b: double): double; overload;
    function    dbAdd(const a,b: single): single; overload;
    function    invdbAdd(const a,b: double): double; overload;
    function    invdbAdd(const a,b: single): single; overload;
    function    nextPow2(const val: int64): int64;

    function    subStringOccurencesI(const subString, sourceString: string): integer;
    function    subStringOccurences(const subString, sourceString: string): integer;

    function    isFloat(const s: string; const initialise: boolean = false):boolean;
    function    isInt(const s: string; const stripSeparators: boolean = true):boolean;
    function    angleRadToCompassAngleDeg(const a: double): double;
    function    angleWrap(const val: double; const radians: boolean = true):double;
    function    integerWrap(const val, target: integer):integer;
    function    fmod(const ParaDividend,ParaDivisor: double ): double;
    function    round1(const val: double):double;
    function    ceilx(const x,y: double): double; overload;
    function    floorx(const x,y: double): double; overload;
    function    roundx(const x,y: double): double; overload;
    function    ceilx(const x: double; const y: integer): integer; overload;
    function    floorx(const x: double; const y: integer): integer; overload;
    function    roundx(const x: double; const y: integer): integer; overload;
    function    safeRound(const val: double; const errorVal: integer = 0):integer;
    function    safeFloor(const val: double; const errorVal: integer = 0):integer;
    function    safeCeil(const val: double; const errorVal: integer = 0):integer;
    function    safeTrunc(const val: double; const errorVal: integer = 0):integer;
    function    safeRound64(const val: double; const errorVal: int64 = 0): int64;
    function    safeFloor64(const val: double; const errorVal: int64 = 0): int64;
    function    safeCeil64(const val: double; const errorVal: int64 = 0): int64;
    function    safeTrunc64(const val: double; const errorVal: int64 = 0): int64;
    function    safeFloatToStrF(Value: Extended; Format: TFloatFormat; Precision, Digits: Integer; const defaultVal: String = '-'): string;


implementation

     {$B-}

procedure ashow(const val: int64);
begin
    {quick access to showmessage for numbers}
    showmessage(tostr(val));
end;

function tostr(const val: int64):string;
begin
    result := inttostr(val);
end;

function tostr(const val: extended):string;
begin
    result := floattostr(val);
end;

function tostrWithTestAndRound1(const val: extended):string;
begin
    if isnan(val) then
        result := ''
    else
        result := tostr(round1(val));
end;

function tostr(const val: boolean):string;
begin
    {generic bool to string}
    if val then
        result := 'True'
    else
        result := 'False';
end;

function    tryTofl(const s : string) : double;
begin
    if isFloat(s) then
        result := tofl(s)
    else
        result := nan;
end;

function forceDotSeparator(const s : string) : string;
Var
    sepPos: Integer;
begin
    Result := s;
    SepPos := Pos( ',', Result );
    If SepPos > 0 Then
        Result[SepPos] := '.';
end;


{$J+}
function tofl(const s: string; const initialise : boolean = false):double;
    function patchLocalSeparator(Const StrValue: String): String;
    Var
        SepPos: Integer;
    Begin
        Result := StrValue;
        SepPos := Pos( '.', Result );
        If SepPos > 0 Then
            Result[SepPos] := ',';
    End;
const
    bUserDecimalSeparatorIsComma : boolean = false;
var
  tmp   : string;
begin
    if initialise then
    begin
        bUserDecimalSeparatorIsComma := TFormatSettings.create(LOCALE_USER_DEFAULT).DecimalSeparator = ',';
    end;

    //strtofloat is locale.decimalSeparator dependent
    if bUserDecimalSeparatorIsComma then
        tmp := patchLocalSeparator(trim(s))
    else
        tmp := trim(s);

    result := strtofloat(tmp);
end;
{$J-}

function toint(const s: string):integer;
begin
    {generic string to number}
    result := strtoint(trim(s));
end;

function    tobool(const s : string) : boolean;
begin
    {generic string to bool}
    case THelper.CaseOfText(s,['True','False','1','0']) of
        0 : Result := true;
        1 : Result := false;
        2 : Result := true;
        3 : Result := false;
    else
        raise Exception.Create('Error: string of boolean must be True or False');
    end;
end;

procedure greater(var existingVal : double; const comparisonVal : double);
begin
    // similar to max but takes nan into account
    if (not isnan(comparisonVal)) and (isnan(existingVal) or (comparisonVal > existingVal)) then
        existingVal := comparisonVal;
end;

procedure   greater(var existingVal : single; const comparisonVal : single);
begin
    if (not isnan(comparisonVal)) and (isnan(existingVal) or (comparisonVal > existingVal)) then
        existingVal := comparisonVal;
end;

procedure lesser(var existingVal : double; const comparisonVal : double);
begin
    // similar to min but takes nan into account
    if (not (isnan(comparisonVal) or IsInfinite(comparisonVal))) and (isnan(existingVal) or (comparisonVal < existingVal)) then
        existingVal := comparisonVal;
end;

procedure lesser(var existingVal : single; const comparisonVal : single);
begin
    // similar to min but takes nan into account
    if (not (isnan(comparisonVal) or IsInfinite(comparisonVal))) and (isnan(existingVal) or (comparisonVal < existingVal)) then
        existingVal := comparisonVal;
end;

procedure   lesserGreater(var existingMin, existingMax : double; const comparisonVal : double);
begin
    lesser (existingMin,comparisonVal);
    greater(existingMax,comparisonVal);
end;

function f2m(const val : double):double;
{feet to meters}
begin
    result := val * 0.3048;
end;

function m2f(const val : double):double;
{meters to feet}
begin
    result := val * 3.28084;
end;

function f2m(const val : double; const displayFeet : boolean):double;
{feet to meters, only convert if displayFeet is true, otherwise simply return val}
begin
    if displayFeet then
        result := val * 0.3048
    else
        result := val;
end;

function m2f(const val : double; const displayFeet : boolean):double;
{meters to feet, only convert if displayFeet is true, otherwise simply return val}
begin
    //the boolean tells us whether to convert or not
    if displayFeet then
        result := val * 3.28084
    else
        result := val;
end;

function makeMunkSSP(const maxDepth: double; depthAtMinSp: double; const minSp: double):string;
{make Munk SSP with depth of min SP and SP at that depth. Returns string of SSPs}
var
    zjump: Integer;
begin
    zJump := maxDepth div 100;


end;

class function THelper.TColorToHex(const Color : TColor) : string;
{convert a Color to a string containig a hex value - used during saving}
begin
   Result :=
     IntToHex(GetRValue(Color), 2) +
     IntToHex(GetGValue(Color), 2) +
     IntToHex(GetBValue(Color), 2) ;
end;

class function   THelper.TColorMix(const color1, color2 : TColor; const mix : double) : TColor;
var
    aMix : double;
begin
    aMix := mix;
    if aMix < 0 then aMix := 0;
    if aMix > 1 then aMix := 1;
    result := RGB(byte(saferound(getRValue(Color1)*(1 - aMix) + getRValue(Color2)*aMix)),
                  byte(saferound(getGValue(Color1)*(1 - aMix) + getGValue(Color2)*aMix)),
                  byte(saferound(getBValue(Color1)*(1 - aMix) + getBValue(Color2)*aMix)));
end;

class function THelper.TColorToLuminosity(const Color : TColor) : integer;
{convert color to luminosity}
begin
   Result := GetRValue(Color) + GetGValue(Color) + GetBValue(Color);
end;

function    dB10(const val : double):single;
{convert to decibels, 10 log version}
begin
    if isnan(val) then exit(nan);
    Result := 10*log10(val);
end;

function    dB20(const val : double):single;
{convert to decibels, 20 log version}
begin
    if isnan(val) then exit(nan);
    Result := 20*log10(val);
end;

function    undB20(const val : single):double;
begin
    if isnan(val) then exit(nan);
    Result := power(10, 0.05 * val);
end;

function    undB10(const val : single):double;
begin
    if isnan(val) then exit(nan);
    Result := power(10, 0.1 * val);
end;

function dbAdd(const a,b : double): double;
{add two numbers, decibel version. eg add energies}
begin
    if isNaN(a) or isNaN(b) then exit(nan);
    Result:= 10*log10(power(10,a/10) + power(10,b/10));
end;

function    dbAdd(const a,b : single): single;
begin
    if isNaN(a) or isNaN(b) then exit(nan);
    Result:= 10*log10(power(10,a/10) + power(10,b/10));
end;

function invdbAdd(const a,b : double): double;
{add two numbers, decibel version. eg add energies}
begin
    if isNaN(a) or isNaN(b) then exit(nan);
    Result:= 10*log10(1 / ((1 / power(10,a/10)) + (1 / power(10,b/10))));
end;

function    invdbAdd(const a,b : single): single;
begin
    if isNaN(a) or isNaN(b) then exit(nan);
    Result:= 10*log10(1 / ((1 / power(10,a/10)) + (1 / power(10,b/10))));
end;

class function THelper.factorial(n: Integer): Integer;
var
    i: Integer;
begin
    Result:= 1;
    for i := 1 to n do
        Result:= Result * i;
end;

class function   THelper.FindMenuItem(const mainitem:TMenuItem; const s:string): TMenuItem;
{return a menu item by search for name}
var
    i: integer;
begin
    result := nil;
    for i := 0 to mainitem.count - 1 do
    begin
        if mainitem.items[i].name = s then
            result := mainitem.items[i]
        else if mainitem.items[i].count > 0 then
            result := FindMenuItem(mainitem.items[i],s);//look through subitems
        if result <> nil then BREAK;
    end;
end;

class function THelper.CaseOfText(const s: string; const a: array of string): Integer;
{find which entry of the array of string, the string matches, return integer. used
to extend the 'case' command. *case insensitive*}
begin
    Result := 0;
    while (Result < high(a)) and (AnsiLowerCase(a[Result]) <> AnsiLowerCase(s)) do
        Inc(Result);
    if (AnsiLowerCase(a[Result]) <> AnsiLowerCase(s)) then
        Result := -1;
end;


class function THelper.processExists(const sProcessName: string): Boolean;
{check (win32) if a process with a given name exists}
var
    ContinueLoop: BOOL;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
begin
    FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
    ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
    Result := False;
    while Integer(ContinueLoop) <> 0 do
    begin
        if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
        UpperCase(sProcessName)) or (UpperCase(FProcessEntry32.szExeFile) =
        UpperCase(sProcessName))) then
            Result := True;
        ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
    end;
    CloseHandle(FSnapshotHandle);
end;

class function THelper.KillProcess(const sProcessName: string): Integer;
{kill (win32) a process of a given name}
const
    PROCESS_TERMINATE = $0001;
var
    ContinueLoop: BOOL;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
begin
    Result := 0;
    FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
    ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
    while Integer(ContinueLoop) <> 0 do
    begin
        if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
            UpperCase(sProcessName)) or (UpperCase(FProcessEntry32.szExeFile) =
            UpperCase(sProcessName))) then
            Result := Integer(TerminateProcess(
                OpenProcess(PROCESS_TERMINATE,BOOL(0),FProcessEntry32.th32ProcessID),0));
            ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
    end;
    CloseHandle(FSnapshotHandle);
end;

class function THelper.TestAndKillProcess(const sProcessName: string): integer;
{Check if a named process exists and kill it if the user confirms.
returns
0 = no such process
1 = process exists, and was killed
2 = process exists, not killed}
var
    buttonSelected : integer;
begin
    result := 0;
    if not processExists(sProcessName) then Exit;

    buttonSelected := MessageDlg('Process ' + sProcessName +
          ' already exists, it will be closed', mtConfirmation, mbOKCancel,
        0);
    if buttonSelected = mrOK then
    begin
        KillProcess(sProcessName);
        result := 1;
    end;
    if buttonSelected = mrCancel then
    begin
        result := 2;
    end;
end;

class function THelper.twoDigitIntToStr(const i: integer): string;
begin
    result := ifthen(i < 10, '0','') + inttostr(i);
end;

class function THelper.GetLocalAppDataPath : string;
{used to get a path in LocalAppData (nb SHGetFolderPath is supposed to be superseded, but this
    //provides backwards compatibility with pre-Vista windows}
const
    SHGFP_TYPE_CURRENT = 0;
var
    path: array [0..MaxChar] of char;
begin
    SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,0,SHGFP_TYPE_CURRENT,@path[0]);
    Result := StrPas(path);
end;

class function  THelper.GetMyDocumentsPath: string;
 var
    r: Bool;
    path: array[0..Max_Path] of Char;
 begin
    r := ShGetSpecialFolderPath(0, path, CSIDL_Personal, False) ;
    if not r then result := 'c:\';
    Result := Path;
 end;

class function THelper.GetLocalComputerName: string;
var
    c1: dword;
    arrCh: array [0 .. Max_Path] of char;
begin
    c1 := Max_Path;
    GetComputerName(arrCh, c1);
    if c1 > 0 then
        result := arrCh
    else
        result := '';
end;

class procedure THelper.saveINI;
{use for saving settings to a file
    // you can also use the registry, ie uses registry,  reg:TRegistry}
Var
    IniF: ISmart<TIniFile>;
    s: String;
    // i : integer;
    // b : boolean;
Begin
    IniF := TSmart<TIniFile>.create(TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')));

    IniF.WriteString('SectionName','IdString','Value');
    IniF.WriteInteger('SectionName','IdInteger',100);
    IniF.WriteBool('SectionName','IdBool',False);

    // and now let's read the ini file
    s := IniF.ReadString('SectionName','IdString','Default');
    //i := IniF.ReadInteger('SectionName','IdInteger',0);
    //b := IniF.ReadBool('SectionName','IdBool',True);
    // The third parameter is the default in case that
    // there is no value to read
End;


class procedure THelper.setClipboard(const s: string);
begin
    Clipboard.AsText := s;
end;

class function THelper.CurrentMemoryUsage: cardinal;
var
    st: TMemoryManagerState;
    sb: TSmallBlockTypeState;
begin
    GetMemoryManagerState(st);
    result := st.TotalAllocatedMediumBlockSize + st.TotalAllocatedLargeBlockSize;
    for sb in st.SmallBlockTypeStates do begin
        result := result + sb.UseableBlockSize * sb.AllocatedBlockCount;
    end;
end;



class function THelper.getImageType(const FileName: String): string;
const
    JPG_HEADER: array[0..2] of byte = ($FF, $D8, $FF);
    GIF_HEADER: array[0..2] of byte = ($47, $49, $46);
    BMP_HEADER: array[0..1] of byte = ($42, $4D);
    PNG_HEADER: array[0..3] of byte = ($89, $50, $4E, $47);
    TIF_HEADER: array[0..2] of byte = ($49, $49, $2A);
var
    Stream: ISmart<TFileStream>;
    MemStr: ISmart<TMemoryStream>;
begin
    Result := '';
    MemStr := TSmart<TMemoryStream>.create(TMemoryStream.Create);
    Stream := TSmart<TFileStream>.create(TFileStream.Create(FileName, fmOpenRead));
    MemStr.CopyFrom(Stream, 5);
    if MemStr.Size > 4 then
    begin
        if CompareMem(MemStr.Memory, @JPG_HEADER, SizeOf(JPG_HEADER)) then
            Result := 'JPG'
        else if CompareMem(MemStr.Memory, @GIF_HEADER, SizeOf(GIF_HEADER)) then
            Result := 'GIF'
        else if CompareMem(MemStr.Memory, @PNG_HEADER, SizeOf(PNG_HEADER)) then
            Result := 'PNG'
        else if CompareMem(MemStr.Memory, @BMP_HEADER, SizeOf(BMP_HEADER)) then
            Result := 'BMP'
        else if CompareMem(MemStr.Memory, @TIF_HEADER, SizeOf(TIF_HEADER)) then
            Result := 'TIF';
    end;
end;


function smaller(const a, b: double): double;
begin
    if isnan(a) or isnan(b) then
        exit(nan);
    result := ifthen(a <= b, a, b);
end;

function smaller(const a, b: Integer): Integer;
begin
    result := ifthen(a <= b, a, b);
end;

function smaller(const a, b: int64): int64;
begin
    result := ifthen(a <= b, a, b);
end;

function larger(const a, b: double): double;
begin
    if isnan(a) or isnan(b) then
        exit(nan);
    result := ifthen(a >= b, a, b);
end;

function larger(const a, b: Integer): Integer;
begin
    result := ifthen(a >= b, a, b);
end;

function larger(const a, b: int64): int64;
begin
    result := ifthen(a >= b, a, b);
end;


class function THelper.isValidFileName(const FileName: string): boolean;
const
    InvalidCharacters: set of ansichar = ['\', '/', ':', '*', '?', '"', '<', '>', '|'];
var
    C: widechar;
begin
    {test if "fileName" is a valid Windows file name}
    if fileName = '' then
    begin
        result := false;
        Exit;
    end;

    result := true;

    for c in fileName do
        if charinset(c,InvalidCharacters) then
        begin
            result := false;
            Exit;
        end;
end;

class function THelper.iterate<T>(const list: TList<T>): TArray<TPair<integer, T>>;
var
    i: integer;
begin
    setlength(result,list.count);
    for i := 0 to list.Count - 1 do
        result[i] := TPair<integer,T>.create(i,list[i]);
end;

class function THelper.iterate<T>(const arr: TArray<T>): TArray<TPair<integer, T>>;
var
    i: integer;
begin
    setlength(result,length(arr));
    for i := 0 to length(arr) - 1 do
        result[i] := TPair<integer,T>.create(i,arr[i]);
end;

class function THelper.IsFileInUse(const fName: TFileName): Boolean;
var
    HFileRes: HFILE;
begin
    try
        HFileRes := CreateFile(PChar(fName),
                             GENERIC_READ or GENERIC_WRITE,
                             0,
                             nil,
                             OPEN_EXISTING,
                             FILE_ATTRIBUTE_NORMAL,
                             0);
        Result := (HFileRes = INVALID_HANDLE_VALUE);
        if not Result then
            CloseHandle(HFileRes);
    except
        result := false;
    end;
end;

class function THelper.getClipboard: string;
begin
    result := Clipboard.AsText;
end;

class function THelper.GetFilenamesInDir(const Path: string): TStringList;
var
    SR: TSearchRec;
begin
    result := TStringList.Create;
    if FindFirst(Path + '*.*', faAnyFile, SR) = 0 then
    repeat
        result.Add(SR.Name);
    until FindNext(SR) <> 0;
    system.SysUtils.FindClose(SR);
end;

class function  THelper.DeleteMatchingFilesInDir(const thePath, matchString: string): boolean;
var
    sl: ISmart<TStringlist>;
    i: Integer;
    sTmpFiles: string;
begin
    result := true;
    sl := TSmart<TStringlist>.create(GetFilenamesInDir(thePath));
    for i := 0 to sl.Count - 1 do
    begin
        sTmpFiles := sl[i];
        if (sTmpFiles <> '.') and (sTmpFiles <> '..') then
            if SubStringOccurences(matchString,sTmpFiles) = 1 then
                if not system.SysUtils.DeleteFile(IncludeTrailingPathDelimiter(thePath) + sTmpFiles) then
                    result := false;
    end;
end;

class function THelper.GetSubDirectories(const directory : string) : TStringlist;
var
    sr : TSearchRec;
begin
    { fills the TStringlist with the subdirectories of the "directory" directory}
    result := TStringlist.Create;
    try
        if FindFirst(IncludeTrailingPathDelimiter(directory) + '*.*', faDirectory, sr) < 0 then
            Exit
        else
        repeat
            if ((sr.Attr and faDirectory <> 0) and (sr.Name <> '.') and (sr.Name <> '..')) then
                result.Add(IncludeTrailingPathDelimiter(directory) + sr.Name) ;
        until FindNext(sr) <> 0;
    finally
        system.SysUtils.FindClose(sr);
    end;
end;

function SubStringOccurencesI(const subString, sourceString : string) : integer;
{return how often a substring occurs within a string, case independent}
var
    sSub,sSource : string;
begin
    sSub := lowercase(subString);
    sSource := lowercase(sourceString);
    result := SubStringOccurences(sSub,sSource);
end;


function SubStringOccurences(const subString, sourceString : string) : integer;
{return how often a substring occurs within a string. case dependent}
var
    pEx : integer;
begin
    result := 0;
    pEx := PosEx(subString, sourceString, 1);
    while pEx <> 0 do
    begin
        Inc(result);
        pEx := PosEx(subString, sourceString, pEx + length(subString));
    end;
end;

class function THelper.ReadFileToBytes(const FileName: TFileName): TBytes;
var
    FileStream: ISmart<TFileStream>;
begin
    FileStream := TSmart<TFileStream>.create(TFileStream.Create(FileName, fmOpenRead));
    if FileStream.Size = 0 then
    begin
        setlength(result, 0);
        Exit;
    end;

    SetLength(result, FileStream.Size);
    FileStream.Read(result[0], FileStream.Size);
end;

class function THelper.ReadFileToString(const FileName: TFileName): String;
var
    FileStream: ISmart<TFileStream>;
    Bytes: TBytes;
begin
    Result:= '';
    FileStream := TSmart<TFileStream>.create(TFileStream.Create(FileName, fmOpenRead));
    if FileStream.Size = 0 then Exit;

    SetLength(Bytes, FileStream.Size);
    FileStream.Read(Bytes[0], FileStream.Size);
    Result:= TEncoding.ASCII.GetString(Bytes);
end;

class function THelper.ReadNBytesOfFileToString(const FileName: TFileName; const n: int64): String;
var
    FileStream: ISmart<TFileStream>;
    Bytes: TBytes;
    bytesToRead: int64;
begin
    Result:= '';
    FileStream := TSmart<TFileStream>.create(TFileStream.Create(FileName, fmOpenRead));
    if FileStream.Size = 0 then Exit;

    bytesToRead := smaller(n,FileStream.Size);
    SetLength(Bytes, bytesToRead);
    FileStream.Read(Bytes[0], bytesToRead);
    Result := TEncoding.ASCII.GetString(Bytes);
end;

class procedure THelper.RemoveWarning(const x); begin end;

class function THelper.WriteStringToFile(const FileName : TFileName; const str : string): boolean;
var
    Writefile : TextFile;
begin
    result := true;
    {check if the file exists, and if so, are we able to get exclusive lock?
    if not, fail and exit.}
    if fileExists(FileName) then
        if IsFileInUse(FileName)  then
        begin
            result := false;
        end;

    if result then
    begin
        {the file doesn't exist, or it does and we were able to lock it. continue with
        writing to file.}
        AssignFile(Writefile, FileName);
        ReWrite(Writefile);
        Write(Writefile, str);
        WriteLn(Writefile);
        CloseFile(Writefile);
    end;
end;

{$J+}
function isFloat(const s : string; const initialise : boolean = false):boolean;
{returns true if the string can be parsed as a number}
    function patchLocalSeparator(Const StrValue: String): String;
    Var
        SepPos: Integer;
    Begin
        Result := StrValue;
        SepPos := Pos( ',', Result );
        If SepPos > 0 Then
            Result[SepPos] := '.';
    End;

const
    bUserDecimalSeparatorIsComma : boolean = false;
var
  iCode : Integer;
  f     : extended;
  tmp   : string;
begin
    if initialise then begin
        bUserDecimalSeparatorIsComma := TFormatSettings.create(LOCALE_USER_DEFAULT).DecimalSeparator = ',';
    end;

    //nb val always uses '.' as decimal separator. So if the user uses ',' values, we need to patch
    if bUserDecimalSeparatorIsComma then
        tmp := patchLocalSeparator(trim(s))
    else
        tmp := trim(s);

    val(tmp, f, iCode);
    THelper.removeWarning(f);
    result := iCode = 0;
end;
{$J-}


function isInt(const s : string; const stripSeparators: boolean = true):boolean;
begin
    result := isFloat(trim(s));
    if result then
        result := tofl(s) = trunc(tofl(s));
end;

function    angleRadToCompassAngleDeg(const a : double): double;
begin
    if isnan(a) then exit(nan);

    //convert angle to compass orientation degrees
    result := 90 - a * 180 / pi;
    while result < 0 do
        result := result + 360;
    while result >= 360 do
        result := result - 360;
end;


function angleWrap(const val : double; const radians : boolean = true):double;
{//make sure angle is within range 0 to 2pi}
begin
    if isnan(val) then exit(nan);

    result := val;
    if radians then
    begin
        while result < 0.0 do
            result := result + 2*pi;
        while result >= 2*pi do
            result := result - 2*pi;
    end
    else
    begin
        while result < 0.0 do
            result := result + 360.0;
        while result >= 360.0 do
            result := result - 360.0;
    end;
end;

function integerWrap(const val, target: integer):integer;
begin
    result := val;
    while result < 0 do
        result := result + (target + 1);
    while result > target do
        result := result - (target + 1);
end;


function fmod(const ParaDividend,ParaDivisor : double ) : double;
{floating point modulo}
var
    vQuotient : integer;
begin
    if isnan(ParaDividend) or isnan(ParaDivisor) then exit(nan);

    vQuotient := trunc( ParaDividend / ParaDivisor );
    result := ParaDividend - (vQuotient * ParaDivisor);
end;

function safeRound(const val : double; const errorVal : integer = 0):integer;
begin
    if isnan(val) or IsInfinite(val) or (val < low(integer)) or (val > high(integer)) then
        result := errorVal
    else
        result := round(val);
end;

function safeFloor(const val : double; const errorVal : integer = 0):integer;
begin
    if isnan(val) or IsInfinite(val) or (val < low(integer)) or (val > high(integer)) then
        result := errorVal
    else
        result := floor(val);
end;

function safeCeil(const val : double; const errorVal : integer = 0):integer;
begin
    if isnan(val) or IsInfinite(val) or (val < low(integer)) or (val > high(integer)) then
        result := errorVal
    else
        result := ceil(val);
end;

function    safeTrunc(const val : double; const errorVal : integer = 0):integer;
begin
    if isnan(val) or IsInfinite(val) or (val < low(integer)) or (val > high(integer)) then
        result := errorVal
    else
        result := trunc(val);
end;

function safeRound64(const val : double; const errorVal : int64 = 0) : int64;
begin
    if isnan(val) or IsInfinite(val) or (val < low(int64)) or (val > high(int64)) then
        result := errorVal
    else
        result := round(val);
end;

function safeFloor64(const val : double; const errorVal : int64 = 0) : int64;
begin
    if isnan(val) or IsInfinite(val) or (val < low(int64)) or (val > high(int64)) then
        result := errorVal
    else
        result := floor(val);
end;

function safeCeil64(const val : double; const errorVal : int64 = 0) : int64;
begin
    if isnan(val) or IsInfinite(val) or (val < low(int64)) or (val > high(int64)) then
        result := errorVal
    else
        result := ceil(val);
end;

function safeTrunc64(const val : double; const errorVal : int64 = 0) : int64;
begin
    if isnan(val) or IsInfinite(val) or (val < low(int64)) or (val > high(int64)) then
        result := errorVal
    else
        result := trunc(val);
end;

function safeFloatToStrF(Value: Extended; Format: TFloatFormat; Precision, Digits: Integer; const defaultVal: String = '-'): string;
begin
    if isnan(value) then
        result := defaultVal
    else
        result := FloatToStrF(value, format, precision, digits);
end;

function round1(const val : double):double;
{round to 1 dp}
begin
    if isnan(val) or IsInfinite(val) then
        result := nan
    else
        result := 0.1*round(10.0*val);
end;

function    ceilx(const x,y : double) : double;
{ceil val to multiple of x}
begin
    if isnan(x) or IsInfinite(x) or isnan(y) or IsInfinite(y) or (y = 0) then
        result := nan
    else
        result := ceil(x/y)*y;
end;

function    floorx(const x,y : double) : double;
{floor val to multiple of x}
begin
    if isnan(x) or IsInfinite(x) or isnan(y) or IsInfinite(y) or (y = 0) then
        result := nan
    else
        result := floor(x/y)*y;
end;

function    roundx(const x,y : double) : double;
{round val to multiple of roundTo}
begin
    if isnan(x) or IsInfinite(x) or isnan(y) or IsInfinite(y) or (y = 0) then
        result := nan
    else
        result := round(x/y)*y;
end;

function    ceilx(const x : double; const y : integer) : integer;
begin
    if isnan(x) or IsInfinite(x) or (y = 0) then
        result := 0
    else
        result := ceil(x/y)*y;
end;

function    floorx(const x : double; const y : integer) : integer;
begin
    if isnan(x) or IsInfinite(x) or (y = 0) then
        result := 0
    else
        result := floor(x/y)*y;
end;

function    roundx(const x : double; const y : integer) : integer;
begin
    if isnan(x) or IsInfinite(x) or (y = 0) then
        result := 0
    else
        result := ceil(x/y)*y;
end;


class procedure THelper.ForceReferenceToClass(C: TClass);
begin
    // to remove compiler complaints
end;

class procedure THelper.ForceReferenceToInt(i : integer);
begin
    // to remove compiler complaints
end;

class procedure THelper.PatchInstanceClass(Instance: TObject; NewClass: TClass);
type
    PClass = ^TClass;
begin
    //to replace an object, component on a form etc
    if Assigned(Instance) and
       Assigned(NewClass) and
       NewClass.InheritsFrom(Instance.ClassType) and
       (NewClass.InstanceSize = Instance.InstanceSize) then
    begin
        PClass(Instance)^ := NewClass;
    end;
end;

class procedure THelper.fixForVCLOnclickProblemWithTComboBox;
var
    f : TForm;
begin
    //there has been a problem with the TComboBox, where sometimes clicking on the box will
    //make it open and then close again immediately. This is a very rubbish way of making that not happen,
    //if it is called in the combobox onclick event.
    f := TForm.Create(application);
    try
//        f.AlphaBlend := true;
//        f.AlphaBlendValue := 0;   //so we don't see the form
        f.BorderStyle := bsnone;
        f.Width := 0;
        f.Show;
        f.Hide;
    finally
        f.Free;
    end;
end;

class procedure THelper.append<T>(const val: T; var arr: TArray<T>);
begin
    setlength(arr, length(arr) + 1);
    arr[length(arr) - 1] := val;
end;

class procedure THelper.asyncWait(const timeout: cardinal; onMainThread: TProc);
begin
    Parallel.async(procedure begin
        sleep(timeout);
    end,
    Parallel.TaskConfig.OnTerminated(procedure begin
        onMainThread();
    end));
end;

class function THelper.bytePos(const Pattern: TBytes; const Buffer: PByte; const BufLen, mustBeAfterByte: cardinal): int64;
var
    PatternLength: cardinal;
    i: cardinal;
    j: cardinal;
    OK: boolean;
begin
    result := 0;
    PatternLength := length(Pattern);
    if ((mustBeAfterByte + PatternLength) > BufLen) or (PatternLength = 0) then
        exit;

    for i := max(0, mustBeAfterByte) to BufLen - PatternLength do
        if PByte(Buffer + i)^ = Pattern[0] then
        begin
            OK := true;
            for j := 1 to PatternLength - 1 do
                if PByte(Buffer + i + j)^ <> Pattern[j] then
                begin
                    OK := false;
                    BREAK
                end;
            if OK then
                exit(i);
        end;
end;

class function THelper.resourceFileUnbundle(const targetDir, filename, resID: string; const forceOverwrite: boolean = false): Boolean;
var
    rStream: ISmart<TResourceStream>;
    fStream: ISmart<TFileStream>;
    fname, theDir: string;
begin
    theDir := IncludeTrailingPathDelimiter(targetDir);
    result := false;
    eventLog.log('Unbundle resource file ' + theDir + filename);

    if FileExists(theDir + filename) then
    begin
        if not forceOverwrite then
            Exit
        else if not deletefile(pwidechar(theDir + filename)) then exit;
    end;

    fname := theDir + filename;
    fStream := nil;
    rStream := TSmart<TResourceStream>.create(TResourceStream.Create(hInstance, resID, RT_RCDATA));
    fStream := TSmart<TFileStream>.create(TFileStream.Create(fname, fmCreate));
    fStream.CopyFrom(rStream, 0);
    result := true;
end;

class function THelper.LinkerTimeStamp(): TDateTime;
//var
//  LI: TLoadedImage;
//  filename: string;
begin
    result := PImageNtHeaders(int64(HInstance) + PImageDosHeader(HInstance)^._lfanew)^.FileHeader.TimeDateStamp / SecsPerDay + UnixDateDelta;

  //Win32Check(MapAndLoad(PChar(FileName), nil, @LI, False, True));
  //Result := LI.FileHeader.FileHeader.TimeDateStamp / SecsPerDay + UnixDateDelta;
  //UnMapAndLoad(@LI);
end;

function nextPow2(const val: int64): int64;
var
    rad: integer;
begin
    if val < 1 then exit(1);

    //return next highest power of 2 above an integer
    rad := 1;
    // Double powof2 until >= val
    while( power(2,rad) < val ) do
        rad := rad + 1;
    Result := safetrunc(power(2,rad));
end;

initialization
    isFloat('0.0',true); //initialise these functions with the local decimal separator
    tofl('0.0',true);


end.
