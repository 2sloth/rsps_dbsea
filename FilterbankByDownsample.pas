unit FilterbankByDownsample;

{$B-}

interface

uses
    system.sysutils, system.classes, system.types, system.math,
    DSP, basetypes, spectrum,
    mcklib, eventlogging, SmartPointer, FilterDef;

type

    TFilterbankByDownsample = class(TObject)
      const
        fo: array[0..10] of double = (16, 31, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000);
      var
        bandFilters: array[0..2] of TFilterDef;
        bandFilter: TFilterDef;
        antialiasFilter: TFilterDef;
        fs: double;
        iTop: integer;

        constructor Create(const afs: double);
        destructor Destroy; override;

        function  thirdOctaveBandFreqs: TDoubleDynArray;
        function  octaveBandFreqs: TDoubleDynArray;
        function  thirdOctaveBandLevels(const x: TDoubleDynArray): TDoubleDynArray;
        function  octaveBandLevels(const x: TDoubleDynArray): TDoubleDynArray;
        function  thirdOctaveBands(const x: TDoubleDynArray): TMatrix;
        function  octaveBands(const x: TDoubleDynArray): TMatrix;
    end;


implementation

constructor TFilterbankByDownsample.Create(const afs: double);
var
    i: integer;
    tmpFilt: ISmart<TFilterDef>;
begin
    inherited create;

    fs := afs;

    iTop := -1;
    for i := 0 to length(fo) - 1 do
        if (fo[i] * power(2,1/3) * power(2,1/6)) < (fs / 2) then
            iTop := i;

    if iTop < 0 then
    begin
        bandFilters[0] := nil;
        bandFilters[1] := nil;
        bandFilters[2] := nil;
        antialiasFilter := nil;
    end
    else
    begin
        //NB we do these filters in reverse freq order, as we loop down through the octaves and will reverse the resultant output
        tmpFilt := TSmart<TFilterDef>.create(TDSP.thirdOctbandFilter(fs,fo[iTop] * power(2,1/3)));
        bandFilters[0] := TDSP.cubeFilter(tmpFilt);

        tmpFilt := TSmart<TFilterDef>.create(TDSP.thirdOctbandFilter(fs,fo[iTop]));
        bandFilters[1] := TDSP.cubeFilter(tmpFilt);

        tmpFilt := TSmart<TFilterDef>.create(TDSP.thirdOctbandFilter(fs,fo[iTop] / power(2,1/3)));
        bandFilters[2] := TDSP.cubeFilter(tmpFilt);

        tmpFilt := TSmart<TFilterDef>.create(TDSP.octbandFilter(fs,fo[iTop]));
        bandFilter := TDSP.cubeFilter(tmpFilt);

        tmpFilt := TSmart<TFilterDef>.create(TDSP.LPF(fs,fo[iTop] / power(2,1/2),0.88));
        antialiasFilter := TDSP.cubeFilter(tmpFilt);
    end;
end;

destructor TFilterbankByDownsample.Destroy;
var
    i: integer;
begin
    for i := 0 to length(bandFilters) - 1 do
        bandfilters[i].free;
    bandFilter.Free;
    antialiasFilter.Free;

    inherited;
end;

function  TFilterbankByDownsample.thirdOctaveBandFreqs: TDoubleDynArray;
var
    i: integer;
begin
    setlength(result,3*(iTop + 1));
    for i := 0 to iTop do
    begin
        result[3*i    ] := fo[i] / power(2,1/3);
        result[3*i + 1] := fo[i];
        result[3*i + 2] := fo[i] * power(2,1/3);
    end;
end;

function  TFilterbankByDownsample.octaveBandFreqs: TDoubleDynArray;
var
    i: integer;
begin
    setlength(result,iTop + 1);
    for i := 0 to iTop do
        result[i] := fo[i];
end;

function TFilterbankByDownsample.thirdOctaveBandLevels(const x: TDoubleDynArray): TDoubleDynArray;
var
    i,j: integer;
    y: TDoubleDynArray;
begin
    setlength(result,3*(iTop + 1));
    setlength(y,length(x));
    for i := 0 to length(x) - 1 do
        y[i] := x[i];
    for j := 0 to iTop do
    begin
        for i := 0 to length(bandFilters) - 1 do
            result[3*(iTop + 1) - (j * 3 + i) - 1] := 20*log10(TDSP.rms(TDSP.applyFilter(bandFilters[i],y)));
        y := TDSP.downsampleBy2(y,antialiasFilter);
    end;
end;

function TFilterbankByDownsample.octaveBandLevels(const x: TDoubleDynArray): TDoubleDynArray;
var
    j: integer;
    y: TDoubleDynArray;
begin
    setlength(result,iTop + 1);
    setlength(y,length(x));
    for j := 0 to length(x) - 1 do
        y[j] := x[j];
    for j := 0 to iTop do
    begin
        result[iTop - j] := 20*log10(TDSP.rms(TDSP.applyFilter(bandFilter,y)));
        y := TDSP.downsampleBy2(y,antialiasFilter);
    end;
end;

function  TFilterbankByDownsample.thirdOctaveBands(const x: TDoubleDynArray): TMatrix;
var
    i: integer;
    j: integer;
    y: TDoubleDynArray;
begin
    setlength(result,3*(iTop + 1));
    setlength(y,length(x));
    for i := 0 to length(x) - 1 do
        y[i] := x[i];
    for j := 0 to iTop do
    begin
        for i := 0 to length(bandFilters) - 1 do
        begin
            result[3*(iTop + 1) - (j * 3 + i) - 1] := TDSP.applyFilter(bandFilters[i],y);
        end;
        y := TDSP.downsampleBy2(y,antialiasFilter);
        TDSP.extendBy1Sample(y);
    end;

    for j := 0 to iTop - 1 do
        for i := 0 to j do
        begin
            result[3*i    ] := TDSP.upsampleBy2(result[3*i    ]);
            result[3*i + 1] := TDSP.upsampleBy2(result[3*i + 1]);
            result[3*i + 2] := TDSP.upsampleBy2(result[3*i + 2]);
        end;

    //make sure all the vectors are the same length
    for j := 0 to iTop - 1 do
    begin
        setlength(result[3*j    ],length(x));
        setlength(result[3*j + 1],length(x));
        setlength(result[3*j + 2],length(x));
    end;
end;

function  TFilterbankByDownsample.octaveBands(const x: TDoubleDynArray): TMatrix;
var
    i,j: integer;
    y: TDoubleDynArray;
begin
    setlength(result,iTop + 1);
    setlength(y,length(x));
    for j := 0 to length(x) - 1 do
        y[j] := x[j];

    //TODO there will also be a lower limit to how many times we can downsample
    for j := 0 to iTop do
    begin
        result[iTop - j] := TDSP.applyFilter(bandFilter,y);
        y := TDSP.downsampleBy2(y,antialiasFilter);
        TDSP.extendBy1Sample(y); //extend the downsampled signal so it is longer than the prev octave
    end;
    //upsample each of those signals by 2 till they get to the correct octave
    for j := 0 to iTop - 1 do
        for i := 0 to j do
            result[i] := TDSP.upsampleBy2(result[i]);

    //make sure all the vectors are the same length
    for j := 0 to iTop - 1 do
        setlength(result[j],length(x));
end;

end.
