unit GLTypes;

interface

uses baseTypes, dbSeaObject;

type
    TMovingGraphicsObjectReturn = Class(TObject)
    public
        aObject : TdBSeaObject;

        constructor Create;
        destructor Destroy; Override;
        procedure clear;
    End;


implementation

constructor TMovingGraphicsObjectReturn.Create;
begin
    inherited;

    self.clear;
end;

destructor TMovingGraphicsObjectReturn.Destroy;
begin
    inherited;
end;

procedure TMovingGraphicsObjectReturn.clear;
begin
    self.aObject := nil;
end;


end.
