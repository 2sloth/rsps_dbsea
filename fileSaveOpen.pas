unit fileSaveOpen;

interface
uses syncommons, eventlogging, system.sysutils, XMLIntf, xmldom, system.Classes,
    vcl.dialogs, vcl.graphics,
    system.UITypes, vcl.forms, MSXML, ComObj,
    msxmldom, XMLDoc, rtti, zlib, math, strutils,
    dbSeaConstants, mcklib, basetypes, helperFunctions, dbseaoptions, HASPSecurity,
    globalsUnit, GLWindow, options, EditSetupFrame, EditWaterFrame,
    synlz, bathymetry, memoryStreamHelper, SmartPointer, problemContainer;

type
    TFileSaveOpen = class(TObject)
    const
        fileGoodStartText = '<UnderwaterAcoustics';
        sBeginCompressedData    = '___Begin compressed data___';
        sEndCompressedData = '___End compressed data___';
    private
        class function  decompressSynLZ(const compressedBytes: TBytes): TBytes;
        class function  SynLZDecompressStr(const s: TBytes): string;
        class function  searchAndDecompress(const sStart,sEnd: string; const MemStream: TMemoryStream; const sSaveVersion: string): string;
    public
        class function  serialise(const aContainer: TProblemContainer; const aUWAOpts: TdBSeaOptions): string;
        class function  FileSaveWithFilename(const savefilename: TFileName; const aContainer: TProblemContainer; const aUWAOpts: TdBSeaOptions; const includeLevels: boolean): string;
        class function  FileOpenWithFilename(const openFileName: TFilename; const aContainer: TProblemContainer; const aUWAOpts: TdBSeaOptions; const doUIUpdates: boolean): string;
        class function  FileStripResults(const openFileName, saveFilename: TFilename): string;
    end;

implementation

uses main,
    EditSolveFrame, EditSourceFrame, ImportedLocationUnit;

class function TFileSaveOpen.FileSaveWithFilename(const savefilename: TFileName; const aContainer: TProblemContainer; const aUWAOpts: TdBSeaOptions; const includeLevels: boolean): string;
var
    s           : string;
    MemStream   : ISmart<TMemorystream>;
    log         : ISynLog;
begin
    { saving all the relevant data from our local variables to disk, in xml format
      unfortunately there is a fair amount of special cases that need to be
      worked through. the rtti methods used can pretty much automatically do
      integers, floats, strings etc. but for classes, such as TProbe, TPos3 and so on
      we need to do something specific for each one. it would maybe be possible to
      automatically write each as a raw string of bytes but then it wouldn't be human
      readable, which is something i want to preserve.

      i think the thing that is stopping us from automatically creating subnodes for each
      of these, is that we get back a stringlist containing the 3 fields for each
      variable: name, type, value. we would instead need to be able to bring back for each
      either the 3 strings, or a subnode. maybe we could do this if the getvarnames
      was also done one at a time, and would either return strings or nodes.

      also i'm not saving every single field, those that are not necessary and also
      would require extra coding are not included. various things can be regenerated
      when we load, eg gdepths

      The depths and calculated results are saved, but they are zipped to save space. Also
      the xml creation functions cannot handle large amounts of data (takes approx 10x mem space
      as the variables themselves), so saving results in plain text was causing crashes even
      for medium sized files.

      Something to look at would be to zip the whole file, as currently we have
      a part in plain text and part that is zipped, and the plain text part
      becomes a bit useless, because the files is no longer interpreted as unicode

      also  keep track of the 'savefileversion', so that if later on we change names of
      fields, we can try and provide some backwards compatibility

      colors are written as B*256*256 + G*256 + R }



    //todo all the stringlist creation is leaking memory. replace with procedure call

    log := TSynLogLevs.Enter(self,'FileSaveWithFilename');
    log.log(sllInfo,'Saving file at path %',[saveFilename]);

    result := '';

    if runningMode = rmDemo then
        Exit('Demo version: saving is disabled');

    if runningMode = rmTrial then
    begin
        //showmessage('Trial version: saving is disabled');
        //Exit;
    end;

    {$IF not Defined(DEBUG) and not Defined(LITE)}
    if (runningMode = rmNormalKey) and not theHASPSecurity.HASPFormWhileRunningChecks then
        exit('Valid HASP key not found, saving disabled');
    {$ENDIF}

    if length(savefilename) = 0 then
        exit;

    if assigned(framSolve) then
    begin
        framSolve.lbSolving.Caption := 'Saving file...';
        framSolve.lbSolving.Visible := true;
    end;

    eventLog.log('Save file ' + savefilename + '   save file version ' + saveFileVersion);

    Screen.Cursor := crHourglass;
    GLNoDraw := true; //so GL won't draw
    Application.ProcessMessages();
    try
        MemStream := TSmart<TMemorystream>.create(TMemorystream.Create());
        s := serialise(aContainer, aUWAOpts);
        aContainer.serialisedOnInit := s;
        MemStream.myWrite(s);

        MemStream.myWrite(#13#10 + sBeginCompressedData + #13#10);
        aContainer.writeCompressedBathyToStream(MemStream);
        if includeLevels then
            aContainer.writeCompressedSourceLevels(memStream);
        MemStream.myWrite(#13#10 + sEndCompressedData + #13#10);

        MemStream.SaveToFile(savefilename);

    finally
        Screen.Cursor := crDefault;
        GLNoDraw := false;

        if assigned(framSolve) then
            framSolve.lbSolving.Visible := false;
    end;
end;

//class function TFileSaveOpen.getSaveVersion(const filetext: string): string;
//var
//    LDoc           : IXMLDocument;
//    sel            : IDOMNodeSelect;
//    iNode          : IDOMNode;
//    childnode      : IDOMNode;
//begin
//    LDoc := TXMLDocument.Create(application);
//    LDoc.LoadFromXML(fileText);
//
//    sel := LDoc.DOMDocument as IDOMNodeSelect;
//
//    {check if this looks like it might be an openable file}
//    iNode := sel.selectNode('/UnderwaterAcoustics');
//    if not assigned(iNode) then
//    begin
//        showmessage('Unable to find file header, check file type');
//        Exit;
//    end;
//
//    childNode := iNode.attributes.getNamedItem('SaveFileVersion');
//    if assigned(childnode) then
//        result := childNode.nodeValue
//    else
//        result := '';
//end;

class function TFileSaveOpen.FileOpenWithFilename(const openFileName: TFilename; const aContainer: TProblemContainer; const aUWAOpts: TdBSeaOptions; const doUIUpdates: boolean): string;
var
    fileText       : String;
    xml            : IXMLDOMDocument;
    LDoc           : IXMLDocument;
    sel            : IDOMNodeSelect;
    iNode          : IDOMNode;
    childnode      : IDOMNode;
    memStream      : ISmart<TMemorystream>;
    aPos1          : int64;
    //srchStr        : string;
    sUncompressed  : string;
    sSaveVersion   : string;
    sProgVersion   : string;
    bmp            : TBitmap;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'FileOpenWithFilename');
    log.log(sllInfo,'Opening file at path %',[openfilename]);

    result := '';

    if runningMode = rmDemo then
        exit('dBSea demo mode. Opening files is disabled');

    {check if the file exists}
    if not fileExists(openFileName) then
        Exit('Unable to open file or file does not exist');

    eventLog.log('Open file ' + openFileName);

    {set GLNoDraw so that GLWin won't try to draw
    during this section. set it back to false afterwards, in the 'finally' section}
    GLNoDraw := true;
    if doUIUpdates then
        framSource.clearSourceThreshold;
    framWater.chartsShouldUpdate := false;

    Screen.Cursor := crHourglass;
    memStream := TSmart<TMemorystream>.create(TMemoryStream.create);
    try
        if doUIUpdates then
        begin
            DefaultSavePath := ExtractFilePath(openFileName);
            CurrentSaveFile := openFileName;
            currentProjectName := openFileName;
            mainform.updateMainCaption;
        end;

        {get main file text via Memstream and put it into a string}
        memStream.LoadFromFile(openFileName);
        //srchStr := sBeginCompressedData;
        //aPos1 := 0;
        aPos1 := Pos(sBeginCompressedData, PChar(memStream.Memory));
        if aPos1 = 0 then
        begin
            {no compressed data found, read the whole file}
            //fileText := ReadFileToString(openFileName)
            setlength(fileText,safeceil(memStream.Size/sizeof(char)));
            memStream.Position := 0;
            memStream.Read(fileText[1],memStream.Size);
        end
        else
        begin
            {read up to the point where the pointer was found}
            setlength(fileText,aPos1 - 1);
            memStream.Position := 0;
            memStream.Read(fileText[1],(aPos1 - 1)*sizeof(Char));
        end;

        if ansicomparetext(fileGoodStartText,AnsiLeftStr(fileText,length(fileGoodStartText))) <> 0  then
        begin
            showmessage('Unable to open file, check file type');
            Exit;
        end;

        {trial xml parse. here we are creating an extra object just for parsing, should check
        if there is a better way to do this}
        xml := CreateOleObject('Microsoft.XMLDOM') as IXMLDOMDocument;
        xml.async := False;
        xml.loadXML(fileText);
        if xml.parseError.errorCode <> 0 then
        begin
            showmessage('Unable to parse file, file format appears faulty');
            Exit;
        end;

        LDoc := TXMLDocument.Create(application);
        LDoc.LoadFromXML(fileText);

        sel := LDoc.DOMDocument as IDOMNodeSelect;

        {check if this looks like it might be an openable file}
        iNode := sel.selectNode('/UnderwaterAcoustics');
        if not assigned(iNode) then
        begin
            showmessage('Unable to find file header, check file type');
            Exit;
        end;

        childNode := iNode.attributes.getNamedItem('SaveFileVersion');
        if assigned(childnode) then
            sSaveVersion := childNode.nodeValue
        else
            sSaveVersion := '';
        childNode := iNode.attributes.getNamedItem('ProgramVersion');
        if assigned(childnode) then
            sProgVersion := childNode.nodeValue
        else
            sProgVersion := '';

        if (aPos1 = 0) or (isFloat(sSaveVersion) and (tofl(sSaveVersion) >= 7.0)) then
            sUncompressed := ''
        else
            {< 7.0, the compressed bathy string and compressed source strings}
            sUncompressed := TFileSaveOpen.searchAndDecompress(sBeginCompressedData + #13#10,sEndCompressedData,memStream,sSaveVersion);

        {have all the levels now so read to the aContainer: this is done first mostly so that all subsequently
        created sources etc will have the correct bw and freq ranges}
        iNode := sel.selectNode('/UnderwaterAcoustics/container');
        if assigned(iNode) then
            aContainer.readDataFromXMLNode(iNode, sUncompressed, sSaveVersion, memStream);

        {populate the aUWAOpts section}
        iNode := sel.selectNode('/UnderwaterAcoustics/UWAOpts');
        if assigned(iNode) then
            aUWAOpts.readDataFromXMLNode(iNode, sSaveVersion);

        //------------------------------------all data loaded, do all the necessary updating-----------------------------------------
        if not doUIUpdates then
            exit;

        {get the levels from the source and use that to get theRA levels}
        aUWAOpts.levelLimits.DeleteUserSetLimits;

        {update levels. }
        aUWAOpts.updateCMaxCMinandLevelColors();
        aUWAOpts.setVisibility(ord(veEndpoints),false);//don't want to see these just after opening file
        if assigned(GLForm) then
        begin
            GLForm.didChangeVisibility(ord(veEndpoints));
            if aContainer.image.valid then
            begin
                bmp := aContainer.image.toBMP;
                try
                    GLForm.setImageOverlayTexture(bmp);
                    GLForm.setRedrawBathymetry;
                finally
                    bmp.free;
                end;
            end;
        end;
        mainform.UpdateVisibilityMenuItems(aUWAOpts.ComponentVisibility);

        mainform.updateSelectBoxesAndChartsAfterScenarioChange;

        mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Setup);
        importedLocation.clear;
        importedLocation.setZoneWith(aContainer.geoSetup.utmZone);
        if importedLocation.hasUTMData then
        begin
            importedLocation.dx := aContainer.geoSetup.worldScale / max(aContainer.bathymetry.iMax, aContainer.bathymetry.jMax);
            importedLocation.dy := aContainer.geoSetup.worldScale / max(aContainer.bathymetry.iMax, aContainer.bathymetry.jMax);
        end;
        framSetup.EditBoxesInit();

        {add this filename to the recent files list, and update the menu}
        mainform.RecentFilesListAdd(openFileName);

        mainform.initXSecSaveRecallMenus();

        mainform.updateMainCaption();

        aContainer.serialisedOnInit := serialise(aContainer, UWAOpts);

    finally
        GLNoDraw := false;
        framWater.chartsShouldUpdate := true;
        //framWater.SSPSelectBoxChange(nil);
        framWater.copyCProfile;
        framWater.updateSSPChart;

        Screen.Cursor := crDefault;
    end;

    {now that ApplicationInitialising := false; we can redraw GL stuff}
    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := false;
        GLForm.setRedrawResults;
        GLForm.invalidateBathyTexture;
        GLForm.setRedrawBathymetry;
        GLForm.force2DrawNow;
    end;
end;

class function TFileSaveOpen.FileStripResults(const openFileName, saveFilename: TFilename): string;
var
    tmpContainer: ISmart<TProblemContainer>;
    tmpOpts: ISmart<TdBSeaOptions>;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'FileStripResults');

    tmpContainer := TSmart<TProblemContainer>.create(TProblemContainer.create);
    tmpOpts := TSmart<TdBSeaOptions>.create(TdBSeaOptions.create);

    FileOpenWithFilename(openFileName, tmpContainer, tmpOpts, false);
    FileSaveWithFilename(saveFilename, tmpContainer, tmpOpts, false);
end;

class function TFileSaveOpen.searchAndDecompress(const sStart,sEnd: string; const MemStream: TMemoryStream; const sSaveVersion: string): string;
var
    aPos1,aPos2: int64;
    aBytes: TBytes;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'searchAndDecompress');

    Result := '';
    aPos1 := THelper.BytePos(TEncoding.Unicode.GetBytes(sStart), MemStream.Memory, MemStream.Size, 0);
    aPos1 := aPos1 + length(sStart)*sizeof(Char);
    aPos2 := THelper.BytePos(TEncoding.Unicode.GetBytes(sEnd), MemStream.Memory, MemStream.Size, 0);
    aPos2 := aPos2 - 2*sizeof(Char);
    if aPos2 <= aPos1 then Exit;

    setlength(aBytes,aPos2 - aPos1);
    MemStream.Position := aPos1;
    MemStream.Read(aBytes[0],aPos2 - aPos1);

    if length(aBytes) = 0 then
        result := ''
    else
    begin
        if isfloat(sSaveVersion) and (tofl(sSaveVersion) >= 7.0) then
            result := SynLZDecompressStr(aBytes)
        else
            result := ZDecompressStr(aBytes);
    end;
end;

class function TFileSaveOpen.serialise(const aContainer: TProblemContainer; const aUWAOpts: TdBSeaOptions): string;
var
    LDoc        : IXMLDocument;
    baseNode    : IXMLNode;
begin
    LDoc := TXMLDocument.Create(application);
    LDoc.Active := true;

     { Define document content. }
    LDoc.DocumentElement := LDoc.CreateNode('UnderwaterAcoustics', ntElement, '');
    LDoc.DocumentElement.Attributes['SaveFileVersion'] := SaveFileVersion;
    LDoc.DocumentElement.Attributes['ProgramVersion'] := majorVersion(application.ExeName);
    LDoc.DocumentElement.Attributes['ProgramVersionFull'] := fileVersion(application.ExeName);
    LDoc.DocumentElement.Attributes['CompressionAlgorithm'] := CompressionAlgorithm;
    case runningMode of
        rmBlocked  : LDoc.DocumentElement.Attributes['ProgramRunningMode'] := 'Blocked';
        rmDemo     : LDoc.DocumentElement.Attributes['ProgramRunningMode'] := 'Demo';
        rmTrial    : LDoc.DocumentElement.Attributes['ProgramRunningMode'] := 'Trial';
        rmNormalKey: LDoc.DocumentElement.Attributes['ProgramRunningMode'] := 'NormalKey';
        rmLite     : LDoc.DocumentElement.Attributes['ProgramRunningMode'] := 'Lite';
    end;

    LDoc.DocumentElement.Attributes['ProgramDebugStatus'] := {$IFDEF DEBUG}'Debug'; {$ELSE} 'Release'; {$ENDIF}
    LDoc.DocumentElement.Attributes['ProgramBuild'] := {$IF Defined(NON_MDA)}'External'; {$ELSEIF defined(LITE)} 'Lite'; {$ELSE} 'Internal'; {$ENDIF}

    //write the TProblemDescription, including all sub-objects
    baseNode := LDoc.DocumentElement.AddChild('container');
    if assigned(baseNode) then
        aContainer.writeDataToXMLNode(baseNode);

    {write all the options from aUWAOpts}
    baseNode := LDoc.DocumentElement.AddChild('UWAOpts');
    if assigned(baseNode) then
        aUWAOpts.writeDataToXMLNode(baseNode);

    LDoc.SaveToXML(result);
end;

class function TFileSaveOpen.SynLZDecompressStr(const s: TBytes): string;
var
    BOut: TBytes;
begin
    BOut := decompressSynLZ(s);
    Result := TEncoding.Unicode.GetString(BOut);
end;

class function TFileSaveOpen.decompressSynLZ(const compressedBytes: TBytes): TBytes;
begin
    if length(compressedBytes) = 0 then Exit;

    setlength(result, SynLZdecompressdestlen(@compressedBytes[0]));
    SynLZdecompress1pas(@compressedBytes[0], length(compressedBytes), @result[0]);
end;

//class function TFileSaveOpen.SynLZCompressStr(const s: string): TBytes;
//var
//    b: TBytes;
//    compressedSize: integer;
//begin
//    b := TEncoding.Unicode.GetBytes(s);
//    setlength(result, SynLZcompressdestlen(Length(b)));
//    compressedSize := SynLZcompress1pas(@b[0], Length(b), @result[0]);
//    setlength(result , compressedSize);
//end;





end.
