unit SlackIntegration;

interface

uses system.SysUtils, system.classes,
    IdSSLOpenSSL, IdHTTP,
    stringshelper,
    smartpointer, helperFunctions, otlparallel;

type

    TSlackIntegration = class(Tobject)
    private
    const
        icon = ':hamster:';
        slackURL = 'https://hooks.slack.com/services/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';
        zapierURL = 'https://hooks.zapier.com/hooks/catch/1856286/taz52h/';

        class procedure genericSend(const body, url: string);
    public
    var
        class procedure sendToSlack(const body: string);
        class procedure logToGoogleSheet(const error: string);
    end;

implementation

class procedure TSlackIntegration.genericSend(const body, url: string);
var
    HTTP: TIdHTTP;
    RequestBody: TStream;
    localBody, localurl: string;
begin
    localBody := body; //for variable capture in anonymous method
    localurl := url;

//    Parallel.Async(procedure
//    var
//        HTTP: TIdHTTP;
//        RequestBody: TStream;
//    begin
        RequestBody := nil;
        HTTP := TIdHTTP.Create;
        try
            http.IOHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
            RequestBody := TStringStream.create(localbody);
            try
                HTTP.Post(localurl, RequestBody);
            except on E: EIdHTTPProtocolException do
            end;
        finally
            RequestBody.Free;
            HTTP.Free;
        end;
//    end);
end;

class procedure TSlackIntegration.logToGoogleSheet(const error: string);
var
    sl: ISmart<TStringList>;
    //response: string;
begin
    sl := TSmart<TStringList>.create(TStringList.create());
    sl.add('"time": "' + formatdatetime('yyyy-mm-dd hh:nn:ss', now) + '"');
    sl.add('"sheet": "error"');
    sl.add('"user": "' + 'dBSea ' + THelper.GetLocalComputerName + '"');
    sl.add('"body": "' + error + '"');
    genericSend(sl.jsonJoin(), zapierURL);
    //response := genericSend(sl.jsonJoin(), zapierURL);
    //result := response.Contains('"success"');

    {
  "status": "success",
  "attempt": "58507912-9cd2-45e7-964c-80baa00c0e10",
  "id": "a76aba94-d9cc-4a83-a4dc-4e7a7736168a",
  "request_id": "9d77dkft9Jw1Wo4D"
}
end;

class procedure TSlackIntegration.sendToSlack(const body: string);
var
    sl: ISmart<TStringList>;
begin
    sl := TSmart<TStringList>.create(TStringList.create());
    sl.add('"username": "' + 'dBSea ' + THelper.GetLocalComputerName + '"');
    sl.add('"icon_emoji": "' + icon + '"');
    sl.add('"text": "' + body + '"');
    genericSend(sl.jsonJoin(), slackURL);
    //result := genericSend(sl.jsonJoin(), slackURL) = 'ok';
end;

end.
