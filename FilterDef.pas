unit FilterDef;

interface

uses
    system.sysutils, system.classes, system.math, system.types,
    fmx.dialogs, fmx.stdctrls, fmx.forms,
    smartPointer, baseTypes, utextendedx87, helperFunctions;

type

    {$Define USEEXTENDED87}
    BigFloat = {$IF Defined(CPU32BITS)}
        Extended
        {$ELSEIF defined(USEEXTENDED87)}
        textendedx87
        {$ELSE}
        extended // alias for double on 64 bit compiler
        {$ENDIF};
    TEVect = array of BigFloat;

    TFilterDef = class(TObject)
        b,a: TEVect;
        oneOverA0: BigFloat;
        hiA, hiB: integer;

        constructor Create(const order: integer);

        function  taps(): integer;
        procedure normalise();
        procedure setup;
    end;
    TFilterDefSmart = TSmart<TFilterDef>;
    IFilterDefSmart = ISmart<TFilterDef>;

implementation

constructor TFilterDef.Create(const order : integer);
begin
    inherited create;

    setlength(b,order);
    setlength(a,order);
end;


function TFilterDef.taps: integer;
begin
    result := max(length(a),length(b));
end;

procedure TFilterDef.setup;
begin
    oneOverA0 := 1/a[0];
    hiA := length(a) - 1;
    hiB := length(b) - 1;
end;

procedure TFilterDef.normalise;
var
    i: integer;
begin
    for i := 1 to length(a) - 1 do
        a[i] := a[i] / a[0];

    for i := 0 to length(b) - 1 do
        b[i] := b[i] / a[0];

    a[0] := 1;
end;

end.
