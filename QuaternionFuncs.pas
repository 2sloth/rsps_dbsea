unit QuaternionFuncs;

interface

uses system.math, system.math.vectors, dglOpenGL;

type

  TQuaternionFuncs = class(TObject)
      class function toEulerAngles(const q: TQuaternion3D): TPoint3D;
      class function fromEulerAngles(const e: TPoint3D): TQuaternion3D;
      class function toGLMatrixf(const q: TQuaternion3D): TGLMatrixf4;
  end;

implementation

class function TQuaternionFuncs.fromEulerAngles(const e: TPoint3D): TQuaternion3D;
var
	t0, t1, t2, t3, t4, t5: double;
begin
	t0 := cos(e.z * 0.5);
	t1 := sin(e.z * 0.5);
	t2 := cos(e.y * 0.5);
	t3 := sin(e.y * 0.5);
	t4 := cos(e.x * 0.5);
	t5 := sin(e.x * 0.5);

	result.RealPart := t0 * t2 * t4 + t1 * t3 * t5;
	result.ImagPart.x := t0 * t3 * t4 - t1 * t2 * t5;
	result.ImagPart.y := t0 * t2 * t5 + t1 * t3 * t4;
	result.ImagPart.z := t1 * t2 * t4 - t0 * t3 * t5;
end;

class function TQuaternionFuncs.toEulerAngles(const q: TQuaternion3D): TPoint3d;
var
    ysqr, t0, t1, t2, t3, t4: double;
begin
	ysqr := system.sqr(q.ImagPart.y);
	t0 := -2.0 * (ysqr + q.ImagPart.z * q.ImagPart.z) + 1.0;
	t1 := +2.0 * (q.ImagPart.x * q.ImagPart.y - q.RealPart * q.ImagPart.z);
	t2 := -2.0 * (q.ImagPart.x * q.ImagPart.z + q.RealPart * q.ImagPart.y);
	t3 := +2.0 * (q.ImagPart.y * q.ImagPart.z - q.RealPart * q.ImagPart.x);
	t4 := -2.0 * (system.sqr(q.ImagPart.x) + ysqr) + 1.0;

	t2 := max(-1.0, min(1.0, t2));
	result.x := arcsin(t2);
	result.y := arctan2(t3, t4);
    result.z := arctan2(t1, t0);
end;

class function TQuaternionFuncs.toGLMatrixf(const q: TQuaternion3D): TGLMatrixf4;
var
    m: TMatrix3D absolute result; // TMatrix3D and TGLMatrixf4 have the same structure
begin
    m := q; //use implicit conversion
end;

end.
