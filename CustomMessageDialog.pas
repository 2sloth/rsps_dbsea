unit CustomMessageDialog;

interface

uses system.SysUtils, system.classes,
    vcl.forms, vcl.dialogs, vcl.stdctrls, vcl.controls,
    SQLite3, sqlitetable3,
    dbseaconstants, mcklib, eventlogging, SmartPointer;

type
    TButtonInfo = record
        MsgDlgBtn: TMsgDlgBtn;
        Caption: string;
        width: integer;
    end;

const
    ModalResults: array [TMsgDlgBtn] of Integer = (
      mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll,
      mrYesToAll, 0, mrClose);

    function ButtonInfo(MsgDlgBtn: TMsgDlgBtn; const Caption: string; const width: integer = 60): TButtonInfo;
    function CustomMessageDlg(const aMsg: string; aDlgType: TMsgDlgType; const Buttons: array of TButtonInfo; aDefault: TMsgDlgBtn): TModalResult;

implementation

function ButtonInfo(MsgDlgBtn: TMsgDlgBtn; const Caption: string; const width: integer = 60): TButtonInfo;
begin
    Result.MsgDlgBtn := MsgDlgBtn;
    Result.Caption := Caption;
    result.width := width;
end;

function FindDialogButton(Form: TForm; MsgDlgBtn: TMsgDlgBtn): TButton;
var
    i: Integer;
    Component: TComponent;
begin
    for i := 0 to Form.componentCount - 1 do
    begin
        Component := Form.Components[i];
        if Component is TButton then
        begin
            if TButton(Component).ModalResult = ModalResults[MsgDlgBtn] then
            begin
                Result := TButton(Component);
                exit;
            end;
        end;
    end;
    Result := nil;
end;

function CustomMessageDlg(
  const aMsg: string;
  aDlgType: TMsgDlgType;
  const Buttons: array of TButtonInfo;
  aDefault: TMsgDlgBtn
  ): TModalResult;
var
    i: Integer;
    MsgDlgButtons: TMsgDlgButtons;
    vDlg: TForm;
begin
    MsgDlgButtons := [];
    for i := low(Buttons) to high(Buttons) do
    begin
        Assert(not(Buttons[i].MsgDlgBtn in MsgDlgButtons)); // assert uniqueness
        Include(MsgDlgButtons, Buttons[i].MsgDlgBtn);
    end;
    vDlg := CreateMessageDialog(aMsg, aDlgType, MsgDlgButtons);
    try
        for i := low(Buttons) to high(Buttons) do
        begin
            FindDialogButton(vDlg, Buttons[i].MsgDlgBtn).Caption := Buttons[i].Caption;
            FindDialogButton(vDlg, Buttons[i].MsgDlgBtn).Width := buttons[i].width;
        end;
        //vDlg.Position := poDefaultPosOnly;
        Result := vDlg.ShowModal;
    finally
        vDlg.Free;
    end;
end;

end.
