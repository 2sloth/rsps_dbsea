unit HighOrder;

interface

uses
    system.sysutils,
    generics.collections,
    SmartPointer;

type

    T_ = class
    public
        class function  reduce<T,TOut>(const AData: TArray<T>; const AFunc: TFunc<TOut,T,TOut>; const AInit: TOut): TOut; overload;
        class function  reduce<T,TOut>(const AData: TEnumerable<T>; const AFunc: TFunc<TOut,T,TOut>; const AInit: TOut): TOut; overload;
        class function  reduce<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,T,T>; const AInit: T): T; overload;
        class function  none<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): boolean; overload;
        class function  none<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): boolean; overload;
        class function  any<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): boolean; overload;
        class function  any<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): boolean; overload;
        class function  all<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): boolean; overload;
        class function  all<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): boolean; overload;
        class function  map<T,TOut>(const AData: TArray<T>; const AFunc: TFunc<T,TOut>): TArray<TOut>; overload;
        class function  map<T,TOut>(const AData: TEnumerable<T>; const AFunc: TFunc<T,TOut>): TList<TOut>; overload;
        class function  filter<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): TArray<T>; overload;
        class function  filter<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): TList<T>; overload;
        class function  filterToArray<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): TArray<T>; overload;
        class function  join<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,string>): string; overload;
        class procedure each<T>(const AData: TArray<T>; const AFunc: TProc<T>); overload;
        class procedure each<T>(const AData: TEnumerable<T>; const AFunc: TProc<T>); overload;
    end;

implementation

class function T_.reduce<T,TOut>(const AData: TArray<T>; const AFunc: TFunc<TOut,T,TOut>; const AInit: TOut): TOut;
var
    Current: T;
begin
    result := AInit;
    for Current in AData do
        result := AFunc(result, Current);
end;

class function T_.reduce<T,TOut>(const AData: TEnumerable<T>; const AFunc: TFunc<TOut,T,TOut>; const AInit: TOut): TOut;
var
    Current: T;
begin
    result := AInit;
    for Current in AData do
        result := AFunc(result, Current);
end;

class function T_.reduce<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,T,T>; const AInit: T): T;
var
    Current: T;
begin
    result := AInit;
    for Current in AData do
        result := AFunc(result, Current);
end;

class function  T_.none<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): boolean;
var
    Current: T;
begin
    for Current in AData do
        if AFunc(Current) then exit(false);
    result := true;
end;

class function T_.none<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): boolean;
var
    Current: T;
begin
    for Current in AData do
        if AFunc(Current) then exit(false);
    result := true;
end;

class function T_.any<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): boolean;
var
    Current: T;
begin
    for Current in AData do
        if AFunc(Current) then exit(true);
    result := false;
end;

class function T_.any<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): boolean;
var
    Current: T;
begin
    for Current in AData do
        if AFunc(Current) then exit(true);
    result := false;
end;

class function T_.all<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): boolean;
var
    Current: T;
begin
    for Current in AData do
        if not AFunc(Current) then exit(false);
    result := true;
end;

class function T_.all<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): boolean;
var
    Current: T;
begin
    for Current in AData do
        if not AFunc(Current) then exit(false);
    result := true;
end;

class function T_.filter<T>(const AData: TArray<T>; const AFunc: TFunc<T,boolean>): TArray<T>;
var
    Current: T;
    list: ISmart<TList<T>>;
    i: integer;
begin
    list := TSmart<TList<T>>.create(TList<T>.Create);
    for Current in AData do
        if aFunc(current) then
            list.Add(Current);

    setlength(result, list.Count);
    for i := 0 to list.count - 1 do
        result[i] := list[i];
end;

class function T_.filter<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T, boolean>): TList<T>;
var
    Current: T;
begin
    result := TList<T>.Create;
    for Current in AData do
        if aFunc(current) then
            result.Add(Current);
end;

class function  T_.filterToArray<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,boolean>): TArray<T>;
var
    Current: T;
    list: ISmart<TList<T>>;
    i: integer;
begin
    list := TSmart<TList<T>>.create(TList<T>.Create);
    for Current in AData do
        if aFunc(current) then
            list.Add(Current);

    setlength(result, list.Count);
    for i := 0 to list.count - 1 do
        result[i] := list[i];
end;

class function T_.join<T>(const AData: TEnumerable<T>; const AFunc: TFunc<T,string>): string;
var
    current: T;
begin
    result := '';
    for current in AData do
        result := result + AFunc(current) + ',';
    setlength(result, length(result) - 1);
end;

class function T_.map<T,TOut>(const AData: TArray<T>; const AFunc: TFunc<T,TOut>): TArray<TOut>;
var
    Current: T;
    i: integer;
begin
    setlength(result, length(adata));
    for i := 0 to length(AData) - 1 do
        result[i] := AFunc(adata[i]);
end;

class function T_.Map<T,TOut>(const AData: TEnumerable<T>; const AFunc: TFunc<T,TOut>): TList<TOut>;
var
    Current: T;
begin
    result := TList<TOut>.Create;
    for Current in AData do
        result.Add(AFunc(Current));
end;

class procedure T_.each<T>(const AData: TArray<T>; const AFunc: TProc<T>);
var
    Current: T;
begin
    for Current in AData do
        AFunc(Current);
end;

class procedure T_.each<T>(const AData: TEnumerable<T>; const AFunc: TProc<T>);
var
    Current: T;
begin
    for Current in AData do
        AFunc(Current);
end;


end.
