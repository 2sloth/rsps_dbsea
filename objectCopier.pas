unit objectCopier;

interface

uses typinfo, rtti
    //,dialogs
    ;

type
{$RTTI INHERIT}
    TObjectCopier = class(TObject)
    public
        class procedure CopyObject<T>(ASourceObject, ATargetObject: T);
        class function  GenericAsObject(const Value): TObject;
    end;

implementation

class function TObjectCopier.GenericAsObject(const Value): TObject;
begin
  Result := TObject(Value);
end;

class procedure TObjectCopier.CopyObject<T>(ASourceObject, ATargetObject: T);
const
    SKIP_PROP_TYPES = [tkUnknown, tkInterface, tkClass, tkClassRef, tkPointer, tkProcedure];
var
    ctx: TRttiContext;
    rType: TRttiType;
    //rProp: TRttiProperty;
    rField : TRttiField;
    AValue, ASource, ATarget: TValue;
    srcObj, targObj : TObject;
begin
    {DO NOT use this function with vcl components}

    //casting nil pointers is ok
    srcObj := TObjectCopier.GenericAsObject(asourceObject);
    targObj := TObjectCopier.GenericAsObject(ATargetObject);

    //if not ( Assigned(TObject(ASourceObject)) and Assigned(TObject(ATargetObject)) ) then Exit;
    if not ( Assigned(srcObj) and Assigned(targObj) ) then Exit;

    ctx := TRttiContext.Create;
    rType := ctx.GetType(srcObj.ClassInfo);
    ASource := TValue.From<T>(ASourceObject);
    ATarget := TValue.From<T>(ATargetObject);

//    for rProp in rType.GetProperties do
//    begin
//        if (rProp.IsReadable) and (rProp.IsWritable) and not (rProp.PropertyType.TypeKind in SKIP_PROP_TYPES) then
//        begin
//            //when copying visual controls you must skip some properties or you will get some exceptions later
//            //if (rProp.Name = 'Name') or (rProp.Name = 'WindowProc') then
//            //  Continue;
//            AValue := rProp.GetValue(ASource.AsObject);
//            rProp.SetValue(ATarget.AsObject, AValue);
//        end;
//    end;

    for rField in rType.GetFields do
    begin
        if assigned(rField.FieldType) then //this will skip array types
        begin
            if not (rField.FieldType.TypeKind in SKIP_PROP_TYPES) then
            begin
                //when copying visual controls you must skip some properties or you will get some exceptions later
                //if (rProp.Name = 'Name') or (rProp.Name = 'WindowProc') then
                //  Continue;
                AValue := rField.GetValue(ASource.AsObject);
                rField.SetValue(ATarget.AsObject, AValue);
            end;
        end;
    end;

end;



end.

