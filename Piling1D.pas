unit Piling1D;

{$B-}

interface

uses system.math, system.types, system.SysUtils, system.Variants, system.typinfo,
system.classes, system.VarUtils;

type
    TRKType = (k1stOrder,k2ndOrder,k4thOrder);

    TPiling1D = class(TObject)
    private
      const
        F0t: TDoubleDynArray = [0e-3, 0.21e-3, 0.1862e-3, 0.3731e-3,
                0.4431e-3, 0.63e-3,   0.91e-3,   1.05e-3,
                1.26e-3,   1.2831e-3, 1.68e-3,   1.89e-3,
                2.0062e-3, 2.1462e-3, 2.38e-3,   2.66e-3,
                3.0331e-3, 3.4062e-3, 3.6862e-3, 4.1062e-3,
                4.4562e-3, 4.6431e-3, 4.76e-3,   4.9931e-3,
                5.1562e-3, 5.1562e-3, 5.6e-3,    5.7862e-3,
                6.0662e-3, 6.16e-3,   6.4162e-3, 6.6262e-3,
                6.9062e-3, 7.1631e-3, 7.56e-3,   8.0031e-3,
                8.2362e-3, 8.2831e-3, 8.6331e-3, 8.8431e-3,
                9.4962e-3, 9.6362e-3, 9.94e-3,   10.4762e-3,
                11.27e-3,  11.69e-3,  12.0631e-3];
        F0x: TDoubleDynArray = [0e6, 5.6e6, 18.928e6, 33.864e6,
                49.6e6,    64.528e6,  57.6e6,    50.664e6,
                62.128e6,  66.664e6,  62.128e6,  72.528e6,
                82.928e6,  102.928e6, 111.728e6, 102.4e6,
                109.6e6,   106.928e6, 114.128e6, 125.064e6,
                110.664e6, 98.128e6,  82.664e6,  79.728e6,
                85.864e6,  93.864e6,  88.528e6,  91.728e6,
                82.664e6,  73.864e6,  64.8e6,    57.864e6,
                51.2e6,    46.128e6,  45.6e6,    53.864e6,
                47.464e6,  40.8e6,    30.664e6,  28.264e6,
                32.264e6,  28.8e6,    29.064e6,  17.328e6,
                8e6,       13.064e6,  0e6];

        function  stepA(const y,v: TDoubleDynArray; const F0: double): TDoubleDynArray;
        procedure rk1Step(const F0: double);
        procedure rk2Step(const F0: double);
        procedure rk4Step(const F0: double);
        procedure addVect(const x, dx: TDoubleDynArray; const dt: double);
        function  copy(const a: TDoubleDynArray): TDoubledynarray;
    public
      var
        y,v : TDoubleDynArray;
        k,kBottom,cShear,cBottom,m: double;
        dt: double;
        rkType: TRKType;
        time: double;

        history: array of TDoubleDynArray;

        constructor Create(const n: integer);

        procedure step();
        class function  interpolate(const t, x : TDoubleDynArray; const atime: double) : double;
    end;

implementation

constructor TPiling1D.Create(const n: integer);
const
    diam: double = 0.75; //m
    length: double = 10; //m
    density: double = 2300; //kg/m3
    E: double = 17e9;//youngs modulus Pa
begin
    dt := 1e-6;
    m := density * PI * diam * diam / 4 * (length / n); //1 block
    k := E*pi*diam*diam / 4 / (length / n);
    kBottom := k * 1e2;
    cShear := 2e6 / n;
    cBottom := cShear * 1e4;
    rkType := k4thOrder;
    time := 0;

    setlength(y,n);
    setlength(v,n);
    setlength(history,0);
end;

procedure TPiling1D.step;
var
    i : integer;
    F0: double;
begin
    F0 := self.interpolate(F0t, F0x, time);
    case rkType of
        k1stOrder: self.rk1Step(F0);
        k2ndOrder: self.rk2Step(F0);
        k4thOrder: self.rk4Step(F0);
    end;

    setlength(history, length(history) + 1);
    setlength(history[Length(history) - 1], length(y));
    for i := 0 to length(y) - 1 do
        history[Length(history) - 1, i] := y[i];

    time := time + dt;
end;

function TPiling1d.stepA(const y,v: TDoubleDynArray; const F0: double): TDoubleDynArray;
var
    i: integer;
begin
    setlength(result,length(y));
    i := 0;
    result[i] := (k * (y[i + 1] - y[i]) + cShear * (-v[i]) + F0) / m;
    for I := 1 to length(y) - 2 do
        result[i] := (k * (y[i + 1] + y[i - 1] - 2*y[i]) + cShear * (-v[i])) / m;
    i := length(y) - 1;
    result[i] := (k * (y[i - 1] - y[i]) + (kBottom * (-y[i])) + cBottom * (-v[i])) / m;
end;

procedure TPiling1D.addVect(const x, dx: TDoubleDynArray; const dt: double);
var
    i: integer;
begin
    for I := 0 to length(x) - 1 do
        x[i] := x[i] + dx[i] * dt;
end;

function TPiling1D.copy(const a: TDoubleDynArray): TDoubledynarray;
var
    i: integer;
begin
    setlength(result, length(a));
    for i := 0 to length(a) - 1 do
        result[i] := a[i];
end;

procedure TPiling1D.rk1Step(const F0: double);
var
    a: TDoubleDynArray;
begin
    a := stepA(y,v,F0);
    addVect(v,a,dt);
    addVect(y,v,dt);
end;

procedure TPiling1D.rk2Step(const F0: double);
var
    a1,a2,v1,y1 : TDoubleDynArray;
begin
    a1 := self.stepA(y,v,F0);
    v1 := copy(v);
    y1 := copy(y);
    addVect(v1,a1,dt/2);
    addVect(y1,v1,dt/2);

    a2 := self.stepA(y1,v1,F0);
    addVect(v,a2,dt);
    addVect(y,v,dt);
end;

procedure TPiling1D.rk4Step(const F0: double);
var
    a1,v1,y1 : TDoubleDynArray;
    a2,v2,y2 : TDoubleDynArray;
    a3,v3,y3 : TDoubleDynArray;
    a4,v4 : TDoubleDynArray;
    i: integer;
begin
    a1 := self.stepA(y,v,F0);
    v1 := copy(v);
    y1 := copy(y);
    addVect(v1,a1,dt/2);
    addVect(y1,v1,dt/2);

    a2 := self.stepA(y1,v1,F0);
    v2 := copy(v);
    y2 := copy(y);
    addVect(v2,a2,dt/2);
    addVect(y2,v2,dt/2);

    a3 := self.stepA(y2,v2,F0);
    v3 := copy(v);
    y3 := copy(y);
    addVect(v3,a3,dt);
    addVect(y3,v3,dt);

    a4 := self.stepA(y3,v3,F0);
    v4 := copy(v);
    addVect(v4,a4,dt);

    for I := 0 to length(y) - 1 do
    begin
        v[i] := v[i] + dt * (0.16666667*a1[i] + 0.33333333*a2[i] + 0.33333333*a3[i] + 0.16666667*a4[i]);
        y[i] := y[i] + dt * (0.16666667*v1[i] + 0.33333333*v2[i] + 0.33333333*v3[i] + 0.16666667*v4[i]);
    end;
end;


class function TPiling1D.interpolate(const t,x : TDoubleDynArray; const atime: double) : double;
var
    ind : integer;
begin
    ind := 0;
    while (ind < length(t)) and (t[ind] < atime) do
        inc(ind);

    if ind = 0 then
        result := x[0]
    else if ind >= (length(x) - 1) then
        result := x[length(x) - 1]
    else
        result := x[ind] + (x[ind - 1] - x[ind]) * (atime - t [ind]) / (t [ind - 1] - t [ind]);
end;


end.
