unit NOAACurves;

interface

uses system.math, system.StrUtils, system.SysUtils,
    Spectrum, basetypes, SmartPointer;

type
// as per http://www.nmfs.noaa.gov/pr/acoustics/Acoustic%20Guidance%20Files/opr-55_acoustic_guidance_tech_memo.pdf
//Technical Guidance for Assessing the
//Effects of Anthropogenic Sound on
//Marine Mammal Hearing
//Underwater Acoustic Thresholds for Onset of
//Permanent and Temporary Threshold Shifts
//July 2016

    TNOAACurve = record
    private
        a,b,f1,f2,C,K: double;
    public
        PTSImpulsiveLpk, PTSImpulsiveLcum, PTSNonImpulsiveLcum: double;
        name: string;

        function level(const fHz: double): double;
        function toDelimitedString(): string;
    end;

const

    //Tables ES2 and ES3, A10 etc
    Curves: array[0..5] of TNOAACurve = (
        (a: 1; b: 2; f1: 0.2; f2: 19; C: 0.13; K: 179; PTSImpulsiveLpk: 219; PTSImpulsiveLcum: 183; PTSNonImpulsiveLcum: 199; name: 'Low-frequency (LF) cetaceans'),
        (a: 1.6; b: 2; f1: 8.8; f2: 110; C: 1.2; K: 177; PTSImpulsiveLpk: 230; PTSImpulsiveLcum: 185; PTSNonImpulsiveLcum: 198; name: 'Mid-frequency (MF) cetaceans'),
        (a: 1.8; b: 2; f1: 12; f2: 140; C: 1.36; K: 152; PTSImpulsiveLpk: 202; PTSImpulsiveLcum: 155; PTSNonImpulsiveLcum: 173; name: 'High-frequency (HF) cetaceans'),
        (a: 1.8; b: 2; f1: 4.3; f2: 25; C: 2.62; K: 183; PTSImpulsiveLpk: 226; PTSImpulsiveLcum: 190; PTSNonImpulsiveLcum: 206; name: 'Sirenians (SI) manatees'),
        (a: 1; b: 2; f1: 1.9; f2: 30; C: 0.75; K: 180; PTSImpulsiveLpk: 218; PTSImpulsiveLcum: 185; PTSNonImpulsiveLcum: 201; name: 'Phocid pinnipeds (PW) (underwater)'),
        (a: 2; b: 2; f1: 0.94; f2: 25; C: 0.64; K: 198; PTSImpulsiveLpk: 232; PTSImpulsiveLcum: 203; PTSNonImpulsiveLcum: 219; name: 'Otariid pinnipeds (OW) (underwater)')
    );

implementation

function TNOAACurve.level(const fHz: double): double;
var
    fkHz: double;
begin
    if isnan(fHz) then exit(nan);
    fkHz := fHz / 1000;
    result := C +
        10*Log10(power(fkHz/f1,2*a) /
        (power(1 + sqr(fkHz/f1),a) * power(1 + sqr(fkHz/f2),b)));
end;

function TNOAACurve.toDelimitedString(): string;
var
    i: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    for i := 0 to TSpectrum.getLength(bwThirdOctave) - 1 do
        sb.append(ifthen(i = 0, '', ',') + floattostrf(-self.level(TSpectrum.getFbyInd(i,bwThirdOctave)), fffixed, 14, 1));
    result := sb.toString;
end;

end.

