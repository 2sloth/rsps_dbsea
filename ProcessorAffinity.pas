unit ProcessorAffinity;

interface
uses system.classes, system.math, winapi.windows, vcl.forms, helperFunctions;

type
    TProcessorAffinity = class(TObject)
    private
        class function  SingleProcessorMask(const ProcessorIndex: Integer): DWORD_PTR;
        class function  ProcessorInMask(const ProcessorMask: DWORD_PTR; const ProcessorIndex: Integer): Boolean;
        class function  CombinedProcessorMask(const Processors: array of Integer): DWORD_PTR;
        class procedure setAffinity(const processors: TArray<Integer>);
    public
        class function  CPUCount: integer;
        class function  ProcessorsInUse: integer;
        class procedure setNCPUs(const n: integer);
    end;

implementation

class function TProcessorAffinity.SingleProcessorMask(const ProcessorIndex: Integer): DWORD_PTR;
begin
    Result := 1 shl (ProcessorIndex - 1);
end;

class function TProcessorAffinity.CPUCount: integer;
begin
    result := TThread.ProcessorCount;
end;

class function TProcessorAffinity.ProcessorInMask(const ProcessorMask: DWORD_PTR; const ProcessorIndex: Integer): Boolean;
begin
    Result := (SingleProcessorMask(ProcessorIndex) and ProcessorMask) <> 0;
end;

class function TProcessorAffinity.CombinedProcessorMask(const Processors: array of Integer): DWORD_PTR;
var
    i: Integer;
begin
    Result := 0;
    for i := low(Processors) to high(Processors) do
        Result := Result or SingleProcessorMask(Processors[i]);
end;

class function TProcessorAffinity.ProcessorsInUse: integer;
var
    i: integer;
    processMask, systemMask: DWORD_PTR;
begin
    if not getprocessAffinityMask(getcurrentProcess, processMask, systemMask) then exit(cpucount);

    result := 0;
    for I := 1 to cpuCount do
        if ProcessorInMask(processMask,i) then
            inc(result);
end;

class procedure TProcessorAffinity.setAffinity(const processors: TArray<Integer>);
begin
    SetProcessAffinityMask(getcurrentProcess,combinedProcessorMask(processors));
end;

class procedure TProcessorAffinity.setNCPUs(const n: integer);
var
    i: integer;
    arr: TArray<integer>;
begin
    if n = 0 then exit;
    
    arr := [];
    for I := CPUCount downto max(1,CPUCount - n + 1) do
        THelper.append<integer>(i, arr);

    setAffinity(arr);
end;

end.
