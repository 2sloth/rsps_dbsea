unit databaseUpdater;

interface

uses system.SysUtils, system.classes,
    vcl.forms, vcl.dialogs, vcl.stdctrls,
    SQLite3, sqlitetable3, CustomMessageDialog,
    dbseaconstants, mcklib, eventlogging, SmartPointer,
    helperFunctions, myregistry;

type

    TDBUpdater = class(TObject)
    private
        class function  deployNewDatabase(var db: TSQLiteDatabase): TSQLiteDatabase;
    public
        class procedure checkForMetaTableInDB(const db: TSQLiteDatabase; const log: TEventLog);
        class procedure checkForCrestFactorInEquiqmentTable(const db: TSQLiteDatabase; const log: TEventLog);
        class function  updateNOAACurves(var db: TSQLiteDatabase; const log: TEventLog): boolean;
    end;

implementation

uses globalsUnit;

class procedure TDBUpdater.checkForMetaTableInDB(const db: TSQLiteDatabase; const log: TEventLog);
var
    s: string;
begin
    if not db.TableExists(metaTblName) then
    begin
        s := fileVersion(application.ExeName);
        if s[1] = 'v' then
        begin
            db.ExecSQLUnicode('CREATE TABLE "meta" ("databaseVersion" INTEGER NOT NULL,"programVersion" TEXT NOT NULL)');
            db.ExecSQLUnicode('INSERT INTO meta VALUES ( ' + inttostr(dbseaconstants.currentDBVersion) + ',"' + s + '")');
            log.log('Created DB META table');
        end
        else
            log.log('Could not create DB META table. Error getting program version info, ' + s);
    end;
end;

class function  TDBUpdater.updateNOAACurves(var db: TSQLiteDatabase; const log: TEventLog): boolean;
    function shouldUpdate(): boolean;
    var
        sltb: ISmart<TSQLiteTable>;
        existingDBVersion, didAskVersion: integer;
    begin
        if not db.TableExists(metaTblName) then exit(true);

        sltb := TSmart<TSQLiteTable>.create(db.GetTable(ansistring('SELECT * FROM ' + metaTblName)));
        sltb.MoveFirst;
        if sltb.EOF then exit(true);

        existingDBVersion := sltb.FieldAsInteger(sltb.fieldIndex['databaseVersion']);
        if existingDBVersion >= currentDBVersion then exit(false);
        if not reg.getDidAskToUpdateDB(didAskVersion) then exit(true);

        result := didAskVersion < currentDBVersion;
    end;
begin
    result := false;
    if not shouldUpdate then exit(false);

    case CustomMessageDlg(
        'It is highly recommended that the database curves and TTS/PTS thresholds are updated. ' +
        'Do you want to update the database entries to match ' +
        'the definitions given in the updated NOAA acoustic guidance document? The existing database curves ' +
        'will be marked as deprecated. Weightings and thresholds applied to existing projects will not be changed. ' +
        'Please close any other running copy of dBSea before proceeding. ' +
        'For more information on the NOAA curves, see http://www.nmfs.noaa.gov/pr/acoustics/guidelines.htm',
        mtWarning,
        [ButtonInfo(mbYes, '&Yes'), ButtonInfo(mbNo, '&Not now'), ButtonInfo(mbIgnore,'&No, and don''t ask me again', 170)],
        mbYes) of
        6: begin  //mbYes
            db := deployNewDatabase(db);
            exit(true);
        end;
        7: exit(false);// no
        5: begin
            // write reg so we don't ask again
            reg.setDidAskToUpdateDB(currentDBVersion);
            exit(false);
        end;
    end;
end;

class function TDBUpdater.deployNewDatabase(var db: TSQLiteDatabase): TSQLiteDatabase;
begin
    db.free;

    THelper.resourceFileUnbundle(TempPath, mdaDBFile, 'acoustLibDB', true);
    if not fileExists(slDBPath) then
    begin
        eventlog.log('Error: Database file not found ' + slDBPath + '. Attempt clearing the temporary directory and restarting the program.');
        raise Exception.Create('Error: Database file not found ' + slDBPath + '. Attempt clearing the temporary directory and restarting the program.');
    end;
    result := TSQLiteDatabase.Create(slDBPath);
end;

class procedure TDBUpdater.checkForCrestFactorInEquiqmentTable(const db: TSQLiteDatabase; const log : TEventLog);
const
    column = 'crestFactorDB';
var
    sltb: ISmart<TSQLiteTable>;
begin
    try
        sltb := TSmart<TSQLiteTable>.create(db.GetTable(ansistring('SELECT ' + column + ' FROM ' + equipTblName)));
    except
        on E: ESQLiteException do
        begin
            log.log('DB needs to update: adding crestFactorDB column in equiptable');
            db.ExecSQLUnicode('ALTER TABLE ' + equipTblName + ' ADD COLUMN "' + column + '" REAL');
            db.ExecSQLUnicode('UPDATE ' + equipTblName + ' SET ' + column + '=12');
            log.log('DB updateed: crestFactorDB column added in equiptable');
        end;
    end;
end;

end.
