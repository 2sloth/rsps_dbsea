//------------------------------------------------------------------------------
// Project:  Shapelib
// Purpose:  Sample application for dumping contents of a shapefile to
//           the terminal in human readable form.
// Author:   Frank Warmerdam, warmerda@home.com
//           Pascal translation Alexander Weidauer, alex.weidauer@huckfinn.de
//------------------------------------------------------------------------------
// Copyright (c) 1999, Frank Warmerdam
//
// This software is available under the following "MIT Style" license,
// or at the option of the licensee under the LGPL (see LICENSE.LGPL).  This
// option is discussed in more detail in shapelib.html.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// aDEALINGS IN THE SOFTWARE.
//------------------------------------------------------------------------------
Unit ShpAPI129;
Interface

Const

// ----------------------------------------------------------------------------
// Current DLL release
// ----------------------------------------------------------------------------
{$IFDEF CPUX64}
 LibName = 'shapelib64.dll';
{$ELSE}
 LibName = 'shapelib129.dll';
 {$ENDIF}

// ----------------------------------------------------------------------------
// Configuration options.
// ----------------------------------------------------------------------------
//  Should the DBFReadStringAttribute() strip leading and trailing white space?                                            }
// ----------------------------------------------------------------------------
{$DEFINE TRIM_DBF_WHITESPACE}

// ----------------------------------------------------------------------------
// Should we write measure values to the Multipatch object?
// Reportedly ArcView crashes if we do write it, so for now it is disabled.                                                     }
// ----------------------------------------------------------------------------
{$DEFINE DISABLE_MULTIPATCH_MEASURE}

Type
// ----------------------------------------------------------------------------
// Basic definitions
// ----------------------------------------------------------------------------
  PDouble = ^Double;
  PLongInt = ^LongInt;
  PLongIntArray = ^TLongIntArray;
  TLongIntArray = Array Of LongInt;
  PDoubleArray = ^TDoubleArray;
  TDoubleArray = Array  Of Double;

// ----------------------------------------------------------------------------
// Shape info record
// ----------------------------------------------------------------------------
  PSHPInfo = ^SHPInfo;
  SHPInfo = Record
    fpSHP: ^File;
    fpSHX: ^File;
    nShapeType: LongInt;
    nFileSize: LongInt;
    nRecords: LongInt;
    nMaxRecords: LongInt;
    panRecOffset: PLongInt;
    panRecSize: PLongInt;
    adBoundsMin: Array[0..3] Of Double;
    adBoundsMax: Array[0..3] Of Double;
    bUpdated: LongInt;
  End;

  SHPHandle = PSHPInfo;

// ----------------------------------------------------------------------------
// Shape types (nSHPType)
// ----------------------------------------------------------------------------
Const
  SHPT_NULL = 0;
  SHPT_POINT = 1;
  SHPT_ARC = 3;
  SHPT_POLYGON = 5;
  SHPT_MULTIPOINT = 8;
  SHPT_POINTZ = 11;
  SHPT_ARCZ = 13;
  SHPT_POLYGONZ = 15;
  SHPT_MULTIPOINTZ = 18;
  SHPT_POINTM = 21;
  SHPT_ARCM = 23;
  SHPT_POLYGONM = 25;
  SHPT_MULTIPOINTM = 28;
  SHPT_MULTIPATCH = 31;
// ----------------------------------------------------------------------------
// Part types - everything but SHPT_MULTIPATCH just uses SHPP_RING.                                                       }
// ----------------------------------------------------------------------------
  SHPP_TRISTRIP = 0;
  SHPP_TRIFAN = 1;
  SHPP_OUTERRING = 2;
  SHPP_INNERRING = 3;
  SHPP_FIRSTRING = 4;
  SHPP_RING = 5;

// ----------------------------------------------------------------------------
// SHPObject - represents on shape (without attributes) read from the .shp file.                                              }
//             -1 is unknown/unassigned
// ----------------------------------------------------------------------------
Type
  PPShpObject = ^PShpObject;
  PShpObject = ^ShpObject;
  ShpObject = Record
    nSHPType: LongInt;
    nShapeId: LongInt;
    nParts: LongInt;
    panPartStart: TLongIntArray;
    panPartType: TLongIntArray;
    nVertices: LongInt;
    padfX:  TDoubleArray;
    padfY:  TDoubleArray;
    padfZ:  TDoubleArray;
    padfM:  TDoubleArray;
    dfXMin: Double;
    dfYMin: Double;
    dfZMin: Double;
    dfMMin: Double;
    dfXMax: Double;
    dfYMax: Double;
    dfZMax: Double;
    dfMMax: Double;
  End;
// ----------------------------------------------------------------------------
// SHP API Prototypes
// ----------------------------------------------------------------------------
Function SHPOpen (pszShapeFile: Pansichar; pszAccess: Pansichar ) : SHPHandle;
   cdecl;  external LibName name {$IFDEF CPUX64} 'SHPOpen' {$ELSE} '_SHPOpen' {$ENDIF};

Function SHPCreate (pszShapeFile: Pansichar; nShapeType: LongInt ) : SHPHandle;
   cdecl;  external LibName name {$IFDEF CPUX64} 'SHPCreate' {$ELSE} '_SHPCreate' {$ENDIF};
Procedure SHPGetInfo (hSHP: SHPHandle; pnEntities: PLongInt; pnShapeType:
  PLongInt; padfMinBound: PDouble; padfMaxBound: PDouble ) ;  cdecl;  external
  LibName name {$IFDEF CPUX64} 'SHPGetInfo' {$ELSE} '_SHPGetInfo' {$ENDIF};
Function SHPReadObject (hSHP: SHPHandle; iShape: LongInt ) : PShpObject;
   cdecl;  external LibName name {$IFDEF CPUX64} 'SHPReadObject' {$ELSE} '_SHPReadObject' {$ENDIF};
Function SHPWriteObject (hSHP: SHPHandle; iShape: LongInt; psObject: PShpObject
  ) : LongInt;  cdecl;  external LibName name {$IFDEF CPUX64} 'SHPWriteObject' {$ELSE} '_SHPWriteObject' {$ENDIF};
Procedure SHPDestroyObject (psObject: PShpObject ) ;  cdecl;  external
  LibName name {$IFDEF CPUX64} 'SHPDestroyObject' {$ELSE} '_SHPDestroyObject' {$ENDIF};
Procedure SHPComputeExtents (psObject: PShpObject ) ;  cdecl;  external
  LibName name {$IFDEF CPUX64} 'SHPComputeExtents' {$ELSE} '_SHPComputeExtents' {$ENDIF};

//------------------------------------------------------------------------------
// SHP Create Object
//------------------------------------------------------------------------------
{** To Create a shape object of certain type

Function SHPCreateObject(    nSHPType: LongInt;
                             nShapeId: LongInt;
                             nParts:   LongInt;
                             panPartStart: PLongInt;
                             panPartType: PLongInt;
                             nVertices: LongInt;
                             padfX: PDouble;
                             padfY: PDouble;
                             padfZ: PDouble;
                             padfM: PDouble ) : PShpObject;

  nSHPType:		The SHPT_ type of the object to be created, such
                        as SHPT_POINT, or SHPT_POLYGON.

  iShape:		The shapeid to be recorded with this shape.
                        The number -1 means a new one.

  nParts:		The number of parts for this object.  If this is
                        zero for ARC, or POLYGON type objects, a single
                        zero valued part will be created internally.

  panPartStart:		The list of zero based start vertices for the rings
                        (parts) in this object.  The first should always be
                        zero.  This may be NULL if nParts is 0.

  panPartType:		The type of each of the parts.  This is only meaningful
                        for MULTIPATCH files.  For all other cases this may
                        be NULL, and will be assumed to be SHPP_RING.

  nVertices:		The number of vertices being passed in padfX,
                        padfY, and padfZ.

  padfX:		An array of nVertices X coordinates of the vertices
                        for this object.

  padfY:		An array of nVertices Y coordinates of the vertices
                        for this object.

  padfZ:		An array of nVertices Z coordinates of the vertices
                        for this object.  This may be NULL in which case
		        they are all assumed to be zero.

  padfM:		An array of nVertices M (measure values) of the
			vertices for this object.  This may be NULL in which
			case they are all assumed to be zero.
}
Function SHPCreateObject ( nSHPType: LongInt;
                           nShapeId: LongInt; nParts: LongInt;
                           panPartStart: PLongInt;
                           panPartType: PLongInt;
                           nVertices: LongInt;
                           padfX: PDouble;
                           padfY: PDouble;
                           padfZ: PDouble;
                           padfM: PDouble ) : PShpObject;  cdecl;  external LibName name
    {$IFDEF CPUX64} 'SHPCreateObject' {$ELSE} '_SHPCreateObject' {$ENDIF};

//------------------------------------------------------------------------------
// SHP Create Siple Object
//------------------------------------------------------------------------------
{**

  Function SHPCreateSimpleObject (  nSHPType:  LongInt;
                                    nVertices: LongInt;
                                    padfX:  PDouble;
                                    padfY: PDouble;
                                    padfZ: PDouble ) : PShpObject;

  nSHPType:		The SHPT_ type of the object to be created, such
                        as SHPT_POINT, or SHPT_POLYGON.

  nVertices:		The number of vertices being passed in padfX,
                        padfY, and padfZ.

  padfX:		An array of nVertices X coordinates of the vertices
                        for this object.

  padfY:		An array of nVertices Y coordinates of the vertices
                        for this object.

  padfZ:		An array of nVertices Z coordinates of the vertices
                        for this object.  This may be NULL in which case
		        they are all assumed to be zero.

}
Function SHPCreateSimpleObject (nSHPType: LongInt; nVertices: LongInt; padfX:
  PDouble; padfY: PDouble; padfZ: PDouble ) : PShpObject;  cdecl;  external
  LibName name {$IFDEF CPUX64} 'SHPCreateSimpleObject' {$ELSE} '_SHPCreateSimpleObject' {$ENDIF};

//------------------------------------------------------------------------------
// Close a Shapefile by a given handle
//------------------------------------------------------------------------------
Procedure SHPClose (hSHP: SHPHandle ) ;  cdecl;  external LibName name
  {$IFDEF CPUX64} 'SHPClose' {$ELSE} '_SHPClose' {$ENDIF};

//------------------------------------------------------------------------------
// Get back the shape type sting
//------------------------------------------------------------------------------
Function SHPTypeName (nSHPType: LongInt ) : Pansichar;  cdecl;  external
  LibName name {$IFDEF CPUX64} 'SHPTypeName' {$ELSE} '_SHPTypeName' {$ENDIF};

//------------------------------------------------------------------------------
// Get back the part type sting
//------------------------------------------------------------------------------
Function SHPPartTypeName (nPartType: LongInt ) : Pansichar;  cdecl;  external
  LibName name {$IFDEF CPUX64} 'SHPPartTypeName' {$ELSE} '_SHPPartTypeName' {$ENDIF};

// ----------------------------------------------------------------------------
// Shape quadtree indexing API.
// .. this can be two or four for binary or quad tree
// ----------------------------------------------------------------------------

Const
  MAX_SUBNODE = 4;

// ----------------------------------------------------------------------------
// region covered by this node list of shapes stored at this node.
// The papsShapeObj pointers or the whole list can be NULL
// ----------------------------------------------------------------------------
Type
  PSHPTreeNode = ^SHPTreeNode;
  SHPTreeNode = Record
    adfBoundsMin: Array[0..3] Of Double;
    adfBoundsMax: Array[0..3] Of Double;
    nShapeCount: LongInt;
    panShapeIds: PLongInt;
    papsShapeObj: PPShpObject;
    nSubNodes: LongInt;
    apsSubNode: Array[0.. (MAX_SUBNODE ) - 1] Of PSHPTreeNode;
  End;

  shape_tree_node = SHPTreeNode;
  Pshape_tree_node = ^shape_tree_node;

  PSHPTree = ^SHpTree;
  SHpTree = Record
    hSHP: SHPHandle;
    nMaxDepth: LongInt;
    nDimension: LongInt;
    psRoot: PSHPTreeNode;
  End;

Function SHPCreateTree (hSHP: SHPHandle; nDimension: LongInt; nMaxDepth:
  LongInt; padfBoundsMin: PDouble; padfBoundsMax: PDouble ) : PSHPTree;
  cdecl;  external LibName name {$IFDEF CPUX64} 'SHPCreateTree' {$ELSE} '_SHPCreateTree' {$ENDIF};
Procedure SHPDestroyTree (hTree: PSHPTree ) ;  cdecl;  external LibName
  name {$IFDEF CPUX64} 'SHPDestroyTree' {$ELSE} '_SHPDestroyTree' {$ENDIF};
Function SHPWriteTree (hTree: PSHPTree; pszFilename: Pansichar ) : LongInt;
  cdecl;  external LibName name '_SHPWriteTree';
Function SHPReadTree (pszFilename: Pansichar ) : SHpTree;  cdecl;  external
  LibName name '_SHPReadTree';
Function SHPTreeAddObject (hTree: PSHPTree; psObject: PShpObject ) : LongInt;
   cdecl;  external LibName name '_SHPTreeAddObject';
Function SHPTreeAddShapeId (hTree: PSHPTree; psObject: PShpObject ) : LongInt;
   cdecl;  external LibName name '_SHPTreeAddShapeId';
Function SHPTreeRemoveShapeId (hTree: PSHPTree; nShapeId: LongInt ) : LongInt;
   cdecl;  external LibName name '_SHPTreeRemoveShapeId';
Procedure SHPTreeTrimExtraNodes (hTree: PSHPTree ) ;  cdecl;  external
  LibName name '_SHPTreeTrimExtraNodes';
Function SHPTreeFindLikelyShapes (hTree: PSHPTree; padfBoundsMin: PDouble;
  padfBoundsMax: PDouble; _para4: PLongInt ) : PLongInt;  cdecl;  external
  LibName name '_SHPTreeFindLikelyShapes';
Function SHPCheckBoundsOverlap (_para1: PDouble; _para2: PDouble; _para3:
  PDouble; _para4: PDouble; _para5: LongInt ) : LongInt;  cdecl;  external
  LibName name '_SHPCheckBoundsOverlap';

// ----------------------------------------------------------------------------
// DBF Support.                              }
// ----------------------------------------------------------------------------

Type
  PDBFInfo = ^DbfInfo;
  DbfInfo = Record
    fp: File;
    nRecords: LongInt;
    nRecordLength: LongInt;
    nHeaderLength: LongInt;
    nFields: LongInt;
    panFieldOffset: PLongInt;
    panFieldSize: PLongInt;
    panFieldDecimals: PLongInt;
    pachFieldType: Pansichar;
    pszHeader: Pansichar;
    nCurrentRecord: LongInt;
    bCurrentRecordModified: LongInt;
    pszCurrentRecord: Pansichar;
    bNoHeader: LongInt;
    bUpdated: LongInt;
  End;

  DBFHandle = PDBFInfo;

  DBFFieldType = (FTString, FTInteger, FTDouble, FTInvalid ) ;

Const  XBASE_FLDHDR_SZ = 32;

Function DBFOpen (pszDBFFile: Pansichar; pszAccess: Pansichar ) : DBFHandle;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFOpen' {$ELSE} '_DBFOpen' {$ENDIF};
Function DBFCreate (pszDBFFile: Pansichar ) : DBFHandle;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFCreate' {$ELSE} '_DBFCreate' {$ENDIF};
Function DBFGetFieldCount (psDBF: DBFHandle ) : LongInt;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFGetFieldCount' {$ELSE} '_DBFGetFieldCount' {$ENDIF};
Function DBFGetRecordCount (psDBF: DBFHandle ) : LongInt;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFGetRecordCount' {$ELSE} '_DBFGetRecordCount' {$ENDIF};
Function DBFAddField (hDBF: DBFHandle; pszFieldName: Pansichar; eType:
  DBFFieldType; nWidth: LongInt; nDecimals: LongInt ) : LongInt;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFAddField' {$ELSE} '_DBFAddField' {$ENDIF};
Function DBFGetFieldInfo (psDBF: DBFHandle; iField: LongInt; pszFieldName:
  Pansichar; pnWidth: PLongInt; pnDecimals: PLongInt ) : DBFFieldType;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFGetFieldInfo' {$ELSE} '_DBFGetFieldInfo' {$ENDIF};
Function DBFGetFieldIndex (psDBF: DBFHandle; pszFieldName: Pansichar ) : LongInt;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFGetFieldIndex' {$ELSE} '_DBFGetFieldIndex' {$ENDIF};
Function DBFReadIntegerAttribute (hDBF: DBFHandle; iShape: LongInt; iField:
  LongInt ) : LongInt;  cdecl;  external LibName name
  {$IFDEF CPUX64} 'DBFReadIntegerAttribute' {$ELSE} '_DBFReadIntegerAttribute' {$ENDIF};
Function DBFReadDoubleAttribute (hDBF: DBFHandle; iShape: LongInt; iField:
  LongInt ) : Double;  cdecl;  external LibName name
  {$IFDEF CPUX64} 'DBFReadDoubleAttribute' {$ELSE} '_DBFReadDoubleAttribute' {$ENDIF};
Function DBFReadStringAttribute (hDBF: DBFHandle; iShape: LongInt; iField:
  LongInt ) : Pansichar;  cdecl;  external LibName name
  {$IFDEF CPUX64} 'DBFReadStringAttribute' {$ELSE} '_DBFReadStringAttribute' {$ENDIF};
Function DBFIsAttributeNULL (hDBF: DBFHandle; iShape: LongInt; iField: LongInt
  ) : LongInt;  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFIsAttributeNULL' {$ELSE} '_DBFIsAttributeNULL' {$ENDIF};
Function DBFWriteIntegerAttribute (hDBF: DBFHandle; iShape: LongInt; iField:
  LongInt; nFieldValue: LongInt ) : LongInt;  cdecl;  external LibName
  name {$IFDEF CPUX64} 'DBFWriteIntegerAttribute' {$ELSE} '_DBFWriteIntegerAttribute' {$ENDIF};
Function DBFWriteDoubleAttribute (hDBF: DBFHandle; iShape: LongInt; iField:
  LongInt; dFieldValue: Double ) : LongInt;  cdecl;  external LibName name
  {$IFDEF CPUX64} 'DBFWriteDoubleAttribute' {$ELSE} '_DBFWriteDoubleAttribute' {$ENDIF};
Function DBFWriteStringAttribute (hDBF: DBFHandle; iShape: LongInt; iField:
  LongInt; pszFieldValue: Pansichar ) : LongInt;  cdecl;  external LibName
  name {$IFDEF CPUX64} 'DBFWriteStringAttribute' {$ELSE} '_DBFWriteStringAttribute' {$ENDIF};
Function DBFWriteNULLAttribute (hDBF: DBFHandle; iShape: LongInt; iField:
  LongInt ) : LongInt;  cdecl;  external LibName name
  {$IFDEF CPUX64} 'DBFWriteNULLAttribute' {$ELSE} '_DBFWriteNULLAttribute' {$ENDIF};
Function DBFReadTuple (psDBF: DBFHandle; hEntity: LongInt ) : Pansichar;
  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFReadTuple' {$ELSE} '_DBFReadTuple' {$ENDIF};
Function DBFWriteTuple (psDBF: DBFHandle; hEntity: LongInt; Var pRawTuple ) :
  LongInt;  cdecl;  external LibName name {$IFDEF CPUX64} 'DBFWriteTuple' {$ELSE} '_DBFWriteTuple' {$ENDIF};
Function DBFCloneEmpty (psDBF: DBFHandle; pszFilename: Pansichar ) : DBFHandle;
   cdecl;  external LibName name {$IFDEF CPUX64} 'DBFCloneEmpty' {$ELSE} '_DBFCloneEmpty' {$ENDIF};
Procedure DBFClose (hDBF: DBFHandle ) ;  cdecl;  external LibName name
  {$IFDEF CPUX64} 'DBFClose' {$ELSE} '_DBFClose' {$ENDIF};
Function DBFGetNativeFieldType (hDBF: DBFHandle; iField: LongInt ) : char;
   cdecl;  external LibName name {$IFDEF CPUX64} 'DBFGetNativeFieldType' {$ELSE} '_DBFGetNativeFieldType' {$ENDIF};

Implementation

End.
// ----------------------------------------------------------------------------
// EndOf
// ----------------------------------------------------------------------------

