unit soundSourceSolveHelper;

{$B-}

interface

uses system.classes, system.Types, system.Math, system.SyncObjs,
    system.SysUtils, system.StrUtils, system.JSON, system.threading,
    vcl.stdctrls,
    vcl.forms,
    vcl.dialogs,
    vcl.comctrls,
    winapi.windows, Winapi.MMSystem,
    generics.collections,
    taskbarProgress,
    soundSource, basetypes, levelConverter, solveTimeEstimator,
    helperFunctions, material, timeSeries, DSP, filterbank,
    raySolverTypes, spectrum,
    dBSeaRay2, dBSeaRay3, dBSeaPE2, dBSeaModes1, dBSeaPESS1, dBSeaPESS3D1,
    dbsearay3d1, dbseaPEElastic1,
    Mcomplex,
    dBSeaconstants, soundSpeedProfile,
    multiLayerReflectionCoefficient, seafloorScheme, directivity,
     mcklib, dBSeaRayArrivals, waterprops, solversGeneralFunctions,
     externalSolvers, globalsUnit, cloudSolve, HighOrder, SmartPointer,
     eventlogging, LocationProperties,
     OtlTask, OtlTaskControl, OtlParallel, RayPlotting,
     runexecutable, selfStarter;

type

    ERayTracerError = class(Exception);

    TLoopResult = record
    private
      FCompleted: Boolean;
      FLowestBreakIteration: Variant;
    public
      property Completed: Boolean read FCompleted;
      property LowestBreakIteration: Variant read FLowestBreakIteration;
    end;

    TFreqSlices = class(TDictionary<double, ISmart<TList<integer>>>);
    TErrorDict = class(TDictionary<TSolverError,ISmart<TFreqSlices>>)
    private
        procedure addNew(const err: TSolverError; const f: double; const n: integer);
    public
        procedure addAll(const errs: TSolverErrors; const f: double; const n: integer);
        //procedure addMatrix(const errs: TArray<TArray<TSolverErrors>>; const f: double; const n: integer);
        procedure appendToStringList(const sl: TStringList);
        function  toStringArray(): TArray<string>;
    end;

    TStatusCode = (kReady,kRunning,kDone,kWaitingInTask,kInSolver,kEndSolver,kPostSolver);
  TSolverStatus = record
  public
     code: TStatusCode;
     lastUpdate: cardinal;

     procedure setCode(const c: TStatusCode);
     function  timeSince(): cardinal;
     function  toString(): string;
  end;
  TSolverStatuses = array of TSolverStatus;
  TSolverStatusesHelper = record helper for TSolverStatuses
    procedure init();
    procedure setAll(const code: TStatusCode);
    function  allEqual(const code: TStatusCode): boolean;
    function  allSame(): boolean;
    function  allTimedOut: boolean;
    //function  shouldTerminate(): boolean;
    function  toString(): string;
    function  shouldRelaunch(): boolean;
  end;

    TCloudFreqSlice = class(TObject)
        fi,slice: integer;
        data: string;
    end;
    TCloudFreqSliceList = TObjectList<TCloudFreqSlice>;

    TSourceSolve = class helper for TSource
    public
        procedure solveAlogrBr(const ALogPart,BLinPart,r0: double; const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
        procedure solve10log(const r0, rd: double; const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText : boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
        procedure solveramGEO(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool; const mmSolverMessages: TMemo; const cloudSolve: boolean);
        procedure solveRamCloud(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool; const mmSolverMessages: TMemo);
        procedure solveBellhop(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool; const mmSolverMessages: TMemo);
        procedure solveKraken(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const mmSolverMessages: TMemo;  const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);

        procedure solveElasticPE(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeterms: integer;
            const aPos: TFPoint3;
            const lbSolving, lbSolveExtraInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
            //const updateLabelProc: TProcedureWithString;
            const stepProgressBars: TProcedureWithInt;
            const setProgressBars: TProcedureWithDouble;
            const estimator: TSolveTimeEstimator;
            const UpdateRemainingSolveTime: TProcedureWithBoolBool);
        procedure solvePESplitStep(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeterms: integer; const aPos: TFPoint3);
        procedure solvePE3dFFT(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer; const aPos: TFPoint3);
        procedure solvePE3dPade(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeTerms, nHorizontalPadeTerms: integer;
            const aPos: TFPoint3;
            const lbSolving,lbSolveExtraInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
            const setProgressBars: TProcedureWithDouble;
            const estimator: TSolveTimeEstimator;
            const UpdateRemainingSolveTime: TProcedureWithBoolBool;
            const allErrorMessages: TStringList);
        procedure solvedBSeaRay3d(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer;
                                const aPos: TFPoint3;
                                const useLowFreqSolverText: boolean;
                                const coherent, calculateVolumeAttenuationAtEachStep: boolean;
                                const raySolverIterationMethod: TRaySolverIterationMethod;
                                const levelType: TLevelType;
                                const allErrorMessages: TStringlist;
                                const lblSolving, lblSolveExtraInfo, lblSliceNumber,lblSolveSourcePosition: TLabel;
                                const stepProgressBars: TProcedureWithInt;
                                const setProgressBars: TProcedureWithDouble;
                                const estimator: TSolveTimeEstimator;
                                const UpdateRemainingSolveTime: TProcedureWithBoolBool);
        procedure solvedBSeaRay(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer;
                                const aPos: TFPoint3;
                                const useLowFreqSolverText: boolean;
                                const coherent, calculateVolumeAttenuationAtEachStep: boolean;
                                const raySolverIterationMethod: TRaySolverIterationMethod;
                                const levelType: TLevelType;
                                const allErrorMessages: TStringlist;
                                const lblSolving, lblSolveExtraInfo, lblSliceNumber,lblSolveSourcePosition: TLabel;
                                const stepProgressBars: TProcedureWithInt;
                                const setProgressBars: TProcedureWithDouble;
                                const estimator: TSolveTimeEstimator;
                                const UpdateRemainingSolveTime: TProcedureWithBoolBool);
        procedure solvedBSeaRayThreaded(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer;
                                const aPos: TFPoint3;
                                const useLowFreqSolverText: boolean;
                                const coherent, calculateVolumeAttenuationAtEachStep: boolean;
                                const raySolverIterationMethod: TRaySolverIterationMethod;
                                const levelType: TLevelType;
                                const allErrorMessages: TStringlist;
                                const lbSolving, lbExtraSolveInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
                                const stepProgressBars: TProcedureWithInt;
                                const setProgressBars: TProcedureWithDouble;
                                const estimator: TSolveTimeEstimator;
                                const UpdateRemainingSolveTime: TProcedureWithBoolBool);

        {dBSeaModes and dBSeaPE}
        procedure solveInternal(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeTerms: integer;
                                const aPos: TFPoint3;
                                const useLowFreqSolverText: boolean;
                                const aSolver: TCalculationMethod;
                                const allErrorMessages: TStringlist;
                                const lbSolving,lbSolveExtraInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
                                const setProgressBars: TProcedureWithDouble;
                                const estimator: TSolveTimeEstimator);

        procedure solveInternalThreaded(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeTerms: integer;
                                         const aPos: TFPoint3;
                                         const useLowFreqSolverText: boolean;
                                         const aSolver: TCalculationMethod;
                                         const allErrorMessages: TStringlist;
                                         const lblSolving, lbExtraSolveInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
                                         const setProgressBars: TProcedureWithDouble;
                                         const estimator: TSolveTimeEstimator);

    private
        procedure setNaNForFiPlusStartFi(const fiPlusStartFi: integer);
        function  excludeTL(const d: double): boolean;
        function  JSONFormatSolverInput(const solveFreqs, solver: string; const strings: TStringlist): string;
        function  parseJSONSolverOutput(const solverOut: string; const freqLookup: TDictionary<string,integer>): TCloudFreqSliceList;
        procedure setFieldbyInd(const n, i, j, fi : integer; const c : TComplex);
        procedure getZMaxAndRmaxFromZProfile(const zprofile: TRangeValArray; const forceContinuePastLand: boolean; out zmax, rmax : double);
        function  formatSecondsToHHMMSS(const secs: integer): string;
        procedure updateSolverUIMessages2(const movingPosIndex, sliceNumber: integer; const cSolverStartTime: cardinal; const totalFractionDone: double; const lbSliceNumber, lbSolveSourcePosition, lbExtraSolveInfo: TLabel; const setProgressBars: TProcedureWithDouble);
        procedure updateSolverUIMessages(const startfi, fi, movingPosIndex, sliceNumber, incrementBarsBy: integer; const useLowFreqSolverText: boolean; const bw: TBandwidth; const lbSliceNumber, lbSolveSourcePosition, lbExtraSolveInfo: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
        function  TL20log(const pos1,pos2: TFPoint3; const r0 : double): Double;
        function  TL10log(const pos1,pos2: TFPoint3; const r0: double):double;
        function  dCountAndMitigationCalc(const dCount, dMitigation: double): double;
        procedure RayArrivalsOutputTime3D(const multipleSolveFrequencies, oversampleFreqs: TDoubleDynArray;
            const coherent, calculateVolumeAttenuationAtEachStep: boolean;
            const solveThisFreq: TBooleanDynArray;
            const startfi, lenfi, nFreqsPerBand, dmax: integer;
            const solverIMax: TWrappedInt;
            const aWater: TWaterProps;
            const aDepths: TDoubleDynArray;
            const pCancelSolve: TWrappedBool;
            const arrivals: TArrivalsField;
            const downsampled: TTimeSeries;
            const levelType: TLevelType;
            const aPos: TFPoint3;
            const filterbank: TFilterbank;
            const timeFractionDone: TWrappedDouble);
        procedure RayArrivalsOutputFreq3D(const multipleSolveFrequencies, oversampleFreqs: TDoubleDynArray;
            const pCancelSolve: TWrappedBool;
            const coherent, calculateVolumeAttenuationAtEachStep: boolean;
            const solveThisFreq: TBooleanDynArray;
            const startfi, lenfi, nFreqsPerBand, solveriMax: integer;
            const levelType: TLevelType;
            const aPos: TFPoint3;
            const tlMultiFreq: TArray<TDoubleField>;
            const pFieldMultiFreq: TComplexFieldArray;
            const outputFractionDone: TWrappedDouble);

        procedure solveKrakenFreq(const fi, startfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const mmSolverMessages: TMemo;  const stepProgressBars: TProcedureWithInt; const pbSolveProgress: TProgressbar; const taskbarProgress: TTaskbarProgress; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
        procedure solvedBSeaRayThreadedLoop(const n, nFreqsPerBand: integer;
                                            const bw: TBandwidth;
                                            const multipleSolveFreqs: TDoubleDynArray;
                                            const solveThisFreq: TBooleanDynArray;
                                            const c, rho, zmax, rayTracingStartAngle, rayTracingEndAngle, rayTracingInitialStepsize: double;
                                            const startfi, dmax, DBSeaRayMaxBottomReflections, BellhopNRays: integer;
                                            const aPos: TFPoint3;
                                            const levelType: TLevelType;
                                            const pCancelSolve: TWrappedBool;
                                            const useLowFreqSolverText, coherent, calculateVolumeAttenuationAtEachStep: boolean;
                                            const raySolverIterationMethod: TRaySolverIterationMethod;
                                            const seafloors: TSeafloorRangeArray;
                                            const aWater: TWaterProps;
                                            const aDepths: TDoubleDynArray;
                                            const azProfile2: TRangeValArray;
                                            const aCProfile3: TDepthValArrayAtRangeArray;
                                            const fractionDone, outputFractionDone: TWrappedDouble;
                                            const sliceRays: TSliceRays;
                                            out solverErrors: TSolverErrors;
                                            var deaths: TRayDeaths);

        procedure solveInternalThreadedLoop(const n, fi, nFreqsPerBand, nPadeTerms: integer;
                                             const starters: TArray<TStarter>;
                                             const oversampleFreqs, rangesArray: TDoubleDynArray;
                                             const fc, adr, zmax: double;
                                             const startfi, dmax, dBSeaModesMaxModes: integer;
                                             const stopAtLand: boolean;
                                             const aPos: TFPoint3;
                                             const pCancelSolve: TWrappedBool;
                                             const useLowFreqSolverText: boolean;
                                             const aSolver: TCalculationMethod;
                                             const seafloors: TSeafloorRangeArray;
                                             const aWater: TWaterProps;
                                             const aDepths: TDoubleDynArray;
                                             const aZProfile2: TRangeValArray;
                                             const aCProfile3: TDepthValArrayAtRangeArray;
                                             const PEzOversampling, PErOversampling: double;
                                             const fractionDone: TWrappedDouble;
                                             out solverErrors: TSolverErrors);
        function averageTLs(const tls: TArray<TDoubleField>): TDoubleField; overload;
        function averageTLs(const tls: TDoubleField): TMatrix; overload;
        function averageTLs(const tls: TDoubleDynArray): double; overload;
        function sumComplexArray(const arr: TComplexVect): Tcomplex;
        procedure raySolverTimeOutput2D(const n, dMax, lenfi, startfi, nFreqsPerBand: integer;
            const aPos: TFPoint3;
            const bw: TBandwidth;
            const levelType: TLevelType;
            const coherent, calculateVolumeAttenuationAtEachStep: boolean;
            const aWater: TWaterProps;
            const aDepths: TDoubleDynArray;
            const pCancelSolve: TWrappedBool;
            const solverIMax: TWrappedInt;
            const solveThisFreq: TBooleanDynArray;
            const oversampleFreqs: TDoubleDynArray;
            const arrivals: TArrivalsField;
            const downsampled: TTimeSeries;
            const filterbank: TFilterbank;
            const outputFractionDone: TWrappedDouble);
        procedure raySolverFreqOutput2D(const n, dMax, lenfi, startfi, nFreqsPerBand: integer;
            const aPos: TFPoint3;
            const bw: TBandwidth;
            const levelType: TLevelType;
            const coherent, calculateVolumeAttenuationAtEachStep: boolean;
            const aWater: TWaterProps;
            const aDepths: TDoubleDynArray;
            const pCancelSolve: TWrappedBool;
            const solverIMax: TWrappedInt;
            const solveThisFreq: TBooleanDynArray;
            const oversampleFreqs: TDoubleDynArray;
            const tlMultiFreq: TDoubleField;
            const pFieldMultiFreq: TComplexField;
            const outputFractionDone: TWrappedDouble);
    end;


implementation

procedure TSourceSolve.solveAlogrBr(const ALogPart, BLinPart, r0: double; const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    i, d, n, fi         : integer;
    bw                  : TBandwidth;
    TL, WaterAttenuation : double;
    recPos              : TFPoint3;
    theLev              : double;
    seawaterAbsorptions : TDoubleDynArray;
    dist                : double;
begin
    bw := delegate.getBandwidth;
    recPos.setPos(aPos);
    setlength(seawaterAbsorptions,lenfi);
    lblSolving.Caption := self.Name + '   ' + tostr(ALogPart) + ' log(r/r0) ' +
                                    ifthen(BLinPart = 0,'',' + ' + tostr(bLinPart) + ' r/r0 ') +
                                    ' ' + TSpectrum.getFStringbyInd(startfi, bw) +
                                    ' to ' + TSpectrum.getFStringbyInd(startfi + lenfi - 1, bw) + ' Hz...';
    application.ProcessMessages;

    for d := 0 to delegate.getDMax - 1 do
    begin
        recPos.z := delegate.depths(d);

        {setup seawater absorption coefficients for faster access later. this changes with depth so needs
        to be within the d loop}
        for fi := 0 to lenfi - 1 do
            seawaterAbsorptions[fi] := delegate.getWater.seawaterAbsorptionCoefficient(TSpectrum.getFbyInd(fi + startfi, bw),
                                                                (aPos.Z + delegate.depths(d))/2);
        for i := 0 to iMax - 1 do
        begin
            {it is only necessary to step the recPos out in the x direction
            as the solution is axially symmetric about z. So we don't need to
            do the cos(angle) sin(angle) distances.}
            recPos.x := aPos.X + ranges(i);

            dist := apos.dist(recPos);
            if dist < r0 then
                dist := r0;

            TL := ALogPart * log10(dist/r0) + BLinPart * dist/r0;
            //TL := TL20log(aPos, @recPos, r0); //geometric transmission loss
            for fi := 0 to lenfi - 1 do
            begin
                WaterAttenuation := TMCKLib.Distance2D(0,
                                               ranges(i),
                                               aPos.Z,
                                               delegate.depths(d)) *
                                               seawaterAbsorptions[fi];
                theLev := TL + WaterAttenuation;

                for n := 0 to NSlices - 1 do
                    self.setTLbyInd(n, i, d, fi + startfi, theLev);
            end;
        end; // i
        stepProgressBars(iMax);
        application.ProcessMessages;
        if delegate.getPCancelSolve.b then Exit;
    end;//d

    self.updateSolverUIMessages(startfi,lenfi - 1,movingPosIndex,NSlices - 1,1,useLowFreqSolverText,bw,lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
end;

procedure TSourceSolve.solve10log(const r0, rd: double; const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    i, d, n, fi        : integer;
    bw                 : TBandwidth;
    TL, WaterAttenuation: double;
    recPos             : TFPoint3;
    seawaterAbsorptions: TDoubleDynArray;
    theLev             : double;
    TLAtRefDist        : double;
begin
    bw := delegate.getBandwidth;
    setlength(seawaterAbsorptions,lenfi);
    recPos.setPos(aPos);

    lblSolving.Caption := self.Name + '   20 log and 10 log ' + TSpectrum.getFStringbyInd(startfi, bw) +
                                                ' to ' + TSpectrum.getFStringbyInd(startfi + lenfi - 1, bw) + ' Hz...';
    application.ProcessMessages;

    for d := 0 to delegate.getDMax - 1 do
    begin
        recPos.z := delegate.depths(d);
        recPos.x := aPos.X + rd;  //reference position (used for local depth)
        TLAtRefDist := self.TL20log(aPos, recPos, r0);

        {setup seawater absorption coefficients for faster access later. this changes with depth so needs
        to be within the d loop}
        for fi := 0 to lenfi - 1 do
            seawaterAbsorptions[fi] := delegate.getWater.seawaterAbsorptionCoefficient(TSpectrum.getFbyInd(fi + startfi, bw),
                                                                (aPos.Z + delegate.depths(d))/2);
        for i := 0 to iMax - 1 do
        begin
            {it is only necessary to step the recPos out in the x direction
            as the solution is axially symmetric about z. So we don't need to
            do the cos(angle) sin(angle) distances.}
            recPos.x := aPos.X + ranges(i); //receiver position

            TL := TLAtRefDist + self.TL10log(aPos, recPos, rd);
            for fi := 0 to lenfi - 1 do
            begin
                WaterAttenuation := TMCKLib.Distance2D(0,
                                               ranges(i),
                                               aPos.Z,
                                               delegate.depths(d)) *
                                               seawaterAbsorptions[fi];
                theLev := TL + WaterAttenuation;
                for n := 0 to NSlices - 1 do
                    self.setTLbyInd(n, i, d, fi + startfi, theLev);
            end;
        end; // i
        stepProgressBars(iMax);
        application.ProcessMessages;
        if delegate.getPCancelSolve.b then Exit;
    end;  //d

    self.updateSolverUIMessages(startfi,lenfi - 1,movingPosIndex,NSlices - 1,1,useLowFreqSolverText,bw,lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
end;


procedure TSourceSolve.solveramGEO(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: Boolean; const lblSolving, lblSolveExtraInfo, lblSliceNumber, lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool; const mmSolverMessages: TMemo; const cloudSolve: Boolean);
    function DelDoubleSpaces(const s: String): string;
    var
        i: integer;
    begin
        if length(s) > 0 then
            result := s[1]
        else
            result := '';

        for i := 1 to length(s) do
        begin
            if s[i] = ' ' then
            begin
                if (i = 1) or not (s[i-1] = ' ') then
                    result := result + ' ';
            end
            else
                result := result + s[i];
        end;
    end;
var
    outputStrUntrimmed : string;
    memoFirstLineStr    : string;
    inputString         : string;
    TLs                 : TMatrix; // calculated transmission losses
    i, j, n, fi         : integer;
    bw                  : TBandwidth;
    zprofile            : TMatrix; // bathymetry profile along solution vector
    tmpCprofile         : TMatrix;
    WaterAttenuation    : double;
    bShowSolveProgress  : boolean;
begin
    { check if a previous solver process is still open, then ask for confirmation to kill it }
    if THelper.TestAndKillProcess(RamBinary) = 2 then
        Exit;

    {check that the number of z divisions won't cause ramgeo to fail. NB this is controlled
    by parameter mz in ramMDAn.f, and also how deep the subbottom is below the seafloor, so
    update these parameters if either of those factors changes.}
    if (3*(delegate.getDMax + 2) > 4000) then
    begin
        showmessage('Number of depth points too large for solver, reduce depth points to solve');
        Exit;
    end;

    bw := delegate.getBandwidth;

    bShowSolveProgress := false;
    mmSolverMessages.Visible := bShowSolveProgress; //this call moved out of the loop so the memo doesn't show and hide constantly
    for fi := 0 to lenfi - 1 do
    begin
        if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
            setNaNForFiPlusStartFi(fi + startfi)
        else
        begin
            lblSolving.Caption := self.Name + '   ' + ifthen(cloudSolve,'RAM cloud ','RamGEO ') + TSpectrum.getFStringbyInd(fi + startfi, bw) + ' Hz...';
            application.ProcessMessages;

            for n := 0 to self.nslices - 1 do
            begin
                zprofile := bathyDelegate.getBathymetryVector(aPos.X, aPos.Y, angles(n), rmax, self.iExtendPastEdge);

                {add the water current velocity to the cprofile for this angle
                the projection of the water current along the current vector is
                V*cos(angle between current and current vect)}
                tmpCprofile := TSSP.removeDuplicateDepths(delegate.getCProfileAtAngle(angles(n)));

                { write the input string which gets appended to the call to the external fortran program }
                inputString := TExternalSolvers.writeRAMInput
                  (TSpectrum.getFbyInd(fi + startfi, bw), aPos.Z, rmax, self.dr, aPos, zprofile, tmpCprofile, true);

                //thelper.writeStringToFile('RAMSolverInput.txt', inputString);
                //inputString := thelper.readFileToString('firstSlice.txt');

                { * call the RAM model, from Mike Collins (US Navy).
                  I've recompiled it with a few changes, as ramMDA. It is written in fortran.
                  Accepts input text (to stdin) in the format of the file rmaMDA.in,
                  and produces stdout in the format of file tl.grid
                  with the calculated transmission losses at the grid of points specified in the
                  input file.* }

                setlength(TLs, imax, delegate.getDMax);
                { this is the version where we get back the result from stdout, no need for a file
                  it is all happening in memory so runs a bit quicker }
                memoFirstLineStr := 'Slice ' + tostr(n + 1) + ' of ' + tostr
                  (nslices) + ', ' + tostr(TSpectrum.getFbyInd(fi + startfi, bw))
                  + ' Hz'; { ramMDA runs quickly, we don't need the feedback }

                if cloudSolve then
                begin
                    outputStrUntrimmed := TCloudSolve.post(sSolveServer, inputString);
                    outputStrUntrimmed := DelDoubleSpaces(outputStrUntrimmed);
                end
                else
                    //outputStrUntrimmed := TRunExecutable.RunDosStdinStdout(RamBinary, inputString);
                    outputStrUntrimmed := TRunExecutable.runDostoString(RamBinary + ' ' + inputString, TempPath, false, true, memoFirstLineStr, mmSolverMessages);

                {write out to file, for testing}
                //thelper.WriteStringToFile('solverOut.txt',outputStrUntrimmed);

                {solver error checking sometimes returns an empty string. Check for this before continuing.}
                if delegate.getPCancelSolve.b then
                    Exit
                else
                if outputStrUntrimmed = '' then
                    showmessage('Solver returned no output') //TODO check conditions that cause this
                else
                begin
                    TLs.fromDelimitedStringKeepSize(outputStrUntrimmed, ' ',false,false);

                    {for each point and freq, put the source level - transmission loss - water attenuation.
                    NB ramGEO already returns TL as dB so no need to convert to dB. Not sure what happens
                    when it gives a NaN, how that works?}
                    for j := 0 to delegate.getDMax - 1 do
                    begin
                        for i := 0 to imax - 1 do
                        begin
                            if excludeTL(TLs[i, j]) then
                                self.setTLbyInd(n, i, j, fi + startfi, NaN)
                            else
                            begin
                                {calculate the seawater attenuation}
                                WaterAttenuation := TMCKLib.Distance2D(0,
                                                               ranges(i),
                                                               aPos.Z,
                                                               delegate.depths(j)) *
                                                    delegate.getWater.seawaterAbsorptionCoefficient(TSpectrum.getFbyInd(fi + startfi,
                                                                                  bw),
                                                                                  (aPos.Z + delegate.depths(j))/2);
                                self.setTLbyInd(n, i, j, fi + startfi, TLs[i, j] + WaterAttenuation);
                            end;   //NaN
                        end;    //i
                    end;//j
                end; //solver returned output

                self.updateSolverUIMessages(startfi,fi,movingPosIndex,n,1,useLowFreqSolverText,bw,lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
                application.ProcessMessages;

                {check if user pressed the cancel button on the misc page}
                if delegate.getPCancelSolve.b then
                    Exit;
            end; // nslices
        end; //level[fi] = nan
    end; // lenfi
    mmSolverMessages.Visible := false; //hide the memo
end;

procedure TSourceSolve.solveRamCloud(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: Boolean; const lblSolving, lblSolveExtraInfo, lblSliceNumber, lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool; const mmSolverMessages: TMemo);
    function DelDoubleSpaces(const s: String): string;
    var
        i: integer;
    begin
        result := '';

        for i := 1 to length(s) do
        begin
            if s[i] = ' ' then
            begin
                if (i = 1) or not(s[i - 1] = ' ') then
                    result := result + ' ';
            end
            else
                result := result + s[i];
        end;
    end;

    procedure splitDataToTL(const TL: TMatrix; inString: string);
    var
        p: TPair<integer,string>;
        q, sa: TArray<String>;
        i: integer;
    begin
        if length(TL) = 0 then exit;
        TL.setAllVal(nan);

        q := inString.replace(#13#10,#10).Split([#10]);
        for p in THelper.iterate<string>(q) do
        begin
            if p.Key >= length(TL) then break;

            sa := p.Value.Split([' ']);
            for i := 0 to length(sa) - 1 do
                if (i < length(TL[p.key])) and isFloat(sa[i]) then
                    TL[p.key,i] := tofl(sa[i]);
        end;
    end;

    procedure parseServerResponse(const solverOut: string; const bw: TBandwidth; const freqLookup: TDictionary<string,integer>);
    var
        solvedData: ISmart<TCloudFreqSliceList>;
        TLs: TMatrix; // calculated transmission losses
        p: TCloudFreqSlice;
        i,j: integer;
        WaterAttenuation: double;
    begin
        solvedData := TSmart<TCloudFreqSliceList>.create(self.parseJSONSolverOutput(solverOut, freqLookup));
        if not assigned(solvedData) then exit;

        setlength(TLs, imax, delegate.getDMax);
        for p in solvedData do
        begin
            splitDataToTL(TLs, p.data);

            {for each point and freq, put the source level - transmission loss - water attenuation}
            for j := 0 to delegate.getDMax - 1 do
            begin
                for i := 0 to imax - 1 do
                begin
                    if excludeTL(TLs[i,j]) then
                        self.setTLbyInd(p.slice, i, j, p.fi + startfi, NaN)
                    else
                    begin
                        {calculate the seawater attenuation}
                        WaterAttenuation := TMCKLib.Distance2D(
                            0,
                            ranges(i),
                            aPos.Z,
                            delegate.depths(j)) *
                            delegate.getWater.seawaterAbsorptionCoefficient(TSpectrum.getFbyInd(p.fi + startfi,bw),
                            (aPos.Z + delegate.depths(j))/2);
                        self.setTLbyInd(p.slice, i, j, p.fi + startfi, TLs[i, j] + WaterAttenuation);
                    end; //NaN
                end; //i
            end;//j
        end;
    end;
const
    bShowSolveProgress  = false;
var
    solverOut           : string;
    inputStrings        : ISmart<TStringList>;
    inputString         : string;
    n, fi               : integer;
    bw                  : TBandwidth;
    zprofile            : TMatrix; // bathymetry profile along solution vector
    tmpCprofile         : TMatrix;
    solveFreqs          : String;
    freqLookup          : ISmart<TDictionary<string,integer>>;
    s                   : string;
    cs                  : ISmart<TCloudSolve>;
begin
    {check that the number of z divisions won't cause ramgeo to fail. NB this is controlled
    by parameter mz in ramMDAn.f, and also how deep the subbottom is below the seafloor, so
    update these parameters if either of those factors changes.}
    if (3*(delegate.getDMax + 2) > 4000) then
    begin
        showmessage('Number of depth points too large for solver, reduce depth points to solve');
        Exit;
    end;

    bw := delegate.getBandwidth;
    solveFreqs := '';
    mmSolverMessages.Visible := bShowSolveProgress; //this call moved out of the loop so the memo doesn't show and hide constantly
    try
        inputStrings := TSmart<TStringList>.create(TStringList.create);

        freqLookup := TSmart<TDictionary<string,integer>>.create(TDictionary<string,integer>.create());
        for fi := 0 to lenfi - 1 do
        begin
            if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
                setNaNForFiPlusStartFi(fi + startfi)
            else
            begin
                s := tostrWithTestAndRound1(TSpectrum.getFbyInd(fi + startfi,bw));
                freqLookup.add(s,fi);
                solveFreqs := solveFreqs + ifthen(solveFreqs.IsEmpty,'',',') + s;
            end;
        end;

        if not solveFreqs.IsEmpty then
        begin
            lblSolving.Caption := self.Name + '   RAM cloud';
            application.ProcessMessages;

            for n := 0 to self.nslices - 1 do
            begin
                zprofile := bathyDelegate.getBathymetryVector(aPos.X, aPos.Y, angles(n), rmax, self.iExtendPastEdge);

                {add the water current velocity to the cprofile for this angle
                the projection of the water current along the current vector is
                V*cos(angle between current and current vect)}
                tmpCprofile := TSSP.removeDuplicateDepths(delegate.getCProfileAtAngle(angles(n)));

                { write the input string which gets appended to the call to the external fortran program }
                inputStrings.add(TExternalSolvers.writeRAMInput
                  (TSpectrum.getFbyInd(startfi, bw), aPos.Z, rmax, self.dr, aPos, zprofile, tmpCprofile, false));
            end;
            inputString := self.JSONFormatSolverInput(solveFreqs,'ram',inputStrings);
            //{$DEFine WRITETESTRAMINPUT}
            {$IF Defined(WRITETESTRAMINPUT) and Defined(Debug)}
            thelper.writeStringToFile('input.json', inputString);
            {$ENDIF}

            cs := TSmart<TCloudSolve>.create(TCloudSolve.create);
            cs.pbStep := stepProgressBars;
            solverOut := cs.postProgress(sSolveServer, inputString);
            //{$DEFine WRITETESTRAMOUTPUT}
            {$IF Defined(WRITETESTRAMOUTPUT) and Defined(Debug)}
            thelper.writeStringToFile('cloudSolveOut.json', solverOut);
            {$ENDIF}
            solverOut := DelDoubleSpaces(solverOut);

            if delegate.getPCancelSolve.b then
                Exit
            else if solverOut = '' then
                showmessage('Solver returned no output')
            else
                parseServerResponse(solverOut, bw, freqLookup);

            self.updateSolverUIMessages(startfi,lenfi-1,movingPosIndex,nSlices-1,1,useLowFreqSolverText,bw,lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
            application.ProcessMessages;

            if delegate.getPCancelSolve.b then Exit;
        end;
    finally
        mmSolverMessages.Visible := false;
    end;
end;

procedure TSourceSolve.solveBellhop(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: Boolean; const lblSolving, lblSolveExtraInfo, lblSliceNumber, lblSolveSourcePosition: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool; const mmSolverMessages: TMemo);
var
    outputStrUntrimmed, outputStrTrimmed: string;
    memoFirstLineStr: string;
    tls: TMatrix; // calculated transmission losses
    i, j, n, fi: integer;
    // spect : TSpectrum;
    bw: TBandwidth;
    // d : double;
    zprofile: TMatrix; // bathymetry profile along solution vector
    tmpCprofile: TMatrix;
    WaterAttenuation: double;
    // recPos : TPos3;
    sBellhopInput: string;
    // bellhopFile : TextFile;
    sBellhopFile: string;
    sBathymetry: string;
    // bathymetryFile : TextFile;
    sBathymetryFile: string;
    // buttonSelected : integer;
    bShowSolveProgress: Boolean;
begin
    { check if a previous solver process is still open, then ask for confirmation to kill it }
    if THelper.TestAndKillProcess(BellhopBinary) = 2 then
        Exit;

    bw := delegate.getBandwidth;
    bShowSolveProgress := true;
    mmSolverMessages.Visible := bShowSolveProgress; //this call moved out of the loop so the memo doesn't show and hide constantly
    for fi := 0 to lenfi - 1 do
    begin
        if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
            setNaNForFiPlusStartFi(fi + startfi)
        else
        begin
            lblSolving.Caption := self.Name + '   Bellhop ' + TSpectrum.getFStringbyInd(fi + startfi, bw) + ' Hz...';
            application.ProcessMessages;
            for n := 0 to nslices - 1 do
            begin

                zprofile := bathyDelegate.getBathymetryVector(aPos.X, aPos.Y, angles(n), rmax, self.iExtendPastEdge);

                {add the water current velocity to the cprofile for this angle
                the projection of the water current along the current vector is
                V*cos(angle between current and current vect)}
                tmpCprofile := TSSP.removeDuplicateDepths(delegate.getCProfileAtAngle(angles(n)));

                { write the input string which gets appended to the call to the external fortran program }
                sBellhopInput := TExternalSolvers.writeBellhopInput
                  (TSpectrum.getFbyInd(fi + startfi, bw), aPos.Z, rmax, self.dr, aPos, zprofile);

                { write the input string to the temp .env file }
                sBellhopFile := TempPath + 'bellhop.env';
                system.SysUtils.DeleteFile(sBellhopFile);
                if not THelper.WriteStringToFile(sBellhopFile,sBellhopInput) then
                begin
                    showmessage('Unable to write to temporary file, solve aborted');
                    Exit;
                end;

                { write the string to be written to the Bathymetry input file }
                sBathymetry := TExternalSolvers.writeBellhopBathymetry(zprofile);
                // showmessage(bathymetryString);

                { write the bathymetry string to the temp BTYFIL file }
                sBathymetryFile := TempPath + 'BTYFIL';
                system.SysUtils.DeleteFile(sBathymetryFile);
                if not THelper.WriteStringToFile(sBathymetryFile,sBathymetry) then
                begin
                    showmessage('Unable to write to temporary file, solve aborted');
                    Exit;
                end;

                { * call the bellhop model, from Mike Collins (US Navy).
                  I've recompiled it with a few changes, as bellhopMDAn. It is written in fortran.
                  Accepts an input environment *.env file, and produces SHDFIL which is a text file
                  with the calculated transmission losses at the grid of points specified in the
                  input file.* }

                // TODO there is an out of memory problem with bellhop.
                memoFirstLineStr := self.name + ' slice ' + tostr(n + 1) + ' of ' + tostr
                  (nslices) + ', ' + tostr(TSpectrum.getFbyInd(fi + startfi, bw))
                  + ' Hz';
                outputStrUntrimmed := TRunExecutable.runDostoString(BellhopBinary, TempPath, false, bShowSolveProgress,
                    memoFirstLineStr, mmSolverMessages); // show console output when solving because it is so slow

                //{write out to file, for testing}
                //WriteStringToFile('solver.out',outputStrUntrimmed);

                {Bellhop sometimes returns an empty string. Check for this before continuing.}
                if delegate.getPCancelSolve.b then
                    Exit
                else if outputStrUntrimmed = '' then
                    showmessage('Solver returned no output') //TODO check conditions that cause this
                else if SubStringOccurencesi('TL Data', outputStrUntrimmed) = 0 then
                    showmessage(wraptext('Solver failed and returned: ' + outputStrUntrimmed))
                else
                begin
                    { Bellhop likes to return error messages before actual output, so trim those from the return string }
                    outputStrTrimmed := TExternalSolvers.trimSolverReturnString(outputStrUntrimmed);

                    //{write out to file, for testing}
                    //WriteStringToFile('bellhopTrim.out',outputStrTrimmed);

                    { I have swapped output matrix orientation of bellhop return to match ramMDA }
                    setlength(TLs, imax, delegate.getDMax);
                    { this is the version where we get back the result from stdout, no need for a file
                      it is all happening in memory so runs a bit quicker }

                    TLs.fromDelimitedStringKeepSize(outputStrTrimmed, ' ',false,false);
                    // readDelimitedString(runDostoString('bellhopMDA1.exe ' + inputString),' ',0,TLs);

                    //{write out to file, for testing}
                    //WriteStringToFile('bellhopTLs.out',serialiseMatrix(TLs));

                    {for each point and freq, put the source level - transmission loss}
                    for i := 0 to imax - 1 do
                        for j := 0 to delegate.getDMax - 1 do
                        begin
                            { currently bellhopMDAn returns linear sound pressures, so we need to convert to dB.
                              it would be possible to get it to directly return dB, but then what to do with the 0 values? }
                            if TLs[i, j] = 0 then
                                TLs[i, j] := NaN
                            else
                                TLs[i, j] := -20 * log10(TLs[i, j]); //Transmission Losses, hence the negative. also this is consistent with ramGEO

                            if excludeTL(TLs[i,j]) then
                                self.setTLbyInd(n, i, j, fi + startfi, NaN)
                            else
                            begin
                                {calculate the seawater attenuation}
                                WaterAttenuation := TMCKLib.Distance2D(0,
                                                               ranges(i),
                                                               aPos.Z,
                                                               delegate.depths(j)) *
                                                    delegate.getWater.seawaterAbsorptionCoefficient(TSpectrum.getFbyInd(fi + startfi,
                                                                                  bw),
                                                                                  (aPos.Z + delegate.depths(j))/2);
                                {store the resulting level in the TSource}
                                self.setTLbyInd(n, i, j, fi + startfi, TLs[i, j] + WaterAttenuation);
                            end;
                        end;
                end;

                self.updateSolverUIMessages(startfi,fi,movingPosIndex,n,1,useLowFreqSolverText,bw,lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
                application.ProcessMessages;

                {check if user pressed the cancel button on the misc page}
                if delegate.getPCancelSolve.b then Exit;
            end; // nslices
        end; //level[fi] = nan
    end; // lenfi
    mmSolverMessages.Visible := false;
end;

procedure TSourceSolve.solveKraken(const startfi, lenfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const mmSolverMessages: TMemo; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    fi: integer;
    sl: TStringlist;
    TmpSubDir: string;
    tmpDelDir: string;
    bw: TBandwidth;
begin
    {check if there is a subdirectory \tmp\ below our current temp dir, which we use for field.exe files}
    TmpSubDir := IncludeTrailingPathDelimiter(TempPath) + 'tmp\';
    if (not DirectoryExists(TmpSubDir)) and (not CreateDir(TmpSubDir)) then
    begin
        showmessage('Unable to create directory ' + TmpSubDir);
        Exit;
    end;

    {cleanup from any previous failed solves}
    THelper.DeleteMatchingFilesInDir(TempPath,'MODFil');
    THelper.DeleteMatchingFilesInDir(TmpSubDir,'tmpModes');

    bw := delegate.getBandwidth;
    {note that we don't do (fi + startfi) here, as it is included later on, within the
    solveKrakenFreq function itself}
    for fi := 0 to lenfi - 1 do
        if not delegate.getPCancelSolve.b then
        begin
            if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
                setNaNForFiPlusStartFi(fi + startfi)
            else
                solveKrakenFreq(fi,startfi,movingPosIndex,aPos,useLowFreqSolverText,
                    lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition,mmSolverMessages,stepProgressBars,nil,nil,estimator,updateRemainingSolveTime);
        end;

    {cleanup the temp directory - delete all files under the \tmp\ subdir}
    sl := THelper.GetSubDirectories(TmpSubDir);
    for tmpDelDir in sl do
    begin
        THelper.DeleteMatchingFilesInDir(tmpDelDir,'tmpModes');
        RemoveDir(tmpDelDir);
    end;
end;

procedure TSourceSolve.solvedBSeaRayThreaded(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer;
    const aPos: TFPoint3;
    const useLowFreqSolverText: Boolean;
    const coherent, calculateVolumeAttenuationAtEachStep: Boolean;
    const raySolverIterationMethod: TRaySolverIterationMethod;
    const levelType: TLevelType;
    const allErrorMessages: TStringList;
    const lbSolving, lbExtraSolveInfo, lbSliceNumber, lbSolveSourcePosition: TLabel;
    const stepProgressBars: TProcedureWithInt;
    const setProgressBars: TProcedureWithDouble;
    const estimator: TSolveTimeEstimator;
    const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    fi                  : integer;
    multipleSolveFrequencies : TDoubleDynArray;
    bw                  : TBandwidth;
    material            : TMaterial;
    c,rho               : double;
    pCancelSolve        : TWrappedBool;
    admax               : integer;
    seabedArrays        : array of TSeafloorRangeArray;
    aWater              : TWaterProps;
    aDepths             : TDoubleDynArray;
    globalZMax          : double;
    solveThisFreq       : TBooleanDynArray;
    nn                  : integer;
    zprofiles2          : array of TRangeValArray;
    tmpCProfiles3       : array of TDepthValArrayAtRangeArray;
    fractionDone, outputFractionDone        : ISmart<TDoubleList>;
    solverErrors        : TArray<TSolverErrors>;
    solvedSlices        : TBooleanDynArray;
    deaths              : TRayDeaths;
    collectedErrors     : ISmart<TErrorDict>;
    thisPos             : TFPoint3;
    lprs                : ISmart<TObjectList<TLPRangeList>>;
begin
    if self.isTimeSeries and (nFreqsPerBand <> 1) then
        raise ERayTracerError.Create('Ray tracer in time series mode cannot have more than 1 solve frequency per band');

    collectedErrors := TSmart<TErrorDict>.create(TErrorDict.create);
    bw := delegate.getBandwidth;
    //don't solve if time series and we are all above nyquist
    if (lenfi = 0) or
       (self.isTimeSeries and (TSpectrum.getFbyInd(startfi, bw) > self.timeSeries.fs / (2 * sqrt(2)))) then
    begin
        self.updateSolverUIMessages(startfi,lenfi,movingPosIndex,nSlices,lenfi,useLowFreqSolverText,bw,lbSliceNumber,lbSolveSourcePosition,lbExtraSolveInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
        allErrorMessages.add('Source ' + self.name + ': no active frequency bands can be solved for this sample rate');
        exit;
    end;

    thisPos.setPos(apos);
    setlength(solvedSlices, nslices);
    admax := delegate.getdMax;
    pCancelSolve := delegate.getpcancelSolve;
    globalZMax := bathyDelegate.getzMax;
    //aSeafloorScheme := delegate.getSeabed;
    awater := delegate.getWater;
    aDepths := delegate.depthsArray();

    setlength(solverErrors,self.nSlices);
    setlength(zprofiles2,self.nSlices);
    SetLength(tmpCProfiles3,self.nSlices);
    //setlength(fractionDone,self.nSlices);
    //setlength(timeFractionDone,self.nSlices);
    fractionDone := TSmart<TDoubleList>.create(TDoubleList.create(true));
    outputFractionDone := TSmart<TDoubleList>.create(TDoubleList.create(true));
    fractionDone.init(nSlices);
    outputFractionDone.init(nSlices);

    setlength(seabedArrays,self.nSlices);
    lprs := TSmart<TObjectList<TLPRangeList>>.create(TObjectList<TLPRangeList>.create(true));
    for nn := 0 to nSlices - 1 do
    begin
        zprofiles2[nn] := bathyDelegate.getBathymetryVector2(apos.x, apos.y, self.angles(nn), rmax, self.iExtendPastEdge);

        lprs.add(delegate.getLocationProps(aPos, self.angles(nn), rMax, self.dr));
        tmpCprofiles3[nn] := lprs[nn].getCProfiles(angles(nn)); //add the water current velocity to the cprofile for this angle the projection of the water current along the current vector is V*cos(angle between current and current vect)
        seabedArrays[nn] := lprs[nn].getSeabeds;

        //tmpCprofiles3[nn] := TSSP.removeDuplicateDepths2(delegate.getCProfileAtAngle2(angles(nn)));
        //sliceRayss.addSlice(nn);
    end;

    //setup for multi freq solve
    setlength(multipleSolveFrequencies,lenfi);
    setlength(solveThisFreq,lenfi);
    for fi := 0 to lenfi - 1 do
    begin
        multipleSolveFrequencies[fi] := TSpectrum.getFbyInd(fi + startfi, bw);
        solveThisFreq[fi] := self.isTimeSeries or not isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw));
        //don't solve bands above the nyquist
        if self.isTimeSeries and (multipleSolveFrequencies[fi] > self.timeSeries.fs / (2 * sqrt(2))) then
            solveThisFreq[fi] := false;
    end;

    TRayDeathsHelper.init(deaths);
    material := delegate.topLayerMaterial(aPos.X,aPos.Y);
    c := material.c;
    rho := material.rho_g;

    lbSolving.Caption := self.Name + '   dBSeaRay ' + TSpectrum.getFStringbyInd(startfi, bw) +
                                    ' to ' + TSpectrum.getFStringbyInd(startfi + lenfi - 1, bw) + ' Hz';
    application.ProcessMessages;

    Parallel.For(0, nSlices - 1)
        .NoWait //run in background
        .Execute(procedure (n: integer)
    begin
        if assigned(pCancelSolve) and not pCancelSolve.b then
        begin
            self.solvedBSeaRayThreadedLoop(n, nFreqsPerBand,
                                           bw,
                                           multipleSolveFrequencies,
                                           solveThisFreq,
                                           c, rho, globalZMax, delegate.getRayTracingStartAngle, delegate.getRayTracingEndAngle, delegate.getRayTracingInitialStepsize,
                                           startfi, admax, delegate.getDBSeaRayMaxBottomReflections, delegate.getBellhopNRays,
                                           thisPos,
                                           levelType,
                                           pCancelSolve,
                                           useLowFreqSolverText, coherent, calculateVolumeAttenuationAtEachStep,
                                           raySolverIterationMethod,
                                           seabedArrays[n],
                                           aWater,
                                           aDepths,
                                           zprofiles2[n],
                                           tmpCProfiles3[n],
                                           fractionDone[n],
                                           outputFractionDone[n],
                                           nil,//sliceRayss[n],
                                           solverErrors[n],
                                           deaths);
            solvedSlices[n] := true;
        end;
    end);

    while not (solvedSlices.alltrue or (assigned(pCancelSolve) and pCancelSolve.b)) do
    begin
        sleep(500);
        Application.ProcessMessages;
        self.updateSolverUIMessages2(movingPosIndex,solvedSlices.counttrue,estimator.cSolverStartTime,
            (movingPosIndex + (fractionDone.totalFractionDone / 2)
                + (outputFractionDone.totalFractionDone / 2)) / getMovingPosMax,
            lbSliceNumber,lbSolveSourcePosition,lbExtraSolveInfo,setProgressBars);
    end;

    for nn := 0 to nSlices - 1 do
    begin
        collectedErrors.addAll(solverErrors[nn],-1,nn);
        //fractionDone[nn].free;
        //timeFractionDone[nn].free;
    end;
    collectedErrors.appendToStringList(allErrorMessages);
    eventlog.log('Ray solver (threaded) finished. Ray deaths: ' + TRayDeathsHelper.summary(deaths));
end;

procedure TSourceSolve.updateSolverUIMessages(const startfi, fi, movingPosIndex, sliceNumber, incrementBarsBy: integer; const useLowFreqSolverText : boolean; const bw : TBandwidth; const lbSliceNumber, lbSolveSourcePosition, lbExtraSolveInfo: TLabel; const stepProgressBars: TProcedureWithInt; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    now : cardinal;
    //secs: integer;
begin
    now := timegettime;
    //secs := (now - estimator.cSolverStartTime) div 1000;
    if (now - cLastlblSliceNumberLastUpdated) > updateUIMessagesIntervalMS then
    begin
        cLastlblSliceNumberLastUpdated := now;
        //lbExtraSolveInfo.Caption := inttostr(round(100* totalFractionDone)) + '% done in ' + formatSecs(secs);
        lbSliceNumber.Caption := tostr(sliceNumber) + ' of ' + tostr(self.NSlices) + ' slices finished';
        lbSolveSourcePosition.Caption := 'Source position ' + tostr(movingPosIndex + 1) + ' of ' + tostr(self.getMovingPosMax);
    end;

    stepProgressBars(incrementBarsBy);

    if (now - cLastlblEstimatedSolveTimeLastUpdated) > updateUIMessagesIntervalMS then
    begin
        cLastlblEstimatedSolveTimeLastUpdated := now;
        if useLowFreqSolverText then
            estimator.iCurrentLowSlicesTimesFreq := estimator.iCurrentLowSlicesTimesFreq + saferound(TSpectrum.getFbyInd(startfi + fi,bw))
        else
            estimator.iCurrentHighSlicesTimesFreq := estimator.iCurrentHighSlicesTimesFreq + saferound(SrcSpectrum.getFbyInd(startfi + fi,bw));
        UpdateRemainingSolveTime(useLowFreqSolverText,delegate.getSplitSolver);
    end;
end;

procedure TSourceSolve.updateSolverUIMessages2(const movingPosIndex, sliceNumber: integer; const cSolverStartTime: cardinal; const totalFractionDone: double; const lbSliceNumber, lbSolveSourcePosition, lbExtraSolveInfo: TLabel; const setProgressBars: TProcedureWithDouble);
var
    secs: integer;
begin
    secs := (timeGetTime - cSolverStartTime) div 1000;
    lbExtraSolveInfo.Caption := floattostrf(100* totalFractionDone, ffFixed, 14,2) + '% done in ' + formatSecondsToHHMMSS(secs);
    lbSliceNumber.Caption := tostr(sliceNumber) + ' of ' + tostr(self.NSlices) + ' slices finished';
    lbSolveSourcePosition.Caption := 'Source position ' + tostr(movingPosIndex + 1) + ' of ' + tostr(self.getMovingPosMax);
    setProgressBars(totalFractionDone);
end;

procedure TSourceSolve.setFieldbyInd(const n, i, j, fi : integer; const c : TComplex);
begin
    self.fSrcField[self.getTLIndex(n, i, j, fi)] := c;
end;

procedure TSourceSolve.setNaNForFiPlusStartFi(const fiPlusStartFi: integer);
var
    n,i,j: integer;
begin
    for n := 0 to self.nslices - 1 do
        for j := 0 to delegate.getDMax - 1 do
            for i := 0 to self.imax - 1 do
                self.setTLbyInd(n, i, j, fiPlusStartFi, NaN);
end;

procedure TSourceSolve.getZMaxAndRmaxFromZProfile(const zprofile: TRangeValArray; const forceContinuePastLand: boolean; out zmax, rmax : double);
var
    j: integer;
    bHaveHitLand, stopAtLand: boolean;
begin
    stopAtLand := delegate.getStopMarchingSolverAtLand and not forceContinuePastLand;
    zmax := zprofile[0].val;
    rMax := zprofile[0].range;
    bHaveHitLand := isnan(zmax) or (stopAtLand and (zmax <= 0));
    for j := 1 to length(zprofile) - 1 do
    begin
        {we get zmax anywhere in the profile, even after land. This may not be the most efficient, now that we are letting the
        solver stop when it hits land}
        if zprofile[j].val > zmax then
            zmax := zprofile[j].val;

        if (not bHaveHitLand) then
        begin
            if j < length(zprofile) - 1 then
                rMax := zprofile[j + 1].range  //extend 1 step onto land
            else
                rMax := zprofile[j].range;
            if isnan(zprofile[j].val) or (stopAtLand and (zprofile[j].val <= 0)) then
                bhaveHitLand := true;
        end;
    end;
end;

function TSourceSolve.excludeTL(const d: double): boolean;
begin
    result := isNaN(d) or (d > self.dSolverReturnMaxTL) or (d <= 0);
end;

function TSourceSolve.JSONFormatSolverInput(const solveFreqs, solver: string; const strings: TStringlist): string;
    function myFormat(const s: string): string;
    begin
        result := '"' + s.Replace(#13#10,'","');
        if result.EndsWith(',"') then
            setlength(result, result.Length - 2)
        else
            result := result + '"';
    end;
var
    n: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    //use linux line endings
    sb.append('['#10#09'{'#10);

    sb.append(#09#09'"freqs": [' + solveFreqs + '],'#10);
    sb.append(#09#09'"solver": "' + solver + '",'#10);
    sb.append(#09#09'"data": ['#10);
    for n := 0 to strings.Count - 1 do
        sb.append(ifthen(n = 0,'',','#10) + #09#09#09'[' + myFormat(strings[n]) + ']');
    sb.append(#10#09#09']'#10);

    sb.append(#09'}'#10']');
    result := sb.ToString;
end;


function TSourceSolve.parseJSONSolverOutput(const solverOut: string; const freqLookup: TDictionary<string,integer>): TCloudFreqSliceList;
var
    o: TJSONObject;
    solvers: TJSONArray;
    i: integer;
    d: TCloudFreqSlice;
    fi: integer;
begin
    try
        solvers := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(solverOut),0) as TJSONArray;
    except on e: exception do
        exit(nil);
    end;
    result := TCloudFreqSliceList.create(true);
    for i := 0 to solvers.count - 1 do
    begin
        o := TJSONObject(solvers.items[i]);
        //o{solver, freq, slice, output}
        if not freqLookup.tryGetValue(o.get('freq').JsonValue.ToString,fi) then
            continue;

        d := TCloudFreqSlice.create();
        d.fi := fi;
        d.slice := o.get('slice').JsonValue.ToString.ToInteger();
        d.data := o.Get('output').JsonValue.ToString;
        result.add(d);
    end;
end;

function    TSourceSolve.TL20log(const pos1,pos2: TFPoint3; const r0 : double): Double;
{calculate simple geometric spherical spreading, purely based on distance}
var
    dist : double;
begin
    dist := pos1.dist(pos2);
    if dist < r0 then
        dist := r0;
    Result := dB20(dist/r0);
end;

function    TSourceSolve.TL10log(const pos1,pos2: TFPoint3; const r0: double): double;
{calculate simple cylindrical spherical spreading, purely based on distance}
var
    dist: double;
begin
    dist := pos1.dist(pos2);
    if dist < r0 then
        dist := r0;
    Result := dB10(dist/r0);
end;

function TSourceSolve.dCountAndMitigationCalc(const dCount, dMitigation: double): double;
begin
    result := unDB20(dMitigation);
    if isnan(result) then
    begin
        if isnan(dcount) then
            result := 1
        else
            result := sqrt(dcount);
    end
    else
    if not isnan(dcount) then
        result := result * sqrt(dcount);
end;

procedure TSourceSolve.solveKrakenFreq(const fi, startfi, movingPosIndex: integer; const aPos: TFPoint3; const useLowFreqSolverText: boolean; const lblSolving,lblSolveExtraInfo,lblSliceNumber,lblSolveSourcePosition: TLabel; const mmSolverMessages: TMemo; const stepProgressBars: TProcedureWithInt; const pbSolveProgress : TProgressbar; const taskbarProgress : TTaskbarProgress; const estimator: TSolveTimeEstimator; const UpdateRemainingSolveTime: TProcedureWithBoolBool);
{solving kraken requires first running kraken to generate modeshape files for
each required depth. then field.exe is used to solve for the field along each
ray, generating a shadefile (file containing TLs)}
var
    outputStrUntrimmed, outputStrTrimmed: string;
    sMemoFirstLine: string;
    TLs, tmpTLs: TMatrix; // calculated transmission losses
    hi, i, j, n: integer;
    iLastWaterInd: integer;
    bw: TBandwidth;
    dLocalDepth, dMaxDepth: double;
    bShowKrakenSolveProgress, bShowFieldSolveProgress: boolean;
    maZProfile: TMatrix; // bathymetry profile along solution vector
    maZProfileTrimmed: TMatrix; //bathymetry, with repeated values trimmed out
    tmpCprofile: TMatrix;
    vAllDepths: TDoubleDynArray; //all depths where we need to calculate modes
    iPrevDepthsLength: integer;
    WaterAttenuation: double;
    sKrakenFile: string;
    sFieldFile: string;
    sKrakenInput: string;
    sFieldInput: string;
    tempFreqDir: string;
    sModeFile1, sModeFile2: string;
    iTmpNFieldCalcs: integer;
    sTmpSolvingCaption: string;
    bCouldNotMoveFileMessageShown: boolean;
    lowFreqDepthThreshold: double;
begin
    //TODO this is working on a per source basis, ideally we would
    //not be repeating the initial steps for each source
    //ie get the modefiles needed for all sources not just this one

    //TODO can't handle water current, as that would mean creating
    //modefiles for each individual angle - cprofile will be slightly different for each

    { check if a previous solver process is still open, then ask for
    confirmation to kill it. exit if it exists and is not killed}
    if THelper.TestAndKillProcess(KrakenBinary) = 2 then
        Exit;
    if THelper.TestAndKillProcess(FieldBinary) = 2 then
        Exit;

    bShowKrakenSolveProgress := true;
    bShowFieldSolveProgress := true;
    try
        bw := delegate.getBandwidth;
        //setlength(vaAllDepths,lenfi); //vaAllDepths has 1 TDoubleDynArray for each freq

        {check if there is a subdirectory \tmp\ below our current temp dir, which we
        use for field.exe files}
        if not DirectoryExists(TempPath + 'tmp\' + TSpectrum.getFStringbyInd(fi + startfi,bw) + '\') then
            if not CreateDir(TempPath + 'tmp\' + TSpectrum.getFStringbyInd(fi + startfi,bw) + '\') then
                begin
                    showmessage('Unable to create subdirectory in ' + TempPath + 'tmp\');
                    Exit;
                end;

        {find all the different depths. we create a modefile for each individual depth}
        setlength(vAllDepths,0);
        for n := 0 to nslices - 1 do
        begin
            maZProfile := bathyDelegate.getBathymetryVector(aPos.X, aPos.Y, angles(n), rmax, self.iExtendPastEdge); //getBathymetryVector also does rounding

            iPrevDepthsLength := Length(vAllDepths);
            setlength(vAllDepths,iPrevDepthsLength + Length(maZProfile[0]));
            for i := 0 to high(maZProfile[0]) do
                vAllDepths[iPrevDepthsLength + i] := maZProfile[1,i];
        end; // nslices

        vAllDepths.sort;
        {return a sorted matrix with only each unique depth, so we don't need to
        calculate the same modes twice}
        vAllDepths := vAllDepths.unique;

        {we need to get, for each frequency, the list of depths that exceed the low freq cutoff. this is given by
        f*depth = 1515 approx. So we now have an array vaAlldepths containing the unique vAllDepths for each freq}

        {update 20.08.13: we now use 2 media, with water and sediment. then below the sediment is a vacuum. so the
        threshold is not such a problem. but we need some way of checking that the sediment thickness is
        adequate on shallow problems, and not too large on deep problems.}
        lowFreqDepthThreshold := 1550/TSpectrum.getFbyInd(fi + startfi, bw); //lowFreqDepthThreshold is a depth, in m

        //TODO this factor (10) should scale with the wavelength and depth. 10 works ok for shallow problems, but
        //takes forever on greater depths. needs to correspond to the factor in krakeninputstring
        if vAllDepths.max*10 < lowFreqDepthThreshold then
        begin
            Showmessage(wraptext('Water depth is too shallow for this solver at this frequency (' +
                TSpectrum.getFStringbyInd(fi + startfi, bw) + '): minimum depth ' + tostr(saferound(lowFreqDepthThreshold/10)) +
                ' m'));
            Exit;
        end;

//        {check whether the depth below the source location is too shallow for this solver. ie it needs to
//        be obove lowFreqDepthThreshold}
//        if maZProfile[1,0] < lowFreqDepthThreshold then
//        begin
//            Showmessage(wraptext('Water depth at source is too shallow for this solver at this frequency (' +
//                spect.getFStringbyInd(fi + startfi, bw) + '): minimum depth ' + tostr(saferound(lowFreqDepthThreshold)) +
//                ' m'));
//            Exit;
//        end;
//
//        //lowFreqDepthThreshold := 0.1;
//        vAllDepths := arrayElementsAboveThreshold(vAllDepths,lowFreqDepthThreshold);


        {kraken.exe generates a modefile for each (greater than 0) depth with name
        MODFil0001. We want to take and store with different temp names, and in different drectories based on freq,
        all of those that we need, and when the time comes
        to call field.exe, copy all the required files to have the names
        MODFil0001, MODFil0002... in the correct sequence of depths}
        iTmpNFieldCalcs := 0;
        if assigned(pbSolveProgress) and assigned(taskbarProgress) then
        begin
            iTmpNFieldCalcs := pbSolveProgress.Max; //store the max value just for now
            sTmpSolvingCaption := lblSolving.Caption;
            lblSolving.Caption := self.Name + '   Kraken ' + TSpectrum.getFStringbyInd(fi + startfi, bw) + ' Hz modes...';
            pbSolveProgress.Max := high(vAllDepths);
            taskbarProgress.Max := high(vAllDepths);
            pbSolveProgress.Position := 0;
            taskbarProgress.Progress := 0;
        end;
        sKrakenInput := '';
        dMaxDepth := vAllDepths.max;

        {delete previous mode files in the dir}
        THelper.DeleteMatchingFilesInDir(TempPath,'MODFil');

        tmpCprofile := TSSP.removeDuplicateDepths(delegate.getCProfileAtAngle(angles(0))); //we don't use water current with calls to this solver, just take the 0 angle

        //j := 1;
        for i := 0 to high(vAllDepths) do
            if vAllDepths[i] > 0 then
            begin
                { write the input string which gets appended to the call to the external fortran program.
                kraken will read a bunch of concatenated 'input env files' and act as if they
                were separate files each with it's own run of kraken. produces mode files
                accordingly }
                dLocalDepth := vAllDepths[i];
                {frequency is ignored, but keep it for consistency}
                sKrakenInput := sKrakenInput + TExternalSolvers.writeKrakenInput
                (TSpectrum.getFbyInd(fi + startfi, bw), aPos.Z, rmax, self.dr, dLocalDepth, dMaxDepth, aPos, tmpCprofile);
                //j := j + 1;
            end;

        sKrakenFile := includetrailingpathdelimiter(TempPath) + 'kraken.in';
        system.SysUtils.DeleteFile(sKrakenFile);
        if not THelper.WriteStringToFile(sKrakenFile,sKrakenInput) then
        begin
            showmessage('Unable to write to temporary file ' + sKrakenFile+ ', solve aborted');
            Exit;
        end;

        {run kraken exe}
        mmSolverMessages.Visible := bShowKrakenSolveProgress;
        outputStrUntrimmed := TRunExecutable.runDostoString(KrakenBinary, includetrailingpathdelimiter(TempPath),
            false, bShowKrakenSolveProgress, sMemoFirstLine, mmSolverMessages);
        mmSolverMessages.Visible := false;

        j := 1;
        bCouldNotMoveFileMessageShown := false;
        tempFreqDir := includetrailingpathdelimiter(TempPath + 'tmp\' + TSpectrum.getFStringbyInd(fi + startfi,bw));
        for i := 0 to high(vAllDepths) do
            if vAllDepths[i] > 0 then
            begin
                {rename modefiles to a temp name. they are all moved to (temppath)\tmp\(frequency)\tmpModesxxxx.dat
                so under the tmp dir there will be a frequency dir for each calculation freq.}
                {modefile numbers based on j, as we don't know how many depths are <= 0. should just
                remove those from the vector earlier on.}
                sModeFile1 := TempPath + 'MODFil' + StringOfChar('0',3 - safefloor(log10(j))) + tostr(j);
                sModeFile2 := tempFreqDir + 'tmpModes' + tostr(i) + '.dat';
                system.SysUtils.DeleteFile(sModeFile2);
                if not (RenameFile(PChar(sModeFile1), PChar(sModeFile2))) then
                    if not bCouldNotMoveFileMessageShown then
                    begin
                        showmessage(wraptext('Could not rename mode file ' + sModeFile1 +
                            ' to ' + sModeFile2 + ', solve aborted : Error : ' + SysErrorMessage(GetLastError)));
                        showmessage(wraptext('Solver returned: ' + outputStrUntrimmed));
                        //bCouldNotMoveFileMessageShown := true;
                        Exit;
                    end;
    //            {$IFOPT I-}
    //            theIOerror := IOResult;
    //            {$ENDIF}
                j := j+1;
                stepProgressBars(1);
                application.ProcessMessages;

                if delegate.getPCancelSolve.b then
                    Exit;
            end;

        {now all the mode files are ready, set up and call field.exe}
        if assigned(pbSolveProgress) and assigned(taskbarProgress) then
        begin
            pbSolveProgress.Max := iTmpNFieldCalcs;
            pbSolveProgress.Position := 0;
            taskbarProgress.Max := iTmpNFieldCalcs;
            taskbarProgress.Progress := 0;
        end;
        lblSolving.Caption := self.Name + '   Kraken ' + TSpectrum.getFStringbyInd(fi + startfi, bw) + 'Hz solving...';
        mmSolverMessages.Visible := bShowFieldSolveProgress;//this call moved out of the loop so the memo doesn't show and hide constantly
        bCouldNotMoveFileMessageShown := false;
        for n := 0 to nslices - 1 do
        begin
            {cleanup hardlinks in the dir, as CreateHardLink won't overwrite an existing file/link.
            this is important when nslices > 1}
            THelper.DeleteMatchingFilesInDir(TempPath,'MODFil');

            {for this angle, get the Z Profile}
            maZProfile := bathyDelegate.getBathymetryVector(aPos.X, aPos.Y, angles(n), rmax, self.iExtendPastEdge);

            {find the first index where there is not water (depth <= 0) - actually the
            last index before that, ie the last index where there is still continuous water
            from the source. we can't calc modes where there is no water.}
            {update 1: this is now depth < threshold, where threshold is 1550/freq due to the way
            kraken works
            update 2: this is now depth <= 0 again, as we are using 2 media for kraken (sediment also)
            and so it can deal with shallow water depths. If we have not previously been
            stopped because of extreme low freq/shallow depth situation, we can continue}
            iLastWaterInd := 0;
            hi := high(maZProfile[0]);
//            {if the depth under the source is less than low freq threshold, then just exit. this case
//            should have been already handled, however}
//            if maZProfile[1,0] <= lowFreqDepthThreshold then
//                Exit;
            {go through and find the last depth < 0}
            while (iLastWaterInd + 1 <= hi) and (maZProfile[1,iLastWaterInd + 1] > 0) do
                iLastWaterInd := iLastWaterInd + 1;

            {create a matrix containing depths and ranges, but without repeated depth values
            (this will speed up the running of field.exe due to not having to repeatedly
            read the same profile). It is particularly effective for problems with
            deep, flat bottoms eg Munk, Dickins Seamount. NB we only remove the middle one of triply repeated values.
            not sure whether field interpolates depths between profiles - i guess that wouldn't make
            sense, given how the mode files work.}
            setlength(maZProfileTrimmed,2,1);
            maZProfileTrimmed[0,0] := maZProfile[0,0];
            maZProfileTrimmed[1,0] := maZProfile[1,0];
            j := 1; //the length of the trimmed profile
            for i := 1 to iLastWaterInd - 1 do
            begin
                if not((maZProfile[1,i] = maZProfile[1,i - 1]) and
                       (maZProfile[1,i] = maZProfile[1,i + 1])) then
                begin
                    setlength(maZProfileTrimmed,2,j + 1);
                    maZProfileTrimmed[0,j] := maZProfile[0,i];
                    maZProfileTrimmed[1,j] := maZProfile[1,i];
                    j := j + 1;
                end;
            end;
            {add the last profile}
            i := iLastWaterInd;
            begin
                setlength(maZProfileTrimmed,2,j + 1);
                maZProfileTrimmed[0,j] := maZProfile[0,i];
                maZProfileTrimmed[1,j] := maZProfile[1,i];
                //j := j + 1;
            end;

            {arrange the temp mode files (with hardlinks) into the correct sequence for this zprofile}
            //TODO check if there are ever cases where we don't get all the files becasue kraken failed
//            for i := 0 to iLastWaterInd do
//            begin
//                sModeFile1 := tempFreqDir + 'tmpModes' + tostr(arrayFind(maZProfile[1,i],vAllDepths)) + '.dat';
//                {append leading zeroes - the filename has 4 digits eg 0001 and starts from 1 not 0}
//                sModeFile2 := Mainform.TempPath + 'MODFil' + StringOfChar('0',3 - safefloor(log10(i + 1))) + tostr(i + 1);
//
//                {create a hard link to the file in the temp dir - NB this doesnt copy, so the data still
//                sits in the subdir. this saves a huge amount of file copying, particularly at high frequencies or deep depths}
//                 if not CreateHardLink(PChar(sModeFile2),PChar(sModeFile1),nil) then
//                    if not bCouldNotMoveFileMessageShown then
//                    begin
//                        showmessage(wraptext('Could not create link for mode file ' + sModeFile1 +
//                            ' to ' + sModeFile2 + ', solve aborted : Error : ' + SysErrorMessage(GetLastError)));
//                        bCouldNotMoveFileMessageShown := true;
//                        Exit;
//                    end;
//            end;

            {arrange the temp mode files (with hardlinks) into the correct sequence for this (trimmed) zprofile}
            for i := 0 to length(maZProfileTrimmed[0]) - 1 do
            begin
                sModeFile1 := tempFreqDir + 'tmpModes' + tostr(vAllDepths.find(maZProfileTrimmed[1,i])) + '.dat';
                {append leading zeroes - the filename has 4 digits eg 0001 and starts from 1 not 0}
                sModeFile2 := TempPath + 'MODFil' + StringOfChar('0',3 - safefloor(log10(i + 1))) + tostr(i + 1);

                {create a hard link to the file in the temp dir - NB this doesnt copy, so the data still
                sits in the subdir. this saves a huge amount of file copying, particularly at high frequencies or deep depths}
                 if not CreateHardLink(PChar(sModeFile2),PChar(sModeFile1),nil) then
                    if not bCouldNotMoveFileMessageShown then
                    begin
                        showmessage(wraptext('Could not create link for mode file ' + sModeFile1 +
                            ' to ' + sModeFile2 + ', solve aborted : Error : ' + SysErrorMessage(GetLastError)));
                        //bCouldNotMoveFileMessageShown := true;
                        Exit;
                    end;
            end;


            { * call field.exe   I've recompiled it with a few changes, as fieldMDAn. It is written in fortran.}

            sMemoFirstLine := self.name + ' slice ' + tostr(n + 1) + ' of ' + tostr
              (nslices) + ', ' + tostr(TSpectrum.getFbyInd(fi + startfi, bw))
              + ' Hz';

            sFieldInput := TExternalSolvers.writeFieldInput
              (TSpectrum.getFbyInd(fi + startfi, bw), aPos.Z, rmax, self.dr, iLastWaterInd, maZProfileTrimmed);

            sFieldFile := TempPath + 'field.in';
            system.SysUtils.DeleteFile(sFieldFile);
            if not THelper.WriteStringToFile(sFieldFile,sFieldInput) then
            begin
                showmessage('Unable to write to temporary file ' + sFieldFile + ', solve aborted');
                Exit;
            end;

            {current version reads input from field.in}
            system.SysUtils.DeleteFile(TempPath + 'SHDFIL');
            outputStrUntrimmed := TRunExecutable.runDostoString(FieldBinary,
                TempPath, false, true, sMemoFirstLine, mmSolverMessages); // show console output when solving because it is so slow

            //{write out to file, for testing}
            //WriteStringToFile('solver.out',outputStrUntrimmed);

            {check for errors}
            if delegate.getPCancelSolve.b then
                Exit
            else if outputStrUntrimmed = '' then
                showmessage('Solver returned no output')
            else if SubStringOccurencesi('TL Data', outputStrUntrimmed) = 0 then
                showmessage(wraptext('Solver failed and returned: ' + outputStrUntrimmed))
            else
            begin
                { trim the return string to be just TL Data}
                outputStrTrimmed := TExternalSolvers.trimSolverReturnString(outputStrUntrimmed);

                //WriteStringToFile('krakenRaw.out',outputStrTrimmed); // to write out raw Kraken data

                { this is the version where we get back the result from stdout, no need for a file
                  it is all happening in memory so runs a bit quicker }
                setlength(TLs, imax, delegate.getDMax);
                {use the function version, let the return data determine the size of TLs. because
                we may have sent data to field that was shorter than imax*jmax, due to water depth <=0}
                tmpTLs.fromDelimitedString(outputStrTrimmed,' ',false);
                for i := 0 to high(tmpTLs) - 1 do
                    for j := 0 to high(tmpTLs[0]) - 1 do
                        if (i < imax) and (j < delegate.getDMax) then //not too happy with this, should do some real checking on returned size
                            TLs[i,j] := tmpTLs[i,j];

                {for each point and freq, put the source level - transmission loss}
                for i := 0 to imax - 1 do
                    for j := 0 to delegate.getDMax - 1 do
                    begin
                        { currently bellhopMDAn returns linear sound pressures, so we need to convert to dB.
                          it would be possible to get it to directly return dB, but then what to do with the 0 values? }
                        if TLs[i, j] = 0 then
                            TLs[i, j] := NaN
                        else
                            TLs[i, j] := -20 * log10(TLs[i, j]); //Transmission Losses, hence the negative. also this is consistent with ramGEO
                        {take out low values below a threshold}
                        if TLs[i,j] > 200 then
                            TLs[i,j] := NaN;

                        if excludeTL(TLs[i,j]) then
                            self.setTLbyInd(n, i, j, fi + startfi, NaN)
                        else
                        begin
                            {calculate the seawater attenuation}
                            WaterAttenuation := TMCKLib.Distance2D(0,
                                                           ranges(i),
                                                           aPos.Z,
                                                           delegate.depths(j)) *
                                                delegate.getWater.seawaterAbsorptionCoefficient(TSpectrum.getFbyInd(fi + startfi,
                                                                              bw),
                                                                              (aPos.Z + delegate.depths(j))/2);
                            self.setTLbyInd(n, i, j, fi + startfi, TLs[i, j] + WaterAttenuation);
                        end;
                    end;

            end;

            updateSolverUIMessages(startfi,fi,movingPosIndex,n,1,
                useLowFreqSolverText,
                bw,
                lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,
                stepProgressBars,
                estimator,
                UpdateRemainingSolveTime);
            application.ProcessMessages;

            {check if user pressed the cancel button on the misc page}
            if delegate.getPCancelSolve.b then
                Exit;
        end; // nslices
    finally
        mmSolverMessages.Visible := false;  //hide the Tmemo

        {clean up the temp directory that held the mode files, and the hard links too. these can get really large, over 1GB at higher freqs}
        THelper.DeleteMatchingFilesInDir(TempPath,'MODFil');
        THelper.DeleteMatchingFilesInDir(tempFreqDir,'tmpModes');
    end;
end;


procedure TSourceSolve.solvedBSeaRayThreadedLoop(const n, nFreqsPerBand: integer;
                                                 const bw: TBandwidth;
                                                 const multipleSolveFreqs: TDoubleDynArray;
                                                 const solveThisFreq: TBooleanDynArray;
                                                 const c, rho, zmax, rayTracingStartAngle, rayTracingEndAngle, rayTracingInitialStepsize: double;
                                                 const startfi, dmax, DBSeaRayMaxBottomReflections, BellhopNRays: integer;
                                                 const aPos: TFPoint3;
                                                 const levelType: TLevelType;
                                                 const pCancelSolve: TWrappedBool;
                                                 const useLowFreqSolverText, coherent, calculateVolumeAttenuationAtEachStep: boolean;
                                                 const raySolverIterationMethod: TRaySolverIterationMethod;
                                                 const seafloors: TSeafloorRangeArray;
                                                 const aWater: TWaterProps;
                                                 const aDepths: TDoubleDynArray;
                                                 const azProfile2: TRangeValArray;
                                                 const aCProfile3: TDepthValArrayAtRangeArray;
                                                 const fractionDone, outputFractionDone: TWrappedDouble;
                                                 const sliceRays: TSliceRays;
                                                 out solverErrors: TSolverErrors;
                                                 var deaths: TRayDeaths);
var
    lenfi, fi           : integer;
    azmax               : double;
    thisRMax            : double;
    solveriMax          : TWrappedInt;
    filterbank          : TFilterbank;  //could move out of loop
    fOversampleInd      : integer;
    oversampleFreqs     : TDoubleDynArray;
    tempOversampleFreqs : TDoubleDynArray;
    oversampleSolveThisFreq : TBooleanDynArray;
    tlMultiFreq         : TDoubleField;
    pFieldMultiFreq     : TComplexField;
    arrivals            : TArrivalsField;
    thisDeaths          : TRayDeaths;
    highestFreq         : double;
    downsampled         : TTimeSeries; //could move out of loop
    fs                  : double;
begin
    //no problem to update TL in background thread as movingsourcetl + gridded happens later, on main thread

    highestFreq := nan;
    arrivals := nil;
    filterbank := nil;
    solveriMax := nil;
    downsampled := nil;
    fs := nan;
    try
        solveriMax := TWrappedInt.create;
        self.getZMaxAndRmaxFromZProfile(azprofile2,false,azmax,thisRMax);
        lenfi := length(multipleSolveFreqs);

        setlength(oversampleFreqs, lenfi * nFreqsPerBand);
        setlength(oversampleSolveThisFreq, lenfi * nFreqsPerBand);
        for fi := 0 to lenfi - 1 do
        begin
            tempOversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(multipleSolveFreqs[fi], bw, nFreqsPerBand);
            for fOversampleInd := 0 to nFreqsPerBand - 1 do
            begin
                oversampleFreqs[fi * nFreqsPerBand + fOversampleInd] := tempOversampleFreqs[fOversampleInd];
                oversampleSolveThisFreq[fi * nFreqsPerBand + fOversampleInd] := solveThisFreq[fi];
            end;

            if solveThisFreq[fi] then
                highestFreq := tempOversampleFreqs[nFreqsPerBand - 1];
        end;

        if self.isTimeSeries then
        begin
            downsampled := self.timeSeries.downsampleTo(highestFreq * 5);
            fs := downsampled.fs;
            filterbank := Tfilterbank.create(downsampled.fs);
        end;


        //do the multi-freq solve, only need to do it once and then all freqs are solved
        TdBSeaRay2Solver.mainMultipleFrequency(oversampleFreqs,
                                               oversampleSolveThisFreq,
                                               aPos.x, aPos.y, aPos.Z, angles(n), thisRMax, self.dr, zmax,
                                               rayTracingStartAngle, rayTracingEndAngle, rayTracingInitialStepsize, DMax,
                                               azprofile2,
                                               aCProfile3,
                                               c, rho, aWater.temperature,
                                               DBSeaRayMaxBottomReflections, BellhopNRays,
                                               not coherent,
                                               seafloors,
                                               directivities,
                                               fs,
                                               self.isTimeSeries,
                                               calculateVolumeAttenuationAtEachStep,
                                               raySolverIterationMethod,
                                               pCancelSolve,
                                               solverIMax,
                                               fractionDone,
                                               sliceRays,
                                               tlMultiFreq,pFieldMultiFreq,arrivals,solverErrors,
                                               thisDeaths);

        TRayDeathsHelper.add(deaths,thisDeaths);

        if self.isTimeSeries then
            self.raySolverTimeOutput2D(n, dMax, lenfi, startfi, nFreqsPerBand,
                aPos, bw, levelType, coherent, calculateVolumeAttenuationAtEachStep,
                aWater, aDepths, pCancelSolve, solverIMax, solveThisFreq, oversampleFreqs,
                arrivals, downsampled, filterbank, outputFractionDone)
        else
            self.raySolverFreqOutput2D(n, dMax, lenfi, startfi, nFreqsPerBand,
                aPos, bw, levelType, coherent, calculateVolumeAttenuationAtEachStep,
                aWater, aDepths, pCancelSolve, solverIMax, solveThisFreq, oversampleFreqs,
                tlMultiFreq, pFieldMultiFreq, outputFractionDone);


    finally
        arrivals.Free;
        filterbank.Free;
        solveriMax.Free;
        downsampled.free;
    end;
end;

procedure TSourceSolve.raySolverTimeOutput2D(const n, dMax, lenfi, startfi, nFreqsPerBand: integer;
    const aPos: TFPoint3;
    const bw: TBandwidth;
    const levelType: TLevelType;
    const coherent, calculateVolumeAttenuationAtEachStep: boolean;
    const aWater: TWaterProps;
    const aDepths: TDoubleDynArray;
    const pCancelSolve: TWrappedBool;
    const solverIMax: TWrappedInt;
    const solveThisFreq: TBooleanDynArray;
    const oversampleFreqs: TDoubleDynArray;
    const arrivals: TArrivalsField;
    const downsampled: TTimeSeries;
    const filterbank: TFilterbank;
    const outputFractionDone: TWrappedDouble);
var
    fi, fi2, j, i, fOversampleInd: integer;
    oversampleFreq, WaterAttenuation, lev: double;
    sparseBandTimeSeries: array of array of TSparseSampleList;
    bandTimeSeries, outTimeSeries, summedOutTimeSeries, localDownsampled: TTimeSeries;
    bandDownsampleOctavesOverOriginalDownsampleOctaves: integer;
begin
    //setup bands per freq, then run through by i and j first: less mem usage
    setlength(sparseBandTimeSeries, lenfi, nFreqsPerBand);
    for fi := 0 to lenfi - 1 do
        for fOversampleInd := 0 to nFreqsPerBand - 1 do
            sparseBandTimeSeries[fi,fOversampleInd] := nil;

    summedOutTimeSeries := nil;
    outTimeSeries := nil;
    localDownsampled := nil;
    try
        summedOutTimeSeries := TTimeSeries.create(downsampled.fs, downsampled.downsampledOctaves);
        outTimeSeries := TTimeSeries.create(downsampled.fs, downsampled.downsampledOctaves);

        localDownsampled := TTimeSeries.create(downsampled.fs, downsampled.downsampledOctaves);
        localDownsampled.cloneFrom(downsampled);

        for fi := lenfi - 1 downto 0 do //backwards to allow downsampling
        begin
            bandDownsampleOctavesOverOriginalDownsampleOctaves := lenfi - 1 - fi;

            if bandDownsampleOctavesOverOriginalDownsampleOctaves > 0 then
                localDownsampled.downsample1Octave; //could more than 1 octave be needed?

            bandTimeSeries := TTimeSeries.create(localDownsampled.fs, localDownsampled.downsampledOctaves);
            try
                for fOversampleInd := nFreqsPerBand - 1 downto 0 do
                begin
                    if not solveThisFreq[fi] then continue;
                    if pCancelSolve.b then Exit;

                    //NB ONLY WORKS WITH 1 FREQ PER BAND
                    oversampleFreq := oversampleFreqs[fi * nFreqsPerBand + fOversampleInd];
                    THelper.RemoveWarning(oversampleFreq);

                    bandTimeSeries.samples := filterbank.band(bw,
                        oversampleFreqs[(lenfi - 1) * nFreqsPerBand + fOversampleInd], //effectively use the same filters each time, due to downsampling
                        localDownsampled.samples);
                    sparseBandTimeSeries[fi,fOversampleInd] := bandTimeSeries.sparseTimeSeries(arrivals.lowAmpCutoff);
                    //multiply by the mitigation level (will be ignored if NaN) and the time series.count (which is the number of time series instances at each calc position - uncorrelated)
                    sparseBandTimeSeries[fi,fOversampleInd].multiplyBy(
                        dCountAndMitigationCalc(
                            ifthen((levelType in [kSPLpk,kSPLpkpk]),1,localDownsampled.count),
                            unDB20(self.srcDBData.mitigationEquipReturn.spectrum.getMitigationAmpbyInd(fi + startfi,bw))));
                end;
            finally
                FreeAndNil(bandTimeSeries);
            end;
        end;

        {for each point and freq, put the transmission loss + water attenuation.}
        for j := 0 to dMax - 1 do
        begin
            for i := 0 to imax - 1 do
            begin
                if pCancelSolve.b then Exit;
                if i >= solveriMax.i then
                begin
                    for fi := 0 to lenfi - 1 do
                        for fOversampleInd := 0 to nFreqsPerBand - 1 do
                            if coherent then
                                self.setFieldbyInd(n, i, j, fi + startfi,nanComplex)// NullComplex)
                            else
                                self.setTLbyInd(n, i, j, fi + startfi,nan);//-NullLev);
                    continue;
                end;

                for fi := 0 to lenfi - 1 do
                begin
                    if pCancelSolve.b then Exit;
                    if not solveThisFreq[fi] then
                    begin
                        self.setTLbyInd(n, i, j, fi + startfi, NaN);
                        continue;
                    end;
                    bandDownsampleOctavesOverOriginalDownsampleOctaves := lenfi - 1 - fi;

                    for fOversampleInd := 0 to nFreqsPerBand - 1 do
                    begin
                        if pCancelSolve.b then Exit;
                        oversampleFreq := oversampleFreqs[fi * nFreqsPerBand + fOversampleInd];

                        if calculateVolumeAttenuationAtEachStep then
                            WaterAttenuation := 0  //attenuation already calculated
                        else
                            WaterAttenuation := TMCKLib.Distance2D(0, ranges(i), aPos.Z, aDepths[j]) *
                                                aWater.seawaterAbsorptionCoefficient(oversampleFreq,(aPos.Z + aDepths[j])/2);

                        {we have to keep hold of a time series right up to the point we calculate splpk or splpkpk.
                        the list of Tarrivals for each point and freq takes source freq dependent directivity into account.
                        It is also very sparse, so we can 'directConvole' instead of going the f*g = F.G route (FFT based).
                        NB we use the band limited time series from the source}
                        //outTimeSeries.samples := raySolver.arrivals.directConvolve(fi,j,i,bandTimeSeries.samples);
                        outTimeSeries.samples := arrivals.sparseDirectConvolve(fi,i,0,j,sparseBandTimeSeries[fi,fOversampleInd], round(power(2,bandDownsampleOctavesOverOriginalDownsampleOctaves)));
                        outTimeSeries.fs := sparseBandTimeSeries[fi,fOversampleInd].fs;
                        outTimeSeries.downsampledOctaves := sparseBandTimeSeries[fi,fOversampleInd].downsampledOctaves;
                        if (levelType in [kSPLpk,kSPLpkpk]) then
                        begin
                            if (fi = 0) and (fOversampleInd = 0) then
                                summedOutTimeSeries.clearSamples;

                            // kSPLpk and kSPLpkpk means all the freq band output series should be summed, and then get an overall level
                            summedOutTimeSeries.addSamples(outtimeseries); //add this band sample to total summed series samples

                            if (fi = (lenfi - 1)) and (fOversampleInd = (nFreqsPerBand - 1)) then
                            begin
                                //last freq band, so all the time series are summed. split the energy between all current bands (- dB10(lenfi))
                                //this is done to carry this level through as if it was a TL.
                                lev := summedOutTimeSeries.level(levelType) - dB10(lenfi * nFreqsPerBand);
                                for fi2 := 0 to lenfi - 1 do
                                    self.setTLbyInd(n, i, j, fi2 + startfi,-lev + WaterAttenuation);
                            end;
                        end
                        else
                        begin
                            //calc the SEL for THIS FREQ BAND.
                            //we are going to carry this level through as if it was a TL. Not sure if that is the best idea, but it takes advantage of existing TL infrastructure
                            if fOversampleInd = 0 then
                            begin
                                summedOutTimeSeries.clearSamples;
                                //don't need to upsample again for SEL, can keep at downsampled freq
                                summedOutTimeSeries.fs := outTimeSeries.fs;
                                summedOutTimeSeries.downsampledOctaves := outTimeSeries.downsampledOctaves;
                            end;

                            summedOutTimeSeries.addSamples(outtimeseries); //add this band sample to total summed series samples

                            if (fOversampleInd = (nFreqsPerBand - 1)) then
                            begin
                                //last freq band, so all the time series are summed. split the energy between all current bands (- dB10(lenfi))
                                //this is done to carry this level through as if it was a TL.
                                lev := summedOutTimeSeries.level(levelType) - dB10(nFreqsPerBand);
                                self.setTLbyInd(n, i, j, fi + startfi,-lev + WaterAttenuation);
                            end;
                        end; //levtype
                    end; //fOversampleInd
                end; //fi
            end; //i

            outputFractionDone.d := j/dMax;
        end;//j

    finally
        summedOutTimeSeries.free;
        outTimeSeries.free;
        localDownsampled.free;

        for fi := 0 to lenfi - 1 do
            for fOversampleInd := 0 to nFreqsPerBand - 1 do
                sparseBandTimeSeries[fi,fOversampleInd].free;
    end;
end;

procedure TSourceSolve.raySolverFreqOutput2D(const n, dMax, lenfi, startfi, nFreqsPerBand: integer;
            const aPos: TFPoint3;
            const bw: TBandwidth;
            const levelType: TLevelType;
            const coherent, calculateVolumeAttenuationAtEachStep: boolean;
            const aWater: TWaterProps;
            const aDepths: TDoubleDynArray;
            const pCancelSolve: TWrappedBool;
            const solverIMax: TWrappedInt;
            const solveThisFreq: TBooleanDynArray;
            const oversampleFreqs: TDoubleDynArray;
            const tlMultiFreq: TDoubleField;
            const pFieldMultiFreq: TComplexField;
            const outputFractionDone: TWrappedDouble);
var
    fi,i,j,fOversampleInd: integer;
    oversampleFreq,outputTempTL,WaterAttenuation: double;

    outputOversampleComplex   : TComplexVect;
    outputOversampleTL        : TDoubleDynArray;
begin
    SetLength(outputOversampleComplex, nFreqsPerBand);
    SetLength(outputOversampleTL, nFreqsPerBand);

    for fi := 0 to lenfi - 1 do
    begin
        if pCancelSolve.b then Exit;
        if not solveThisFreq[fi] then
        begin
            for j := 0 to delegate.getDMax - 1 do
                for i := 0 to self.imax - 1 do
                    self.setTLbyInd(n, i, j, fi + startfi, NaN);
            continue;
        end;

        for fOversampleInd := 0 to nFreqsPerBand - 1 do
        begin
            if pCancelSolve.b then Exit;
            oversampleFreq := oversampleFreqs[fi * nFreqsPerBand + fOversampleInd];

            for j := 0 to dMax - 1 do
            begin
                for i := 0 to imax - 1 do
                begin
                    if pCancelSolve.b then Exit;
                    if (i >= solveriMax.i) then
                    begin
                        if (fOversampleInd = 0) then
                        begin
                            if coherent then
                                self.setFieldbyInd(n, i, j, fi + startfi,nanComplex)// NullComplex)
                            else
                                self.setTLbyInd(n, i, j, fi + startfi,nan);//-NullLev);
                        end;
                        continue;
                    end;

                    if calculateVolumeAttenuationAtEachStep then
                        WaterAttenuation := 0  //attenuation already calculated
                    else
                        WaterAttenuation := TMCKLib.Distance2D(0, ranges(i), aPos.Z, aDepths[j]) *
                                            aWater.seawaterAbsorptionCoefficient(oversampleFreq,(aPos.Z + aDepths[j])/2);

                    //gather data on each oversampled freq passby
                    if coherent then
                        outputOversampleComplex[fOversampleInd] := pFieldMultiFreq[fi * nFreqsPerBand + fOversampleInd,j,i]
                    else
                        outputOversampleTL[fOversampleInd] := tlMultiFreq[fi * nFreqsPerBand + fOversampleInd,j,i];

                    if fOversampleInd = nFreqsPerBand - 1 then
                    begin
                        //write output vals
                        if coherent then
                            self.setFieldbyInd(n, i, j, fi + startfi, sumComplexArray(outputOversampleComplex) * undB20(WaterAttenuation)) //need to undB the attenuation
                        else
                        begin
                            outputTempTL := averageTLs(outputOversampleTL);
                            if isNaN(outputTempTL) or (outputTempTL > self.dSolverReturnMaxTL) or (outputTempTL <= 0) then
                                self.setTLbyInd(n, i, j, fi + startfi, NaN)
                            else
                                self.setTLbyInd(n, i, j, fi + startfi, outputTempTL + WaterAttenuation);
                        end;
                    end;
                end; //i
                outputFractionDone.d := (fi + j/dMax) / lenfi;
            end;//j
        end; //fOversampleInd
    end; //fi
end;

procedure TSourceSolve.solveElasticPE(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeterms: integer;
    const aPos: TFPoint3;
    const lbSolving, lbSolveExtraInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
    //const updateLabelProc: TProcedureWithString;
    const stepProgressBars: TProcedureWithInt;
    const setProgressBars: TProcedureWithDouble;
    const estimator: TSolveTimeEstimator;
    const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    fc, zmax            : double;
    oversampleFreqs     : TDoubleDynArray;
    bw                  : TBandwidth;
    zprofile2           : TRangeValArray;
    tmpCprofile2        : TDepthValArray;
    WaterAttenuation    : double;
    thisRMax            : double;
    solveriMax          : integer;
    solvedFreqs,solvedSlices  : TBooleanDynArray;
    oversampleTLs       : TDoubleField;
    tl                  : TMatrix;
    pCancelSolve        : TWrappedBool;
    starters            : TArray<TStarter>;
    fractionDone        : ISmart<TDoubleList>;
    taskControl         : IOmniTaskControl;//
    thisPos             : TFPoint3;
    solverErrors        : TSolverErrors;
begin
    solverErrors := [];
    thisPos.setPos(aPos);
    pCancelSolve := delegate.getPCancelSolve;
    setlength(oversampleTLs,nFreqsPerBand);
    setlength(starters,nFreqsPerBand);
    fractionDone := TSmart<TDoubleList>.create(TDoubleList.create);
    fractionDone.init(nFreqsPerBand);

    setlength(zprofile2,nslices);
    bw := delegate.getBandwidth;

    zprofile2 := bathyDelegate.getBathymetryVector2(aPos.X, aPos.Y, angles(0), rmax, self.iExtendPastEdge);
    getZMaxAndRmaxFromZProfile(zprofile2,false,zmax,thisRMax);

    SetLength(solvedFreqs,lenfi);
    setlength(solvedSlices,nslices);
    //task := TTask.create(procedure
    taskControl := CreateTask(procedure (const task: IOmniTask)
    //(procedure
    var
        fi,fOversampleInd,j,i,n: integer;
    begin
        for fi := 0 to lenfi - 1 do
        begin
            if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
                setNaNForFiPlusStartFi(fi + startfi)
            else
            begin
                fc := TSpectrum.getFbyInd(fi + startfi, bw);
                oversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(fc, bw, nFreqsPerBand);
                tmpCprofile2 := TSSP.removeDuplicateDepths2(delegate.getCProfileAtAngle2(angles(0))); //todo do this for all angles

                //get the starter from dBSeaModes (have to do it here as we now have z and c profiles)
                for fOversampleInd := 0 to nFreqsPerBand - 1 do
                begin
                    //make the modal starter, that will be used by all slices at this freq
                    starters[fOversampleInd] := TdBSeaModes1Solver.modalStarter(oversampleFreqs[fOversampleInd], thisPos.Z, bathyDelegate.getzmax,
                                     delegate.getDMax,
                                     zprofile2[0].val,
                                     tmpCprofile2,
                                     delegate.getSeabed,
                                     delegate.getdBSeaModesMaxModes,
                                     delegate.getdBSeaPEZOversampling,
                                     solverErrors);
                end;

                solvedSlices.setAll(false);
                for n := 0 to nslices - 1 do
                begin
                    for fOversampleInd := 0 to nFreqsPerBand - 1 do
                        if not pCancelSolve.b then
                            oversampleTLs[fOversampleInd] := TdBSeaPEElastic1Solver.elastic_v3(oversampleFreqs[fOversampleInd],
                                                  thisPos.Z, thisRMax, self.dr, bathyDelegate.getzMax, {TODO can we truncate where local zmax is less?}
                                                  delegate.getdMax,
                                                  max(1, nPadeterms),
                                                  zprofile2,
                                                  tmpCprofile2,
                                                  starters[fOversampleInd],
                                                  delegate.getSeabed,
                                                  pCancelSolve,
                                                  fractiondone[fOversampleInd],
                                                  solverErrors);

                    {solver error checking sometimes returns an empty string. Check for this before continuing.}
                    if pCancelSolve.b then Exit;
                    tl := averageTLs(oversampleTLs);

                    if length(tl) = 0 then
                        solveriMax := 0
                    else
                        solveriMax := length(tl[0]);

                        {for each point and freq, put the transmission loss + water attenuation.}
                        for j := 0 to delegate.getDMax - 1 do
                            for i := 0 to imax - 1 do
                                if (i >= solveriMax) or excludeTL(tl[j,i]) then
                                    self.setTLbyInd(n, i, j, fi + startfi, NaN)
                                else
                                begin
                                    WaterAttenuation := TMCKLib.Distance2D(0,
                                                                   ranges(i),
                                                                   thisPos.Z,
                                                                   delegate.depths(j)) *
                                                        delegate.getWater.seawaterAbsorptionCoefficient(fc,
                                                                   (thisPos.Z + delegate.depths(j))/2);
                                    self.setTLbyInd(n, i, j, fi + startfi, tl[j,i] + WaterAttenuation);
                                end; //NaN, solverimax etc

                    if pCancelSolve.b then Exit;
                end;// n
            end; // level[fi] = nan
            solvedFreqs[fi] := true;
        end; // fi
    end).SetPriority(TOTLThreadPriority.tpHighest).run();
    //end)();

    while not taskControl.WaitFor(500) do
    begin
        Application.ProcessMessages;
        self.updateSolverUIMessages2(movingPosIndex,solvedSlices.countTrue,estimator.cSolverStartTime,
            (movingPosIndex + fractionDone.totalFractionDone) / getMovingPosMax,
            lbSliceNumber,lbSolveSourcePosition,lbSolveExtraInfo,setProgressBars);

        if pCancelSolve.b then
        begin
            taskControl.Terminate(1000);
            break;
        end;
    end;
end;

procedure TSourceSolve.solvePESplitStep(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeterms: integer; const aPos: TFPoint3);
var
    fc, zmax            : double;
    oversampleFreqs     : TDoubleDynArray;
    bw                  : TBandwidth;
    zprofile2           : TRangeValArray;
    tmpCprofile2        : TDepthValArray;
    WaterAttenuation    : double;
    thisRMax            : double;
    solveriMax,nn       : integer;
    solvedFreqs,solvedSlices  : TBooleanDynArray;
    oversampleTLs       : TDoubleField;
    errors              : TSolverErrors;
    tl                  : TMatrix;
    pCancelSolve        : TWrappedBool;
    starters            : TArray<TStarter>;
    fractionDone        : TArray<TWrappedDouble>;
    thisPos             : TFPoint3;
    solverErrors        : TSolverErrors;
begin
    solverErrors := [];
    thisPos.setPos(aPos);
    pCancelSolve := delegate.getPCancelSolve;
    setlength(oversampleTLs,nFreqsPerBand);
    setlength(starters,nFreqsPerBand);
    setlength(fractionDone,nFreqsPerBand);

    for nn := 0 to nFreqsPerBand - 1 do
        fractionDone[nn] := TWrappedDouble.Create;

    setlength(zprofile2,nslices);
    bw := delegate.getBandwidth;


    SetLength(solvedFreqs,lenfi);
    setlength(solvedSlices,nslices);
    //task := TTask.create(procedure
    (procedure
    var
        fi,fOversampleInd,j,i,n: integer;
    begin
        for fi := 0 to lenfi - 1 do
        begin
            if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
                setNaNForFiPlusStartFi(fi + startfi)
            else
            begin
                fc := TSpectrum.getFbyInd(fi + startfi, bw);
                oversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(fc, bw, nFreqsPerBand);
                tmpCprofile2 := TSSP.removeDuplicateDepths2(delegate.getCProfileAtAngle2(angles(0))); //todo do this for all angles

                //get the starter from dBSeaModes (have to do it here as we now have z and c profiles)
                for fOversampleInd := 0 to nFreqsPerBand - 1 do
                begin
                    //make the modal starter, that will be used by all slices at this freq
                    starters[fOversampleInd] := TdBSeaModes1Solver.modalStarter(oversampleFreqs[fOversampleInd], thisPos.Z, bathyDelegate.getzmax,
                                     delegate.getDMax,
                                     zprofile2[0].val,
                                     tmpCprofile2,
                                     delegate.getSeabed,
                                     delegate.getdBSeaModesMaxModes,
                                     delegate.getdBSeaPEZOversampling,
                                     solverErrors);
                end;

                solvedSlices.setAll(false);
                for n := 0 to nslices - 1 do
                begin
                    zprofile2 := bathyDelegate.getBathymetryVector2(thisPos.X, thisPos.Y, angles(n), rmax, self.iExtendPastEdge);
                    getZMaxAndRmaxFromZProfile(zprofile2,false,zmax,thisRMax);

                    for fOversampleInd := 0 to nFreqsPerBand - 1 do
                        if not pCancelSolve.b then
                            oversampleTLs[fOversampleInd] := TdBSeaPESS1Solver.main(oversampleFreqs[fOversampleInd],
                                                  thisPos.Z, thisRMax, self.dr, bathyDelegate.getzMax, {TODO can we truncate where local zmax is less?}
                                                  delegate.getdMax,
                                                  zprofile2,
                                                  tmpCprofile2,
                                                  starters[fOversampleInd],
                                                  delegate.getSeabed,
                                                  pCancelSolve,
                                                  fractiondone[fOversampleInd],
                                                  delegate.getdBSeaPEZOversampling,
                                                  delegate.getdBSeaPErOversampling,
                                                  errors);

                    {solver error checking sometimes returns an empty string. Check for this before continuing.}
                    if pCancelSolve.b then Exit;
                    tl := averageTLs(oversampleTLs);

                    if length(tl) = 0 then
                        solveriMax := 0
                    else
                        solveriMax := length(tl[0]);

                        {for each point and freq, put the transmission loss + water attenuation.}
                        for j := 0 to delegate.getDMax - 1 do
                            for i := 0 to imax - 1 do
                                if (i >= solveriMax) or excludeTL(tl[j,i]) then
                                    self.setTLbyInd(n, i, j, fi + startfi, NaN)
                                else
                                begin
                                    WaterAttenuation := TMCKLib.Distance2D(0,
                                                                   ranges(i),
                                                                   thisPos.Z,
                                                                   delegate.depths(j)) *
                                                        delegate.getWater.seawaterAbsorptionCoefficient(fc,
                                                                   (thisPos.Z + delegate.depths(j))/2);
                                    self.setTLbyInd(n, i, j, fi + startfi, tl[j,i] + WaterAttenuation);
                                end; //NaN, solverimax etc

                    if pCancelSolve.b then Exit;
                end;// n
            end; // level[fi] = nan
            solvedFreqs[fi] := true;
        end; // fi
    end)();
end;


procedure TSourceSolve.solvedBSeaRay(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer;
    const aPos: TFPoint3;
    const useLowFreqSolverText: Boolean;
    const coherent, calculateVolumeAttenuationAtEachStep: Boolean;
    const raySolverIterationMethod: TRaySolverIterationMethod;
    const levelType: TLevelType;
    const allErrorMessages: TStringList;
    const lblSolving, lblSolveExtraInfo, lblSliceNumber, lblSolveSourcePosition: TLabel;
    const stepProgressBars: TProcedureWithInt;
    const setProgressBars: TProcedureWithDouble;
    const estimator: TSolveTimeEstimator;
    const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    zmax                : double;
    oversampleFreqs     : TDoubleDynArray;
    tempOversampleFreqs : TDoubleDynArray;
    oversampleSolveThisFreq : TBooleanDynArray;
    ffOversampleInd     : integer;
    multiFreqLevels     : array of array of TMatrix;
    ffi                 : integer;
    multipleSolveFrequencies : TDoubleDynArray;
    bw                  : TBandwidth;
    zprofile2           : TRangeValArray; // bathymetry profile along solution vector
    tmpCprofile3        : TDepthValArrayAtRangeArray;
    material            : TMaterial;
    c,rho               : double;
    filterbank          : TFilterbank;
    thisRMax            : double;
    solveriMax          : TWrappedInt;
    solverErrors        : TSolverErrors;
    solveThisFreq       : TBooleanDynArray;
    solvedSlices        : TBooleanDynArray;
    pCancelSolve        : TWrappedBool;
    tlMultiFreq         : TDoubleField;
    pFieldMultiFreq     : TComplexField;
    arrivals            : TArrivalsField;
    deaths, thisDeaths  : TRayDeaths;
    fractionDone,outputFractionDone       : TWrappedDouble;
    taskControl         : IOmniTaskControl;
    collectedErrors     : ISmart<TErrorDict>;
    thisPos             : TFPoint3;
    downsampled         : TTimeSeries;
    highestFreq, fs     : double;
    lpr                 : ISmart<TLPRangeList>;
    seabedArray         : TSeafloorRangeArray;
begin
    collectedErrors := TSmart<TErrorDict>.create(TErrorDict.create);
    pCancelSolve := delegate.getPCancelSolve;
    bw := delegate.getBandwidth;
    //don't solve if time series and we are all above nyquist
    if (lenfi = 0) or
       (self.isTimeSeries and (TSpectrum.getFbyInd(startfi, bw) > self.timeSeries.fs / (2 * sqrt(2)))) then
    begin
        self.updateSolverUIMessages(startfi,lenfi,movingPosIndex,nSlices,lenfi,useLowFreqSolverText,bw,lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
        allErrorMessages.add('Source ' + self.name + ': no active frequency bands can be solved for this sample rate');
        exit;
    end;

    thisPos.setPos(aPos);
    TRayDeathsHelper.init(deaths);
    arrivals := nil;
    filterbank := nil;
    fractionDone := nil;
    solveriMax := nil;
    downsampled := nil;
    fs := nan;
    try
        solveriMax := TWrappedInt.create;
        fractionDone := TWrappedDouble.create;
        outputFractionDone := TWrappedDouble.create;
        highestFreq := nan;

        //setup for multi freq solve
        setlength(multipleSolveFrequencies,lenfi);
        setlength(solveThisFreq,lenfi);
        setlength(oversampleFreqs,lenfi * nFreqsPerBand);
        setlength(oversampleSolveThisFreq,lenfi * nFreqsPerBand);
        for ffi := 0 to lenfi - 1 do
        begin
            multipleSolveFrequencies[ffi] := TSpectrum.getFbyInd(ffi + startfi, bw);
            solveThisFreq[ffi] := self.isTimeSeries or not isnan(self.srcSpectrum.getAmpbyInd(ffi + startfi,bw));
            //don't solve bands above the nyquist
            if self.isTimeSeries and (multipleSolveFrequencies[ffi] > self.timeSeries.fs / (2 * sqrt(2))) then
                solveThisFreq[ffi] := false;

            tempOversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(multipleSolveFrequencies[ffi], bw, nFreqsPerBand);
            for ffOversampleInd := 0 to nFreqsPerBand - 1 do
            begin
                oversampleFreqs[ffi * nFreqsPerBand + ffOversampleInd] := tempOversampleFreqs[ffOversampleInd];
                oversampleSolveThisFreq[ffi * nFreqsPerBand + ffOversampleInd] := solveThisFreq[ffi];
            end;

            if solveThisFreq[ffi] then
                highestFreq := tempOversampleFreqs[nFreqsPerBand - 1];
        end;

        if self.isTimeSeries then
        begin
            downsampled := self.timeSeries.downsampleTo(highestFreq * 5);
            fs := downsampled.fs;
            filterbank := Tfilterbank.create(downsampled.fs);
        end;

        material := delegate.topLayerMaterial(aPos.X,aPos.Y);
        c := material.c;
        rho := material.rho_g;

        if self.isTimeSeries then
            setlength(multiFreqLevels, lenfi * nFreqsPerBand, nslices, imax, delegate.getDMax);

        lblSolving.Caption := self.Name + '   dBSeaRay ' + TSpectrum.getFStringbyInd(startfi, bw) +
                                        ' to ' + TSpectrum.getFStringbyInd(startfi + lenfi - 1, bw) + ' Hz...';
        application.ProcessMessages;

        SetLength(solvedSlices,nslices);
        //task := TTask.create(procedure
        taskControl := CreateTask(procedure(const task: IOmniTask)
        var
            n: integer;
        begin
            if pCancelSolve.b then Exit;

            for n := 0 to self.nslices - 1 do
            begin
                zprofile2 := bathyDelegate.getBathymetryVector2(thisPos.X, thisPos.Y, angles(n), rmax, self.iExtendPastEdge);
                self.getZMaxAndRmaxFromZProfile(zprofile2,false,zmax,thisRMax);

                {add the water current velocity to the cprofile for this angle
                the projection of the water current along the current vector is
                V*cos(angle between current and current vect)}
                //tmpCprofile2 := TSSP.removeDuplicateDepths2(delegate.getCProfileAtAngle2(angles(n)));

                lpr := TSmart<TLPRangeList>.create(delegate.getLocationProps(thisPos, angles(n), thisRMax, self.dr));
                tmpCprofile3 := lpr.getCProfiles(angles(n));
                seabedArray := lpr.getSeabeds;

                //sliceRays := sliceRayss.addSlice(n);

                //do the multi-freq solve, only need to do it once and then all freqs are solved

                {$IFDEF DEBUG}
                if bUseGPU then
                    TdBSeaRay3Solver.mainMultipleFrequency(
                                            oversampleFreqs,
                                            oversampleSolveThisFreq,
                                            thisPos.Z, thisRMax, self.dr, zmax,
                                            delegate.getRayTracingStartAngle, delegate.getRayTracingEndAngle, delegate.getRayTracingInitialStepsize, delegate.getDMax,
                                            zprofile2,
                                            tmpCprofile3,
                                            c, rho, delegate.getWater.Temperature,
                                            delegate.getDBSeaRayMaxBottomReflections, delegate.getBellhopNRays, //TESTING
                                            not coherent,
                                            seabedArray,
                                            directivities,
                                            fs,
                                            self.isTimeSeries,
                                            calculateVolumeAttenuationAtEachStep,
                                            raySolverIterationMethod,
                                            pCancelSolve,
                                            solverIMax,
                                            fractionDone,
                                            bUseGPU,
                                            tlMultiFreq,pFieldMultiFreq,arrivals,solverErrors);
                {$ENDIF}
                if not bUseGPU then
                begin
                    TdBSeaRay2Solver.mainMultipleFrequency(
                                            oversampleFreqs,
                                            oversampleSolveThisFreq,
                                            thisPos.x, thisPos.y, thisPos.Z, angles(n), thisRMax, self.dr, zmax,
                                            delegate.getRayTracingStartAngle, delegate.getRayTracingEndAngle, delegate.getRayTracingInitialStepsize, delegate.getDMax,
                                            zprofile2,
                                            tmpCprofile3,
                                            c, rho, delegate.getWater.Temperature,
                                            delegate.getDBSeaRayMaxBottomReflections, delegate.getBellhopNRays,
                                            not coherent,
                                            seabedArray,
                                            directivities,
                                            fs,
                                            self.isTimeSeries,
                                            calculateVolumeAttenuationAtEachStep,
                                            raySolverIterationMethod,
                                            pCancelSolve,
                                            solverIMax,
                                            fractionDone,
                                            nil,//sliceRays,
                                            tlMultiFreq,pFieldMultiFreq,arrivals,solverErrors,
                                            thisDeaths);
                end;

                collectedErrors.addAll(solverErrors,-1,n);

                TRayDeathsHelper.add(deaths, thisDeaths);
                eventLog.log('Ray solver finished. Ray deaths: ' + TRayDeathsHelper.summary(deaths));

                if pCancelSolve.b then Exit;

                //WaterAttenuation := 0;
                if self.isTimeSeries then
                    self.raySolverTimeOutput2D(n, delegate.getDMax, lenfi, startfi, nFreqsPerBand,
                        thisPos, bw, levelType, coherent, calculateVolumeAttenuationAtEachStep,
                        delegate.getWater, delegate.depthsArray(), pCancelSolve, solverIMax, solveThisFreq, oversampleFreqs,
                        arrivals, downsampled, filterbank, outputFractionDone)
                else
                    self.raySolverFreqOutput2D(n, delegate.getDMax, lenfi, startfi, nFreqsPerBand,
                        thisPos, bw, levelType, coherent, calculateVolumeAttenuationAtEachStep,
                        delegate.getWater, delegate.depthsArray(), pCancelSolve, solverIMax, solveThisFreq, oversampleFreqs,
                        tlMultiFreq, pFieldMultiFreq, outputFractionDone);

                solvedSlices[n] := true;
            end; // nslices
        end).SetPriority(TOTLThreadPriority.tpHighest).run();

        while not taskControl.WaitFor(500) do
        begin
            application.ProcessMessages;
            self.updateSolverUIMessages2(movingPosIndex,solvedSlices.countTrue,estimator.cSolverStartTime,
                (movingPosIndex + (solvedSlices.countTrue + (fractionDone.d / 2) + (outputFractionDone.d / 2))/nSlices) / getMovingPosMax,
                lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,setProgressBars);

            if pCancelSolve.b then
            begin
                taskControl.Terminate(1000);
                break;
            end;
        end;

        collectedErrors.appendToStringList(allErrorMessages);

    finally
        arrivals.Free;
        filterbank.Free;
        fractionDone.Free;
        outputFractionDone.free;
        solverIMax.Free;
        downsampled.free;
    end;
end;

procedure TSourceSolve.solvedBSeaRay3d(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer;
    const aPos: TFPoint3;
    const useLowFreqSolverText, coherent, calculateVolumeAttenuationAtEachStep: boolean;
    const raySolverIterationMethod: TRaySolverIterationMethod;
    const levelType: TLevelType;
    const allErrorMessages: TStringlist;
    const lblSolving, lblSolveExtraInfo, lblSliceNumber, lblSolveSourcePosition: TLabel;
    const stepProgressBars: TProcedureWithInt;
    const setProgressBars: TProcedureWithDouble;
    const estimator: TSolveTimeEstimator;
    const UpdateRemainingSolveTime: TProcedureWithBoolBool);
var
    azmax,zmax      : double;
    oversampleFreqs,tempOversampleFreqs : TDoubleDynArray;
    oversampleSolveThisFreq : TBooleanDynArray;
    ffOversampleInd,ffi{,n} : integer;
    multiFreqLevels     : array of array of TMatrix;
    multipleSolveFrequencies : TDoubleDynArray;
    bw                  : TBandwidth;
    zprofile2           : TRangeValArray; // bathymetry profile along solution vector
    tmpCprofile2        : TDepthValArray;
    WaterAttenuation    : double;
    material            : TMaterial;
    c,rho               : double;
    thisRMax            : double;
    solverErrors        : TSolverErrors;
    solveThisFreq       : TBooleanDynArray;
    //outputOversampleComplex   : TComplexVect;
    //outputOversampleTL        : TDoubleDynArray;
    pCancelSolve        : TWrappedBool;
    tlMultiFreq         : TArray<TDoubleField>;
    pFieldMultiFreq     : TComplexFieldArray;
    arrivals            : TArrivalsField;
    deaths, thisDeaths  : TRayDeaths;
    fractionDone, outputFractionDone        : TWrappedDouble;
    taskControl         : IOmniTaskControl;
    gridImax, gridJmax  : integer;
    xMax, yMax, fs, highestFreq      : double;
    collectedErrors     : ISmart<TErrorDict>;
    solverIMax          : TWrappedInt;
    downsampled         : TTimeSeries;
    filterbank          : TFilterbank;
    thisPos             : TFPoint3;
begin
    collectedErrors := TSmart<TErrorDict>.create(TErrorDict.create);
    pCancelSolve := delegate.getPCancelSolve;
    bw := delegate.getBandwidth;
    thisPos.setPos(aPos);
    gridImax := delegate.getGridImax;
    gridjmax := delegate.getGridJmax;
    xmax := delegate.getxmax;
    ymax := delegate.getymax;
    //don't solve if time series and we are all above nyquist
    if (lenfi = 0) or
       (self.isTimeSeries and (TSpectrum.getFbyInd(startfi, bw) > self.timeSeries.fs / (2 * sqrt(2)))) then
    begin
        self.updateSolverUIMessages(startfi,lenfi,movingPosIndex,nSlices,lenfi,useLowFreqSolverText,bw,lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,stepProgressBars,estimator,UpdateRemainingSolveTime);
        allErrorMessages.add('Source ' + self.name + ': no active frequency bands can be solved for this sample rate');
        exit;
    end;

    TRayDeathsHelper.init(deaths);
    arrivals := nil;
    fractionDone := nil;
    outputFractionDone := nil;
    solverIMax := nil;
    downsampled := nil;
    filterbank := nil;
    try
        solverIMax := TWrappedInt.create( high(integer));
        fractionDone := TWrappedDouble.create;
        outputFractionDone := TWrappedDouble.create;
        highestFreq := nan;

        //setup for multi freq solve
        setlength(multipleSolveFrequencies,lenfi);
        setlength(solveThisFreq,lenfi);
        setlength(oversampleFreqs,lenfi * nFreqsPerBand);
        setlength(oversampleSolveThisFreq,lenfi * nFreqsPerBand);
//        setlength(outputOversampleComplex,nFreqsPerBand);
//        setlength(outputOversampleTL,nFreqsPerBand);
        for ffi := 0 to lenfi - 1 do
        begin
            multipleSolveFrequencies[ffi] := TSpectrum.getFbyInd(ffi + startfi, bw);
            solveThisFreq[ffi] := self.isTimeSeries or not isnan(self.srcSpectrum.getAmpbyInd(ffi + startfi,bw));
            //don't solve bands above the nyquist
            if self.isTimeSeries and (multipleSolveFrequencies[ffi] > self.timeSeries.fs / (2 * sqrt(2))) then
                solveThisFreq[ffi] := false;

            tempOversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(multipleSolveFrequencies[ffi], bw, nFreqsPerBand);
            for ffOversampleInd := 0 to nFreqsPerBand - 1 do
            begin
                oversampleFreqs[ffi * nFreqsPerBand + ffOversampleInd] := tempOversampleFreqs[ffOversampleInd];
                oversampleSolveThisFreq[ffi * nFreqsPerBand + ffOversampleInd] := solveThisFreq[ffi];
            end;

            if solveThisFreq[ffi] then
                highestFreq := tempOversampleFreqs[nFreqsPerBand - 1];
        end;

        if self.isTimeSeries then
        begin
            downsampled := self.timeSeries.downsampleTo(highestFreq * 5);
            fs := downsampled.fs;
            filterbank := Tfilterbank.create(downsampled.fs);
        end;

        material := delegate.topLayerMaterial(aPos.X,aPos.Y);
        c := material.c;
        rho := material.rho_g;

        if self.isTimeSeries then
            setlength(multiFreqLevels, lenfi * nFreqsPerBand, nslices, imax, delegate.getDMax);

        lblSolving.Caption := self.Name + '   dBSeaRay ' + TSpectrum.getFStringbyInd(startfi, bw) +
                                        ' to ' + TSpectrum.getFStringbyInd(startfi + lenfi - 1, bw) + ' Hz...';
        application.ProcessMessages;

        taskControl := CreateTask(procedure(const task: IOmniTask)
        var
            n: integer;
        begin
            zmax := 0;
            for n := 0 to self.nslices - 1 do
            begin
                zprofile2 := bathyDelegate.getBathymetryVector2(thisPos.X, thisPos.Y, angles(n), rmax, self.iExtendPastEdge);
                self.getZMaxAndRmaxFromZProfile(zprofile2,false,azmax,thisRMax);
                if not isnan( azMax) and (azmax > zmax) then
                    zmax := azmax;
            end;

            //nb no current, only 1 c profile
            tmpCprofile2 := TSSP.removeDuplicateDepths2(delegate.getCProfileAtAngle2(angles(0)));

            TdBSeaRay3D1Solver.main(oversampleFreqs,
                                    oversampleSolveThisFreq,
                                    thisPos.x, thisPos.y, thisPos.Z, xmax, ymax, zmax, rmax, dr,
                                    delegate.getRayTracingStartAngle, delegate.getRayTracingEndAngle, delegate.getRayTracingInitialStepsize,
                                    gridImax,gridJmax,delegate.getDMax, nslices, delegate.getBellhopNRaysAzimuth, delegate.getBellhopNRays, delegate.getDBSeaRayMaxBottomReflections,
                                    bathyDelegate.getDepthsMatrix,
                                    tmpCprofile2,
                                    c, rho, delegate.getWater.Temperature,
                                    not coherent, false,
                                    delegate.getSeabed,
                                    directivities,
                                    timeseries.fs,
                                    self.isTimeSeries,
                                    calculateVolumeAttenuationAtEachStep,
                                    raySolverIterationMethod,
                                    pCancelSolve,
                                    fractionDone,
                                    tlMultiFreq,pFieldMultiFreq,arrivals,solverErrors,
                                    thisDeaths);

            collectedErrors.addAll(solverErrors, -1, -1);
            TRayDeathsHelper.add(deaths, thisDeaths);
            eventLog.log('Ray solver 3D finished. Ray deaths: ' + TRayDeathsHelper.summary3d(deaths));

            if pCancelSolve.b then Exit;

            WaterAttenuation := 0;
            THelper.RemoveWarning(WaterAttenuation);
            if self.isTimeSeries then
                RayArrivalsOutputTime3D(multipleSolveFrequencies, oversampleFreqs,
                    coherent, calculateVolumeAttenuationAtEachStep,
                    solveThisFreq,
                    startfi, lenfi, nFreqsPerBand, delegate.getDMax,
                    solverImax,
                    delegate.getWater, delegate.depthsArray(),
                    pCancelSolve,
                    arrivals,
                    downsampled,
                    levelType,
                    thisPos,
                    filterbank,
                    outputFractionDone)
            else
                RayArrivalsOutputFreq3D(multipleSolveFrequencies, oversampleFreqs,
                    pCancelSolve,
                    coherent, calculateVolumeAttenuationAtEachStep,
                    solveThisFreq,
                    startfi, lenfi, nFreqsPerBand, high(integer),
                    levelType,
                    thisPos,
                    tlMultiFreq,
                    pFieldMultiFreq,
                    outputFractionDone);

            if pCancelSolve.b then Exit;
        end).SetPriority(TOTLThreadPriority.tpHighest).run();

        while not taskControl.WaitFor(500) do
        begin
            application.ProcessMessages;
            self.updateSolverUIMessages2(movingPosIndex,0,estimator.cSolverStartTime,
                ((movingPosIndex + (fractionDone.d*ifthen(isTimeSeries,0.5,0.9)) + (outputFractionDone.d*ifThen(isTimeSeries,0.5,0.1)))) / getMovingPosMax,
                lblSliceNumber,lblSolveSourcePosition,lblSolveExtraInfo,setProgressBars);

            if pCancelSolve.b then
            begin
                //task.Cancel;
                taskControl.Terminate(1000);
                break;
            end;
        end;

        collectedErrors.appendToStringList(allErrorMessages);

    finally
        arrivals.Free;
        filterbank.Free;
        fractionDone.Free;
        outputFractionDone.free;
        solverIMax.Free;
        downsampled.free;
    end;
end;

procedure TSourceSolve.RayArrivalsOutputTime3D(const multipleSolveFrequencies, oversampleFreqs: TDoubleDynArray;
    const coherent, calculateVolumeAttenuationAtEachStep: boolean;
    const solveThisFreq: TBooleanDynArray;
    const startfi, lenfi, nFreqsPerBand, dmax: integer;
    const solverIMax: TWrappedInt;
    const aWater: TWaterProps;
    const aDepths: TDoubleDynArray;
    const pCancelSolve: TWrappedBool;
    const arrivals: TArrivalsField;
    const downsampled: TTimeSeries;
    const levelType: TLevelType;
    const aPos: TFPoint3;
    const filterbank: TFilterbank;
    const timeFractionDone: TWrappedDouble);
var
    fi,fi2,n,j,i,fOversampleInd: integer;
    oversampleFreq, WaterAttenuation, lev: double;
    summedOutTimeSeries, bandTimeSeries, outTimeSeries, localDownsampled: TTimeSeries;
    sparseBandTimeSeries: array of array of TSparseSampleList;
    bw: TBandwidth;
    bandDownsampleOctavesOverOriginalDownsampleOctaves: integer;
begin
    bw := delegate.getBandwidth;

    //setup bands per freq, then run through by i and j first: less mem usage
    setlength(sparseBandTimeSeries, lenfi, nFreqsPerBand);
    for fi := 0 to lenfi - 1 do
        for fOversampleInd := 0 to nFreqsPerBand - 1 do
            sparseBandTimeSeries[fi,fOversampleInd] := nil;

    summedOutTimeSeries := nil;
    outTimeSeries := nil;
    localDownsampled := nil;
    try
        summedOutTimeSeries := TTimeSeries.create(downsampled.fs, downsampled.downsampledOctaves);
        outTimeSeries  := TTimeSeries.Create(downsampled.fs, downsampled.downsampledOctaves);
        //sparseBandTimeSeries := TSparseSampleList.create(true);

        localDownsampled := TTimeSeries.create(downsampled.fs, downsampled.downsampledOctaves);
        localDownsampled.cloneFrom(downsampled);

        for fi := lenfi - 1 downto 0 do //backwards to allow downsampling
        begin
            bandDownsampleOctavesOverOriginalDownsampleOctaves := lenfi - 1 - fi;

            if bandDownsampleOctavesOverOriginalDownsampleOctaves > 0 then
                localDownsampled.downsample1Octave;

            bandTimeSeries := TTimeSeries.create(localDownsampled.fs, localDownsampled.downsampledOctaves);
            try
                for fOversampleInd := nFreqsPerBand - 1 downto 0 do
                begin
                    if not solveThisFreq[fi] then continue;
                    if pCancelSolve.b then Exit;

                    //NB ONLY WORKS WITH 1 FREQ PER BAND
                    oversampleFreq := oversampleFreqs[fi * nFreqsPerBand + fOversampleInd];
                    THelper.RemoveWarning(oversampleFreq);

                    bandTimeSeries.samples := filterbank.band(bw,
                        oversampleFreqs[(lenfi - 1) * nFreqsPerBand + fOversampleInd], //effectively use the same filters each time, due to downsampling
                        localDownsampled.samples);
                    sparseBandTimeSeries[fi,fOversampleInd] := bandTimeSeries.sparseTimeSeries(arrivals.lowAmpCutoff);
                    //multiply by the mitigation level (will be ignored if NaN) and the time series.count (which is the number of time series instances at each calc position - uncorrelated)
                    sparseBandTimeSeries[fi,fOversampleInd].multiplyBy(
                        dCountAndMitigationCalc(
                            ifthen((levelType in [kSPLpk,kSPLpkpk]),1,localDownsampled.count),
                            unDB20(self.srcDBData.mitigationEquipReturn.spectrum.getMitigationAmpbyInd(fi + startfi,bw))));

                end;
            finally
                freeAndNil(bandTimeSeries);
            end;
        end;

        {for each point and freq, put the transmission loss + water attenuation.}
        for n := 0 to self.nSlices - 1 do
        begin
            for j := 0 to dMax - 1 do
            begin
                for i := 0 to imax - 1 do
                begin
                    if pCancelSolve.b then Exit;
                    if i >= solveriMax.i then
                    begin
                        for fi := 0 to lenfi - 1 do
                            for fOversampleInd := 0 to nFreqsPerBand - 1 do
                                if coherent then
                                    self.setFieldbyInd(n, i, j, fi + startfi,nanComplex)// NullComplex)
                                else
                                    self.setTLbyInd(n, i, j, fi + startfi,nan);//-NullLev);
                        continue;
                    end;

                    for fi := 0 to lenfi - 1 do
                    begin
                        if pCancelSolve.b then Exit;
                        if not solveThisFreq[fi] then
                        begin
                            self.setTLbyInd(n, i, j, fi + startfi, NaN);
                            continue;
                        end;
                        bandDownsampleOctavesOverOriginalDownsampleOctaves := lenfi - 1 - fi;

                        for fOversampleInd := 0 to nFreqsPerBand - 1 do
                        begin
                            if pCancelSolve.b then Exit;
                                oversampleFreq := oversampleFreqs[fi * nFreqsPerBand + fOversampleInd];

                            if calculateVolumeAttenuationAtEachStep then
                                WaterAttenuation := 0  //attenuation already calculated
                            else
                                WaterAttenuation := TMCKLib.Distance2D(0, ranges(i), aPos.Z, aDepths[j]) *
                                                    aWater.seawaterAbsorptionCoefficient(oversampleFreq,(aPos.Z + aDepths[j])/2);

                            {we have to keep hold of a time series right up to the point we calculate splpk or splpkpk.
                            the list of Tarrivals for each point and freq takes source freq dependent directivity into account.
                            It is also very sparse, so we can 'directConvole' instead of going the f*g = F.G route (FFT based).
                            NB we use the band limited time series from the source}
                            //outTimeSeries.samples := raySolver.arrivals.directConvolve(fi,j,i,bandTimeSeries.samples);
                            outTimeSeries.samples := arrivals.sparseDirectConvolve(fi,i,0,j,sparseBandTimeSeries[fi,fOversampleInd], round(power(2,bandDownsampleOctavesOverOriginalDownsampleOctaves)));
                            outTimeSeries.fs := sparseBandTimeSeries[fi,fOversampleInd].fs;
                            outTimeSeries.downsampledOctaves := sparseBandTimeSeries[fi,fOversampleInd].downsampledOctaves;

                            if (levelType in [kSPLpk,kSPLpkpk]) then
                            begin
                                if (fi = 0) and (fOversampleInd = 0) then
                                    summedOutTimeSeries.clearSamples;

                                // kSPLpk and kSPLpkpk means all the freq band output series should be summed, and then get an overall level
                                summedOutTimeSeries.addSamples(outtimeseries); //add this band sample to total summed series samples

                                if (fi = (lenfi - 1)) and (fOversampleInd = (nFreqsPerBand - 1)) then
                                begin
                                    //last freq band, so all the time series are summed. split the energy between all current bands (- dB10(lenfi))
                                    //this is done to carry this level through as if it was a TL.
                                    lev := summedOutTimeSeries.level(levelType) - dB10(lenfi * nFreqsPerBand);
                                    for fi2 := 0 to lenfi - 1 do
                                        self.setTLbyInd(n, i, j, fi2 + startfi,-lev + WaterAttenuation);
                                end;
                            end
                            else
                            begin
                                //calc the SEL for THIS FREQ BAND.
                                //we are going to carry this level through as if it was a TL. Not sure if that is the best idea, but it takes advantage of existing TL infrastructure
                                if fOversampleInd = 0 then
                                begin
                                    summedOutTimeSeries.clearSamples;
                                    //don't need to upsample again for SEL, can keep at downsampled freq
                                    summedOutTimeSeries.fs := outTimeSeries.fs;
                                    summedOutTimeSeries.downsampledOctaves := outTimeSeries.downsampledOctaves;
                                end;

                                summedOutTimeSeries.addSamples(outtimeseries); //add this band sample to total summed series samples

                                if (fOversampleInd = (nFreqsPerBand - 1)) then
                                begin
                                    //last freq band, so all the time series are summed. split the energy between all current bands (- dB10(lenfi))
                                    //this is done to carry this level through as if it was a TL.
                                    lev := summedOutTimeSeries.level(levelType) - dB10(nFreqsPerBand);
                                    self.setTLbyInd(n, i, j, fi + startfi,-lev + WaterAttenuation);
                                end;
                            end; //levtype
                        end; //fOversampleInd
                    end; //fi
                end; //i
                timeFractionDone.d := (n + j/dMax) / nSlices;
            end; //j
        end; //n

    finally
        summedOutTimeSeries.free;
        outTimeSeries.free;
        localDownsampled.free;

        for fi := 0 to lenfi - 1 do
            for fOversampleInd := 0 to nFreqsPerBand - 1 do
                sparseBandTimeSeries[fi,fOversampleInd].free;
    end;
end;

procedure TSourceSolve.RayArrivalsOutputFreq3D(const multipleSolveFrequencies, oversampleFreqs: TDoubleDynArray;
            const pCancelSolve: TWrappedBool;
            const coherent, calculateVolumeAttenuationAtEachStep: boolean;
            const solveThisFreq: TBooleanDynArray;
            const startfi, lenfi, nFreqsPerBand, solveriMax: integer;
            const levelType: TLevelType;
            const aPos: TFPoint3;
            const tlMultiFreq: TArray<TDoubleField>;
            const pFieldMultiFreq: TComplexFieldArray;
            const outputFractionDone: TWrappedDouble);
var
    fi,i,j,n,fOversampleInd: integer;
    oversampleFreq,WaterAttenuation,outputTempTL: double;
    outputOversampleComplex: TComplexVect;
    outputOversampleTL: TDoubleDynArray;
    //d: double;
    //a,b,c,e: integer;
begin
    setlength(outputOversampleComplex,nFreqsPerBand);
    setlength(outputOversampleTL,nFreqsPerBand);
    WaterAttenuation := 0;

    for n := 0 to nSlices - 1 do
        for fi := 0 to length(multipleSolveFrequencies) - 1 do
        begin
            if pCancelSolve.b then Exit;
            if not solveThisFreq[fi] then
            begin
                for j := 0 to delegate.getDMax - 1 do
                    for i := 0 to self.imax - 1 do
                        self.setTLbyInd(n, i, j, fi + startfi, NaN);
                outputFractionDone.d := (n + (fi / length(multipleSolveFrequencies))) / nSlices;
            end
            else
            begin
                {for each point and freq, put the transmission loss + water attenuation.}
                for j := 0 to delegate.getDMax - 1 do
                begin
                    for i := 0 to imax - 1 do
                    begin
                        if i >= solveriMax then
                        begin
                            if coherent then
                                self.setFieldbyInd(n, i, j, fi + startfi,NullComplex) //Maybe some other level here, if undb(NullLev)...
                            else
                                self.setTLbyInd(n, i, j, fi + startfi,nan);//-NullLev);
                        end
                        else
                        begin
                            for fOversampleInd := 0 to nFreqsPerBand - 1 do
                            begin
                                oversampleFreq := oversampleFreqs[fi * nFreqsPerBand + fOversampleInd];

                                if calculateVolumeAttenuationAtEachStep then
                                    WaterAttenuation := 0  //attenuation already calculated
                                else
                                    WaterAttenuation := TMCKLib.Distance2D(0, ranges(i), aPos.Z, delegate.depths(j)) *
                                                        delegate.getWater.seawaterAbsorptionCoefficient(oversampleFreq,(aPos.Z + delegate.depths(j))/2);

                                //gather data on each oversampled freq passby
                                if coherent then
                                    outputOversampleComplex[fOversampleInd] := pFieldMultiFreq[n,fi * nFreqsPerBand + fOversampleInd,j,i]
                                else
                                begin
                                    //a := length(tlMultiFreq);
                                    //b := length(tlMultiFreq[0]);
                                    //c := length(tlMultiFreq[0,0]);
                                    //e := length(tlMultiFreq[0,0,0]);

                                    //if a + b + c + e > 9999 then
                                    //    d := 1
                                    //else
                                    //d := tlMultiFreq[n,fi * nFreqsPerBand + fOversampleInd,j,i];
                                    //outputOversampleTL[fOversampleInd] := d;
                                    outputOversampleTL[fOversampleInd] := tlMultiFreq[n,fi * nFreqsPerBand + fOversampleInd,j,i];
                                end;
                            end; //oversample freqs

                            if coherent then
                                self.setFieldbyInd(n, i, j, fi + startfi, sumComplexArray(outputOversampleComplex) * undB20(WaterAttenuation)) //need to undB the attenuation
                            else
                            begin
                                outputTempTL := averageTLs(outputOversampleTL);
                                if isNaN(outputTempTL) or (outputTempTL > self.dSolverReturnMaxTL) or (outputTempTL <= 0) then
                                self.setTLbyInd(n, i, j, fi + startfi, NaN)
                            else
                                self.setTLbyInd(n, i, j, fi + startfi, outputTempTL + WaterAttenuation);
                            end;
                        end; //solveriMax
                    end; //i
                    outputFractionDone.d := (n + ((fi + (j / delegate.getDMax)) / length(multipleSolveFrequencies))) / nSlices;
                end;//j
            end; // level[fi] = nan
        end;
end;

procedure TSourceSolve.solvePE3dFFT(const startfi, lenfi, nFreqsPerBand, movingPosIndex: integer; const aPos: TFPoint3);
var
    fc, zmax, azmax     : double;
    oversampleFreqs     : TDoubleDynArray;
    bw                  : TBandwidth;
    zprofile2           : TArray<TRangeValArray>;
    tmpCprofile2        : TDepthValArray;
    WaterAttenuation    : double;
    totalRmax,aRMax     : double;
    solveriMax,nn       : integer;
    solvedFreqs         : integer;
    oversampleTLs       : TArray<TDoubleField>;
    tl                  : TDoubleField;
    starters            : TArray<TStarter>;
    pCancelSolve        : TWrappedBool;
    thisPos             : TFPoint3;
    solverErrors        : TSolverErrors;
begin
    solverErrors := [];
    thisPos.setPos(aPos);
    pCancelSolve := delegate.getPCancelSolve;
    setlength(oversampleTLs,nFreqsPerBand);
    setlength(zprofile2,nslices);
    bw := delegate.getBandwidth;

    zmax := 0;
    totalRmax := 0;
    for nn := 0 to nslices - 1 do
    begin
        zprofile2[nn] := bathyDelegate.getBathymetryVector2(aPos.X, aPos.Y, angles(nn), rmax, self.iExtendPastEdge);
        getZMaxAndRmaxFromZProfile(zprofile2[nn],false,azmax,aRMax);
        if not isnan( azMax) and (azmax > zmax) then
            zmax := azmax;
        if not isnan( aRMax) and (armax > totalRmax) then
            totalRmax := aRMax;
    end;

    //get the starter from dBSeaModes
    setlength(starters,nFreqsPerBand);

    solvedFreqs := 0;
    //task := TTask.create(procedure
    (procedure
    var
        fi,fOversampleInd,j,i,n: integer;
    begin
        for fi := 0 to lenfi - 1 do
        begin
            if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
                setNaNForFiPlusStartFi(fi + startfi)
            else
            begin
                fc := TSpectrum.getFbyInd(fi + startfi, bw);
                oversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(fc, bw, nFreqsPerBand);
                tmpCprofile2 := TSSP.removeDuplicateDepths2(delegate.getCProfileAtAngle2(angles(0))); //todo do this for all angles

                //get the starter from dBSeaModes (have to do it here as we now have z and c profiles)
                for fOversampleInd := 0 to nFreqsPerBand - 1 do
                begin
                    //make the modal starter, that will be used by all slices at this freq
                    starters[fOversampleInd] := TdBSeaModes1Solver.modalStarter(oversampleFreqs[fOversampleInd], thisPos.Z, bathyDelegate.getzmax,
                                     delegate.getDMax,
                                     zprofile2[0][0].val,
                                     tmpCprofile2,
                                     delegate.getSeabed,
                                     delegate.getdBSeaModesMaxModes,
                                     delegate.getdBSeaPEZOversampling,
                                     solverErrors);
                end;

                for fOversampleInd := 0 to nFreqsPerBand - 1 do
                    if not pCancelSolve.b then
                        oversampleTLs[fOversampleInd] := TdBSeaPESS3d1Solver.mainFFT(oversampleFreqs[fOversampleInd],
                                              thisPos.Z, totalRmax, dr, bathyDelegate.getzMax,
                                              delegate.getdMax, nSlices,
                                              zprofile2,
                                              tmpCprofile2,
                                              starters[fOversampleInd],
                                              delegate.getSeabed,
                                              pCancelSolve,
                                              solverErrors);

                if pCancelSolve.b then Exit;
                tl := averageTLs(oversampleTLs);

                for n := 0 to nslices - 1 do
                begin
                    if (length(tl) = 0) or (length(tl[n]) = 0) then
                        solveriMax := 0
                    else
                        solveriMax := length(tl[n,0]);

                        {for each point and freq, put the transmission loss + water attenuation.}
                        for j := 0 to delegate.getDMax - 1 do
                            for i := 0 to imax - 1 do
                                if (i >= solveriMax) or excludeTL(tl[n,j,i]) then
                                    self.setTLbyInd(n, i, j, fi + startfi, NaN)
                                else
                                begin
                                    WaterAttenuation := TMCKLib.Distance2D(0,
                                                                   ranges(i),
                                                                   thisPos.Z,
                                                                   delegate.depths(j)) *
                                                        delegate.getWater.seawaterAbsorptionCoefficient(fc,
                                                                   (thisPos.Z + delegate.depths(j))/2);
                                    self.setTLbyInd(n, i, j, fi + startfi, tl[n,j,i] + WaterAttenuation);
                                end; //NaN, solverimax etc

                    if pCancelSolve.b then Exit;
                end; //nslices
            end; // level[fi] = nan
            inc(solvedFreqs);
        end; // fi
    end)();
    //end);
{    task.start();
    while task.Status <> TTaskStatus.Completed do
    begin
        application.ProcessMessages;

        //lbSolving.Caption := self.Name + '   dBSeaPE ' + TSpectrum.getFStringbyInd(solvedFreqs + startfi, bw) + ' Hz...';

//        self.updateSolverUIMessages2(movingPosIndex,solvedSlices,estimator.cSolverStartTime,
//            (solvedFreqs + (solvedSlices + fractionDone.totalFractionDone)/nSlices) / lenfi,
//            lbSliceNumber,lbSolveSourcePosition,lbSolveExtraInfo,setProgressBars);
//        sleep(200);
    end;}
end;

procedure TSourceSolve.solvePE3dPade(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeTerms, nHorizontalPadeTerms: integer;
    const aPos: TFPoint3;
    const lbSolving,lbSolveExtraInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
    const setProgressBars: TProcedureWithDouble;
    const estimator: TSolveTimeEstimator;
    const UpdateRemainingSolveTime: TProcedureWithBoolBool;
    const allErrorMessages: TStringList);
var
    taskControl         : iomnitaskcontrol;
    fractionDone        : TWrappedDouble;
    collectedErrors     : ISmart<TErrorDict>;
    thisPos             : TFPoint3;
    pCancelSolve        : TWrappedBool;
    lbSliceNumberWasVisible: boolean;
begin
    collectedErrors := TSmart<TErrorDict>.create(TErrorDict.create);
    fractionDone := TWrappedDouble.create;
    thisPos.setPos(aPos);
    pCancelSolve := delegate.getPCancelSolve;
    lbSliceNumberWasVisible := lbSliceNumber.Visible;

    taskControl := CreateTask(procedure(const task: IOmniTask)
    var
        solverErrors        : TSolverErrors;
        fi,fOversampleInd,j,i,n: integer;
        oversampleTLs       : TArray<TDoubleField>;
        tl                  : TDoubleField;
        oversampleFreqs     : TDoubleDynArray;
        fc, zmax, azmax     : double;
        bw                  : TBandwidth;
        zprofile2           : TArray<TRangeValArray>;
        tmpCprofile2        : TArray<TDepthValArray>;
        WaterAttenuation    : double;
        totalRmax,aRMax     : double;
        solveriMax,nn       : integer;
        starters            : TArray<TStarter>;
    begin
        solverErrors := [];
        lbSliceNumber.Visible := false;
        setlength(oversampleTLs,nFreqsPerBand);
        setlength(zprofile2,nslices);
        setlength(tmpCprofile2,nslices);
        bw := delegate.getBandwidth;

        zmax := 0;
        totalRmax := 0;
        for nn := 0 to nslices - 1 do
        begin
            tmpCprofile2[nn] := TSSP.removeDuplicateDepths2(delegate.getCProfileAtAngle2(angles(nn)));
            zprofile2[nn] := bathyDelegate.getBathymetryVector2(thisPos.X, thisPos.Y, angles(nn), rmax, self.iExtendPastEdge);
            getZMaxAndRmaxFromZProfile(zprofile2[nn],true,azmax,aRMax);
            if not isnan( azMax) and (azmax > zmax) then
                zmax := azmax;
            if not isnan( aRMax) and (armax > totalRmax) then
                totalRmax := aRMax;
        end;

        //get the starter from dBSeaModes
        setlength(starters,nFreqsPerBand);

        for fi := 0 to lenfi - 1 do
        begin
            if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
                setNaNForFiPlusStartFi(fi + startfi)
            else
            begin
                fc := TSpectrum.getFbyInd(fi + startfi, bw);
                oversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(fc, bw, nFreqsPerBand);

                //get the starter from dBSeaModes (have to do it here as we now have z and c profiles)
                for fOversampleInd := 0 to nFreqsPerBand - 1 do
                begin
                    //make the modal starter, that will be used by all slices at this freq
                    starters[fOversampleInd] := TdBSeaModes1Solver.modalStarter(oversampleFreqs[fOversampleInd], thisPos.Z, bathyDelegate.getzmax,
                                     delegate.getDMax,
                                     zprofile2[0][0].val,
                                     tmpCprofile2[0],
                                     delegate.getSeabed,
                                     delegate.getdBSeaModesMaxModes,
                                     delegate.getdBSeaPEZOversampling,
                                     solverErrors);
                end;

                for fOversampleInd := 0 to nFreqsPerBand - 1 do
                    if not pCancelSolve.b then
                        oversampleTLs[fOversampleInd] := TdBSeaPESS3D1Solver.mainPade(oversampleFreqs[fOversampleInd],
                                              thisPos.Z, totalRmax, dr, bathyDelegate.getzMax,
                                              delegate.getdMax, nSlices, nPadeTerms, nHorizontalPadeTerms,
                                              self.rangesArray(),
                                              zprofile2,
                                              tmpCprofile2,
                                              delegate.getdBSeaPEZOversampling,
                                              delegate.getdBSeaPErOversampling,
                                              delegate.getdBSeaPEPhiOversampling,
                                              delegate.getSeabed,
                                              starters[fOversampleInd],
                                              pCancelSolve,
                                              fractionDone,
                                              solverErrors);

                if pCancelSolve.b then Exit;
                collectedErrors.addAll(solverErrors,fi,-1);
                tl := averageTLs(oversampleTLs);

                for n := 0 to nslices - 1 do
                begin
                    if (length(tl) = 0) or (length(tl[n]) = 0) then
                        solveriMax := 0
                    else
                        solveriMax := length(tl[n,0]);

                        {for each point and freq, put the transmission loss + water attenuation.}
                        for j := 0 to delegate.getDMax - 1 do
                            for i := 0 to imax - 1 do
                                if (i >= solveriMax) or excludeTL(tl[n,j,i]) then
                                    self.setTLbyInd(n, i, j, fi + startfi, NaN)
                                else
                                begin
                                    WaterAttenuation := TMCKLib.Distance2D(0,
                                                                   ranges(i),
                                                                   thisPos.Z,
                                                                   delegate.depths(j)) *
                                                        delegate.getWater.seawaterAbsorptionCoefficient(fc,
                                                                   (thisPos.Z + delegate.depths(j))/2);
                                    self.setTLbyInd(n, i, j, fi + startfi, tl[n,j,i] + WaterAttenuation);
                                end; //NaN, solverimax etc

                    if pCancelSolve.b then Exit;
                end; //nslices
            end; // level[fi] = nan
        end; // fi
    end).SetPriority(TOTLThreadPriority.tpHighest).run;

    while not taskControl.WaitFor(500) do
    begin
        Application.ProcessMessages;
        self.updateSolverUIMessages2(movingPosIndex,0,estimator.cSolverStartTime,
            (movingPosIndex + fractionDone.d) / getMovingPosMax,
            lbSliceNumber,lbSolveSourcePosition,lbSolveExtraInfo,setProgressBars);

        if pCancelSolve.b then
        begin
            taskControl.Terminate(1000);
            break;
        end;
    end;

    collectedErrors.appendToStringList(allErrorMessages);

    fractionDone.free;
    lbSliceNumber.Visible := lbSliceNumberWasVisible;
end;

procedure TSourceSolve.solveInternal(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeTerms: integer;
                                const aPos: TFPoint3;
                                const useLowFreqSolverText: boolean;
                                const aSolver :TCalculationMethod;
                                const allErrorMessages: TStringlist;
                                const lbSolving, lbSolveExtraInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
                                const setProgressBars: TProcedureWithDouble;
                                const estimator: TSolveTimeEstimator);
var
    fc, zmax            : double;
    oversampleFreqs     : TDoubleDynArray;
    bw                  : TBandwidth;
    zprofile2           : TRangeValArray;
    tmpCprofile3        : TDepthValArrayAtRangeArray;
    lpr                 : ISmart<TLPRangeList>;
    seabedArray         : TSeafloorRangeArray;
    WaterAttenuation    : double;
    thisRMax            : double;
    solveriMax          : integer;
    solverErrors        : TSolverErrors;
    solvedSlices        : TBooleanDynArray;
    solvedFreqs         : TBooleanDynArray;
    pCancelSolve        : TWrappedBool;
    oversampleTLs       : TDoubleField;
    tl                  : TMatrix;
    starters            : TArray<TStarter>;
    fractionDone        : ISmart<TDoubleList>;
    taskControl         : IOmnitaskcontrol;
    collectedErrors     : ISmart<TErrorDict>;
    thisPos             : TFPoint3;
begin
    collectedErrors := TSmart<TErrorDict>.create(TErrorDict.create);
    solverErrors := [];
    pCancelSolve := delegate.getPCancelSolve;
    setlength(oversampleTLs,nFreqsPerBand);
    //setlength(fractionDone,nFreqsPerBand);
    fractionDone := TSmart<TDoubleList>.create(TDoubleList.create(true));
    fractionDone.init(nFreqsPerBand);
    bw := delegate.getBandwidth;

    if (aSolver = cmdBSeaPE) then
        setlength(starters,nFreqsPerBand);

    SetLength(solvedFreqs,lenfi);
    SetLength(solvedSlices,nslices);
    thisPos.setPos(aPos);

    taskControl := CreateTask(procedure(const task: IOmniTask)
    var
        fi,n,fOversampleInd,j,i: integer;
    begin
        for fi := 0 to lenfi - 1 do
        begin
            if isnan(self.srcSpectrum.getAmpbyInd(fi + startfi,bw)) then
                setNaNForFiPlusStartFi(fi + startfi)
            else
            begin
                fc := TSpectrum.getFbyInd(fi + startfi, bw);
                oversampleFreqs := TSpectrum.evenlySpacedFrequenciesInBand(fc, bw, nFreqsPerBand);

                solvedSlices.setAll(false);
                for n := 0 to self.nslices - 1 do
                begin
                    zprofile2 := bathyDelegate.getBathymetryVector2(thisPos.X, thisPos.Y, angles(n), rmax, self.iExtendPastEdge);
                    getZMaxAndRmaxFromZProfile(zprofile2,false,zmax,thisRMax);

                    lpr := TSmart<TLPRangeList>.create(delegate.getLocationProps(thisPos, angles(n), thisRMax, self.dr));
                    tmpCprofile3 := lpr.getCProfiles(angles(n)); //tmpCprofile3 := delegate.getCProfilesAtAngle3(thisPos, angles(n), thisRMax, self.dr); {add the water current velocity to the cprofile for this angle the projection of the water current along the current vector is V*cos(angle between current and current vect)}
                    seabedArray := lpr.getSeabeds;// delegate.getSeabedsAtAngle(thisPos,angles(n),rmax,self.dr);

                    if (aSolver = cmdBSeaPE) and (n = 0) then
                    begin
                        //get the starter from dBSeaModes (have to do it here as we now have z and c profiles)
                        for fOversampleInd := 0 to nFreqsPerBand - 1 do
                        begin
                            //make the modal starter, that will be used by all slices at this freq
                            starters[fOversampleInd] := TdBSeaModes1Solver.modalStarter(oversampleFreqs[fOversampleInd], thisPos.Z, bathyDelegate.getzmax,
                                             delegate.getDMax,
                                             zprofile2[0].val,
                                             tmpCprofile3[0].depthVal,
                                             seabedArray[0].seafloor,
                                             delegate.getdBSeaModesMaxModes,
                                             delegate.getdBSeaPEZOversampling,
                                             solverErrors);
                        end;
                    end;

                    for fOversampleInd := 0 to nFreqsPerBand - 1 do
                    begin
                        if not pCancelSolve.b then
                        begin
                            case aSolver of
                                cmdBSeaPE: begin
                                    if bPESS then
                                    begin
                                        oversampleTLs[fOversampleInd] := TdBSeaPESS1Solver.main(oversampleFreqs[fOversampleInd],
                                                              thisPos.Z, thisRMax, self.dr, bathyDelegate.getzMax, {TODO can we truncate where local zmax is less?}
                                                              delegate.getdMax,
                                                              zprofile2,
                                                              tmpCprofile3[0].depthVal,
                                                              starters[fOversampleInd],
                                                              seabedArray[0].seafloor,
                                                              pCancelSolve,
                                                              fractionDone[fOversampleInd],
                                                              delegate.getdBSeaPEZOversampling,
                                                              delegate.getdBSeaPErOversampling,
                                                              solverErrors);
                                    end
                                    else
                                    begin
                                        oversampleTLs[fOversampleInd] := TdBSeaPE2Solver.main(oversampleFreqs[fOversampleInd],
                                                          thisPos.Z, thisRMax, bathyDelegate.getzMax, {TODO can we truncate where local zmax is less?}
                                                          delegate.getdMax,
                                                          nPadeTerms,
                                                          self.rangesArray(),
                                                          zprofile2,
                                                          tmpCprofile3,
                                                          delegate.getdBSeaPEZOversampling,
                                                          delegate.getdBSeaPErOversampling,
                                                          seabedArray,
                                                          starters[fOversampleInd],
                                                          pCancelSolve,
                                                          fractionDone[fOversampleInd],
                                                          {$IF Defined(DEBUG)}self.directivities,{$ENDIF}
                                                          solverErrors);
                                    end;
                                end;
                                cmdBSeaModes: begin
                                    TdBSeaModes1Solver.main(oversampleFreqs[fOversampleInd], thisPos.Z, thisRMax, self.dr, bathyDelegate.getzmax,
                                                     delegate.getdMax,
                                                     zprofile2,
                                                     tmpCprofile3,
                                                     seabedArray,
                                                     delegate.getdBSeaModesMaxModes,
                                                     delegate.getdBSeaModesZOversampling,
                                                     delegate.getdBSeaModesrOversampling,
                                                     delegate.getStopMarchingSolverAtLand,
                                                     pCancelSolve,
                                                     fractionDone[fOversampleInd],
                                                     oversampleTLs[fOversampleInd],
                                                     solverErrors);
                                end;
                            end; //asolver

                            collectedErrors.addAll(solverErrors, oversampleFreqs[fOversampleInd], n);
                        end;
                    end;

                    {solver error checking sometimes returns an empty string. Check for this before continuing.}
                    if pCancelSolve.b then Exit;
                    tl := averageTLs(oversampleTLs);

                    if length(tl) = 0 then
                        solveriMax := 0
                    else
                        solveriMax := length(tl[0]);

                    {for each point and freq, put the transmission loss + water attenuation.}
                    for j := 0 to delegate.getDMax - 1 do
                        for i := 0 to imax - 1 do
                            if (i >= solveriMax) or excludeTL(tl[j,i]) then
                                self.setTLbyInd(n, i, j, fi + startfi, NaN)
                            else
                            begin
                                WaterAttenuation := TMCKLib.Distance2D(0,
                                                               ranges(i),
                                                               thisPos.Z,
                                                               delegate.depths(j)) *
                                                    delegate.getWater.seawaterAbsorptionCoefficient(fc,
                                                               (thisPos.Z + delegate.depths(j))/2);
                                self.setTLbyInd(n, i, j, fi + startfi, tl[j, i] + WaterAttenuation);
                            end; //NaN, solverimax etc

                    solvedSlices[n] := true;
                    if pCancelSolve.b then Exit;
                end; // nslices
            end; // level[fi] = nan
            solvedFreqs[fi] := true;
        end; // fi
    end).SetPriority(TOTLThreadPriority.tpHighest).run();

    while not taskControl.WaitFor(500) do
    begin
        application.ProcessMessages;

        case aSolver of
            cmdBSeaPE: lbSolving.Caption := self.Name + '   dBSeaPE ' + TSpectrum.getFStringbyInd(solvedFreqs.countTrue + startfi, bw) + ' Hz...';
            cmdBSeaModes: lbSolving.Caption := self.Name + '   dBSeaModes ' + TSpectrum.getFStringbyInd(solvedFreqs.countTrue + startfi, bw) + ' Hz...';
        end;

        self.updateSolverUIMessages2(movingPosIndex,solvedSlices.countTrue,estimator.cSolverStartTime,
            (movingPosIndex + (solvedFreqs.countTrue + (solvedSlices.countTrue + fractionDone.totalFractionDone)/nSlices) / lenfi) / getMovingPosMax,
            lbSliceNumber,lbSolveSourcePosition,lbSolveExtraInfo,setProgressBars);

        if pCancelSolve.b then
        begin
            taskControl.Terminate(1000);
            break;
        end;
    end;

    collectedErrors.appendToStringList(allErrorMessages);
end;

procedure TSourceSolve.solveInternalThreaded(const startfi, lenfi, nFreqsPerBand, movingPosIndex, nPadeTerms: integer;
                                         const aPos: TFPoint3;
                                         const useLowFreqSolverText: boolean;
                                         const aSolver: TCalculationMethod;
                                         const allErrorMessages: TStringlist;
                                         const lblSolving, lbExtraSolveInfo, lbSliceNumber,lbSolveSourcePosition: TLabel;
                                         const setProgressBars: TProcedureWithDouble;
                                         const estimator: TSolveTimeEstimator);
var
    oversampleFs        : TMatrix;
    globalZMax          : double;
    ffi, nn             : integer;
    bw                  : TBandwidth;
    solvedSlices        : TBooleanDynArray;
    pCancelSolve        : TWrappedBool;
    admax               : integer;
    //aSeafloorScheme     : TSeafloorScheme;
    aWater              : TWaterProps;
    aDepths             : TDoubleDynArray;
    PEzOversampling     : double;
    PErOversampling     : double;
    stopAtLand          : boolean;
    zprofiles2          : array of TRangeValArray;
    tmpCProfiles3       : array of TDepthValArrayAtRangeArray;
    seabedArrays        : array of TSeafloorRangeArray;
    dBSeaModesMaxModes  : integer;
    fractionDone        : ISmart<TDoubleListList>;
    perSliceErrors      : TArray<TArray<TSolverErrors>>;
    collectedErrors     : ISmart<TErrorDict>;
    solveThisFreq       : TBooleanDynArray;
    aRangesArray        : TDoubleDynArray;
    thisPos             : TFPoint3;
    lprs                : ISmart<TObjectList<TLPRangeList>>;
begin
    collectedErrors := TSmart<TErrorDict>.create(TErrorDict.create);
    bw := delegate.getBandwidth;
    case aSolver of
        cmdBSeaPE: lblSolving.Caption := self.Name + '   dBSeaPE ' + TSpectrum.getFStringbyInd(startfi, bw) + ' to ' + TSpectrum.getFStringbyInd(startfi + lenfi - 1, bw) + ' Hz';
        cmdBSeaModes: lblSolving.Caption := self.Name + '   dBSeaModes ' + TSpectrum.getFStringbyInd(startfi, bw) + ' to ' + TSpectrum.getFStringbyInd(startfi + lenfi - 1, bw) + ' Hz';
    end;
    application.ProcessMessages;

    //setlength(perSliceErrorMessages,lenfi,nslices);
    setlength(perSliceErrors,lenfi,nslices);
    setlength(solvedSlices,nslices);
    admax := delegate.getdMax;
    pCancelSolve := delegate.getpcancelSolve;
    globalZMax := bathyDelegate.getzmax;
    //aSeafloorScheme := delegate.getseabed;
    awater := delegate.getwater;
    aDepths := delegate.depthsArray();
    dBSeaModesMaxModes := delegate.getdBSeaModesMaxModes;
    PEzOversampling := delegate.getdBSeaPEzOversampling;
    PErOversampling := delegate.getdBSeaPErOversampling;
    stopAtLand := delegate.getStopMarchingSolverAtLand;
    SetLength(zprofiles2, nSlices);
    lprs := TSmart<TObjectList<TLPRangeList>>.create(TObjectList<TLPRangeList>.create(true));
    setlength(tmpCProfiles3, nSlices);
    fractionDone := TSmart<TDoubleListList>.create(TDoubleListList.Create());
    fractionDone.init(lenfi, nSlices);
    setlength(seabedArrays,nSlices);
    thisPos.setPos(aPos);

    for nn := 0 to nSlices - 1 do
    begin
        zprofiles2[nn] := bathyDelegate.getBathymetryVector2(apos.x, apos.y, self.angles(nn), rmax, self.iExtendPastEdge);

        lprs.add(delegate.getLocationProps(aPos, self.angles(nn), rMax, self.dr));
        tmpCprofiles3[nn] := lprs[nn].getCProfiles(angles(nn)); //tmpCprofile3 := delegate.getCProfilesAtAngle3(thisPos, angles(n), thisRMax, self.dr); {add the water current velocity to the cprofile for this angle the projection of the water current along the current vector is V*cos(angle between current and current vect)}
        seabedArrays[nn] := lprs[nn].getSeabeds;// delegate.getSeabedsAtAngle(thisPos,angles(n),rmax,self.dr);
//        tmpCprofiles3[nn] := delegate.getCProfilesAtAngle3(apos,angles(nn),rmax,self.dr);
//        for tdvr in tmpCprofiles3[nn] do
//            TSSP.removeDuplicateDepths2(tdvr.depthVal);
//
//        seabedArrays[nn] := delegate.getSeabedsAtAngle(apos,angles(nn),rmax,self.dr);
    end;
    setlength(oversampleFs,lenfi);

    SetLength(solveThisFreq, lenfi);
    bw := delegate.getBandwidth;
    for ffi := 0 to lenfi - 1 do
    begin
        solveThisFreq[ffi] := not isnan(self.srcSpectrum.getAmpbyInd(ffi + startfi,bw));

        if not solveThisFreq[ffi] then
            setNaNForFiPlusStartFi(ffi + startfi)
        else
            oversampleFs[ffi] := TSpectrum.evenlySpacedFrequenciesInBand(TSpectrum.getFbyInd(ffi + startfi, bw), bw, nFreqsPerBand);
    end;
    aRangesArray := self.rangesArray();

    solvedSlices.setAll(false);
    Parallel.For(0, nSlices - 1)
        .NoWait //run in background
        .Execute(procedure (n: integer)
    var
        fi, fOversampleInd: integer;
        fc: double;
        starters: TArray<TArray< TStarter>>;
    begin
        SetLength(starters,lenfi,nFreqsPerBand);
        for fi := 0 to lenfi - 1 do
        begin
            if not solveThisFreq[fi] then continue;

            fc := TSpectrum.getFbyInd(fi + startfi, bw);
            if (aSolver = cmdBSeaPE) then
            begin
                if pCancelSolve.b then break;
                for fOversampleInd := 0 to nFreqsPerBand - 1 do
                begin
                    //prepare starter, that will be used by all slices at this freq

                    //{$DEFINE TEST_COLLINS_STARTER}
                    //{$DEFINE TESTCEDERBERG_}
                    {$IF defined(DEBUG) and defined (TEST_COLLINS_STARTER)}
                    starters[fi,fOversampleInd] := TSelfStarter.starter(
                        oversampleFs[fi,fOversampleInd],
                        self.bathyDelegate.getzmax,
                        self.delegate.dz,
                        zprofiles2[0][0].val,
                        thisPos.Z,
                        tmpCprofiles3[0][0].depthVal,
                        seabedArrays[0][0].seafloor);

                    {$ELSEIF defined(DEBUG) and defined (TESTCEDERBERG)}
                    starters[fi,fOversampleInd] :=
                        TdBSeaPEElastic1Solver.selfStarterCederbergCollins(
                        oversampleFs[fi,fOversampleInd],
                        thisPos.Z,
                        globalZMax,
                        admax, nPadeTerms,
                        zprofiles2[0],
                        tmpCprofiles3[0][0].depthVal,
                        seabedArrays[0][0].seafloor,
                        perSliceErrors[fi,n]);

                    {$ELSE}
                    starters[fi,fOversampleInd] := TdBSeaModes1Solver.modalStarter(oversampleFs[fi,fOversampleInd],
                                            thisPos.Z,
                                            globalZMax,
                                            admax,
                                            zprofiles2[0][0].val,
                                            tmpCprofiles3[0][0].depthVal,
                                            seabedArrays[0][0].seafloor,
                                            dBSeaModesMaxModes,
                                            delegate.getdBSeaPEZOversampling,
                                            perSliceErrors[fi,n]);
                    {$ENDIF}
                end;
            end;

            if pCancelSolve.b then break;

            solveInternalThreadedLoop(n, fi, nFreqsPerBand, nPadeTerms,
                                       starters[fi],
                                       oversampleFs[fi], aRangesArray,
                                       fc, self.dr, globalZMax,
                                       startfi, aDmax, dBSeaModesMaxModes,
                                       stopAtLand,
                                       thisPos, pCancelSolve, useLowFreqSolverText, aSolver,
                                       seabedArrays[0],
                                       aWater,
                                       aDepths,
                                       zprofiles2[n],
                                       tmpCProfiles3[n],
                                       PEzOversampling, PErOversampling,
                                       fractionDone[fi][n],
                                       perSliceErrors[fi,n]);
        end;
        solvedSlices[n] := true;
    end);

    while not (solvedSlices.allTrue or pCancelSolve.b) do
    begin
        sleep(500);
        application.ProcessMessages;
        self.updateSolverUIMessages2(movingPosIndex,solvedSlices.countTrue,estimator.cSolverStartTime,
            (movingPosIndex + fractionDone.totalFractionDone) / getMovingPosMax,
            lbSliceNumber,lbSolveSourcePosition,lbExtraSolveInfo,setProgressBars);
    end;

    for ffi := 0 to lenfi - 1 do
        if solveThisFreq[ffi] then
            for nn := 0 to nslices - 1 do
                collectedErrors.addAll(perSliceErrors[ffi,nn],oversampleFs[ffi,0],nn);

    collectedErrors.appendToStringList(allErrorMessages);
end;


procedure TSourceSolve.solveInternalThreadedLoop(const n, fi, nFreqsPerBand, nPadeTerms: integer;
                                             const starters: TArray<TStarter>;
                                             const oversampleFreqs, rangesArray: TDoubleDynArray;
                                             const fc, adr, zmax: double;
                                             const startfi, dmax, dBSeaModesMaxModes: integer;
                                             const stopAtLand: boolean;
                                             const aPos: TFPoint3;
                                             const pCancelSolve: TWrappedBool;
                                             const useLowFreqSolverText: boolean;
                                             const aSolver: TCalculationMethod;
                                             const seafloors: TSeafloorRangeArray;
                                             const aWater: TWaterProps;
                                             const aDepths: TDoubleDynArray;
                                             const aZProfile2: TRangeValArray;
                                             const aCProfile3: TDepthValArrayAtRangeArray;
                                             const PEzOversampling, PErOversampling: double;
                                             const fractionDone: TWrappedDouble;
                                             out solverErrors: TSolverErrors);
var
    i,j                 : integer;
    azmax               : double;
    WaterAttenuation    : double;
    thisRMax            : double;
    solveriMax          : integer;
    fOversampleInd      : integer;
    tls                 : TDoubleField;
    tl                  : TMatrix;
begin
    setlength(tls, nFreqsPerBand);
    self.getZMaxAndRmaxFromZProfile(azprofile2,false,azmax,thisRMax);
    for fOversampleInd := 0 to nFreqsPerBand - 1 do
        if not pCancelSolve.b then
        begin
            case aSolver of
                cmdBSeaPE: begin
                    if bPESS then
                    begin
                        tls[fOversampleInd] := TdBSeaPESS1Solver.main(oversampleFreqs[fOversampleInd],
                          aPos.Z, thisRMax, self.dr, bathyDelegate.getzMax, {TODO can we truncate where local zmax is less?}
                          delegate.getdMax,
                          aZProfile2,
                          aCprofile3[0].depthVal,
                          starters[fOversampleInd],
                          seafloors[0].seafloor,
                          pCancelSolve,
                          fractionDone,
                          delegate.getdBSeaPEZOversampling,
                          delegate.getdBSeaPErOversampling,
                          solverErrors);
                    end
                    else
                    begin
                        tls[fOversampleInd] := TDBSeaPE2Solver.main(
                                      oversampleFreqs[fOversampleInd], aPos.z, thisRMax, zMax {TODO can we truncate where local zmax is less?},
                                      dMax,
                                      nPadeTerms,
                                      rangesArray,
                                      aZProfile2,
                                      aCprofile3,
                                      PEzOversampling, PErOversampling,
                                      seafloors,
                                      starters[fOversampleInd],
                                      pCancelSolve,
                                      fractionDone,
                                      {$IFDEF DEBUG}self.directivities,{$ENDIF}
                                      solverErrors);
                    end;
                end;
                cmdBSeaModes: begin
                    TdBSeaModes1Solver.main(oversampleFreqs[fOversampleInd], aPos.z, thisRMax, adr, zMax,
                                     dMax,
                                     aZProfile2,
                                     aCprofile3,
                                     seafloors,
                                     dBSeaModesMaxModes,
                                     delegate.getdBSeaModesZOversampling,
                                     delegate.getdBSeaModesROversampling,
                                     stopAtLand,
                                     pCancelSolve,
                                     fractionDone,
                                     tls[fOversampleInd],
                                     solverErrors);
                end;
            end; //asolver, not cancel, freq oversample
        end;

    if pCancelSolve.b then exit;
    tl := averageTLs(tls);

    if length(tl) = 0 then
        solveriMax := 0
    else
        solveriMax := length(tl[0]);

    //for each point and freq, put the transmission loss + water attenuation.
    //no problem to do this in background thread as movingsourcetl + gridded happens later, on main thread
    for j := 0 to dMax - 1 do
    begin
        if pCancelSolve.b then exit;
        for i := 0 to imax - 1 do
        begin
            if (i >= solveriMax) or excludeTL(TL[j, i]) then
                self.setTLbyInd(n, i, j, fi + startfi, NaN)
            else
            begin
                WaterAttenuation := TMCKLib.Distance2D(0,
                                               ranges(i),
                                               aPos.z,
                                               adepths[j]) *
                                    aWater.seawaterAbsorptionCoefficient(fc,
                                               (aPos.z + adepths[j])/2);
                self.setTLbyInd(n, i, j, fi + startfi, TL[j, i] + WaterAttenuation);
            end; //NaN, solverimax etc
        end;
    end;
end;

function TSourceSolve.averageTLs(const tls: TDoubleField): TMatrix;
var
    imax, jmax: integer;
    i,j,n: integer;
    oversampleTLs: TDoubleDynArray;
begin
    setlength(oversampleTLs, length(tls));

    imax := 0;
    jmax := 0;
    for n := 0 to length(tls) - 1 do
    begin
        if length(tls[n]) > imax then
            imax := length(tls[n]);
        if (length(tls[n]) > 0) and (length(tls[n,0]) > jmax) then
            jmax := length(tls[n,0]);
    end;

    setlength(result,imax,jmax);
    for I := 0 to imax - 1 do
        for j := 0 to jmax - 1 do
        begin
            for n := 0 to length(tls) - 1 do
                if (length(tls[n]) > i) and (length(tls[n,i]) > j) then
                    oversampleTLs[n] := tls[n,i,j]
                else
                    oversampleTLs[n] := nan;
            result[i,j] := averageTLs(oversampleTLs);
        end;
end;

function TSourceSolve.averageTLs(const tls: TArray<TDoubleField>): TDoubleField;
var
    fOversample, n, j, i, nmax, jmax, imax: integer;
    oversampleTLs: TDoubleDynArray;
begin
    //oversample, n (theta), j (z), i (r)
    if length(tls) = 0 then
    begin
        setlength(result,0);
        exit;
    end;

    setlength(oversampleTLs, length(tls));

    nmax := 0;
    imax := 0;
    jmax := 0;
    for fOversample := 0 to length(tls) - 1 do
    begin
        if length(tls[fOversample]) = 0 then continue;
        if length(tls[fOversample]) > nmax then
            nmax := length(tls[fOversample]);
        if length(tls[fOversample,0]) > imax then
            imax := length(tls[fOversample,0]);
        if (length(tls[fOversample,0]) > 0) and (length(tls[fOversample,0,0]) > jmax) then
            jmax := length(tls[fOversample,0,0]);
    end;

    setlength(result,nmax,imax,jmax);
    for n := 0 to nmax - 1 do
        for I := 0 to imax - 1 do
            for j := 0 to jmax - 1 do
            begin
                for fOversample := 0 to length(tls) - 1 do
                    if (n < length(tls[fOversample])) and
                        (i < length(tls[fOversample,n])) and
                        (j < length(tls[fOversample,n,i])) then
                        oversampleTLs[fOversample] := tls[fOversample,n,i,j]
                    else
                        oversampleTLs[fOversample] := nan;
                result[n,i,j] := averageTLs(oversampleTLs);
            end;
end;

function TSourceSolve.averageTLs(const tls: TDoubleDynArray): double;
var
    d: double;
    sum: double;
begin
    sum := nan;
    for d in tls do
        if not isnan(d) then
            if isnan(sum) then
                sum := 1/tls.length*power(10,-0.1*d)
            else
                sum := sum + 1/tls.length*power(10,-0.1*d);

    if isnan(sum) then
        result := nan
    else
        result := -10*log10(sum);
end;

function TSourceSolve.sumComplexArray(const arr: TComplexVect): Tcomplex;
var
    n: integer;
begin
    result := nanComplex;

    for n := 0 to length(arr) - 1 do
    begin
        if not isnan(arr[n].r) then
            if isnan(result.r) then
                result.r := arr[n].r
            else
                result.r := Result.r + arr[n].r;

        if not isnan(arr[n].i) then
            if isnan(result.i) then
                result.i := arr[n].i
            else
                result.i := Result.i + arr[n].i;
    end;
end;

function TSourceSolve.formatSecondsToHHMMSS(const secs: integer): string;
begin
    result := inttostr(secs div 3600) + ':' + THelper.twoDigitInttostr((secs mod 3600) div 60) + ':' + THelper.twoDigitInttostr(secs mod 60);
end;

procedure TSolverStatus.setCode(const c: TStatusCode);
begin
    code := c;
    lastUpdate := timeGetTime;
end;

function TSolverStatus.timeSince: cardinal;
begin
    result := timeGetTime - lastUpdate;
end;


function TSolverStatus.toString: string;
begin
    case code of
        kReady: result := 'q';
        kRunning: result := 'r';
        kDone: result := 'd';
    else
        result := inttostr(ord(code));
    end;
end;

procedure TSolverStatusesHelper.init();
begin
    self.setAll(kReady);
end;

procedure TSolverStatusesHelper.setAll(const code: TStatusCode);
var
    s: TSolverStatus;
begin
    for s in self do
        s.setCode(code);
end;

function TSolverStatusesHelper.allEqual(const code: TStatusCode): boolean;
var
    s: TSolverStatus;
begin
    result := true;
    for s in self do
        if s.code  <> code then
            result := false;
end;

function TSolverStatusesHelper.allSame: boolean;
begin
    result := allEqual(TSolverStatus(self[0]).code);
end;

function TSolverStatusesHelper.allTimedOut: boolean;
var
    s: TSolverStatus;
begin
    result := true;
    for s in self do
        if s.timeSince  < 2000 then
            result := false;
end;


function TSolverStatusesHelper.shouldRelaunch: boolean;
begin
    result := (not allEqual(kRunning)) and allSame and allTimedOut;
end;

function TSolverStatusesHelper.toString: string;
var
    s: TSolverStatus;
begin
    result := '';
    for s in self do
        result := result + s.toString + ' ';
end;

{ TSolverErrorInfo }

//constructor TSolverErrorInfo.create(aerror: TSolverError; an: integer; af: double);
//begin
//    inherited;
//
//    self.error := aerror;
//    self.n := an;
//    self.f := af;
//end;

{ TErrorDict }

procedure TErrorDict.appendToStringList(const sl: TStringList);
var
    s: string;
begin
    for s in self.toStringArray do
        sl.add(s);
end;

function TErrorDict.toStringArray: TArray<string>;

    function sliceSummary(slices: TList<integer>): string;
    var
        n: integer;
    begin
        result := ' slices: ';
        for n in slices do
            if n >= 0 then
                result := result + inttostr(n) + ' '
            else
                result := result + ' 3d slices ';
        setlength(result, length(result) - 1);
    end;

    function sliceFreqSummary(const freqSlices: TFreqSlices): string;
    var
        fsl: TPair<double, ISmart<TList<integer>>>;
    begin
        result := '';
        for fsl in freqSlices do
            result := result + ifthen(fsl.key < 0,' ',' f: ' + floattostrf(fsl.Key, ffFixed, 14, 1) + ' Hz, ') + sliceSummary(TSmart<TList<integer>>(fsl.Value).invoke) + ',';
        setlength(result, length(result) - 1);
    end;

    function errorSummary(const p: TPair<TSolverError, ISmart<TFreqSlices>>): string;
    begin
        result := p.key.toString + sliceFreqSummary(TSmart<TFreqSlices>(p.Value).Invoke);
    end;
var
    e: TPair<TSolverError, ISmart<TFreqSlices>>;
begin
    for e in self do
        THelper.append<string>(errorSummary(e),result);
end;

procedure TErrorDict.addAll(const errs: TSolverErrors; const f: double; const n: integer);
var
    e: TSolverError;
begin
    for e := Low(TSolverError) to High(TSolverError) do
        if e in errs then
            self.addNew(e, f, n);
end;

//procedure TErrorDict.addMatrix(const errs: TArray<TArray<TSolverErrors>>; const f: double; const n: integer);
//var
//    i,j: integer;
//begin
//    for i := 0 to high(errs) do
//        for j := 0 to high(errs[i]) do
//            addAll(errs[i,j]);
//end;

procedure TErrorDict.addNew(const err: TSolverError; const f: double; const n: integer);
var
    freqSlices: ISmart<TFreqSlices>;
    slices: ISmart<TList<integer>>;
begin
    if not self.TryGetValue(err, freqSlices) then
    begin
        freqSlices := TSmart<TFreqSlices>.create(TFreqSlices.create);
        self.Add(err, freqSlices);
    end;
    if not freqSlices.TryGetValue(f, slices) then
    begin
        slices := TSmart<TList<integer>>.create(TList<integer>.create());

        freqSlices.add(f, slices);
    end;
    slices.add(n);
end;

{ TFreqSlices }

end.
