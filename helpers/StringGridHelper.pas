unit StringGridHelper;

interface

uses
    system.strutils,
    vcl.Grids;

type
    TStringGridHelper = class helper for TStringGrid
        procedure clear();
        function  myToString(): string;
        function  selectionToString(): string;
    end;

implementation

procedure   TStringGridHelper.clear();
var
    i,j: integer;
begin
    for i := 0 to ColCount - 1 do
        for j := 0 to RowCount - 1 do
            Cells[i,j] := '';
end;


function   TStringGridHelper.myToString(): string;
var
    i, j: integer;
    s: string;
begin
    {copy the strings in the stringgrid to a string, separated by #09 tab
    and line breaks. This allows you to paste directly into excel. note, the
    calls to leftstr are there to remove the last extra #09 and #13#10}
    s := '';
    for j := 0 to RowCount - 1 do
    begin
        for i := 0 to ColCount - 1 do
            s := s + Cells[i,j] + #09;
        s := leftstr(s,length(s) - 1) + #13#10;
    end;
    result := leftstr(s,length(s) - 2);
end;


function TStringGridHelper.selectionToString: string;
var
    row, col: integer;
begin
    result := '';
    for Row := self.Selection.Top to self.Selection.Bottom do
    begin
        for Col := self.Selection.Left to self.Selection.Right do
            result := result + self.Cells[col,row] + #09;
        result := LeftStr(result, length(result) - 1) + #13#10;
    end;

    if length(result) >= 2 then
        result := LeftStr(result, length(result) - 2);
end;

end.
