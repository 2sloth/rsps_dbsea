unit MemoryStreamHelper;

interface
uses system.classes, system.zlib, system.sysutils, system.threading,
    smartPointer;

type
    TMemoryStreamHelper = class helper for TMemoryStream
        function  compress: TMemoryStream;
        function  decompress: TMemoryStream;
        procedure myWrite(const b: TBytes); overload;
        procedure myWrite(const s: String); overload;
        function  getBytes(const aPos1, aPos2: int64): TBytes;
    end;

implementation


function TMemoryStreamHelper.compress: TMemoryStream;
var
    future: IFuture<integer>;
    ZStream   : ISmart<TCompressionStream>;
begin
    result := TMemoryStream.Create();
    ZStream := TSmart<TCompressionStream>.create(TCompressionStream.Create( TCompressionLevel( 2 ), result));
    future := TTask.future<integer>(function: integer
    begin
        //do work on background thread
        ZStream.CopyFrom( self, 0 );
        result := 0;
    end);
    result.Position := future.value;
end;

function TMemoryStreamHelper.decompress: TMemoryStream;
var
    future: IFuture<TDecompressionStream>;
    ZStream    : ISmart<TDeCompressionStream>;
begin
    result := TMemoryStream.Create();
    self.Position := 0;
    future := TTask.Future<TDecompressionStream>(function: TDecompressionStream
    begin
        result := TDeCompressionStream.Create( self );
    end);
    ZStream := TSmart<TDeCompressionStream>.create(future.value);
    result.CopyFrom( ZStream, 0 );
    result.Position := 0;
end;

procedure TMemoryStreamHelper.myWrite(const b: TBytes);
begin
    if length(b) > 0 then
        self.write(b[0], length(b));
end;

function  TMemoryStreamHelper.getBytes(const aPos1, aPos2: int64): TBytes;
begin
    setlength(result,aPos2 - aPos1);
    self.Position := aPos1;
    self.Read(result[0],aPos2 - aPos1);
end;

procedure TMemoryStreamHelper.myWrite(const s: String);
begin
    if length(s) > 0 then
        self.write(s[1], length(s) * sizeof(char));
end;

end.
