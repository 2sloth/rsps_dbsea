unit StringsHelper;

interface

uses system.classes, system.StrUtils;

type
    TStringsHelper = class helper for TStrings
        function  contains(const s: string): boolean;
        function  join(const pre, delim, post: string): string;
        function  jsonJoin: string;
    end;

implementation

{ TStringsHelper }

function TStringsHelper.contains(const s: string): boolean;
var
    q: string;
begin
    for q in self do
        if q = s then
            exit(true);

    result := false;
end;

function TStringsHelper.join(const pre, delim, post: string): string;
var
    i: integer;
begin
    result := pre;
    for I := 0 to self.count - 1 do
        result := result + ifthen(i = 0, '', delim) + self[i];
    result := result + post;
end;

function TStringsHelper.jsonJoin: string;
begin
    result := self.join('{',',','}');
end;

end.
