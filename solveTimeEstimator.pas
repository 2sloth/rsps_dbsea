unit solveTimeEstimator;

interface

uses
  winapi.MMSystem, helperFunctions, system.math;

type

  TSolveTimeEstimator = class(TObject)
  public
    iLowTotSlicesTimesFreq,iCurrentLowSlicesTimesFreq: int64;
    iHighTotSlicesTimesFreq,iCurrentHighSlicesTimesFreq: int64;
    cSolverStartTime: cardinal;
    prevSolveTimes: array [0..31] of cardinal;
    iNPrev: cardinal;

    constructor create;
    procedure SolveTimerStart;
    procedure rotatePrevTimes;
    procedure UpdateRemainingSolveTime(const useLowFreqSolverText, splitSolver : boolean; out hrsStr, minsStr, secsStr : String; out fractionDone : double);
    procedure newSolveTime(aVal : cardinal);
    function  getAverageSolveTime : cardinal;
  end;


implementation

constructor TSolveTimeEstimator.Create;
begin
    inherited Create;
    iLowTotSlicesTimesFreq := 0;
    iCurrentLowSlicesTimesFreq := 0;
    iHighTotSlicesTimesFreq := 0;
    iCurrentHighSlicesTimesFreq := 0;
    cSolverStartTime := timegettime;
    iNPrev := 0;
end;


procedure TSolveTimeEstimator.SolveTimerStart;
var
    i : integer;
begin
    cSolverStartTime := timegettime;
    {set up the averaging functions}
    iNPrev := 0;
    for I := 0 to length(prevSolveTimes) - 1 do
        prevSolveTimes[i] := 0;
end;

procedure TSolveTimeEstimator.rotatePrevTimes;
var
    i : integer;
begin
    {move the prev estimates along to make space for the new one}
    for I := length(prevSolveTimes) - 1 downto 1 do
        prevSolveTimes[i] := prevSolveTimes[i - 1];
end;

procedure  TSolveTimeEstimator.newSolveTime(aVal : cardinal);
begin
    {add our most recent estimate to the list of estimated times}
    rotatePrevTimes;
    prevSolveTimes[0] := aVal;
    iNPrev := iNPrev + 1;
    if iNPrev > cardinal(length(prevSolveTimes)) then
        iNPrev := cardinal(length(prevSolveTimes));
end;

function TSolveTimeEstimator.getAverageSolveTime : cardinal;
var
    sum, i : cardinal;
begin
    {return the average of the time estimates in the list}
    if iNPrev = 0 then Exit(0);

    sum := 0;
    for I := 0 to iNPrev - 1 do
        sum := sum + prevSolveTimes[i];
    result := sum div iNPrev;
end;

procedure TSolveTimeEstimator.UpdateRemainingSolveTime(const useLowFreqSolverText, splitSolver : boolean;
                                                        out hrsStr, minsStr, secsStr : String;
                                                        out fractionDone : double);
var
    elapsedSecs : integer;
    estHours : integer;
    estSecs : integer;
    estMins : integer;
begin
    //NB if the solver is dBSeaRay, we have already taken account of the multi-freq solve

    elapsedSecs := (timeGetTime - cSolverStartTime) div 1000;
    if splitSolver and not useLowFreqSolverText then
        fractionDone := iCurrentHighSlicesTimesFreq/max(1,iHighTotSlicesTimesFreq) //currently doing the high f solve
    else
        fractionDone := iCurrentLowSlicesTimesFreq/max(1,iLowTotSlicesTimesFreq); //doing low f solve or not splitsolver

    estSecs := safetrunc((elapsedSecs/max(0.001,fractionDone)) * (1 - fractionDone));
    {do some averaging so it doesn't fly around quite so crazily}
    if estSecs < 0 then
        estSecs := 0;
    newSolveTime(estSecs);
    estSecs := getAverageSolveTime;

    estHours := estSecs div 3600;
    estSecs := estSecs - 3600*estHours;
    estMins := estSecs div 60;
    estSecs := estSecs - 60*estMins;

    if estHours > 0 then
        hrsStr := tostr(estHours) + ':'
    else
        hrsStr := '';

    if estMins < 10 then
        minsStr := '0' + tostr(estMins) + ':'
    else
        minsStr := tostr(estMins) + ':';

    if estSecs < 10 then
        SecsStr := '0' + tostr(estSecs)
    else
        SecsStr := tostr(estSecs);
end;


end.
