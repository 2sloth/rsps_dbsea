
var express = require('express');
var app = express();

app.get('/serialNumber/:id', function(req, res) {
    Parse.Cloud.useMasterKey();
    (new Parse.Query(Parse.Object.extend('License'))).equalTo('keyNumber', req.params.id).find().then(function(licenses) {
    	if (!licenses) {
    		res.send(500, 'Found no licenses object');
		} else if (licenses.length === 0) {
        	res.send(500, 'Found no license with that serial number');
        } else if (licenses.length > 1) {
        	res.send(500, 'Found multiple licenses with that serial number');
        } else if (!licenses[0].get('start') || !licenses[0].get('end') || !licenses[0].get('version')) {
        	res.send(500, 'License malformed');
        } else {
        	res.send(200, '<dBSeaMaintenance>' + 
        				'<key>' + req.params.id + '</key>' + 
        				'<start>' + formatDate(licenses[0].get('start')) + '</start>' + 
        				'<end>' + formatDate(licenses[0].get('end')) + '</end>' + 
        				'<version>' + licenses[0].get('version') + '</version>' + 
        			'</dBSeaMaintenance>');
        }
    }, function() {
        res.send(500, "Database error");  
    });   
});

function formatDate(date) {
	return date.getDate().toString() + '/' + (date.getMonth() + 1).toString() + '/' + date.getFullYear().toString();
}

app.listen();
