unit GPXParser;

interface
uses system.sysutils,
    xml.xmlintf, Xml.xmldom, xml.xmldoc,
    system.win.comobj,
    vcl.forms,
    winapi.msxmlintf, winapi.windows,
    generics.collections,
    helperFunctions, dbseaconstants, highorder, baseTypes;

type

    TLatLng = class(TObject)
    public
    var
        lat,lng: double;
        time: TDateTime;

        function  toTEastNorth(): TEastNorth;
    end;
    TLatLngList = TObjectList<TLatLng>;

    TTrack = class(TObject)
    public
      var
        name: String;
        points: TLatLngList;

        constructor create;
        destructor Destroy; override;
    end;
    TTrackList = TObjectList<TTrack>;

    TGPXParser = class(TObject)
    private
        class function  trackWithNode(const node: IDomNode): TTrack;
        class function  latLngWithNode(const node: IDOMNode): TLatLng;
    public
        class function  parse(const filename: TFilename): TTrackList;
    end;

implementation

constructor TTrack.create;
begin
    inherited;

    points := TLatLngList.Create(true);
end;

destructor TTrack.Destroy;
begin
    points.Free;

    inherited;
end;


class function TGPXParser.parse(const filename: TFilename): TTrackList;
var
    fileText        : String;
    xml             : IXMLDOMDocument;
    LDoc            : IXMLDocument;
    track           : TTrack;
    sel             : IDOMNodeSelect;
    iNode           : IDOMNode;
    i               : integer;
begin
    result := nil;
    fileText := THelper.readFileToString(filename);

    {trial xml parse. here we are creating an extra object just for parsing, should check
    if there is a better way to do this}
    xml := CreateOleObject('Microsoft.XMLDOM') as IXMLDOMDocument;
    xml.async := False;
    xml.loadXML(fileText);
    if xml.parseError.errorCode <> 0 then
    begin
        //showmessage('Unable to parse file, file format appears faulty');
        Exit;
    end;

    LDoc := TXMLDocument.Create(application);
    LDoc.LoadFromXML(fileText);

    for i := 0 to ldoc.DOMDocument.childNodes.length - 1 do
        if ansicomparetext(ldoc.DOMDocument.childNodes[i].nodeName, 'gpx') = 0 then
            iNode := ldoc.DOMDocument.childNodes[i];

    sel := LDoc.DOMDocument as IDOMNodeSelect;

    {check if this looks like it might be an openable file}
    if not assigned(iNode) then
    begin
        //showmessage('Unable to find file header, check file type');
        Exit;
    end;

    result := TTrackList.create(true);

    for i := 0 to iNode.childNodes.length - 1 do
        if AnsiCompareText(iNode.childNodes[i].nodeName, 'trk') = 0 then
        begin
            track := TGPXParser.trackWithNode(iNode.childNodes[i]);
            if track.points.Count > 0 then
                result.add(track);
        end;
end;


class function TGPXParser.latLngWithNode(const node: IDOMNode): TLatLng;
var
    sTime : string;
    i: integer;
    lat, lon: string;
    child: IDOMNode;

  FmtStngs: TFormatSettings;
begin
    result := nil;

    if node.attributes.getNamedItem('lat') = nil then exit;
    if node.attributes.getNamedItem('lon') = nil then exit;

    lat := node.attributes.getNamedItem('lat').nodeValue;
    lon := node.attributes.getNamedItem('lon').nodeValue;

    if not isFloat(lat) or not isFloat(lon) then exit;

    result := TLatLng.Create;
    result.lat := StrToFloat(lat);
    result.lng := StrToFloat(lon);
    result.time := NULL_DATE;

    FmtStngs := TFormatSettings.Create(LOCALE_USER_DEFAULT);
    FmtStngs.DateSeparator := '-';
    FmtStngs.ShortDateFormat := 'yyyy mm dd';

    for i := 0 to node.childNodes.length - 1 do
    begin
        child := node.childNodes[i];
        if AnsiCompareText(child.nodeName, 'time') = 0 then
        begin
            sTime := child.childNodes[0].nodeValue;
            try
                result.time := StrToDateTime(sTime, FmtStngs);
            except
            end;
        end;
    end;
end;


class function TGPXParser.trackWithNode(const node: IDomNode): TTrack;
var
    child: IDomNode;
    i, j: integer;
    latlng: TLatLng;
begin
    result := TTrack.create;

    for I := 0 to node.childNodes.length - 1 do
    begin
        child := node.childNodes[i];
        if AnsiCompareText(child.nodeName, 'name') = 0 then
            result.name := child.childNodes[i].nodeValue;

        if AnsiCompareText(child.nodeName, 'trkseg') = 0 then
            for j := 0 to child.childNodes.length - 1 do
                if AnsiCompareText(child.childNodes[j].nodeName, 'trkpt') = 0 then
                begin
                    latlng := TGPXParser.latLngWithNode(child.childNodes[j]);
                    if latlng <> nil then
                        result.points.add(latlng);
                end;
    end;
end;

{ TLatLng }

function TLatLng.toTEastNorth: TEastNorth;
begin
    result.latitude := self.lat;
    result.longitude := self.lng;
end;

end.
