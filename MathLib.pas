unit MathLib;

//{$DEFINE USE_MATHLIB}
{$IFDEF USE_MATHLIB}

interface

const

    dll = {$IFDEF WIN64} 'MathLib64.dll'; {$ELSE} 'MathLib.dll'; {$ENDIF}

  function forwardFFTSingle(pHandle: pointer; x_in, x_out: psingle): LongInt; cdecl;
  function backwardFFTSingle(pHandle: pointer; x_in, x_out: psingle): LongInt; cdecl;
  function forwardFFTDouble(pHandle: pointer; x_in, x_out: pdouble): LongInt; cdecl;
  function backwardFFTDouble(pHandle: pointer; x_in, x_out: pdouble): LongInt; cdecl;
  function setupFFTDescriptorSingle(var pHandle: pointer; length: integer; backwardScale: boolean): LongInt; cdecl;
  function setupFFTDescriptorDouble(var pHandle: pointer; length: integer; backwardScale: boolean): LongInt; cdecl;
  function freeFFTDescriptor(var pHandle: pointer): LongInt; cdecl;

type
    TComplexSingle = packed record
        re,im: single;
    end;
    TComplexSingleArray = array of TComplexSingle;
    TComplexSingleArrayHelper = record helper for TComplexSingleArray
        function pFirst: pSingle;
    end;

    TComplexDouble = packed record
        re,im: double;
    end;
    TComplexDoubleArray = array of TComplexDouble;
    TComplexDoubleArrayHelper = record helper for TComplexDoubleArray
        function pFirst: pDouble;
    end;

implementation

    function forwardFFTSingle; external dll;
    function backwardFFTSingle; external dll;
    function forwardFFTDouble; external dll;
    function backwardFFTDouble; external dll;
    function setupFFTDescriptorSingle; external dll;
    function setupFFTDescriptorDouble; external dll;
    function freeFFTDescriptor; external dll;

{ TComplexDoubleArrayHelper }

function TComplexDoubleArrayHelper.pFirst: pDouble;
begin
    result := pdouble(@self[0]);
end;

{ TComplexSingleArrayHelper }

function TComplexSingleArrayHelper.pFirst: pSingle;
begin
    result := psingle(@self[0]);
end;

{$ELSE}

interface

implementation

{$ENDIF}

end.
