unit eventLogging;

interface

uses math, classes, sysutils, dialogs, system.UITypes, vcl.forms, vcl.appevnts
    {$IFNDEF HASP_DIAGNOSTIC}
    , syncommons
    {$ENDIF}
    ;

type
    {$IFNDEF HASP_DIAGNOSTIC}
    TSynLogLevs = class(TSynLog);
    {$ENDIF}

    TEventLog = class(TStringList)
    public
        procedure log(const s: string; const synlog: ISynLog = nil); overload;
        procedure log(const synlog: ISynLog; const s: string); overload;
        procedure logException(const synlog: ISynLog; const s: string);
        procedure appendToLast(const s: string);
        procedure onApplicationException(Sender: TObject; E: Exception);

        class function completeVersionString: string; static;
    end;

var
    eventLog: TEventLog;

implementation

uses MCKLib;

procedure TEventLog.appendToLast(const s: string);
begin
    if self.count <> 0 then
        self[self.count - 1] := self[self.count - 1] + s;
end;

procedure TEventLog.log(const s: string; const synlog: ISynLog = nil);
begin
    //prevent log file from growing too large
    if self.Count > 10000 then
        self.Delete(0);
    self.Add(DateToStr(now) + ' ' + TimeToStr(now) + ' ' + s);

    if assigned(synlog) then
        synlog.Log(sllInfo, rawutf8(s));
end;


procedure TEventLog.log(const synlog: ISynLog; const s: string);
begin
    self.log(s, synlog);
end;

procedure TEventLog.logException(const synlog: ISynLog; const s: string);
begin
    synlog.Log(sllException, rawutf8(s));
    self.log(s, nil);
end;

procedure TEventLog.onApplicationException(Sender: TObject; E: Exception);
var
    e0: Exception;
begin
    e0 := Exception(AcquireExceptionObject);
    try
        raise e0 at ExceptAddr; //for some reason, if we reraise then the stack trace is available
    except on e1: exception do
        begin
            TSynLogLevs.Enter().log(sllException, rawutf8(e.ToString));
            TSynLogLevs.Enter().log(sllException, rawutf8(e.StackTrace));
            TSynLogLevs.Family.SynLog.Flush(true);

            self.log('ERROR: ' + e.ToString, nil);
            self.log('STACK TRACE: ' + e.StackTrace, nil);

            application.ShowException(e);
        end;
    end;
end;

class function TEventLog.completeVersionString: string;
begin
    result := fileVersion(application.ExeName);
    {$IFDEF LITE}
    result := result + ' Basic';
    {$ENDIF}
    {$IFDEF DEBUG}
    result := result + ' DEBUG';
    {$ENDIF}
end;

initialization
    eventLog := TEventLog.create;
    eventLog.log('dBSea version: ' + TEventLog.completeVersionString());
    Application.OnException := eventLog.onApplicationException;

finalization
    eventLog.free;

end.
