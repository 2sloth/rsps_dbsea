unit levelConverter;

interface

uses math, strutils;

type

    TLevelType = (kSPL,kSELfreq,kSPLCrestFactor,kSELtime,kSPLpk,kSPLpkpk);
    TSourceLevelType = (kSL,kSWL,kSELfreqSrc);
      //freq based:
      //  kSPL source level dB re 1 uPa at 1 m, or sound power level dB re 1 uPa
      //  kSWL source power level (Lw)  dB re 1 pW
      //  kSELfreq sound exposure level  dB re 1 uPa with T0 = 1 s  (needs assessment period)
      //time based:
      //  kSELtime sound exposure level  dB re 1 uPa with T0 = 1 s  (needs assessment period)
      //  kSPLpk sound power level pk  dB re 1 uPa
      //  kSPLpkpk sound power level pk to pk  dB re 1 uPa

    TLevelConverter = class(TObject)
    public
        class function convert(const lev : double; const fromType, toType : TLevelType; const assessmentPeriod : double = nan; const crestFactor : double = 0) : double; overload;
        class function convert(const lev : single; const fromType, toType : TLevelType; const assessmentPeriod : double = nan; const crestFactor : double = 0) : single; overload;
        class function convertSourceLevel(const lev : double; const fromType, toType : TSourceLevelType;  const assessmentPeriod : double = nan; const crestFactor : double = 0) : double; overload;
        class function convertSourceLevel(const lev : single; const fromType, toType : TSourceLevelType;  const assessmentPeriod : double = nan; const crestFactor : double = 0) : single; overload;
        class function isTimeSeries(const levType : TLevelType) : boolean;
        class function isUnweighted(const levType : TLevelType) : boolean;
        class function levelTypeName(const levType : TLevelType) : string;
        class function sourceLevelTypeName(const levType : TSourceLevelType) : string;
    end;


implementation

class function TLevelConverter.convert(const lev : double;
                                       const fromType, toType : TLevelType;
                                       const assessmentPeriod : double = nan;
                                       const crestFactor : double = 0) : double;
begin
    //NB we don't know how to convert kSPLpkpk and kSPLpk to rms-based measures without more info
    if fromType = toType then exit(lev);

    result := nan;
    case fromType of
        kSPL: begin
            case toType of
                kSPL: result := lev;
                kSELfreq: result := lev + 10*log10(assessmentPeriod);
                kSPLCrestFactor : result := lev + crestFactor;
                kSELtime : ;
                kSPLpk: ;
                kSPLpkpk: ;
            end;
        end;

        kSELfreq: begin
            case toType of
                kSPL: result := lev - 10*log10(assessmentPeriod);
                kSELfreq: result := lev;
                kSPLCrestFactor : result := lev - 10*log10(assessmentPeriod) + crestFactor;
                kSELtime : ;
                kSPLpk: ;
                kSPLpkpk: ;
            end;
        end;

        kSPLCrestFactor: begin
            case toType of
                kSPL: result := lev - crestFactor;
                kSELfreq: result := lev + 10*log10(assessmentPeriod) - crestFactor;
                kSPLCrestFactor : result := lev;
                kSELtime : ;
                kSPLpk: ;
                kSPLpkpk: ;
            end;
        end;

        kSELtime: begin
            case toType of
                kSPL: ;
                kSELfreq: ;
                kSPLCrestFactor : ;
                kSELtime : result := lev;
                kSPLpk: ;
                kSPLpkpk: ;
            end;
        end;

        kSPLpk: begin
            case toType of
                kSPL: ;
                kSELfreq: ;
                kSPLCrestFactor : ;
                kSELtime : ;
                kSPLpk: result := lev;
                kSPLpkpk: ;
            end;
        end;

        kSPLpkpk: begin
            case toType of
                kSPL: ;
                kSELfreq: ;
                kSPLCrestFactor : ;
                kSELtime : ;
                kSPLpk: ;
                kSPLpkpk: result := lev;
            end;
        end;
    end;
end;

class function TLevelConverter.convertSourceLevel(const lev : double;
                                                  const fromType, toType : TSourceLevelType;
                                                  const assessmentPeriod : double = nan;
                                                  const crestFactor : double = 0) : double;
const
    const1m : double = 50.7688139503; //10*log10(1000*1500/4/pi)
begin
    if fromType = toType then exit(lev);

    result := nan;
    case fromType of
        kSL: begin
            case toType of
                kSL: result := lev;
                kSWL: result := lev - const1m;
                kSELfreqSrc: result := lev + 10*log10(assessmentPeriod);
            end;
        end;

        kSWL: begin
            case toType of
                kSL: result := lev + const1m;
                kSWL: result := lev;
                kSELfreqSrc: result := lev + const1m + 10*log10(assessmentPeriod);
            end;
        end;

        kSELfreqSrc: begin
            case toType of
                kSL: result := lev - 10*log10(assessmentPeriod);
                kSWL: result := lev - 10*log10(assessmentPeriod) - const1m;
                kSELfreqSrc: result := lev;
            end;
        end;
    end;
end;

class function TLevelConverter.convert(const lev : single; const fromType, toType : TLevelType; const assessmentPeriod : double = nan; const crestFactor : double = 0) : single;
var
    d : double;
begin
    d := lev;
    result := TLevelConverter.convert(d,fromType,toType,assessmentPeriod,crestFactor);
end;

class function TLevelConverter.convertSourceLevel(const lev : single; const fromType, toType : TSourceLevelType;  const assessmentPeriod : double = nan; const crestFactor : double = 0) : single;
var
    d : double;
begin
    d := lev;
    result := TLevelConverter.convertSourceLevel(d,fromType,toType,assessmentPeriod,crestFactor);
end;

class function TLevelConverter.isTimeSeries(const levType : TLevelType) : boolean;
begin
    result := (levType = kSELtime) or (levType = kSPLpk) or (levtype = kSPLpkpk);
end;

class function TLevelConverter.isUnweighted(const levType : TLevelType) : boolean;
begin
    result := (levType = kSPLCrestFactor) or (levType = kSPLpk) or (levtype = kSPLpkpk);
end;


class function TLevelConverter.levelTypeName(const levType : TLevelType) : string;
begin
    case levType of
        kSPL: result := 'SPL';
        //kSWL: result := 'SWL';
        kSELfreq: result := 'SELcum';
        kSPLCrestFactor: result := 'SPLpeak';
        kSELtime: result := 'SELcum';
        kSPLpk: result := 'SPLpeak';
        kSPLpkpk: result := 'SPLpk-pk';
    end;
end;

class function TLevelConverter.sourceLevelTypeName(const levType : TSourceLevelType) : string;
begin
    case levType of
        kSL: result := 'SL';
        kSWL: result := 'SWL';
        kSELfreqSrc: result := 'SEL';
    end;
end;

end.
