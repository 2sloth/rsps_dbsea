unit flock;

{$B-}

interface

uses
    syncommons, eventlogging,
    system.classes, system.math,
    vcl.Graphics, vcl.forms, vcl.extctrls, vcl.dialogs,
    generics.collections, generics.Defaults,
    system.sysutils, system.types, system.threading,
    system.Diagnostics,
    basetypes, helperFunctions,
    dbseaconstants,
    taskbarprogress, animat,
    BathymetryInterface,
    soundSource, OtlParallel, SmartPointer;

type

{$RTTI INHERIT}



    IFlockDelegate = interface(IInterface)
        function resultsExist: boolean;
        function getLevAtInds(const d, i, j, p: integer; const levelsDisplay: TLevelsDisplay; const bSeafloorNaNs: boolean): single;
        function levelsDifferBetweenTimes(const time1, time2: double): boolean;
        function getFieldAtTime(const time: double): TDoubleField;
    end;

    TFlock = class(TInterfacedPersistent, IAnimatDelegate)
    private
    var
        fIsReady: boolean;
        stepTime: double;
        fKeepPosHistory: boolean;

        delegate: IFlockDelegate;
        bathyDelegate : IBathymetryDelegate;

        procedure calcDistances;
        procedure initField;
        procedure initAnimats(const numberOfAnimats : integer);
        procedure setThresholdLevel(const thresholdLevel : double);
        procedure init(const numberOfAnimats : integer; const thresholdLevel : double; const aStepTime : double; const keepPosHistory : boolean; const imax,jmax,dmax: integer; const adx,ady,adz: double; const sources: TSourceList);
        procedure runFlocking(const bAnimatsReactToSoundLevels, bParallel, bSimpleFleeing : boolean);

        //delegate methods
        function  getStepTime: double;
    public
    var
        nx : integer;
        ny : integer;
        nz : integer;
        dx : double;
        dy : double;
        dz : double;

        step : integer;
        animats : TAnimatList;
        field : TDoubleField;
        distances : array of TDoubleDynArray;
        nAnimatsOverThresholdAtStep : TList<integer>;
        sources: TSourceList;

        separationForce : double;
        alignmentForce : double;
        cohesionForce : double;
        soundForce : double;

        constructor Create(const aDelegate: IFlockDelegate; const aBathyDelegate : IBathymetryDelegate);
        destructor Destroy; override;

        procedure runFlock(const glDraw, glSetRedrawResults: TObjectProcedure;
                           var glLevsRA: TSingleMatrix;
                           const pCancel: TWrappedBool;
                           const nSteps, nAnimats, updateGLEveryNSteps: integer;
                           const threshold: single;
                           const totalTime, stepTime: double;
                           const bAnimatsReactToSoundLevels,parallel,bSimpleFleeing: boolean;
                           const stepProgressBars: TProcedureWithInt;
                           const imax,jmax,dmax: integer;
                           const adx,ady,adz: double;
                           const sources: TSourceList);
        function  outputPositions : string;
        function  nActive : integer;
        procedure clear;
        procedure sortAnimatsBySEL;
        procedure sortAnimatsByPeak;
        function  simulationTime : double;

        function  xMax : double;
        function  yMax : double;
        function  zMax : double;

        function  levelAtPos(const p : TFPoint3) : double;
        function  gradientAtPos(const p : TFPoint3) : TFPoint3;
        procedure getijkFromPos(const p : TFPoint3; out i,j,k : integer);
        function  animatSELArray : TSingleDynArray;
        function  animatPeakArray : TSingleDynArray;
        function  animatLevsHistory(const outputPerNSteps : integer) : TSingleMatrix;
        procedure writeToFile(const filename: TFilename; const writeAll : boolean; const selected: TBooleanDynArray);

        property  isReady : boolean read fIsReady;
        property  keepPosHistory : boolean read fKeepPosHistory;
    end;


implementation

constructor TFlock.Create(const aDelegate: IFlockDelegate; const aBathyDelegate : IBathymetryDelegate);
begin
    fIsReady := false;

    self.delegate := aDelegate;
    self.bathyDelegate := aBathyDelegate;
    self.stepTime := 1.0;
    nAnimatsOverThresholdAtStep := TList<integer>.create;
    step := 0;

    fKeepPosHistory := false;

    separationForce := 3.;
    alignmentForce := 2.;
    cohesionForce := 2.;
    soundForce := 6.;

    animats := TAnimatList.create(true);
end;

destructor TFlock.Destroy;
begin
    animats.Clear;
    animats.Free;
    nAnimatsOverThresholdAtStep.Free;

    inherited;
end;

procedure TFlock.init(const numberOfAnimats : integer;
                      const thresholdLevel : double;
                      const aStepTime : double;
                      const keepPosHistory : boolean;
                      const imax,jmax,dmax: integer;
                      const adx,ady,adz: double;
                      const sources: TSourceList);
begin
    nx := iMax;
    ny := jMax;
    nz := dMax;
    dx := adx;
    dy := ady;
    dz := adz;

    fKeepPosHistory := keepPosHistory;
    self.stepTime := aSteptime;
    step := 0;
    nAnimatsOverThresholdAtStep.Clear;

    initField;
    initAnimats(numberOfAnimats);
    setThresholdLevel(thresholdLevel);

    self.sources := sources;

    fIsReady := true;
end;

procedure TFlock.initField;
var
    i,j,d,p: integer;
begin
    //setup sound field
    p := -1; //moving pos index
    setlength(field,nx,ny,nz);
    if delegate.resultsExist then
        for d := 0 to length(field[0,0]) - 1 do
            for i := 0 to length(field) - 1 do
                for j := 0 to length(field[0]) - 1 do
                    field[i,j,d] := delegate.getLevAtInds(d,i,j,p, ldSingleLayer, true); //don't need to convert to user leveltype
end;

procedure TFlock.initAnimats(const numberOfAnimats : integer);
var
    i : integer;
    animat : TAnimat;
begin
    animats.clear;
    for i := 0 to numberOfAnimats - 1 do
    begin
        animats.Add(TAnimat.Create(self));
        animat := animats.Last;

        animat.thresholdLevel := 0;

        repeat
            animat.pos.x := random * xMax;
            animat.pos.y := random * yMax;
            animat.pos.z := random * bathyDelegate.getBathymetryAtPos(animat.pos.x, animat.pos.x, false); //will get changed later if too deep
            //if animat.pos.z < 0 then
            //    animat.pos.z := nan;
        until animat.pos.z > 0;

        animat.dpos.x := random - 0.5;
        animat.dpos.y := random - 0.5;
        animat.dpos.z := 0.05 * (random - 0.5); //almost horizontal motion
        animat.dpos.normalise;
        animat.dpos.multBy(animat.maxSpeed * random); //animat can be swimming by some fraction of its maxSpeed
        //animat.dpos.setZero;

        animat.removeFromCalc := false;
    end;
end;

procedure TFlock.setThresholdLevel(const thresholdLevel : double);
var
    animat : TAnimat;
begin
    for animat in animats do
        animat.thresholdLevel := thresholdLevel;
end;

function TFlock.gradientAtPos(const p : TFPoint3) : TFPoint3;
var
    i,j,k : integer;
begin
    getijkFromPos(p,i,j,k);

    //forward, backward or central difference
    if i <= 0 then
        result.x := (field[i + 1,j,k] - field[i    ,j,k]) / dx
    else if i >= length(field) - 1 then
        result.x := (field[i    ,j,k] - field[i - 1,j,k]) / dx
    else
        result.x := (field[i + 1,j,k] - field[i - 1,j,k]) / dx * 0.5;

    if j <= 0 then
        result.y := (field[i,j + 1,k] - field[i,j    ,k]) / dy
    else if j >= length(field[0]) - 1 then
        result.y := (field[i,j    ,k] - field[i,j - 1,k]) / dy
    else
        result.y := (field[i,j + 1,k] - field[i,j - 1,k]) / dy * 0.5;

    if k <= 0 then
        result.z := (field[i,j,k + 1] - field[i,j,k    ]) / dz
    else if k >= length(field[0,0]) - 1 then
        result.z := (field[i,j,k    ] - field[i,j,k - 1]) / dz
    else
        result.z := (field[i,j,k + 1] - field[i,j,k - 1]) / dz * 0.5;
end;

function  TFlock.levelAtPos(const p : TFPoint3) : double;
var
    i,j,k : integer;
begin
    getijkFromPos(p,i,j,k);
    result := field[i,j,k];
end;

procedure TFlock.getijkFromPos(const p : TFPoint3; out i,j,k : integer);
begin
    i := max(0,min(length(field     ) - 1 , saferound(p.x / dx)));
    j := max(0,min(length(field[0  ]) - 1 , saferound(p.y / dy)));
    k := max(0,min(length(field[0,0]) - 1 , saferound(p.z / dz)));
end;

function TFlock.getStepTime: double;
begin
    result := self.steptime;
end;

function  TFlock.animatSELArray : TSingleDynArray;
var
    i : integer;
begin
    setlength(result,self.animats.Count);
    for i := 0 to self.animats.count - 1 do
        result[i] := self.animats[i].SEL;
end;

function  TFlock.animatPeakArray : TSingleDynArray;
var
    i : integer;
begin
    setlength(result,self.animats.Count);
    for i := 0 to self.animats.count - 1 do
        result[i] := self.animats[i].peakLevel;
end;

function  TFlock.animatLevsHistory(const outputPerNSteps : integer) : TSingleMatrix;
var
    animat : TAnimat;
    i,j,k : integer;
begin
    setlength(result, self.animats.Count, step div outputPerNSteps);
    for i := 0 to self.animats.Count - 1 do
    begin
        animat := self.animats[i];
        for j := 0 to (step div outputPerNSteps) - 1 do
        begin
            k := j * outputPerNSteps;
            if k < animat.levelHistory.count then
                result[i,j] := animat.levelHistory[k]
            else
                result[i,j] := nan;
        end;
    end;
end;

procedure TFlock.runFlock(const glDraw, glSetRedrawResults: TObjectProcedure; var glLevsRA: TSingleMatrix; const pCancel: TWrappedBool; const nSteps, nAnimats, updateGLEveryNSteps: integer; const threshold: single; const totalTime, stepTime: double; const bAnimatsReactToSoundLevels,parallel,bSimpleFleeing : boolean; const stepProgressBars: TProcedureWithInt; const imax,jmax,dmax: integer; const adx,ady,adz: double; const sources: TSourceList);
var
    step, i, j, d : integer;
    Log : ISynLog;
    stopwatch: TStopwatch;
    thisLev: single;
begin
    Log := TSynLogLevs.Enter(self,'SolveNow');
    eventLog.log('Starting moving receivers simulation with ' + IntToStr(nAnimats) + ' animats, ' + inttostr(nSteps) + ' steps');
    Log.Log(sllInfo,'Starting moving receivers simulation with % animats, % steps',[nAnimats, nSteps]);
    stopwatch := TStopwatch.StartNew;

    self.init(nAnimats,threshold,stepTime,true,imax,jmax,dmax,adx,ady,adz,sources);
    setlength(glLevsRA,imax,jmax);
    for step := 0 to nSteps - 1 do
    begin
        if pCancel.b then break;

        if (step = 0) or delegate.levelsDifferBetweenTimes((step - 1) * stepTime, step * stepTime) then
        begin
            self.field := delegate.getFieldAtTime(step * stepTime);

            for i := 0 to imax - 1 do
                for j := 0 to jmax - 1 do
                begin
                    thisLev := nan;

                    for d := 0 to dmax - 1 do
                        if not isnan(field[i,j,d]) and (isnan(thisLev) or (field[i,j,d] > thisLev)) then
                            thisLev := field[i,j,d];

                    if not isNaN(thisLev) and (thisLev >= lowLev) then
                        glLevsRA[i,j] := thisLev;
                end;

            glSetRedrawResults;
            gldraw;
            application.processMessages;
        end;

        runFlocking(bAnimatsReactToSoundLevels,parallel,bSimpleFleeing);
        stepProgressBars(1);
        if step mod updateGLEveryNSteps = 0 then
        begin
            glDraw;
            application.processmessages;
        end;
    end;
    setlength(glLevsRA,0);
    eventLog.log('Finished moving receivers simulation, elapsed time (s): ' + tostrWithTestAndRound1(Stopwatch.Elapsed.TotalSeconds));
    Log.Log(sllInfo,'Finished moving receivers simulation, elapsed time (s): %',[tostrWithTestAndRound1(Stopwatch.Elapsed.TotalSeconds)]);
end;

procedure TFlock.runFlocking(const bAnimatsReactToSoundLevels, bParallel, bSimpleFleeing : boolean);
var
    nAboveThreshold: TBooleanDynArray;
    procedure nonParallelLoop();
    var
        i: integer;
        a: TAnimat;
    begin
        for i := 0 to animats.count - 1 do
        begin
            a := animats[i];
            if (not a.removeFromCalc) and a.runFlocking(self.steptime,
                                                        animats,
                                                        self.distances[i],
                                                        separationForce, alignmentForce, cohesionForce, soundForce,
                                                        levelAtPos(a.pos),
                                                        gradientAtPos(a.pos),
                                                        bathyDelegate.getBathymetryAtPos(a.pos.x,a.pos.y,false),
                                                        self.fKeepPosHistory,
                                                        bAnimatsReactToSoundLevels,
                                                        bSimpleFleeing,
                                                        sources) then
                nAboveThreshold[i] := true;
        end;
    end;

begin
    if not isReady then exit;
    if isnan(stepTime) or (stepTime <= 0) then exit;

    if (length(field     ) <> nx) or
       (length(field[0  ]) <> ny) or
       (length(field[0,0]) <> nz) then
       initField;

    calcDistances;

    setlength(nAboveThreshold,animats.count);
    if bParallel then
    begin
        //run animats in parallel
        //TParallel.for(0, animats.count - 1, procedure(i: integer)
        Parallel.for(0, animats.count - 1).execute(procedure (i: integer)
        var
            a: TAnimat;
        begin
            a := animats[i];
            if (not a.removeFromCalc) and a.runFlocking(self.steptime,
                                                        animats,
                                                        self.distances[i],
                                                        separationForce, alignmentForce, cohesionForce, soundForce,
                                                        levelAtPos(a.pos),
                                                        gradientAtPos(a.pos),
                                                        bathyDelegate.getBathymetryAtPos(a.pos.x,a.pos.y,false),
                                                        self.fKeepPosHistory,
                                                        bAnimatsReactToSoundLevels,
                                                        bSimpleFleeing,
                                                        sources) then
                nAboveThreshold[i] := true;
        end);
    end
    else
        nonParallelLoop();
    nAnimatsOverThresholdAtStep.Add(nAboveThreshold.countTrue);

    inc(step);
end;

function TFlock.outputPositions: string;
const
    delim: char = ',';
var
    a: TAnimat;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    sb.append(tostr(nActive) + ' active' + #13#10 + tostr(nAnimatsOverThresholdAtStep.Last) + ' over threshold' + #13#10);
    for a in animats do
        if not a.removeFromCalc then
            sb.append(tostr(animats.indexof(a)) + delim + tostr(round1(a.pos.x)) + delim + tostr(round1(a.pos.y)) + delim + tostr(round1(a.pos.z)) + #13#10);
    result := sb.ToString;
end;

function  TFlock.nActive : integer;
var
    a : TAnimat;
begin
    result := 0;
    for a in animats do
        if not a.removeFromCalc then
            inc(result);
end;

procedure TFlock.clear;
begin
    fIsReady := false;
end;

procedure TFlock.calcDistances;
begin
    setlength(distances,animats.Count,animats.Count);

    //TParallel.for(0,animats.count - 1, procedure(i : integer)
    Parallel.For(0,animats.count - 1).Execute(procedure (i: integer)
    var
        j: Integer;
        a, b : TAnimat;
    begin
        a := animats[i];
        if a.removeFromCalc then

        begin
            for j := 0 to animats.Count - 1 do
                distances[i,j] := nan;
        end
        else
        for j := 0 to animats.Count - 1 do
        begin
            b := animats[j];
            if b.removeFromCalc then
                distances[i,j] := nan
            else
                distances[i,j] := a.pos.dist(b.pos);
        end;
    end);
end;

function  TFlock.xMax : double;
begin
    result := (nx - 1) * dx;
end;

function  TFlock.yMax : double;
begin
    result := (ny - 1) * dy;
end;

function  TFlock.zMax : double;
begin
    result := (nz - 1) * dz;
end;


procedure TFlock.sortAnimatsBySEL;
begin
    animats.Sort(TComparer<TAnimat>.Construct(
        function (const L, R: TAnimat): integer
        var
            ls,rs : single;
        begin
            ls := L.SEL;
            rs := R.SEL;
            if ls = rs then
                Result := 0
            else
            if ls < rs then
                Result := -1
            else
                Result := 1;
        end
    ));
end;

procedure TFlock.writeToFile(const filename: TFilename; const writeAll : boolean; const selected: TBooleanDynArray);
var
    s: string;
    i: integer;
    animat: TAnimat;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    sb.append('Output levels: index' + #09 + 'SEL' + #09 + 'Peak' + #13#10);
    for i := 0 to self.animats.Count - 1 do
    begin
        animat := self.animats[i];
        if writeAll or ((i < length(selected)) and selected[i]) then
            sb.append(
                 IntToStr(i) +
                 tostrWithTestAndRound1(animat.SEL) + #09 +
                 tostrWithTestAndRound1(animat.peakLevel)  + #13#10);
    end;
    THelper.writeStringToFile(filename,s);
end;

procedure TFlock.sortAnimatsByPeak;
begin
    animats.Sort(TComparer<TAnimat>.Construct(
        function (const L, R: TAnimat): integer
        var
            ls,rs : single;
        begin
            ls := L.peakLevel;
            rs := R.peakLevel;
            if ls = rs then
                Result := 0
            else
            if ls < rs then
                Result := -1
            else
                Result := 1;
        end
    ));
end;


function  TFlock.simulationTime : double;
begin
    result := self.stepTime * self.step;
end;

end.
