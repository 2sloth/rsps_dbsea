unit wavReader;

interface

uses
    system.sysutils, system.classes, system.types,
    system.StrUtils, system.DateUtils, system.math,
    vcl.comctrls, vcl.forms,
    helperFunctions, dsp, filterbank, SmartPointer,
    baseTypes, FilterDef;

type

    TWAVInfo = record
    public
        nSamples: int64;
        channels, fs, bits: integer;
        startTime: TDate;
        hydrophoneID: ShortString;

        procedure clear;
    end;

    TWAVReader = class(TObject)
    {Unless otherwise mentioned, words are 16-bit unsigned integers
    ranging in value from 0..65535, integers are 16-bit values ranging
    from -32768..32767, doublewords are 32-bit unsigned integers ranging
    in value from 0..4294967295, singles are IEEE format 32-bit floating-point
    values, and doubles are IEEE format 64-bit floating-point values.}
    const
        WAVHeaderBytes = 44;
        channelByte = 22;
        bitsByte = 34;
        fsByte = 24;
        nSamplesByte = 40;
        nFileLengthByte = 4;
        ChunkSize = 1024*4;
    private
    var
        intBuffer16: array[0..ChunkSize - 1] of SmallInt;
        fInfo: TWAVInfo;

        fileStream: TFileStream;


        function  hydrophoneIDAsString: String;
        //function  readWAVOld(const FileName: string; const Offset, LengthToRead : Integer): TSmallIntDynArray;
        function  readWAV(const filename: TFilename; const channel, startSample, samplesToRead: int64; const pb: TProgressBar = nil): TSmallintdynarray;
        function  readWAVToFloat(const filename: TFilename; const channel, startSample, samplesToRead: int64; const pb: TProgressBar = nil): TSingledynarray;
        function  readSamplesFromTo(const filename: TFilename; const startMilliSecs, lengthMilliSecs: int64; const pb: TProgressBar = nil): TSmallIntDynArray;
        class function  readWAVHeader(const filename: TFilename; var info: TWAVInfo): boolean;
    public
    var
        buffer16: TSmallIntDynArray;
        weighted: TSingleDynArray;
        //dd: TDoubleDynArray;
        //eof: boolean;

        constructor Create;
        function  readWAVInfo(const filename: TFilename): boolean;
        function  readChunk(const buffer: TDoubleDynArray): boolean;
        procedure setupForReadChunk(const filename: TFileName; out buffer: TDoubleDynArray);
        procedure teardownAfterChunks();

        function  readAllWAVSamples(const filename: TFilename; const toFloat: boolean; const channel: integer; const pb: TProgressBar = nil): boolean;
        procedure clear();
        procedure buffer16ToFloat(const pb: TProgressBar);
        procedure clearBuffer16();

        property  hydrophoneID: String read hydrophoneIdAsString;
        property  fs: integer read finfo.fs;
        property  bits: integer read finfo.bits;
        property  channels: integer read finfo.channels;
        property  nSamples: int64 read finfo.nSamples;
        property  startTime: TDate read fInfo.startTime;
        class function  copyWAV(const inFilename, outFilename: TFilename; const startMilliSecs, lengthMilliSecs: integer): boolean;
        class procedure selectedSamples(const inFilename: TFilename; const startMilliSecs, lengthMilliSecs: integer; out fs: double; out result: TSmallintDynArray);
        class function  writeWAV(const FileName: string; const info: TWAVInfo; const WAVSamples: TSmallIntDynArray): boolean;
    end;

var
    wr: TWAVReader;

implementation

procedure TWAVInfo.clear;
begin
    self.fs := -1;
    self.bits := -1;
    self.channels := -1;
    self.nSamples := 0;
end;

constructor TWAVReader.create;
begin
    inherited;

    finfo.clear;
end;

function TWavReader.hydrophoneIDAsString : String;
begin
    result := string(self.fInfo.hydrophoneID);
end;

function TWAVReader.readWAV(const filename: TFilename; const channel, startSample, samplesToRead: int64; const pb: TProgressBar = nil): TSmallintdynarray;
var
    AFile: ISmart<TFileStream>;
    chunk: array of SmallInt;// array[0..chunkSize - 1] of SmallInt;
    offset: integer;
    i,j: integer;
    didRead: integer;
begin
    if assigned(pb) then
    begin
        pb.Visible := true;
        pb.max := samplesToRead;
        pb.Position := 0;
        Application.ProcessMessages;
    end;
    AFile := TSmart<TFileStream>.create(TFileStream.Create(filename, fmOpenRead));

    //header
    afile.Position := WAVHeaderBytes + (startSample * 2);
    offset := 0;
    setlength(result,0);
    setlength(result,samplesToRead);
    setlength(chunk, ChunkSize * channels);
    for i := 0 to (samplesToRead div chunkSize) do
    begin
        didRead := afile.read(chunk[0], chunkSize * 2 * channels) div (2 * channels);
        for j := 0 to didread - 1 do
            if offset + j < samplesToRead then
                result[offset + j] := chunk[(j * channels) + channel];

        inc(offset,chunksize);
        if assigned(pb) and (i mod 50000 = 0) then
        begin
            pb.Position := offset;
            Application.ProcessMessages;
        end;
    end;
end;

function  TWAVReader.readWAVInfo(const filename: TFilename): boolean;
begin
    result := FileExists(filename);
    if result then
        result := TWAVReader.readWAVHeader(filename,self.finfo);
end;

function  TWAVReader.readChunk(const buffer: TDoubleDynArray): boolean;
var
    i,didRead: integer;
begin
    didRead := fileStream.read(intBuffer16[0], chunkSize * 2);
    for I := 0 to ChunkSize - 1 do
        if i < didRead then
            buffer[i] := intBuffer16[i]
        else
            buffer[i] := 0;
    result := didRead = ChunkSize * 2;
end;

function  TWAVReader.readAllWAVSamples(const filename: TFilename; const toFloat: boolean; const channel: integer; const pb: TProgressBar = nil): boolean;
begin
    result := FileExists(filename);
    if result then
    begin
        result := TWAVReader.readWAVHeader(filename,self.finfo);
        if channel >= self.channels then
            exit(false);

        if toFloat then
            self.weighted := self.readWAVToFloat(FileName, channel, 0, self.fInfo.nSamples, pb)
        else
            self.buffer16 := self.readWav(FileName, channel, 0, self.fInfo.nSamples, pb);
    end;
end;

function TWAVReader.readSamplesFromTo(const filename: TFilename; const startMilliSecs, lengthMilliSecs: int64; const pb: TProgressBar): TSmallIntDynArray;
begin
    if not FileExists(filename) then exit();
    if not TWAVReader.readWAVHeader(filename,finfo) then exit;
    result := readWAV(FileName, 0, floor(startMilliSecs / int64(1000) * int64(fInfo.fs)), floor(lengthmilliSecs / int64(1000) * int64(fInfo.fs)), pb);
end;

procedure TWAVReader.clear();
begin
    setlength(self.buffer16,0);
    setlength(self.weighted,0);
    self.fInfo.clear;
end;

class function  TWAVReader.readWAVHeader(const filename: TFilename; var info: TWAVInfo): boolean;
var
    SourceFile: file;
    hdrByte : array[0..WAVHeaderBytes - 1] of byte;
begin
    AssignFile(SourceFile, FileName);
    try
        ReSet(SourceFile, 1);
        BlockRead(SourceFile, hdrByte[0], WAVHeaderBytes);
    finally
        CloseFile(SourceFile);
    end;

    info.channels := hdrByte[channelByte];
    info.fs := plongword(@hdrByte[fsByte])^;
    info.bits := hdrByte[bitsByte];
    info.nSamples := int64(plongword(@hdrByte[nSamplesByte])^) div int64((info.channels * (info.bits div 8)));

    result := true;
end;

procedure TWAVReader.setupForReadChunk(const filename: TFileName; out buffer: TDoubleDynArray);
begin
    fileStream := TFileStream.Create(filename, fmOpenRead);
    //skip header
    fileStream.Position := WAVHeaderBytes;
    setlength(buffer,ChunkSize);
end;

procedure TWAVReader.teardownAfterChunks();
begin
    fileStream.free;
end;

function TWAVReader.readWAVToFloat(const filename: TFilename; const channel, startSample, samplesToRead: int64; const pb: TProgressBar): TSingledynarray;
var
    AFile: ISmart<TFileStream>;
    chunk: array of SmallInt;// array[0..chunkSize - 1] of SmallInt;
    offset: integer;
    i,j: integer;
    didRead: integer;
begin
    if assigned(pb) then
    begin
        pb.Visible := true;
        pb.max := samplesToRead;
        pb.Position := 0;
        Application.ProcessMessages;
    end;
    AFile := TSmart<TFileStream>.create(TFileStream.Create(filename, fmOpenRead));

    //header
    afile.Position := WAVHeaderBytes + (startSample * 2);
    offset := 0;
    setlength(result,0);
    setlength(result,samplesToRead);
    setlength(chunk, ChunkSize * channels);
    for i := 0 to (samplesToRead div chunkSize) do
    begin
        didRead := afile.read(chunk[0], chunkSize * 2 * channels) div (2 * channels);
        for j := 0 to didread - 1 do
            if offset + j < samplesToRead then
                result[offset + j] := chunk[(j * channels) + channel];

        inc(offset,chunksize);
        if assigned(pb) and (i mod 50000 = 0) then
        begin
            pb.Position := offset;
            Application.ProcessMessages;
        end;
    end;
end;

class function TWAVReader.writeWAV(const FileName: string; const info: TWAVInfo; const WAVSamples: TSmallIntDynArray): boolean;
var
    SourceFile: file;
    header: array of byte;
begin
    header := [$52, $49, $46, $46, $24, $00, $00, $00,
                                  $57, $41, $56, $45, $66, $6D, $74, $20,
                                  $10, $00, $00, $00, $01, $00, $01, $00,
                                  $44, $AC, $00, $00, $88, $58, $01, $00,
                                  $02, $00, $10, $00, $64, $61, $74, $61,
                                  $00, $00, $00, $00];

    //set info
    header[channelByte] := byte(info.channels);
    PLongWord(@header[fsByte])^ := longword(info.fs);
    header[bitsByte] := byte(info.bits);

    //set correct file length in wav file header
    PLongWord(@header[nFileLengthByte])^ := longword(length(WAVsamples)*2 + 36);
    PLongWord(@header[nSamplesByte])^ := longword(length(WAVsamples)*2);

    AssignFile(SourceFile, FileName);
    FileMode := 1; //write
    ReWrite(SourceFile, 1);
    BlockWrite(SourceFile, header[0], length(header));
    BlockWrite(SourceFile, WAVSamples[0], length(WAVSamples) * 2);

    CloseFile(SourceFile);
    result := true;
end;

procedure TWAVReader.buffer16ToFloat(const pb: TProgressBar);
begin
    weighted := self.buffer16.toSingle(pb);
end;

procedure TWAVReader.clearBuffer16();
begin
    setlength(buffer16,0);
end;

class function TWAVReader.copyWAV(const inFilename, outFilename: TFilename; const startMilliSecs, lengthMilliSecs: integer): boolean;
var
    samples: TSmallIntDynArray;
    wr: ISmart<TWAVReader>;
begin
    wr := TSmart<TWAVReader>.create(TWAVReader.create);
    samples := wr.readSamplesFromTo(inFilename,startMilliSecs, lengthMilliSecs);
    result := writeWAV(outFilename,wr.fInfo,samples);
end;

class procedure TWAVReader.selectedSamples(const inFilename: TFilename; const startMilliSecs, lengthMilliSecs: integer; out fs: double; out result: TSmallintDynArray);
var
    wr: ISmart<TWAVReader>;
begin
    wr := TSmart<TWAVReader>.create(TWAVReader.create);
    result := wr.readSamplesFromTo(inFilename,startMilliSecs, lengthMilliSecs);
    fs := wr.fInfo.fs;
end;

initialization
    wr := TWAVReader.create;

finalization
    wr.free;

end.
