unit StackTrace;

interface

uses
    SysUtils, Classes, JclDebug, forms, stdctrls;

type
    TExceptionHandler = class(TObject)
    public
        memo: TMemo;
        procedure onExcept(Sender: TObject; E: Exception);
    end;

var
    exceptionHandler: TExceptionHandler;

implementation

procedure TExceptionHandler.onExcept(Sender: TObject; E: Exception);
var
    e0: Exception;
begin
    e0 := Exception(AcquireExceptionObject);
    try
        raise e0 at ExceptAddr; //for some reason, if we reraise then the stack trace is available
    except on e1: exception do
        if assigned(memo) then
            memo.text := memo.text + #13#10
                //+ inttostr(length(e4.StackTrace)) + #13#10
                + e1.message + #13#10#13#10
                + e1.StackTrace;
    end;
end;

function GetExceptionStackInfoProc(P: PExceptionRecord): Pointer;
var
    LLines: TStringList;
    LText: String;
    LResult: PChar;
begin
    LLines := TStringList.Create;
    try
        JclLastExceptStackListToStrings(LLines, True, True, True, True);
        LText := LLines.Text;
        LResult := StrAlloc(Length(LText));
        StrCopy(LResult, PChar(LText));
        Result := LResult;
    finally
        LLines.Free;
    end;
end;

function GetStackInfoStringProc(Info: Pointer): string;
begin
    Result := string(PChar(Info));
end;

procedure CleanUpStackInfoProc(Info: Pointer);
begin
    StrDispose(PChar(Info));
end;

initialization

// Start the Jcl exception tracking and register our Exception
// stack trace provider.
if JclStartExceptionTracking then
begin
    Exception.GetExceptionStackInfoProc := GetExceptionStackInfoProc;
    Exception.GetStackInfoStringProc := GetStackInfoStringProc;
    Exception.CleanUpStackInfoProc := CleanUpStackInfoProc;
end;

exceptionHandler := TExceptionHandler.create();
//Application.OnException := exceptionHandler.onExcept;


finalization

// Stop Jcl exception tracking and unregister our provider.
if JclExceptionTrackingActive then
begin
    Exception.GetExceptionStackInfoProc := nil;
    Exception.GetStackInfoStringProc := nil;
    Exception.CleanUpStackInfoProc := nil;
    JclStopExceptionTracking;
end;

exceptionHandler.free;

end.
