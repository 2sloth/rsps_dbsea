unit baseTypes;

{$B-}

interface

uses
    system.sysutils,system.math,system.classes,system.types,system.UITypes,
    vcl.stdCtrls,
    vcl.comctrls,
    vcl.forms,
    vcl.clipbrd,
    generics.collections,
    uTExtendedX87,
    helperFunctions,
    dbSeaConstants,
    nullable,
    smartPointer;

type

    TRunningMode = (rmBlocked,           //immediate quit
                    rmDemo,        //load bathymetry, save and export disabled
                    rmUnlicensed,   //only quick solvers allowed
                    rmTrial,        //save and export disabled
                    rmNormalKey, //full version
                    rmLite); //lite version

    TObjectProcedure = procedure of object;
    TProcedureWithDouble = reference to procedure(const d: double);
    TProcedureWithInt = reference to procedure(const i: integer);
    TProcedureWithBoolBool = procedure(const b1,b2:boolean) of object;
    TProcedureWithString = procedure(const s: string) of object;
    TProcedureWithStringIntReturn = function(const s: string): integer of object;
    TResourceUnbundleFunction = function(const s1,s2,s3:string; const b: boolean = false): boolean of object;

    EJSONParsing = class(Exception);

    TSingleMatrix = array of TSingleDynArray;
    TSingleField = array of TSingleMatrix;

    TMatrix = array of TDoubleDynArray;

    TDoubleField = array of TMatrix;
    TWrappedDouble = class(TObject) public
        d: double;
        constructor create(const d: double = 0);
    end;
    TDoubleList = class(TObjectList<TWrappedDouble>)
        function totalFractionDone: double;
        procedure init(const n: integer);
    end;
    TDoubleListList = class(TObjectList<TDoubleList>)
        function totalFractionDone: double;
        procedure init(const m,n: integer);
    end;
    TWrappedDoubleArray = array of TWrappedDouble;
    TWrappedDoubleArrayHelper = record helper for TWrappedDoubleArray
        function  totalFractionDone(): double;
        procedure init(const n: integer);
        procedure free;
    end;
    TWrappedDoubleArrayArray = array of TWrappedDoubleArray;
    TWrappedDoubleArrayArrayHelper = record helper for TWrappedDoubleArrayArray
        function totalFractionDone(): double;
        procedure init(const m,n: integer);
        procedure free;
    end;

    TWrappedBool = class(TObject) public
        b: boolean;
    end;
    TBoolList = TObjectList<TWrappedBool>;

    TWrappedInt = class(TObject) public
        i: integer;

        constructor create(const i: integer = 0);
    end;
    TIntList = TObjectList<TWrappedInt>;

    TE87Vect = array of TExtendedX87;

    TSmallIntMatrix = array of TSmallIntDynArray;

    TLongIntDynArray = array of LongInt;

    TIntegerMatrix = array of TIntegerDynArray;
    TIntegerMatrix3 = array of TIntegerMatrix;

    TStringArray = array of String; // used because RTTI was causing problems when we tried to save file
    TStringMatrix = array of TStringArray; // used because RTTI was causing problems when we tried to save file
    TBoolMatrix = array of TBooleanDynArray;
    TColorArray = array of TColor;

    {$SCOPEDENUMS ON}
    //TMovingGraphicsObject = (Source,SourceMovingPos,Probe,ReceiverArea,CrossSecEndpoint); //which type of object has been selected onscreen
    TLastAction = (kNull,kMoveSource,kMoveSourceMovingPos,kMoveProbe,kMoveCSEP); //used for undo
    TDataSource = (dsMDA,dsUser);
    {$SCOPEDENUMS OFF}
    TBandwidth = (bwOctave, bwThirdOctave);
    TCalculationMethod = (
        cm20log,
        cm10log,
        cmRamGEO,
        cmBellhop,
        cmKraken,
        cmdBSeaPE,
        cmdBSeaRay,
        cmdBSeaModes,
        cmUserDefined,
        cmRAMCloud,
        cmdBSeaPE3D,
        cmdBSeaRay3d); //which solver to use
    TVisibilityEnum = (veAxes,veWater,veBathymetry,veSources,veProbes,veRAs,veResults,veContours,veEndpoints,veScaleBar,veGrid,veRays);
    TLevelsDisplay = (ldMax,ldSingleLayer,ldAllLayers); //which type of levels to display for results
    TRaySolverIterationMethod = (kConstSSP,kRK2,kRK4);
    TRectControl = (kLL, kLR, kUL, kUR, kCenter);

    TPointArray = array of TPoint;

    TFPoint = record
    public
        x,y: double;

        procedure setXY(const x,y: double);
    end;
    TFPointArray = array of TFPoint;
    TFPointArrayHelper = record helper for TFPointArray
        function serialise: string;
        procedure deserialise(const s: string);
    end;

    TEastNorth = record
        easting,northing: double;

        property latitude: double read northing write northing;
        property longitude: double read easting write easting;
    end;

    TFPoint3 = record
    public
        x,y,z: double;

        procedure setPos(const aX, aY, aZ: double); overload;
        procedure setPos(const p: TFPoint3); overload;
        procedure setZero;
        function  mag: double;
        procedure normalise;
        procedure reverse;
        function  anyNaN: boolean;
        procedure divBy(const d: double);
        procedure multBy(const d: double);
        procedure add(const p: TFPoint3);
        procedure sub(const p: TFPoint3);
        procedure limit(const d: double);
        function  dist(const p: TFPoint3): double;
        function  subPoint(const p: TFPoint3): TFPoint3;
    end;

    TExclusionZonePoint = record
        p: TFPoint;
        isLand: boolean;
    end;
    TExclusionZonePointArray = array of TExclusionZonePoint;

    TDepthVal = record
        depth, val:double;
    end;
    TDepthValArray = array of TDepthVal;
    TDepthValArrayHelper = record helper for TDepthValArray
        function  findDepth(const depth: double): integer;
        function  average(): double;
        function  maxDepth: double;
    end;

    TRangeVal = record
        range, val: double;
    end;
    TRangeValArray = array of TRangeVal;
    //NB compiler is being useless and giving errors when TRangeDepthArray is TArray<TRangeDepth>, so keep it array of...
    TRangeValArrayHelper = record helper for TRangeValArray
        function  findRange(const range: double): integer;
        function  min: double;
        function  max: double;
    end;

    TDepthValArrayAtRange = record
        range: double;
        depthVal: TDepthValArray;

        class function create(r: double; dv: TDepthValArray): TDepthValArrayAtRange; static;
    end;
    TDepthValArrayAtRangeArray = array of TDepthValArrayAtRange;
    TDepthValArrayAtRangeArrayHelper = record helper for TDepthValArrayAtRangeArray
        function  depthValForRange(const range: double; out index: integer): TDepthValArray;
    end;

    TExtents = record
        xmin, xmax, ymin, ymax: double;
        utmz: string;

        function width: double;
        function height: double;

        property easting: double read xmin write xmin;
        property northing: double read ymin write ymin;
    end;

    TLabelList = TObjectList<TLabel>;

    TWrappedString = class(TObject) {used so we can have TStringlist with objects of TWrappedString, which effectively = TDictionary<string,string> but is ordered}
        str: string;
    end;

    TMouseInfo = record
        button: Nullable<TMouseButton>;
        x,y: integer;
        constructor create(const ax, ay: integer);
        constructor createWithButton(const ax, ay: integer; const aButton: TMouseButton);
        function  point: TPoint;
    end;

    TMyObjectList<T: class> = class(TObjectList<T>)
    private
    var
        fSelected: T;

        function  getSelected(): T;
        procedure setSelected(val: T);
    public
        constructor Create(ownsObjects: boolean = true);

        function  enumerate(): TArray<TPair<integer,T>>;

        property selected: T read getSelected write setSelected;
    end;

    TBandwithHelper = record helper for TBandwidth
        function oct: boolean;
    end;

    TCalculationMethodHelper = record Helper for TCalculationMethod
        function toStr: string;
        function solvesAllFreqsAtOnce: boolean;
        function availableInUnlicensed: boolean;
        function availableInLite: boolean;
        function MDAOnly: boolean;
        function isSymmetric: boolean;
    end;

    TSingleDynArrayHelper = record helper for TSingleDynArray
        function  serialise(const forceDot: boolean = true): string;
        procedure deserialise(const s: string);
        function  toDoubleArray: TDoubleDynArray;
        procedure setAllVal(const d: single);
        function  copy: TSingleDynArray;
        function  length: integer;
        function  max():double;
        function  min():double;
    end;

    TDoubleDynArrayHelper = record helper for TDoubleDynArray
    private
        //function getJ(const i,j,n: integer): integer;
        //function getDivisor(const i,n: integer): integer;
    public
        function  serialise(const forceDot: boolean = true): string;
        procedure deserialise(const s: string);
        function  toSingleArray: TSingleDynArray;
        procedure setAllVal(const d: double);
        function  copy: TDoubleDynArray;
        procedure copyInPlace(var result: TDoubleDynArray);
        function  length: integer;
        procedure sort();
        function  plusConst(const d: double): TDoubleDynArray;
        procedure plusConstInPlace(const d: double; const result: TDoubleDynArray);
        function  unique():TDoubleDynArray;
        function  max(): double;
        function  min(): double;
        function  find(const x: double): integer;
        function  findExact(const x: double): integer;
        function  isAllNaN(): boolean;
        function  reverse: TDoubleDynArray;
        //function  get1stDeriv(const i,n: integer; const dz: double): double;
        function  getVal(const i,n: integer): double;
        function  truncateAtVal(const d: double): TDoubleDynArray;
        function  firstNTerms(n: integer): TDoubleDynArray;
        procedure normalise();
        procedure multiplyBy(const d: double);
    end;

    TBooleanDynArrayHelper = record helper for TBooleanDynArray
        function  serialise: string;
        procedure deserialise(const s: string);
        function  toInt: integer;
        function  countTrue: integer;
        function  allTrue: boolean;
        function  allFalse: boolean;
        procedure setAll(const b: boolean);
    end;

    TIntegerDynArrayHelper = record helper for TIntegerDynArray
        function  serialise: string;
        procedure deserialise(const s: string);
        function  max():integer;
        function  findExact(const x:integer):integer;
        function  reverse: TIntegerDynArray;
    end;

    TStringDynArrayHelper = record helper for TStringDynArray
        function  serialise: string;
    end;

    TStringArrayHelper = record helper for TStringArray
        function  serialise: string;
        procedure deserialise(const s: string);
        function  floatVal(const i: integer): nullable<double>;
    end;

    TSingleMatrixHelper = record helper for TSingleMatrix
        function  serialise(const forceDot: boolean = true): string;
        procedure deserialise(const s: string);
        procedure setAllVal(const d: single);
        function  copy: TSingleMatrix;
        function  swapAxes: TSingleMatrix;
        function  min: single;
        function  max: single;
        function  add(const d: double): TSingleMatrix;
        function  downsample(const iDecimate: integer):TSingleMatrix;
        procedure fromDelimitedFile(const Filename: string; const delim: char; const lblSolving: TLabel; const pbSolveProgress: TProgressbar);
        procedure fromDelimitedString(const s: string; const delim: char; const bNaNs: boolean);
        function  toSingleDynArray: TSingleDynArray;
        procedure fromSingleDynArray(const s: TSingleDynArray);
        function  allValuesEqual: boolean;
    end;

    TMatrixHelper = record helper for TMatrix
        function  serialise(const forceDot: boolean = true): string;
        procedure deserialise(const s: string);
        procedure setAllVal(const d: double);
        function  copy: TMatrix;
        procedure sort(const iCol: integer);
        function  uniqueElementsInColumn(const iCol: integer):TMatrix;
        procedure removeRow(const iRow: integer);
        function  column(const iCol: integer): TDoubleDynArray;
        function  row(const aRow: integer): TDoubleDynArray;
        function  min: double;
        function  max: double;
        function  findWithNoInternalAssigns(const x: double; const iCol: integer): integer;
        procedure fromDelimitedString(const s: string; const delim: char; const bNaNs: boolean);
        procedure fromDelimitedStringKeepSize(const s: string; const delim: char; const bNaNs, rotate: boolean);
        function  rotate90(): TMatrix;
        procedure fromClipboard();
    end;

    TIntergerMatrixHelper = record helper for TIntegerMatrix
        function  serialise: string;
        procedure deserialise(const s: string);
        function  copy: TIntegerMatrix;
    end;

    TStringMatrixHelper = record helper for TStringMatrix
        function  serialise: string;
        procedure deserialise(const s: string);
        function  copy: TStringMatrix;
        procedure fromStringList(const sl1: TStringlist; const delim: char);
        procedure fromDelimitedString(const s: string; delim: char);
    end;

    TBoolMatrixHelper = record helper for TBoolMatrix
        procedure setAllVal(const b: boolean);
    end;

    TSmallIntDynArrayHelper = record helper for TSmallIntDynArray
        function toSingle(const pb: TProgressBar): TSingleDynArray;
        function toDouble(): TDoubleDynArray;
    end;

implementation

{$B-}

procedure TFPoint3.setPos(const aX, aY, aZ: double);
begin
    x := aX;
    y := aY;
    z := aZ;
end;

procedure TFPoint3.setPos(const p: TFPoint3);
begin
    self.x := p.x;
    self.y := p.y;
    self.z := p.z;
end;

procedure TFPoint3.setZero;
begin
    x := 0.;
    y := 0.;
    z := 0.;
end;

function  TFPoint3.mag: double;
begin
    result := sqrt(sqr(x) + sqr(y) + sqr(z));
end;

procedure TFPoint3.normalise;
var
    tot: double;
begin
    tot := mag;
    if mag = 0 then exit;
    x := x / tot;
    y := y / tot;
    z := z / tot;
end;

procedure TFPoint3.reverse;
begin
    x := -x;
    y := -y;
    z := -z;
end;

function  TFPoint3.anyNaN: boolean;
begin
    result := isnan(x) or isnan(y) or isnan(z);
end;

procedure TFPoint3.divBy(const d: double);
begin
    if d = 0 then exit;
    x := x / d;
    y := y / d;
    z := z / d;
end;

procedure TFPoint3.multBy(const d: double);
begin
    x := x * d;
    y := y * d;
    z := z * d;
end;

procedure TFPoint3.add(const p: TFPoint3);
begin
    x := x + p.x;
    y := y + p.y;
    z := z + p.z;
end;

procedure TFPoint3.sub(const p: TFPoint3);
begin
    x := x - p.x;
    y := y - p.y;
    z := z - p.z;
end;

procedure TFPoint3.limit(const d: double);
var
    q: double;
begin
    q := mag;
    if q = 0 then exit;
    if q <= d then exit;

    q := d / q;
    x := x * q;
    y := y * q;
    z := z * q;
end;

function TFpoint3.subPoint(const p: TFPoint3): TFPoint3;
begin
    result.x := self.x - p.x;
    result.y := self.y - p.y;
    result.z := self.z - p.z;
end;

function TFPoint3.dist(const p: TFPoint3): double;
begin
    result := self.subPoint(p).mag;
end;

{ TCalculationMethodHelper }

function TCalculationMethodHelper.availableInUnlicensed: boolean;
begin
    result := self in [cm20log,cm10Log,cmUserDefined];
end;

function TCalculationMethodHelper.isSymmetric: boolean;
begin
    result := self in [cm20log,cm10Log,cmUserDefined];
end;

function TCalculationMethodHelper.availableInLite: boolean;
begin
    result := self in [cm20log,cm10Log,cmUserDefined];
end;

function TCalculationMethodHelper.MDAOnly: boolean;
begin
    result := self in [cmRamGEO,cmBellhop,cmKraken];
end;

function TCalculationMethodHelper.solvesAllFreqsAtOnce: boolean;
begin
    result := self in [cmdBSeaRay];
end;

function TCalculationMethodHelper.toStr: string;
begin
    case self of
        cm20log    : result := '20 log';
        cm10log    : result := '10 log';
        cmRamGEO   : result := 'ramGEO (parabolic equation)';
        cmBellhop  : result := 'Bellhop (ray tracing)';
        cmKraken   : result := 'Kraken (normal modes)';
        cmdBSeaPE  : result := 'dBSeaPE (parabolic equation)';
        cmdBSeaRay : result := 'dBSeaRay (ray tracing)';
        cmdBSeaModes: result := 'dBSeaModes (normal modes)';
        cmUserDefined   : result := 'User defined';
    else
        result := '';
    end;
end;

{ TSingleDynArrayHelper }

function TSingleDynArrayHelper.copy: TSingleDynArray;
var
    i: integer;
begin
    setlength(result, self.length);
    for i := 0 to self.length - 1 do
        result[i] := self[i];
end;

procedure TSingleDynArrayHelper.deserialise(const s: string);
var
    j: integer;
    sA: TStringArray;
begin
    sA.deserialise(s);
    setlength(self,system.length(sA));
    for j := 0 to system.length(sA) - 1 do
        self[j] := tryTofl(sA[j]);
end;

function TSingleDynArrayHelper.length: integer;
begin
    result := system.Length(Self);
end;

function TSingleDynArrayHelper.max: double;
var
    i: integer;
begin
    result := NaN;
    for i := 0 to high(self) do
        greater(result,self[i]);
end;

function TSingleDynArrayHelper.min: double;
var
    i: integer;
begin
    result := NaN;
    for i := 0 to high(self) do
        lesser(result,self[i]);
end;

function TSingleDynArrayHelper.serialise(const forceDot: boolean): string;
var
    s: string;
    hj,j: integer;
    sA: TStringArray;
begin
    {all the TSingleArrays are currently used for storing levels (and no doubles are). which we can round1 to save a lot of
    file space.}
    s := '';
    hj := high(self);
    setlength(sA,hj + 1);
    for j := 0 to hj do
    begin
        if isNaN(self[j]) or isinfinite(self[j]) then
            sA[j] := nanString
        else
        begin
            if forceDot then
                sA[j] := forceDotSeparator( tostr(round1(self[j])) )
            else
                sA[j] := tostr(round1(self[j]));
        end;
    end;
    result := sA.serialise; //TODO this can cause out of memory
end;


procedure TSingleDynArrayHelper.setAllVal(const d: single);
var
    i: integer;
begin
    for i := 0 to self.length - 1 do
        self[i] := d;
end;

function TSingleDynArrayHelper.toDoubleArray: TDoubleDynArray;
var
    i: integer;
begin
    setlength(result,self.length);
    for i := 0 to self.length - 1 do
        result[i] := self[i];
end;

{ TDoubleDynArrayHelper }

function TDoubleDynArrayHelper.copy: TDoubleDynArray;
var
    i: integer;
begin
    setlength(result, self.length);
    for i := 0 to self.length - 1 do
        result[i] := self[i];
end;

procedure TDoubleDynArrayHelper.copyInPlace(var result: TDoubleDynArray);
var
    i: integer;
begin
    if system.length(result) <> self.length then
        setlength(result, self.length);

    for i := 0 to self.length - 1 do
        result[i] := self[i];
end;

procedure TDoubleDynArrayHelper.deserialise(const s: string);
var
    j: integer;
    sA: TStringArray;
begin
    sA.deserialise(s);
    setlength(self,system.length(sA));
    for j := 0 to system.length(sA) - 1 do
        self[j] := tryTofl(sA[j]);
end;

function TDoubleDynArrayHelper.length: integer;
begin
    result := system.Length(Self);
end;

function TDoubleDynArrayHelper.serialise(const forceDot: boolean): string;
var
    s: string;
    hj,j: integer;
    sA: TStringArray;
begin
    s := '';
    hj := high(self);
    setlength(sA,hj + 1);
    for j := 0 to hj do
    begin
        if isNaN(self[j]) or isinfinite(self[j]) then
            sA[j] := nanString
        else
        begin
            if forceDot then
                sA[j] := forceDotSeparator( tostr(round1(self[j])) )
            else
                sA[j] := tostr(round1(self[j]));
        end;
    end;
    result := sA.serialise;
end;

procedure TDoubleDynArrayHelper.setAllVal(const d: double);
var
    i: integer;
begin
    for i := 0 to self.length - 1 do
        self[i] := d;
end;

procedure TDoubleDynArrayHelper.sort();
var
    hi, i: Integer;
    T: TMatrix;
begin
    hi := high(self);
    setlength(T,1,hi+1);
    for i := 0 to hi do
        T[0,i] := self[i];

    T.sort(0);
    for i := 0 to hi do
        self[i] := T[0,i];
end;

function    TDoubleDynArrayHelper.plusConst(const d: double): TDoubleDynArray;
var
    i: integer;
begin
    setlength(result, self.length);
    for i := 0 to self.length - 1 do
        result[i] := self[i] + d;
end;

procedure TDoubleDynArrayHelper.plusConstInPlace(const d: double; const result: TDoubleDynArray);
var
    i: integer;
begin
    for i := 0 to self.length - 1 do
        result[i] := self[i] + d;
end;

function   TDoubleDynArrayHelper.unique():TDoubleDynArray;
{get a vector with unique entries. NB vector must be sorted!}
var
    i: Integer;
    T: TMatrix;
begin
    //SORT vector BEFORE RUNNING THIS

    {make use of the TMatrix version}
    setlength(T,1,self.length);
    for i := 0 to self.length - 1 do
        T[0,i] := self[i];

    T := T.uniqueElementsInColumn(0);
    setlength(result,T[0].length);
    for i := 0 to T[0].length - 1 do
        result[i] := T[0,i];
end;

function    TDoubleDynArrayHelper.max():double;
var
    i: integer;
begin
    result := NaN;
    for i := 0 to high(self) do
        greater(result,self[i]);
end;


function TDoubleDynArrayHelper.min: double;
var
    i: integer;
begin
    result := NaN;
    for i := 0 to high(self) do
        lesser(result,self[i]);
end;

procedure TDoubleDynArrayHelper.multiplyBy(const d: double);
var
    i: integer;
begin
    for i := 0 to system.length(self) - 1 do
        self[i] := self[i] * d;
end;

procedure TDoubleDynArrayHelper.normalise;
var
    mx: double;
    i: integer;
begin
    mx := system.math.max(abs(self.max),abs(self.min));
    for i := 0 to system.length(self) - 1 do
        self[i] := self[i] / mx;
end;

function    TDoubleDynArrayHelper.find(const x:double):integer;
{find value x within an array - finds the value equal to or less than x (ie trunc not round)}
var
    i: integer;
begin
    {can be used for an exact match, or also the next continuous. returns -1 if all x < self}
    result := -1;

    if isNaN(x) then Exit;

    for i := 0 to self.length - 1 do
        if x >= self[i] then    //also used to find exact values
            result := i;
end;

function    TDoubleDynArrayHelper.findExact(const x:double):integer;
{find value x within an array - only finds exact matches}
var
    i: integer;
begin
    {ONLY for an exact match - returns -1 if no exact match found}
    result := -1;

    if isNaN(x) then Exit;

    for i := 0 to self.length - 1 do
        if x = self[i] then
            result := i;
end;

function TDoubleDynArrayHelper.firstNTerms(n: integer): TDoubleDynArray;
var
    i: integer;
begin
    n := system.math.min(self.length,n);
    setlength(result, n);
    for I := 0 to n - 1 do
        result[i] := self[i];
end;

//function TDoubleDynArrayHelper.get1stDeriv(const i,n: integer; const dz: double): double;
//begin
//    result := (self[i + getJ(i,1,n)] - self[i + getJ(i,-1,n)]) / self.getDivisor(i,n) / dz;
//end;

//function TDoubleDynArrayHelper.getDivisor(const i,n: integer): integer;
//begin
//    result := ifthen((i = 0) or (i = n - 1),1,2);
//end;
//
//function TDoubleDynArrayHelper.getJ(const i, j, n: integer): integer;
//begin
//    if i = 0 then
//        result := ifthen(j = -1,0,1)
//    else if i = n - 1 then
//        result := ifthen(j = -1,-1,0)
//    else
//        result := j;
//end;

function TDoubleDynArrayHelper.getVal(const i, n: integer): double;
begin
    if (i < 0) or (i >= n) then
        result := 0
    else
        result := self[i];
end;

function    TDoubleDynArrayHelper.isAllNaN(): boolean;
var
    i: integer;
begin
    for i := 0 to self.length - 1 do
        if not isNaN(self[i]) then
            Exit(false);
    result := true;
end;

function TDoubleDynArrayHelper.toSingleArray: TSingleDynArray;
var
    i: integer;
begin
    setlength(result,self.length);
    for i := 0 to self.length - 1 do
        result[i] := self[i];
end;

function TDoubleDynArrayHelper.truncateAtVal(const d: double): TDoubleDynArray;
var
    nm, i: integer;
begin
    nm := 0;
    for i := 0 to self.length - 1 do
        if self[i] > d then
        begin
            nm := i;
            break;
        end;

    result := self.firstNTerms(nm + 1);
end;

function    TDoubleDynArrayHelper.reverse:TDoubleDynArray;
var
    i,hi: integer;
begin
    setlength(result,self.length);
    hi := high(self);
    for i := 0 to hi do
        result[i] := self[hi - i];
end;


{ TBooleanDynArrayHelper }

function TBooleanDynArrayHelper.allFalse: boolean;
var
    b: boolean;
begin
    result := true;
    for b in self  do
        if b then
            result := false;
end;

function TBooleanDynArrayHelper.allTrue: boolean;
var
    b: boolean;
begin
    result := true;
    for b in self  do
        if not b then
            result := false;
end;

function TBooleanDynArrayHelper.countTrue: integer;
var
    b: boolean;
begin
    result := 0;
    for b in self do
        if b then
            inc(result);
end;

procedure TBooleanDynArrayHelper.deserialise(const s: string);
var
    len,j: integer;
    sl: ISmart<TStringList>;
begin
    sl := TSmart<TStringList>.create(TStringList.create);
    sl.commatext := s;
    len := sl.count;
    setlength(self,len);
    for j := 0 to len - 1 do
        self[j] := tobool(sl[j]);
end;

function TBooleanDynArrayHelper.serialise: string;
var
    hj,j: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    hj := high(self);
    for j := 0 to hj do
    begin
        sb.append(tostr(self[j]));
        if j < hj then
            sb.append(',');  //field separator
    end;
    result := sb.ToString;
end;

procedure TBooleanDynArrayHelper.setAll(const b: boolean);
var
    i: integer;
begin
    for i := 0 to length(self) - 1 do
        self[i] := b;
end;

function TBooleanDynArrayHelper.toInt: integer;
{convert an array of bool to an integer.
used by marching squares algorithms. BIG ENDIAN!
use allTrue or allFalse to check if all true or false}
var
    hi, i, multiplier: integer;
begin
    result := 0;
    hi := high(self);
    multiplier := 1;
    for i := hi downto 0 do
    begin
        if boolean(self[i]) then
            result := result + multiplier;
        multiplier := multiplier*2;
    end;
end;

{ TIntegerDynArrayHelper }

procedure TIntegerDynArrayHelper.deserialise(const s: string);
var
    j: integer;
    sA: TStringArray;
begin
    sA.deserialise(s);
    setlength(self,length(sA));
    for j := 0 to length(sA) - 1 do
    begin
        if isInt(sA[j]) then
            self[j] := toint(sA[j])
        else
            self[j] := 0;
    end;
end;

function TIntegerDynArrayHelper.max: integer;
var
    i: integer;
begin
    if length(self) = 0 then exit(0);
    result := self[0];
    for i := 1 to high(self) do
        if self[i] > result then
            result := self[i];
end;

function    TIntegerDynArrayHelper.findExact(const x:integer):integer;
{find value x within an array - finds the value equal to or less than x (ie trunc not round)}
var
    i: integer;
begin
    result := -1;
    for i := 0 to high(self) do
        if x = self[i] then
            result := i;
end;

function    TIntegerDynArrayHelper.reverse:TIntegerDynArray;
var
    i,hi: integer;
begin
    setlength(result,system.length(self));
    hi := high(self);
    for i := 0 to hi do
        result[i] := self[hi - i];
end;

function TIntegerDynArrayHelper.serialise: string;
var
    hj,j: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    hj := high(self);
    for j := 0 to hj do
    begin
        sb.Append(tostr(self[j]));
        if j < hj then
            sb.append(',');  //field separator
    end;
    result := sb.ToString;
end;

{ TStringArrayHelper }

procedure TStringArrayHelper.deserialise(const s: string);
var
    j: integer;
    sl: ISmart<TStringList>;
    sTmp: string;
begin
    sl := TSmart<TStringList>.create(TStringList.create);

    setlength(self,0);
    {ExtractStrings is no good here, as it won't add empty lines to the list}

    {replace any spaces with #255 (not often used ascii code), as space gives problems. also remove EOL}
    sTmp := StringReplace(s,' ',AnsiString(#255),[rfReplaceAll, rfIgnoreCase]);
    sl.CommaText := StringReplace(sTmp,AnsiString(#13#10),'',[rfReplaceAll, rfIgnoreCase]);
    if sl.Count > 0 then
    begin
        setlength(self,sl.Count);
        for j := 0 to sl.Count - 1 do
            self[j] := StringReplace(sl[j],AnsiString(#255),' ',[rfReplaceAll, rfIgnoreCase]);
    end;
end;

function TStringArrayHelper.floatVal(const i: integer): nullable<double>;
begin
    if isFloat(self[i]) then
        result := tofl(self[i]);
end;

function TStringArrayHelper.serialise: string;
var
    hj,j: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    hj := high(self);
    for j := 0 to hj do
    begin
        sb.Append(stringreplace(stringreplace(self[j],',',' ',[rfReplaceAll]),';',' ',[rfReplaceAll])); //replace any delimeters within the fields with spaces
        if j < hj then
            sb.append(',');  //field separator
    end;
    result := sb.ToString
end;

{ TSingleMatrixHelper }

function TSingleMatrixHelper.copy: TSingleMatrix;
var
    i,j: integer;
    la1,la2: integer;
begin
    la1 := length(self);
    if la1 = 0 then
    begin
        setlength(result,0);
        Exit;
    end;

    la2 := Length(self[0]);
    setlength(result,la1,la2);
    for i := 0 to la1 - 1 do
        for j := 0 to la2 - 1 do
            result[i,j] := self[i,j];
end;

procedure TSingleMatrixHelper.deserialise(const s: string);
var
    i,j: integer;
    sM: TStringMatrix;
begin
    if s = '' then
    begin
        setlength(self,0);
        Exit;
    end;

    sM.deserialise(s);
    if length(sM) = 0 then
    begin
        setlength(self,0);
        Exit;
    end;

    setlength(self, 0, 0);
    setlength(self,length(sM),length(sM[0]));
    for i := 0 to length(sM) - 1 do
        for j := 0 to length(sM[0]) - 1 do
            self[i,j] := tryTofl(sM[i,j]);
end;

function TSingleMatrixHelper.serialise(const forceDot: boolean): string;
var
    s: string;
    li,lj,i,j: integer;
    sMat: TStringMatrix;
begin
    {all the TSingleMatrix can be round1 to save a lot of file space.}
    s := '';
    li := length(self);
    if li < 1 then Exit;

    lj := length(self[0]);
    {convert the matrix to a stringmatrix and then use the stringmatrix serialise function}
    setlength(sMat,li,lj);
    for i := 0 to li - 1 do
    begin
        for j := 0 to lj - 1 do
        begin
            if isNaN(self[i,j]) or isinfinite(self[i,j]) then
                sMat[i,j] := nanString
            else
            begin
                if forceDot then
                    sMat[i,j] := forceDotSeparator( tostr(round1(self[i,j])) )
                else
                    sMat[i,j] := tostr(round1(self[i,j]));
            end;
        end;
    end;
    result := sMat.serialise;
end;

procedure TSingleMatrixHelper.setAllVal(const d: single);
var
    i,j: integer;
begin
    for i := 0 to high(self) do
        for j := 0 to high(self[0]) do
            self[i,j] := d;
end;

function TSingleMatrixHelper.swapAxes: TSingleMatrix;
var
    i,j: integer;
begin
    if length(self) = 0 then
    begin
        setlength(result,0);
        exit;
    end;

    setlength(result,length(self[0]),length(self));
    for i := 0 to length(self) - 1 do
        for j := 0 to length(self[0]) - 1 do
            result[j,i] := self[i,j];
end;

function TSingleMatrixHelper.toSingleDynArray: TSingleDynArray;
var
    iRows, iCols, i,j: integer;
begin
    if (length(self) = 0) then exit;

    //the first element tells us how many rows the matrix has
    iRows := Length(self);
    iCols := Length(self[0]);
    setlength(result, iRows * iCols + 1);
    result[0] := iRows;
    for i := 0 to iRows - 1 do
        for j := 0 to iCols - 1 do
            result[1 + i * iCols + j] := self[i,j];
end;

function TSingleMatrixHelper.max: single;
var
    i,j: integer;
begin
    result := NaN;

    if length(self) = 0 then exit;

    for i := 0 to high(self) do
        for j := 0 to high(self[0]) do
            greater(result, self[i,j]);
end;

function  TSingleMatrixHelper.min:single;
var
    i,j: integer;
begin
    result := NaN;

    if length(self) = 0 then exit;

    for i := 0 to high(self) do
        for j := 0 to high(self[0]) do
            lesser(result, self[i,j]);
end;

function  TSingleMatrixHelper.downsample(const iDecimate: integer):TSingleMatrix;
{downsample a matrix in both directions, ie each entry in the returned matrix is
the average of a decimate*decimate square in the original}
var
    i,j,m,n: integer;
    cnt: TIntegerMatrix; //matrix to hold the counts for each matrix sum entry
begin
    setlength(result,safefloor(length(self)/iDecimate),safefloor(length(self[0])/iDecimate));
    setlength(cnt,length(result),length(result[0]));
    result.setAllVal(nan);
    for i := 0 to high(result) do
        for j := 0 to high(result[0]) do
            for m := 0 to iDecimate - 1 do
                for n := 0 to iDecimate - 1 do
                    if not isNaN(self[iDecimate*i + m,iDecimate*j + n]) then
                    begin
                        if isNaN(result[i,j]) then
                            result[i,j] := self[iDecimate*i+m,iDecimate*j+n]//*oneOverIDecimateSquared
                        else
                            result[i,j] := result[i,j] + self[iDecimate*i+m,iDecimate*j+n];//*oneOverIDecimateSquared;
                        cnt[i,j] := cnt[i,j] + 1;
                    end;

    {divide each entry by the counts, to give average}
    for i := 0 to high(result) do
        for j := 0 to high(result[0]) do
            if cnt[i,j] = 0 then
                result[i,j] := NaN
            else
                result[i,j] := result[i,j]/cnt[i,j];
end;

procedure  TSingleMatrixHelper.fromDelimitedFile(const Filename: string; const delim: char; const lblSolving: TLabel; const pbSolveProgress: TProgressbar);
const
    iLinesToIncrease = 20;
var
    sl: ISmart<TStringList>;
    s: string;
    i, j: integer;
    jmax,masterJMax: integer;
    sr: ISmart<TStreamReader>;
begin
    {read file into a stringlist and then use the string version of the function. previously this was
    being read into a string, then the string read into the stringlist. but sometimes we are reading in string
    that are hundreds of MB long, so here we can save 1 copy by going straight into the stringlist}

    sr := TSmart<TStreamReader>.create(TStreamreader.Create(Filename));
    sl := TSmart<TStringList>.create(TStringList.Create);

    {get bathymetry from a CSV file. round data to 0.1m }

    if lblSolving <> nil then
    begin
        lblSolving.Caption := 'Reading grid data...';
        lblSolving.Visible := true;
    end;

    if pbSolveProgress <> nil then
    begin
        pbSolveProgress.Max := 50;
        pbSolveProgress.Step := 1;
        pbSolveProgress.Position := 0;
        pbSolveProgress.Visible := true;
    end;

    sl.Delimiter := delim;
    sl.StrictDelimiter := true;
    s := trim(sr.ReadLine);
    sl.DelimitedText := s;
    jmax := sl.Count;
    masterJMax := jmax;
    setlength(self, 0,0);
    i := 0;
    while s <> '' do
    begin
        if length(self) = i then
        begin
            //need to increase size of matrix
            setlength(self,i + iLinesToIncrease,masterJMax);
            if pbSolveProgress <> nil then
            begin
                pbSolveProgress.StepIt;
                application.ProcessMessages;
            end;
        end;

        sl.DelimitedText := trim(s);
        {we check the jmax for this row against the masterJMax from the first row, so that
        we don't error on rows that are longer than they should be}
        for j := 0 to masterJMax - 1 do
            if (j >= sl.count) or (not isFloat(sl[j])) then
                self[i,j] := NaN
            else
                self[i,j] := tofl(sl[j]);

        s := sr.ReadLine;
        inc(i);
    end;
    setlength(self, i, masterJMax);
end;

procedure TSingleMatrixHelper.fromDelimitedString(const s: string; const delim: char; const bNaNs: boolean);
{generic reading of delimited file into a TMatrix where delim is the delimiter.
this version returns an array of the correct size. bNans says whether we clear
the matrix with 0 or NaN.
If the input string is 0 length, the result is initialised and returned. (Kraken
in particular can lead to this happening)}
var
    i,j,imax,jmax, masterJMax: integer;
    sl1,sl2: ISmart<TStringlist>;
    tmpS: string;
begin
    if length(s) = 0 then Exit;

    sl2 := TSmart<TStringList>.create(TStringlist.Create);
    sl1 := TSmart<TStringList>.create(TStringlist.Create);
    sl1.Text := s;
    imax := sl1.Count;
    if imax > 0 then
    begin
        sl2.Delimiter := delim;
        sl2.StrictDelimiter := true;
        sl2.DelimitedText:=(sl1[0]);
        jmax := sl2.Count;
        masterJMax := jmax;
        setlength(self,0,0);
        setlength(self,imax,jmax);
        if bNaNs then
            self.setAllVal(nan);

        for i := 0 to imax - 1 do
        begin
            sl2.Delimiter := delim;
            sl2.StrictDelimiter := true;
            tmpS := trim(sl1[i]);
            if length(tmpS) > 0 then
            begin
                {sometimes we have ';' at the end of line, trim these (unless ';' is actually the delimiter)}
                if (delim <> ';') and (tmpS[length(tmpS)] = ';') then
                    setlength(tmpS,length(tmpS) - 1);
                sl2.DelimitedText := trim(tmpS);   //fortran puts a leading space on each line
                {we check the jmax for this row against the masterJMax from the first row, so that
                we don't error on rows that are longer than they should be}
                for j := 0 to sl2.Count - 1 do
                    if (j < masterJMax) and isFloat(sl2[j]) then
                        self[i,j] := tofl(sl2[j]);
            end;
        end;//i
    end; //imax > 0
end;

procedure TSingleMatrixHelper.fromSingleDynArray(const s: TSingleDynArray);
var
    iRows,iCols,i,j: integer;
begin
    if length(s) <= 1 then
    begin
        setlength(self, 0);
        exit;
    end;

    iRows := saferound(s[0]);
    iCols := (length(s) - 1) div iRows;
    setlength(self, iRows, iCols);
    for i := 0 to iRows - 1 do
        for j := 0 to iCols - 1 do
            self[i,j] := s[1 + i * iCols + j];
end;

function TSingleMatrixHelper.add(const d: double): TSingleMatrix;
var
    i,j: integer;
begin
    if length(self) = 0 then exit;

    setlength(result, length(self), length(self[0]));
    for i := 0 to length(self) - 1 do
        for j := 0 to length(self[0]) - 1 do
            result[i,j] := self[i,j] + d;
end;

function TSingleMatrixHelper.allValuesEqual(): boolean;
var
    s: single;
    i,j: integer;
begin
    s := nan;
    for i := 0 to length(Self) - 1 do
        for j := 0 to length(Self[0]) - 1 do
            if (not isnan(Self[i,j])) and (Self[i,j] <= 0) then
                if isnan(s) then
                    s := Self[i,j]
                else if s <> Self[i,j] then
                    exit(false);
    result := true;
end;

{ TMatrixHelper }

function TMatrixHelper.column(const iCol: integer): TDoubleDynArray;
var
    j: integer;
begin
    if (iCol < 0) or (icol >= length(self)) then
    begin
        setlength(result,0);
        exit;
    end;

    setlength(result,length(self[0]));
    for j := 0 to high(self[0]) do
        result[j] := self[iCol,j];
end;

function TMatrixHelper.max: double;
var
    i,j: integer;
begin
    result := NaN;

    if length(self) = 0 then exit;

    for i := 0 to high(self) do
        for j := 0 to high(self[0]) do
            greater(result, self[i,j]);
end;

function  TMatrixHelper.min:double;
var
    i,j: integer;
begin
    result := NaN;

    if length(self) = 0 then exit;

    for i := 0 to high(self) do
        for j := 0 to high(self[0]) do
            lesser(result, self[i,j]);
end;

function TMatrixHelper.copy: TMatrix;
var
    i,j: integer;
    la1,la2: integer;
begin
    la1 := length(self);
    if la1 = 0 then
    begin
        setlength(result,0);
        Exit;
    end;

    la2 := Length(self[0]);
    setlength(result,la1,la2);
    for i := 0 to la1 - 1 do
        for j := 0 to la2 - 1 do
            result[i,j] := self[i,j];
end;

function TMatrixHelper.rotate90: TMatrix;
var
    i,j: integer;
begin
    if length(self) = 0 then exit;

    setlength(result, length(self[0]), length(self));
    for i := 0 to length(self) - 1 do
        for j := 0 to length(self[0]) - 1 do
            result[j,i] := self[i,j];
end;

function   TMatrixHelper.row(const aRow: integer): TDoubleDynArray;
var
    i: integer;
    la1,la2: integer;
begin
    la1 := length(self);
    if la1 = 0 then
    begin
        setlength(result,0);
        Exit;
    end;

    la2 := Length(self[0]);
    if aRow >= la2 then
    begin
        setlength(result,0);
        Exit;
    end;

    setlength(result,la1);
    for i := 0 to la1 - 1 do
        result[i] := self[i,aRow];
end;

procedure   TMatrixHelper.sort(const iCol: integer);
{sort matrix along column _index_ iCol (starts from 0)}
var
    hi, hj, I, J, k: integer;
    T: array of double;
begin
    setlength(T,length(self));
    hi := high(self);
    if iCol > hi then
        raise Exception.Create('Error: Selected column is greater than matrix width');

    hj := high(self[0]);
    for I := hj downto 0 do
        for J := 0 to hj - 1 do
            if self[iCol,J] > self[iCol,J + 1] then
            begin
                for k := 0 to hi do
                begin
                    T[k] := self[k,J];
                    self[k,J] := self[k,J + 1];
                    self[k,J + 1] := T[k];
                end;
            end;
end;

function    TMatrixHelper.uniqueElementsInColumn(const iCol: integer):TMatrix;
{get a matrix where column col only contains unique entries. NB Matrix must be
sorted!}
var
  hi, hj, I, J, count: Integer;
  CopyRows: array of integer;
begin
    //SORT MATRIX A BEFORE RUNNING THIS

    hi := high(self);
    if iCol > hi then
        raise Exception.Create('Error: Selected column is greater than matrix width');

    hj := high(self[0]);

    {copyrows contains the row indices of all rows we need to copy to the result. the remaining elements above count are not used}
    setlength(CopyRows,hj + 1); //max possible number of rows to copy is the length of self
    CopyRows[0] := 0;
    count := 1;
    for J := 1 to hj do
    begin
        {compare this value to the last value added, if unequal then add the current row to CopyRows and increment the row count}
        if self[iCol,J] <> self[iCol,CopyRows[count-1]] then
        begin
            CopyRows[count] := j;
            count := count + 1;
        end;
    end;

    {size the result matrix and copy all necessary rows (given by copyRows) to it}
    setlength(result,length(self),count);
    for j := 0 to count - 1 do
        for i := 0 to length(self) - 1 do
            result[i,j] := self[i,CopyRows[j]];
end;

procedure  TMatrixHelper.removeRow(const iRow: integer);
var
    hi,hj,i,j: integer;
begin
    hi := high(self);
    hj := high(self[0]);

    for j := iRow + 1 to hj do
        for i := 0 to hi do
            self[i,j-1] := self[i,j];
    SetLength(self, hi+1, hj);
end;

procedure TMatrixHelper.deserialise(const s: string);
var
    i,j: integer;
    sM: TStringMatrix;
begin
    sM.deserialise(s);
    setlength(self,0,0);
    setlength(self,length(sM),length(sM[0]));
    for i := 0 to length(sM) - 1 do
        for j := 0 to length(sM[0]) - 1 do
            self[i,j] := tryTofl(sM[i,j]);
end;

function TMatrixHelper.serialise(const forceDot: boolean): string;
var
    s: string;
    hi,hj,i,j: integer;
    sMat: TStringMatrix;
begin
    s := '';
    hi := high(self);
    if length(self) = 0 then
        exit;

    hj := high(self[0]);
    {convert the matrix to a stringmatrix and then use the stringmatrix serialise function}
    setlength(sMat,hi + 1,hj + 1);
    for i := 0 to hi do
    begin
        for j := 0 to hj do
        begin
            if isNaN(self[i,j]) then
                sMat[i,j] := nanString
            else
            begin
                if forceDot then
                    sMat[i,j] := forceDotSeparator( tostr(round1(self[i,j])) )
                else
                    sMat[i,j] := tostr(round1(self[i,j]));
            end;
        end;
    end;
    result := sMat.serialise;
end;

procedure TMatrixHelper.setAllVal(const d: double);
var
    i,j: integer;
begin
    for i := 0 to high(self) do
        for j := 0 to high(self[0]) do
            self[i,j] := d;
end;

function  TMatrixHelper.findWithNoInternalAssigns(const x: double; const iCol: integer):integer;
var
    i: integer;
begin
    //find value in matrix column, without extra array assigns (for speed/not locking processors)
    if iCol > (length(self) - 1) then raise Exception.Create('Error: Selected column is greater than matrix width');

    {can be used for an exact match, or also the next continuous. returns -1 if all x < self}
    result := -1;
    if isNaN(x) then Exit;
    for i := 0 to length(self[0]) - 1 do
        if x >= self[iCol,i] then    //also used to find exact values
            result := i;
end;

procedure TMatrixHelper.fromClipboard;
var
    s: string;
begin
    s := Clipboard.AsText;
    if s = '' then
    begin
        setlength(self,0);
        Exit;
    end;

    {replace tabcharacters with ',', the expected field delimiter in deserialiseStringMatrix}
    s := StringReplace(s,AnsiString(#09),',',[rfReplaceAll, rfIgnoreCase]);
    {replace line breaks #13#10 as we get them from spreadsheet with ';', the expected line break char in deserialiseStringMatrix}
    s := StringReplace(s,AnsiString(#13#10),';',[rfReplaceAll, rfIgnoreCase]);
    {strip extra apostrophes}
    s := StringReplace(s,AnsiString(#39),'',[rfReplaceAll, rfIgnoreCase]);
    {strip last ';'}
    if s[length(s)] = ';' then
        setlength(s,length(s) - 1);

    self.deserialise(s);
end;

procedure TMatrixHelper.fromDelimitedString(const s: string; const delim: char; const bNaNs: boolean);
{generic reading of delimited file into a TMatrix where delim is the delimiter.
this version returns an array of the correct size. bNans says whether we clear
the matrix with 0 or NaN.
If the input string is 0 length, the result is initialised and returned. (Kraken
in particular can lead to this happening)}
var
    i,j,imax,jmax, masterJMax: integer;
    sl1,sl2: ISmart<TStringlist>;
    tmpS: string;
begin
    if length(s) = 0 then Exit;

    sl1 := TSmart<TStringList>.create(TStringlist.Create);
    sl2 := TSmart<TStringList>.create(TStringlist.Create);

    sl1.Text := s;
    imax := sl1.Count;
    if imax > 0 then
    begin
        sl2.Delimiter := delim;
        sl2.StrictDelimiter := true;
        sl2.DelimitedText:=(sl1[0]);
        jmax := sl2.Count;
        masterJMax := jmax;
        setlength(self,0,0);
        setlength(self,imax,jmax);
        if bNaNs then
            self.setAllVal(nan);

        for i := 0 to imax - 1 do
        begin
            sl2.Delimiter := delim;
            sl2.StrictDelimiter := true;
            tmpS := trim(sl1[i]);
            {sometimes we have ';' at the end of line, trim these (unless ';' is actually the delimiter)}
            if (delim <> ';') and (tmpS[length(tmpS)] = ';') then
                setlength(tmpS,length(tmpS) - 1);
            sl2.DelimitedText := trim(tmpS);   //fortran puts a leading space on each line
            {we check the jmax for this row against the masterJMax from the first row, so that
            we don't error on rows that are longer than they should be}
            for j := 0 to sl2.Count - 1 do
                if (j < masterJMax)and isFloat(sl2[j]) then
                    self[i,j] := tofl(sl2[j]);
        end;//i
    end; //imax > 0
end;

procedure TMatrixHelper.fromDelimitedStringKeepSize(const s: string; const delim: char; const bNaNs, rotate: boolean);
{generic reading of delimited strings into a TMatrix where delim is the delimiter.
this function works in place, as we already have a results array of the correct size (or 1 too large, possibly)
which is set to NaN.
bNans says whether we clear the matrix with 0 or NaN}
var
    hi,hj,i,j: integer;
    tmpMat: TMatrix;
    sTmpMati,sTmpMatj: integer;
begin
    {size of self array}
    hi := high(self);
    hj := high(self[0]);

    if bNaNs then
        self.setAllVal(nan)
    else
        self.setAllVal(0);

    {use the other overloaded method to read string}
    tmpMat.fromDelimitedString(s,delim,bNaNs);
    if rotate then
        tmpMat := tmpMat.rotate90();
    //showmessage(inttostr(length(self)) + ' ' + inttostr(length(self[0])) + ', ' + inttostr(length(tmpMat)) + ' ' + inttostr(length(tmpMat[0])));

//    {currently ramMDA and bellhop can return matrices that differ in
//    the lengths of i and j by 1 so there are 2 possible jmax values}
//    if (not checkRange(high(self),hi,hi + 1)) or (not checkRange(high(self[0]),hj,hj + 1)) then
//        raise Exception.Create('Returned data wrong size for results array.');

    {RAMMDA is inconsistent about how many depth points it returns. so we have to be fairly
    flexible about the size of the returned matrix. if the returned matrix is short, entries will be left at the
    initialised value}
    sTmpMati := length(tmpMat);
    sTmpMatj := length(tmpMat[0]);
    for i := 0 to hi do
        for j := 0 to hj do
            if (i < sTmpMati) and (j < sTmpMatj) then
                self[i,j] := tmpMat[i,j];
end;

{ TIntergerMatrixHelper }

function TIntergerMatrixHelper.copy: TIntegerMatrix;
var
    i,j: integer;
    la1,la2: integer;
begin
    la1 := length(self);
    if la1 = 0 then
    begin
        setlength(result,0);
        Exit;
    end;

    la2 := Length(self[0]);
    setlength(result,la1,la2);
    for i := 0 to la1 - 1 do
        for j := 0 to la2 - 1 do
            result[i,j] := self[i,j];
end;

procedure TIntergerMatrixHelper.deserialise(const s: string);
var
    i,j: integer;
    sM: TStringMatrix;
begin
    sM.deserialise(s);
    setlength(self,0,0);
    setlength(self,length(sM),length(sM[0]));
    for i := 0 to length(sM) - 1 do
        for j := 0 to length(sM[0]) - 1 do
            if isInt(sM[i,j]) then
                self[i,j] := toint(sM[i,j])
            else
                self[i,j] := 0;
end;

function TIntergerMatrixHelper.serialise: string;
var
    hi,hj,i,j: integer;
    sMat: TStringMatrix;
begin
    hi := high(self);
    if length(self) = 0 then
        exit;

    hj := high(self[0]);
    {convert the matrix to a stringmatrix and then use the stringmatrix serialise function}
    setlength(sMat,hi + 1,hj + 1);
    for i := 0 to hi do
        for j := 0 to hj do
            sMat[i,j] := tostr(self[i,j]);
    result := sMat.serialise;
end;

{ TStringMatrixHelper }

function TStringMatrixHelper.copy: TStringMatrix;
var
    i,j: integer;
    la1,la2: integer;
begin
    la1 := length(self);
    if la1 = 0 then
    begin
        setlength(result,0);
        Exit;
    end;

    la2 := Length(self[0]);
    setlength(result,la1,la2);
    for i := 0 to la1 - 1 do
        for j := 0 to la2 - 1 do
            result[i,j] := self[i,j];
end;

procedure TStringMatrixHelper.deserialise(const s: string);
var
    i,hi,j: integer;
    sl: ISmart<TStringList>;
    sTmp: string;
    sA: TStringArray;
begin
    sl := TSmart<TStringList>.create(TStringList.create);
    setlength(self,0,0);

     {ExtractStrings is no good here, as it won't add empty lines to the list}

    sl.Delimiter := ';';
    {replace any spaces with #255 (not often used ascii code), as space gives problems. also remove EOL}
    sTmp := StringReplace(s,' ',AnsiString(#255),[rfReplaceAll, rfIgnoreCase]);
    sl.DelimitedText := StringReplace(sTmp,AnsiString(#13#10),'',[rfReplaceAll, rfIgnoreCase]);
    hi := sl.count - 1;
    if sl.Count > 0 then
    begin
        sA.deserialise(sl[0]);
        if length(sA) > 0 then
        begin
            setlength(self,sl.Count,length(sA));
            for i := 0 to hi do
            begin
                {go through each field in sA and replace #255 with spaces, then
                put it in the self}
                sA.deserialise(sl[i]);
                if length(sA) = length(self[0]) then
                    for j := 0 to length(sA) - 1 do
                        self[i,j] := StringReplace(sA[j],AnsiString(#255),' ',[rfReplaceAll, rfIgnoreCase]);
            end;
        end;
    end;
end;

function TStringMatrixHelper.serialise: string;
var
    sA: TStringArray;
    hi,hj,i,j: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    hi := high(self);
    hj := high(self[0]);
    setlength(sA,hj + 1);
    for i := 0 to hi do
    begin
        for j := 0 to hj do
            sA[j] := self[i,j];
        sb.append(sA.serialise);
//        for j := 0 to hj do
//        begin
//            s := s + stringreplace(stringreplace(sM[i,j],',',' ',[rfReplaceAll]),';',' ',[rfReplaceAll]); //replace any delimeters within the fields with spaces
//            if j < hj then
//                s := s + ',';  //field separator
//        end;
        if i < hi then
            sb.append(';' + #13#10);  //line separator (except not on last line) - this is just there so it looks nicer in the XML file, we will remove it later on deserialise
    end;
    result := sb.ToString;
end;

procedure TStringMatrixHelper.fromStringList(const sl1: TStringlist; const delim: char);
var
    i,j,imax,jmax, masterJMax: integer;
    sl2: ISmart<TStringlist>;
    tmpS: string;
begin
    if length(sl1.Text) = 0 then Exit;

    imax := sl1.Count;
    if imax = 0 then Exit;

    sl2 := TSmart<TStringList>.create(TStringlist.Create);
    sl2.Delimiter := delim;
    sl2.StrictDelimiter := true;
    sl2.DelimitedText := (sl1[0]);
    jmax := sl2.Count;
    masterJMax := jmax;
    setlength(self,0,0);
    setlength(self,imax,jmax);

    for i := 0 to imax - 1 do
    begin
        sl2.Delimiter := delim;
        sl2.StrictDelimiter := true;
        tmpS := trim(sl1[i]);
        {sometimes we have ';' at the end of line, trim these (unless ';' is actually the delimiter)}
        if delim <> ';' then
            if tmpS[length(tmpS)] = ';' then
                setlength(tmpS,length(tmpS) - 1);
        sl2.DelimitedText := trim(tmpS);   //fortran puts a leading space on each line
        {we check the jmax for this row against the masterJMax from the first row, so that
        we don't error on rows that are longer than they should be}
        for j := 0 to sl2.Count - 1 do
            if j < masterJMax then
                self[i,j] := sl2[j];
    end;//i
end;

procedure TStringMatrixHelper.fromDelimitedString(const s: string; delim: char);
var
    sl1: ISmart<TStringList>;
begin
    {read file into a string and then use the string version of the function}
    sl1 := TSmart<TStringList>.create(TStringlist.Create);
    sl1.Text := s;
    self.fromStringList(sl1,delim);
end;

{ TBoolMatrixHelper }

procedure TBoolMatrixHelper.setAllVal(const b: boolean);
var
    i,j: integer;
begin
    for i := 0 to length(self) - 1 do
        for j := 0 to length(self[0]) - 1 do
            self[i,j] := b;
end;

constructor TMyObjectList<T>.Create(ownsObjects: boolean);
begin
    inherited;

    fselected := nil;
end;

function TMyObjectList<T>.getSelected: T;
begin
    if (not assigned(fSelected)) or (not self.Contains(fSelected)) then
        exit(nil);
    result := fselected;
end;

function TMyObjectList<T>.enumerate: TArray<TPair<integer, T>>;
var
    i: integer;
begin
    setlength(result,self.count);
    for i := 0 to self.count - 1 do
        result[i] := TPair<integer,T>.create(i,self[i]);
end;

procedure TMyObjectList<T>.setSelected(val: T);
begin
    if (not self.Contains(val)) then
        self.fSelected := nil
    else
        self.fSelected := val;
end;

{ TMouseInfo }

constructor TMouseInfo.create(const ax, ay: integer);
begin
    self.x := ax;
    self.y := ay;
    self.button.null();
end;

constructor TMouseInfo.createWithButton(const ax, ay: integer; const aButton: TMouseButton);
begin
    self.x := ax;
    self.y := ay;
    self.button := aButton;
end;

function TMouseInfo.point: TPoint;
begin
    result.x := x;
    result.y := y;
end;

{ TBandwithHelper }

function TBandwithHelper.oct: boolean;
begin
    result := self = bwOctave;
end;

{ TStringDynArrayHelper }

function TStringDynArrayHelper.serialise: string;
var
    hj,j: integer;
    sb: ISmart<TStringBuilder>;
begin
    sb := TSmart<TStringBuilder>.create(TStringBuilder.Create);
    hj := high(self);
    for j := 0 to hj do
    begin
        sb.append(stringreplace(stringreplace(self[j],',',' ',[rfReplaceAll]),';',' ',[rfReplaceAll])); //replace any delimeters within the fields with spaces
        if j < hj then
            sb.append(',');  //field separator
    end;
    result := sb.ToString;
end;



function TRangeValArrayHelper.findRange(const range: double): integer;
var
    i: integer;
begin
    {can be used for an exact match, or also the next continuous. returns -1 if all x < self}
    result := -1;
    if isNaN(range) then Exit;
    for i := 0 to length(self) - 1 do
        if range >= TRangeVal(self[i]).range then
            result := i;
end;

{ TDepthValArrayHelper }

function TDepthValArrayHelper.average: double;
var
    i: integer;
begin
    result := 0;
    for i := 0 to length(self) - 1 do
        result := result + TDepthVal(self[i]).val;
    result := result / length(self);
end;

function TDepthValArrayHelper.findDepth(const depth: double): integer;
var
    i: integer;
begin
    {can be used for an exact match, or also the next continuous. returns -1 if all x < self}
    result := -1;
    if isNaN(depth) then Exit;
    for i := 0 to length(self) - 1 do
        if depth >= TDepthVal(self[i]).depth then
            result := i;
end;

function TDepthValArrayHelper.maxDepth: double;
begin
    result := TDepthVal(self[length(self) - 1]).depth;
end;

function TRangeValArrayHelper.min: double;
var
    rv: TRangeVal;
begin
    result := nan;
    if length(self) = 0 then
        exit;

    for rv in self do
        if not isnan(rv.val) and isnan(result) or (rv.val < result) then
            result := rv.val;
end;

function TRangeValArrayHelper.max: double;
var
    rv: TRangeVal;
begin
    result := nan;
    if length(self) = 0 then
        exit;

    for rv in self do
        if not isnan(rv.val) and isnan(result) or (rv.val > result) then
            result := rv.val;
end;

{ TDepthValArrayAtRangeArrayHelper }

function TDepthValArrayAtRangeArrayHelper.depthValForRange(const range: double; out index: integer): TDepthValArray;
var
    i: integer;
begin
    result := TDepthValArrayAtRange(self[0]).depthVal;
    index := 0;
    for i := 1 to length(self) - 1 do
        if TDepthValArrayAtRange(self[i]).range <= range then
        begin
            result := TDepthValArrayAtRange(self[i]).depthVal;
            index := i;
        end
        else
            exit();
end;

{ TDepthValArrayAtRange }

class function TDepthValArrayAtRange.create(r: double; dv: TDepthValArray): TDepthValArrayAtRange;
begin
    result.range := r;
    result.depthVal := dv;
end;

{ TWrappedDouble }

constructor TWrappedDouble.create(const d: double = 0);
begin
    inherited create;

    self.d := d;
end;

{ TSmallIntDynArrayHelper }

function TSmallIntDynArrayHelper.toDouble: TDoubleDynArray;
var
    i: integer;
begin
    setlength(result, length(self));
    for i := 0 to length(self) - 1 do
        result[i] := self[i];
end;

function TSmallIntDynArrayHelper.toSingle(const pb: TProgressBar): TSingleDynArray;
const
    updateUI = 10000000;
var
    i: integer;
begin
    if assigned(pb) then
    begin
        pb.Position := 0;
        pb.Max := length(self);
        Application.ProcessMessages;
    end;

    setlength(result, length(self));
    for i := 0 to length(self) - 1 do
    begin
        result[i] := self[i];

        if assigned(pb) and (i mod updateUI = 0) then
        begin
            pb.Position := i;
            Application.ProcessMessages;
        end;
    end;
end;


{ TFPointArrayHelper }

procedure TFPointArrayHelper.deserialise(const s: string);
var
    m: TMatrix;
    i: Integer;
begin
    setlength(self,0);

    m.deserialise(s);
    if (length(m) = 0) or (length(m[0]) <> 2) then exit;
    setlength(self, length(m));

    for i := 0 to length(m) - 1 do
    begin
        TFPoint(self[i]).x := m[i,0];
        TFPoint(self[i]).y := m[i,1];
    end;
end;

function TFPointArrayHelper.serialise: string;
var
    m: TMatrix;
    i: integer;
begin
    result := '';

    setlength(m, length(self), 2);
    for i := 0 to length(self) - 1 do
    begin
        m[i,0] := TFPoint(self[i]).x;
        m[i,1] := TFPoint(self[i]).y;
    end;

    result := m.serialise();
end;

procedure TFPoint.setXY(const x, y: double);
begin
    self.x := x;
    self.y := y;
end;

{ TWrappedInt }

constructor TWrappedInt.create(const i: integer);
begin
    inherited create;
    self.i := i;
end;

{ TWrappedDoubleArrayHelper }

procedure TWrappedDoubleArrayHelper.free;
var
    i: integer;
begin
    for i := 0 to length(self) - 1 do
        self[i].free;
end;

procedure TWrappedDoubleArrayHelper.init(const n: integer);
var
    i: integer;
begin
    setlength(self, n);
    for i := 0 to n - 1 do
        self[i] := TWrappedDouble.create();
end;

function TWrappedDoubleArrayHelper.totalFractionDone: double;
var
    wd: TWrappedDouble;
begin
    result := 0;
    for wd in self do
        result := result + wd.d;
    result := result / length(self);
end;

{ TWrappedDoubleArrayArrayHelper }

procedure TWrappedDoubleArrayArrayHelper.free;
var
    i: integer;
begin
    for I := 0 to length(self) - 1 do
        TWrappedDoubleArray(self[i]).free;
end;

procedure TWrappedDoubleArrayArrayHelper.init(const m, n: integer);
var
    i: integer;
begin
    setlength(self,m);
    for I := 0 to m - 1 do
        TWrappedDoubleArray(self[i]).init(n);
end;

function TWrappedDoubleArrayArrayHelper.totalFractionDone: double;
var
    arr: TWrappedDoubleArray;
begin
    result := 0;
    for arr in self do
        result := result + arr.totalFractionDone;
    result := result / length(self);
end;

{ TDoubleList }

procedure TDoubleList.init(const n: integer);
var
    i: integer;
begin
    for i := 0 to n - 1 do
        self.add(TWrappedDouble.create);
end;

function TDoubleList.totalFractionDone: double;
var
    wd: TWrappedDouble;
begin
    result := 0;
    for wd in self do
        result := result + wd.d;
    result := result / self.count;
end;

{ TDoubleListList }

procedure TDoubleListList.init(const m, n: integer);
var
    i: integer;
    list: TDoubleList;
begin
    for I := 0 to m - 1 do
    begin
        list := TDoubleList.create();
        list.init(n);
        self.add(list);
    end;
end;

function TDoubleListList.totalFractionDone: double;
var
    arr: TDoubleList;
begin
    result := 0;
    for arr in self do
        result := result + arr.totalFractionDone;
    result := result / self.count;
end;

function TExtents.width;
begin
    result := xmax - xmin;
end;

function TExtents.height;
begin
    result := ymax - ymin;
end;

end.

