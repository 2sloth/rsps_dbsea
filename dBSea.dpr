﻿program dBSea;

{$R *.RES}


{$DEFINE MSWINDOWS}
{$DEFINE SDI}
{$WARN SYMBOL_PLATFORM OFF}
{$EXCESSPRECISION OFF} // for 64 bit, this should speed things up when we are using singles and don't really care about precision all that much

uses
  {$DEFINE USE_SCALEMM}
  {$IFDEF USE_SCALEMM}
  ScaleMM2 in 'scaleMM\ScaleMM2.pas',
  Optimize.Move in 'scaleMM\Optimize.Move.pas',
  smmFunctions in 'scaleMM\smmFunctions.pas',
  smmGlobal in 'scaleMM\smmGlobal.pas',
  smmLargeMemory in 'scaleMM\smmLargeMemory.pas',
  smmLogging in 'scaleMM\smmLogging.pas',
  smmMediumMemory in 'scaleMM\smmMediumMemory.pas',
  smmSmallMemory in 'scaleMM\smmSmallMemory.pas',
  smmStatistics in 'scaleMM\smmStatistics.pas',
  smmTypes in 'scaleMM\smmTypes.pas',
  {$ENDIF }
  Forms,
  Windows,
  Classes,
  SysUtils,
  MCKLib in 'MCKLib.pas',
  SQLite3 in 'sqlitedelphi\SQLite3.pas',
  sqlite3udf in 'sqlitedelphi\sqlite3udf.pas',
  SQLiteTable3 in 'sqlitedelphi\SQLiteTable3.pas',
  dglOpenGL in 'dglOpenGL.pas',
  SynPdf in 'synpdf\SynPdf.pas',
  mORMotReport in 'synpdf\mORMotReport.pas',
  SynCommons in 'synpdf\SynCommons.pas',
  SynCrypto in 'synpdf\SynCrypto.pas',
  SynGdiPlus in 'synpdf\SynGdiPlus.pas',
  SynLZ in 'synpdf\SynLZ.pas',
  SynZip in 'synpdf\SynZip.pas',
  hasp_unit in 'hasp\hasp_unit.pas',
  Evalutor in 'MMatrix\Evalutor.pas',
  MathMethod in 'MMatrix\MathMethod.pas',
  Matrixadd in 'MMatrix\Matrixadd.pas',
  mcomplex in 'MMatrix\mcomplex.pas',
  MMatrix in 'MMatrix\MMatrix.pas',
  PasswordChecking in 'PasswordChecking.pas',
  dBSeaColours in 'dBSeaColours.pas',
  baseTypes in 'baseTypes.pas',
  helperFunctions in 'helperFunctions.pas',
  dbSeaConstants in 'dbSeaConstants.pas',
  solveTimeEstimator in 'solveTimeEstimator.pas',
  objectCopier in 'objectCopier.pas',
  ESRIReader in 'ESRIReader.pas',
  flock in 'flock.pas',
  multiLayerReflectionCoefficient in 'multiLayerReflectionCoefficient.pas',
  levelConverter in 'levelConverter.pas',
  dBSeaRay2 in 'solvers\dBSeaRay2.pas',
  dBSeaModes1 in 'solvers\dBSeaModes1.pas',
  dBSeaModes2 in 'solvers\dBSeaModes2.pas',
  DSP in 'DSP.pas',
  filterbank in 'filterbank.pas',
  latlong2UTM in 'latlong2UTM.pas',
  thresholdLevels in 'thresholdLevels.pas',
  externalSolvers in 'solvers\externalSolvers.pas',
  EditFishFrame in 'frames\EditFishFrame.pas' {EditFishFrame: TFrame},
  EditFrequencyFrame in 'frames\EditFrequencyFrame.pas' {FrequencyFrame: TFrame},
  EditProbeFrame in 'frames\EditProbeFrame.pas' {EditProbeFrm: TFrame},
  EditResultsFrame in 'frames\EditResultsFrame.pas' {EditResultsFrame: TFrame},
  EditScenarioFrame in 'frames\EditScenarioFrame.pas' {ScenarioFrame: TFrame},
  EditSedimentFrame in 'frames\EditSedimentFrame.pas' {EditSedimentFrame: TFrame},
  EditSetupFrame in 'frames\EditSetupFrame.pas' {SetupFrame: TFrame},
  EditSolveFrame in 'frames\EditSolveFrame.pas' {EditSolveFrame: TFrame},
  EditSourceFrame in 'frames\EditSourceFrame.pas' {EditSourceFrame: TFrame},
  EditWaterFrame in 'frames\EditWaterFrame.pas' {EditWaterFrame: TFrame},
  about in 'forms\about.pas' {AboutBox},
  crossSecNameChange in 'forms\crossSecNameChange.pas' {crossSecNameChangeFrm},
  CSVdelimiters in 'forms\CSVdelimiters.pas' {CSVDelimitersForm},
  directivityInput in 'forms\directivityInput.pas' {directivityInputForm},
  equipDBFrm in 'forms\equipDBFrm.pas' {EquipDBForm},
  ExportImageTitleBlock in 'forms\ExportImageTitleBlock.pas' {FormExportTitleBlock},
  FishAddDBFrm in 'forms\FishAddDBFrm.pas' {FishAddToDB},
  flockInput in 'forms\flockInput.pas' {flockInputForm},
  flockOutput in 'forms\flockOutput.pas' {flockOutputForm},
  glDrawProgress in 'forms\glDrawProgress.pas' {glDrawProgressForm},
  GLWindow in 'forms\GLWindow.pas' {GLForm},
  LevelLimitsDlg in 'forms\LevelLimitsDlg.pas' {LevelLimitsDialog},
  Main in 'forms\Main.pas' {MainForm},
  mitigationDialog in 'forms\mitigationDialog.pas' {MitigationForm},
  Options in 'forms\Options.pas' {OptionsForm},
  ResultsColourSet in 'forms\ResultsColourSet.pas' {ResultsColoursFrm},
  scenarioDiffForm in 'forms\scenarioDiffForm.pas' {scenarioDiffFrm},
  scenarioSummary in 'forms\scenarioSummary.pas' {frmScenarioSummary},
  SedimentAddDB in 'forms\SedimentAddDB.pas' {SedimentAddForm},
  solverDebugChart in 'forms\solverDebugChart.pas' {solverDebugForm},
  sourceMotionFrm in 'forms\sourceMotionFrm.pas' {SourceMotionForm},
  sourceSliceSelector in 'forms\sourceSliceSelector.pas' {sourceSliceSelectorForm},
  sourceSpectFrm in 'forms\sourceSpectFrm.pas' {sourceSpectrumForm},
  sourceTimeSeries in 'forms\sourceTimeSeries.pas' {sourceTimeSeriesForm},
  splashScreen in 'forms\splashScreen.pas' {SplashScreenForm},
  WorldScaleDialog in 'forms\WorldScaleDialog.pas' {frmWorldScale},
  HASPAPICalls in 'hasp\HASPAPICalls.pas',
  HASPSecurity in 'hasp\HASPSecurity.pas',
  bathymetry in 'dBSeaObjs\bathymetry.pas',
  crossSec in 'dBSeaObjs\crossSec.pas',
  CrossSecEndpoint in 'dBSeaObjs\CrossSecEndpoint.pas',
  dBSeaObject in 'dBSeaObjs\dBSeaObject.pas',
  dBSeaOptions in 'dBSeaObjs\dBSeaOptions.pas',
  directivity in 'dBSeaObjs\directivity.pas',
  equipReturn in 'dBSeaObjs\equipReturn.pas',
  fishCurve in 'dBSeaObjs\fishCurve.pas',
  geoSetup in 'dBSeaObjs\geoSetup.pas',
  griddedTL in 'dBSeaObjs\griddedTL.pas',
  levelLimits in 'dBSeaObjs\levelLimits.pas',
  levelServer in 'dBSeaObjs\levelServer.pas',
  Material in 'dBSeaObjs\Material.pas',
  myColor in 'dBSeaObjs\myColor.pas',
  Pos3 in 'dBSeaObjs\Pos3.pas',
  probe in 'dBSeaObjs\probe.pas',
  problemContainer in 'dBSeaObjs\problemContainer.pas',
  receiverArea in 'dBSeaObjs\receiverArea.pas',
  scenario in 'dBSeaObjs\scenario.pas',
  scenarioDiff in 'dBSeaObjs\scenarioDiff.pas',
  seafloorScheme in 'dBSeaObjs\seafloorScheme.pas',
  soundSource in 'dBSeaObjs\soundSource.pas',
  soundSpeedProfile in 'dBSeaObjs\soundSpeedProfile.pas',
  sourceDBData in 'dBSeaObjs\sourceDBData.pas',
  spectrum in 'dBSeaObjs\spectrum.pas',
  srcPoint in 'dBSeaObjs\srcPoint.pas',
  timeSeries in 'dBSeaObjs\timeSeries.pas',
  waterProps in 'dBSeaObjs\waterProps.pas',
  eventLogging in 'eventLogging.pas',
  logViewer in 'forms\logViewer.pas' {eventLogViewerForm},
  TaskbarProgress in 'TaskbarProgress.pas',
  releaseNotes in 'forms\releaseNotes.pas' {releaseNotesForm},
  dBSeaRay3 in 'solvers\dBSeaRay3.pas',
  glRect in 'glRect.pas',
  fileSaveOpen in 'fileSaveOpen.pas',
  myRegistry in 'myRegistry.pas',
  databaseUpdater in 'databaseUpdater.pas',
  raySolverTypes in 'solvers\raySolverTypes.pas',
  ray3_2GPU in 'solvers\ray3_2GPU.pas',
  ray3GPUBase in 'solvers\ray3GPUBase.pas',
  HistogramCalc in 'HistogramCalc.pas',
  solversGeneralFunctions in 'solvers\solversGeneralFunctions.pas',
  dBSeaPE2 in 'solvers\dBSeaPE2.pas',
  uTExtendedX87 in 'uTExtendedX87.pas',
  webGetHTTP in 'webGetHTTP.pas',
  ArrayToolFormUnit in 'forms\ArrayToolFormUnit.pas' {ArrayToolForm},
  globalsUnit in 'globalsUnit.pas',
  commentUnit in 'dBSeaObjs\commentUnit.pas',
  soundSourceSolveHelper in 'helpers\soundSourceSolveHelper.pas',
  GLWindowOptions in 'forms\GLWindowOptions.pas',
  animat in 'animat.pas',
  BathymetryInterface in 'dBSeaObjs\BathymetryInterface.pas',
  GeoSetupAndBathyDelegate in 'dBSeaObjs\GeoSetupAndBathyDelegate.pas',
  Piling1D in 'Piling1D.pas',
  FilterbankByDownsample in 'FilterbankByDownsample.pas',
  ScenarioSolveHelper in 'helpers\ScenarioSolveHelper.pas',
  GPXParser in 'GPXParser.pas',
  MyFrameInterface in 'frames\MyFrameInterface.pas',
  HighOrder in 'HighOrder.pas',
  StringGridHelper in 'helpers\StringGridHelper.pas',
  SolveReturn in 'dBSeaObjs\SolveReturn.pas',
  MemoryStreamHelper in 'helpers\MemoryStreamHelper.pas',
  lz4 in 'lib\delphi\lz4.pas',
  lz4common in 'lib\delphi\lz4common.pas',
  lz4frame in 'lib\delphi\lz4frame.pas',
  lz4frame_static in 'lib\delphi\lz4frame_static.pas',
  lz4HC in 'lib\delphi\lz4HC.pas',
  xxHash in 'lib\delphi\xxHash.pas',
  MyCompression in 'MyCompression.pas',
  SynLZO in 'synpdf\SynLZO.pas',
  MaintenanceStringParser in 'MaintenanceStringParser.pas',
  MyJavascriptViaTWebBrowser in 'js\MyJavascriptViaTWebBrowser.pas',
  JSSpidermonkey in 'js\JSSpidermonkey.pas',
  js15decl in 'js\js15decl.pas',
  jsDbgServer in 'js\jsDbgServer.pas',
  jsintf in 'js\jsintf.pas',
  NamedPipesImpl in 'js\NamedPipesImpl.pas',
  cloudSolve in 'solvers\cloudSolve.pas',
  CrossSectionForm in 'forms\CrossSectionForm.pas' {editCrossSectionForm},
  UTMConversionForm in 'forms\UTMConversionForm.pas' {UTMConversionFrm},
  LatLongEntryFrm in 'forms\LatLongEntryFrm.pas' {LatLongEntryForm},
  SmartPointer in 'SmartPointer.pas',
  Nullable in 'Nullable.pas',
  NewDepth in 'forms\NewDepth.pas' {NewDepthForm},
  ProcessorAffinity in 'ProcessorAffinity.pas',
  dBSeaRayArrivals in 'solvers\dBSeaRayArrivals.pas',
  fmath in 'fft\fmath.pas',
  fourier in 'fft\fourier.pas',
  matrices in 'fft\matrices.pas',
  dBSeaPESS1 in 'solvers\dBSeaPESS1.pas',
  dBSeaPESS3D1 in 'solvers\dBSeaPESS3D1.pas',
  PadeApproximants in 'solvers\PadeApproximants.pas',
  OtlCollections in 'OTL\OtlCollections.pas',
  OtlComm in 'OTL\OtlComm.pas',
  OtlCommBufferTest in 'OTL\OtlCommBufferTest.pas',
  OtlCommon in 'OTL\OtlCommon.pas',
  OtlCommon.Utils in 'OTL\OtlCommon.Utils.pas',
  OtlContainerObserver in 'OTL\OtlContainerObserver.pas',
  OtlContainers in 'OTL\OtlContainers.pas',
  OtlDataManager in 'OTL\OtlDataManager.pas',
  OtlEventMonitor in 'OTL\OtlEventMonitor.pas',
  OtlHooks in 'OTL\OtlHooks.pas',
  OtlLogger in 'OTL\OtlLogger.pas',
  OtlParallel in 'OTL\OtlParallel.pas',
  OtlRegister in 'OTL\OtlRegister.pas',
  OtlSync in 'OTL\OtlSync.pas',
  OtlTask in 'OTL\OtlTask.pas',
  OtlTaskControl in 'OTL\OtlTaskControl.pas',
  OtlThreadPool in 'OTL\OtlThreadPool.pas',
  DetailedRTTI in 'OTL\src\DetailedRTTI.pas',
  DSiWin32 in 'OTL\src\DSiWin32.pas',
  GpLists in 'OTL\src\GpLists.pas',
  GpStringHash in 'OTL\src\GpStringHash.pas',
  GpStuff in 'OTL\src\GpStuff.pas',
  HVStringBuilder in 'OTL\src\HVStringBuilder.pas',
  HVStringData in 'OTL\src\HVStringData.pas',
  MathLib in 'MathLib.pas',
  dBSeaRay3D1 in 'solvers\dBSeaRay3D1.pas',
  raySolverTypes3D in 'solvers\raySolverTypes3D.pas',
  RaySolverCommon in 'solvers\RaySolverCommon.pas',
  TriDiagonal in 'solvers\TriDiagonal.pas',
  BandSolver in 'solvers\BandSolver.pas',
  dBSeaPEElastic1 in 'solvers\dBSeaPEElastic1.pas',
  RayPlotting in 'solvers\RayPlotting.pas',
  RayVisibilityFrm in 'forms\RayVisibilityFrm.pas' {RayVisibilityForm},
  ProbeTimeSeries in 'dBSeaObjs\ProbeTimeSeries.pas',
  RunExecutable in 'RunExecutable.pas',
  NOAACurves in 'NOAACurves.pas',
  CustomMessageDialog in 'CustomMessageDialog.pas',
  Complex80 in 'MMatrix\Complex80.pas',
  MMatrix80 in 'MMatrix\MMatrix80.pas',
  PEStarterPadeExtended in 'solvers\PEStarterPadeExtended.pas',
  PEStarterPadeDouble in 'solvers\PEStarterPadeDouble.pas',
  FilterDef in 'FilterDef.pas',
  StatefulBiquadFilter in 'StatefulBiquadFilter.pas',
  StatefulDownsample in 'StatefulDownsample.pas',
  MovingSourceGIFFormUnit in 'forms\MovingSourceGIFFormUnit.pas' {MovingSourceGIFForm},
  LocationProperties in 'dBSeaObjs\LocationProperties.pas',
  LocationPropertiesFormUnit in 'forms\LocationPropertiesFormUnit.pas' {LocationPropertiesForm},
  WMI_Info in 'WMI_Info.pas',
  StackTrace in 'StackTrace.pas',
  wavReader in 'wavReader.pas',
  BatchFormUnit in 'forms\BatchFormUnit.pas' {BatchForm},
  StringsHelper in 'helpers\StringsHelper.pas',
  ShpAPI129 in 'ShpAPI129.pas',
  MyShapefileWriter in 'MyShapefileWriter.pas',
  QuaternionFuncs in 'QuaternionFuncs.pas',
  SlackIntegration in 'SlackIntegration.pas',
  GeoOverlay in 'dBSeaObjs\GeoOverlay.pas',
  WrappedFPointArray in 'dBSeaObjs\WrappedFPointArray.pas',
  GeoOverlaysFormUnit in 'forms\GeoOverlaysFormUnit.pas' {GeoOverlaysForm},
  AviWriter_2 in 'avi\AviWriter_2.pas',
  BMP2AVI in 'avi\BMP2AVI.pas',
  Silence in 'avi\Silence.pas',
  SelfStarter in 'solvers\SelfStarter.pas',
  MarchingSquares in 'MarchingSquares.pas',
  ImageOverlay in 'dBSeaObjs\ImageOverlay.pas',
  ImageOverlayAnchorFormUnit in 'forms\ImageOverlayAnchorFormUnit.pas' {ImageOverlayAnchorForm},
  ImportedLocationUnit in 'ImportedLocationUnit.pas',
  SourceWeightedLevelsFormUnit in 'forms\SourceWeightedLevelsFormUnit.pas' {SourceWeightedLevelsForm},
  Jacobian in 'solvers\Jacobian.pas',
  Combinations in 'solvers\Combinations.pas';

begin
  {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := true;
  {$ENDIF}

  Application.Initialize;

  theSplashScreen := TSplashScreenForm.Create(application) ;
  try
    theSplashScreen.Show();
    theSplashScreen.Update();

    Application.Title := 'dBSea Underwater Acoustics';
    Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TGLForm, GLForm);
  Application.CreateForm(TSourceMotionForm, SourceMotionForm);
  Application.CreateForm(TFishAddToDB, FishAddToDB);
  Application.CreateForm(TsourceSpectrumForm, sourceSpectrumForm);
  Application.CreateForm(TsourceTimeSeriesForm, sourceTimeSeriesForm);
  Application.CreateForm(TEquipDBForm, EquipDBForm);
  Application.CreateForm(TOptionsForm, OptionsForm);
  Application.CreateForm(TglDrawProgressForm, glDrawProgressForm);
  Application.CreateForm(TLevelLimitsDialog, LevelLimitsDialog);
  Application.CreateForm(TcrossSecNameChangeFrm, crossSecNameChangeFrm);
  Application.CreateForm(TCSVDelimitersForm, CSVDelimitersForm);
  Application.CreateForm(TfrmWorldScale, frmWorldScale);
  Application.CreateForm(TResultsColoursFrm, ResultsColoursFrm);
  Application.CreateForm(TdirectivityInputForm, directivityInputForm);
  Application.CreateForm(TMitigationForm, MitigationForm);
  Application.CreateForm(TsourceSliceSelectorForm, sourceSliceSelectorForm);
  Application.CreateForm(TflockInputForm, flockInputForm);
  Application.CreateForm(TflockOutputForm, flockOutputForm);
  Application.CreateForm(TfrmScenarioSummary, frmScenarioSummary);
  Application.CreateForm(TArrayToolForm, ArrayToolForm);
  Application.CreateForm(TeventLogViewerForm, eventLogViewerForm);
  Application.CreateForm(TeditCrossSectionForm, editCrossSectionForm);
  Application.CreateForm(TUTMConversionFrm, UTMConversionFrm);
  Application.CreateForm(TLatLongEntryForm, LatLongEntryForm);
  Application.CreateForm(TNewDepthForm, NewDepthForm);
  Application.CreateForm(TRayVisibilityForm, RayVisibilityForm);
  Application.CreateForm(TreleaseNotesForm, releaseNotesForm);
  Application.CreateForm(TMovingSourceGIFForm, MovingSourceGIFForm);
  Application.CreateForm(TLocationPropertiesForm, LocationPropertiesForm);
  Application.CreateForm(TBatchForm, BatchForm);
  Application.CreateForm(TGeoOverlaysForm, GeoOverlaysForm);
  Application.CreateForm(TImageOverlayAnchorForm, ImageOverlayAnchorForm);
  Application.CreateForm(TSourceWeightedLevelsForm, SourceWeightedLevelsForm);
  Mainform.FormResize(application); //to get the correct size for the GL Window
    GLNoDraw := false;
    theSplashScreen.Hide;
    GLForm.initialSpinAnimation(); //fancy spin animation to get started
  finally
    theSplashScreen.Free;
  end;
  Application.Run;
end.
