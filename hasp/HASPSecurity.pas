unit HASPSecurity;

{$B-}

interface

uses SynCommons,
    system.sysUtils, system.Classes, system.UITypes,
    winapi.Windows,
    vcl.stdctrls, vcl.Forms, vcl.Dialogs,
    PasswordChecking, helperFunctions, dBSeaconstants,
    hasp_unit, HASPAPICalls, logViewer,
    mcklib, eventlogging, baseTypes, globalsUnit, MyRegistry,
    SmartPointer;

type

//TRunningMode = (rmBlocked,           //immediate quit
//                      rmDemo,        //load bathymetry, save and export disabled
//                      rmTrial,             //save and export disabled
//                      rmNormalKeyPresent); //full version

    THASPSecurity = class(TObject)
    private
    const
        yearForThisTrialVersion     : word = 2017; //the trial version will fail if run after this year
    var
        keypresent                  : Boolean;
        HASPProgramName             : String;
        keyVersion                  : String;
        serialNo                    : String;
        netHaspType                 : integer;
        haspID                      : integer;
        sLicenseOrTrialExpiryDate   : String;
        HASPshandle                 : hasp_handle_t;

        function  CheckYear: Boolean;
        procedure checkCalendar;
        function  isNetworkKey(): Boolean;
        procedure trialModeChecks();
    public
    var
        hasPerformedInitialCheck: boolean;
        constructor Create;

        procedure updateAboutBoxCaptions(const lblSerialNo, lblComments, lblExpiryDate : TLabel);
        procedure HASPFormOnShowChecks(const lblSerialNo, lblComments, lblExpiryDate : TLabel; out returnMessage : string);
        function  HASPFormWhileRunningChecks(): boolean;
        function  allowDBSecurityIndex(const index: integer): boolean;
        procedure importLicenseFile;
        function  exportChecks(): boolean;
        procedure logout();
        function  keySerialNumber: string;
        function  hardwarekeyPresentForCurrentVersion(): boolean;
        function  c2v: string;
        class function  VersionAsInteger(s : string) : integer;
    end;

var
    theHASPSecurity: THASPSecurity;

implementation

uses MaintenanceStringParser;

constructor THASPSecurity.create;
begin
    inherited;

    keypresent := false;
    hasPerformedInitialCheck := false;
end;

procedure THASPSecurity.updateAboutBoxCaptions(const lblSerialNo, lblComments, lblExpiryDate : TLabel);
var 
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'updateAboutBoxCaptions');

    try
        if assigned(lblSerialNo) then
        begin
            lblSerialNo.caption := 'Serial number: ' + SerialNo + '   HASP ID: ' + IntToStr(self.haspID) + '  ';
            if self.isNetworkKey then
                lblSerialNo.caption := lblSerialNo.caption + ' (Network key)';
        end;

        if assigned(lblComments) then
            lblComments.caption := 'Key version: ' + HASPProgramName + KeyVersion; //KeyVersionStr + ProgramName + KeyVersion;

        if assigned(lblExpiryDate) then
            lblExpiryDate.caption := sLicenseOrTrialExpiryDate;   {will be blank if an ordinary key}
    except on e: Exception do
        eventLog.logException(log, 'Exception on updating labels ' + e.ToString);
    end;
end;

function THASPSecurity.HASPFormWhileRunningChecks: boolean;
var
    forceShowReturnMessage: Boolean;
    returnMessage: string;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'HASPFormWhileRunningChecks');

    returnMessage := '';
    {$IF Defined(LITE)}
    exit(true);
    {$ENDIF}
    //exit(true); //for testing on release

    result := false;

    try
        THASPAPICaller.checkHaspKey(result,
                                    self.HASPshandle,
                                    returnMessage,
                                    forceShowReturnMessage,
                                    HASPProgramName,
                                    keyVersion,
                                    serialNo,
                                    netHaspType,
                                    haspID,
                                    sLicenseOrTrialExpiryDate);
    except on e: Exception do 
        eventLog.logException(log, 'Exception on check hasp key while running ' + e.ToString);
    end;

    try
        if result and (THASPSecurity.VersionAsInteger(string(KeyVersion)) < THASPSecurity.VersionAsInteger(majorVersion(application.ExeName))) then
            if (TMaintenanceStringParser.maxVersionForKey() < THASPSecurity.VersionAsInteger(majorVersion(application.ExeName))) then
                result := false;
    except on e: Exception do 
        eventLog.logException(log, 'Exception on checking hasp key version while running ' + e.ToString);
    end;
end;

procedure THASPSecurity.HASPFormOnShowChecks(const lblSerialNo, lblComments, lblExpiryDate: TLabel; out returnMessage : string);
var
    forceShowMessages: Boolean;
    log: ISynLog;
    haveTrialLicense: Boolean;
    waitCount: integer;
    logViewer: ISmart<TEventLogViewerForm>;
    regLock: TRegistryLock;
    hardwareKeyOK: boolean;
begin
    log := TSynLogLevs.Enter(self,'HASPFormOnShowChecks');
    eventLog.log(log,'Check HASP key and licences');

    {check if registry has locked flag}
    try
        eventLog.log(log, 'Check registry lock');
        regLock := reg.CheckRegistryLock;
        case regLock of
            kNoPreviousDateKey: begin
            showmessage('License error 1011');
            application.Terminate;
            end;
            kCannotDecodeDate: begin
            showmessage('License error 1012');
            application.Terminate;
            end;
            kClockTurnedBack: begin
            showmessage('License error 1013');
            application.Terminate;
            end;
        end;
    except on e: Exception do
        eventLog.logException(log, 'Exception on check registry lock ' + e.ToString);
    end;

    returnMessage := '';
    keypresent := false;

    try
        eventLog.log(log, 'Call check HASP key');
        THASPAPICaller.checkHaspKey(Keypresent,
                                    self.HASPshandle,
                                    returnMessage,
                                    forceShowMessages,
                                    HASPProgramName,
                                    keyVersion,
                                    serialNo,
                                    netHaspType,
                                    haspID,
                                    sLicenseOrTrialExpiryDate);
    except on e: Exception do
        eventLog.logException(log, 'Exception on call check HASP key ' + e.ToString);
    end;
    eventLog.log(log, 'HASP details: ' + HASPProgramName + ' ' + keyVersion + ' ' + serialNo + ' ' + inttostr(haspID) + ' ' + sLicenseOrTrialExpiryDate);
    
    try
        hardwareKeyOK := hardwarekeyPresentForCurrentVersion(); 
    except on e: Exception do
    begin
        hardwareKeyOK := false;
        eventLog.logException(log, 'Exception on hardware key present for current version ' + e.ToString);
    end;
    end;
    
    If keypresent and not hardwareKeyOK then
    begin
        //checking for license server updates to key info

        try
            eventLog.log(log,'Check registry for updates to key license');
            //check against data currently in reg
            if (TMaintenanceStringParser.maxVersionForKey() < THASPSecurity.VersionAsInteger(majorVersion(application.ExeName))) then
            begin
                //no existing license data in reg makes this version valid.

                //are we querying the license server?
                if licenseDBCheckStatus in [kNoInternet,kFinished] then
                begin
                    returnMessage := returnMessage + #13#10 + '1342: Key does not match dBSea version';
                    Keypresent := false;
                end
                else
                begin
                    eventLog.log(log, 'Check server for updates to key license');
                    waitCount := 0;
                    logViewer := TSmart<TEventLogViewerForm>.create(TEventLogViewerForm.create(Application));
                    logViewer.Width := 500;
                    logViewer.Height := 350;
                    logViewer.btSaveLog.Visible := false;
                    logViewer.btOK.Visible := false;
                    logViewer.mmLog.Height := logViewer.ClientHeight;

                    try
                        eventLog.log(log, 'Waiting for key server to return');
                        //wait for server check to return
                        while licenseDBCheckStatus = kInProgress do
                        begin
                            if waitCount = 0 then
                                logViewer.Show;
                            if waitCount mod 10 = 0 then
                            begin
                                eventLog.appendToLast('.');
                                logViewer.setText(eventLog);
                                Application.ProcessMessages;
                            end;
                            sleep(50);
                            inc(waitCount);
                        end;
                    except on e: Exception do
                        eventLog.logException(log, 'Exception on waiting for key server to return ' + e.ToString);
                    end;

                    //server check has returned
                    try
                        eventLog.log('Parse key server response');
                        if (TMaintenanceStringParser.maxVersionForKey() < THASPSecurity.VersionAsInteger(majorVersion(application.ExeName))) then
                        begin
                            returnMessage := returnMessage + #13#10 + '1342: Key does not match dBSea version';
                            Keypresent := false;
                        end;
                    except on e: Exception do
                        eventLog.logException(log, 'Exception on parse key server response ' + e.ToString);
                    end;                
                end;
            end;
        except on e: Exception do
            eventLog.logException(log, 'Exception on check registry for updates to key license ' + e.ToString);
        end;
    end;

    if forceShowMessages and (length(returnMessage) > 0) then
        showmessage(returnMessage);

    self.updateAboutBoxCaptions(lblSerialNo, lblComments, lblExpiryDate);

    if keypresent then
        runningMode := rmNormalKey
    else
    begin
        eventLog.log('No valid HASP key found');

        {check for valid trial code in registry. if not found, prompt for
        trial license file. if not successful, quit.}
        runningMode := rmDemo;

        try
            eventLog.log('Check for trial license');
            reg.CheckForTrialLicense(sLicenseOrTrialExpiryDate,haveTrialLicense);
        except on e: Exception do
            eventLog.logException(log, 'Exception on check for trial license ' + e.ToString);
        end;

        if haveTrialLicense then
        begin
            eventLog.log('Have trial license ' + sLicenseOrTrialExpiryDate);
            returnMessage := '';
            runningMode := rmTrial;
        end
        else
        begin
            eventLog.log('No trial license found');
            if MessageDlg(returnMessage + #13#10 + 'Would you like to import a trial license file?' +
                           #13#10 + 'Without a trial license, dBSea will run in demo mode with some reduced functionality',
                       TMsgDlgType.mtConfirmation,
                       mbYesNo, 0) = mrYes then
            begin
                try
                    importLicenseFile;
                except on e: Exception do
                    eventLog.logException(log, 'Exception on check for trial license ' + e.ToString);
                end;
                
                try
                    reg.CheckForTrialLicense(sLicenseOrTrialExpiryDate,haveTrialLicense);
                    if haveTrialLicense then
                        runningMode := rmTrial;
                except on e: Exception do
                    eventLog.logException(log, 'Exception on check for trial license in registry ' + e.ToString);
                end;
            end;
            returnMessage := '';
        end;

        {no key found, so trial or demo}

        if runningMode = rmTrial then
            self.trialModeChecks;
    end;

    self.hasPerformedInitialCheck := true;
end;

procedure THASPSecurity.trialModeChecks();
var 
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'trialModeChecks');

    try
        {for trial, check that the clock hasn't been rewound. Do nothing for demo mode - the program is basically unusable for work}
        if not reg.CheckPreviousOpenDate then
        begin
            showmessage('License error 109');
            application.Terminate;
        end;
    except on e: Exception do
        eventLog.logException(log, 'Exception on check previous open date ' + e.ToString);
    end;

    try
        if not CheckYear then
        begin
            showmessage('License error 131: trial year check');
            application.Terminate;
        end;
    except on e: Exception do
        eventLog.logException(log,'Exception on check year ' + e.ToString);
    end;
end;

class function THASPSecurity.VersionAsInteger(s : string) : integer;
begin
    if length(s) <> 3 then exit(0);
    result := 10*strtoint(system.sysutils.trim(s[1])) + strtoint(system.sysutils.trim(s[3]));
end;

function  THASPSecurity.CheckYear: boolean;
var
    year, month, day: Word;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self,'CheckYear');

    DecodeDate(date, year, month, day);
    result := (year = yearForThisTrialVersion) or (year = yearForThisTrialVersion - 1);

    if not result then
        checkCalendar; //so non-gregorian calendars get a message about why it fails
end;

function THASPSecurity.allowDBSecurityIndex(const index: integer): boolean;
begin
    if index < 0 then exit(true);

    //TODO
    result := false;
end;

function THASPSecurity.c2v: string;
begin
    result := THASPAPICaller.generateC2V(self.HASPshandle);
end;

procedure THASPSecurity.checkCalendar;
var
    OutputBuffer: PChar; // holds local info
    SelectedLCID: LCID; // holds the selected LCID
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'Check calendar');

    {allocate memory for the string}
    OutputBuffer := StrAlloc(255);

    {get the native language}
    SelectedLCID := GetUserDefaultLCID;

    GetLocaleInfo(SelectedLCID, LOCALE_ICALENDARTYPE, OutputBuffer, 255);
    if not toint(OutputBuffer) in [1,2,10,11,12] then
        showmessage('1170: System calendar is not Gregorian, please contact your distributor');

//          Case "1":  Text9.Text = "1 Gregorian (localized) "
//      Case "2":  Text9.Text = "2 Gregorian (English strings always)"
//      Case "3":  Text9.Text = "3 Era: Year of the Emperor (Japan)"
//      Case "4":  Text9.Text = "4 Era: Year of Taiwan Region"
//      Case "5":  Text9.Text = "5 Tangun Era(Korea)"
//      Case "6":  Text9.Text = "6 Hijri (Arabic lunar)"
//      Case "7":  Text9.Text = "7 Thai"
//      Case "8":  Text9.Text = "8 Hebrew (Lunar)"
//      Case "9":  Text9.Text = "9 Gregorian Middle East French"
//      Case "10": Text9.Text = "10 Gregorian Arabic calendar"
//      Case "11": Text9.Text = "11 Gregorian Transliterated English"
//      Case "12": Text9.Text = "12 Gregorian Transliterated French"

    {dispose of the string memory}
    StrDispose(OutputBuffer);
end;

function  THASPSecurity.isNetworkKey(): boolean;
begin
    result := self.netHaspType = 10;
end;

function THASPSecurity.keySerialNumber: string;
var
    keyPresent, forceReturnMessage: boolean;
    returnMessage: String;
    HaspProgramName: String;
    keyVersion: String;
    netHaspType: integer;
    haspId: integer;
    sLicenseOrTrialExpiryDate: string;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'keySerialNumber');

    try
        THASPAPICaller.checkHaspKey(Keypresent,
                                self.HASPshandle,
                                returnMessage,
                                forceReturnMessage,
                                HASPProgramName,
                                keyVersion,
                                result,
                                netHaspType,
                                haspID,
                                sLicenseOrTrialExpiryDate);
    except on e: Exception do
        eventLog.logException(log,'Exception on check hasp key for serial number ' + e.ToString);
    end;
end;

procedure THASPSecurity.logout;
begin
    THASPAPICaller.keyLogout(HASPshandle);
end;

procedure THASPSecurity.ImportLicenseFile;
var
    sDecodedLicenceDate: String;
    odTrialFile: ISmart<TOpenDialog>;
    myFile: TextFile;
    sFileFirstLine: String;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'ImportLicenseFile');

    odTrialFile := TSmart<TOpenDialog>.create( TOpenDialog.Create(application));
    odTrialFile.Options := [ofFileMustExist];
    odTrialFile.Filter := 'All files|*.*|TXT files|*.txt';
    odTrialFile.FilterIndex := 0;
    if not odTrialFile.Execute then Exit;

    if not FileExists(odTrialFile.filename) then
    begin
        Showmessage('1158: File does not exist');
        Exit;
    end;

    if THelper.IsFileInUse(odTrialFile.filename) then
    begin
        Showmessage('1159: File could not be opened as it is in use by another process');
        Exit;
    end;

    try
        AssignFile(myFile, odTrialFile.filename);
        Reset(myFile);
        ReadLn(myFile, sFileFirstLine);
        CloseFile(myFile);
    except on e: Exception do
        eventLog.logException(log, 'Exception on read trial file ' + e.ToString);
    end;

    try
        sDecodedLicenceDate := TPasswordChecking.DecodePWDEx(sFileFirstLine, SecurityString);
        if not isFloat(sDecodedLicenceDate) then
        begin
            ShowMessage('1160: License file could not be decoded');
            Exit;
        end;
    except on e: Exception do
        eventLog.logException(log, 'Exception on decode trial file ' + e.ToString);
    end;

    try
        reg.writeLicenseFile(sFileFirstLine);
    except on e: Exception do
        eventLog.logException(log, 'Exception on write license details to registry ' + e.ToString);
    end;

    showmessage('Trial license imported. Expiry date: ' + FormatDateTime('yyyy-mm-dd',FloatToDateTime(strtofloat(sDecodedLicenceDate))));
end;


function THASPSecurity.exportChecks(): boolean;
begin
    {$IF Defined(LITE)}
    exit(true);
    {$ENDIF}

    if runningMode = rmDemo then
    begin
        showmessage('Demo version. Export is disabled');
        exit(false);
    end;

    {$IFNDEF DEBUG}
    if runningMode = rmTrial then
    begin
        showmessage('Trial version. Export is disabled');
        exit(false);
    end;
    {$ENDIF}

    {$IFNDEF DEBUG}
    if (runningMode = rmNormalKey) and not HASPFormWhileRunningChecks then
    begin
        showmessage('Valid HASP key not found, export disabled');
        exit(false);
    end;
    {$ENDIF}

    result := true;
end;

function THASPSecurity.hardwarekeyPresentForCurrentVersion: boolean;
begin
    result := keypresent and (THASPSecurity.VersionAsInteger(string(KeyVersion)) >= THASPSecurity.VersionAsInteger(majorVersion(application.ExeName)));
end;

initialization
    theHASPSecurity := THASPSecurity.create();
finalization
    theHASPSecurity.logout;
    theHASPSecurity.free;

end.
