(******************************************************************************
*
* Sentinel LDK Licensing API Interface for Embarcadero Delphi
*
* Copyright (C) 2014, SafeNet, Inc. All rights reserved.
*
* Include file for defining the vendor specific OBJ. 
*******************************************************************************)

{$IFDEF WIN32}
  //{$L hasp_windows_delphi_coff_demo.obj}
  {$L hasp_windows_delphi_68817.obj}
{$ENDIF}

{$IFDEF WIN64}
   //{$L hasp_windows_delphi_coff_x64_demo.obj}
   {$L hasp_windows_delphi_coff_x64_68817.obj}
{$ENDIF}
