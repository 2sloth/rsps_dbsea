unit HASPAPICalls;

{$B-}

interface

uses syncommons,
    system.SysUtils, system.Classes, system.StrUtils,
    vcl.forms,
    xml.XMLIntf, xml.XMLDoc,
    eventlogging, Hasp_unit, helperFunctions,
    SmartPointer;

type

    THASPAPICaller = class(TObject)
    private
     {$INCLUDE 'hasp/hasp_vcode_yryke.inc'}
        class procedure CheckForTimeKey(var HASPshandle : hasp_handle_t;
                                        out Keypresent: Boolean;
                                        out returnMessage : string;
                                        out forceShowReturnMessage: boolean;
                                        out sLicenseOrTrialExpiryDate : String);
    public
        class procedure CheckHaspKey(out Keypresent: Boolean;
                                     var HASPshandle      : hasp_handle_t;
                                     out returnMessage    : string;
                                     out forceShowReturnMessage: boolean;
                                     out HASPProgramName  : String;
                                     out keyVersion       : String;
                                     out serialNo         : String;
                                     out netHaspType      : integer;
                                     out haspID           : integer;
                                     out sLicenseOrTrialExpiryDate : String);
        class procedure keyLogout(const HASPshandle: hasp_handle_t);
        class function  generateC2V(const HASPshandle: hasp_handle_t): string;
    end;

implementation


class procedure THASPAPICaller.CheckHaspKey(out Keypresent          : Boolean;
                                            var HASPshandle      : hasp_handle_t;
                                            out returnMessage       : string;
                                            out forceShowReturnMessage: boolean;
                                            out HASPProgramName     : string;
                                            out keyVersion          : String;
                                            out serialNo            : String;
                                            out netHaspType         : integer;
                                            out haspID              : integer;
                                            out sLicenseOrTrialExpiryDate : String);
type
    TMemBuf = array [0 .. 11] of ANSIchar; //We are currently storing 12 bytes in the mem. could do a lot more if needed
const
    ProKeyFeatureID = 4;
    NetworkKeyFeatureID = 5;
    NetworkHASPKeyType = 10;
var
    HASPReturn      : hasp_status_t;
    membuffer       : TMemBuf;
    HaspInfo        : PansiChar;
    XMLDocumentHASP : IXMLDocument;
    KeyType         : IXMLNode;
    log             : ISynLog;

    function setAndFillString(const start, len : integer; const buffer : TMemBuf) : string;
    var
        i : integer;
    begin
        result := '';
        for I := 0 to len - 1 do
            result := result + char(buffer[start + i]);//Copy(buffer, start, len);
    end;
    procedure forceReturnMessge(const s: string);
    begin
        forceShowReturnMessage := true;
        returnMessage := s;
    end;
begin
    log := TSynLogLevs.Enter(self,'CheckHaspKey');

    try
        eventLog.log(log,'Checking for local HASP key');
        HASPReturn := Hasp_login(ProKeyFeatureID, @vendor_code[1], HASPshandle); { ProKeyFeatureID = dBSea standard feature number on the Hasp key, defined in Business Studio }
    except on e: Exception do
    begin
        HASPReturn := HASP_FEATURE_NOT_FOUND;
        eventLog.logException(log,'Exception on checking for local HASP key ' + e.ToString);
    end;
    end;

    if HASPReturn = HASP_FEATURE_NOT_FOUND then //check for network key
    begin
        try
            eventLog.log(log,'Checking for network HASP key');
            HASPReturn := Hasp_login(NetworkKeyFeatureID, @vendor_code[1], HASPshandle);
        except on e: Exception do
            eventLog.logException(log,'Exception on checking for network HASP key ' + e.ToString);
        end;
    end;

    eventLog.log(log,'Hasp return ' + inttostr(Ord(HASPReturn)) + ' ' + ifthen(haspreturn = HASP_STATUS_OK,'(success)',''));

    Keypresent := false;
    forceShowReturnMessage := false;
    case HASPReturn of
        HASP_STATUS_OK              : Keypresent := true;
        HASP_HASP_NOT_FOUND         : returnMessage := 'No Hasp key found';
        HASP_INV_VCODE              : forceReturnMessge('2414: Invalid vendor code');
        HASP_TS_DETECTED            : forceReturnMessge('2415: Terminal services (remote terminal) detected');
        HASP_FEATURE_NOT_FOUND      : THASPAPICaller.CheckForTimeKey(HASPshandle,Keypresent,returnMessage,forceShowReturnMessage,sLicenseOrTrialExpiryDate); {ShowMessage('Hasp key does not contain a licence for this program')}
        Hasp_No_driver              : forceReturnMessge('2416: No driver for Hasp key');
        Hasp_No_Vlib                : forceReturnMessge('2417: No vendor Library');
        Hasp_Old_Driver             : forceReturnMessge('2418: Old Driver');
        Hasp_Too_Many_Users         : forceReturnMessge('Too many users for this key');
        Hasp_Old_LM                 : forceReturnMessge('2420: Old Licence Manager');
        hasp_Device_Err             : forceReturnMessge('2421: USB error');
        HASP_UNKNOWN_VCODE          : forceReturnMessge('2422: Vendor Code not recognized by API');
    else
        returnMessage := 'Hasp HL error code ' + IntToStr(HASPReturn);
    end;

    if not Keypresent then exit;

    try
        eventLog.log(log, 'Read HASP key memory');
        HASPReturn := Hasp_read(HASPshandle, HASP_FILEID_RW, 0, length(membuffer), membuffer);
    except on e: Exception do
        eventLog.logException(log, 'Exception on read HASP key memory ' + e.ToString);
    end;

    case HASPReturn of
        HASP_STATUS_OK : ; //Request was successfully completed
        HASP_HASP_NOT_FOUND : ; // Sentinel HASP protection key is no longer available
        HASP_INV_HND : ; // Invalid input handle
        HASP_INV_FORMAT : ; // Unrecognized format
        HASP_INSUF_MEM : ; // Out of memory
        HASP_BROKEN_SESSION : ; // Session has been interrupted
        HASP_LOCAL_COMM_ERR : ; // Communication error occurred between the application and the local HASP License Manager
        HASP_REMOTE_COMM_ERR : ; // Communication error occurred between the local and remote HASP License Managers
        HASP_DEVICE_ERR : ; // Input/output error in HASP SL secure storage, OR, in thecase of a HASP HL key, USB communication error
        HASP_TIME_ERR : ; // System time has been tampered with
    end;

    try
        HASPProgramName := setAndFillString(0, 5, membuffer);
        KeyVersion := setAndFillString(5, 3, membuffer);
        SerialNo := setAndFillString(8, 4, membuffer);
    except on e: Exception do
        eventLog.logException(log, 'Exception on fill strings from memory ' + e.ToString);
    end;

    try
        eventLog.log(log, 'HASP get session info');
        HASPReturn := Hasp_Get_SessionInfo(HASPshandle, PAnsiChar(HASP_KEYINFO), HaspInfo);
    except on e: Exception do
        eventLog.logException(log, 'Exception on HASP get session info ' + e.ToString);
    end;

    case HASPReturn of
        HASP_STATUS_OK : ; //Request was successfully completed
        HASP_HASP_NOT_FOUND : ; // Sentinel HASP protection key is no longer available
        HASP_INV_HND : ; // Invalid input handle
        HASP_INV_FORMAT : ; // Unrecognized format
        HASP_INSUF_MEM : ; // Out of memory
        HASP_BROKEN_SESSION : ; // Session has been interrupted
        HASP_LOCAL_COMM_ERR : ; // Communication error occurred between the application and the local HASP License Manager
        HASP_REMOTE_COMM_ERR : ; // Communication error occurred between the local and remote HASP License Managers
        HASP_DEVICE_ERR : ; // Input/output error in HASP SL secure storage, OR, in thecase of a HASP HL key, USB communication error
        HASP_TIME_ERR : ; // System time has been tampered with
    end;

    if HASPReturn <> HASP_STATUS_OK then exit;

    XMLDocumentHASP := nil;
    try
        try
            XMLDocumentHASP := TXMLDocument.Create(application); //owner of TXMLDocument should not be nil!
        except on e: Exception do
            eventLog.logException(log, 'Exception on create XML doc ' + e.ToString);
        end;

        try
            XMLDocumentHASP.xml.Text := string(haspinfo);
        except on e: Exception do
            eventLog.logException(log, 'Exception on set XML doc text ' + e.ToString);
        end;

        try
            XMLDocumentHASP.Active := true;
        except on e: Exception do
            eventLog.logException(log, 'Exception on set XML doc active ' + e.ToString);
        end;

        try
            KeyType := XMLDocumentHASP.DocumentElement.ChildNodes['keyspec'].ChildNodes['hasp'];
        except on e: Exception do
            eventLog.logException(log, 'Exception on XML get keytype ' + e.ToString);
        end;

        NetHaspType := -1;
        if assigned(KeyType) then
        begin
            try
                HaspID := strtoint(trim(KeyType.ChildNodes['haspid'].Text));
            except on e: Exception do
                eventLog.logException(log, 'Exception on get HaspID ' + e.ToString);
            end;

            eventLog.log('HASP key ID: ' + inttostr(haspID));

            try
                NetHaspType := strtoint(trim(KeyType.ChildNodes['nethasptype'].Text));
            except on e: Exception do
                eventLog.logException(log, 'Exception on get net hasp type param ' + e.ToString);
            end;
        end;

        if netHaspType = NetworkHASPKeyType then
            eventLog.log(log, 'Network HASP key found');

    except on E:exception do
        eventLog.logException(log, 'Exception on main XML block ' + e.ToString);
    end;
end;

class function THASPAPICaller.generateC2V(const HASPshandle: hasp_handle_t): string;
var
    status: hasp_status_t;
    format: AnsiString;
    info : PAnsiChar;
    //infoOut: AnsiString;
    //s: string;
begin
    result := '';
    format := AnsiString('<haspformat format="updateinfo"/>');
    try
        status := hasp_get_sessioninfo(HASPshandle, @format[1], info);
        if status <> HASP_STATUS_OK then exit('');
        result := string(info);
    except
        result := '';
    end;
end;

class procedure THASPAPICaller.keyLogout(const HASPshandle: hasp_handle_t);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'keyLogout');

    try
        hasp_logout(HASPshandle);
    except on e: Exception do
        eventLog.logException(log, 'Exception on HASP logout ' + e.ToString);
    end;
end;

class procedure THASPAPICaller.CheckForTimeKey(var HASPshandle : hasp_handle_t;
                                               out Keypresent: Boolean;
                                               out returnMessage : string;
                                               out forceShowReturnMessage: boolean;
                                               out sLicenseOrTrialExpiryDate : String);
{This procedure is for time limited licences}
const
    nulldate: TDate = -693594;
var
    HASPReturn: hasp_status_t;
    HASPtime: HASP_TIME_T;
    day, month, year, hour, minute, second: cardinal;
    fsize: HASP_SIZE_T;
    ExpireDay, ExpireMonth, ExpireYear: string;
    ExpiryDate, KeyDate: extended;
    membuffer: array [0 .. 11] of ANSIchar; { was 11 }
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'CheckForTimeKey');

    try
        eventLog.log(log, 'Check for time HASP key');
        HASPReturn := Hasp_login(15, @vendor_code[1], HASPshandle); { dBSea for time keys is program or feature number 15 on the Hasp key - define in business studio }
    except on e: Exception do
        eventLog.logException(log, 'Exception on check for time HASP key ' + e.ToString);
    end;

    Keypresent := false;
    {first check if key is a time key}
    case HASPReturn of
        HASP_STATUS_OK: Keypresent := true;
        HASP_FEATURE_NOT_FOUND: begin
            forceShowReturnMessage := true;
            returnMessage := '1121: Hasp key does not contain a licence for this program';
        end;
    end;

    if not Keypresent then exit;

    try
        eventLog.log(log, 'HASP get rtc');
        HASPReturn := hasp_get_rtc(HASPshandle,HASPtime);
    except on e: Exception do
        eventLog.logException(log, 'Exception on HASP get rtc ' + e.ToString);
    end;

    sLicenseOrTrialExpiryDate := '';
    {if a time key then read time/date and set a TDate variable for real time and set expiry date}
    if HASPReturn <> HASP_STATUS_OK then exit;

    try
        eventLog.log(log, 'HASP get hasp time');
        HASPReturn := hasp_hasptime_to_datetime(HASPtime,day, month, year,hour, minute, second);
    except on e: Exception do
        eventLog.logException(log, 'Exception on HASP get hasp time ' + e.ToString);
    end;
    THelper.RemoveWarning(HASPReturn);
    fsize := 8;

    try
        eventLog.log(log, 'HASP read memory');
        HASPReturn := Hasp_read(HASPshandle, HASP_FILEID_RW, 64, fsize, membuffer);
    except on e: Exception do
        eventLog.logException(log, 'Exception on HASP read memory ' + e.ToString);
    end;
    THelper.RemoveWarning(HASPReturn);

    try
        ExpireDay := string((Copy(membuffer, 1, 2)));
        ExpireMonth := string((Copy(membuffer, 3, 2)));
        ExpireYear := string((Copy(membuffer, 5, 4)));
    except on e: Exception do
        begin
            eventLog.logException(log, 'Exception on copy HASP memory ' + e.ToString);
            ExpireDay := '';
            ExpireMonth := '';
            ExpireYear := '';
        end;
    end;
    FormatSettings.DateSeparator := '-';

    try
        ExpiryDate := StrToDate(ExpireDay + '-' + ExpireMonth + '-' + ExpireYear,FormatSettings);
    except on e: Exception do
        begin
            eventLog.logException(log, 'Exception on parse HASP memory date ' + e.ToString);
            ExpiryDate := nullDate;
            Keypresent := false;
            forceShowReturnMessage := true;
            returnMessage := '1123: Could not decode expiry date from HASP memory';
            eventLog.log(log, '1123: Could not decode expiry date from HASP memory');
        end;
    end;

    try
        KeyDate := StrToDate(IntToStr(Day) + '-' + IntToStr(Month) + '-' + IntToStr(Year),FormatSettings);
    except on e: exception do
        begin
            eventLog.logException(log, 'Exception on parse HASP key date ' + e.ToString);
            KeyDate := nulldate;
            Keypresent := false;
            forceShowReturnMessage := true;
            returnMessage := '1125: Could not decode HASP key date';
            eventLog.log(log, '1125: Could not decode HASP key date');
        end;
    end;
    sLicenseOrTrialExpiryDate := 'HASP licence expires in ' + FloatToStr(ExpiryDate - KeyDate) + ' days';

    if KeyDate > ExpiryDate then
    begin
        eventLog.log(log, '1124: HASP license has expired');
        Keypresent := false;
        forceShowReturnMessage := true;
        returnMessage := '1124: HASP license has expired';
    end;
end;

end.
