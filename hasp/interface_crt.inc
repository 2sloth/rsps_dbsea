(******************************************************************************
*
* Sentinel LDK Licensing API Interface for Embarcadero Delphi
*
* Copyright (C) 2014, SafeNet, Inc. All rights reserved.
*
* Include file for defining the functions of MSVCRT.dll 
*******************************************************************************)
{$IFDEF WIN32}
FUNCTION _strncat(strDest:PANSICHAR;const strSource:PANSICHAR;count:SIZE_T):PANSICHAR;cdecl;external 'msvcrt.dll' name  'strncat';
FUNCTION _mktime(timeptr :Pointer ):integer; stdcall;external 'msvcrt.dll' name  'mktime';
PROCEDURE __exit( status :integer);stdcall; external 'msvcrt.dll' name  '_exit';
FUNCTION _rand : integer;stdcall; external 'msvcrt.dll' name  'rand';
FUNCTION _strftime(strDest:PANSICHAR;maxsize:SIZE_T;const format:PANSICHAR;const timeptr:pointer):size_t;stdcall; external 'msvcrt.dll' name  'strftime';
PROCEDURE _exit( status :integer); stdcall;external 'msvcrt.dll' name  '_exit';
PROCEDURE exit( status :integer); stdcall;external 'msvcrt.dll' name  '_exit';
FUNCTION _ftell(stream :pointer):LONG;stdcall;external 'msvcrt.dll' name  'ftell';
FUNCTION __localtime64(const timer:pointer):pointer; stdcall;external 'msvcrt.dll' name  '_localtime64';
FUNCTION _strtok(strToken:PANSICHAR;const strDelimit :PANSICHAR):PANSICHAR;stdcall; external 'msvcrt.dll' name  'strtok';
FUNCTION _wrmdir(const dirname:PCHAR):integer;stdcall;external 'msvcrt.dll' name  '_wrmdir';
FUNCTION _strspn(const str:PCHAR ;const strCharSet :PCHAR):size_t;stdcall;external 'msvcrt.dll' name  'strspn';
FUNCTION _fread(buffer:PVOID;size:size_t;count:size_t;stream :pointer):size_t;stdcall;external 'msvcrt.dll' name  'fread';
FUNCTION __wstat(const path:PCHAR;buffer:pointer):integer;stdcall; external 'msvcrt.dll' name  '_wstat';
FUNCTION _fwrite( const buffer:pointer;  size:size_t; count:size_t;stream :pointer ):size_t;stdcall;external 'msvcrt.dll' name  'fwrite';
FUNCTION __wfindfirst(const filespec:PCHAR;fileinfo : pointer):PINT;stdcall;external 'msvcrt.dll' name  '_wfindfirst';
FUNCTION __read(handle:integer; buffer:pointer;count:UINT ):integer;stdcall;external 'msvcrt.dll' name  '_read';
FUNCTION __wfopen(const filename:PCHAR;const mode:PCHAR):pointer; stdcall;external 'msvcrt.dll' name  '_wfopen';
FUNCTION __findclose(handle:PINT):integer;stdcall;external 'msvcrt.dll' name  '_findclose';
FUNCTION __gmtime64(const timer:pointer):pointer;stdcall;external 'msvcrt.dll' name  '_gmtime64';
PROCEDURE _srand(seed:UINT);stdcall;external 'msvcrt.dll' name  'srand';
FUNCTION __wrename(const oldname:PCHAR;const newname:PCHAR):integer;stdcall;external 'msvcrt.dll' name  '_wrename';
FUNCTION _fopen(const filename:PANSICHAR;const mode:PANSICHAR):pointer; stdcall;external 'msvcrt.dll' name  'fopen';
FUNCTION __wremove(const path:PCHAR):integer; stdcall;external 'msvcrt.dll' name  '_wremove';
FUNCTION _fflush(stream:pointer):integer;stdcall;external 'msvcrt.dll' name 'fflush';
FUNCTION __lseek(handle:integer;offset:LONG;origin:integer ):LONG; stdcall;external 'msvcrt.dll' name '_lseek';
FUNCTION _fseek(stream :pointer;offset:LONG;origin:integer):integer; stdcall; external 'msvcrt.dll' name 'fseek';
FUNCTION _atoi(const str:PANSICHAR):integer;stdcall;external 'msvcrt.dll' name 'atoi';
FUNCTION _strtoul(const nptr:PANSICHAR;var endptr:PANSICHAR;base:integer):ULONG;stdcall;external 'msvcrt.dll' name 'strtoul';
FUNCTION _open(const filename:PANSICHAR;oflag:integer):integer; stdcall;external 'msvcrt.dll' name '_open';
FUNCTION __snprintf(buffer:PANSICHAR;count:size_t;const format :PANSICHAR):integer;stdcall;external 'msvcrt.dll' name '_snprintf';varargs;
FUNCTION __beginthreadex( security:pointer;stack_size:UINT;start_address:pointer;arglist:pointer;initflag:UINT;thrdaddr:pointer):pointer;stdcall;external 'msvcrt.dll' name '_beginthreadex';
FUNCTION __wopen(const filename:PCHAR;oflag:integer):integer;stdcall;external 'msvcrt.dll' name '_wopen';
FUNCTION __close(fd :integer):integer;stdcall;external 'msvcrt.dll' name '_close';
FUNCTION __write(fd:integer;const buffer:pointer;count:UINT):integer;stdcall;external 'msvcrt.dll' name '_write';
FUNCTION _fclose(stream:pointer):integer;stdcall; external 'msvcrt.dll' name 'fclose';
FUNCTION __chsize(fd:integer;size:LONG):integer;stdcall;external 'msvcrt.dll' name '_chsize';
FUNCTION _time(timer:pointer):integer; stdcall;external 'msvcrt.dll' name 'time';
FUNCTION __wmkdir(dirname:PCHAR):integer;stdcall;external 'msvcrt.dll' name '_wmkdir';
FUNCTION __mkdir(const dirname:PCHAR):integer;stdcall;external 'msvcrt.dll' name '_mkdir';
FUNCTION __waccess(const dirname:PCHAR;mode:integer):integer;stdcall;external 'msvcrt.dll' name '_waccess';
FUNCTION __wunlink(const filename:PCHAR):integer;stdcall; external 'msvcrt.dll' name '_wunlink';
FUNCTION __wrmdir(const dirname:PCHAR):integer; stdcall;external 'msvcrt.dll' name '_wrmdir';
FUNCTION __wfindnext(handle:pointer;fileinfo:pointer):integer; stdcall;external 'msvcrt.dll' name '_wfindnext';
FUNCTION _abs(n:integer):integer;stdcall;external 'msvcrt.dll' name 'abs';
FUNCTION   __errno():integer;external 'msvcrt.dll' name '_errno';
function  _util_strtrim(psz:PAnsiChar;pstrTrim:PAnsiChar):BOOL;stdcall;external 'Shlwapi.dll' name 'StrTrimA';

Procedure   __chkstk;stdcall; external 'ntdll.dll' name '_chkstk';
Procedure   __aullrem;stdcall; external 'ntdll.dll' name '_aullrem';
Procedure   __aulldiv;stdcall; external 'ntdll.dll' name '_aulldiv';
Procedure   __allmul;stdcall; external 'ntdll.dll' name '_allmul';
Procedure   __aullshr;stdcall; external 'ntdll.dll' name '_aullshr';
Procedure   __alldiv;stdcall; external 'ntdll.dll' name '_alldiv';
Procedure   __aulldvrm;stdcall; external 'ntdll.dll' name '_aulldvrm';
Procedure   __alldvrm;stdcall; external 'ntdll.dll' name '_alldvrm';
Procedure   __allrem;stdcall; external 'ntdll.dll' name '_allrem';
Procedure   __allshl;stdcall; external 'ntdll.dll' name '_allshl';
Procedure   __allshr;stdcall; external 'ntdll.dll' name '_allshr';
{$ENDIF}

{$IFDEF WIN64}
FUNCTION strncat(strDest:PANSICHAR;const strSource:PANSICHAR;count:SIZE_T):PANSICHAR;stdcall;external 'msvcrt.dll' name  'strncat';
FUNCTION mktime(timeptr :Pointer ):integer; stdcall;external 'msvcrt.dll' name  'mktime';
PROCEDURE _exit( status :integer);stdcall; external 'msvcrt.dll' name  '_exit';
FUNCTION rand : integer;stdcall; external 'msvcrt.dll' name  'rand';
FUNCTION strftime(strDest:PANSICHAR;maxsize:SIZE_T;const format:PANSICHAR;const timeptr:pointer):size_t;stdcall; external 'msvcrt.dll' name  'strftime';
PROCEDURE exit( status :integer); stdcall;external 'msvcrt.dll' name  '_exit';
FUNCTION ftell(stream :pointer):LONG;stdcall;external 'msvcrt.dll' name  'ftell';
FUNCTION _localtime64(const timer:pointer):pointer; stdcall;external 'msvcrt.dll' name  '_localtime64';
FUNCTION strtok(strToken:PANSICHAR;const strDelimit :PANSICHAR):PANSICHAR;stdcall; external 'msvcrt.dll' name  'strtok';
FUNCTION wrmdir(const dirname:PCHAR):integer;stdcall;external 'msvcrt.dll' name  '_wrmdir';
FUNCTION strspn(const str:PCHAR ;const strCharSet :PCHAR):size_t;stdcall;external 'msvcrt.dll' name  'strspn';
FUNCTION fread(buffer:PVOID;size:size_t;count:size_t;stream :pointer):size_t;stdcall;external 'msvcrt.dll' name  'fread';
FUNCTION _wstat(const path:PCHAR;buffer:pointer):integer;stdcall; external 'msvcrt.dll' name  '_wstat';
FUNCTION fwrite( const buffer:pointer;  size:size_t; count:size_t;stream :pointer ):size_t;stdcall;external 'msvcrt.dll' name  'fwrite';
FUNCTION _wfindfirst(const filespec:PCHAR;fileinfo : pointer):PINT;stdcall;external 'msvcrt.dll' name  '_wfindfirst';
FUNCTION _wfindfirst64(const filespec:PCHAR;fileinfo : pointer):PINT;stdcall;external 'msvcrt.dll' name  '_wfindfirst64';
FUNCTION _read(handle:integer; buffer:pointer;count:UINT ):integer;stdcall;external 'msvcrt.dll' name  '_read';
FUNCTION _wfopen(const filename:PCHAR;const mode:PCHAR):pointer; stdcall;external 'msvcrt.dll' name  '_wfopen';
FUNCTION _findclose(handle:PINT):integer;stdcall;external 'msvcrt.dll' name  '_findclose';
FUNCTION _gmtime64(const timer:pointer):pointer;stdcall;external 'msvcrt.dll' name  '_gmtime64';
PROCEDURE srand(seed:UINT);stdcall;external 'msvcrt.dll' name  'srand';
FUNCTION _wrename(const oldname:PCHAR;const newname:PCHAR):integer;stdcall;external 'msvcrt.dll' name  '_wrename';
FUNCTION fopen(const filename:PANSICHAR;const mode:PANSICHAR):pointer; stdcall;external 'msvcrt.dll' name  'fopen';
FUNCTION _wremove(const path:PCHAR):integer; stdcall;external 'msvcrt.dll' name  '_wremove';
FUNCTION fflush(stream:pointer):integer;stdcall;external 'msvcrt.dll' name 'fflush';
FUNCTION _lseek(handle:integer;offset:LONG;origin:integer ):LONG; stdcall;external 'msvcrt.dll' name '_lseek';
FUNCTION fseek(stream :pointer;offset:LONG;origin:integer):integer; stdcall; external 'msvcrt.dll' name 'fseek';
FUNCTION atoi(const str:PANSICHAR):integer;stdcall;external 'msvcrt.dll' name 'atoi';
FUNCTION strtoul(const nptr:PANSICHAR;var endptr:PANSICHAR;base:integer):ULONG;stdcall;external 'msvcrt.dll' name 'strtoul';
FUNCTION open(const filename:PANSICHAR;oflag:integer):integer; stdcall;external 'msvcrt.dll' name '_open';
FUNCTION _snprintf(buffer:PANSICHAR;count:size_t;const format :PANSICHAR):integer;stdcall;external 'msvcrt.dll' name '_snprintf';varargs;
FUNCTION _beginthreadex( security:pointer;stack_size:UINT;start_address:pointer;arglist:pointer;initflag:UINT;thrdaddr:pointer):pointer;stdcall;external 'msvcrt.dll' name '_beginthreadex';
FUNCTION _wopen(const filename:PCHAR;oflag:integer):integer;stdcall;external 'msvcrt.dll' name '_wopen';
FUNCTION _close(fd :integer):integer;stdcall;external 'msvcrt.dll' name '_close';
FUNCTION _write(fd:integer;const buffer:pointer;count:UINT):integer;stdcall;external 'msvcrt.dll' name '_write';
FUNCTION fclose(stream:pointer):integer;stdcall; external 'msvcrt.dll' name 'fclose';
FUNCTION _chsize(fd:integer;size:LONG):integer;stdcall;external 'msvcrt.dll' name '_chsize';
FUNCTION time(timer:pointer):integer; stdcall;external 'msvcrt.dll' name 'time';
FUNCTION _wmkdir(dirname:PCHAR):integer;stdcall;external 'msvcrt.dll' name '_wmkdir';
FUNCTION _mkdir(const dirname:PCHAR):integer;stdcall;external 'msvcrt.dll' name '_mkdir';
FUNCTION _waccess(const dirname:PCHAR;mode:integer):integer;stdcall;external 'msvcrt.dll' name '_waccess';
FUNCTION _wunlink(const filename:PCHAR):integer;stdcall; external 'msvcrt.dll' name '_wunlink';
FUNCTION _wrmdir(const dirname:PCHAR):integer; stdcall;external 'msvcrt.dll' name '_wrmdir';
FUNCTION _wfindnext(handle:pointer;fileinfo:pointer):integer; stdcall;external 'msvcrt.dll' name '_wfindnext';
FUNCTION _wfindnext64(handle:pointer;fileinfo:pointer):integer; stdcall;external 'msvcrt.dll' name '_wfindnext64';
FUNCTION abs(n:integer):integer;stdcall;external 'msvcrt.dll' name 'abs';
function  _util_strtrim(psz:PAnsiChar;pstrTrim:PAnsiChar):BOOL;stdcall;external 'Shlwapi.dll' name 'StrTrimA';
FUNCTION  _errno():integer;stdcall;external 'msvcrt.dll' name '_errno';
Procedure   __chkstk;stdcall; external 'ntdll.dll' name '__chkstk';

{$ENDIF}
