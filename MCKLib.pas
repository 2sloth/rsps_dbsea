{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit MCKLib;

interface

uses system.SysUtils, system.Strutils, system.Classes, system.Math,
    system.rtti, system.types,
     winapi.Windows, winapi.psAPI,
     {$IFNDEF HASP_DIAGNOSTIC}
     SQLiteTable3,
     {$ENDIF}
     baseTypes, helperFunctions,
     smartPointer;

type
    TMCKLib = class(TObject)
        class procedure loopSmoothArray(const kernel: TDoubleDynArray; const arr: TDoubleDynArray; const smoothOverNans: boolean);
        class function  pointInPolygon(const Points: TPointArray; X, Y: Integer): boolean; overload;
        class function  pointInPolygon(const Points: TFPointArray; X, Y: double): boolean; overload;
        {$IFNDEF HASP_DIAGNOSTIC}
        class function  checkSQLTable(const aDB: TSQLiteDatabase; const tblName: string; const requiredFields: TStringArray): boolean;
        {$ENDIF}
        class function  linspace(const start, finish: double; const count: Integer): TDoubleDynArray;
        class procedure arrayLoopSmoothNaNs(const A: TDoubleDynArray);
        class function  triangleDistribution(const m: Integer): TDoubleDynArray;

        class function  objectHasMethod(const AClass: TObject; methodName: string): boolean;
        class function  GetMethodAddress(const AClass: TObject; methodName: string): TRttiMethod;

        class function  Distance3D(const x1, x2, y1, y2, z1, z2: double): double; overload;
        class function  Distance2D(const x1, x2, y1, y2: double): double;
        class function  Distance2Points(const Point1, Point2: TPoint): double;

        {$IFDEF MSWINDOWS}
        class function committedStackSize(): NativeUInt;
        {$ENDIF}

    end;

    procedure   forceRange(var val : double; const min,max :double); overload; //force val to be within range
    procedure   forceRange(var val : single; const min,max :single); overload;
    procedure   forceRange(var val : integer; const min,max :integer); overload;

    function    checkRange(const val,min,max :double):boolean; overload; inline;
    function    checkRange(const val,min,max :integer):boolean; overload; inline;
    function    checkRange(const val : integer; const Arr : TDoubleDynArray):boolean; overload; inline;
    function    checkRange(const val : integer; const Arr : TSingleDynArray):boolean; overload; inline;
    function    checkRange(const val1, val2 : integer; const Mat : TSingleMatrix):boolean; overload;

    function    availableMemory() : int64;
    function    TotalMemoryUsed() : int64;
    function    enoughMemoryForAssignment(const arr : TSingleDynArray; const targetLength : int64) : boolean; overload;
    function    enoughMemoryForAssignment(const arr : TIntegerDynArray; const targetLength : int64) : boolean; overload;
    function    enoughMemoryForAssignment(const arr : TSingleMatrix; const targetLength : int64) : boolean; overload;
    {$IFDEF MSWINDOWS}
    function    fileVersion(const FileName: TFileName): String;
    function    majorVersion(const FileName: TFileName): String;
    {$ENDIF}

implementation

{$B-}


class function  TMCKLib.Distance3D(const x1, x2, y1, y2, z1, z2: double): Double;
var
    distx, disty, distz : double;
begin
    distx := x1 - x2;
    disty := y1 - y2;
    distz := z1 - z2;
    Result := sqrt(sqr(distx) + sqr(disty) + sqr(distz));
end;

class function  TMCKLib.Distance2D(const x1, x2, y1, y2: double): Double;
{distance between 2x TPos3 in 3D}
begin
    Result := hypot(x1 - x2,y1 - y2);
end;

class function  TMCKLib.Distance2Points(const Point1,Point2 : TPoint) : double;
{2d distance between 2 tpoints}
begin
    Result := hypot(Point1.X - Point2.X,Point1.Y - Point2.Y);
end;

class procedure  TMCKLib.arrayLoopSmoothNaNs(const A :TDoubleDynArray);
{the array loops around on itself}
var
    i,len : integer;
    firstInd, lastInd, startInd, nextInd : integer;
    den : integer;
begin
    //if A is all nan, then all nan is returned
    len := length(A);
    if len = 0 then Exit;
    if A.isAllNaN then Exit;

    //set the first and last inds to edge value
    firstInd := -1;
    lastInd := -1;

    {find the first index with not nan}
    for i := 0 to len - 1 do //downto 0 do
        if not isNaN(A[i]) then
        begin
            firstInd := i;
            break;
        end;

    {last index with not nan}
    for i := len - 1 downto 0 do
        if not isNaN(A[i]) then
        begin
            lastInd := i;
            break;
        end;

    {make sure first and last entries are not nan - using looping}
    if isNaN(A[0]) and (firstInd <> -1) then
    begin
        if (lastInd = -1) then
            A[0] := A[firstInd]
        else
        begin
            den := firstInd + len - lastInd;
            A[0] := (1 - (firstInd/den))*A[firstInd] + (firstInd/den)*A[lastInd];
        end;
    end;
    if isNaN(A[len - 1]) and (lastInd <> -1) then
    begin
        if (firstInd = -1) then
            A[len - 1] := A[lastInd]
        else
        begin
            den := firstInd + len - lastInd;
            A[len - 1] := (1 - ((firstInd + 1)/den))*A[firstInd] + ((firstInd + 1)/den)*A[lastInd];
        end;
    end;

    {loop through the rest of the array}
    for i := 1 to len - 2 do
        if isNaN(A[i]) then
        begin
            startInd := i - 1;
            nextInd  := i + 1;
            while isNaN(A[nextInd]) do
                nextInd := nextInd + 1;

            den := nextInd - startInd;
            A[i] := (1 - ((i - startind)/den))*A[startind] + (i - startind)/den*A[nextInd];
        end;
end;

procedure forceRange(var val : double; const min,max :double);
{force a double to be within the two limits}
begin
    if isNaN(val) then Exit;

    if val < min then
        val := min;
    if val > max then
        val := max;
end;

procedure forceRange(var val : single; const min,max :single);
begin
    if isNaN(val) then Exit;

    if val < min then
        val := min;
    if val > max then
        val := max;
end;

procedure forceRange(var val : integer; const min,max :integer);
{force an integer to be within the two limits}
begin
    if val < min then
        val := min;
    if val > max then
        val := max;
end;

function checkRange(const val,min,max :double):boolean;
begin
    result := (val >= min) and (val <= max);
end;

function checkRange(const val,min,max :integer):boolean;
begin
    result := (val >= min) and (val <= max);
end;

function    checkRange(const val : integer; const Arr : TDoubleDynArray):boolean;
begin
    result := (val > -1) and (val < length(Arr));
end;

function    checkRange(const val : integer; const Arr : TSingleDynArray):boolean;
begin
    result := (val > -1) and (val < length(Arr));
end;

function    checkRange(const val1, val2 : integer; const Mat : TSingleMatrix):boolean; overload;
begin
    if length(mat) = 0 then exit(false);
    result := (val1 > -1) and (val1 < length(Mat)) and (val2 > -1) and (val2 < length(Mat[0]));
end;



class function  TMCKLib.ObjectHasMethod(const AClass : TObject; methodName : string) : boolean;
var
    lType       : TRttiType;
    lContext    : TRttiContext;
    lMethod     : TRttiMethod;
begin
    Result := false;
    LType := lContext.GetType(AClass.ClassType);
    if assigned(LType) then
        for lMethod in ltype.GetMethods do
            if ansiCompareText(lmethod.Name,methodName) = 0 then
                exit(true);
end;

class function  TMCKLib.GetMethodAddress(const AClass : TObject; methodName : string) : TRttiMethod;
var
    lType       : TRttiType;
    lContext    : TRttiContext;
    lMethod     : TRttiMethod;
begin
    Result := nil;
    LType := lContext.GetType(AClass.ClassType);
    if assigned(LType) then
        for lMethod in ltype.GetMethods do
            if ansiCompareText(lmethod.Name,methodName) = 0 then
                exit(lmethod);
end;

class function TMCKLib.triangleDistribution(const m : integer) : TDoubleDynArray;
var
    k : integer;
    aSum : double;
begin
    {generate a triangle distribution and normalise it (sum = 1)}
    setlength(result,1 + 2*m);
    for k := 0 to m - 1 do
    begin
        result[k      ] := (k + 1)/(m + 1);
        result[2*m - k] := (k + 1)/(m + 1);
    end;
    result[m] := 1;

    aSum := 0;
    for k := 0 to 2*m do
        aSum := aSum + result[k];

    for k := 0 to 2*m do
        result[k] := result[k]/aSum;
end;


{$IFNDEF HASP_DIAGNOSTIC}
class function   TMCKLib.checkSQLTable(const aDB : TSQLiteDatabase; const tblName : string; const requiredFields : TStringArray) : boolean;
var
    sltb: ISmart<TSQLiteTable>;
    i, j: Integer;
    bMatchFound: boolean;
    s: string;
begin
    sltb := TSmart<TSQLiteTable>.create(aDB.GetTable(ansistring('PRAGMA table_info(' + tblName + ');')));

    result := true;
    for I := 0 to length(requiredFields) - 1 do
    begin
        bMatchFound := false;
        sltb.MoveFirst;
        for j := 0 to sltb.Count - 1 do
        begin
            s := sltb.FieldAsString(sltb.FieldIndex['NAME']);
            if ansiCompareText(s,requiredFields[i]) = 0 then
                bMatchFound := true;
            sltb.Next;
        end;
        if not bMatchFound then
            result := false;
    end;
end;
{$ENDIF}


function availableMemory : int64;
var
    MemoryStatus: TMemoryStatus;
begin
    MemoryStatus.dwLength := SizeOf(MemoryStatus) ;
    GlobalMemoryStatus(MemoryStatus) ;

    result := memoryStatus.dwAvailPhys;
end;

function    enoughMemoryForAssignment(const arr : TSingleDynArray; const targetLength : int64) : boolean;
var
    currMemThisArray, wantedMemThisArray, extraMemWanted, memLimit : int64;
    //currentMemUsage : int64;
begin
    currMemThisArray := int64(length(arr))*int64(sizeof(Single));
    wantedMemThisArray := targetLength*int64(sizeof(Single));
    if wantedMemThisArray <= currMemThisArray then
    begin
        result := true;
    end
    else
    begin
        extraMemWanted := wantedMemThisArray - currMemThisArray;
        //currentMemUsage := TotalMemoryUsed;
        memLimit := saferound64((availableMemory)*0.75);
        result := (extraMemWanted) < memLimit;
    end;
end;

function    enoughMemoryForAssignment(const arr : TIntegerDynArray; const targetLength : int64) : boolean;
var
    currMemThisArray, wantedMemThisArray, extraMemWanted, memLimit: int64;
    //currentMemUsage : int64;
begin
    currMemThisArray := int64(length(arr))*int64(sizeof(Integer));
    wantedMemThisArray := targetLength*int64(sizeof(Integer));
    if wantedMemThisArray <= currMemThisArray then
    begin
        result := true;
    end
    else
    begin
        extraMemWanted := wantedMemThisArray - currMemThisArray;
        //currentMemUsage := TotalMemoryUsed;
        memLimit := saferound64((availableMemory)*0.75);
        result := (extraMemWanted) < memLimit;
    end;
end;

function    enoughMemoryForAssignment(const arr : TSingleMatrix; const targetLength : int64) : boolean;
var
    currMemThisArray, wantedMemThisArray, extraMemWanted, memLimit : int64;
    //currentMemUsage : int64;
begin
    currMemThisArray := int64(length(arr))*int64(sizeof(Single));
    wantedMemThisArray := targetLength*int64(sizeof(Single));
    if wantedMemThisArray <= currMemThisArray then
    begin
        result := true;
    end
    else
    begin
        extraMemWanted := wantedMemThisArray - currMemThisArray;
        //currentMemUsage := TotalMemoryUsed;
        memLimit := saferound64((availableMemory)*0.75);
        result := (extraMemWanted) < memLimit;
    end;
end;

{$IFDEF MSWINDOWS}
class function TMCKLib.CommittedStackSize: NativeUInt;
//NB: Win32 uses FS, Win64 uses GS as base for Thread Information Block.
asm
 {$IFDEF WIN32}
  mov eax, [fs:04h] // TIB: base of the stack
  mov edx, [fs:08h] // TIB: lowest committed stack page
  sub eax, edx      // compute difference in EAX (=Result)
 {$ENDIF}
 {$IFDEF WIN64}
  mov rax, abs [gs:08h] // TIB: base of the stack
  mov rdx, abs [gs:10h] // TIB: lowest committed stack page
  sub rax, rdx          // compute difference in RAX (=Result)
 {$ENDIF}
end;
{$ENDIF}


function TotalMemoryUsed: int64;
var
    //uses psapi
    MemCounters: TProcessMemoryCounters;
begin
    MemCounters.cb := SizeOf(MemCounters);
    Result := 0;
    if GetProcessMemoryInfo(GetCurrentProcess,@MemCounters,SizeOf(MemCounters)) then
        Result := MemCounters.WorkingSetSize
    else
        RaiseLastOSError;
end;

class procedure  TMCKLib.loopSmoothArray(const kernel : TDoubleDynArray; const arr : TDoubleDynArray; const smoothOverNans: boolean);
var
    n, k : integer;
    avLev, thisLev : double;
    aLevs : TDoubleDynArray;
    activeKernel : double;
    kernelSum : double;
    kLen : integer;
    nanInput : TBooleanDynArray;
begin
    {radial smoothing, using an arbitrary kernel. Go around the array in n, hooking up the end to the start.}

    if not smoothOverNans then
    begin
        setlength(nanInput,length(arr));
        for n := 0 to length(arr) - 1 do
            nanInput[n] := isnan(arr[n]);
    end;

    kLen := length(kernel) div 2;
    setlength(aLevs,length(arr) + 2*kLen);

    kernelSum := 0;
    for k := 0 to length(kernel) - 1 do
        kernelSum := kernelSum + kernel[k];

    for n := 0 to length(arr) + 2*kLen - 1 do
        aLevs[n] := arr[integerWrap(n - kLen,length(arr) - 1)];

    for n := 0 to length(arr) - 1 do
    begin
        avLev := nan;
        activeKernel := 0; //how much of the kernel do we use? unused parts are due to nan
        for k := 0 to 2*kLen do
            if not isnan(aLevs[n + k]) then
            begin
                thisLev := kernel[k] * aLevs[n + k];
                activeKernel := activeKernel + kernel[k];
                if isnan(avLev) then
                    avLev := thisLev
                else
                    avLev := avLev + thisLev;
            end;

        //if there were nans, then we need to increase the avLev to accomodate
        if (activeKernel > 0) and (activeKernel < kernelSum) then
            avLev := avLev * kernelSum / activeKernel;

        arr[n] := avLev;
    end;//n

    //anywhere we had nan on the input, set nan on the output
    if not smoothOverNans then
        for n := 0 to length(arr) - 1 do
            if nanInput[n] then
                arr[n] := nan;
end;

class function   TMCKLib.linspace(const start, finish : double; const count : integer) : TDoubleDynArray;
var
    i : integer;
begin
    setlength(result,count);
    for I := 0 to count - 1 do
        result[i] := start + finish * i / (count - 1);
end;

{$IFDEF MSWINDOWS}
function fileVersion(const FileName: TFileName): String;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
  iLastError: DWord;
begin
  Result := '';
  VerInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(PVerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), 0, VerInfoSize, PVerInfo) then
      begin
        if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          with PVerValue^ do
            Result := Format('v%d.%d.%d build %d', [
              HiWord(dwFileVersionMS), //Major
              LoWord(dwFileVersionMS), //Minor
              HiWord(dwFileVersionLS), //Release
              LoWord(dwFileVersionLS)]); //Build
      end
      else
      begin
        iLastError := GetLastError;
        Result := Format('GetFileVersionInfo failed: (%d) %s',
                      [iLastError, SysErrorMessage(iLastError)]);
      end;
    finally
      FreeMem(PVerInfo, VerInfoSize);
    end;
  end
  else
  begin
    iLastError := GetLastError;
    Result := Format('GetFileVersionInfo failed: (%d) %s',
                     [iLastError, SysErrorMessage(iLastError)]);
  end;
end;

function majorVersion(const FileName: TFileName): String;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
  iLastError: DWord;
begin
  Result := '';
  VerInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(PVerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), 0, VerInfoSize, PVerInfo) then
      begin
        if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          with PVerValue^ do
            Result := Format('%d.%d', [
              HiWord(dwFileVersionMS), //Major
              LoWord(dwFileVersionMS)]); //Minor
      end
      else
      begin
        iLastError := GetLastError;
        Result := Format('GetFileVersionInfo failed: (%d) %s',
                      [iLastError, SysErrorMessage(iLastError)]);
      end;
    finally
      FreeMem(PVerInfo, VerInfoSize);
    end;
  end
  else
  begin
    iLastError := GetLastError;
    Result := Format('GetFileVersionInfo failed: (%d) %s',
                     [iLastError, SysErrorMessage(iLastError)]);
  end;
end;
{$ENDIF}


class function TMCKLib.pointInPolygon(const Points: TPointArray; X,Y: Integer):Boolean;
var
    count, k, j : Integer;
begin
    Result := False;
    count := Length(Points) ;
    J := Count - 1;
    for K := 0 to Count-1 do
    begin
        if ((Points[K].Y <=Y) and (Y < Points[J].Y)) or
          ((Points[J].Y <=Y) and (Y < Points[K].Y)) then
        begin
            if (x < (Points[j].X - Points[K].X) *
               (y - Points[K].Y) /
               (Points[j].Y - Points[K].Y) + Points[K].X) then
                Result := not Result;
        end;
        J := K;
    end;
end;

class function TMCKLib.pointInPolygon(const Points: TFPointArray; X,Y: double):Boolean;
var
    count, k, j : Integer;
begin
    Result := False;
    count := Length(Points) ;
    J := Count - 1;
    for K := 0 to Count - 1 do
    begin
        if ((Points[K].Y <= Y) and (Y < Points[J].Y)) or
          ((Points[J].Y <= Y) and (Y < Points[K].Y)) then
        begin
            if (x < (Points[j].X - Points[K].X) *
               (y - Points[K].Y) /
               (Points[j].Y - Points[K].Y) + Points[K].X) then
                Result := not Result;
        end;
        J := K;
    end;
end;


end.
