unit ESRIReader;

interface

uses syncommons,
    system.sysutils, system.math, system.classes, system.strutils, system.UITypes,
    vcl.dialogs, vcl.forms, vcl.controls, vcl.stdctrls, vcl.comctrls,
    winapi.windows,
    latlong2UTM,
    helperFunctions, basetypes,
    eventlogging, SmartPointer, nullable;

type
    TESRIReader = class(TObject)
    private
        class function readData(const sr: TStreamReader; const nrows, ncols: integer; const sNODATA_value: string; const NODATA_value, cellValue: single; const lb: TLabel; const pb: TProgressBar): TSingleMatrix;
    public
    const
        delim: char = ' '; // esri delimiter is always space

        class function readESRIascii(const Filename: string; const lb: TLabel; const pb: TProgressBar): TSingleMatrix;
    end;

implementation

uses problemContainer, main, importedLocationUnit;

class function TESRIReader.readESRIascii(const Filename: string; const lb: TLabel; const pb: TProgressBar): TSingleMatrix;
const
    testStrings: Array [0..10] of string = ('ncols',
                                             'nrows',
                                             'xllcorner','xllcenter',
                                             'yllcorner','yllcenter',
                                             'cellsize',
                                             'dx',
                                             'dy',
                                             'cellvalue',
                                             'NODATA_value');
    iIncreaseResultsRowsBy = 100;
var
    s: string;
    sCompare, sCompare2: string;
    cellsize, dx,dy: double;
    nrows,ncols: integer;
    sNODATA_value: string;
    NODATA_value: single;
    cellvalue: single;
    sr: ISmart<TStreamReader>;
    log: ISynLog;
    tmp, bottomLeftCorner, topLeftCorner, leftmost, rightmost: TEastNorth;
    //oldWorldScale: double;
begin
    log := TSynLogLevs.Enter(self,'readESRIascii');

    {read file into a stringlist and then use the string version of the function. previously this was
    being read into a string, then the string read into the stringlist. but sometimes we are reading in string
    that are hundreds of MB long, so here we can save 1 copy by going straight into the stringlist}
    setlength(Result,0,0);

    //regionalSettingsFixes := nil;
    sr := TSmart<TStreamReader>.create(TStreamreader.Create(Filename));
    //regionalSettingsFixes := TregionalSettingsFixes.Create;

    {get bathymetry from a CSV or ArcGIS ascii grid file. round data to 0.1m }
    nrows := 1;
    ncols := 1;
    cellsize := 1;
    cellvalue := 1;
    dx := 1;
    dy := 1;
    sNODATA_value := '-9999';
    NODATA_value := -9999;

    {ncols}
    sCompare :=  testStrings[0];
    s := sr.ReadLine;
    if SubStringOccurencesi(sCompare,s) <> 1 then Exit;
    s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare));
    if isInt(trim(s)) then
        ncols := toint(trim(s));

    {nrows}
    sCompare :=  testStrings[1];
    s := sr.ReadLine;
    if SubStringOccurencesi(sCompare,s) <> 1 then Exit;
    s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare));
    if isInt(trim(s)) then
        nrows := toint(trim(s));

    {xllcorner/xllcenter aka easting - need to compare against both strings}
    sCompare  := testStrings[2];
    sCompare2 := testStrings[3];
    s := sr.ReadLine;
    if (SubStringOccurencesi(sCompare,s) <> 1) and (SubStringOccurencesi(sCompare2,s) <> 1) then Exit;
    if (SubStringOccurencesi(sCompare,s) = 1) then
        s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare))
    else
    if (SubStringOccurencesi(sCompare2,s) = 1) then
        s := MidStr(s,length(sCompare2) + 1,length(s) - length(sCompare2));
    s := trim(s);//regionalSettingsFixes.patchLocalSeparator(trim(s));
    if isFloat(trim(s)) then
        container.geoSetup.setEasting(tofl(s))
    else
        container.geoSetup.setEasting(0.0);

    {yllcorner/yllcenter aka northing - need to compare against both}
    sCompare :=  testStrings[4];
    sCompare2 := testStrings[5];
    s := sr.ReadLine;
    if (SubStringOccurencesi(sCompare,s) <> 1) and (SubStringOccurencesi(sCompare2,s) <> 1) then Exit;
    if (SubStringOccurencesi(sCompare,s) = 1) then
        s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare))
    else
    if (SubStringOccurencesi(sCompare2,s) = 1) then
        s := MidStr(s,length(sCompare2) + 1,length(s) - length(sCompare2));
    s := trim(s);//regionalSettingsFixes.patchLocalSeparator(trim(s));
    if isFloat(s) then
        container.geoSetup.setNorthing(tofl(s))
    else
        container.geoSetup.setNorthing(0.0);

    {cellsize or dx dy}
    s := sr.ReadLine;
    if not ((SubStringOccurencesi(testStrings[6],s) = 1) or (SubStringOccurencesi(testStrings[7],s) = 1)) then Exit;
    sCompare :=  testStrings[6];
    if SubStringOccurencesi(sCompare,s) = 1 then
    begin
        s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare));
        s := trim(s);//regionalSettingsFixes.patchLocalSeparator(trim(s));
        if isFloat(s) then
            cellsize := tofl(s);
        dx := cellsize;
        dy := cellsize;
    end
    else
    begin
        sCompare :=  testStrings[7];
        if SubStringOccurencesi(sCompare,s) <> 1 then Exit;
        s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare));
        s := trim(s);//regionalSettingsFixes.patchLocalSeparator(trim(s));
        if isFloat(s) then
            dx := tofl(s);

        s := sr.ReadLine;
        sCompare :=  testStrings[8];
        if SubStringOccurencesi(sCompare,s) <> 1 then Exit;
        s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare));
        s := trim(s);//regionalSettingsFixes.patchLocalSeparator(trim(s));
        if isFloat(s) then
            dy := tofl(s);

        if (larger(dx,dy) / smaller(dx,dy)) > 1.1 then
            showmessage('Warning: X and Y axes of input data not at same scale');

        cellsize := hypot(dx,dy);
    end;

    if (isnan(cellsize) or (cellsize = 0)) then
        showmessage('Unable to parse cellsize or dx,dy')
    else
        container.geoSetup.setWorldScale(cellsize*larger(nrows,ncols));

    importedLocation.clear;
    if (cellsize < 0.1) and
       (abs(container.geoSetup.Easting) <= 360) and
       (abs(container.geoSetup.Northing) <= 90) then
    begin
        //if (mrYes = MessageDlg('Are the sizes given in the file header in degrees latitude/longitude?',
        //                        mtConfirmation,[mbYes,mbNo],0)) then
        begin
            //earth circumference is 40,000 km
            //oldWorldScale := container.geoSetup.worldScale;
            //tmp.longitude := container.geoSetup.Easting;
            //tmp.latitude := container.geoSetup.Northing;
            tmp := container.geoSetup.centerToTEastNorth(nrows, ncols);
            importedLocation.setWith(tmp,dx,dy);
            tmp := TLatLong2UTM.latlong2UTM(tmp);

            bottomLeftCorner.longitude := container.geoSetup.Easting;
            bottomLeftCorner.latitude := container.geoSetup.Northing;
            bottomLeftCorner := TLatLong2UTM.latlong2UTM(bottomLeftCorner, importedLocation.utmLongZone);

            topLeftCorner.longitude := container.geoSetup.Easting;
            topLeftCorner.latitude := container.geoSetup.Northing + dy * (nrows - 1);
            topLeftCorner := TLatLong2UTM.latlong2UTM(topLeftCorner, importedLocation.utmLongZone);

            leftmost.longitude := container.geoSetup.Easting;
            rightmost.longitude := container.geoSetup.Easting + dx * (ncols - 1);
            if container.geoSetup.Northing >= 0 then
                //northern hemisphere, use lower left corner
                leftmost.latitude := container.geoSetup.Northing
            else if dy * (nrows - 1) > -container.geoSetup.Northing then
                //crosses equator, use equator
                leftmost.latitude := 0
            else
                //southern hemisphere, use top left corner
                leftmost.latitude := container.geoSetup.Northing + dy * (nrows - 1);

            rightmost.latitude := leftmost.latitude;
            leftmost := TLatLong2UTM.latlong2UTM(leftmost, importedLocation.utmLongZone);
            rightmost := TLatLong2UTM.latlong2UTM(rightmost, importedLocation.utmLongZone);

            container.geoSetup.setWorldScale(max(rightmost.easting - leftmost.easting, topLeftCorner.northing - bottomLeftCorner.northing));

            //container.geoSetup.setWorldScale(container.geoSetup.worldScale * TLatLong2UTM.mPerDegreeLongAroundEquator * cos(tmp.latitude/180*Pi));
            //container.geoSetup.setEasting(tmp.easting - container.geoSetup.worldScale / 2 * ncols / max(nrows,ncols));
            //container.geoSetup.setNorthing(tmp.northing - container.geoSetup.worldScale / 2 * nrows / max(nrows,ncols));
            container.geoSetup.setEasting(leftmost.easting);
            container.geoSetup.setNorthing(bottomLeftCorner.northing);
        end;
    end;

    {maybe cellvalue, then NODATA_value}
    sCompare :=  testStrings[9];
    s := sr.ReadLine;
    if SubStringOccurencesi(sCompare,s) = 1 then
    begin
        s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare));
        s := trim(s);//regionalSettingsFixes.patchLocalSeparator(trim(s));
        if isFloat(s) then
            cellvalue := tofl(s);
        if cellvalue = 0 then
            cellvalue := 1;

        s := sr.ReadLine; //get next line for nodata value (if not in this loop, s already contains the nodata line)
    end;

    {NODATA_value}
    sCompare :=  testStrings[10];
    if SubStringOccurencesi(sCompare,s) = 1 then
    begin
        s := MidStr(s,length(sCompare) + 1,length(s) - length(sCompare));
        s := trim(s);//regionalSettingsFixes.patchLocalSeparator(trim(s));

        if isFloat(s) then
            sNODATA_value := s;   //can't actually use this, as it is a truncated version of the value

        {check for a value with something like e+038 at the end. These values
        can be truncated and then rounded, which will lead to the strings not matching
        when we check for them later. So, truncate the number before the e and the
        last rounded digit.}

        {04/12/14, just not sure how to handle this. many programs will write a different value for NODATA in header as what they use in the
        file data. Some use a very large value, and that is fine, but some use a very small value which is much harder to detect.}
        //if AnsiContainsStr(sNODATA_value,'E') then
        //    sNODATA_value := leftstr(sNODATA_value,AnsiPos('E', sNODATA_value) - 2)
        //else if AnsiContainsStr(sNODATA_value,'e') then
        //    sNODATA_value := leftstr(sNODATA_value,AnsiPos('e', sNODATA_value) - 2);

        NODATA_value := tofl(s);
        if abs(NODATA_value) > 1.0e6 then
            NODATA_value := NODATA_value * 0.9; //to take care of files that use a different value in the header and the data
    end;

    if assigned(pb) or assigned(lb) then
    begin
        mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Solve);
        if assigned(lb) then
        begin
            lb.Caption := 'Reading grid data...';
            lb.Visible := true;
        end;
        if assigned(pb) then
        begin
            pb.Max := 50;
            pb.Step := 1;
            pb.Position := 0;
            pb.Visible := true;
        end;
    end;

    result := TESRIReader.readData(sr, nrows, ncols, sNODATA_value, NODATA_value, cellValue, lb, pb);

    //check if reshape needed
    if importedLocation.hasUTMData then
    begin
        result := importedLocation.reshapeWithImportLocation(result);
        container.geoSetup.setWorldScale(importedLocation.dx.val*larger(length(result),length(result[0])));
    end;
end;

class function TESRIReader.readData(const sr: TStreamReader; const nrows, ncols: integer; const sNODATA_value: string; const NODATA_value, cellValue: single; const lb: TLabel; const pb: TProgressBar): TSingleMatrix;
var
    sLine       : string;
    sl          : ISmart<TStringlist>;
    i, j        : integer;
    sField, sTrimField      : string;
begin
    setlength(result, nrows, ncols);
    result.setAllVal(nan);

    sl := TSmart<TStringlist>.create(TStringList.Create);
    sl.Delimiter := delim;

    i := 0;
    j := 0;
    repeat
        sLine := sr.ReadLine;
        sl.DelimitedText := trim(sLine);
        for sField in sl do
        begin
            sTrimField := trim(sField);
            if (ansiCompareStr(sNODATA_value,sTrimField) <> 0) and
                isfloat(sTrimField) and
                (abs(tofl(sTrimField)) < 0.9e6) then
                result[i,j] := tofl(sField)*cellvalue;

            inc(j);
            if j = ncols then
            begin
                j := 0;
                inc(i);

                if assigned(pb) or assigned(lb) then
                begin
                    if (i mod 20) = 0 then
                    begin
                        if assigned(pb) then
                            pb.StepIt;
                        if assigned(lb) then
                            lb.Caption := 'Reading grid data: row ' + tostr(i);
                        application.ProcessMessages;
                    end;
                end;
            end;
        end;
    until sr.EndOfStream;
end;

end.
