object SetupFrame: TSetupFrame
  Left = 0
  Top = 0
  Width = 630
  Height = 920
  AutoScroll = True
  Constraints.MinHeight = 800
  TabOrder = 0
  OnMouseDown = FrameMouseDown
  object imPageTitle: TImage
    Left = 48
    Top = 18
    Width = 45
    Height = 45
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000002D0000
      002D08060000003A1AE29A00000006624B474400980098009815E12590000000
      097048597300000B1300000B1301009A9C180000000774494D4507DE03190C34
      2F97B082F0000005E04944415478DAD599694C54671486DF19669895619F61DF
      5110448B8A020315958245DA109B986E69524D4DD4BAFC704B6D630444298B54
      1CB5C4D228CADED29A346D637FB489266D9A585BD37F6D5C58155B907D60989E
      EF4E651119BE995147DF64929BCCDC7B9F39F79CF39EEF7E223309CF994453A1
      D9A17174142693C9D95C10D1472295422A91CC0ECD400706079DCD3A4362B118
      2AA5122291683AF4D8D8180687869CCD37AB5C5C5C04F009E8F1F171F32045D8
      343EEE6C36AB92CBE570A57411A0292DCCFD0303CE669A532C3DDCD46ACB31A5
      C67301CDE4AED14C42F7F5F773FFDB1B1D5D104F290A5B15E0EB0D172A2E7BE4
      E1EE3E09DD7BFF3ED7491A9512D2E824EA452EACDDF0DF8D352816252AF80D79
      3938BD6F07A89660AB4578797A5AA04719746F2FD7496AA502F2783D5AAA8E21
      6D713CF7CD3CDDD41027A4232C3A4278526F6465A07CC7669B23EEEDE5F53FF4
      E8A8F99F7F7BB8A1558BD2D17CAA0CFA4571DC37F32068D9E217515D7C10D7FF
      BA81D23335D8B4210F455BDE15A2FD20E2AC27B3C49BAD93697D7D2CD0E480E6
      EEEE7BDCD09AC40C341A4A909AB0801F9AAA5EB974158E1FDC83F52BF538D17C
      1185D517909B9E8C93BBDF176A8445FD466717FA0686101F194AE9333375FCFD
      7476402BE4D02C598506C3C7D027D81069B50ACA656BB073D3DB58999800A5CC
      157B0DD5B87AED3A5A2A0E6379DC7C74F7F461E19B9BE146755397BF1F4B62A2
      66804F421B47CD77BBBB39A115705FBA1A0D278A6D8A34CB69656C327308E0C1
      A367C54C2ED7527A080BC24290BDEB00FE6EEF148A353438107F9C33A06F70BA
      4B07F8FB3D3D6896B32CB53025702A7A6AD2B854D4561EC5B61203EEF5F4B0A4
      16F2FF1245DFDFC76BC675A6401BCD77EEF2437BD063AE3F7114A90BF9A11F25
      1545DD33752D343A2DEEF7F5331310FE58D3E10348880A175AE2C30A0CF0772E
      B41B01BAAFC8A6038B35D34C8C6B7555F0F1D0CCDABFED8496137426EAE991DA
      921E0F8BB5B4D73F3A8A2B54882C252434C57DBA773BB29627C29ADD4C83EEBA
      73973BD29E4999A8AB3C6277A419E05EC36738FBCD252125306244131563727C
      CC235362AA820203EC81A63C4C7AC921E8A2B3F530D4340A9D83758AC2AD1BB1
      31370BE4CC739E3B0DBAB3EB0E37B4D78A2CD47D5284141BA19979E457D7E254
      E3D710B94A611E1AA6B08BD152720889F3A3B8AE111C14F8F4A0993DB7FC7805
      DB8A2B2DFD99D6A115BBB761477E099A8F1FC1B2D879B64377907DF2427B2767
      A3B68241C7729DC3C6D9AAAFBE45FEE9CF01998C52C284B7723251B1F33D7853
      009A4E9660594C34D7B54282839E3C3403BEFCFB9F78A7A0943ADA98D0D65E5D
      9D8E537BB642EA22814F72169A097AA9ADD0230CBAA3931BDA27652D2E9063A5
      C45B87665DE2875F7F23E032CB3C4D9FBC95A9A8DAB75DB067E6885ABA5613CD
      31BCD0A121C14F169A2D18CA6ABF4041E519EA140A24CC8B4263C17EC85C2D8B
      53E688BE29D976428F18CDED1D1DDCD0BEA92FE3FCB142ABD03421536B6B40AE
      7E39BEFFE52A7EBA7A1DDF95E763D8689CF88D522E838EAED548730C2F745868
      C893831EA1EE90B46917E414D596231FC28F561C0F2F2B5504AD7D96A059D15D
      BCFC335ECBD0CF3A473818E911735B3B3FB4569F83F3E50564BBD673DA552281
      D18ACB31683FFD3A3490BBF2428787853A025D28CC0A8E88A587CE11E8D6B676
      6E685DDA3AD49415380CCD22ED9F964B13234173DA784478D8B301CD22BDC41E
      E8DBAD6DDCD07EE9B93857968FE438C7A103D25F41FDF1226EE8C88870FBA1BF
      64AB711BDE7B3C4ACC5C942F6408E96117F4ADDBADDCD0FE6BD643F46089E4A0
      CC643675851F7043474546D80E2D934AB1A5CC00059BD61E83C64C266CCDCB41
      A89FD636E861067DEB36F78DEC7DE3399BC6A7BC169B4BD15191F6413B5313D0
      6CCABB79F396B379E694841C76C25CD8F6452B750F8AB8B3B9AC4AA7D34EEE04
      B0DDAD411ACA5BDBF8DA9E33A4502826D68713D0EC80459AE5F6B3B6812B2713
      0A0E0A1216C733A099D8CB929E9E5E0C0D0F4104FBF7551E874462B69BE506B5
      5A35F3BBE7716FFC3F7ABED298D42075E00000000049454E44AE426082}
  end
  object lblPageTitle: TLabel
    Left = 110
    Top = 35
    Width = 151
    Height = 13
    Caption = 'Set up the overall project world'
  end
  object lblSetupScenario: TLabel
    Left = 33
    Top = 395
    Width = 94
    Height = 13
    Caption = 'Set up this scenario'
  end
  object btSetupCrossSec: TButton
    Left = 48
    Top = 195
    Width = 180
    Height = 25
    Caption = 'Setup cross-section'
    TabOrder = 0
    TabStop = False
    OnClick = btSetupCrossSecClick
  end
  object btSetWorldScale: TButton
    Left = 48
    Top = 145
    Width = 180
    Height = 25
    Caption = 'Set world scale'
    DropDownMenu = puSetWorldScale
    PopupMenu = puSetWorldScale
    TabOrder = 1
    TabStop = False
    OnClick = btSetWorldScaleClick
  end
  object btOk: TButton
    Left = 47
    Top = 724
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 7
    OnClick = btOkClick
  end
  object btCancel: TButton
    Left = 145
    Top = 724
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    TabStop = False
    OnClick = btCancelClick
  end
  object btLoadBathymetry: TButton
    Left = 48
    Top = 96
    Width = 180
    Height = 25
    Caption = 'Load bathymetry'
    TabOrder = 3
    TabStop = False
    OnClick = btLoadBathymetryClick
  end
  object gbGrid: TGroupBox
    Left = 33
    Top = 426
    Width = 400
    Height = 128
    Caption = 'Calculation grid'
    ParentBackground = False
    TabOrder = 4
    object lblDepthPoints: TLabel
      Left = 14
      Top = 93
      Width = 68
      Height = 13
      Caption = 'z depth points'
    end
    object lblXGridPoints: TLabel
      Left = 14
      Top = 29
      Width = 38
      Height = 13
      Caption = 'x points'
    end
    object lblYGridPoints: TLabel
      Left = 14
      Top = 61
      Width = 38
      Height = 13
      Caption = 'y points'
    end
    object edDMax: TEdit
      Left = 154
      Top = 90
      Width = 80
      Height = 21
      TabOrder = 4
      Text = '50'
      OnChange = EditBoxesChange
    end
    object edIMax: TEdit
      Left = 154
      Top = 26
      Width = 80
      Height = 21
      TabOrder = 2
      Text = '50'
      OnChange = EditBoxesChange
    end
    object edJMax: TEdit
      Left = 154
      Top = 58
      Width = 80
      Height = 21
      TabOrder = 3
      Text = '50'
      OnChange = EditBoxesChange
    end
    object udDepthPoints: TUpDown
      Left = 234
      Top = 90
      Width = 17
      Height = 21
      Associate = edDMax
      Min = 3
      Max = 1000000
      Position = 50
      TabOrder = 5
      Thousands = False
    end
    object udIMax: TUpDown
      Left = 234
      Top = 26
      Width = 17
      Height = 21
      Associate = edIMax
      Min = 3
      Max = 1000000
      Position = 50
      TabOrder = 6
      Thousands = False
    end
    object udJMax: TUpDown
      Left = 234
      Top = 58
      Width = 17
      Height = 21
      Associate = edJMax
      Min = 3
      Max = 1000000
      Position = 50
      TabOrder = 7
      Thousands = False
    end
    object btAspectRatio: TButton
      Left = 256
      Top = 24
      Width = 129
      Height = 25
      Caption = 'Square aspect ratio'
      TabOrder = 0
      TabStop = False
      OnClick = btAspectRatioClick
    end
    object btShowSizes: TButton
      Left = 256
      Top = 88
      Width = 129
      Height = 25
      Caption = 'Step sizes'
      TabOrder = 1
      TabStop = False
      OnClick = btShowSizesClick
    end
  end
  object gbSourceSolution: TGroupBox
    Left = 33
    Top = 570
    Width = 400
    Height = 112
    Caption = 'Source solution'
    ParentBackground = False
    TabOrder = 5
    object lblRangePoints: TLabel
      Left = 14
      Top = 67
      Width = 63
      Height = 13
      Caption = 'Range points'
    end
    object lblSolutionSlices: TLabel
      Left = 15
      Top = 35
      Width = 57
      Height = 13
      Caption = 'Radial slices'
    end
    object edNSlices: TEdit
      Left = 154
      Top = 32
      Width = 80
      Height = 21
      TabOrder = 2
      Text = '50'
      OnChange = EditBoxesChange
    end
    object edSrcIMax: TEdit
      Left = 154
      Top = 66
      Width = 80
      Height = 21
      TabOrder = 3
      Text = '50'
      OnChange = EditBoxesChange
    end
    object udSrcNSlices: TUpDown
      Left = 234
      Top = 32
      Width = 17
      Height = 21
      Associate = edNSlices
      Min = 1
      Max = 1000000
      Position = 50
      TabOrder = 0
      Thousands = False
    end
    object udSrcRangePoints: TUpDown
      Left = 234
      Top = 66
      Width = 17
      Height = 21
      Associate = edSrcIMax
      Min = 3
      Max = 1000000
      Position = 50
      TabOrder = 1
      Thousands = False
    end
  end
  object gbCoordinateSystem: TGroupBox
    Left = 33
    Top = 242
    Width = 400
    Height = 147
    Caption = 'Coordinate system'
    ParentBackground = False
    TabOrder = 6
    object lblEasting: TLabel
      Left = 18
      Top = 51
      Width = 54
      Height = 13
      Caption = 'Easting (m)'
    end
    object lblNorthing: TLabel
      Left = 18
      Top = 81
      Width = 60
      Height = 13
      Caption = 'Northing (m)'
    end
    object lblWorldScale: TLabel
      Left = 18
      Top = 24
      Width = 74
      Height = 13
      Caption = 'World scale (m)'
    end
    object lbUTMZone: TLabel
      Left = 18
      Top = 113
      Width = 96
      Height = 13
      Caption = 'UTM zone (optional)'
    end
    object edEasting: TEdit
      Left = 146
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '0.0'
      OnChange = EditBoxesChange
    end
    object edNorthing: TEdit
      Left = 146
      Top = 78
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '0.0'
      OnChange = EditBoxesChange
    end
    object edUTMZone: TEdit
      Left = 146
      Top = 110
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '0.0'
      OnChange = edUTMZoneChange
    end
  end
  object btMakeDiff: TButton
    Left = 439
    Top = 254
    Width = 75
    Height = 25
    Caption = 'makeDiff'
    TabOrder = 8
    TabStop = False
    Visible = False
    OnClick = btMakeDiffClick
  end
  object btRemoveDiff: TButton
    Left = 439
    Top = 285
    Width = 75
    Height = 25
    Caption = 'removeDiff'
    TabOrder = 9
    TabStop = False
    Visible = False
    OnClick = btRemoveDiffClick
  end
  object btFlocking: TButton
    Left = 439
    Top = 347
    Width = 75
    Height = 25
    Caption = 'Moving receivers'
    TabOrder = 10
    TabStop = False
    Visible = False
    OnClick = btFlockingClick
  end
  object cbUseGPU: TCheckBox
    Left = 439
    Top = 378
    Width = 97
    Height = 17
    TabStop = False
    Caption = 'Use GPU'
    TabOrder = 11
    Visible = False
    OnClick = cbUseGPUClick
  end
  object btComments: TButton
    Left = 439
    Top = 316
    Width = 75
    Height = 25
    Caption = 'btComments'
    TabOrder = 12
    TabStop = False
    Visible = False
    OnClick = btCommentsClick
  end
  object btLocationProperties: TButton
    Left = 252
    Top = 96
    Width = 180
    Height = 25
    Caption = 'Properties map'
    TabOrder = 13
    TabStop = False
    Visible = False
    OnClick = btLocationPropertiesClick
  end
  object btGeoOverlays: TButton
    Left = 252
    Top = 145
    Width = 180
    Height = 25
    Caption = 'Shape overlays'
    TabOrder = 14
    TabStop = False
    Visible = False
    OnClick = btGeoOverlaysClick
  end
  object btTestShowMovingPosIndexUp: TButton
    Left = 464
    Top = 96
    Width = 25
    Height = 25
    Caption = '+'
    TabOrder = 15
    TabStop = False
    Visible = False
    OnClick = btTestShowMovingPosIndexUpClick
  end
  object btTestShowMovingPosIndexDown: TButton
    Tag = 1
    Left = 464
    Top = 120
    Width = 25
    Height = 25
    Caption = '-'
    TabOrder = 16
    TabStop = False
    Visible = False
    OnClick = btTestShowMovingPosIndexUpClick
  end
  object btSetImageOverlay: TButton
    Left = 252
    Top = 195
    Width = 100
    Height = 25
    Caption = 'Image overlay'
    TabOrder = 17
    Visible = False
    OnClick = btSetImageOverlayClick
  end
  object btGLSetOverlay: TButton
    Left = 351
    Top = 195
    Width = 82
    Height = 25
    Caption = 'Set on map'
    TabOrder = 18
    Visible = False
    OnClick = btGLSetOverlayClick
  end
  object puSetWorldScale: TPopupMenu
    Left = 457
    Top = 558
    object miSetFromEW: TMenuItem
      Caption = 'Set from East-West extents'
      OnClick = SetWorldScaleWithTag
    end
    object miSetFromNS: TMenuItem
      Tag = 1
      Caption = 'Set from Noth-South extents'
      OnClick = SetWorldScaleWithTag
    end
    object miSetFromDiagonal: TMenuItem
      Tag = 2
      Caption = 'Set from diagonal'
      OnClick = SetWorldScaleWithTag
    end
    object miSetBy2Points: TMenuItem
      Tag = 3
      Caption = 'Set by clicking two points'
      OnClick = SetWorldScaleWithTag
    end
  end
  object sdExportXSec: TSaveDialog
    Filter = 'Text files|*.txt|CSV files|*.csv|All files|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Choose output data file'
    Left = 456
    Top = 472
  end
  object odImage: TOpenDialog
    Filter = 'JPG files|*.jpg'
    FilterIndex = 0
    Left = 488
    Top = 512
  end
end
