object EditSourceFrame: TEditSourceFrame
  Left = 0
  Top = 0
  Width = 630
  Height = 920
  Constraints.MinHeight = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object lblName: TLabel
    Left = 51
    Top = 219
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblZ: TLabel
    Left = 52
    Top = 300
    Width = 37
    Height = 13
    Caption = 'Depth z'
  end
  object lblY: TLabel
    Left = 51
    Top = 273
    Width = 6
    Height = 13
    Caption = 'y'
  end
  object lblX: TLabel
    Left = 51
    Top = 246
    Width = 6
    Height = 13
    Caption = 'x'
  end
  object lbDepthUnderSource: TLabel
    Left = 51
    Top = 383
    Width = 153
    Height = 13
    Caption = 'Water depth at source location:'
  end
  object imPageTitle: TImage
    Left = 48
    Top = 18
    Width = 45
    Height = 45
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000002D0000
      002D08060000003A1AE29A00000006624B474400980098009815E12590000000
      097048597300000B1300000B1301009A9C180000000774494D4507DE03190C34
      239E06CEDB000008C04944415478DADD990B5494651AC7FFC36D98196E32C31D
      23ED78C5AD3C5ECAC3E6BA2A99C77475D3D4524C8F8AA6689A2209882BA26421
      6D1D90B3A6929986EC1E15DD3297D6D672F39A1BBB1642D20A8A5C66B8CE0CC3
      70DBE77967189804816017D88733679899EF7BBFDFFBBCCFF37F9EF7FB244D64
      E86726F9BF8036D6D5A1B6B6167D612E12890452A9140EF6F66D43F39BBEA606
      0D0D0DBDCDFA80D9DADA422E93894958416B753A343636F6365FBB6663630327
      85A205BAAEBE1E35E4E5BE6E32F2B6BD9D9D099AC3A28E62B9AF9B3DC5B60813
      01ADD783BDDDD78DBD2C97CB4DD05A9DBEDF78DA49D10CADD509A9EB09336B91
      F59794F5921E189BA5CFC9496182AED66A61341ABB3C888C34B4BC4A8B1B393F
      20F76E21AE67E7E25EA91A459A72682AAB04ACBF8712EE2ECE1832D00F230705
      60C4A3033176F810A106C6BA7A48BA301B070707383B3999A02BABAAA8A0741E
      DAD6D60636121B44FFE103A465FE0D5A4A64039FCF92C92F7677A3D9DB044707
      8B7709BD1C69A2BE2A776C7A692E963DFF2C2A29343B5BC8A45207B8BAB898A0
      2B2A2AE9A2B51DC3D245ABE822699F5F40140137196AF94B0C18E0061F021935
      E8518C1F390CDECA01E4595F0173F3C77CE4DD2BC2B5EC1CDCBE775FAC82969C
      84FA060490E7F7AC59865F3E1E280A4847F03C6137375713745979050C064387
      A1F055D64DAC4D48427E51092873E1E6EE8EDF2D5F84712386C2DF5305391DD3
      409E6EE23F012021274B4425E330A8A8D6A1A0A414672F5DC3DBC7FE84465E1D
      8AD3A0C7472265731894AE2E0F05777474843B39A8D3D0272F7C8D3504CC6647
      5E593F6F3636BFFC02454193E54296324B2F86E7B86DB12634F3D899BD1A129B
      80CCABDF884AEC4930197BB623C0C7933EB70DDE696819C5D19E8FFE88F8D463
      4C82714F0422216C05460D0E408D390FE48E5254900271225EF9EE96F8BFA25A
      0B17CA740E293F955224DFF8C061682220A3B926B0EE9EF9EA32620E7C88024A
      6499B3138EC48423E81723C5A41F0E5D568E9A36A0D953074E7F86E8FD872906
      EB31F9A931381EBB1586564AC35E8B3D741407CE9C2398BA36BD24314B9E935C
      86F8D54BF1E2E4895663F004835F8BC47D4D9970C0B9C4380CF2F57E2054640C
      ED3EA07D681BCAF8EC7FDFC5CCF0EDD0913A4C7B7A2C0E456E145D2043F078E9
      E7BF4444F24118C9B3BCFCDE7E3E18ECE34D49A9844226859E129565AD90606E
      DD29407579251DD68811C386207AE94B081E371AB5941B1CF76555D508DE40E0
      85C50878C40F67F7EE842B1592C656E056D09AB2326A98ACA179C643172C8746
      53017F5F2F5CD9FF8E883D1E424127CFD8BC0D5FFF2B9B42A601721A3C71DD4A
      4C183542FCC6E736990B0C87839E94A98CF4FCF4C54BD879E89890453BD2DCD0
      DF4C47C4E2172DABAA26150B5AF53AB45A3D82278C45EAD60DD6D032472829F9
      DB84E6388CA4903874F213486949BF4D4D124BCBCBC59E5B1EFF0ECE5FBD011B
      CAFCF95326E2F7EB4351DFD840FC2D71684ACA96E46393D2F16A2A3AAFC4EDC5
      959BD922E45E7B791E2216CD439DB98FFFECF275AC7CF35D218147B787E3E9C0
      E16D43AB356556ADE9DD123526854550135587B8954B10F2DC143163F6E2AAB7
      DE433AE9346B58D2A6B57861529068B67842EC2D1979D0C1DE8EA44D0D074A34
      D6EC1AF2B4C158673A86CEE3248B3F928E7D1F1E67F145CCCA1084CD9D25C289
      1D36676B2CAE5CCFC2F4C9CFE0E01BEB69FC0633B40C2A653BD07C627E712942
      09F0E3ED5B849739E132AFFD03216FC472DA237E5D2896CE988A5AA3A96761A0
      1FA87824A69DC099BF7E49EB6D2B3C2DA5D0593C6D0AD6CD9B05372785E51A3C
      B11D142AC9E91990D1F8196FC66018C5324F3C27FF2E76D18452C8297C5C8BA7
      5B43AB3556E1C1F1C8E032F282CEFC3D27CAC435E1A8A24C5FF0EC24246D5C23
      CA375BB5BE067187D3702CE3AC580111B3245FF574ACF84C496D4317DC387F36
      36CC9F43C5D0E4391E7BFAEBDB70A7A8188BA64D465CE81293D3A8CA3AD28AB1
      E75BF75E025AD50A5ADFC1CEE5D2CD5B5818130F7757679CDABD4D544093D6DA
      62E6961DB8FE7D8EF83C75FC68442E5E003F6A941A4829AE7D9F8BCDC9075054
      AA11BF07936CA6ED88101365C79CFF260B8BA376C1D7CF1B7F4F49B014A8B64C
      2EA0959D87E6B6B098A4319D967EF59C19222ED91B91D483BC7FFC14EB1176AE
      08C1ABBF9D213CD89CF51C565CA0225252F13E697E23C5FF5E4ADCB9BF0E1249
      EA4A21B3766F32A25F5928C67B98594197AAD5D0EB3BDE23B21758BF5925F8FF
      DBF70AA92844A1812AE33E4A98D9CF4C10BADB9E8525A690EC5DC6F0007F9CD8
      152D26D33C31D1B374D030C929F63D54AAAE41FF74025FDCF82722F61D14FD72
      3A554AD6675E5C4EA66678963906E2E3EF149760E2AA4DF0F7F2C4A7093BE04C
      105D312BE8925286D677690036EE1D2A6803915B5088D1431F1372C67D4762DA
      49D186727872DBB962D673C28BDC297EF06926A6515CB39274F58610EF0F3D3D
      BA09FD532BA6C66B16256509BD5B8C94E2296A804EEE8E125ADD1DEB71682E24
      5B28D98E9C3E0B17EAC45EA5642DA2C43DFA972F60A412BE3E643EC2A995E58A
      DA33D0D498EBBA09ED4C037A4C9D23343A737F2286F8F90A05F9E8DC7944ED4B
      4580BF0F2E26BF65694B7F8E2918DAD3A3E7A05D6840D5AF668A6292F7C9C716
      BDBD98F51D9650AFA1A25DC9E7EFEE12BB9B1E812E2E29814ED73D68EE4B9E5C
      1686222AFF139E1885C3511B51425DDBEAB79390959387318143914145C9F033
      76FD966B504BE0E5E9D973D0ACDF9957BFC58ADD89A61D38EF6AD8D97C9B96BC
      7E21790F0652156D6F2BD565E8A26286D6750B9A8D25EC146D9F924FFC1979B9
      7902F849F2F0B6250B3186B65BDDBDF5A65028E0EDD5C3D0ADE15B4CD2A51B32
      BD06FDDF326B686A0DB5FD009A6FAA7B7B7BF563E8C2FB45FD263C7C69B72FA0
      CBA957506B34BDCDD4A1A9944A71DF5040F3AD81DB793FF689C770ED1957D8C7
      060F126DAFE5E9163F3BBC935FD0DB6CED5AC02303C533453181D60F3FB9D3E3
      0D81C1D0F16DDFFF9539D2C6821B7FEEF09AED8127B61C2AF5E6FB18BD6D1C12
      76D4F25ADF7D05FE03DBB1D1B6F40344A10000000049454E44AE426082}
  end
  object lblPageTitle: TLabel
    Left = 110
    Top = 35
    Width = 155
    Height = 13
    Caption = 'Add, remove, edit noise sources'
  end
  object lbLevel: TLabel
    Left = 52
    Top = 540
    Width = 32
    Height = 13
    Caption = 'Level: '
  end
  object edName: TEdit
    Left = 120
    Top = 216
    Width = 192
    Height = 21
    TabOrder = 0
    Text = 'edName'
    OnChange = edNameChange
  end
  object btSrcColorPick: TButton
    Left = 49
    Top = 604
    Width = 128
    Height = 25
    Caption = 'Colour'
    TabOrder = 1
    OnClick = btSrcColorPickClick
  end
  object cbSource: TComboBox
    Left = 51
    Top = 168
    Width = 261
    Height = 21
    DropDownCount = 100
    TabOrder = 2
    Text = 'cbSource'
    OnChange = cbSourceChange
    OnClick = cbSourceClick
  end
  object edZ: TEdit
    Left = 120
    Top = 297
    Width = 192
    Height = 21
    TabOrder = 3
    Text = '0.0'
    OnChange = EditBoxesChange
  end
  object edY: TEdit
    Left = 120
    Top = 270
    Width = 192
    Height = 21
    TabOrder = 4
    Text = '0.0'
    OnChange = EditBoxesChange
    OnMouseDown = edMouseDown
  end
  object edX: TEdit
    Left = 120
    Top = 243
    Width = 192
    Height = 21
    TabOrder = 5
    Text = '0.0'
    OnChange = EditBoxesChange
    OnMouseDown = edMouseDown
  end
  object btOk: TButton
    Left = 52
    Top = 342
    Width = 128
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 6
    OnClick = btOkClick
  end
  object btCancel: TButton
    Left = 186
    Top = 342
    Width = 128
    Height = 25
    Caption = 'Cancel'
    TabOrder = 7
    OnClick = btCancelClick
  end
  object cbSrcEnabled: TCheckBox
    Left = 51
    Top = 438
    Width = 97
    Height = 17
    Caption = 'Source enabled'
    Checked = True
    State = cbChecked
    TabOrder = 8
    OnClick = cbSrcEnabledClick
  end
  object btSourceAdd: TButton
    Left = 48
    Top = 88
    Width = 128
    Height = 25
    Caption = 'Add'
    TabOrder = 9
    OnClick = btSourceAddClick
  end
  object btSourceRemove: TButton
    Left = 184
    Top = 88
    Width = 128
    Height = 25
    Caption = 'Remove'
    TabOrder = 10
    OnClick = btSourceRemoveClick
  end
  object btSrcSpectEdit: TButton
    Left = 49
    Top = 573
    Width = 128
    Height = 25
    Hint = 'Edit source spectrum'
    Caption = 'Spectrum'
    TabOrder = 11
    OnClick = btSrcSpectEditClick
  end
  object btImportSources: TButton
    Left = 318
    Top = 88
    Width = 128
    Height = 25
    Caption = 'Import CSV'
    TabOrder = 12
    OnClick = btImportSourcesClick
  end
  object btSetMotion: TButton
    Left = 184
    Top = 470
    Width = 128
    Height = 25
    Caption = 'Motion'
    TabOrder = 13
    OnClick = btSetMotionClick
  end
  object cbMovingsource: TCheckBox
    Left = 51
    Top = 474
    Width = 97
    Height = 17
    Caption = 'Moving source'
    TabOrder = 14
    OnClick = cbMovingsourceClick
  end
  object btDuplicateSource: TButton
    Left = 48
    Top = 127
    Width = 128
    Height = 25
    Caption = 'Duplicate'
    TabOrder = 15
    OnClick = btDuplicateSourceClick
  end
  object btShowXSec: TButton
    Left = 185
    Top = 604
    Width = 128
    Height = 25
    Caption = 'Cross section'
    TabOrder = 16
    OnClick = btShowXSecClick
  end
  object Panel1: TPanel
    Left = 49
    Top = 663
    Width = 398
    Height = 194
    BevelKind = bkFlat
    BevelOuter = bvNone
    ParentBackground = False
    ParentColor = True
    TabOrder = 17
    object lbThreshold: TLabel
      Left = 16
      Top = 36
      Width = 95
      Height = 13
      Caption = 'Threshold level (dB)'
    end
    object lbMaxDist: TLabel
      Left = 16
      Top = 94
      Width = 3
      Height = 13
    end
    object lbAverageDist: TLabel
      Left = 16
      Top = 130
      Width = 3
      Height = 13
    end
    object edThreshold: TEdit
      Left = 136
      Top = 33
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object btShowThresholdsGL: TButton
      Left = 11
      Top = 160
      Width = 117
      Height = 25
      Caption = 'Show threshold'
      TabOrder = 1
      OnClick = btShowThresholdsGLClick
    end
    object cbThresholds: TComboBox
      Left = 13
      Top = 60
      Width = 244
      Height = 21
      DropDownCount = 100
      TabOrder = 2
      Text = 'cbThresholds'
      OnChange = cbThresholdsChange
      OnClick = cbThresholdsClick
    end
    object cbShowThresholds: TCheckBox
      Left = 14
      Top = 8
      Width = 379
      Height = 17
      Caption = 
        'Display optional thresholds for this source (ignoring other sour' +
        'ces)'
      TabOrder = 3
      OnClick = cbShowThresholdsClick
    end
  end
  object btDirectivity: TButton
    Left = 185
    Top = 573
    Width = 128
    Height = 25
    Caption = 'Directivity'
    TabOrder = 18
    OnClick = btDirectivityClick
  end
  object btSetTimeSeries: TButton
    Left = 319
    Top = 573
    Width = 128
    Height = 25
    Caption = 'Time series'
    TabOrder = 19
    OnClick = btSetTimeSeriesClick
  end
  object cbDontUseGriddedTL: TCheckBox
    Left = 52
    Top = 506
    Width = 397
    Height = 17
    TabStop = False
    Caption = 'Moving sources retain all calculated levels (memory intensive)'
    TabOrder = 20
    OnClick = cbDontUseGriddedTLClick
  end
  object odImportSources: TOpenDialog
    Filter = 'All files|*.*|CSV files|*.csv'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 544
    Top = 464
  end
end
