object EditWaterFrame: TEditWaterFrame
  Left = 0
  Top = 0
  Width = 630
  Height = 920
  Constraints.MinHeight = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object imPageTitle: TImage
    Left = 48
    Top = 18
    Width = 45
    Height = 45
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000002D0000
      002D08060000003A1AE29A00000006624B474400980098009815E12590000000
      097048597300000B1300000B1301009A9C180000000774494D4507DE03190C34
      1C2860E3E6000005E54944415478DAD5996B6C536518C7FFA73D5DBBB5EBC6D8
      D66D6DD7EECA861943865C365118D729C17889688C9268408917A29F35F18386
      0F62A2899A8072510C891F54125004D44044A61304E42A44195BB776EB2EDDDA
      6EEBF5F8BC6FD731243D3D7CE83A9E6CD9C939CF39FDBD4FFFCFFF7DCF5E41A2
      C05D16C2646876180C85108944D2CD05817E458D061A514C0CCD40FD2323E966
      BD2D542A15F459591004E156E870388C91D1D174F3250CB55ACDC127A0A3D1A8
      3442158E44A3E966930D9D4E870C920B872659483EBF3FDD4C4983C923DB6088
      1D933424AF6FFA43B3C8CD31DE841EF6FAD2CDA32866E4E64C86F6264C64E6A2
      D7EB530E140C06C10C411E3A37061D0A8525CFD0902C7441FECC9443B3BE1A1B
      0BC8E6E4CFCC8B4387A4FE81C18489E42E282E32A51C9A49743489ED9A0A0B62
      D034034A7D7DFDB2D0E69262D9879DEBEA45BDB95036E7D77FBBD0546E4E78DD
      4B12F5F9E527B778F19242EB4435B69DBC88579636A0D868B8E55AE7E030DEFD
      E1248E5E69C7A373ABF1EA8394936340064D062C2251090ECF30B61E6EC5F16B
      1D58DF508B0D8BEA609D618448331D8B20CDC49D03C3700F7A603366412E1443
      676B3568FCE45BCCC8D2A1DE5288EAC23CBE2E38D5E1427BFF10FAFDA3D0D2C0
      02E108F4191A9417E4D23D193CC71F0CA17D2096C3CEF90221E4666A51969FCB
      73798503413EF867EAABB06EB65D21743024B9FBFA1226EA35229EFBFA181C83
      5EB0454A942D55E847AD12789336D88AF1C1132BF0EC9E03E8204036AF0A93EE
      67F9AB6ACBB0F591A5786AE77EFCD3E7E1CF89E7C48F5F6BACC3CA2A8B2C7449
      71913268228368CCC14F24818F8F9F86DB37C24EA134CF882DCBE653F54DD8DB
      7601CF2F9E83B67627F6FE7E01671D3D7C50F714E7E3C979B558682FC1EEDFCE
      E385C67AB45E77E0ABD39771C9D9C767B91A531E36DE7F2F169BF33196A41127
      4107A55E776268268FE59F1DC45B2D4D5837A78A375D167DB515F415EF387116
      BB5AFFE2EB1656D197E8C3372CAE83765CD30C6A0FC1EE387186CB4754ABB079
      C93CAE6DD62BACC651AAF5873F9F428E5AC29A6AAB2C74DC1014412F224D3328
      3335D95C4B11018470C6D18B81713DB34AB6DD70F2A6D2A8D4D48C7A0EDCE5F1
      22148E224B2B92735878338668801A1A5451762CC7417A8ED0B3B734D5616D8D
      4D39744FAF3B61A29A0477D51FC6A67D8778F3C435C8FEAAC901DE7FBC190F56
      9662FFB96B78F3C031720535AD81857165491CECA3F5ABB0A8AC04FBDA2EE21D
      729B4CEA93F8FA98E530B96D5BDB889A994659688BB944193433A68E1078D733
      7B737B47F80756904BBCD1BC80BBC3DB077FC17B8F35A390AAB7FDC49FB8E21A
      E0F7DA48F7AF53CED5DE011AD0716C7FBA854B6B67EB395CA3730C96697AF303
      0DD04643080702CAA15D3DBDB2F258FAE901345558C905ECA82CC8432812E5FE
      FBCDD9BFF107351FD32AABDB5C6ACA259556EEE76A3AE71AF6E1E8E5EB38DFED
      A6EB026F4E669B4C2A4594C38ADDE3F5E3C8A5EB586637617512F7B05ACCCAA1
      9B777EC7A1D864111D7FA554093188FF47947FDD12970FCB5109897338C0F8F5
      CD0B67A3254923DE0AED4A0C6DA0265ABEEB7B08485D70E759301B0FCDB2C69A
      2511B47512B4D3D593189A34B862F75440D726AD74A9D5A20C9AFBF49454FA0E
      A0030CDAE992855EB1FBD09440AF49D288B652AB3268268F957B520FFDE27DAC
      D24AA10341A9DBE9948116B1EAF3C353005D93B4D2765B691C3A207575CB416B
      B0FA8BD4436F9A3F2B297499DDA6145A24E823D30FDAD1D52D03ADC19ABD5303
      BDBAD22C9B575E66570A2D12F4D1E907DDE9E89281D6A0E5CBD4436F6CA84E0A
      5D515EA6145A24E81FA71F7447A723616236557A099F5C5287CD164F2F2FA8C1
      C3497CBAB2A25C19347F68CA706F869292DC11F4748909E83106DDD1996E1E45
      515559711743B3B547FB8D1BE9E6491AA228DEF469B67DE120CB1B4BF25299EE
      30990A91631CDF0960BB5B6CA348CEABD31D999999132F0013D0EC80559A697B
      BA6DE0EA745A7AA1B5F0FDC4DBA059B0FF457B3C43181D1B4DE964A2240415DB
      CDCA86C170FBD6897037EE8DFF07DDD918A7B994B7090000000049454E44AE42
      6082}
  end
  object lblPageTitle: TLabel
    Left = 110
    Top = 35
    Width = 82
    Height = 13
    Caption = 'Water properties'
  end
  object btCancel: TButton
    Left = 142
    Top = 720
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 0
    OnClick = btCancelClick
  end
  object btOk: TButton
    Left = 48
    Top = 720
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 1
    OnClick = btOkClick
  end
  object cbLocationProps: TComboBox
    Left = 224
    Top = 32
    Width = 198
    Height = 21
    TabOrder = 2
    Text = 'cbLocationProps'
    OnChange = cbLocationPropsChange
    OnClick = cbLocationPropsClick
  end
  object pcWater: TPageControl
    Left = 48
    Top = 69
    Width = 545
    Height = 628
    ActivePage = tsSSP
    TabOrder = 3
    object tsSSP: TTabSheet
      Caption = 'Sound speed profile'
      object lblSSPName: TLabel
        Left = 19
        Top = 376
        Width = 27
        Height = 13
        Caption = 'Name'
      end
      object edSSPName: TEdit
        Left = 57
        Top = 373
        Width = 464
        Height = 21
        TabOrder = 0
        OnChange = edSSPNameChange
      end
      object gbSSPDB: TGroupBox
        Left = 19
        Top = 471
        Width = 147
        Height = 114
        Caption = 'Database'
        TabOrder = 1
        object btSaveSSPToDB: TButton
          Left = 11
          Top = 24
          Width = 120
          Height = 25
          Caption = 'Save profile to DB'
          TabOrder = 0
          OnClick = btSaveSSPToDBClick
        end
        object btRemoveSSPFromDB: TButton
          Left = 11
          Top = 70
          Width = 120
          Height = 25
          Caption = 'Remove from DB'
          TabOrder = 1
          OnClick = btRemoveSSPFromDBClick
        end
      end
      object mmSSPComments: TMemo
        Left = 18
        Top = 400
        Width = 503
        Height = 65
        ScrollBars = ssVertical
        TabOrder = 2
        OnChange = mmSSPCommentsChange
      end
      object pcSSPChart: TPageControl
        Left = 7
        Top = 33
        Width = 514
        Height = 334
        ActivePage = tsSSPChart
        MultiLine = True
        TabOrder = 3
        TabPosition = tpRight
        object tsSSPChart: TTabSheet
          Caption = 'Graph'
          object chSSP: TChart
            Left = 3
            Top = 3
            Width = 480
            Height = 320
            Legend.Visible = False
            MarginBottom = 2
            MarginTop = 0
            Title.Text.Strings = (
              'TChart')
            Title.Visible = False
            BottomAxis.Grid.Visible = False
            BottomAxis.Title.Caption = 'Speed of sound c (m/s)'
            LeftAxis.Grid.Visible = False
            LeftAxis.Inverted = True
            LeftAxis.Title.Caption = 'Depth z (m)'
            View3D = False
            Zoom.Allow = False
            BevelOuter = bvNone
            Color = clWhite
            OnDragOver = chSSPDragOver
            TabOrder = 0
            OnDblClick = chSSPDblClick
            OnMouseDown = chSSPMouseDown
            DefaultCanvas = 'TGDIPlusCanvas'
            ColorPaletteIndex = 13
            object srSSPOld: TFastLineSeries
              Selected.Hover.Visible = True
              SeriesColor = 5255457
              LinePen.Color = 5255457
              LinePen.Style = psDot
              TreatNulls = tnDontPaint
              XValues.Name = 'X'
              XValues.Order = loNone
              YValues.Name = 'Y'
              YValues.Order = loAscending
            end
            object srSSP: TLineSeries
              Marks.Font.Color = clScrollBar
              Marks.Transparency = 100
              Marks.Transparent = True
              Marks.Style = smsValue
              Marks.Symbol.Visible = True
              SeriesColor = 14135633
              Brush.BackColor = clDefault
              LinePen.Color = 14135633
              LinePen.EndStyle = esSquare
              Pointer.HorizSize = 3
              Pointer.InflateMargins = True
              Pointer.Pen.Width = 0
              Pointer.Pen.EndStyle = esSquare
              Pointer.Style = psRectangle
              Pointer.VertSize = 3
              Pointer.Visible = True
              XValues.Name = 'X'
              XValues.Order = loNone
              YValues.Name = 'Y'
              YValues.Order = loAscending
            end
          end
        end
        object tsSSPTable: TTabSheet
          Caption = 'Table'
          ImageIndex = 1
          object sgSSP: TStringGrid
            Left = 5
            Top = 5
            Width = 468
            Height = 276
            ColCount = 4
            DefaultColWidth = 120
            FixedCols = 0
            TabOrder = 0
            OnMouseDown = sgSSPMouseDown
            OnSelectCell = sgSSPSelectCell
            OnSetEditText = sgSSPSetEditText
          end
          object btSSPOK: TButton
            Left = 308
            Top = 287
            Width = 75
            Height = 25
            Caption = 'OK'
            TabOrder = 1
            OnClick = btSSPOKClick
          end
          object btSSPCancel: TButton
            Left = 398
            Top = 287
            Width = 75
            Height = 25
            Caption = 'Cancel'
            TabOrder = 2
            OnClick = btSSPCancelClick
          end
          object btPasteFromClipboard: TButton
            Left = 3
            Top = 287
            Width = 75
            Height = 25
            Caption = 'Paste data'
            TabOrder = 3
            OnClick = btPasteFromClipboardClick
          end
        end
      end
      object SSPSelectBox: TComboBox
        Left = 11
        Top = 8
        Width = 510
        Height = 21
        DropDownCount = 100
        TabOrder = 4
        Text = 'SSPSelectBox'
        OnChange = SSPSelectBoxChange
        OnClick = SSPSelectBoxClick
      end
      object gbMunkBox: TGroupBox
        Left = 344
        Top = 471
        Width = 177
        Height = 114
        Caption = 'gbMunkBox'
        TabOrder = 5
        object btMakeMunkSSP: TButton
          Left = 13
          Top = 20
          Width = 148
          Height = 25
          Caption = 'Generate Munk profile'
          TabOrder = 0
          OnClick = btMakeMunkSSPClick
        end
        object edMinSp: TEdit
          Left = 13
          Top = 51
          Width = 148
          Height = 21
          TabOrder = 1
          Text = 'Lowest soundspeed'
        end
        object edMinSpDepth: TEdit
          Left = 13
          Top = 78
          Width = 148
          Height = 21
          TabOrder = 2
          Text = 'Depth of lowest soundspeed'
        end
      end
    end
    object tsCurrent: TTabSheet
      Caption = 'Current and tide'
      ImageIndex = 1
      object lblCurrentDirection: TLabel
        Left = 16
        Top = 43
        Width = 97
        Height = 13
        Caption = 'Ocean current (m/s)'
      end
      object lblDirection: TLabel
        Left = 193
        Top = 43
        Width = 58
        Height = 13
        Caption = 'Direction ('#176')'
      end
      object lbTide: TLabel
        Left = 16
        Top = 154
        Width = 39
        Height = 13
        Caption = 'Tide (m)'
      end
      object pbDirection: TPaintBox
        Left = 323
        Top = 33
        Width = 79
        Height = 79
        OnMouseDown = pbDirectionMouseDown
        OnPaint = pbDirectionPaint
      end
      object btTideSet: TButton
        Left = 193
        Top = 150
        Width = 41
        Height = 22
        Caption = 'Set'
        TabOrder = 0
        TabStop = False
        OnClick = btTideSetClick
      end
      object edCurrent: TEdit
        Left = 123
        Top = 38
        Width = 64
        Height = 21
        TabOrder = 1
        Text = '0'
        OnChange = otherPropsChange
      end
      object edDirection: TEdit
        Left = 253
        Top = 40
        Width = 64
        Height = 21
        TabOrder = 2
        Text = '0'
        OnChange = otherPropsChange
        OnEnter = edDirectionEnter
        OnExit = edDirectionExit
      end
      object edTide: TEdit
        Left = 66
        Top = 151
        Width = 121
        Height = 21
        TabOrder = 3
        Text = '0'
      end
    end
    object tsTemperature: TTabSheet
      Caption = 'Temperature and salinity'
      ImageIndex = 2
      object gbProps: TGroupBox
        Left = 11
        Top = 402
        Width = 147
        Height = 105
        Caption = 'Database'
        TabOrder = 0
        object btRemoveWaterPropsFromDB: TButton
          Left = 11
          Top = 65
          Width = 120
          Height = 25
          Caption = 'Remove from DB'
          TabOrder = 0
          OnClick = btRemoveWaterPropsFromDBClick
        end
        object btSaveWaterPropsToDB: TButton
          Left = 11
          Top = 28
          Width = 120
          Height = 25
          Caption = 'Save properties to DB'
          TabOrder = 1
          OnClick = btSaveWaterPropsToDBClick
        end
      end
      object pcAttenuationChart: TPageControl
        Left = 7
        Top = 11
        Width = 514
        Height = 385
        ActivePage = tsEditAttenuation
        MultiLine = True
        TabOrder = 1
        TabPosition = tpRight
        object tsEditAttenuation: TTabSheet
          Caption = 'Edit'
          object lblSalinity: TLabel
            Left = 16
            Top = 104
            Width = 61
            Height = 13
            Caption = 'Salinity (ppt)'
          end
          object lblTemp: TLabel
            Left = 16
            Top = 77
            Width = 49
            Height = 13
            Caption = 'Temp ('#176'C)'
          end
          object lblWaterName: TLabel
            Left = 16
            Top = 48
            Width = 27
            Height = 13
            Caption = 'Name'
          end
          object edWaterName: TEdit
            Tag = 1
            Left = 94
            Top = 45
            Width = 379
            Height = 21
            TabOrder = 0
            Text = 'EditName'
            OnChange = WaterPropsBoxesChange
          end
          object WaterPropsSelectBox: TComboBox
            Left = 16
            Top = 18
            Width = 457
            Height = 21
            DropDownCount = 100
            TabOrder = 1
            Text = 'WaterSelectBox'
            OnChange = WaterPropsSelectBoxChange
            OnClick = WaterPropsSelectBoxClick
          end
          object edSalinity: TEdit
            Tag = 1
            Left = 94
            Top = 99
            Width = 379
            Height = 21
            TabOrder = 2
            Text = '0'
            OnChange = WaterPropsBoxesChange
          end
          object edTemp: TEdit
            Tag = 1
            Left = 94
            Top = 72
            Width = 379
            Height = 21
            TabOrder = 3
            Text = '0'
            OnChange = WaterPropsBoxesChange
          end
          object MemoWaterComments: TMemo
            Tag = 1
            Left = 16
            Top = 136
            Width = 457
            Height = 77
            Lines.Strings = (
              'MemoComments')
            ScrollBars = ssVertical
            TabOrder = 4
            OnChange = WaterPropsBoxesChange
          end
        end
        object tsAttenuationGraph: TTabSheet
          Caption = 'Graph'
          ImageIndex = 1
          object chAttenuation: TChart
            Tag = 1
            Left = 3
            Top = 3
            Width = 480
            Height = 358
            LeftWall.Size = 3
            Legend.Visible = False
            MarginBottom = 0
            MarginTop = 0
            Title.Font.Color = clBlack
            Title.Text.Strings = (
              'Sound attenuation with distance')
            BottomAxis.Grid.Visible = False
            BottomAxis.Logarithmic = True
            BottomAxis.Title.Caption = 'f (Hz)'
            DepthAxis.Automatic = False
            DepthAxis.AutomaticMaximum = False
            DepthAxis.AutomaticMinimum = False
            DepthAxis.Maximum = 0.680000000000000800
            DepthAxis.Minimum = -0.319999999999999400
            DepthTopAxis.Automatic = False
            DepthTopAxis.AutomaticMaximum = False
            DepthTopAxis.AutomaticMinimum = False
            DepthTopAxis.Maximum = 0.680000000000000800
            DepthTopAxis.Minimum = -0.319999999999999400
            LeftAxis.Grid.Visible = False
            LeftAxis.LabelsFormat.Font.OutLine.Width = 4
            LeftAxis.Title.Caption = 'Attenuation    log10(dB/km)'
            RightAxis.Automatic = False
            RightAxis.AutomaticMaximum = False
            RightAxis.AutomaticMinimum = False
            View3D = False
            Zoom.Allow = False
            BevelOuter = bvNone
            Color = clWhite
            TabOrder = 0
            DefaultCanvas = 'TGDIPlusCanvas'
            PrintMargins = (
              15
              19
              15
              19)
            ColorPaletteIndex = 13
            object srAttenuationOld: TFastLineSeries
              SeriesColor = 4470016
              Title = 'tmpAttenuation'
              LinePen.Color = 4470016
              LinePen.Style = psDot
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Y'
              YValues.Order = loNone
            end
            object srAttenuation: TFastLineSeries
              SeriesColor = 10389298
              Title = 'tmpAttenuation'
              LinePen.Color = 10389298
              LinePen.Width = 3
              TreatNulls = tnDontPaint
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Y'
              YValues.Order = loNone
            end
          end
        end
      end
    end
  end
end
