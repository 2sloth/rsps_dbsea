{ ******************************************************* }
{ }
{ dBSea Underwater Acoustics }
{ }
{ Copyright (c) 2013 2014 dBSea }
{ }
{ ******************************************************* }

unit EditSetupFrame;

interface

uses
    syncommons,
    system.UITypes, system.SysUtils, system.StrUtils, system.math,
    system.Variants, system.Classes,
    winapi.Windows, winapi.Messages,
    vcl.Graphics, vcl.Controls, vcl.Forms,
    vcl.Dialogs, vcl.StdCtrls, vcl.ComCtrls, vcl.ExtCtrls,
    Vcl.Menus, vcl.imaging.pngimage, vcl.imaging.gifimg,
    generics.collections,
    baseTypes, helperFunctions, probe, soundSource, pos3,
    scenarioDiffForm, flock, flockInput, flockOutput,
    mcklib, GLWindow, WorldScaleDialog, dbseaoptions, eventLogging,
    globalsUnit, problemContainer, commentUnit, bathymetry,
    HASPSecurity, MyFrameInterface, dbseaconstants, SmartPointer,
    LocationPropertiesFormUnit, geooverlaysFormUnit,
    ImageOverlayAnchorFormUnit, ImportedLocationUnit;

type

    TPaintboxList = TObjectList<TPaintbox>;

    TSetupFrame = class(TFrame, IMyFrame)
        edNorthing : TEdit;
        edEasting : TEdit;
        lblNorthing : TLabel;
        lblEasting : TLabel;
        lblWorldScale : TLabel;
        btSetupCrossSec: TButton;
        btSetWorldScale: TButton;
        lblXGridPoints: TLabel;
        lblYGridPoints: TLabel;
    btOk: TButton;
    btCancel: TButton;
    edIMax: TEdit;
    edJMax: TEdit;
        lblDepthPoints: TLabel;
    edDMax: TEdit;
        udDepthPoints: TUpDown;
        udIMax: TUpDown;
        udJMax: TUpDown;
        imPageTitle: TImage;
        lblPageTitle: TLabel;
        lblSolutionSlices: TLabel;
        lblRangePoints: TLabel;
        edNSlices: TEdit;
        udSrcNSlices: TUpDown;
    edSrcIMax: TEdit;
        udSrcRangePoints: TUpDown;
        btLoadBathymetry: TButton;
        gbGrid: TGroupBox;
        gbSourceSolution: TGroupBox;
        gbCoordinateSystem: TGroupBox;
    lblSetupScenario: TLabel;
    btMakeDiff: TButton;
    btRemoveDiff: TButton;
    btFlocking: TButton;
    btAspectRatio: TButton;
    cbUseGPU: TCheckBox;
    puSetWorldScale: TPopupMenu;
    miSetFromEW: TMenuItem;
    miSetFromNS: TMenuItem;
    miSetFromDiagonal: TMenuItem;
    miSetBy2Points: TMenuItem;
    btComments: TButton;
    btShowSizes: TButton;
    sdExportXSec: TSaveDialog;
    btLocationProperties: TButton;
    btGeoOverlays: TButton;
    btTestShowMovingPosIndexUp: TButton;
    btTestShowMovingPosIndexDown: TButton;
    btSetImageOverlay: TButton;
    odImage: TOpenDialog;
    btGLSetOverlay: TButton;
    lbUTMZone: TLabel;
    edUTMZone: TEdit;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure frameshow(Sender : TObject);
    procedure framehide(Sender : TObject);
    procedure EditBoxesInit;

    procedure EditBoxesChange(Sender: TObject);
    procedure setModified(const val : boolean);
    procedure setButtonsEnabled(const val : boolean);

    procedure btCrossSecClick(Sender: TObject);
    procedure btNoCrossSecClick(Sender: TObject);
    procedure btSetupCrossSecClick(Sender: TObject);
    procedure btSetWorldScaleClick(Sender: TObject);
    procedure SetWorldScaleWithTag(Sender: TObject);

    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btLoadBathymetryClick(Sender: TObject);

    procedure btMakeDiffClick(Sender: TObject);
    procedure btRemoveDiffClick(Sender: TObject);
    procedure btFlockingClick(Sender: TObject);
    procedure btAspectRatioClick(Sender: TObject);
    procedure cbUseGPUClick(Sender: TObject);
    procedure FrameMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btCommentsClick(Sender: TObject);
    procedure btShowSizesClick(Sender: TObject);
    procedure btTestSrcIndsClick(Sender: TObject);
    procedure btLocationPropertiesClick(Sender: TObject);
    procedure btGeoOverlaysClick(Sender: TObject);
    procedure btTestShowMovingPosIndexUpClick(Sender: TObject);
    procedure btSetImageOverlayClick(Sender: TObject);
    procedure btGLSetOverlayClick(Sender: TObject);
    procedure edUTMZoneChange(Sender: TObject);
    private
        updateFromCode  : boolean;
        modified        : boolean;
        comments        : TCommentList;
        commentBoxes    : TPaintboxList;
        didShowEndpointsMessage: boolean;

        procedure updateComments();
        procedure showWorldScaleDialog(const settingStyle: integer; out P1,P2 : TFPoint);
        procedure setEastingNorthing(const P1,P2 : TFPoint; const x1,x2,y1,y2 : double);
        procedure didChangeUnits;
        procedure didChangeScenario;
    public
    end;

var
    framSetup               : TSetupFrame;

implementation

{$R *.dfm}

uses Main, editsolveframe;

procedure TSetupFrame.cbUseGPUClick(Sender: TObject);
begin
    bUseGPU := cbUseGPU.Checked;
end;

constructor TSetupFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        udIMax.Associate            := edimax;
        udJMax.Associate            := edjmax;
        udDepthPoints.Associate     := edDmax;

        udSrcNSlices.Associate      := EdnSlices;
        udSrcRangePoints.Associate  := EdSRCimax;

        {$IFDEF DEBUG}
            btMakeDiff.Visible := true;
            btRemoveDiff.Visible := true;
            btFlocking.Visible := true;
            cbUseGPU.Visible := true;
            btComments.Visible := true;
            btTestShowMovingPosIndexUp.Visible := true;
            btTestShowMovingPosIndexDown.Visible := true;
        {$ENDIF}

        {$IF defined (DEBUG) or defined(VER2)}
            btGeoOverlays.Visible := true;
            btLocationProperties.Visible := true;
            btSetImageOverlay.Visible := true;
            btGLSetOverlay.Visible := true;
        {$ELSE}
            btGeoOverlays.Visible := false;
        {$ENDIF}

        didShowEndpointsMessage := false;

        comments := TCommentList.create(false);
        commentBoxes := TPaintboxList.create(true);

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

destructor TSetupFrame.Destroy;
begin
    comments.Free;
    commentBoxes.Free;

    inherited;
end;

procedure TSetupFrame.didChangeScenario;
begin
    frameShow(nil);
end;

procedure TSetupFrame.didChangeUnits;
begin
    EditBoxesInit;
end;

procedure TSetupFrame.frameshow(Sender : TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'frameshow');
    try
        updateFromCode := true;
        self.updateComments;
        EditBoxesInit;
        updateFromCode := false;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TSetupFrame.framehide(Sender : TObject);
begin
    btOkClick(self);
end;

procedure TSetupFrame.updateComments();
var
    comment : TComment;
    pb : TPaintbox;
begin
    container.scenario.commentsForFrame(self,comments);
    self.commentBoxes.clear;
    for comment in comments do
    begin
        pb := TPaintbox.create(self);
        pb.left := comment.x;
        pb.top := comment.y;
        pb.width := 5;
        pb.height := 5;
        pb.canvas.brush.color := clBlack;
        pb.canvas.rectangle(pb.clientrect);
        self.commentBoxes.add(pb);
    end;
end;

procedure TSetupFrame.FrameMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    if button = mbRight then
    begin
        //container.scenario.comments.add(TComment.create('some Comment',x,y,self));
        //self.updateComments;
    end;
end;

procedure TSetupFrame.EditBoxesInit;
var
    sUnits: String;
begin
    updateFromCode := true;
    try
        EdEasting .Text := container.geoSetup.makeXString(0);
        EdNorthing.Text := container.geoSetup.makeYString(0);

        sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');

        lblNorthing.Caption := 'Northing ' + sUnits;
        lblEasting.Caption  := 'Easting '  + sUnits;

        {NB j,y is the first dimension in the array}
        lblWorldScale.Caption := 'Project area: x length = ' +
                tostr(saferound(m2f(container.scenario.theRA.xMax, UWAOpts.dispFeet))) + ' ' + sUnits +
                ', y length = ' + tostr(saferound(m2f(container.scenario.theRA.yMax, UWAOpts.dispFeet))) + ' ' + sUnits;

        udIMax.Position := container.scenario.theRA.imax;
        udJMax.Position := container.scenario.theRA.jmax;
        udDepthPoints.Position := container.scenario.dmax;

        udSrcNSlices.Position := container.scenario.SourcesNSlices;
        udSrcRangePoints.Position := container.scenario.SourcesIMax;

        self.cbUseGPU.Checked := bUseGPU;

        importedLocation.setZoneWith(container.geoSetup.utmZone);
        edUTMZone.Text := importedLocation.zoneToString;

    finally
        setModified(false);
        updateFromCode := false;
    end;
end;

procedure TSetupFrame.edUTMZoneChange(Sender: TObject);
begin
    if updateFromCode then exit;
    importedLocation.setZoneWith(edUTMZone.Text);
    container.geoSetup.utmZone := importedLocation.zoneToString;
end;

procedure TSetupFrame.setModified(const val: boolean);
begin
    if modified <> val then
    begin
        modified := val;
        setButtonsEnabled(modified);
    end;
end;

procedure TSetupFrame.setButtonsEnabled(const val: boolean);
begin
    self.btOk.Enabled := val;
    self.btCancel.Enabled := val;
end;

procedure TSetupFrame.EditBoxesChange(Sender: TObject);
begin
    if not updatefromcode then
        setModified(true);
end;

procedure TSetupFrame.btCrossSecClick(Sender: TObject);
begin
    {we no longer need to show the endpoints}
    UWAOpts.setVisibility(ord(veEndpoints),false);
    {plot the X-sec}
    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := true;
        GLForm.forceDrawNow;
    end;
end;

procedure TSetupFrame.btNoCrossSecClick(Sender: TObject);
begin
    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := false;
        GLForm.drawnow;
    end;
end;

procedure TSetupFrame.btAspectRatioClick(Sender: TObject);
begin
    if container.bathymetry.xAspectRatio = 1 then
    begin
        if modified and isint(edImax.Text) then
            edJMax.Text := tostr(saferound(toInt(edImax.Text) * container.bathymetry.yAspectRatio))
        else
            edJMax.Text := tostr(saferound(container.scenario.theRA.iMax * container.bathymetry.yAspectRatio))
    end
    else
        if modified and isint(edJMax.Text) then
            edIMax.Text := tostr(saferound(toint(edJmax.Text) * container.bathymetry.xAspectRatio))
        else
            edIMax.Text := tostr(saferound(container.scenario.theRA.jMax * container.bathymetry.xAspectRatio));
    setModified(true);
end;

procedure TSetupFrame.btCancelClick(Sender: TObject);
begin
    updateFromCode := true;
    EditBoxesInit;
    setModified(false);
    updateFromCode := false;
end;

procedure TSetupFrame.btCommentsClick(Sender: TObject);
var
    comment: TComment;
    s: string;
begin
    container.scenario.commentsForFrame(self,comments);
    s := '';
    for comment in comments do
        s := s + comment.comment + ' ' + inttostr(comment.x) + ' ' + inttostr(comment.y) + #13#10;
    ShowMessage(s);
end;

procedure TSetupFrame.btOkClick(Sender: TObject);
    function checkForConfirmation : boolean;
    var
        buttonSelected : integer;
        s : string;
        src : TSource;
    begin
        buttonSelected := mrOK;
        s := '';
        if  container.scenario.movingSourcesUseGriddedTL then
            for src in container.scenario.sources do
                if src.isMovingSource then
                    s := s + tostr(container.scenario.sources.IndexOf(src) + 1) + ': ' + src.name + '  ';
        if length(s) > 0 then
            buttonSelected := MessageDlg('This will delete the calculated levels in the following sources: ' + s + ', proceed?',mtConfirmation, mbOKCancel, 0);
        result := buttonSelected = mrOK;
    end;

begin
    if not modified then Exit;
    if container.isSolving then Exit;

    {clicking other buttons during some of these functions, which can seem like they are taking a long time,
    and cause problems. inactivate the form temporarily.}
    MainForm.Enabled := false;
    try
        btOk.enabled := false;

        {set each of the values to the text in the boxes. checking that
        the values are ok is done within each setfoo function}

        if  isFloat(ednSlices.Text) and
            isFloat(edSrcImax.Text) then
        begin
            container.scenario.setSourcesNSlices(toint(ednSlices.Text),true);
            container.scenario.setSourcesIMax(toint(edSrcImax.Text),true);
            eventLog.log('Set source radial slices ' + tostr(container.scenario.sourcesNSlices) + ', range points ' + tostr(container.scenario.sourcesIMax));
        end
        else
        begin
            if isFloat(ednSlices.Text) then
                container.scenario.setSourcesNSlices(toint(ednSlices.Text));
            if isFloat(edSrcImax.Text) then
                container.scenario.setSourcesIMax(toint(edSrcImax.Text));
        end;

        if isFloat(edimax.Text) and
            isFloat(edjmax.Text) and
            isFloat(edDmax.Text) then
        begin
            if (toint(edimax.Text) <> container.scenario.theRA.iMax) or
               (toint(edjmax.Text) <> container.scenario.theRA.jMax) or
               (toint(eddmax.Text) <> container.scenario.dMax) then
            begin
                if checkForConfirmation then
                begin
                    container.scenario.theRA.setIMax(toint(edimax.Text));
                    container.scenario.theRA.setJMax(toint(edjmax.Text));
                    container.scenario.setDMax(toint(edDmax.Text),true);
                    eventLog.log('Set grid x points ' + tostr(container.scenario.theRA.iMax) +
                                 ', y points ' + tostr(container.scenario.theRA.jmax) +
                                 ', z points ' + tostr(container.scenario.dMax));
                end;
            end;
        end
        else
        begin
            if isFloat(edimax.Text) and checkForConfirmation then
                container.scenario.theRA.setIMax(toint(edimax.Text));
            if isFloat(edjmax.Text) and checkForConfirmation then
                container.scenario.theRA.setJMax(toint(edjmax.Text));
            if isFloat(edDmax.Text) and checkForConfirmation then
                container.scenario.setDMax(toint(edDmax.Text));
        end;

        {northing, easting}
        if isFloat(EdNorthing.Text) and isFloat(EdEasting.Text) then
        begin
            container.geoSetup.setNorthing(f2m(tofl(EdNorthing.Text), UWAOpts.dispFeet));
            container.geoSetup.setEasting (f2m(tofl(EdEasting.Text ), UWAOpts.dispFeet));
            eventLog.log('Set northing ' + tostrWithTestAndRound1(container.geoSetup.Northing) +
                          ', easting ' + tostrWithTestAndRound1(container.geoSetup.Easting));
        end;

        EditBoxesInit;
        setModified(false);

        if assigned(GLForm) then
        begin
            GLForm.setRedrawResults();
            GLForm.force2DrawNow;
        end;
    finally
        MainForm.Enabled := true;
    end;
end;

procedure TSetupFrame.btSetImageOverlayClick(Sender: TObject);
var
    bmp: TBitmap;
begin
    ImageOverlayAnchorForm.image.cloneFrom(container.image);
    if ImageOverlayAnchorForm.showmodal <> mrOK then exit;

    container.image.cloneFrom(ImageOverlayAnchorForm.image);

    if not container.image.valid then
    begin
        GLForm.clearImageOverlayTexture;
        GLForm.setRedrawBathymetry;
        GLForm.force2DrawNow;
        exit;
    end;

    bmp := container.image.toBMP;
    try
        GLForm.setImageOverlayTexture(bmp);
        GLForm.setRedrawBathymetry;
        GLForm.force2DrawNow;
    finally
        bmp.free;
    end;
end;

procedure TSetupFrame.btSetupCrossSecClick(Sender: TObject);
var
    shouldRedrawGL: boolean;
begin
    container.CrossSecReady(shouldRedrawGL);

    if not didShowEndpointsMessage then
    begin
        ShowMessage('Click and drag the endpoints to set the cross section' + #13#10#13#10
            + 'Press 1 - 9 to move the left end of the cross section to the corresponding source' + #13#10#13#10
            + 'Press enter or click the Cross section view toolbar button once you have set the endpoints' + #13#10#13#10
            + 'You can move the cross section using the small window at top right in the cross section view' + #13#10#13#10
            + 'You can edit the cross section on the full map again by pressing Setup cross-section on the Setup project tab');
        didShowEndpointsMessage := true;
    end;

    {set the xsec endpoints visible and reset and redraw the 3d glwin}
    UWAOpts.setVisibility(ord(veEndpoints),true);
    if assigned(GLForm) then
    begin
        GLForm.bDrawCrossSection := false;
        GLForm.resetView;
        GLForm.forcedrawnow;
    end;
end;

procedure TSetupFrame.btSetWorldScaleClick(Sender: TObject);
var
    pnt: TPoint;
begin
    if GetCursorPos(pnt) then
        TButton(sender).PopupMenu.Popup(pnt.X, pnt.Y);
end;

procedure TSetupFrame.btShowSizesClick(Sender: TObject);
var
    sUnits: string;
begin
    sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');
    showmessage('dx ' + sUnits + ': '   + tostrWithTestAndRound1(m2f(container.scenario.theRA.dx, UWAOpts.dispFeet)) + #13#10 +
                'dy ' + sUnits + ': ' + tostrWithTestAndRound1(m2f(container.scenario.theRA.dy, UWAOpts.dispFeet)) + #13#10 +
                'dz ' + sUnits + ': ' + tostrWithTestAndRound1(m2f(container.scenario.dz, UWAOpts.dispFeet)) + #13#10 +
                'slice step angle (degrees): ' + tostrWithTestAndRound1(360 / container.scenario.sourcesNSlices) + #13#10 +
                'range step ' + sUnits + ': ' + tostrWithTestAndRound1(m2f(container.scenario.sources.First.dr, UWAOpts.dispFeet)));
end;

procedure TSetupFrame.btTestShowMovingPosIndexUpClick(Sender: TObject);
begin
    if GLForm.resultsMovingPosIndForGIFTool < 0 then
        GLForm.resultsMovingPosIndForGIFTool := 0
    else if TControl(sender).Tag = 0 then
        inc(GLForm.resultsMovingPosIndForGIFTool)
    else
        dec(GLForm.resultsMovingPosIndForGIFTool);
    GLForm.setRedrawResults;
    GLForm.force2DrawNow;
end;

procedure TSetupFrame.btTestSrcIndsClick(Sender: TObject);
begin
    {$IF Defined(DEBUG)}
    container.scenario.sources.First.testIndicesCalcs;
    {$ENDIF}
end;

procedure TSetupFrame.SetWorldScaleWithTag(Sender: TObject);
var
    points : TFPointScaleClicks;
begin
    if sender is TMenuItem then
    begin
        if container.isSolving then Exit;

        if assigned(GLForm) then
        begin
            eventLog.log('Set world scale ' + tostr(TMenuItem(sender).Tag));
            if TMenuItem(sender).Tag = 3 then
                points := GLForm.get2ClicksForSetScale();
            self.showWorldScaleDialog(TMenuItem(sender).Tag, points[TClickInd.kStart], points[TClickInd.kEnd]);
        end;
        GLForm.setRedrawResults();
        GLForm.force2DrawNow;
        EditBoxesInit;
    end;
end;

procedure TSetupFrame.showWorldScaleDialog(const settingStyle: integer; out P1,P2 : TFPoint);
var
    x1,x2,y1,y2 : double;
    dist : double;
begin
    if isNaN(container.geoSetup.worldscale) then
        frmWorldScale.edScale.Text := ''
    else
        frmWorldScale.edScale.Text := tostr(saferound(m2f(container.geoSetup.worldscale, UWAOpts.dispFeet)));

    if settingStyle <> 3 then
    begin
        P1.x := 0;
        P1.y := 0;
        P2.x := 0;
        P2.y := 0;
        case settingStyle of
            0: P2.x := container.geoSetup.worldscale * container.bathymetry.xAspectRatio;
            1: P2.y := container.geoSetup.worldscale * container.bathymetry.yAspectRatio;
            2: begin
                P2.x := container.geoSetup.worldscale * container.bathymetry.xAspectRatio;
                P2.y := container.geoSetup.worldscale * container.bathymetry.yAspectRatio;
            end;
        end;
    end;

    if not (isnan(container.geoSetup.easting) and isnan(container.geoSetup.northing)) then
    begin
        {set text in the dialog x and y boxes}
        frmWorldScale.edAx.Text := container.geoSetup.makeXString(P1.X);
        frmWorldScale.edAy.Text := container.geoSetup.makeYString(P1.Y);
        frmWorldScale.edBx.Text := container.geoSetup.makeXString(P2.X);
        frmWorldScale.edBy.Text := container.geoSetup.makeYString(P2.Y);
    end;
    frmWorldScale.edScale.Text := tostr(saferound(m2f(TMCKLib.Distance2D(P1.X,P2.X,P1.Y,P2.Y),UWAOpts.dispFeet)));
    frmWorldScale.setLabelsForSettingStyle(settingStyle);

    if frmWorldScale.ShowModal = mrOK then
    begin
        container.clearLevelsCaches;
        if frmWorldScale.rbDistBetweenPoints.Checked then
        begin
            {can only set worldscale, no northing or easting}
            dist := f2m(tofl(frmWorldScale.edScale.Text), UWAOpts.dispFeet);
            container.geoSetup.setWorldScaleWithPoints(P1,P2,dist);
        end
        else
        begin
            {we have 4 points and can therefore calculate both an overall scale, and also the
            offsets Easting and Northing}

            if not (isFloat(frmWorldScale.edAx.Text) and isFloat(frmWorldScale.edAy.Text) and
                    isFloat(frmWorldScale.edBx.Text) and isFloat(frmWorldScale.edBx.Text)) then
            begin
                showmessage('Invalid entry in one or more edit boxes, unable to set world scale');
                Exit;
            end
            else
            begin
                x1 := f2m(tofl(frmWorldScale.edAx.Text),UWAOpts.DispFeet);
                y1 := f2m(tofl(frmWorldScale.edAy.Text),UWAOpts.DispFeet);
                x2 := f2m(tofl(frmWorldScale.edBx.Text),UWAOpts.DispFeet);
                y2 := f2m(tofl(frmWorldScale.edBy.Text),UWAOpts.DispFeet);
                dist := TMCKLib.Distance2D(x1,x2,y1,y2);
            end;

            container.geoSetup.setWorldScaleWithPoints(P1,P2,dist);
            self.setEastingNorthing(P1,P2,x1,x2,y1,y2);
        end;
    end;
end;

procedure TSetupFrame.setEastingNorthing(const P1,P2 : TFPoint; const x1,x2,y1,y2 : double);
var
    gx1,gx2,gy1,gy2,m,c : double;
begin
    {convert those to 'gl coords' but with pan and zoom taken out, ie each is then an idealised
    gl pos from -1 to 1. Therefore the easting and northing origin point is at -1,-1}
    gx1 := container.geoSetup.w2g(P1.X);
    gx2 := container.geoSetup.w2g(P2.X);
    gy1 := container.geoSetup.w2g(P1.Y);
    gy2 := container.geoSetup.w2g(P2.Y);

    {do y = mx + c in each direction, then find y at x = -1 (here, y and x are NOT onscreen directions,
    jsut the classic y and x graph you used in high school). The y value at x = -1 is either the northing
    or easting.}
    m := (x2 - x1)/(gx2 - gx1);
    c := x1 - gx1*m;
    container.geoSetup.setEasting (-1*m + c);
    m := (y2 - y1)/(gy2 - gy1);
    c := y1 - gy1*m;
    container.geoSetup.setNorthing(-1*m + c);
end;

procedure TSetupFrame.btMakeDiffClick(Sender: TObject);
var
    f: ISmart<TScenarioDiffFrm>;
begin
    //if container.scenarios.count < 2 then exit;

    f := TSmart<TScenarioDiffFrm>.create(TScenarioDiffFrm.create(self));
    if f.ShowModal = mrOK then
    begin
        container.levelServer.setDiffScenarios(f.diff.s1,f.diff.s2);
        container.levelServer.updateDiff(UWAOpts.levelsDisplay);
        container.levelServer.useDiff := true;
        if assigned(GLForm) then
        begin
            GLForm.setRedrawResults;
            GLForm.force2DrawNow;
        end;
    end;
end;

procedure TSetupFrame.btRemoveDiffClick(Sender: TObject);
begin
    //container.levelServer.invalidateDiff;
    container.levelServer.useDiff := false;
    if assigned(GLForm) then
    begin
        GLForm.setRedrawResults;
        GLForm.force2DrawNow;
    end;
end;

procedure TSetupFrame.btFlockingClick(Sender: TObject);
var
    totalTime : double;
    threshold : single;
    nAnimats : integer;
    stepTime : double;
    updateGLEveryNSteps, nSteps : integer;
    bAnimatsReactToSoundLevels : boolean;
    parallel, bSimpleFleeing : boolean;
begin
    //if not container.scenario.resultsExist then exit;

    if flockInputForm.ShowModal <> mrOK then
        exit;
    //checking for isfloat etc happens on the form
    nAnimats            := toint(flockInputForm.edNAnimats.Text);
    totalTime           := tofl (flockInputForm.edTotalTime.Text);
    threshold           := container.scenario.unconvertLevel( tofl (flockInputForm.edThreshold.Text) );
    updateGLEveryNSteps := toint(flockInputForm.edGLUpdate.Text);
    stepTime            := tofl (flockInputForm.edStepTime.Text);
    GLForm.showFlock    := flockInputForm.cbGLUpdate.Checked;
    bAnimatsReactToSoundLevels := flockInputForm.cbThreshold.Checked;
    parallel            := flockInputForm.cbParallel.Checked;
    bSimpleFleeing      := flockInputForm.cbSimpleFleeing.checked;

    container.cancelSolve.b := false;

    nSteps := safefloor(totalTime / stepTime);
    framsolve.initFlockUI(nSteps);
    mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Solve);
    container.isSolving := true;
    container.scenario.flock.runFlock(GLForm.forceDrawNow,
                                      GLForm.setRedrawResults,
                                      {var}GLForm.currentLevsRA,
                                      container.cancelSolve,
                                      nSteps,
                                      nAnimats,
                                      updateGLEveryNSteps,
                                      threshold,
                                      totalTime,
                                      stepTime,
                                      bAnimatsReactToSoundLevels,
                                      parallel,
                                      bSimpleFleeing,
                                      framSolve.stepProgressBars,
                                      container.scenario.theRA.iMax,
                                      container.scenario.theRA.jMax,
                                      container.scenario.dMax,
                                      container.scenario.theRA.dx,
                                      container.scenario.theRA.dy,
                                      container.scenario.dz,
                                      container.scenario.sources);
    container.isSolving := false;
    mainform.mainPageControl.ActivePageIndex := ord(TMyPages.Setup);
    framSolve.endFlockUI;

    GLForm.showFlock := false;

    flockOutputForm.outputPerNSteps := updateGLEveryNSteps;
    flockOutputForm.stepTime := stepTime;
    flockOutputForm.flock := container.scenario.flock;
    flockOutputForm.exportChecksFunc := theHASPSecurity.exportChecks;
    flockOutputForm.defaultSavePath := defaultSavePath;
    flockOutputForm.ShowModal;

    container.scenario.flock.clear;
    GLForm.force2DrawNow;
end;

procedure TSetupFrame.btGeoOverlaysClick(Sender: TObject);
begin
    GeoOverlaysForm.overlays := container.geoOverlays;
    GeoOverlaysForm.ShowModal;
    GeoOverlaysForm.overlays := nil;
    GLForm.force2DrawNow;
end;

procedure TSetupFrame.btGLSetOverlayClick(Sender: TObject);
begin
    GLForm.setSettingOverlayScale(not GLForm.getSettingOverlayScale);
    GLForm.forceDrawNow;
end;

procedure TSetupFrame.btLoadBathymetryClick(Sender: TObject);
begin
    mainform.miLoadbathymetryClick(self);
end;

procedure TSetupFrame.btLocationPropertiesClick(Sender: TObject);
begin
    GLForm.bathyColorsBMP(LocationPropertiesForm.bmp);
    //LocationPropertiesForm.propsMap := container.scenario.propsMap;
    LocationPropertiesForm.ShowModal;
end;

end.
