object EditSolveFrame: TEditSolveFrame
  Left = 0
  Top = 0
  Width = 630
  Height = 800
  Constraints.MinHeight = 800
  TabOrder = 0
  object lbSolving: TLabel
    Left = 46
    Top = 80
    Width = 46
    Height = 13
    Caption = 'Solving...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lblSolveCancelling: TLabel
    Left = 50
    Top = 357
    Width = 88
    Height = 13
    Caption = 'Cancelling solve...'
    Visible = False
  end
  object lbEstimatedLowSolveTime: TLabel
    Left = 46
    Top = 240
    Width = 122
    Height = 13
    Caption = 'lbEstimatedLowSolveTime'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lbSolveExtraInfo: TLabel
    Left = 46
    Top = 170
    Width = 80
    Height = 13
    Caption = 'lbSolveExtraInfo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lbEstimatedHighSolveTime: TLabel
    Left = 46
    Top = 271
    Width = 124
    Height = 13
    Caption = 'lbEstimatedHighSolveTime'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lbSliceNumber: TLabel
    Left = 46
    Top = 138
    Width = 46
    Height = 13
    Caption = 'Solving...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lbScenarioSolveInfo: TLabel
    Left = 46
    Top = 47
    Width = 95
    Height = 13
    Caption = 'lbScenarioSolveInfo'
  end
  object lbSolveSourcePosition: TLabel
    Left = 46
    Top = 111
    Width = 46
    Height = 13
    Caption = 'Solving...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object pbSolveProgress: TProgressBar
    Left = 46
    Top = 200
    Width = 207
    Height = 18
    Step = 1
    TabOrder = 0
    Visible = False
  end
  object btCancelSolve: TButton
    Left = 46
    Top = 326
    Width = 113
    Height = 25
    Caption = 'Cancel solve'
    TabOrder = 1
    Visible = False
    OnClick = btCancelSolveClick
  end
  object mmSolverMessages: TMemo
    Left = 46
    Top = 510
    Width = 363
    Height = 276
    Lines.Strings = (
      '')
    ScrollBars = ssVertical
    TabOrder = 2
    Visible = False
  end
  object mmSolverErrors: TMemo
    Left = 46
    Top = 376
    Width = 363
    Height = 117
    Lines.Strings = (
      '')
    ScrollBars = ssVertical
    TabOrder = 3
    Visible = False
  end
end
