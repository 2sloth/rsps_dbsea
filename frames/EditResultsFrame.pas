{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit EditResultsFrame;

interface

uses
    syncommons,
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, ExtCtrls, baseTypes, TeEngine, TeeProcs, Chart, StdCtrls,
    ComCtrls, Clipbrd, Grids, StrUtils,
    generics.Defaults, generics.Collections, Series, math,
    ResultsColourSet, LevelLimitsDlg, SQLiteTable3, pngimage, dBSeaColours,
    helperFunctions, levelConverter, types, mycolor, dbseaconstants, eventLogging,
    globalsUnit, GLWindow, problemContainer, scenarioDiff, thresholdLevels,
    MyFrameInterface, SmartPointer, otlTask, OtlTaskControl, OtlParallel;

type
    TEditResultsFrame = class(TFrame, IMyFrame)
        imResultsColorBar: TImage;
        btColourSet: TButton;
        btSetLims: TButton;
        imPageTitle: TImage;
        lblPageTitle: TLabel;
        lbExclusionZone: TLabel;
        cbLockLimits: TCheckBox;
        mmSolverErrors: TMemo;
        lbDiffInfo: TLabel;

        constructor Create(AOwner: TComponent); override;
        destructor Destroy; override;

        procedure frameshow(Sender: TObject);
        procedure framehide(Sender: TObject);
        procedure EditBoxesInit;

        procedure updateResultsColorBar;
        procedure updateAllGraphics;

        procedure setLabelsVisibility(const val : boolean);
        procedure btColourSetClick(Sender: TObject);

        function  getResultsScaleBMP(const w,h,y0: integer; const aCol: TColor): TBitmap;
        procedure btSetLimsClick(Sender: TObject);
        procedure cbLockLimitsClick(Sender: TObject);
    private
        imResultsColorBarInitialHeight: integer;
        updateFromCode          : Boolean;

        procedure didChangeScenario;
        procedure didChangeUnits;
        function  getAllLevelsArrayForDialog(const pb: TProgressBar): TSingleDynArray;
    public
        levLabels               : TLabelList;
    end;

var
    framResults             : TEditResultsFrame;

implementation

{$R *.dfm}

uses Main, dBSeaOptions;

constructor TEditResultsFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        {create list to hold the level labels we want to create later}
        LevLabels := TLabelList.Create(true);

        updateFromCode := false;
        imResultsColorBarInitialHeight := imResultsColorBar.Height;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

destructor TEditResultsFrame.Destroy;
begin
    LevLabels.Clear;
    LevLabels.Free;

    inherited;
end;


procedure TEditResultsFrame.didChangeScenario;
begin
    //
end;

procedure TEditResultsFrame.didChangeUnits;
begin
    EditBoxesInit;
end;

procedure TEditResultsFrame.frameshow(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'frameshow');
    try
        EditBoxesInit;
        updateAllGraphics;
        updateFromCode := false;
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditResultsFrame.framehide(Sender: TObject);
begin
    //nothing
end;

procedure TEditResultsFrame.EditBoxesInit;
begin
    updateFromCode := true;
    cbLockLimits.Checked := UWAOpts.levelLimits.lockLimits;

    self.lbDiffInfo.Visible := container.levelServer.isDiff;
    if container.levelServer.isDiff and container.levelServer.diff.isValid then
        self.lbDiffInfo.caption := 'Showing scenario diff: ' + container.levelServer.diff.s1.name + ' - ' + container.levelServer.diff.s2.name
    else
        self.lbDiffInfo.Caption := '';

    updateFromCode := false;
end;

procedure TEditResultsFrame.updateResultsColorBar;
    procedure updateColorBarLabels();
    const
        iMaxLevs: integer = 14;
    var
        lbl: TLabel;
        i, lblCount, iLevelsDecimate: integer;
    begin
        LevLabels.Clear;
        if (container.scenario.resultsExist and not UWAOpts.levelLimits.maxOrMinNaN) then
        begin
            {create labels to show levels. create 1 more than there are resultscolors}
            if (UWAOpts.getResultsColorsSteps + 1) > iMaxLevs then
                iLevelsDecimate := (UWAOpts.getResultsColorsSteps + 1) div iMaxLevs
            else
                iLevelsDecimate := 1;

            if iLevelsDecimate <= 0 then
                iLevelsDecimate := 1;

            lblCount := (UWAOpts.getResultsColorsSteps + 1) div iLevelsDecimate;
            while LevLabels.Count < lblCount do
                LevLabels.Add(TLabel.Create(self));

            i := 0;
            for lbl in LevLabels do
            begin
                lbl.Parent := self;
                lbl.Left := imResultsColorBar.Left + imResultsColorBar.Width + 5;
                lbl.Caption := '--  ' + container.scenario.formatLevel(UWAOpts.getLevelContour(i * iLevelsDecimate)); {formatlevel includes conversion to user Leveltype}

                {move to correct pos and set tag}
                lbl.Top := safetrunc(imResultsColorBar.Top +
                    ((lblCount - 1 - i)/(lblCount-1))*imResultsColorBar.Height + lbl.Font.Height/2);
                lbl.Tag := i;

                inc(i);
            end;
        end;
    end;
    procedure drawSmoothColormapToCanvas();
    var
        resultscolormap : TMatrix;
        lrcm, w, h, i, ind : integer;
    begin
        {draw one line at a time, in the desired colour drawn from the colormap}
        resultscolormap := UWAOpts.resultscolormap;
        lrcm := high(resultscolormap);
        w := imResultscolorBar.Width;
        h := imResultscolorBar.Height;
        with imResultsColorBar.Canvas do
        begin
            Pen.Width := 1;
            for i := 0 to h - 1 do
            begin
                ind := safetrunc(lrcm * i /(h - 1));
                Pen.Color := RGB
                  (byte(safetrunc(resultscolormap[ind, 0] * 255)),
                   byte(safetrunc(resultscolormap[ind, 1] * 255)),
                   byte(safetrunc(resultscolormap[ind, 2] * 255)));
                MoveTo(0, i);
                LineTo(w, i);
            end;
        end;
    end;
    procedure drawNonSmoothColormapToCanvas();
    var
        listlen, w, h, i: integer;
        MyCol: TMyColorRec;
    begin
        w := imResultscolorBar.Width;
        listlen := UWAOpts.getResultsColorsSteps;

        {rescale the colorbar so that it divides evenly by the number of colors}
        imResultscolorBar.Height := listlen*(imResultsColorBarInitialHeight div listlen);
        h := imResultscolorBar.Height;
        with imResultsColorBar.Canvas do
        begin
            for i := 0 to listlen - 1 do
            begin
                myCol := UWAOpts.ResultsColorsRec(i);
                Pen.Color := MyCol.Color;
                Pen.Width := 2;
                pen.Style := psSolid;
                Brush.Color := MyCol.Color;
                {if the color has its visibility turned off, then just show it hashed}
                if MyCol.Visible then
                    Brush.Style := bsSolid
                else
                    Brush.Style := bsFDiagonal;
                Rectangle(0,i*(h div listlen),w - 1,(i+1)*(h div listlen) - 1);
            end;
        end;
    end;
    procedure drawBlankBoxInCanvas();
    begin
        imResultsColorBar.Canvas.Pen.Color   := clGray;
        imResultsColorBar.Canvas.Pen.Width   := 2;
        imResultsColorBar.Canvas.pen.Style   := psSolid;
        imResultsColorBar.Canvas.Brush.Color := clGray;
        imResultsColorBar.Canvas.Brush.Style := bsSolid;
        imResultsColorBar.Canvas.FillRect(GetClientRect);
    end;
begin
    if (not container.scenario.resultsExist) or UWAOpts.levelLimits.maxOrMinNaN then
        drawBlankBoxInCanvas()
    else
    begin
        if UWAOpts.ResultsColormapSmooth then
            drawSmoothColormapToCanvas
        else
            drawNonSmoothColormapToCanvas;
    end;

    imResultsColorBar.Enabled := container.scenario.resultsExist;
    btSetLims.Enabled         := container.scenario.resultsExist;
    btColourSet.Enabled       := container.scenario.resultsExist;

    updateColorBarLabels();

    imResultsColorBar.Invalidate;
end;

procedure TEditResultsFrame.updateAllGraphics;
begin
    if not assigned(UWAOpts) then exit;

    if UWAOpts.levelLimits.showExclusionZone then
    begin
        imResultsColorBar.Visible := false;
        lbExclusionZone.Visible := true;
        if isnan(UWAOpts.levelLimits.exclusionZoneLevel) then
            lbExclusionZone.Caption := 'Cannot show exclusion zone, no exclusion threshold set'
        else
            lbExclusionZone.Caption := 'Showing exclusion zone at ' + container.scenario.formatLevel(UWAOpts.levelLimits.exclusionZoneLevel);
        levLabels.Clear;
    end
    else
    begin
        imResultsColorBar.Visible := true;
        lbExclusionZone.Visible := false;
        updateResultsColorBar;
    end;
end;

procedure TEditResultsFrame.btColourSetClick(Sender: TObject);
var
    myColRec                : TMyColorRec;
    myCol                   : TMyColor;
    i                       : integer;
    listsAreEqual           : boolean;
begin
    {copy the colorlist in UWAOpts to a local temp copy in the form}
    ResultsColoursFrm.tmpColorList.clear;
    for i := 0 to UWAOpts.ResultsColorsCount - 1 do
    begin
        myColRec := UWAOpts.ResultsColorsRec(i);
        ResultsColoursFrm.tmpColorList.Add(TMyColor.CreateWithColor(myColRec.color, myColRec.visible));
    end;

    if ResultsColoursFrm.showmodal = mrOK then
    begin
        {if modal result is OK, check to see if the tmp and UWAOpts lists are equal.
        if not then we copy back the temp results colorlist from
        the TResultsColours form to the UWAOpts color list. otherwise just discard it.}
        listsAreEqual := true;
        if UWAOpts.ResultsColorsCount <> ResultsColoursFrm.tmpColorList.Count then
            listsAreEqual := false
        else
            for i := 0 to UWAOpts.ResultsColorsCount - 1 do
            begin
                myColRec := UWAOpts.ResultsColorsRec(i);
                if (myColRec.color <> ResultsColoursFrm.tmpColorList[i].color) or (myColRec.visible <> ResultsColoursFrm.tmpColorList[i].visible) then
                    listsAreEqual := false;
            end;

        //if not ColorListsAreEqual(UWAOpts.ResultsColors,ResultsColoursFrm.tmpColorList) then
        if not listsAreEqual then
        begin
            if ResultsColoursFrm.bResetColorList then
                UWAOpts.bUserSetColors := false
            else
                UWAOpts.bUserSetColors := true;

            UWAOpts.ResultsColorsClear;
            for myCol in ResultsColoursFrm.tmpColorList do
                UWAOpts.ResultsColorsAdd(myCol.color,myCol.visible);

            EditBoxesInit;
            updateAllGraphics;
            if assigned(GLForm) then
            begin
                GLForm.setRedrawResults;
                GLForm.force2DrawNow;
            end;
        end;
    end;
end;

function TEditResultsFrame.getAllLevelsArrayForDialog(const pb: TProgressBar): TSingleDynArray;
var
    i, j, d : integer;
    bNaNSeafloorLevels : boolean;
begin
    setlength(result,container.scenario.theRA.imax * container.scenario.theRA.jmax);
    d := UWAOpts.DisplayZInd;
    bNaNSeafloorLevels := container.scenario.NaNSeafloorLevels;
    for i := 0 to container.scenario.theRA.imax - 1 do
    begin
        for j := 0 to container.scenario.theRA.jmax - 1 do
            result[i + j * container.scenario.theRA.imax] := container.scenario.theRA.getLevAtInds(d, i, j, -1, {this array is converted to user leveltypes in TLevelLimitsDialog}
                                                                                                   UWAOpts.LevelsDisplay,                                                                                                   bNaNSeafloorLevels);
        if i mod 20 = 0 then
        begin
            TThread.Queue(nil, procedure begin
                pb.Position := i;
                Application.ProcessMessages;
            end);
        end;
    end;

    for i := 0 to length(result) - 1 do
        if result[i] < lowLev then
            result[i] := nan;
end;

procedure TEditResultsFrame.btSetLimsClick(Sender: TObject);
var
    bSomethingChanged : boolean;
    task: IOmnitaskcontrol;
begin
    cursor := crHourGlass;
    btSetLims.Enabled := false;
    Application.ProcessMessages();
    try
        bSomethingChanged := false;
        LevelLimitsDialog.aLevelLimits.cloneFrom(UWAOpts.levelLimits);
        levelLimitsDialog.allActiveLevels.null;
        LevelLimitsDialog.allActiveLevelsAsUserLevelType.null;

        levelLimitsDialog.lbGatheringLevels.Visible := true;
        levelLimitsDialog.pbGatheringLevels.Visible := true;
        levelLimitsDialog.pbGatheringLevels.max := container.scenario.theRA.iMax;
        levelLimitsDialog.pbGatheringLevels.Position := 0;

        task := CreateTask(procedure(const task: IOmniTask) begin
        //Parallel.Async(procedure(const task: IOmnitask) begin
            LevelLimitsDialog.allActiveLevels := self.getAllLevelsArrayForDialog(LevelLimitsDialog.pbGatheringLevels);
            TThread.Queue(nil, procedure begin
                LevelLimitsDialog.updateHistogram();
            end);
        end).run;
        LevelLimitsDialog.levelType := container.scenario.levelType;
        LevelLimitsDialog.customColors := UWAOpts.customColors;
        LevelLimitsDialog.exclusionZonecolor := UWAOpts.exclusionZoneColor;
        LevelLimitsDialog.levelConversion := container.scenario.convertLevel;
        LevelLimitsDialog.levelUnConversion := container.scenario.unconvertLevel;

        if LevelLimitsDialog.showModal() = mrOK then
        begin
            UWAOpts.customColors := LevelLimitsDialog.customColors;
            if not isnan(LevelLimitsDialog.dExclusionZoneLevel) then
            begin
                if UWAOpts.levelLimits.exclusionZoneLevel <> LevelLimitsDialog.dExclusionZoneLevel then
                    bSomethingChanged := true;
                UWAOpts.levelLimits.exclusionZoneLevel := LevelLimitsDialog.dExclusionZoneLevel;
            end;

            if UWAOpts.levelLimits.showExclusionZone <> LevelLimitsDialog.cbExclusionZone.Checked then
            begin
                if container.scenario.sourcesNSlices < 4 then
                    showmessage('Too few slices to show exclusion zone')
                else
                begin
                    bSomethingChanged := true;
                    UWAOpts.levelLimits.showExclusionZone := LevelLimitsDialog.cbExclusionZone.Checked;
                end;
            end;

            if UWAOpts.levelLimits.showExclusionZone and (LevelLimitsDialog.exclusionZonecolor <> UWAOpts.exclusionZoneColor) then
                bSomethingChanged := true;
            UWAOpts.exclusionZoneColor := LevelLimitsDialog.exclusionZonecolor;

            if (not LevelLimitsDialog.bUserSet) then
            begin
                bSomethingChanged := true;
                UWAOpts.levelLimits.deleteUserSetLimits;
            end;

            if (not (isNaN(LevelLimitsDialog.dLevMax) or isNaN(LevelLimitsDialog.dLevMin))) and
               ((LevelLimitsDialog.dLevMin <> UWAOpts.levelLimits.Min) or
                (LevelLimitsDialog.dLevMax <> UWAOpts.levelLimits.Max) or
                (LevelLimitsDialog.dLevelSpacing <> UWAOpts.levelLimits.LevelSpacing)) then
            begin
                bSomethingChanged := true;

                UWAOpts.levelLimits.levelSpacing := LevelLimitsDialog.dLevelSpacing;
                UWAOpts.levelLimits.setLimsWithSpacing(LevelLimitsDialog.dLevMin,LevelLimitsDialog.dLevMax,LevelLimitsDialog.dLevelSpacing,LevelLimitsDialog.bUserSet);
            end;

            if bSomethingChanged then
            begin
                eventLog.log('Update output levels colours');
                UWAOpts.updateResultsLevelColors;
                updateAllGraphics;

                if assigned(GLForm) then
                begin
                    GLForm.setRedrawResults;
                    GLForm.force2Drawnow;
                end;
            end;
        end;

        if not task.WaitFor(0) then
            task.Terminate(0);
        task := nil;

    finally
        cursor := crDefault;
        btSetLims.Enabled := true;
    end;
end;

procedure TEditResultsFrame.cbLockLimitsClick(Sender: TObject);
begin
    UWAOpts.levelLimits.lockLimits := cbLockLimits.Checked;
end;

function  TEditResultsFrame.getResultsScaleBMP(const w,h,y0 : integer; const aCol : TColor): TBitmap;
const
    {little bit of spacing at the left side, so that the colorbar doesn't dirctly touch the
    main GL image}
    iLeftSpacing = 50;
var
    tmpForm: ISmart<TForm>;
    aLabel: TLabel;
    i, colorbarTop, colorbarLeft: integer;
    Tops, Lefts: TIntegerDynArray;
    firstTop: integer;
    txtheight: integer;
    iLabelX: integer;
begin
    {create a fake form with the correct colour background, send all the components
    we want to over there, take a bmp snapshot, then put the components back on this form.
    w and h are width and height, y0 is how far down (j) we want to start drawing the components}
    tmpForm := TSmart<TForm>.create(tform.Create(self));
    tmpForm.ClientWidth := w;
    tmpForm.ClientHeight := h;
    tmpForm.Color := aCol;

    colorbarTop := imResultsColorBar.Top;
    colorbarLeft := imResultsColorBar.Left;

    if LevLabels.Count > 0 then
    begin
        aLabel := LevLabels.first;
        firstTop := aLabel.Top;
        iLabelX := aLabel.Left - imResultsColorBar.Left;
        txtheight := aLabel.Canvas.TextHeight('A');

        {store locations of all components, and copy them to the tmpform at
        new locations}
        imResultsColorBar.Left := iLeftSpacing;
        imResultsColorBar.Top := y0 + txtheight;
        imResultsColorBar.Parent := tmpForm;

        setlength(tops,LevLabels.Count);
        setlength(lefts,LevLabels.Count);
        i := 0;
        for aLabel in LevLabels do
        begin
            aLabel.Parent := tmpForm;
            tops[i] := aLabel.top;
            lefts[i] := aLabel.left;
            aLabel.Left := iLabelX + iLeftSpacing;
            aLabel.Top := y0 + imResultsColorBar.Height + tops[i] - firstTop + txtheight div 2;
            i := i + 1;
        end;
    end;
    tmpForm.Invalidate;  //force form to redraw

    result := tmpForm.GetFormImage();

    {replace components on this form, return them to the correct positions}
    imResultsColorBar.Parent := self;
    imResultsColorBar.Top := colorbarTop;
    imResultsColorBar.Left := colorbarLeft;

    i := 0;
    for aLabel in self.LevLabels do
    begin
        aLabel.Parent := self;
        aLabel.left := lefts[i];
        aLabel.Top := tops[i];
        i := i + 1;
    end;
end;

procedure TEditResultsFrame.setLabelsVisibility(const val : boolean);
var
    lbl : TLabel;
begin
    for lbl in self.LevLabels do
        lbl.Visible := val;
end;


end.
