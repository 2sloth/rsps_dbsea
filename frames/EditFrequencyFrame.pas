unit EditFrequencyFrame;

interface

uses
    syncommons,
    system.SysUtils, system.Variants, system.Classes, system.math,
  winapi.Windows, winapi.Messages,
  vcl.Graphics, vcl.Controls, vcl.Forms,
  vcl.Dialogs, vcl.StdCtrls, vcl.imaging.pngimage, vcl.ExtCtrls, vcl.ComCtrls,
  basetypes, spectrum,
  levelConverter, helperfunctions, eventLogging,
  globalsUnit, options, problemContainer, MyFrameInterface,
  SmartPointer;

type
  TFrequencyFrame = class(TFrame, IMyFrame)
    imPageTitle: TImage;
    lblPageTitle: TLabel;
    rgBandwidth: TRadioGroup;
    lblMasterFreqs: TLabel;
    lblMasterSpectrumFreqs: TLabel;
    udLowFreq: TUpDown;
    udHighFreq: TUpDown;
    cbSplitSolver_split: TCheckBox;
    udSplitSolverFreq: TUpDown;
    rgSolver: TRadioGroup;
    btSetCurrentSolversToDefault: TButton;
    lbCrossoverFreqLo: TLabel;
    lbCrossoverFreqHi: TLabel;
    rgHighSolver: TRadioGroup;
    rgLowSolver: TRadioGroup;
    cbCoherent: TCheckBox;
    btAdvancedSolverOptions: TButton;
    lbUserDefined1: TLabel;
    edUserDefinedLogPart: TEdit;
    lbUserDefined2: TLabel;
    edUserDefinedLinPart: TEdit;
    lbUserDefined3: TLabel;
    btUserDefined: TButton;
    mmLiteVersion: TMemo;
    pcSolver: TPageControl;
    tsSingleSolver: TTabSheet;
    tsSplitSolver: TTabSheet;
    cbSplitSolver_single: TCheckBox;
    lbCrossoverFreq: TLabel;

    constructor Create(AOwner: TComponent); override;
    procedure frameshow(Sender: TObject);
    procedure framehide(Sender: TObject);
    procedure EditBoxesInit;

    procedure removeSolverEntries;

    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure rgBandwidthClick(Sender: TObject);
    procedure cbSplitSolver_splitClick(Sender: TObject);
    procedure udLowFreqClick(Sender: TObject; Button: TUDBtnType);
    procedure udHighFreqClick(Sender: TObject; Button: TUDBtnType);
    procedure udSplitSolverFreqClick(Sender: TObject; Button: TUDBtnType);
    procedure rgSolverClick(Sender: TObject);
    procedure rgLowSolverClick(Sender: TObject);
    procedure rgHighSolverClick(Sender: TObject);
    procedure cbCoherentClick(Sender: TObject);
    procedure btAdvancedSolverOptionsClick(Sender: TObject);
    procedure btUserDefinedClick(Sender: TObject);
  private
    updateFromCode: boolean;
    modified: boolean;

    procedure didChangeScenario;
    procedure didChangeUnits;
    procedure setModified(const val: boolean);
    procedure setButtonsEnabled(const val: boolean);

    function GetSolverFromIndex(Ind: integer): TCalculationMethod;
    function GetIndexFromSolver(cm: TCalculationMethod): integer;
    procedure removeEntriesFromSolverList(const rg: TRadioGroup; const removeDBSeaSolvers: boolean);
  public
  end;

var
    framFrequency           : TFrequencyFrame;

implementation

{$R *.dfm}

uses main, GLWindow, EditFishFrame;

constructor TFrequencyFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'Create');
    try
        inherited Create(AOwner);

        updateFromCode        := true;
        modified := false;
        updateFromCode        := false;

        {$IF Defined(LITE)}
        cbSplitSolver_single.Visible := false;
        cbSplitSolver_split.Visible := false;
        udSplitSolverFreq.Visible := false;
        lbCrossoverFreqLo.Visible := false;
        lbCrossoverFreqHi.Visible := false;
        rgLowSolver.Visible := false;
        rgHighSolver.Visible := false;
        rgSolver.enabled := true;
        {$ELSE}
        mmLiteVersion.Visible := false;
        {$ENDIF}

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TFrequencyFrame.didChangeScenario;
begin
    frameshow(nil);
end;

procedure TFrequencyFrame.didChangeUnits;
begin
    EditBoxesInit;
end;

procedure TFrequencyFrame.framehide(Sender: TObject);
begin
    //
end;

procedure TFrequencyFrame.frameshow(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'frameshow');
    try
        {$IFNDEF DEBUG}
            cbCoherent.Visible := false;
        {$ENDIF}

        updateFromCode := false;

        EditBoxesInit;
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TFrequencyFrame.EditBoxesInit;
var
    fi: integer;
    s: String;
    bw: TBandwidth;
begin
    updateFromCode := true;

    bw := container.scenario.bandwidth;

    {create string showing assessment freqs}
    s := ''; // 'Assessment frequencies :';
    fi := container.scenario.startfi;
    s := s + ' ' + TSpectrum.getFStringbyInd(fi, bw);
    fi := container.scenario.endfi;
    s := s + 'Hz to ' + TSpectrum.getFStringbyInd(fi, bw);

    {and the crossover frequency caption. the }
    lbCrossoverFreqLo.Caption := 'Low frequency solver, ' + TSpectrum.getFStringbyInd(container.scenario.startfi,bw) + ' Hz to ' +
        TSpectrum.getFStringbyInd(container.scenario.SplitSolverfi,bw) + ' Hz';
    if container.scenario.startfi = container.scenario.endfi then
        lbCrossoverFreqHi.Caption := 'High frequency solver not used'
    else
        lbCrossoverFreqHi.Caption := 'High frequency solver, ' + TSpectrum.getFStringbyInd(container.scenario.SplitSolverfi + 1,bw) + ' Hz to ' +
            TSpectrum.getFStringbyInd(container.scenario.endfi,bw) + ' Hz';
    case bw of
        bwOctave : begin
            s := s + 'Hz at octaves';
            rgBandwidth.ItemIndex := 0;
        end;
        bwThirdOctave : begin
            s := s + 'Hz at 1/3 octaves';
            rgBandwidth.ItemIndex := 1;
        end;
    end;
    lblMasterSpectrumFreqs.Caption := s;

    {solver choice}
    cbSplitSolver_single.Checked    := container.scenario.SplitSolver;
    cbSplitSolver_split.Checked     := container.scenario.SplitSolver;
    lbCrossoverFreqLo.Enabled       := container.scenario.SplitSolver;
    lbCrossoverFreqHi.Enabled       := container.scenario.SplitSolver;
    udSplitSolverFreq.Enabled       := container.scenario.SplitSolver;
    rgLowSolver.Enabled             := container.scenario.SplitSolver;
    rgHighSolver.Enabled            := container.scenario.SplitSolver;
    pcSolver.ActivePageIndex        := ifthen(container.scenario.splitSolver, 1, 0);
    {$IF not Defined(LITE)}
    rgSolver.Enabled            := not container.scenario.SplitSolver;
    {$ENDIF}
    cbCoherent.Checked          := container.scenario.coherent;

    if isnan(container.scenario.userSolverLogPart) then
        edUserDefinedLogPart.Text := ''
    else
        edUserDefinedLogPart.Text := tostr(container.scenario.userSolverLogPart);
    if isnan(container.scenario.userSolverLinPart) then
        edUserDefinedLinPart.Text := ''
    else
        edUserDefinedLinPart.Text := tostr(container.scenario.userSolverLinPart);

    //solvers radio buttons
    if GetIndexFromSolver(container.scenario.Solver) >= self.rgSolver.Items.Count then
        self.rgSolver.ItemIndex     := 1
    else
        self.rgSolver.ItemIndex     := GetIndexFromSolver(container.scenario.Solver);

    if GetIndexFromSolver(container.scenario.LowSolver) >= self.rgLowSolver.Items.Count then
        self.rgLowSolver.ItemIndex     := 1
    else
        self.rgLowSolver.ItemIndex     := GetIndexFromSolver(container.scenario.LowSolver);

    if GetIndexFromSolver(container.scenario.HighSolver) >= self.rgHighSolver.Items.Count then
        self.rgHighSolver.ItemIndex     := 1
    else
        self.rgHighSolver.ItemIndex     := GetIndexFromSolver(container.scenario.HighSolver);

    updateFromCode := false;
    setModified(false);
end;

procedure TFrequencyFrame.removeSolverEntries;
begin
    removeEntriesFromSolverList(rgSolver, runningMode = rmUnlicensed);
    removeEntriesFromSolverList(rgLowSolver, runningMode = rmUnlicensed);
    removeEntriesFromSolverList(rgHighSolver, runningMode = rmUnlicensed);
end;

procedure TFrequencyFrame.removeEntriesFromSolverList(const rg: TRadioGroup; const removeDBSeaSolvers: boolean);
var
    solverList: ISmart<TStringList>;
    i: integer;
begin
    {Reducing the list of solvers available to non-MDA users.
    there are some strange errors with TStrings. calling delete causes problems.
    So, we store the strings in tstringlist, and put back the ones we need}
    solverList := TSmart<TStringList>.create(TStringlist.Create);
    for i := 0 to rg.Items.Count - 1 do
        solverList.Add(rg.Items[i]);

    rg.Items.Clear;
    if removeDBSeaSolvers then
    begin
        for i := 0 to 2 do
            rg.Items.Add(solverList[i])
    end
    else
        //{$DEFINE SHOW3DSOLVERS}
        for i := 0 to solverList.Count - {$IFDEF SHOW3DSOLVERS}4{$ELSE}6{$ENDIF} do
            rg.Items.Add(solverList[i]);

    {$IFDEF DEBUG}
        rg.Items.add('RAM cloud');
        rg.Items.add('RAM binary');
        rg.Items.add('Bellhop binary');
        rg.Items.add('Kraken binary');
    {$ENDIF}
end;

procedure TFrequencyFrame.setModified(const val : boolean);
begin
    if modified <> val then
    begin
        modified := val;
        setButtonsEnabled(modified);
    end;
end;

procedure TFrequencyFrame.udHighFreqClick(Sender: TObject; Button: TUDBtnType);
begin
    if Button = btNext then
        container.scenario.setendfi(container.scenario.endfi + 1)
    else
        container.scenario.setendfi(container.scenario.endfi - 1);

    eventLog.log('Set highest frequency ' + TSpectrum.getFStringbyInd(container.scenario.endfi,container.scenario.bandwidth) + ' in scenario ' + container.scenario.name);
    EditBoxesInit;

    {check if we need to update the fish weighting graph}
    if mainform.mainPageControl.ActivePageIndex = ord(TMyPages.Fish) then
        framFish.chartUpdate;

    GLForm.setredraw;
end;

procedure TFrequencyFrame.udLowFreqClick(Sender: TObject; Button: TUDBtnType);
begin
    if Button = btNext then
        container.scenario.setstartfi(container.scenario.startfi + 1)
    else
        container.scenario.setstartfi(container.scenario.startfi - 1);

    eventLog.log('Set lowest frequency ' + TSpectrum.getFStringbyInd(container.scenario.startfi,container.scenario.bandwidth) + ' in scenario ' + container.scenario.name);
    EditBoxesInit;

    {check if we need to update the fish weighting graph}
    if mainform.mainPageControl.ActivePageIndex = ord(TMyPages.Fish) then
        framFish.chartUpdate;

    GLForm.setRedraw;
end;

procedure TFrequencyFrame.udSplitSolverFreqClick(Sender: TObject;
  Button: TUDBtnType);
begin
    if Button = btNext then
        container.scenario.setSplitSolverfi(container.scenario.SplitSolverfi + 1)
    else
        container.scenario.setSplitSolverfi(container.scenario.SplitSolverfi - 1);

    eventLog.log('Set split frequency ' + TSpectrum.getFStringbyInd(container.scenario.SplitSolverfi,container.scenario.bandwidth) + ' in scenario ' + container.scenario.name);
    EditBoxesInit;
    GLForm.setredraw;
end;

procedure TFrequencyFrame.setButtonsEnabled(const val : boolean);
begin
    //self.btOK.Enabled := val;
    //self.btCancel.Enabled := val;
end;


procedure TFrequencyFrame.btAdvancedSolverOptionsClick(Sender: TObject);
begin
    mainform.ShowOptionsForm(optionsForm.iAdvancedSolverTab);
end;

procedure TFrequencyFrame.btCancelClick(Sender: TObject);
begin
    editBoxesInit;
end;

procedure TFrequencyFrame.btOkClick(Sender: TObject);
begin

    setModified(false);
    editBoxesInit;
end;

procedure TFrequencyFrame.btUserDefinedClick(Sender: TObject);
begin
    if not(isfloat(self.edUserDefinedLogPart.Text) and isfloat(self.edUserDefinedLinPart.Text)) then
    begin
        showmessage('Unable to parse the text in both boxes, please check your entries');
        exit;
    end;

    container.scenario.userSolverLogPart := tofl(self.edUserDefinedLogPart.Text);
    container.scenario.userSolverLinPart := tofl(self.edUserDefinedLinPart.Text);
end;

procedure TFrequencyFrame.cbCoherentClick(Sender: TObject);
begin
    if TLevelConverter.isTimeSeries(container.scenario.levelType) then
    begin
        showmessage('Time series calculations do not support multiple sources');
    end
    else
    begin
        container.scenario.coherent := cbCoherent.Checked;
        eventLog.log('Change to coherent calculation in scenario ' + container.scenario.name);
    end;
end;

procedure TFrequencyFrame.cbSplitSolver_splitClick(Sender: TObject);
begin
    if TLevelConverter.isTimeSeries(container.scenario.levelType) then
    begin
        showmessage('dBSeaRay must be used for time series calculations');
    end
    else
    begin
        container.scenario.SplitSolver := TCheckBox(Sender).Checked;
        if container.scenario.SplitSolver then
            eventLog.log('Use split solvers in scenario ' + container.scenario.name)
        else
            eventLog.log('Do not use split solvers in scenario ' + container.scenario.name);
        EditBoxesInit;
    end;
end;

procedure TFrequencyFrame.rgBandwidthClick(Sender: TObject);
begin
    case self.rgBandwidth.ItemIndex of
        0 : container.scenario.setbandwidth(bwOctave);
        1 : container.scenario.setbandwidth(bwThirdOctave);
    end;

    if container.scenario.bandwidth = bwOctave then
        eventLog.log('Set scenario ' + container.scenario.name + ' to octave bands')
    else
        eventLog.log('Set scenario ' + container.scenario.name + ' to third octave bands');

    EditBoxesInit;

    {check if we need to update the fish weighting graph}
    if mainform.mainPageControl.ActivePageIndex = ord(TMyPages.Fish) then
        framFish.chartUpdate;

    GLForm.setRedraw;
end;

procedure TFrequencyFrame.rgHighSolverClick(Sender: TObject);
begin
    if TLevelConverter.isTimeSeries(container.scenario.levelType) and not (getSolverFromIndex(rgHighSolver.ItemIndex) = cmdBSeaRay) then
    begin
        showmessage('dBSeaRay must be used for time series calculations');
        container.scenario.highSolver := cmdBSeaRay;
        self.rgHighSolver.ItemIndex     := GetIndexFromSolver(container.scenario.highSolver);
    end
    else
    begin
        container.scenario.highSolver := GetSolverFromIndex(rgHighSolver.ItemIndex);
        eventLog.log('Set scenario ' + container.scenario.name + ' high freq solver to ' + container.scenario.highSolver.toStr);
    end;
end;

procedure TFrequencyFrame.rgLowSolverClick(Sender: TObject);
begin
    if TLevelConverter.isTimeSeries(container.scenario.levelType) and not (getSolverFromIndex(rgLowSolver.ItemIndex) = cmdBSeaRay) then
    begin
        showmessage('dBSeaRay must be used for time series calculations');
        container.scenario.lowSolver := cmdBSeaRay;
        self.rgLowSolver.ItemIndex     := GetIndexFromSolver(container.scenario.lowSolver);
    end
    else
    begin
        container.scenario.LowSolver := GetSolverFromIndex(rgLowSolver.ItemIndex);
        eventLog.log('Set scenario ' + container.scenario.name + ' low freq solver to ' + container.scenario.lowSolver.toStr);
    end;
end;

procedure TFrequencyFrame.rgSolverClick(Sender: TObject);
begin
    {$IFDEF LITE}
    if getSolverFromIndex(rgSolver.ItemIndex).availableInLite then
    begin
        container.scenario.Solver := getSolverFromIndex(rgSolver.ItemIndex);
        eventLog.log('Set scenario ' + container.scenario.name + ' solver to ' + container.scenario.solver.toStr);
    end
    else
        showmessage(mmLiteVersion.Text);
    {$ELSE}
    if TLevelConverter.isTimeSeries(container.scenario.levelType) and not (getSolverFromIndex(rgSolver.ItemIndex) = cmdBSeaRay) then
    begin
        showmessage('dBSeaRay must be used for time series calculations');
        container.scenario.solver := cmdBSeaRay;
        self.rgSolver.ItemIndex     := GetIndexFromSolver(container.scenario.Solver);
    end
    else
    begin
        container.scenario.Solver := getSolverFromIndex(rgSolver.ItemIndex);
        eventLog.log('Set scenario ' + container.scenario.name + ' solver to ' + container.scenario.solver.toStr);
    end;
    {$ENDIF}
end;

function TFrequencyFrame.GetSolverFromIndex(Ind: integer): TCalculationMethod;
begin
    case Ind of
        0: result := cm20log;
        1: result := cm10log;
        2: result := cmUserDefined;
        3: result := cmdBSeaPE;
        4: result := cmdBSeaModes;
        5: result := cmdBSeaRay;
        6: result := cmdBSeaPE3d;
        7: result := cmdBSeaRay3d;
        8: result := cmRAMCloud;
        9: result := cmRamGEO;
        10: result := cmBellhop;
        11: result := cmKraken;
    else
        result := cm10log;
    end;
end;

function TFrequencyFrame.GetIndexFromSolver(cm: TCalculationMethod): integer;
begin
    case cm of
        cm20log: result := 0;
        cm10log: result := 1;
        cmUserDefined: result := 2;
        cmdBSeaPE: result := 3;
        cmdBSeaModes: result := 4;
        cmdBSeaRay: result := 5;
        cmdBSeaPE3D: result := 6;
        cmdBSeaRay3d: result := 7;
        cmRAMCloud: result := 8;
        cmRamGEO: result := 9;
        cmBellhop: result := 10;
        cmKraken: result := 11;
    else
        result := 1;
    end;
end;


end.
