{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit EditFishFrame;

interface

uses
    syncommons,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, baseTypes, Math,
  ExtCtrls, Chart, Series,
  VCLTee.TeEngine, VclTee.TeeGDIPlus, VCLTee.TeeProcs,
  StdCtrls, Generics.Collections, SQLiteTable3, pngimage, FishAddDBFrm,
  StrUtils, spectrum, fishCurve, helperFunctions, dBSeaColours,
  dBSeaconstants, AppEvnts, levelConverter, system.UITypes,
  eventLogging, globalsUnit, problemContainer, MyFrameInterface,
  SmartPointer;

type
  TEditFishFrame = class(TFrame, IMyFrame)
    chFishCurve             : TChart;
    cbWeightingSelect       : TComboBox;
    mmComments: TMemo;
    lblComments             : TLabel;
    lblNoThreshold          : TLabel;
    imPageTitle             : TImage;
    lblPageTitle            : TLabel;
    btInsertIntoDB          : TButton;
    lblDataSource           : TLabel;
    btRemoveFromDB          : TButton;
    btEditDB                : TButton;
    gbFish                  : TGroupBox;
    cbFixScale              : TCheckBox;
    srFishCurve: TBarSeries;

    constructor Create(AOwner: TComponent); override;
    procedure clearFishCurves;

    procedure frameshow(Sender: TObject);
    procedure framehide(Sender: TObject);
    procedure EditBoxesInit;

    procedure chartUpdate;
    procedure weightingSelectBoxInit;
    procedure cbWeightingSelectChange(Sender: TObject);

    procedure btInsertIntoDBClick(Sender: TObject);
    procedure btRemoveFromDBClick(Sender: TObject);
    procedure btEditDBClick(Sender: TObject);
    procedure cbFixScaleClick(Sender: TObject);
    procedure cbWeightingSelectClick(Sender: TObject);

  private
  var
    updateFromCode          : boolean;

    procedure didChangeScenario;
    procedure didChangeUnits;
  public
    procedure didSetLevelType(const levelType: TLevelType);
  end;

var
    framFish                : TEditFishFrame;

implementation

{$R *.dfm}

uses Main, GLWindow;

constructor TEditFishFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        eventlog.log(log, 'Open weighting curve DB');
        if not MDASldb.TableExists(fishTblName) then
            raise Exception.Create(rsErrMDANoTable + fishTblName);
        if not UserSldb.TableExists(fishTblName) then
            raise Exception.Create(rsUserNoTable + fishTblName);

        updateFromCode := true;
        weightingSelectBoxInit;
        cbWeightingSelect.ItemIndex := 0;
        {call the onchange event, so as to get it to read the name and comments and put them on the form}
        cbWeightingSelectChange(nil);

        chartUpdate;

        updateFromCode := false;
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditFishFrame.clearFishCurves;
var
    i : integer;
begin
    for I := 0 to cbWeightingSelect.Items.Count - 1 do
        TFishCurve(cbWeightingSelect.Items.Objects[i]).Free;
end;


procedure TEditFishFrame.frameshow(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'frameshow');

    try
        updateFromCode := true;

        cbWeightingSelect.OnChange := nil;
        if (container.scenario.weightingInd >= 0) and
            (container.scenario.weightingInd < cbWeightingSelect.Items.Count) and
            (container.scenario.weightingInd <> cbWeightingSelect.ItemIndex) then
            cbWeightingSelect.ItemIndex := container.scenario.weightingInd;
        cbWeightingSelect.OnChange := cbWeightingSelectChange;

        EditBoxesInit;
        chartUpdate;
        updateFromCode := false;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditFishFrame.framehide(Sender: TObject);
begin
    //nothing
end;

procedure TEditFishFrame.cbWeightingSelectChange(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'cbWeightingSelectChange');

    if (cbWeightingSelect.ItemIndex <> 0) and TLevelConverter.isUnweighted(container.scenario.levelType) then
    begin
        showmessage(TLevelConverter.levelTypeName(kSPLCrestFactor) + ', ' + TLevelConverter.levelTypeName(kSPLpk) + ' and ' + TLevelConverter.levelTypeName(kSPLpkpk) +
                    ' output levels are unweighted');
        Exit;
    end;

    updateFromCode := true;
    if cbWeightingSelect.Items.Objects[cbWeightingSelect.ItemIndex] is TFishCurve then
    begin
        container.scenario.setWeightingInd(cbWeightingSelect.ItemIndex, TFishCurve(cbWeightingSelect.Items.Objects[cbWeightingSelect.ItemIndex]));
        eventLog.log('Select freq weighting ' + container.scenario.weighting.Name);
        EditBoxesInit;
        chartUpdate;
        if assigned(GLForm) then
        begin
            GLForm.setRedrawResults();
            GLForm.force2DrawNow();
        end;
    end;
    updateFromCode := false;
end;

procedure TEditFishFrame.cbWeightingSelectClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox;
end;

procedure TEditFishFrame.weightingSelectBoxInit;
var
    sltbMDA, sltbUser: ISmart<TSQLiteTable>;
    curves: ISmart<TObjectList<TFishCurve>>;
    curve: TFishCurve;
begin
    //sort the db results by id, as the table entries have some with low ID that we want to come first

    updateFromCode := false;

    clearFishCurves;
    sltbMDA := TSmart<TSQLiteTable>.create(MDASldb.GetTable(ansistring('SELECT * FROM ' + fishTblName + ' ORDER BY ID')));
    sltbUser := TSmart<TSQLiteTable>.create(userSlDb.GetTable(ansistring('SELECT * FROM ' + fishTblName + ' ORDER BY ID')));
    curves := TSmart<TObjectList<TFishCurve>>.create(TObjectList<TFishCurve>.create(false)); //combobox will own curves
    TFishCurve.addEntries(sltbMDA, curves);
    TFishCurve.addEntries(sltbUser, curves);

    cbWeightingSelect.Clear;
    for curve in curves do
        cbWeightingSelect.AddItem(curve.Name, curve);

    btEditDB.Enabled := false;
    btRemoveFromDB.Enabled := false;
end;

procedure TEditFishFrame.EditBoxesInit;
var
    FC: TFishCurve;
begin
    FC := TFishCurve(cbWeightingSelect.Items.Objects[cbWeightingSelect.ItemIndex]);
    //EditName.Text       := FC.Name;
    mmComments.Text := FC.Comments;
    lblDataSource.Caption := ifthen(FC.DataSource = TDataSource.dsUser,rsDSUser,rsDSMDA);

    btEditDB.Enabled := FC.DataSource = TDataSource.dsUser;
    btRemoveFromDB.Enabled := FC.DataSource = TDataSource.dsUser;
end;


procedure TEditFishFrame.btEditDBClick(Sender: TObject);
var
    sWeighting  : string;
    i, ind      : integer;
    sSQL        : String;
    fi          : integer;
    aLev        : double;
    bw          : TBandWidth;
    sl          : ISmart<TStringlist>;
begin
    sl := TSmart<TStringList>.create(TStringList.Create);
    FishAddToDB.setupForFish;

    ind := cbWeightingSelect.ItemIndex;

    FishAddToDB.EditName.Text := TFishCurve(cbWeightingSelect.Items.Objects[ind]).Name; //sltb.FieldAsString(sltb.FieldIndex['Name']);
    FishAddToDB.memoComments.Text := TFishCurve(cbWeightingSelect.Items.Objects[ind]).Comments; //sltb.FieldAsString(sltb.FieldIndex['Comments']);


    bw := TFishCurve(cbWeightingSelect.Items.Objects[ind]).Bandwidth;
    for fi := 0 to TSpectrum.getLength(bw) - 1 do
    begin
        aLev := TFishCurve(cbWeightingSelect.Items.Objects[ind]).Spectrum.getAmpbyInd(fi,bw);
        FishAddToDB.sgWeighting.Cells[fi + 1,1] := ifthen(isNaN(aLev),'',tostr(aLev));
    end;

    if FishAddToDB.ShowModal = mrOK then
    begin
        {get user data back from the form and do an sql update}
        sWeighting := '';
        for I := 0 to TSpectrum.getLength(bw) - 1 do
            sWeighting := sWeighting + ifthen(isFloat(FishAddToDB.sgWeighting.Cells[i+1,1]),
                                              forceDotSeparator( FishAddToDB.sgWeighting.Cells[i+1,1]) + ',','NaN,');
        sWeighting := leftStr(sWeighting,length(sWeighting) - 1); //strip out last comma

        {create the sql string, and execute. we are adding the user data into the user database. note that some fields
        are numeric.}
        eventLog.log('Edit freq weighting in database');
        userSldb.BeginTransaction;
        sSQL := 'UPDATE ' + fishTblName + ' SET Name="' + FishAddToDB.EditName.Text +'",';
        sSQL := sSQL + 'Comments="' + FishAddToDB.memoComments.Text + '",';
        sSQL := sSQL + 'ThirdOctaveLevelsSerial="' + sWeighting + '" ';
        sSQL := sSQL + ' WHERE ID=' + tostr(TFishCurve(cbWeightingSelect.Items.Objects[ind]).ID) + ';';
        userSldb.ExecSQLUnicode(sSQL);
        userSldb.Commit;

        {update the list of weightings}
        weightingSelectBoxInit;
        cbWeightingSelect.ItemIndex := ind;
        EditBoxesInit;
    end;
end;

procedure TEditFishFrame.btInsertIntoDBClick(Sender: TObject);
var
    sWeighting : string;
    i : integer;
    sSQL        : String;
    iNewID : integer;
begin
    {make the form ready for accepting fish weighting input}
    FishAddToDB.setupForFish;

    {show form modal}
    if FishAddToDB.ShowModal = mrOK then
    begin
        //get user data back from the form and do an sql insert - nb we always add as third octave data

        sWeighting := '';
        for I := 0 to TSpectrum.getLength(bwThirdOctave) - 1 do
            sWeighting := sWeighting + ifthen(isFloat(FishAddToDB.sgWeighting.Cells[i+1,1]),
                                 forceDotSeparator(FishAddToDB.sgWeighting.Cells[i+1,1]) + ',','NaN,');
        sWeighting := leftStr(sWeighting,length(sWeighting) - 1); //strip out last comma

        {get current table IDs so we can add an ID integer. The ID will be higher than
        all the other IDs}
        iNewID := 1;
        for i := 0 to cbWeightingSelect.Items.Count - 1 do
            if TFishCurve(cbWeightingSelect.Items.Objects[i]).DataSource = TDataSource.dsUser then
                if TFishCurve(cbWeightingSelect.Items.Objects[i]).ID >= iNewID then
                    iNewID := TFishCurve(cbWeightingSelect.Items.Objects[i]).ID + 1;

        {create the sql string, and execute. we are adding the user data into the user database. note that some fields
        are numeric.}
        eventLog.log('Add freq weighting to database');
        userSldb.BeginTransaction; //begin a transaction
        //NB this inserts the data as 'User', not 'MDA'
        sSQL := 'INSERT INTO ' + fishTblName + ' (ID,Datasource,Name,Category1,Bandwidth,Comments,ThirdOctaveLevelsSerial,baseFreq';
        sSQL := sSQL + ') VALUES (' + tostr(iNewID) + ',"' + sUserDB + '","' + FishAddToDB.EditName.Text + '"';
        sSQL := sSQL + ',"Fish","' + TSpectrum.bandwidthString(bwThirdOctave) +'"';
        sSQL := sSQL + ', "' + FishAddToDB.memoComments.Text + '","' + sWeighting + '"';
        sSQL := sSQL + ',12.5);';
        userSldb.ExecSQLUnicode(sSQL);     //do the insert
        userSldb.Commit;   //end the transaction

        {update the list of weightings}
        weightingSelectBoxInit;
        cbWeightingSelect.ItemIndex := cbWeightingSelect.Items.Count - 1; //go to last item, which will be the new one
        cbWeightingSelectChange(nil);
        EditBoxesInit;
    end;
end;

procedure TEditFishFrame.btRemoveFromDBClick(Sender: TObject);
var
    buttonSelected : Integer;
    sSQL        : String;
    //FC        : TFishCurve;
    ind : integer;
begin
    ind := cbWeightingSelect.ItemIndex;

    if not (TFishCurve(cbWeightingSelect.Items.Objects[ind]).DataSource = TDataSource.dsUser) then Exit;

    {ask the user if they really want to delete the selected item}
    buttonSelected := MessageDlg('Really delete item ' + TFishCurve(cbWeightingSelect.Items.Objects[ind]).Name + '?',
                                 mtConfirmation,
                                 mbOKCancel,
                                 0);

    {if they do, then do the SQL Delete}
    if buttonSelected = mrOK then
    begin
        eventLog.log('Remove freq weighting from database');
        userSldb.BeginTransaction; //begin a transaction
        sSQL := 'DELETE FROM ' + fishTblName + ' WHERE ID=' + tostr(TFishCurve(cbWeightingSelect.Items.Objects[ind]).ID) + ';';
        userSldb.ExecSQLUnicode(sSQL);     //do the delete
        userSldb.Commit;   //end the transaction

        {update the list of weightings. }
        weightingSelectBoxInit;
        cbWeightingSelect.ItemIndex := 0;
        cbWeightingSelectChange(nil); //call onchange, to update graphs etc
        EditBoxesInit;
    end;
end;

procedure TEditFishFrame.cbFixScaleClick(Sender: TObject);
begin
    chartUpdate;
end;

procedure TEditFishFrame.chartUpdate;
var
    fi,startfi,lenfi : integer;
    mn, mx, L : double;
    bw          : TBandwidth;
    minInd, maxInd : integer;
    fc : TFishCurve;
begin
    startfi := container.scenario.startfi;
    lenfi := container.scenario.lenfi;
    bw := container.scenario.bandwidth;

    mx := NaN;
    mn := NaN;

    fc := TFishCurve(cbWeightingSelect.Items.Objects[cbWeightingSelect.ItemIndex]);
    if not assigned(fc) then
    begin
        eventLog.log('Error: TFishCurve not assigned in TEditFishFrame');
        exit;
    end;

    srFishCurve.Clear;
    srFishCurve.ColorEachPoint := true;
    minInd := 0;
    for fi := 0 to TSpectrum.getLength(bw) - 1 do
        if not isnan(fc.spectrum.getMitigationAmpbyInd(fi,bw,eventlog)) then
        begin
            minInd := fi;
            break;
        end;

    maxInd := TSpectrum.getLength(bw) - 1;
    for fi := TSpectrum.getLength(bw) - 1 downto 0 do
        if not isnan(fc.spectrum.getMitigationAmpbyInd(fi,bw,eventlog)) then
        begin
            maxInd := fi;
            break;
        end;

    for fi := minInd to maxInd do
    begin
        L := fc.Spectrum.getMitigationAmpbyInd(fi,bw,eventlog);
        srFishCurve.AddXY(fi,
                          ifthen(isNaN(L),0,L),
                          TSpectrum.getFStringbyInd(fi,bw,eventlog) + 'Hz',
                          ifthen((fi >= startfi) and (fi <= lenfi), TDBSeaColours.darkBlue, TDBSeaColours.greyText));
        greater(mx,L);
        smaller(mn,L);
    end;

    chFishCurve.Axes.Bottom.Increment := 1;
    if cbFixScale.Checked or isnan(mx) then
        chFishCurve.Axes.Left.SetMinMax(0,140)
    else
        chFishCurve.Axes.Left.SetMinMax(0,5 * ceil(mx / 5));

    {if isnan mx, then there are only nan values for the given freqencies, so show
    the label saying 'no threshold at these freqs'}
    lblNoThreshold.Visible := isNaN(mx);
end;

procedure TEditFishFrame.didChangeScenario;
begin
    frameshow(nil);
end;

procedure TEditFishFrame.didChangeUnits;
begin
    editboxesInit;
end;

procedure TEditFishFrame.didSetLevelType(const levelType: TLevelType);
begin
    if TLevelConverter.isUnweighted(levelType) then
    begin
        if cbWeightingSelect.Items.Count = 0 then
        begin
            eventLog.log('Error: Marine species weighting select box contains no entries');
        end
        else
        begin
            cbWeightingSelect.ItemIndex := 0;
            cbWeightingSelectChange(self);
        end;
    end;
end;

end.

