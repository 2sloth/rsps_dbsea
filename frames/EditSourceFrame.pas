{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit EditSourceFrame;

interface

uses
    syncommons,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, baseTypes,
  types, sourceSpectFrm, StrUtils, CSVDelimiters, math,
  Buttons, sourceMotionFrm, pngimage,
  helperFunctions, srcPoint, soundSource, dBSeaconstants, spectrum,
  sourceSliceSelector, directivityInput, levelConverter, sourceTimeSeries,
  thresholdLevels, system.UITypes, scenario, problemContainer, mcklib,
  dBSeaOptions, globalsUnit, GLWindow, MyFrameInterface, LatLongEntryFrm,
  SmartPointer, eventLogging;

type

  TEditSourceFrame = class(TFrame, IMyFrame)
    lblName: TLabel;
    edName: TEdit;
    btSrccolorPick: TButton;
    cbSource: TComboBox;
    edZ: TEdit;
    edY: TEdit;
    edX: TEdit;
    lblZ: TLabel;
    lblY: TLabel;
    lblX: TLabel;
    btOk: TButton;
    btCancel: TButton;
    cbSrcEnabled: TCheckBox;
    btSourceAdd: TButton;
    btSourceRemove: TButton;
    btSrcSpectEdit: TButton;
    lbDepthUnderSource: TLabel;
    imPageTitle: TImage;
    lblPageTitle: TLabel;
    btImportSources: TButton;
    btSetMotion: TButton;
    cbMovingsource: TCheckBox;
    btDuplicateSource: TButton;
    btShowXSec: TButton;
    lbThreshold: TLabel;
    Panel1: TPanel;
    edThreshold: TEdit;
    lbMaxDist: TLabel;
    lbAverageDist: TLabel;
    btDirectivity: TButton;
    btShowThresholdsGL: TButton;
    btSetTimeSeries: TButton;
    cbThresholds: TComboBox;
    lbLevel: TLabel;
    cbShowThresholds: TCheckBox;
    odImportSources: TOpenDialog;
    cbDontUseGriddedTL: TCheckBox;

    constructor Create(AOwner: TComponent); override;
    procedure frameshow(Sender: TObject);
    procedure framehide(Sender: TObject);

    procedure cbSourceChange(Sender: TObject);
    procedure cbSourceClick(Sender: TObject);

    procedure btSrccolorPickClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btSourceAddClick(Sender: TObject);
    procedure btSourceRemoveClick(Sender: TObject);

    procedure btSrcSpectEditClick(Sender: TObject);
    procedure EditBoxesChange(Sender: TObject);
    procedure btImportSourcesClick(Sender: TObject);
    procedure btSetMotionClick(Sender: TObject);
    procedure cbMovingsourceClick(Sender: TObject);
    procedure btDuplicateSourceClick(Sender: TObject);
    procedure btShowXSecClick(Sender: TObject);

    procedure btDirectivityClick(Sender: TObject);
    procedure btShowThresholdsGLClick(Sender: TObject);
    procedure btSetTimeSeriesClick(Sender: TObject);
    procedure cbThresholdsChange(Sender: TObject);
    procedure cbThresholdsClick(Sender: TObject);
    procedure cbShowThresholdsClick(Sender: TObject);
    procedure edMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbSrcEnabledClick(Sender: TObject);
    procedure edNameChange(Sender: TObject);
    procedure cbDontUseGriddedTLClick(Sender: TObject);
  private
    updateFromCode      : boolean;
    modified            : boolean;
    iSourceIndex        : integer;   //needed so that we can modify the 'previous' source data, when changing selectbox index away from the current source
    //lastThresholdLevel  : double;

        procedure myUpdate(p: TProc);
        procedure setModified(const val: boolean);
        procedure setButtonsEnabled(const val: boolean);
        procedure updateDistanceToThresholdLabels();
        procedure didChangeUnits;
        procedure didChangeScenario;
    public
        // bLastActionWasMoveSource : boolean;
        procedure EditBoxesInit;
        procedure AskToSaveChanges;
        procedure cbSourceInit;
        procedure clearSourceThreshold;
        function  sourceForThreshold: TSource;
    end;

var
    framSource: TEditSourceFrame;

implementation
  {$B-}

{$R *.dfm}

uses Main, EditFishFrame;

constructor TEditSourceFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        iSourceIndex := 0;
        cbSourceInit;
        myUpdate(procedure begin
            cbSource.ItemIndex := 0;
        end);

        //TThresholdLevels.thresholdsBoxInit(self.cbThresholds,MDASldb,userSldb);
        //lastThresholdLevel := nan;

        //bLastActionWasMoveSource := false;

    //    {call the framehide, as the first source is currently set to the
    //    wrong color. calling framehide will set it back to the original color}
    //    framehide(nil);

        {$IF Defined(LITE)}
        btSetTimeSeries.Enabled := false;
        btDirectivity.Enabled := false;
        cbMovingsource.Enabled := false;
        {$ENDIF}
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditSourceFrame.didChangeScenario;
begin
    iSourceIndex := 0;
    cbSourceInit;
    if cbSource.Items.Count > 0 then
        cbSource.ItemIndex := 0;
    frameshow(nil);
end;

procedure TEditSourceFrame.didChangeUnits;
begin
    EditBoxesInit;
end;

procedure TEditSourceFrame.frameshow(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'frameshow');
    try
        updateFromCode := true;

        EditBoxesInit;

        eventlog.log(log, 'Open thresholds DB');
        TThresholdLevels.thresholdsBoxInit(self.cbThresholds,MDASldb,userSldb);
        cbThresholds.enabled := container.scenario.levelType <> kSPLCrestFactor;

        updateFromCode := false;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditSourceFrame.framehide(Sender: TObject);
var
    src             : TSource;
begin
    AskToSaveChanges;

    {reset all sources to their base colors when we exit the frame page}
    for src in container.scenario.sources do
        src.setAltColor(false);
    if assigned(GLForm) then
        GLForm.forceDrawNow;
end;

procedure TEditSourceFrame.AskToSaveChanges;
begin
    if not modified then Exit;
    btOkClick(self);
end;

procedure TEditSourceFrame.btSetMotionClick(Sender: TObject);
var
    i : integer;
    src         : TSource;
    ind : integer;
begin
    if container.isSolving then Exit;
    if container.scenario.sources.Count = 0 then Exit;

    AskToSaveChanges;

    {create the TSourceMotionForm and set the current source as the data for the TSourceMotionForm}
    ind := cbSource.itemindex;
    if not checkrange(ind,0,container.scenario.sources.Count - 1) then exit;
    src := container.scenario.sources[ind];
    sourceMotionForm.setSrc(src);
    if sourceMotionForm.showModal = mrOK then
    begin
        {copy back all the points from the temp store in the form}
        src.MovingPos.Clear;
        for i := 0 to sourceMotionForm.Points.Count - 1 do
        begin
            src.MovingPos.Add(TSrcPoint.Create);
            src.MovingPos.Last.cloneFrom(sourceMotionForm.Points[i]);
        end;
        src.updateTempMovingPosList;

        eventLog.log('Update source motion for source ' + src.name + ' in scenario ' + container.scenario.name +
                     ', number of points ' + tostr(src.getMovingPosMax));

        EditBoxesInit;
        //bLastActionWasMoveSource := false;

        if assigned(GLForm) then
           GLForm.forceDrawNow;
    end;
end;

procedure TEditSourceFrame.btSetTimeSeriesClick(Sender: TObject);
var
    src : TSource;
    log : ISynLog;
begin
    if container.isSolving then Exit;
    if container.scenario.sources.Count = 0 then Exit;

    log := TSynLogLevs.enter(self,'btSetTimeSeriesClick');

    if not TLevelConverter.isTimeSeries(container.scenario.levelType) then
    begin
        if MessageDlg('Change this scenario from frequency-based calculation to time series calculation (NOAA: impulsive)?',
                      mtConfirmation ,
                      mbOKCancel,
                      0) = mrOK then
        begin
            eventLog.log('Change scenario to time domain ' + container.scenario.name);
            container.scenario.setLevelType(kSELtime);
            framFish.didSetLevelType(container.scenario.levelType);
            glForm.setRedrawResults();

            //dBSeaRay must be used for time series calc
            container.scenario.splitSolver := false;
            container.scenario.solver := cmdBSeaRay;
        end
        else
            exit;
    end;

    if (self.cbSource.ItemIndex >= container.scenario.sources.Count) then
    begin
        eventLog.log('Error: Attempt to choose source index ' + tostr(self.cbSource.ItemIndex) + ', scenario contains ' + tostr(container.scenario.sources.Count) + ' sources');
        exit;
    end;

    src := container.scenario.sources[self.cbSource.ItemIndex];
    sourceTimeSeriesForm.timeSeries.cloneFrom(src.timeSeries);
    sourceTimeSeriesForm.mitigation.cloneFrom(src.srcDBData.mitigationEquipReturn);
    if sourceTimeSeriesForm.ShowModal = mrOK then
    begin
        src.setTimeSeries(sourceTimeSeriesForm.timeSeries);
        src.srcDBData.mitigationEquipReturn.cloneFrom(sourceTimeSeriesForm.mitigation);
        eventLog.log('Update time series for source ' + src.name + ' in scenario ' + container.scenario.name +
                     ', fs ' + tostrWithTestAndRound1(src.timeSeries.fs) + ', number of samples ' + tostr(length(src.timeSeries.samples)));
    end;

    GLForm.drawNow;
end;

procedure TEditSourceFrame.btShowThresholdsGLClick(Sender: TObject);
var
    src : TSource;
begin
    if not checkrange(cbSource.itemindex,0,container.scenario.sources.Count - 1) then exit;

    src := container.scenario.sources[cbSource.itemindex];
    if src.srcSpectrumSet and src.srcSolved and isfloat(edThreshold.Text) and not src.isMovingSource then
    begin
        if src.nSlices < 4 then
        begin
            showmessage('Too few slices to show exclusion zone');
            exit;
        end;

        src.updateMaxDistancesToThreshold(container.scenario.unconvertLevel(tofl(edThreshold.Text)));
        updateDistanceToThresholdLabels();

        if not assigned(GLForm) then exit;

        if GLForm.showSourceThreshold then
            clearSourceThreshold
        else
        begin
            glForm.setDrawSourceThresholds();
            glForm.forceDrawNow;
            btShowThresholdsGL.Caption := 'Hide threshold';
        end;
    end;
end;

function TEditSourceFrame.sourceForThreshold: TSource;
begin
    if not checkrange(self.cbSource.ItemIndex,0,container.scenario.sources.Count - 1) then exit(nil);

    result := container.scenario.sources[self.cbSource.ItemIndex];
end;

procedure TEditSourceFrame.clearSourceThreshold;
begin
    if assigned(GLForm) then
    begin
        if GLForm.showSourceThreshold then
        begin
            GLForm.clearSourceThreshold;
            glForm.forceDrawNow;
        end;
        btShowThresholdsGL.Caption := 'Show threshold';
    end;
end;

procedure TEditSourceFrame.btShowXSecClick(Sender: TObject);
var
    ind : integer;
    x2,y2 : double;
    r, ang : double;
begin
    ind := cbSource.itemindex;
    if not checkrange(ind,0,container.scenario.sources.Count - 1) then exit;

    sourceSliceSelectorForm.src := container.scenario.sources[ind];
    if sourceSliceSelectorForm.ShowModal = mrOK then
    begin
        //need to work out the maximum length that the xsec can be
        r := sourceSliceSelectorForm.src.rMax;
        ang := sourceSliceSelectorForm.src.angles(sourceSliceSelectorForm.udSlice.Position);
        if (ang > 0) and (ang < pi) then
            r := min(r, abs((container.scenario.theRA.yMax - sourceSliceSelectorForm.src.pos.Y) / sin(ang)));
        if (ang > pi) and (ang < 2*pi) then
            r := min(r, abs((sourceSliceSelectorForm.src.pos.Y - 0.0) / sin(ang)));

        if (ang > pi/2) and (ang < 3*pi/2) then
            r := min(r, abs((sourceSliceSelectorForm.src.pos.X - 0.0) / sin(ang)));
        if (ang < pi/2) or (ang > 3*pi/2) then
            r := min(r, abs((container.scenario.theRA.xMax - sourceSliceSelectorForm.src.pos.X) / sin(ang)));

        r := r - 0.1; //because sometimes we are getting math rounding problems

        x2 := sourceSliceSelectorForm.src.pos.X + r * cos(ang);
        y2 := sourceSliceSelectorForm.src.pos.Y + r * sin(ang);
        forcerange(x2,0.0,container.scenario.theRA.xMax);
        forcerange(y2,0.0,container.scenario.theRA.yMax);
        container.crossSec.setPos(sourceSliceSelectorForm.src.pos.X, sourceSliceSelectorForm.src.pos.Y, x2, y2);
        if assigned(GLForm) then
        begin
            GLForm.bDrawCrossSection := true;
            GLForm.force2DrawNow;
        end;
    end;
end;

procedure TEditSourceFrame.btSourceAddClick(Sender: TObject);
var
    src : TSource;
begin
    if container.isSolving then Exit;

    AskToSaveChanges;

    src := container.scenario.addSource;

    cbSourceInit;
    cbSource.ItemIndex := container.scenario.sources.Count - 1;
    cbSourceChange(self);
    EditBoxesInit;
    //bLastActionWasMoveSource := false;
    if assigned(GLForm) then
    begin
        {set the 'Source n' GL text}
        GLForm.setSourceProbeText(src.Name,src.Pos.X,src.Pos.Y);
        clearSourceThreshold;
        GLForm.bDrawCrossSection := false; //we want birdseye view after adding a source
        GLForm.force2Drawnow;
    end;
end;

procedure TEditSourceFrame.btSourceRemoveClick(Sender: TObject);
var
    buttonSelected : Integer;
    ind : integer;
begin
    if container.isSolving then Exit;

    if container.scenario.sources.Count <= 1 then
    begin
        Showmessage('At least one source must exist');
        Exit;
    end;

    ind := cbSource.itemindex;

    buttonSelected := MessageDlg('Really delete source ' + container.scenario.sources[ind].Name + '?',
                                 mtConfirmation ,
                                 mbOKCancel,
                                 0);

    if buttonSelected = mrOK then
    begin
        container.scenario.sources.Delete(ind);

        ind := ind - 1;
        forceRange(ind,0,container.scenario.sources.Count - 1);
        cbSourceInit;
        cbSource.ItemIndex := ind;
        EditBoxesInit;
        clearSourceThreshold;
        //bLastActionWasMoveSource := false;
        if assigned(GLForm) then
        begin
            GLForm.clearSourceProbeText;
            GLForm.forceDrawNow;
        end;
    end;
end;

procedure TEditSourceFrame.btSrccolorPickClick(Sender: TObject);
var
    ColorDialog: ISmart<TColorDialog>;
begin
    if not checkRange(cbSource.ItemIndex,0,container.scenario.sources.Count - 1) then Exit;

    ColorDialog := TSmart<TColorDialog>.create(TColorDialog.Create(Self));
    Colordialog.CustomColors.CommaText := UWAOpts.customColors;
    if colordialog.execute then
        container.scenario.sources[cbSource.ItemIndex].setColor(colordialog.color);
    UWAOpts.customColors := colordialog.CustomColors.CommaText;
    if assigned(GLForm) then
        GLForm.setRedraw();
end;

procedure TEditSourceFrame.btCancelClick(Sender: TObject);
begin
    EditBoxesInit;
    setModified(false);
    //btSrcEditOk.Enabled := false;
end;

procedure TEditSourceFrame.btDirectivityClick(Sender: TObject);
begin
    directivityInputForm.src := container.scenario.sources[cbSource.itemindex];
    directivityInputForm.defaultopenDir := DefaultSavePath;
    if directivityInputForm.ShowModal = mrOK then
    begin
        //do nothing, the form itself manages the src object
    end;
end;

procedure TEditSourceFrame.btDuplicateSourceClick(Sender: TObject);
var
    ind : integer;
    src, newSrc : TSource;
begin
    if container.isSolving then Exit;

    AskToSaveChanges;

    ind := cbSource.ItemIndex;
    if not checkRange(ind,0,container.scenario.sources.Count - 1) then exit;

    //get the currently selected source
    src := container.scenario.sources[ind];

    //create a new source and clone it from src
    newSrc := container.scenario.addSource;
    newSrc.cloneFrom(src);

    //append 'copy' to the source name
    newSrc.Name := newSrc.Name + ' copy';

    cbSourceInit;
    cbSource.ItemIndex := container.scenario.sources.Count - 1;
    cbSourceChange(self);
    EditBoxesInit;
    //bLastActionWasMoveSource := false;
    if assigned(GLForm) then
    begin
        {set the 'Source n' GL text}
        GLForm.setSourceProbeText(newSrc.Name,newSrc.Pos.X,newSrc.Pos.Y);
        GLForm.bDrawCrossSection := false; //we want birdseye view after adding a source
        clearSourceThreshold;
        GLForm.force2Drawnow;
    end;
end;

procedure TEditSourceFrame.btImportSourcesClick(Sender: TObject);
begin
    if not assigned(container) then Exit;
    if container.isSolving then Exit;

    if not odImportSources.Execute then exit;

    if not FileExists(odImportSources.FileName) then
    begin
        Showmessage('File does not exist');
        Exit;
    end;

    if THelper.IsFileInUse(odImportSources.FileName) then
    begin
        Showmessage('File could not be opened as it is in use by another process');
        Exit;
    end;

    //ok to proceed with reading from file
    container.scenario.readSourcesFromFile(odImportSources.FileName);

    cbSourceInit;
    EditBoxesInit;
    clearSourceThreshold;
    GLForm.bDrawCrossSection := false;
    GLForm.force2drawNow();
end;

procedure TEditSourceFrame.btOkClick(Sender: TObject);
var
    //ind: integer;
    src : TSource;
begin
    if container.isSolving then Exit;

    if not checkRange(iSourceIndex,0,container.scenario.sources.Count - 1) then
    begin
        if container.scenario.sources.Count < 1 then
            showmessage('Create a source before editing values.')
        else
            showmessage('Invalid or no source selected.');
        Exit;
    end;

    if not isFloat(edx.Text) then Exit;
    if not isFloat(edy.Text) then Exit;
    if not isFloat(edz.Text) then Exit;
    if edname.Text = '' then
        edname.Text := 'Unnamed source';

    src := container.scenario.sources[iSourceIndex];
    {setpos includes the f2m command, if necessary (decided by the last boolean argument)}
    //src.setPos(f2m(tofl(edx.Text),UWAOpts.dispFeet) - prob.Easting,
    //            f2m(tofl(edy.Text),UWAOpts.dispFeet) - prob.Northing,
    //            f2m(tofl(edz.Text),UWAOpts.dispFeet));
    if src.setPos(container.geoSetup.getXFromString(edx.Text),container.geoSetup.getYFromString(edy.Text),container.geoSetup.getZFromString(edz.Text)) then
        if assigned(GLForm) then
        begin
            GLForm.clearSourceThreshold;
            GLForm.setRedraw;
        end;

    src.Name := edname.Text;
    src.Enabled := cbSrcEnabled.Checked;
    //src.Comments := mmComments.Text;

    lastAction := TLastAction.kMoveSource;

    cbSourceInit;   //because names may have changed
    cbSource.ItemIndex := iSourceIndex;
    cbSourceChange(self);

    setModified(false);
    if assigned(GLForm) then
        GLForm.forceDrawNow;
end;

procedure TEditSourceFrame.btSrcSpectEditClick(Sender: TObject);
var
    i           : integer;
    src         : TSource;
    ind         : integer;
    tmpSrcSpect : TDoubleDynArray; //used for seeing if the spectrum has changed, so we need to update results
    bSpectraDiffer : boolean;
    log         : ISynLog;
begin
    if container.isSolving then Exit;
    if container.scenario.sources.Count = 0 then Exit;

    log := TSynLogLevs.enter(self,'btSrcSpectEditClick');

    if TLevelConverter.isTimeSeries(container.scenario.levelType) then
    begin
        if MessageDlg('Change this scenario from time series calculation to frequency-based calculation (NOAA: non-impulsive)?',
                      mtConfirmation ,
                      mbOKCancel,
                      0) = mrOK then
        begin
            container.scenario.setLevelType(kSPL);
            framFish.didSetLevelType(container.scenario.levelType);
            GLForm.setRedrawResults;

            eventLog.log('Change scenario to freq domain ' + container.scenario.name);
        end
        else
            exit;
    end;

    AskToSaveChanges;

    ind := cbSource.itemindex;
    forceRange(ind,0,container.scenario.sources.Count - 1);
    src := container.scenario.sources[ind];

    sourceSpectrumForm.setSrc(src);
    if sourceSpectrumForm.showModal = mrOK then
    begin
        src.displayLevelType := sourceSpectrumForm.displayLevelType;
        setlength(tmpSrcSpect,TSpectrum.maxLength);
        for i := 0 to TSpectrum.maxLength - 1 do
            tmpSrcSpect[i] := src.SrcSpectrum.getAmpbyInd(i,bwThirdOctave);
        src.SrcSpectrum.clear;
        src.spectrumFromEquipmentReturn;

        {now we can see if the results should update}
        bSpectraDiffer := false;
        for i := 0 to TSpectrum.maxLength - 1 do
            if src.SrcSpectrum.getAmpbyInd(i,bwThirdOctave) <> tmpSrcSpect[i] then
                bSpectraDiffer := true;

        if bSpectraDiffer and src.SrcSolved then
        begin
            eventLog.log('Update spectrum for source ' + src.name + ' in scenario ' + container.scenario.name);
            if assigned(GLForm) then
            begin
                GLForm.setRedrawResults;
                GLForm.force2DrawNow;
            end;
        end;

        EditBoxesInit;
    end;

    GLForm.drawNow;
end;

procedure TEditSourceFrame.cbDontUseGriddedTLClick(Sender: TObject);
begin
    if not updateFromCode then
    begin
        container.scenario.movingSourcesUseGriddedTL := not cbDontUseGriddedTL.checked;
        if container.scenario.movingSourcesUseGriddedTL then
            eventLog.log('Moving sources in scenario ' + container.scenario.name + ' to use gridded TL memory plan')
        else
            eventLog.log('Moving sources in scenario ' + container.scenario.name + ' to retain all calculated levels for every point');
    end;
end;

procedure TEditSourceFrame.cbMovingsourceClick(Sender: TObject);
var
    src : TSource;
    ind : integer;
begin
    ind := cbSource.itemindex;
    forceRange(ind,0,container.scenario.sources.Count - 1);
    src := container.scenario.sources[ind];
    src.IsMovingSource := cbMovingsource.Checked;
    if src.isMovingSource then
        eventLog.log('Set source ' + tostr(ind) + ' in scenario ' + container.scenario.name + ' to moving source')
    else
        eventLog.log('Set source ' + tostr(ind) + ' in scenario ' + container.scenario.name + ' to static source');
    EditBoxesInit;

    if assigned(GLForm) then
    begin
        GLForm.clearSourceThreshold;
        GLForm.forceDrawNow;
    end;
end;

procedure TEditSourceFrame.cbShowThresholdsClick(Sender: TObject);
begin
    self.EditBoxesInit;
end;

procedure TEditSourceFrame.cbThresholdsChange(Sender: TObject);
begin
    if updateFromCode then exit;
    if cbThresholds.ItemIndex = 0 then exit;

    edThreshold.Text := tostrWithTestAndRound1(TThresholdLevels(cbThresholds.Items.Objects[cbthresholds.ItemIndex]).threshold(container.scenario.levelType));
    btShowThresholdsGLClick(self);
end;

procedure TEditSourceFrame.cbThresholdsClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox;
end;

procedure TEditSourceFrame.cbSourceChange(Sender: TObject);
var
    src : TSource;
    ind : integer;
begin
    AskToSaveChanges;

    if cbSource.ItemIndex >= 0 then
        iSourceIndex := cbSource.ItemIndex;
    EditBoxesInit;
    //bLastActionWasMoveSource := false;
    //btSrcEditOk.Enabled := false;
    if assigned(GLForm) then
    begin
        {set the 'Source n' GL text}
        ind := self.cbSource.ItemIndex;
        if checkRange(ind,0,container.scenario.sources.Count - 1) then
        begin
            src := container.scenario.sources[ind];
            GLForm.setSourceProbeText(src.Name,src.Pos.X,src.Pos.Y);
        end;
        clearSourceThreshold;
        GLForm.force2DrawNow;
    end;
end;

procedure TEditSourceFrame.cbSourceClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox;
end;

procedure TEditSourceFrame.EditBoxesInit;
var
    src: TSource;
    dDepth: double;
    sUnits: String;
    level: double;
begin
    if container.scenario.sources.Count = 0 then exit;
    if not checkrange(cbSource.itemindex,0,container.scenario.sources.Count - 1) then exit;

    updateFromCode := true;

    {only enable the source remove button, if more than 1 source is there}
    btSourceRemove.Enabled := container.scenario.sources.Count > 1;

    sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');

    lblX.Caption := 'x ' + sUnits;
    lblY.Caption := 'y ' + sUnits;
    lblZ.Caption := 'Depth z ' + sUnits;

    src := container.scenario.sources[cbSource.itemindex];
    edName.Text := src.Name;

    if src.srcspectrumset then
        lbLevel.Caption := 'Level: ' + container.scenario.formatLevelForSource(src)
    else
        lbLevel.Caption := 'Level not set';

    cbSrcEnabled.checked := src.Enabled;
    edx.Text := ifthen(src.IsMovingSource,'Moving source',container.geoSetup.makeXString(src.Pos.x));
    edy.Text := ifthen(src.IsMovingSource,'Moving source',container.geoSetup.makeYString(src.Pos.Y));
    edz.Text := ifthen(src.IsMovingSource,'Moving source',container.geoSetup.makeZString(src.Pos.Z));
    cbMovingsource.Checked := src.IsMovingSource;
    edx.Enabled := not src.IsMovingSource;
    edy.Enabled := not src.IsMovingSource;
    edz.Enabled := not src.IsMovingSource;
    btSetMotion.Enabled := src.IsMovingSource;

    cbDontUseGriddedTL.checked := not container.scenario.movingSourcesUseGriddedTL;

    {get the water depth under the source and set the label to show it}
    dDepth := container.getBathymetryAtPos(src.Pos.X,src.Pos.Y);
    if isnan(dDepth) then
        lbDepthUnderSource.Caption := 'Water depth at source location: n/a'
    else
        lbDepthUnderSource.Caption := 'Water depth at source location: ' + container.geoSetup.makeZString(dDepth) + ' ' + sUnits;

    if src.srcSpectrumSet and (not isfloat(edThreshold.Text)) then
    begin
        level := src.SrcSpectrum.Level(container.scenario.startfi,container.scenario.endfi,container.scenario.Bandwidth);
        //if container.scenario.levelType is a time series type, then it just goes to nan and nothing happens
        level := TLevelConverter.convert(level, kSPL, container.scenario.levelType, container.scenario.assessmentPeriod, src.srcSpectrumCrestFactor);
        if not isnan(level) then
            edThreshold.Text := tostr(round1(level - 50));
    end;
    if cbShowThresholds.Checked and src.srcSpectrumSet and src.srcSolved and isfloat(edThreshold.Text) and not src.isMovingSource then
    begin
        src.updateMaxDistancesToThreshold(container.scenario.unconvertLevel(tofl(edThreshold.Text)));
        updateDistanceToThresholdLabels();
    end;

    if assigned(GLForm) then
    begin
        if GLForm.showSourceThreshold then
            btShowThresholdsGL.Caption := 'Hide threshold'
        else
            btShowThresholdsGL.Caption := 'Show threshold';
    end;

    updateFromCode := false;
    setModified(false);
end;

procedure TEditSourceFrame.edMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if (Button = mbRight) and (sender is TEdit) and (latLongEntryForm.setEditsAndShowModal(edx,edy) = mrOK) then
        btOkClick(sender);
end;

procedure TEditSourceFrame.edNameChange(Sender: TObject);
var
    oldItemIndex: integer;
begin
    if String(edName.Text).isEmpty then exit;

    container.scenario.sources[iSourceIndex].name := edName.Text;
    if (iSourceIndex < cbSource.items.count) then
    begin
        oldItemIndex := cbSource.ItemIndex;
        cbSource.Items[iSourceIndex] := edName.Text;
        cbSource.ItemIndex := oldItemIndex;
        if (cbSource.ItemIndex = iSourceIndex) or (cbSource.ItemIndex = -1) then
            cbSource.Text := edName.Text;
    end;
end;

procedure TEditSourceFrame.myUpdate(p: TProc);
begin
    updateFromCode := true;
    try
        p();
    finally
        updateFromCode := false;
    end;
end;

procedure TEditSourceFrame.updateDistanceToThresholdLabels();
var
    src : TSource;
    maxDist, maxAngle : double;
begin
    edThreshold.Enabled := cbShowThresholds.Checked;
    cbThresholds.Enabled := cbShowThresholds.Checked;
    btShowThresholdsGL.Enabled := cbShowThresholds.Checked;

    src := self.sourceForThreshold;
    if cbShowThresholds.Checked and src.srcSpectrumSet and src.srcSolved and isfloat(edThreshold.Text) and (not src.isMovingSource) then
    begin
        src.maxDistAngleToThreshold(maxDist,maxAngle);
        if isnan(maxDist) or isnan(maxAngle) then
        begin
            lbMaxDist.Caption := 'Max distance: N/A';
        end
        else
        begin
            lbMaxDist.Caption := 'Max distance: ' + tostr(round1(m2f(maxDist,UWAOpts.dispFeet))) +
                                 ifthen(UWAOpts.dispFeet,' ft',' m') + ' at ' + tostr(round1(angleRadToCompassAngleDeg(maxAngle))) + '�';
        end;

        if isnan(src.averageDistanceToThreshold()) then
            lbAverageDist.Caption := 'Average distance: N/A'
        else
            lbAverageDist.Caption := 'Average distance: ' + tostr(round1(m2f(src.averageDistanceToThreshold(),UWAOpts.dispFeet))) + ifthen(UWAOpts.dispFeet,' ft',' m');

        lbMaxDist.Visible := true;
        lbAverageDist.Visible := true;
    end
    else
    begin
        lbMaxDist.Visible := false;
        lbAverageDist.Visible := false;
    end;
end;

procedure TEditSourceFrame.setModified(const val : boolean);
begin
    if modified <> val then
    begin
        modified := val;
        setButtonsEnabled(modified);
    end;
end;

procedure TEditSourceFrame.setButtonsEnabled(const val : boolean);
begin
    self.btOk.Enabled := val;
    self.btCancel.Enabled := val;
end;

procedure TEditSourceFrame.EditBoxesChange(Sender: TObject);
begin
    if updatefromcode then exit;
    setModified(true);
end;

procedure TEditSourceFrame.cbSourceInit;
var
    src: TSource;
begin
    cbSource.items.clear;
    cbSource.DoubleBuffered := true;
    cbSource.AutoComplete := true;
    for src in container.scenario.sources do
        cbSource.AddItem(src.Name, nil);
    if (container.scenario.sources.Count = 0) or (iSourceIndex < 0) or (iSourceIndex >= container.scenario.sources.count) then
    begin
        cbSource.ItemIndex := -1;
        cbSource.Text := '';
    end
    else
        cbSource.ItemIndex := iSourceIndex;

    setModified(false);
    EditBoxesInit;
end;


procedure TEditSourceFrame.cbSrcEnabledClick(Sender: TObject);
begin
    container.scenario.sources[iSourceIndex].enabled := cbSrcEnabled.Checked;
end;

end.

