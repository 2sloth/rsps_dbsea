object EditSedimentFrame: TEditSedimentFrame
  Left = 0
  Top = 0
  Width = 511
  Height = 920
  Constraints.MinHeight = 800
  TabOrder = 0
  object imPageTitle: TImage
    Left = 48
    Top = 18
    Width = 45
    Height = 45
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000002D0000
      002D08060000003A1AE29A00000006624B474400980098009815E12590000000
      097048597300000B1300000B1301009A9C180000000774494D4507DE03190C34
      3383B1DEBF000004DA4944415478DADDD9496C1B551C06F06FBCC4FB9EDD4B16
      B79500094111A8485581527AE8090989251C3872811B9B7A42DC9090581A5A90
      AAB245400B1C2A551CD24015A0A5A5884A592825296D12C78E6313C7763C5EC6
      33C37B93D6B1158F6702711CF34951ACCCB3F59BBF5FDE368C4882260B538EA6
      2F0B1C079EE71BED02437E747A3DF43A9D3C9A42332CDB68EBBA68341A58CC66
      300C53892E168B60B3D946FB64A3D56A2578092D0882C8920AF382D0685BCD18
      8D46B490EE22A149B710573299469B1443BB87CD6A5D7D4DBA4653A0691C76FB
      1A3ABDB2D2688FAA381D8E35743295926D683018A0AB32EC6C66E800C6AA18B9
      DC2ED72A9AA3E86452B6A1D56291E0F50C190CB09448540C6BD5E271BB6FA139
      4E5C4A2CCB3674D86D75479352231A8B2BA2DBDB5A57D1640614E3F1BFE5ABA0
      6F81D7E392BD3EB59840A7C3029BA1A5EA758E17104D67E073DA643F23944841
      9B579E27BA3A3B94D11A72E76F8C5C86B7D58DE7F7DE038FC554BA966073189D
      9AC5E1D3A3E8B05BF0E6E30F6367BB1B2EB351BA9E2FF25848ADE0AD915F7066
      621A2FECBB0F03F7DF89369B193A32CBD12C657218FEE3068E8DFE8AD3CF1D42
      962BAA44173831168FCBA2DF3D3F8E6FAFCDA0C36641C06DC76E7F27FE8A2FE3
      CFC525CC910AD136B7972F7E971DEDA49D56C320952B48D753B9BC84A49397CB
      6C82CF6583957C2B79028CA659849369B4921BFDEAD9836015D0DD5D9D6AD163
      189E0A95FEC60B22590F30A8DDFB3616B7D980530307152B5D862E888BB11AE8
      0BE3385B86AE47DC26034E0E3CA688F67677FD6FD120DD630267A7B7027D80A0
      6BAFE32BD0D1C5980C9AC17B17B606FDE53307142BEDF37637397A21BA288B3E
      F23345CFD71DFDC5D38F2AA2FD3E6F93A3230BD11AE8498C5CAF3FFAF3A7F62B
      A2037E9F3AF4E0A5DFB764C8DB103A4FD19185EA6832FD1E3E3E842BBC19469B
      1DA28A7DA4866C42798E937EAB3D527151F4938F28A27B027E65B496A05F1A3C
      8E6FCE5FC61DFB0F416F32C97C1C99D635AB6B90F9F12B484642083EF8100C56
      F9955D793C560B869ED8AB384EAFA1F305311C89C8A25F3E7A0227BFFB018E4E
      2FFAF7EC2338CD2D2703A1C849552DB019A4A21144AE8ED1153DFD8AE0ECF621
      B07B0FF406A34CC5C9CC250AC8A553C8DDBC868B6FBF8E0C595CD54A6F4F6063
      688AF1F4EF922A48A1B1E9AB88DFBC8E7C260D3ECB4A50D227D6DECCF330D89D
      E8BEEB6E787A8210CA4EADB4648D9E8E2D909B1C27371B469BDD8AC9A10FB092
      CD6D329A86C05B83BBB01C9E43315FBB2AA590F7B87B8308DCFB00B9279DF47F
      31F3DB452CCDDE283569773931F9D9D18DA0F3E27CB836FAD4F73F962148C518
      8DD43D54877619D2459CFE3EA9B242214F8F8D2AD0139FBEAF88EEEBED51877E
      8556BA1C5D87FC2B74683E5CB5111DF25E3DF65165A5EB841EFF645011DDDFD7
      AB8C962ABD5DD173A1EAD334ADF46B147DEEA7BAA3C73E3EA2880EF6F729A369
      A55F7CE7439C193E5757B4D1EDC2CCD72790666B1F2354A067E7E4D7165BF574
      43E9A0866647B05F1D7A3BA584CE51F4EC5CA33DAAB27347B089D17495373333
      DB688F62E871736972A18F2F4264F4C8A95D4B34281D1DED6B4F02E8D32D960C
      35A1F9FA6EA9FE4B4C641D7F7B7F5842D317B4D2B46F6FB707B846A381807DD2
      44B70E4D434FE4979793C8E6B2D8DC23C68D87EE846C64E76325BB9A75D79AF1
      D9F83F96E6F198784DDAE20000000049454E44AE426082}
  end
  object lblPageTitle: TLabel
    Left = 110
    Top = 35
    Width = 92
    Height = 13
    Caption = 'Seafloor properties'
  end
  object pnMaterial: TPanel
    Left = 24
    Top = 204
    Width = 397
    Height = 581
    BevelKind = bkFlat
    BevelOuter = bvNone
    ParentBackground = False
    ParentColor = True
    TabOrder = 0
    object lblAttenuation: TLabel
      Left = 24
      Top = 256
      Width = 140
      Height = 13
      Caption = 'Attenuation (dB/wavelength)'
    end
    object lblC: TLabel
      Left = 24
      Top = 176
      Width = 33
      Height = 13
      Caption = 'c (m/s)'
    end
    object lblComments: TLabel
      Left = 24
      Top = 283
      Width = 50
      Height = 13
      Caption = 'Comments'
    end
    object lblDataSource: TLabel
      Left = 24
      Top = 361
      Width = 58
      Height = 13
      Caption = 'Data source'
    end
    object lblDensity: TLabel
      Left = 24
      Top = 216
      Width = 75
      Height = 13
      Caption = 'Density (kg/m'#179')'
    end
    object lbMaterialName: TLabel
      Left = 24
      Top = 139
      Width = 67
      Height = 13
      Caption = 'Material name'
    end
    object lbDepth: TLabel
      Left = 24
      Top = 41
      Width = 88
      Height = 13
      Caption = 'Starting depth (m)'
    end
    object lbLayerName: TLabel
      Left = 24
      Top = 14
      Width = 56
      Height = 13
      Caption = 'Layer name'
    end
    object lbThickness: TLabel
      Left = 26
      Top = 72
      Width = 65
      Height = 13
      Caption = 'Thickness (m)'
    end
    object mmComments: TMemo
      Left = 24
      Top = 302
      Width = 288
      Height = 43
      ScrollBars = ssVertical
      TabOrder = 0
      OnChange = mmCommentsChange
    end
    object cbSediment: TComboBox
      Left = 24
      Top = 102
      Width = 288
      Height = 21
      DropDownCount = 100
      TabOrder = 1
      OnChange = cbSedimentChange
      OnClick = cbSedimentClick
    end
    object gbSediment: TGroupBox
      Left = 24
      Top = 444
      Width = 225
      Height = 117
      Caption = 'Database'
      TabOrder = 2
      object btRemoveFromDB: TButton
        Left = 24
        Top = 76
        Width = 171
        Height = 25
        Caption = 'Remove selected properties'
        TabOrder = 0
        OnClick = btRemoveFromDBClick
      end
      object btSaveToDB: TButton
        Left = 24
        Top = 28
        Width = 171
        Height = 25
        Caption = 'Save current properties'
        TabOrder = 1
        OnClick = btSaveToDBClick
      end
    end
    object btCancel: TButton
      Left = 103
      Top = 392
      Width = 75
      Height = 25
      Caption = 'Cancel'
      TabOrder = 3
      OnClick = btCancelClick
    end
    object btOk: TButton
      Left = 22
      Top = 392
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 4
      OnClick = btOkClick
    end
    object edAttenuation: TEdit
      Left = 192
      Top = 253
      Width = 120
      Height = 21
      TabOrder = 5
      Text = '0.0'
      OnChange = EditBoxesChange
    end
    object edC: TEdit
      Left = 192
      Top = 173
      Width = 120
      Height = 21
      TabOrder = 6
      Text = '0.0'
      OnChange = EditBoxesChange
    end
    object edName: TEdit
      Left = 192
      Top = 136
      Width = 120
      Height = 21
      TabOrder = 7
      OnChange = edNameChange
    end
    object edRho: TEdit
      Left = 192
      Top = 213
      Width = 120
      Height = 21
      TabOrder = 8
      Text = '0.0'
      OnChange = EditBoxesChange
    end
    object edDepth: TEdit
      Left = 192
      Top = 38
      Width = 120
      Height = 21
      TabOrder = 9
      Text = '0.0'
      OnChange = edDepthChange
    end
    object edLayerName: TEdit
      Left = 192
      Top = 11
      Width = 120
      Height = 21
      TabOrder = 10
      OnChange = edLayerNameChange
    end
  end
  object cbLayers: TComboBox
    Left = 48
    Top = 90
    Width = 373
    Height = 21
    DropDownCount = 100
    TabOrder = 1
    OnChange = cbLayersChange
    OnClick = cbLayersClick
  end
  object btAddLayer: TButton
    Left = 48
    Top = 132
    Width = 133
    Height = 25
    Caption = 'Add layer'
    TabOrder = 2
    OnClick = btAddLayerClick
  end
  object btRemoveLayer: TButton
    Left = 203
    Top = 132
    Width = 133
    Height = 25
    Caption = 'Remove Layer'
    TabOrder = 3
    OnClick = btRemoveLayerClick
  end
  object cbRefToSeafloor: TCheckBox
    Left = 48
    Top = 170
    Width = 301
    Height = 17
    Caption = 'Layer depths referenced from local seafloor'
    TabOrder = 4
    OnClick = cbRefToSeafloorClick
  end
  object cbLocationProps: TComboBox
    Left = 232
    Top = 32
    Width = 189
    Height = 21
    TabOrder = 5
    Text = 'cbLocationProps'
    OnChange = cbLocationPropsChange
    OnClick = cbLocationPropsClick
  end
end
