unit MyFrameInterface;

interface
uses
    generics.collections;

type

    IMyFrame = Interface(IInterface)
        procedure didChangeUnits;
        procedure didChangeScenario;
        procedure frameshow(Sender: TObject);
        procedure framehide(Sender: TObject);
    End;
    IMyFrameList = TList<IMyFrame>;

implementation

end.
