{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit EditProbeFrame;

interface

uses
    syncommons,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, baseTypes, MCKLib,
  StdCtrls, Series, ExtCtrls, Chart, StrUtils,
  ComCtrls, SQLiteTable3, Generics.Collections, Grids, ClipBrd, Math,
  pngimage, pos3, spectrum, helperFunctions, probe, dBSeaColours, scenario,
  dBSeaconstants, soundSource, VCLTee.TeEngine, VclTee.TeeGDIPlus,
  system.UITypes, VCLTee.TeeProcs, problemContainer, receiverArea,
  dBSeaOptions, GLWindow, globalsUnit, options, MyFrameInterface,
  StringGridHelper, LatLongEntryFrm, SmartPointer, ProbeTimeSeries,
  eventLogging;

type

  TDepth = double;
  TLevel = single;
  TDepthLevelRec = record
    depth : TDepth;
    level : TLevel;
    aboveSediment : boolean;
  end;
  TDepthLevelArray = array of TDepthLevelRec;

  TEditProbeFrm = class(TFrame, IMyFrame)
    chSpectrum: TChart;
    srSpectrum: TBarSeries;
    lblZ: TLabel;
    lblY: TLabel;
    lblX: TLabel;
    edZ: TEdit;
    edY: TEdit;
    edX: TEdit;
    btOK: TButton;
    btProbeAdd: TButton;
    btProbeRemove: TButton;
    cbProbe: TComboBox;
    edName: TEdit;
    lblName: TLabel;
    srWeighting: TFastLineSeries;
    lblLevelAboveThreshold: TLabel;
    btProbecolorPick: TButton;
    btCancel: TButton;
    sgProbeLevels: TStringGrid;
    btDataToClipboard: TButton;
    imPageTitle: TImage;
    lblPageTitle: TLabel;
    pcCharts: TPageControl;
    tsSpectrum: TTabSheet;
    tsVerticalLevel: TTabSheet;
    chDepthLevels: TChart;
    //FastLineSeries1: THorizLineSeries;
    lblDepthUnderProbe: TLabel;
    weightingSelectBox: TComboBox;
    lblWeighting: TLabel;
    btGraphicsPrefs: TButton;
    rgLevelsDisplay: TRadioGroup;
    lbLayerNumber: TLabel;
    lbDepth: TLabel;
    btShowHideLevDisp: TButton;
    btDisplayIndUp: TButton;
    btDisplayIndDn: TButton;
    srWaterLevels: TLineSeries;
    srSeabedLevels: TLineSeries;
    btGraphLevelsToClipboard: TButton;
    btDuplicateProbe: TButton;
    btDistancesToSources: TButton;
    tsTimeSeries: TTabSheet;
    chTimeSeries: TChart;
    srTimeSeries: TLineSeries;
    lbTimeSeries: TLabel;

    constructor Create(AOwner: TComponent); override;
    procedure frameshow(Sender: TObject);
    procedure framehide(Sender: TObject);
    procedure EditBoxesInit;

    procedure cbProbeInit;
    procedure cbProbeChange(Sender: TObject);
    procedure updateProbeChartAndTable;
    procedure updateDepthChart(const aPos :TPos3);

    procedure AskToSaveChanges;

    procedure btOkClick(Sender: TObject);
    procedure btProbeAddClick(Sender: TObject);
    procedure btProbeRemoveClick(Sender: TObject);

    procedure btCancelClick(Sender: TObject);
    procedure btProbecolorPickClick(Sender: TObject);
    procedure EditBoxesChange(Sender: TObject);
    procedure btDataToClipboardClick(Sender: TObject);
    procedure sgProbeLevelsDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure setModified(const val : boolean);
    procedure setButtonsEnabled(const val : boolean);
    procedure weightingSelectBoxChange(Sender: TObject);
    procedure btGraphicsPrefsClick(Sender: TObject);
    procedure rgLevelsDisplayClick(Sender: TObject);
    procedure setLevelsDisplayVisible(const val : boolean);
    procedure btShowHideLevDispClick(Sender: TObject);
    procedure udZDisplayIndClick(Sender: TObject; Button: TUDBtnType);
    procedure zIndButtonsClick(dir : integer);
    procedure btDisplayIndUpClick(Sender: TObject);
    procedure btDisplayIndDnClick(Sender: TObject);
    procedure cbProbeClick(Sender: TObject);
    procedure weightingSelectBoxClick(Sender: TObject);
    procedure btGraphLevelsToClipboardClick(Sender: TObject);
    procedure btDuplicateProbeClick(Sender: TObject);
    procedure btDistancesToSourcesClick(Sender: TObject);
    procedure edMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edNameChange(Sender: TObject);
    procedure tsTimeSeriesShow(Sender: TObject);
  private
    updateFromCode          : boolean;
    modified                : boolean;
    bLevelsDisplayVisible   : boolean;
    iProbeIndex             : integer;

    procedure didChangeScenario;
    procedure didChangeUnits;
    function  getDepthLevelArray(const aPos: TPos3): TDepthLevelArray;
  public
    //bLastActionWasMoveProbe : boolean;
  end;

var
    framProbe               : TEditProbeFrm;

implementation

{$R *.dfm}
{$B-}

uses Main, EditFishFrame;

constructor TEditProbeFrm.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        updateFromCode        := true;

        {setting this true then false, forces the function to fire and hide
        all the stuff we want invisible}
        bLevelsDisplayVisible := true;
        setLevelsDisplayVisible(false);

        cbProbeInit();
        cbProbe.ItemIndex := 0;
        iProbeIndex := 0;
        pcCharts.ActivePageIndex := 0;

        //bLastActionWasMoveProbe := false;
        updateFromCode        := false;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditProbeFrm.didChangeScenario;
begin
    updateFromCode := true;
    cbProbeInit;
    if cbProbe.items.count > 0 then
        cbProbe.itemIndex := 0;

    weightingSelectBox.ItemIndex := framFish.cbWeightingSelect.ItemIndex;
    updateFromCode := false;
end;

procedure TEditProbeFrm.didChangeUnits;
begin
    EditBoxesInit;
end;

procedure TEditProbeFrm.weightingSelectBoxChange(Sender: TObject);
var
    ind : integer;
begin
    {when the box selection changes, check if the index is valid, then set
    the box on the framEditFish page and call the onchange event,
    which will then fire all the necessary stuff}
    if not updateFromCode then
    begin
        ind := self.weightingSelectBox.ItemIndex;
        if checkRange(ind,0,framFish.cbWeightingSelect.Items.Count - 1) then
        begin
            framFish.cbWeightingSelect.ItemIndex := ind;
            framFish.cbWeightingSelectChange(self);
        end;
        updateProbeChartAndTable();
        //bLastActionWasMoveProbe := false;
    end;
end;

procedure TEditProbeFrm.weightingSelectBoxClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox();
end;

procedure TEditProbeFrm.frameshow(Sender: TObject);
var
    i : integer;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        updateFromCode := false;
        EditBoxesInit();

        {initialise the weightingSelectBox by copying all the items from the
        FishPage selectbox. note we just copy the names, there is no data lying behind
        the combobox, it is all kept on the framEditFish frame combobox}
        if assigned(framFish) then
        begin
            weightingSelectBox.Clear();
            for i := 0 to framFish.cbWeightingSelect.Items.Count - 1 do
                weightingSelectBox.AddItem(framFish.cbWeightingSelect.Items[i],nil);
            updateFromCode := true;
            weightingSelectBox.ItemIndex := framFish.cbWeightingSelect.ItemIndex;
            updateFromCode := false;
        end;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditProbeFrm.framehide(Sender: TObject);
var
    prb : TProbe;
begin
    AskToSaveChanges();

    {set all probes to original color}
    for prb in container.scenario.theRA.probes do
        if prb.setAltColor(false) then
            if assigned(GLForm) then
                GLForm.setRedraw();
end;

procedure TEditProbeFrm.AskToSaveChanges;
//var
//    name            : String;
begin
    {if we have modified unsaved data, ask if we want to save it}
    if not modified then Exit;

//    iProbeIndex := cbProbe.ItemIndex;
//    if checkrange(iProbeIndex,0,container.scenario.theRA.probes.Count - 1) then
//        name := container.scenario.theRA.probes[iProbeIndex].Name
//    else
//        name := '';
//
//    if MessageDlg('Keep changes to probe: ' + name + '?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    btOKClick(self);
end;

procedure TEditProbeFrm.EditBoxesChange(Sender: TObject);
begin
    setModified(true);
end;

procedure TEditProbeFrm.cbProbeChange(Sender: TObject);
var
    //ind : integer;
    prb : TProbe;
begin
    AskToSaveChanges();
    iProbeIndex := cbProbe.ItemIndex;

    updateProbeChartAndTable();
    EditBoxesInit();
    //bLastActionWasMoveProbe := false;
    if assigned(GLForm) then
    begin
        {set the 'Probe n' GL text}
        //iProbeIndex := cbProbe.ItemIndex;
        if checkRange(iProbeIndex,0,container.scenario.theRA.probes.Count - 1) then
        begin
            prb := container.scenario.theRA.probes[iProbeIndex];
            GLForm.setSourceProbeText(prb.Name,prb.Pos.X,prb.Pos.Y);
        end;
        GLForm.force2DrawNow();
    end;
end;

procedure TEditProbeFrm.cbProbeClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox();
end;

procedure TEditProbeFrm.cbProbeInit;
var
    prb : TProbe;
begin
    if not assigned(container) then exit;

    cbProbe.items.clear();
    cbProbe.DoubleBuffered := true;
    cbProbe.AutoComplete := true;

    if container.scenario.theRA.probes.Count < 1 then
    begin
        cbProbe.ItemIndex := -1;
        cbProbe.Text := '';
    end
    else
    begin
        for prb in container.scenario.theRA.probes do
            cbProbe.AddItem(prb.Name, nil);
    end;
end;

procedure TEditProbeFrm.rgLevelsDisplayClick(Sender: TObject);
var
    tmpDisplay : TLevelsDisplay;
begin
    case rgLevelsDisplay.ItemIndex of
        0 : tmpDisplay := ldMax;
        1 : tmpDisplay := ldSingleLayer;
        2 : tmpDisplay := ldAllLayers;
    else
        tmpDisplay := ldMax;
    end;

    if tmpDisplay <> UWAOpts.LevelsDisplay then
    begin
        updateFromCode := true;

        UWAOpts.setLevelsDisplay(tmpDisplay, container.scenario.dMax); {setLevelsDisplay also updates the cmax cmin and levelcontours}
        if assigned(GLForm) then
        begin
            GLForm.setRedrawResults();
            GLForm.force2Drawnow();
        end;
        EditBoxesInit();

        updateFromCode := false;
    end;
end;

procedure TEditProbeFrm.sgProbeLevelsDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
    {change the font/colour for certain cells in the grid}
    with TStringGrid(Sender) do
    begin
        {heading column in bold}
        //if (ACol = 1) then
        //    Canvas.Font.Style := Canvas.Font.Style + [fsBold];

        {weighting row in mid blue}
        if (ARow = 2) then
            Canvas.Font.Color := TDBSeaColours.midBlue;

        {$IFDEF VER280}
        if DefaultDrawing then
            Rect.Left := Rect.Left - 4;
        Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+4, Cells[ACol, ARow]);
        {$ELSE}
        Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
        {$ENDIF}
    end;
end;

procedure TEditProbeFrm.tsTimeSeriesShow(Sender: TObject);
var
    ts: TProbeTimeSeries;
    tsp: TProbeTimeSeriesPoint;
    prb: TProbe;
begin
    if container.scenario.canShowProbeTimeSeries then
    begin
        lbTimeSeries.Visible := false;
        chTimeSeries.Visible := true;
        srTimeSeries.XValues.DateTime := true;

        srTimeSeries.Clear;
        if not checkRange(cbProbe.ItemIndex,0,container.scenario.theRA.probes.Count - 1) then exit;
        prb := container.scenario.theRA.probes[cbProbe.ItemIndex];
        ts := container.scenario.theRA.probeTimeSeries(prb.pos);
        for tsp in ts do
            if not isnan(tsp.level) then
                srTimeSeries.AddXY(tsp.time, tsp.level);
    end
    else
    begin
        lbTimeSeries.Visible := true;
        chTimeSeries.Visible := false;
    end;
end;

procedure TEditProbeFrm.setModified(const val : boolean);
begin
    if modified <> val then
    begin
        modified := val;
        setButtonsEnabled(modified);
    end;
end;

procedure TEditProbeFrm.setButtonsEnabled(const val : boolean);
begin
    self.btOK.Enabled := val;
    self.btCancel.Enabled := val;
end;

procedure TEditProbeFrm.udZDisplayIndClick(Sender: TObject; Button: TUDBtnType);
begin
    if not updateFromCode then
    begin
        if Button = btNext then
            UWAOpts.setDisplayZInd(UWAOpts.DisplayZInd + 1, container.scenario.dMax)
        else
            UWAOpts.setDisplayZInd(UWAOpts.DisplayZInd - 1, container.scenario.dMax);

        EditBoxesInit();
        //bLastActionWasMoveProbe := false;
        application.ProcessMessages();
        if assigned(GLForm) then
        begin
            GLForm.setRedrawResults;
            GLForm.drawnow();
        end;
        application.ProcessMessages();
    end;
end;

procedure TEditProbeFrm.zIndButtonsClick(dir : integer);
begin
    UWAOpts.setDisplayZInd(UWAOpts.DisplayZInd + dir, container.scenario.dMax);
    EditBoxesInit();
    //bLastActionWasMoveProbe := false;
    if assigned(GLForm) then
    begin
        GLForm.setRedrawResults;
        GLForm.drawnow();
    end;
end;

procedure TEditProbeFrm.updateProbeChartAndTable;
var
    fi, startfi, lenfi, i : integer;
    prb             : TProbe;
    probeSpect, weightedSpect   : ISmart<TSpectrum>;
    bw              : TBandwidth;
    prbLev,fishCurveLev,weightedLev : double;
    sFreq           : string;
    theLevs         : array[0..2] of double;
    bWeightingAllNaNs       : boolean;
begin
    startfi := container.scenario.startfi;
    bw := container.scenario.bandwidth;
    lenfi := container.scenario.lenfi;
    weightedSpect := TSmart<TSpectrum>.create(TSpectrum.create());

    sgProbeLevels.ColCount := lenfi + 2;//extra column for labels and total level
    sgProbeLevels.clear;

    sgProbeLevels.Cells[0,0] := 'Freq';
    sgProbeLevels.Cells[0,1] := 'Level';
    sgProbeLevels.Cells[0,2] := 'Weighting';
    sgProbeLevels.Cells[1,2] := container.scenario.weighting.Name;
    sgProbeLevels.Cells[0,3] := 'dB';
    {set freq text along header row}
    for fi := 0 to lenfi - 1 do
        sgProbeLevels.Cells[fi + 2,0] := TSpectrum.getFStringbyInd(fi + startfi,bw);

    srSpectrum.Clear();
    srWeighting.Clear();

    //container.scenario.theRA.updateProbes;
    if not checkRange(cbProbe.ItemIndex,0,container.scenario.theRA.probes.Count - 1) then exit;
    prb := container.scenario.theRA.probes[cbProbe.ItemIndex];
    prb.visible := true;
    updateDepthChart(prb.Pos);

    probeSpect := TSmart<TSpectrum>.create(TSpectrum.create);
    //get the spectrum at the probe location
    container.scenario.theRA.getSpectAtXY(probeSpect,prb.Pos.x,prb.pos.y,UWAOpts.LevelsDisplay,saferound(prb.Pos.Z / container.scenario.dz()),true);

    if isnan(probeSpect.Level()) or (probeSpect.Level() < lowLev) then
        sgProbeLevels.Cells[1,1] := '-'
    else
        sgProbeLevels.Cells[1,1] := tostrWithTestAndRound1(container.scenario.convertLevel(probeSpect.Level()));

    {plot the spectrum of the selected probe in the given RA}
    srSpectrum.ColorEachPoint := true;
    bWeightingAllNaNs := true;  //used for checking whether we have weighting at these freqs, or if they are all NaN
    for fi := 0 to lenfi - 1 do
    begin
        sFreq  := TSpectrum.getFStringbyInd(fi + startfi,bw);
        prbLev := probeSpect.getAmpbyInd(fi + startfi,bw);
        fishCurveLev  := container.scenario.weighting.Spectrum.getMitigationAmpbyInd(fi + startfi,bw);
        if not isnan(fishCurveLev) then
            bWeightingAllNaNs := false;
        if isNaN(prbLev) or isNaN(fishCurveLev) or (prbLev < fishCurveLev) then
            weightedLev := NaN
        else
            weightedLev := prbLev - fishCurveLev;
        {put the weighted level into a spectrum so we can get total level later}
        weightedSpect.setAmpbyInd(fi + startfi,weightedLev,bw);

        theLevs[0] := container.scenario.convertLevel( prbLev );
        theLevs[1] := fishCurveLev;
        theLevs[2] := container.scenario.convertLevel( weightedLev );

        {update chart and stringgrid}
        if IsNan(theLevs[0]) or (theLevs[0] < lowLev) then
            srSpectrum.AddXY(fi,0,sFreq,TDBSeaColours.darkBlue)
        else
            srSpectrum.AddXY(fi,theLevs[0],sFreq,TDBSeaColours.darkBlue());
        srWeighting.AddXY(fi,theLevs[1],sFreq);

        for i := 0 to 2 do
        begin
            {stringgrid}
            if isNaN(theLevs[i]) or (theLevs[i] < lowLev) then
                sgProbeLevels.Cells[fi + 2,i + 1] := '-'
            else
                sgProbeLevels.Cells[fi + 2,i + 1] := tostrWithTestAndRound1(theLevs[i]);
        end;
    end;
    lblLevelAboveThreshold.Visible := bWeightingAllNaNs;

    chSpectrum.Axes.Bottom.Increment := 1;
    //chrtProbe.Axes.Bottom.Items.Clear;
    //chrtProbe.Axes.Left.Minimum := 0;//chrtProbe.axes.Left.Maximum - 80;

    {we now have total weighted level after going through all the spectrum freqs}
    if isNaN(weightedSpect.Level) then
    begin
        lblLevelAboveThreshold.Caption := 'Weighted level : N/A';
        sgProbeLevels.Cells[1,3] := '-';
    end
    else
    begin
        lblLevelAboveThreshold.Caption := 'Weighted level : ' + tostr(round1(container.scenario.convertLevel(weightedSpect.Level()))) + ' dB';
        sgProbeLevels.Cells[1,3] := tostr(round1(container.scenario.convertLevel(weightedSpect.Level())));
    end;
end;

function TEditProbeFrm.getDepthLevelArray(const aPos: TPos3): TDepthLevelArray;
var
    fi, startfi, lenfi, i : integer;
    weightedSpect   : ISmart<TSpectrum>;
    depthSpect      : ISmart<TSpectrum>;
    bw              : TBandwidth;
    prbLev,fishCurveLev : double;
    dDepth          : double;
begin
    if not (container.scenario.resultsExist()) then exit;

    setlength(result,container.scenario.dmax);
    startfi := container.scenario.startfi;
    bw := container.scenario.bandwidth;
    lenfi := container.scenario.lenfi;

    weightedSpect := TSmart<TSpectrum>.create(TSpectrum.Create());
    depthSpect := TSmart<TSpectrum>.create(TSpectrum.Create());

    dDepth := container.getBathymetryAtPos(aPos.X,aPos.Y);//get the depth at that point
    for i := 0 to container.scenario.dMax - 1 do
    begin
        weightedSpect.clear();
        container.scenario.theRA.getSpectAtXY(depthSpect,aPos.x,aPos.y,ldSingleLayer,i,true);

        for fi := 0 to lenfi - 1 do
        begin
            prbLev := depthSpect.getAmpbyInd(fi + startfi,bw);
            fishCurveLev := container.scenario.weighting.Spectrum.getMitigationAmpbyInd(fi + startfi,bw);
            if (not isNaN(prbLev)) and (not isNaN(fishCurveLev)) and (prbLev > fishCurveLev) then
                weightedSpect.setAmpbyInd(fi + startfi,prbLev - fishCurveLev,bw); {put the weighted lev into a spectrum so we can get total level later}
        end;

        result[i].depth := container.scenario.depths(i);
        result[i].level := container.scenario.convertLevel(weightedSpect.Level()); //get the level from the spectrum
        result[i].aboveSediment := isnan(dDepth) or (result[i].depth < dDepth);
    end;
end;

procedure TEditProbeFrm.updateDepthChart(const aPos: TPos3);
const
    showLevelsInSediment = false;
var
    i               : integer;
    depthLevels     : TDepthLevelArray;
begin
    srWaterLevels.Clear();
    srSeabedLevels.Clear();
    if not (container.scenario.resultsExist()) then exit;

    depthLevels := self.getDepthLevelArray(aPos);
    chDepthLevels.Axes.Bottom.Title.caption := 'Level (dB)'; {TODO update to show 'dBht, dB M' or whatever}
    for i := 0 to container.scenario.dMax - 1 do
    begin
        {only add to chart if it is not a NaN. This could cause problems if there were NaNs interspersed between data, but NaN is normally only at the top and bottom so this is ok for most cases}
        if not isNaN(depthLevels[i].level) then
        begin
            if showLevelsInSediment then
            begin
                if depthLevels[i].aboveSediment then
                    srWaterLevels.AddXY(depthLevels[i].level,container.scenario.depths(i))
                else
                    srSeabedLevels.AddXY(depthLevels[i].level,container.scenario.depths(i));
            end
            else if depthLevels[i].aboveSediment then
                srWaterLevels.AddXY(depthLevels[i].level,container.scenario.depths(i));
        end;
    end;
end;


procedure TEditProbeFrm.EditBoxesInit;
var
    prb         : TProbe;
    sUnits      : String;
    dDepth      : double;
begin
    if not assigned(container) then exit;

    updateFromCode := true;

    if container.scenario.theRA.probes.Count < 1 then
        container.scenario.theRA.addProbe();

    {set all probes to original color}
    for prb in container.scenario.theRA.probes do
        if prb.setAltColor(false) then
            if assigned(GLForm) then
                GLForm.setRedraw();

    if not checkRange(cbProbe.ItemIndex,0,container.scenario.theRA.probes.Count - 1) then exit;

    prb := container.scenario.theRA.probes[cbProbe.ItemIndex];
    cbProbe.Text := cbProbe.Items[cbProbe.ItemIndex];
    {set this probe to alternate color}
    //prb.setColor(prb.altcolor);

    {enable/disable components}
    btProbeRemove.Enabled       := container.scenario.theRA.probes.Count > 1;
    btDataToClipboard.Enabled   := container.scenario.resultsExist();

    {if we are on single layer view ldSingleLayer, then show the depth. not needed when on ldMax}
    edz.Enabled  := UWAOpts.LevelsDisplay <> ldMax;
    lblZ.Enabled := UWAOpts.LevelsDisplay <> ldMax;

    {change the units string in the labels, if necessary}
    sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');

    lblX.Caption := 'x ' + sUnits;
    lblY.Caption := 'y ' + sUnits;
    lblZ.Caption := 'z ' + sUnits;

    dDepth := container.getBathymetryAtPos(prb.Pos.X,prb.Pos.Y);
    if isnan(dDepth) then
        lblDepthUnderProbe.Caption := 'Water depth at probe location: n/a'
    else
        lblDepthUnderProbe.Caption := 'Water depth at probe location: ' + container.geoSetup.makeZString(dDepth) + ' ' + sUnits;

    {wipe text in the total level label, so if there is no solution there
    is no text there. if there is a solution, the text gets written to the
    label from another function}
    lblLevelAboveThreshold.Caption := '';

    setLevelsDisplayVisible(self.bLevelsDisplayVisible);

    case UWAOpts.LevelsDisplay of
        ldMax           : rgLevelsDisplay.ItemIndex := 0;
        ldSingleLayer   : rgLevelsDisplay.ItemIndex := 1;
        ldAllLayers     : rgLevelsDisplay.ItemIndex := 2;
    end;

    edName.Text := prb.Name;
    edx.Text    := container.geoSetup.makeXString(prb.Pos.x);
    edy.Text    := container.geoSetup.makeYString(prb.Pos.Y);
    if UWAOpts.LevelsDisplay = ldMax then
    begin
        edz.Text  := 'Maximum';
        edz.ReadOnly := true;
        btDisplayIndUp.Enabled      := false;
        btDisplayIndDn.Enabled      := false;
        lbDepth.Enabled             := false;
        lbLayerNumber.Enabled       := false;
    end
    else
    begin
        edz.Text                    := container.geoSetup.makeZString(prb.Pos.Z);
        edz.ReadOnly                := false;
        updateFromCode              := true;
        btDisplayIndUp.Enabled      := true;
        btDisplayIndDn.Enabled      := true;
        lbDepth.Enabled             := true;
        lbLayerNumber.Enabled       := true;
        lbLayerNumber.Caption       := 'Display solution depth # ' + inttostr(UWAOpts.DisplayZInd + 1);
        {get the current z display index so we can say what depth is being displayed}
        lbDepth.Caption             := ('Depth ' + container.geoSetup.makeZString(container.scenario.depths(UWAOpts.DisplayZInd)) + ' ' + sUnits);
    end;

    updateProbeChartAndTable();
    updateFromCode := false;
    setModified(false);
end;

procedure TEditProbeFrm.edMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if (Button = mbRight) and (sender is TEdit) and (latLongEntryForm.setEditsAndShowModal(edx,edy) = mrOK) then
        btOkClick(sender);
end;

procedure TEditProbeFrm.edNameChange(Sender: TObject);
var
    oldItemIndex: integer;
begin
    if String(edName.Text).isEmpty then exit;

    container.scenario.theRA.probes[iProbeIndex].name := edName.Text;
    if (iProbeIndex < cbProbe.items.count) then
    begin
        oldItemIndex := cbProbe.ItemIndex;
        cbProbe.Items[iProbeIndex] := edName.Text;
        cbProbe.ItemIndex := oldItemIndex;
        if (cbProbe.ItemIndex = iProbeIndex) or (cbProbe.ItemIndex = -1) then
            cbProbe.Text := edName.Text;
    end;
end;

procedure TEditProbeFrm.btDataToClipboardClick(Sender: TObject);
var
    s : string;
    sZInfo : string;
    prb : TProbe;
    sUnits : string;
    ind : integer;
begin
    {export a string to the clipboard, containing info on the current probe position,
    and the levels and freqs.}

    sUnits := ifthen(UWAOpts.dispFeet,'(ft)','(m)');

    ind := cbProbe.ItemIndex;
    forceRange(ind,0,container.scenario.theRA.probes.Count - 1);
    prb := container.scenario.theRA.probes[ind];

    sZInfo := ifthen(UWAOpts.LevelsDisplay=ldMax,'z = N/A','z = ' + container.geoSetup.makeZString(prb.pos.z) + sUnits);
    s := prb.Name + #13#10 + 'x = ' + container.geoSetup.makeXString(prb.Pos.x) + sUnits + #13#10 +
            'y = ' + container.geoSetup.makeYString(prb.Pos.Y) + sUnits + #13#10 + sZInfo + #13#10;

    Clipboard.AsText := s + sgProbeLevels.myToString;
end;

procedure TEditProbeFrm.btGraphicsPrefsClick(Sender: TObject);
begin
    MainForm.ShowOptionsForm(TOptionsForm.iGraphicsSizesTab);
end;

procedure TEditProbeFrm.btGraphLevelsToClipboardClick(Sender: TObject);
    function formatCell(const val : double) : string;
    begin
        if isnan(val) then
            result := '-'
        else
            result := tostrWithTestAndRound1(val);
    end;
const
    delim = #09;
var
    s : string;
    depthLevels : TDepthLevelArray;
    i : integer;
begin
    if not container.scenario.resultsExist then exit;
    if not checkRange(cbProbe.ItemIndex,0,container.scenario.theRA.probes.Count - 1) then exit;

    s := ifthen(UWAOpts.dispFeet,'Depth (ft)','Depth (m)') + delim + 'Level (dB)' + #13#10;
    depthLevels := self.getDepthLevelArray(container.scenario.theRA.probes[cbProbe.ItemIndex].Pos);
    for i := 0 to length(depthLevels) - 1 do
        s := s + formatCell(depthLevels[i].depth) + delim +
                 formatCell(ifthen(depthLevels[i].aboveSediment,depthLevels[i].level,nan)) + #13#10;

    Clipboard.AsText := s;
end;

procedure TEditProbeFrm.btProbeAddClick(Sender: TObject);
var
    prb : TProbe;
begin
    AskToSaveChanges();

    prb := container.scenario.theRA.addProbe();
    cbProbeInit();
    cbProbe.ItemIndex := container.scenario.theRA.probes.Count - 1;
    cbProbeChange(self);
    EditBoxesInit();
    if assigned(GLForm) then
    begin
        GLForm.setSourceProbeText(prb.Name,prb.Pos.X,prb.Pos.Y);
        GLForm.resetView(); //we want birdseye view after adding a source
        GLForm.force2DrawNow();
    end;
end;

procedure TEditProbeFrm.btCancelClick(Sender: TObject);
begin
    setModified(false);
    EditBoxesInit();
end;

procedure TEditProbeFrm.btProbecolorPickClick(Sender: TObject);
var
    ColorDialog: ISmart<TColorDialog>;
    iPrb: integer;
begin
    iPrb := cbProbe.ItemIndex;
    if not checkRange(iPrb,0,container.scenario.theRA.probes.Count - 1) then Exit;

    AskToSaveChanges();

    ColorDialog := TSmart<TColorDialog>.create(TColorDialog.Create(Self));
    Colordialog.CustomColors.CommaText := UWAOpts.customColors;
    if colordialog.execute() and
        container.scenario.theRA.probes[iPrb].setColor(colordialog.color) and
        assigned(GLForm) then
        GLForm.setRedraw();

    UWAOpts.customColors := colordialog.CustomColors.CommaText;
    //bLastActionWasMoveProbe := false;
end;

procedure TEditProbeFrm.btOkClick(Sender: TObject);
var
    x,y,z   : double;
    //RA : TReceiverArea;
    prb : TProbe;
    //ind : integer;
begin
    {get the new x and y values from the text boxes and check they are within the bounds of the RA}
    if not isFloat(edx.Text) or
       not isFloat(edy.Text) then exit;

    if not assigned(UWAOpts) then exit;

    if (UWAOpts.LevelsDisplay <> ldMax) and not isFloat(edz.Text) then exit;

    if not checkRange(iProbeIndex,0,container.scenario.theRA.probes.Count - 1) then exit;

    prb := container.scenario.theRA.probes[iProbeIndex];

    x := container.geoSetup.getXfromString(edx.Text); //f2m(tofl(edx.Text),UWAOpts.dispFeet) - container.geoSetup.Easting;
    y := container.geoSetup.getYfromString(edY.Text); //f2m(tofl(edy.Text),UWAOpts.dispFeet) - container.geoSetup.Northing;

    forceRange(x,container.scenario.theRA.Pos1.X,container.scenario.theRA.Pos2.X);
    forceRange(y,container.scenario.theRA.Pos1.Y,container.scenario.theRA.Pos2.Y);

    if (UWAOpts.LevelsDisplay <> ldMax) then
    begin
        z := container.geoSetup.getZFromString(edz.Text);
        forceRange(z,0,container.bathymetry.zMax);
    end
    else
        z := prb.Pos.Z;

    if prb.setPos(x,y,z) then
        if assigned(GLForm) then
            GLForm.setRedraw();

    lastAction := TLastAction.kMoveProbe;
    prb.Name := edName.Text;
    cbProbeInit();
    cbProbe.ItemIndex := iProbeIndex;
    EditBoxesInit();
    setModified(false);

    if assigned(GLForm) then
        GLForm.forceDrawNow();
end;

procedure TEditProbeFrm.btProbeRemoveClick(Sender: TObject);
var
    ind : integer;
begin
    ind := cbProbe.ItemIndex;
    if container.scenario.theRA.probes.Count > 1 then
    begin
        if checkrange(ind,0,container.scenario.theRA.probes.Count - 1) then
            container.scenario.theRA.deleteProbe(ind)
    end
    else
        Showmessage('At least one probe must exist');

    ind := ind - 1;
    forceRange(ind,0,container.scenario.theRA.probes.Count - 1);
    cbProbeInit();
    cbProbe.ItemIndex := ind;
    EditBoxesInit();
    //bLastActionWasMoveProbe := false;
    if assigned(GLForm) then
    begin
        GLForm.clearSourceProbeText();
        GLForm.forceDrawNow();
    end;
end;

procedure TEditProbeFrm.btShowHideLevDispClick(Sender: TObject);
begin
    updateFromCode := true;
    setLevelsDisplayVisible(not bLevelsDisplayVisible);
    updateFromCode := false;
end;

procedure TEditProbeFrm.btDisplayIndUpClick(Sender: TObject);
begin
    zIndButtonsClick(1);
end;

procedure TEditProbeFrm.btDistancesToSourcesClick(Sender: TObject);
    function myToStr(const d: double): string;
    begin
        if d < 1000 then
            result := tostr(d) + ' m'
        else
            result := tostr(d/1000) + ' km';
    end;
var
    s: String;
    src: TSource;
    prb: TProbe;
begin
    prb := container.scenario.theRA.probes[cbProbe.ItemIndex];

    s := 'Distance from probe ' + prb.Name + ' to:' + #13#10;
    for src in container.scenario.sources do
        if UWAOpts.dispFeet then
            s := s + src.name + ': ' + tostr(saferound(f2m(src.pos.distanceToTPos3(prb.Pos),UWAOpts.dispFeet))) +' ft' + #13#10
        else
            s := s + src.name + ': ' + myToStr(safeRound(src.pos.distanceToTPos3(prb.Pos))) + #13#10;
    ShowMessage(s);
end;

procedure TEditProbeFrm.btDuplicateProbeClick(Sender: TObject);
var
    ind : integer;
    prb, newPrb : TProbe;
begin
    if container.isSolving then Exit;

    AskToSaveChanges;

    ind := self.cbProbe.ItemIndex;
    if not checkRange(ind,0,container.scenario.theRA.probes.Count - 1) then exit;
    prb := container.scenario.theRA.probes[ind];

    newPrb := container.scenario.theRA.addProbe;
    newPrb.cloneFrom(prb);
    newPrb.Name := newPrb.Name + ' copy'; //append 'copy' to the source name

    self.cbProbeInit;
    cbProbe.ItemIndex := container.scenario.theRA.probes.Count - 1;
    self.cbProbeChange(self);
    EditBoxesInit;
    //bLastActionWasMoveSource := false;
    if assigned(GLForm) then
    begin
        {set the 'Source n' GL text}
        GLForm.setSourceProbeText(newPrb.name,newPrb.Pos.X,newPrb.Pos.Y);
        GLForm.bDrawCrossSection := false; //we want birdseye view after adding a source
        GLForm.force2Drawnow;
    end;
end;

procedure TEditProbeFrm.btDisplayIndDnClick(Sender: TObject);
begin
    zIndButtonsClick(-1);
end;

procedure TEditProbeFrm.setLevelsDisplayVisible(const val : boolean);
begin
    if bLevelsDisplayVisible <> val then
    begin
         bLevelsDisplayVisible      := val;
         rgLevelsDisplay.Visible    := bLevelsDisplayVisible;
         lbLayerNumber.Visible      := bLevelsDisplayVisible;
         lbDepth.Visible            := bLevelsDisplayVisible;
         btDisplayIndUp.Visible     := bLevelsDisplayVisible;
         btDisplayIndDn.Visible     := bLevelsDisplayVisible;
         //edDisplayZInd.Visible      := bLevelsDisplayVisible;
         //udZDisplayInd.Visible      := bLevelsDisplayVisible;
         if bLevelsDisplayVisible then
            btShowHideLevDisp.Caption := 'Hide depth controls'
         else
            btShowHideLevDisp.Caption := 'Show depth controls';
    end;
end;


end.
