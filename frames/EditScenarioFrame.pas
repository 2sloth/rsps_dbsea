unit EditScenarioFrame;

interface

uses
    SynCommons,
    system.SysUtils, system.Variants, system.Classes,
  system.math, system.strutils, system.UITypes,
  winapi.Windows, winapi.Messages,
  vcl.Graphics, vcl.Controls, vcl.Forms,
  vcl.Dialogs, vcl.imaging.pngimage, vcl.ExtCtrls, vcl.StdCtrls, vcl.Grids,
  scenario, helperFunctions,
  dBSeaColours, dBSeaconstants, scenarioSummary,
  soundSource, baseTypes, eventLogging, GLWindow,
  problemContainer, MyFrameInterface, HighOrder, StringGridHelper,
  SmartPointer;

type

  TScenarioFrame = class(TFrame, IMyFrame)
    imPageTitle: TImage;
    btAddScenario: TButton;
    sgScenarios: TStringGrid; //will be replaced with TMyStringGrid
    btDuplicateScenario: TButton;
    btDeleteScenario: TButton;
    btDeleteCurrentScenarioLevels: TButton;
    lblScenarioName: TLabel;
    edScenarioName: TEdit;
    lblHeading: TLabel;
    lblWorking: TLabel;
    lbComments: TLabel;
    mmComments: TMemo;
    btSummary: TButton;
    sgCurrentScenarioInfo: TStringGrid;

    constructor Create(AOwner: TComponent); override;
    procedure frameshow(Sender: TObject);
    procedure framehide(Sender: TObject);
    procedure EditBoxesInit;

    procedure btAddScenarioClick(Sender: TObject);
    procedure btDuplicateScenarioClick(Sender: TObject);
    procedure btDeleteScenarioClick(Sender: TObject);

    procedure sgScenariosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btDeleteCurrentScenarioLevelsClick(Sender: TObject);
    procedure edScenarioNameChange(Sender: TObject);
    procedure btSummaryClick(Sender: TObject);
    procedure sgScenariosMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sgScenariosMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure sgScenariosMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure mmCommentsChange(Sender: TObject);

  private
  const
    iIndexCol       : integer = 0;
    iActiveCol      : integer = 1;
    iNameCol        : integer = 2;
    iSolveCol       : integer = 3;
    iResultsCol     : integer = 4;
  var
    updateFromCode  : boolean;
    modified        : boolean;

    procedure setModified(const val : boolean);
    procedure setButtonsEnabled(const val : boolean);
    procedure scenariosSummariesToStringGrid(const sg : TStringGrid; const scenarios : TScenarioList);
    procedure didChangeUnits;
    procedure didChangeScenario;
  public
  end;

var
    framScenario            : TScenarioFrame;

implementation

{$R *.dfm}

uses Main;


constructor TScenarioFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        updateFromCode        := true;
        modified := false;
        updateFromCode        := false;
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TScenarioFrame.didChangeScenario;
begin
    //
end;

procedure TScenarioFrame.didChangeUnits;
begin
    EditBoxesInit;
end;

procedure TScenarioFrame.framehide(Sender: TObject);
begin
    //
end;

procedure TScenarioFrame.frameshow(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'frameshow');
    try
        updateFromCode := false;

        EditBoxesInit();

        sgScenarios.ColWidths[iIndexCol] := 20;
        sgScenarios.ColWidths[iActiveCol] := 60;
        sgScenarios.ColWidths[iNameCol] := 200;
        sgScenarios.ColWidths[iSolveCol] := 50;
        sgScenarios.ColWidths[iResultsCol] := 44;
    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TScenarioFrame.mmCommentsChange(Sender: TObject);
begin
    container.scenario.comment := mmComments.Text;
end;

procedure TScenarioFrame.EditBoxesInit;
var
    scen: TScenario;
    ARow: Integer;
    thisScenario: ISmart<TScenarioList>;
begin
    updateFromCode := true;

    edScenarioName.Text := container.scenario.Name;
    mmComments.Text := container.scenario.comment;
    //cbScenarioSolve.Checked := container.scenario.solveEnabled;
    thisScenario := TSmart<TScenarioList>.create(TScenarioList.create(false));
    thisScenario.add(container.scenario);
    self.scenariosSummariesToStringGrid(sgCurrentScenarioInfo,thisScenario);
    self.sgCurrentScenarioInfo.Cells[1,0] := 'Active scenario summary';

    //update the scenarios combobox
    sgScenarios.clear;
    sgScenarios.RowCount := container.scenarios.count + 1;
    sgScenarios.Cells[iIndexCol,0]      := '';
    sgScenarios.Cells[iActiveCol,0]     := 'Activate';
    sgScenarios.Cells[iNameCol,0]       := 'Name';
    sgScenarios.Cells[iSolveCol,0]      := 'Solve';
    sgScenarios.Cells[iResultsCol,0]    := 'Levels';
    for scen in container.scenarios do
    begin
        aRow := container.scenarios.IndexOf(scen) + 1;
        sgScenarios.Cells[iIndexCol,aRow]   := tostr(container.scenarios.IndexOf(scen) + 1);
        sgScenarios.Cells[iActiveCol,aRow]  := ifthen(scen = container.scenario,'  ' + sTickMark,'');
        sgScenarios.Cells[iNameCol,aRow]    := scen.Name;
        sgScenarios.Cells[iSolveCol,aRow]   := ifthen(scen.solveEnabled,'  ' + sTickMark,'');
        sgScenarios.Cells[iResultsCol,aRow] := ifthen(scen.resultsExist,'  ' + sTickMark,'');
    end;

    lblWorking.Visible := false;
    updateFromCode := false;
    setModified(false);
end;


procedure TScenarioFrame.edScenarioNameChange(Sender: TObject);
begin
    if string(edScenarioName.Text).IsEmpty or updateFromCode then exit;

    container.scenario.name := edScenarioName.Text;
    Mainform.updateMainCaption;
    EditBoxesInit;
end;

procedure TScenarioFrame.btAddScenarioClick(Sender: TObject);
begin
    lblWorking.Visible := true;
    application.ProcessMessages;
    container.addScenario;
    mainform.didChangeScenario;
    editBoxesInit;
    if assigned(GLForm) then
        GLForm.force2DrawNow();
end;

procedure TScenarioFrame.btDeleteCurrentScenarioLevelsClick(Sender: TObject);
var
    buttonSelected: Integer;
begin
    if container.isSolving then Exit;

    buttonSelected := MessageDlg('Really remove calculated levels from scenario : ' + container.scenario.Name + '?',
                                 mtConfirmation,
                                 mbOKCancel,
                                 0);

    if buttonSelected = mrOK then
    begin
        container.scenario.resetSourcesandRAs;

        if assigned(GLForm) then
            GLForm.setRedrawResults;

        eventLog.log('Remove calculated levels from scenario ' + container.scenario.name);
        editBoxesInit;
    end;
end;

procedure TScenarioFrame.btDeleteScenarioClick(Sender: TObject);
var
  buttonSelected : Integer;
begin
    if container.isSolving then Exit;

    if container.scenarios.Count <= 1 then
    begin
        Showmessage('At least one scenario must exist');
        Exit;
    end;

    buttonSelected := MessageDlg('Really delete scenario : ' + container.scenario.Name + '?',
                                 mtConfirmation,
                                 mbOKCancel,
                                 0);

    if buttonSelected = mrOK then
    begin
        container.deleteCurrentScenario;
        mainform.didChangeScenario;
        if assigned(GLForm) then
        begin
            GLForm.setRedrawResults;
            GLForm.force2DrawNow();
        end;
    end;
    editBoxesInit;
end;

procedure TScenarioFrame.btDuplicateScenarioClick(Sender: TObject);
begin
    lblWorking.Visible := true;
    application.ProcessMessages;
    container.duplicateCurrentScenario;
    mainform.didChangeScenario;
    editBoxesInit;
end;

procedure TScenarioFrame.scenariosSummariesToStringGrid(const sg: TStringGrid; const scenarios: TScenarioList);
type
    TStringListWithWrappedStringObjects = TStringList;
    TStringListWithStringListObjects = TStringList;

    procedure populateStringGridFromDict(const sg: TStringgrid; const orderedKeys: TStringListWithStringListObjects);
    var
        i,j : integer;
        sl : TStringList;
    begin
        sg.RowCount := orderedKeys.Count + 1;
        if orderedKeys.Count = 0 then exit;
        sg.ColCount := TStringList(orderedKeys.Objects[0]).Count + 1;

        for j := 0 to orderedKeys.Count - 1 do
        begin
            sg.Cells[0,j + 1] := orderedKeys[j];
            sl := TStringlist(orderedKeys.Objects[j]);
            for i := 0 to sl.Count - 1 do
                sg.Cells[i + 1,j + 1] := sl[i];
        end;

        sg.DefaultColWidth := 200;
        sg.ColWidths[0] := 100;
    end;
const
    cDelim : char = ',';
var
    scenarioKeysAndValues: ISmart<TStringListWithWrappedStringObjects>;
    allScenariosKeysAndValues: ISmart<TStringListWithStringListObjects>;
    j: Integer;
    scenario: TScenario;
    maxSourceCount, maxProbeCount: Integer;
begin
    if scenarios.Count = 0 then exit;

    allScenariosKeysAndValues := TSmart<TStringListWithStringListObjects>.create(TStringListWithStringListObjects.Create(true));
    maxSourceCount := T_.reduce<TScenario,integer>(scenarios, function(memo: integer; scenario: TScenario):integer
    begin
        result := max(memo, scenario.sources.count);
    end, 0);

    maxProbeCount := T_.reduce<TScenario,integer>(scenarios, function(memo: integer; scenario: TScenario):integer
    begin
        result := max(memo, scenario.theRa.probes.count);
    end, 0);

    for scenario in scenarios do
    begin
        scenarioKeysAndValues := TSmart<TStringListWithWrappedStringObjects>.create(scenario.printInfo(cDelim, maxSourceCount, maxProbeCount));
        for j := 0 to scenarioKeysAndValues.Count - 1 do
        begin
            if scenarios.IndexOf(scenario) = 0 then
                allScenariosKeysAndValues.AddObject(scenarioKeysAndValues[j],TStringlist.Create);
            TStringListWithStringListObjects(allScenariosKeysAndValues.Objects[j]).Add(TWrappedString(scenarioKeysAndValues.Objects[j]).str);
        end;
    end;

    populateStringGridFromDict(sg,allScenariosKeysAndValues);
end;

procedure TScenarioFrame.btSummaryClick(Sender: TObject);
begin
    if container.scenarios.Count = 0 then exit;

    self.scenariosSummariesToStringGrid(frmScenarioSummary.sgSummary,container.scenarios);
    frmScenarioSummary.ShowModal;
end;

procedure TScenarioFrame.setModified(const val : boolean);
begin
    if modified <> val then
    begin
        modified := val;
        setButtonsEnabled(modified);
    end;
end;

procedure TScenarioFrame.sgScenariosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
    {change the font/colour for certain cells in the grid}
    with TStringGrid(Sender) do
    begin
        {header row with background}
        if (ARow = 0) then
            Canvas.Brush.Color := TDBSeaColours.headerRow
        else
        begin
            if aCol in [iActiveCol,iSolveCol,iResultsCol] then
                Canvas.Font.Color := TDBSeaColours.midBlue;
        end;

        {$IFDEF VER280}
        if DefaultDrawing then
            Rect.Left := Rect.Left - 4;
        Canvas.TextRect(Rect, Rect.Left+6, Rect.Top+5, Cells[ACol, ARow]);
        {$ELSE}
        Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
        {$ENDIF}
    end;
end;

procedure TScenarioFrame.sgScenariosMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    aCol, aRow: Integer;
    scen : TScenario;
    ind : integer;
begin
    sgScenarios.MouseToCell(X, Y, aCol, aRow);
    if (button = mbLeft) and (aRow > 0) then
    begin
        ind := aRow - 1;

        if aCol = iActiveCol then
        begin
            if ind < container.scenarios.Count then
            begin
                //it can take some time to perform the scenario change, so disable the frame while we do it
                self.Enabled := false;
                lblWorking.Visible := true;
                application.ProcessMessages;
                try
                    //activate the selected scenario
                    container.currentScenario := ind;
                    if assigned(GLForm) then
                    begin
                        GLForm.setRedrawResults;
                        GLForm.force2DrawNow();
                    end;
                    mainform.didChangeScenario;

                finally
                    self.Enabled := true;
                    lblWorking.Visible := false;
                end;
            end;
        end;

        if (aCol = iSolveCol) then
        begin
            if ind < container.scenarios.Count then
            begin
                //activate or deactivate this scenario solve
                scen := container.scenarios[ind];
                scen.solveEnabled := not scen.solveEnabled;
            end;
        end;

        EditBoxesInit;
    end;
end;

procedure TScenarioFrame.sgScenariosMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    handled := not PtInRect(sgScenarios.ClientRect, sgScenarios.ScreenToClient(Mouse.CursorPos)); //ignore mousewheel outside the component
end;

procedure TScenarioFrame.sgScenariosMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    handled := not PtInRect(sgScenarios.ClientRect, sgScenarios.ScreenToClient(Mouse.CursorPos)); //ignore mousewheel outside the component
end;

procedure TScenarioFrame.setButtonsEnabled(const val : boolean);
begin
    //self.btOK.Enabled := val;
    //self.btCancel.Enabled := val;
end;

end.
