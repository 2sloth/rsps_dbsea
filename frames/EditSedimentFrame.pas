{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

unit EditSedimentFrame;

interface

uses
    syncommons,
  system.SysUtils, system.Variants, system.Classes, system.Math, system.StrUtils, system.UITypes,
  vcl.Graphics, vcl.Controls, vcl.Forms, Vcl.Imaging.pngimage,
  vcl.Dialogs, vcl.StdCtrls, vcl.ExtCtrls,
  winapi.Windows, winapi.Messages,
  Generics.Collections,
  baseTypes,
  SQLiteTable3, SedimentAddDB,
  helperFunctions, material, dBSeaconstants, scenario, seafloorScheme,
  dBSeaOptions, eventLogging, globalsUnit,
  problemContainer, MyFrameInterface, SmartPointer;

type
  TEditSedimentFrame = class(TFrame, IMyFrame)
    edC: TEdit;
    edRho: TEdit;
    edAttenuation: TEdit;
    lblC: TLabel;
    lblDensity: TLabel;
    lblAttenuation: TLabel;
    btOk: TButton;
    btCancel: TButton;
    cbSediment: TComboBox;
    edName: TEdit;
    lbMaterialName: TLabel;
    mmComments: TMemo;
    lblComments: TLabel;
    imPageTitle: TImage;
    lblPageTitle: TLabel;
    btRemoveFromDB: TButton;
    btSaveToDB: TButton;
    lblDataSource: TLabel;
    gbSediment: TGroupBox;
    pnMaterial: TPanel;
    cbLayers: TComboBox;
    btAddLayer: TButton;
    btRemoveLayer: TButton;
    lbDepth: TLabel;
    edDepth: TEdit;
    cbRefToSeafloor: TCheckBox;
    lbLayerName: TLabel;
    edLayerName: TEdit;
    lbThickness: TLabel;
    cbLocationProps: TComboBox;

    constructor Create(AOwner: TComponent); override;

    procedure frameshow(Sender: TObject);
    procedure framehide(Sender: TObject);
    procedure editBoxesInit;
    procedure clearSedimentsBoxObjects;

    procedure cbLayersInit;
    procedure cbLayersChange(Sender: TObject);

    procedure cbSedimentInit;
    procedure cbSedimentChange(Sender: TObject);

    procedure btCancelClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure EditBoxesChange(Sender: TObject);
    procedure setModified(const val : boolean);
    procedure setButtonsEnabled(const val : boolean);
    procedure btSaveToDBClick(Sender: TObject);
    procedure btRemoveFromDBClick(Sender: TObject);

    procedure btAddLayerClick(Sender: TObject);
    procedure btRemoveLayerClick(Sender: TObject);
    procedure cbRefToSeafloorClick(Sender: TObject);
    procedure cbLayersClick(Sender: TObject);
    procedure cbSedimentClick(Sender: TObject);
    procedure edNameChange(Sender: TObject);
    procedure mmCommentsChange(Sender: TObject);
    procedure edLayerNameChange(Sender: TObject);
    procedure edDepthChange(Sender: TObject);
    procedure cbLocationPropsChange(Sender: TObject);
    procedure cbLocationPropsClick(Sender: TObject);
  private
  var
    updateFromCode          : boolean;
    modified                : boolean;

    procedure didChangeScenario;
    procedure didChangeUnits;
  public
  end;

var
    framSediment            : TEditSedimentFrame;

implementation

{$R *.dfm}

uses Main;

constructor TEditSedimentFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        eventlog.log(log, 'Open sediment DB');
        if not MDASldb.TableExists(sedimentTblName) then
            raise Exception.Create('Error: Database contains no table ' + sedimentTblName);

        updateFromCode      := true;
        //fleditc             := Tfloatedit.create(self,edC);
        //fleditrho           := Tfloatedit.create(self,edRho);
        //fleditattenuation   := Tfloatedit.create(self,edAttenuation);

        btRemoveFromDB.Enabled := false;

        cbSedimentInit;
        cbSediment.ItemIndex := 0;
        cbSedimentChange(nil);

        updateFromCode := false;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditSedimentFrame.didChangeScenario;
begin
    frameShow(nil);
    cbSedimentInit;
end;

procedure TEditSedimentFrame.didChangeUnits;
var
    tmp: integer;
begin
    updateFromCode := true;
    tmp := cbLayers.ItemIndex;
    cbLayersInit;
    cbLayers.ItemIndex := tmp;
    updateFromCode := false;
    editboxesInit;
end;

procedure TEditSedimentFrame.edDepthChange(Sender: TObject);
begin
    if (self.cbLayers.ItemIndex > 0) and isFloat(edDepth.Text) then
    begin
        container.scenario.propsMap.selected.seabed.setLayerDepth(cbLayers.ItemIndex,container.geoSetup.getZfromString(edDepth.Text));
        cbLayersInit;
    end;
end;

procedure TEditSedimentFrame.clearSedimentsBoxObjects;
var
    i: integer;
begin
    for I := 0 to cbSediment.Items.Count - 1 do
        TMaterial(cbSediment.Items.Objects[i]).Free;
end;


procedure TEditSedimentFrame.frameshow(Sender: TObject);
var
    prevItemIndex: integer;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'frameshow');
    try
        updateFromCode := true;

        container.scenario.propsMap.initComboBox(self.cbLocationProps);
        {$IF defined (DEBUG) or defined (VER2)}
        self.cbLocationProps.Visible := container.scenario.propsMap.count > 1;
        {$ELSE}
        self.cbLocationProps.Visible := false;
        {$ENDIF}

        prevItemIndex := cbLayers.ItemIndex;
        cbLayersInit;
        if (prevItemIndex >= 0) and (prevItemIndex < container.scenario.propsMap.selected.seabed.layers.count) then
            cbLayers.itemIndex := prevItemIndex;
        EditBoxesInit;
        updateFromCode := false;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditSedimentFrame.mmCommentsChange(Sender: TObject);
var
    scheme: TSeafloorScheme;
begin
    scheme := container.scenario.propsMap.selected.seabed;
    if (cbLayers.ItemIndex < 0) or (cbLayers.ItemIndex >= scheme.layers.count) then exit;
    scheme.layers[cbLayers.ItemIndex].material.comments := mmComments.Text;
end;

procedure TEditSedimentFrame.framehide(Sender: TObject);
begin
    btOkClick(self);
end;

procedure TEditSedimentFrame.btAddLayerClick(Sender: TObject);
begin
    eventLog.log('Add sediment layer');
    container.scenario.propsMap.selected.seabed.addLayer;
    cbLayersInit;
    cbLayers.ItemIndex := cbLayers.Items.Count - 1;
    editboxesinit;
end;

procedure TEditSedimentFrame.btCancelClick(Sender: TObject);
begin
    updateFromCode := true;
    EditBoxesInit;
    setModified(false);
    updateFromCode := false;
end;

procedure TEditSedimentFrame.btSaveToDBClick(Sender: TObject);
var
    i           : integer;
    sSQL        : String;
    iNewID      : integer;
begin
    {get user data back from the form and do an sql insert}

    {checking returned values}
    if edName.Text = '' then
    begin
        showmessage('Name field cannot be blank');
        Exit;
    end;

    if not(isFloat(edC.Text) and
            isFloat(edRho.Text) and
            isFloat(edAttenuation.Text)) then
    begin
        showmessage('Given values could not be parsed, aborting');
        Exit;
    end;

    if tofl(edC.Text) <= 0 then
    begin
        showmessage('Speed of sound must be greater than 0');
        Exit;
    end;

    if tofl(edRho.Text) <= 0 then
    begin
        showmessage('Density must be greater than 0');
        Exit;
    end;

    {confirm any changes we have made to the material}
    self.btOkClick(nil);

    {get current table IDs so we can add an ID integer. The ID will be higher than
    all the other IDs}
    iNewID := 1;
    for i := 0 to cbSediment.Items.Count - 1 do
        if TMaterial(cbSediment.Items.Objects[i]).DataSource = TDataSource.dsUser then
            if TMaterial(cbSediment.Items.Objects[i]).ID >= iNewID then
                iNewID := TMaterial(cbSediment.Items.Objects[i]).ID + 1;

    {we could also check that the current name is not taken by any others in the db. but we
    can also let there be entries with the same name by doing nothing.}

    {create the sql string, and execute. we are adding the user data into the user database. note that some fields
    are numeric.}
    eventLog.log('Add sediment properties to database');
    userSldb.BeginTransaction; //begin a transaction
    //NB this inserts the data as 'User', not 'MDA'
    sSQL := 'INSERT INTO ' + sedimentTblName + ' (ID,Datasource,Name,Comments,SoundSpeed,Density,Attenuation';
    sSQL := sSQL + ') VALUES (' + tostr(iNewID) + ',"' + sUserDB + '","' + edName.Text + '"';
    sSQL := sSQL + ', "' + mmComments.Text + '",' + forceDotSeparator(edC.Text);
    sSQL := sSQL + ',' + forceDotSeparator(edRho.Text) + ',' + forceDotSeparator(edAttenuation.Text) + ');';
    userSldb.ExecSQLUnicode(sSQL);     //do the insert
    userSldb.Commit;   //end the transaction

    cbSedimentInit;
    cbSediment.ItemIndex := cbSediment.Items.Count - 1;
    cbSedimentChange(nil);
    EditBoxesInit;
end;

procedure TEditSedimentFrame.cbLayersChange(Sender: TObject);
begin
    if updateFromCode then exit;
    if self.cbLayers.ItemIndex >= container.scenario.propsMap.selected.seabed.layers.Count then exit;

    editboxesInit;
end;

procedure TEditSedimentFrame.cbLayersClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox;
end;

procedure TEditSedimentFrame.cbRefToSeafloorClick(Sender: TObject);
begin
    container.scenario.propsMap.selected.seabed.isReferencedFromSeafloor := self.cbRefToSeafloor.Checked;
end;

procedure TEditSedimentFrame.btOkClick(Sender: TObject);
var
    material: TMaterial;
    tmpInd: integer;
begin
    if container.isSolving or not modified then Exit;
    if self.cbLayers.ItemIndex >= container.scenario.propsMap.selected.seabed.layers.Count then exit;

    material := container.scenario.propsMap.selected.seabed.layers[cbLayers.ItemIndex].material;
    {check if we are in feet or meters, and apply conversions if in feet}
    if isFloat(edC.Text) and isFloat(edRho.Text) and isFloat(edAttenuation.Text) then
    begin
        if UWAOpts.dispFeet then
            material.setProps(f2m(tofl(edC.Text),UWAOpts.dispFeet),tofl(edRho.Text) * 16.02,tofl(edAttenuation.Text))
        else
            material.setProps(tofl(edC.Text),tofl(edRho.Text),tofl(edAttenuation.Text));
    end;

    updateFromCode := true;
    try
        tmpInd := cbLayers.ItemIndex;
        cbLayersInit;
        cbLayers.ItemIndex := tmpInd;
        EditBoxesInit;
    finally
        setModified(false);
        updateFromCode := false;
    end;
end;

procedure TEditSedimentFrame.btRemoveFromDBClick(Sender: TObject);
var
    buttonSelected : Integer;
    sSQL        : String;
    ind : integer;
begin
    ind := cbSediment.ItemIndex;

    if not (TMaterial(cbSediment.Items.Objects[ind]).DataSource = TDataSource.dsUser) then Exit;

    {ask the user if they really want to delete the selected item}
    buttonSelected := MessageDlg('Really delete item ' + TMaterial(cbSediment.Items.Objects[ind]).Name + '?',
                                 mtConfirmation ,
                                 mbOKCancel,
                                 0);

    {if they do, then do the SQL Delete}
    if buttonSelected = mrOK then
    begin
        eventLog.log('Remove sediment properties from database');
        userSldb.BeginTransaction; //begin a transaction
        sSQL := 'DELETE FROM ' + sedimentTblName + ' WHERE ID=' + tostr(TMaterial(cbSediment.Items.Objects[ind]).ID) + ';';
        userSldb.ExecSQLUnicode(sSQL);     //do the delete
        userSldb.Commit;   //end the transaction

        {update the list of weightings}
        cbSedimentInit;
        cbSediment.ItemIndex := 0;
        cbSedimentChange(nil);
        EditBoxesInit;
    end;
end;

procedure TEditSedimentFrame.btRemoveLayerClick(Sender: TObject);
begin
    if cbLayers.ItemIndex = 0 then exit;

    if container.scenario.propsMap.selected.seabed.layers.count < 2 then
    begin
        showmessage('At least 1 layer must exist');
        exit;
    end;

    container.scenario.propsMap.selected.seabed.deleteLayer(cbLayers.ItemIndex);
    cbLayersInit;
    cbLayers.ItemIndex := 0;
    eventLog.log('Remove sediment layer');
    editboxesinit;
end;

procedure TEditSedimentFrame.cbSedimentChange(Sender: TObject);
var
    scheme: TSeafloorScheme;
    layer: TSeafloorLayer;
begin
    if container.isSolving then Exit;

    if not updateFromCode then
    begin
        if cbLayers.ItemIndex < 0 then exit;
        scheme := container.scenario.propsMap.selected.seabed;
        if cbLayers.ItemIndex >= scheme.layers.Count then exit;

        {having the material simply point to the SedimentSelectBox.Items.Objects could cause
        problems on reinitialising the combobox, so instead copy over the data for each field. need to keep this
        updated however.}
        layer := scheme.layers[cbLayers.ItemIndex];
        layer.material.cloneFrom(TMaterial(cbSediment.Items.Objects[cbSediment.ItemIndex]));
    end;
    EditBoxesInit;
end;

procedure TEditSedimentFrame.cbSedimentClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox;
end;

procedure TEditSedimentFrame.cbLayersInit;
var
    layer: TSeafloorLayer;
    s: string;
    scheme: TSeafloorScheme;
    oldItemIndex: integer;
begin
    oldItemIndex := cbLayers.ItemIndex;
    self.cbLayers.Clear;

    scheme := container.scenario.propsMap.selected.seabed;
    for layer in scheme.layers do
    begin
        s := '  ' + container.geoSetup.makeZString(layer.depth);
        if layer <> scheme.layers.last then
            s := s + ' to ' + container.geoSetup.makeZString(layer.depth + scheme.layerThickness(layer));
        s := s + ifthen(UWAOpts.DispFeet,' ft',' m');
        self.cbLayers.AddItem(layer.name + ', ' + layer.material.Name + s,nil);
    end;

    if (oldItemIndex >= 0) and (oldItemIndex < scheme.layers.count) then
        cbLayers.ItemIndex := oldItemIndex
    else if cbLayers.Items.count > 0 then
        cbLayers.ItemIndex := 0;
end;

procedure TEditSedimentFrame.cbLocationPropsChange(Sender: TObject);
begin
    if (cbLocationProps.ItemIndex < 0) or (cbLocationProps.ItemIndex >= container.scenario.propsMap.count) then exit;

    self.updateFromCode := true;

    container.scenario.propsMap.selected := container.scenario.propsMap[cbLocationProps.ItemIndex];
    cbLayersInit;
    editBoxesInit;

    self.updateFromCode := false;
end;

procedure TEditSedimentFrame.cbLocationPropsClick(Sender: TObject);
begin
    THelper.fixForVCLOnclickProblemWithTComboBox;
end;

procedure TEditSedimentFrame.cbSedimentInit;
    procedure addEntries(const sltb: TSQLIteTable);
    var
        id               : integer;
        c,rho,att           : double;
        name, comments      : string;
        sDatasource         : TDataSource;
        sediment: TMaterial;
    begin
        sltb.MoveFirst;
        while not sltb.EOF do
        begin
            name        := sltb.FieldAsString(sltb.FieldIndex['Name']);
            comments    := ifthen(sltb.FieldIsNull(sltb.FieldIndex['Comments']),'',sltb.FieldAsString(sltb.FieldIndex['Comments']));
            c           := sltb.FieldAsDouble(sltb.FieldIndex['SoundSpeed']);
            rho         := sltb.FieldAsDouble(sltb.FieldIndex['Density']);
            att         := sltb.FieldAsDouble(sltb.FieldIndex['Attenuation']);
            id          := sltb.FieldAsInteger(sltb.FieldIndex['ID']);
            if sltb.FieldAsString(sltb.FieldIndex['DataSource']) = sUserDB then
                sDatasource := TDataSource.dsUser
            else
                sDatasource := TDataSource.dsMDA;

            sediment := TMaterial.create(name,c,rho,att,ID,sDatasource);
            cbSediment.AddItem(name, sediment);
            sediment.Comments := comments;

            sltb.Next;
        end;

    end;
var
    sltbMDA, sltbUser: ISmart<TSQLIteTable>;
begin
    sltbMDA := TSmart<TSQLIteTable>.create(MDASldb.GetTable(ansistring('SELECT * FROM ' + sedimentTblName)));
    sltbUser := TSmart<TSQLIteTable>.create(userSlDb.GetTable(ansistring('SELECT * FROM ' + sedimentTblName)));

    updateFromCode := false;

    clearSedimentsBoxObjects;
    cbSediment.Clear;
    addEntries(sltbMDA);
    addEntries(sltbUser);
end;

procedure TEditSedimentFrame.editBoxesInit;
var
    material : TMaterial;
    layer : TSeafloorLayer;
    thickness: string;
begin
    self.updateFromCode := true;
    try
        if not assigned(container) then exit;

        if cbLayers.ItemIndex < 0 then exit;
        if cbLayers.ItemIndex >= container.scenario.propsMap.selected.seabed.layers.Count then exit;

        btRemoveLayer.Enabled := cbLayers.ItemIndex <> 0;
        edDepth.Enabled := cbLayers.ItemIndex <> 0;

        cbRefToSeafloor.Checked := container.scenario.propsMap.selected.seabed.isReferencedFromSeafloor;

        layer               := container.scenario.propsMap.selected.seabed.layers[cbLayers.ItemIndex]; //only supports a singleseafloorscheme currently
        edDepth.Text        := tostr(round1(m2f(layer.depth,UWAOpts.dispFeet)));
        edLayerName.Text    := layer.name;

        material            := layer.material;
        edName.Text         := material.Name;
        edC.Text            := tostr(saferound(m2f(material.C,UWAOpts.dispFeet)));
        if UWAOpts.dispFeet then
            edRho.Text          := tostr(round1(material.Rho * 0.0624)) //convert to lb/ft^3
        else
            edRho.Text          := tostr(material.Rho);
        edAttenuation.Text  := tostr(material.Attenuation);
        mmComments.Text       := material.comments;

        lbDepth.Caption := ifthen(UWAOpts.DispFeet,'Starting depth (ft)','Starting depth (m)');
        lblC.Caption := ifthen(UWAOpts.DispFeet,'c (ft/s)','c (m/s)');
        lblDensity.Caption := ifthen(UWAOpts.DispFeet,'Density (lb/ft�)','Density (kg/m�)');

        if cbLayers.ItemIndex = cbLayers.Items.Count - 1 then
            lbThickness.Caption := 'Thickness: infinity'
        else
        begin
            thickness := container.geoSetup.makeZString(container.scenario.propsMap.selected.seabed.layerThickness(cbLayers.ItemIndex));
            lbThickness.Caption := ifthen(UWAOpts.DispFeet,'Thickness: ' + thickness + ' ft','Thickness: ' + thickness + ' m');
        end;


        {set datasource label, enable 'remove' button if dsuser}
        lblDataSource.Caption := ifthen(material.DataSource = TDataSource.dsUser,
                                            'Data source: user defined','Data source: default library');
        btRemoveFromDB.Enabled := material.DataSource = TDataSource.dsUser;
    finally
        updateFromCode := false;
        setModified(false);
    end;
end;

procedure TEditSedimentFrame.edLayerNameChange(Sender: TObject);
var
    scheme: TSeafloorScheme;
begin
    if updateFromCode or String(edLayerName.Text).isEmpty then exit;
    scheme := container.scenario.propsMap.selected.seabed;
    if (cbLayers.ItemIndex < 0) or (cbLayers.ItemIndex >= scheme.layers.count) then exit;
    scheme.layers[cbLayers.ItemIndex].name := edLayerName.Text;
    cbLayersInit;
end;

procedure TEditSedimentFrame.edNameChange(Sender: TObject);
var
    scheme: TSeafloorScheme;
begin
    if String(edName.Text).isEmpty then exit;
    scheme := container.scenario.propsMap.selected.seabed;
    if (cbLayers.ItemIndex < 0) or (cbLayers.ItemIndex >= scheme.layers.count) then exit;
    scheme.layers[cbLayers.ItemIndex].material.name := edName.Text;
    cbLayersInit;
end;

procedure TEditSedimentFrame.EditBoxesChange(Sender: TObject);
begin
    if not updateFromCode then
        setModified(true);
end;

procedure TEditSedimentFrame.setModified(const val : boolean);
begin
    if modified <> val then
    begin
        modified := val;
        setButtonsEnabled(modified);
    end;
end;

procedure TEditSedimentFrame.setButtonsEnabled(const val : boolean);
begin
    self.btOk.Enabled := val;
    self.btCancel.Enabled := val;

    if modified then
        self.btRemoveFromDB.Enabled := false;
end;


end.
