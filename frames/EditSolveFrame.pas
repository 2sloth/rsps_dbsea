{*******************************************************}
{                                                       }
{       dBSea Underwater Acoustics                      }
{                                                       }
{       Copyright (c) 2013 2014 dBSea       }
{                                                       }
{*******************************************************}

{this page started out having info on the ReceiverAreas.
however now we only have the 1 and it covers the whole area,
so now the page has info during the solve, and the cancel solve button}

unit EditSolveFrame;

interface

uses
    SynCommons,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, generics.Collections, ComCtrls, MMSystem,
  helperFunctions, math, solveTimeEstimator,
  strutils, TaskbarProgress, problemContainer, soundSource,
  scenario, MyFrameInterface, globalsunit, eventLogging;

type
  TEditSolveFrame = class(TFrame, IMyFrame)
    lbSolving: TLabel;
    pbSolveProgress: TProgressBar;
    btCancelSolve: TButton;
    mmSolverMessages: TMemo;
    lblSolveCancelling: TLabel;
    lbEstimatedLowSolveTime: TLabel;
    lbSolveExtraInfo: TLabel;
    lbEstimatedHighSolveTime: TLabel;
    lbSliceNumber: TLabel;
    lbScenarioSolveInfo: TLabel;
    mmSolverErrors: TMemo;
    lbSolveSourcePosition: TLabel;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure frameshow(Sender: TObject);
    procedure framehide(Sender: TObject);

    procedure btCancelSolveClick(Sender: TObject);
  private
  const
    updateProgressBarsTimeoutMS : integer = 300;
  var
    cLastStepProgressBars : cardinal;
    updateFromCode  : boolean;

    procedure didChangeScenario;
    procedure didChangeUnits;
  public
    SolveTimeEstimator : TSolveTimeEstimator;
    taskbarProgress : TTaskbarProgress;

    procedure UpdateRemainingSolveTime(const useLowFreqSolverText, splitSolver : boolean);
    procedure initSolveUI(const scen: TScenario);
    procedure initFlockUI(const aMax : integer);
    procedure stepProgressBars(const delta : integer);
    procedure setProgressBars(const fractionDone: double);
    procedure endFlockUI;
    procedure endSolveUI;
  end;

var
   framSolve               : TEditSolveFrame;


implementation

{$R *.dfm}

uses Main;

constructor TEditSolveFrame.Create(AOwner: TComponent);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'Create');
    try
        inherited Create(AOwner);

        SolveTimeEstimator := TSolveTimeEstimator.create;

        lbScenarioSolveInfo.caption := '';
        taskbarProgress := nil;
        cLastStepProgressBars := timeGetTime;

        lbSolveExtraInfo.Caption := '';
        lbScenarioSolveInfo.Caption := '';
        lbEstimatedHighSolveTime.Caption := '';
        lbEstimatedLowSolveTime.Caption := '';

        //solveMessages := mmSolverMessages;

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

destructor TEditSolveFrame.Destroy;
begin
    SolveTimeEstimator.Free;
    taskbarProgress.Free;

    inherited;
end;

procedure TEditSolveFrame.didChangeScenario;
begin
    //
end;

procedure TEditSolveFrame.didChangeUnits;
begin
    //
end;

procedure TEditSolveFrame.frameshow(Sender: TObject);
var
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self, 'frameshow');
    try
        updateFromCode := false;

        mmSolverErrors.Visible := false;
        mmSolverErrors.Text := '';

    except on e: Exception do
        eventLog.logException(log, e.ToString);
    end;
end;

procedure TEditSolveFrame.framehide(Sender: TObject);
begin
    updateFromCode := false;
end;

procedure TEditSolveFrame.btCancelSolveClick(Sender: TObject);
begin
    container.CancelSolve.b := true;
    TSource.killExternalSolvers();
    lblSolveCancelling.Visible := true;
    application.ProcessMessages();
end;


procedure TEditSolveFrame.UpdateRemainingSolveTime(const useLowFreqSolverText, splitSolver : boolean);
var
    hrsStr,minsStr,secsStr : string;
    sScenario : string;
    fractionDone : double;
begin
    //NB if the solver is dBSeaRay, we have already taken account of the multi-freq solve

    self.solveTimeEstimator.UpdateRemainingSolveTime(useLowFreqSolverText, splitSolver, hrsStr,minsStr,secsStr,fractionDone);

    //SolveProgressPaintbox.setFractionDone(fractionDone);

    //TODO fix these estimates
    lbEstimatedLowSolveTime.visible := false;//true;
    lbEstimatedHighSolveTime.Visible := false;//splitSolver;
    sScenario := ifthen(container.scenarios.Count = 1,'Scenario/source','This scenario/source');
    if splitSolver then
    begin
        //split solver, update UI for the solver that is currently working.
        if useLowFreqSolverText then
        begin
            lbEstimatedLowSolveTime.Caption := sScenario + ' estimated low frequency solve time remaining: ' +
                                                hrsStr + minsStr + secsStr;
            lbEstimatedHighSolveTime.Caption := sScenario + ' estimated high frequency solve time remaining unknown';
        end
        else
        begin
            lbEstimatedLowSolveTime.Caption := sScenario + ' low frequency solve finished';
            lbEstimatedHighSolveTime.Caption := sScenario + ' estimated high frequency solve time remaining: ' +
                                                 hrsStr + minsStr + secsStr;;
        end;
    end
    else
    begin
        //not a split solver, just show the single message
        lbEstimatedLowSolveTime.Caption := sScenario + ' estimated solve time remaining: ' +
                                            hrsStr + minsStr + secsStr;
    end;
end;

procedure TEditSolveFrame.initFlockUI(const aMax : integer);
begin
    self.pbSolveProgress.Min := 0;
    self.pbSolveProgress.Position := 0;
    self.taskbarProgress := TTaskbarProgress.create(Application.Handle);
    self.taskbarProgress.Progress := 0;
    self.taskbarProgress.Visible := true;
    self.taskbarProgress.Max := aMax;
    self.pbSolveProgress.visible := true;
    self.pbSolveProgress.Max := aMax;
    self.btCancelSolve.Visible := true;
end;

procedure TEditSolveFrame.initSolveUI(const scen: TScenario);
begin
    self.SolveTimeEstimator.iLowTotSlicesTimesFreq := 0;
    self.SolveTimeEstimator.iHighTotSlicesTimesFreq := 0;
    self.SolveTimeEstimator.iCurrentLowSlicesTimesFreq := 0;
    self.SolveTimeEstimator.iCurrentHighSlicesTimesFreq := 0;
    self.pbSolveProgress.Min := 0;
    self.pbSolveProgress.Position := 0;
    self.taskbarProgress := TTaskbarProgress.create(Application.Handle);
    self.taskbarProgress.Progress := 0;
    self.taskbarProgress.Visible := true;
    self.taskbarProgress.Max := scen.sources.Count * scen.SourcesNSlices * scen.lenfi;
    self.pbSolveProgress.visible := true;
    self.pbSolveProgress.Max := 1000; //we set the position during a solve with fractionDone: double, so can just set the max to some large value
    self.btCancelSolve.Visible := true;
    self.lblSolveCancelling.Visible := false;
    self.lbSolving.visible := true;
    self.lbSolving.Caption := '';
    self.lbSliceNumber.Visible := true;
    self.lbSliceNumber.Caption := '';
    self.lbSolveSourcePosition.visible := true;
    self.lbSolveSourcePosition.Caption := '';
    self.lbSolveExtraInfo.Visible := true;
    self.lbSolveExtraInfo.Caption := '';
end;

procedure TEditSolveFrame.stepProgressBars(const delta:  integer);
begin
    self.pbSolveProgress.StepBy(delta);
    if self.pbSolveProgress.Position >= self.pbSolveProgress.Max then
        self.pbSolveProgress.Position := self.pbSolveProgress.Position - self.pbSolveProgress.Max;

    if assigned(self.taskbarProgress) then
    begin
        self.taskbarProgress.progress := self.taskbarProgress.Progress + delta;
        if self.taskbarProgress.Progress > self.taskbarProgress.Max then
            self.taskbarProgress.Progress := self.taskbarProgress.Progress - self.taskbarProgress.max;
    end;
end;

procedure TEditSolveFrame.setProgressBars(const fractionDone: double);
begin
    self.pbSolveProgress.Position := round(fractionDone*pbSolveProgress.Max);
    self.pbSolveProgress.Invalidate;
    if not assigned(self.taskbarProgress) then exit;
    self.taskbarProgress.progress := round(fractionDone*taskbarProgress.Max);
end;

procedure TEditSolveFrame.endFlockUI;
begin
    FreeAndNil(self.taskbarProgress);

    self.pbSolveProgress.Position := 0;
    self.pbSolveProgress.Visible := false;
    self.lbSolving.visible := false;
    self.btCancelSolve.Visible := false;
    self.lbEstimatedLowSolveTime.Visible := false;
    self.lbEstimatedHighSolveTime.Visible := false;
    self.lbSliceNumber.Visible := false;
    self.lbSolveSourcePosition.visible := false;
    self.lbSolveExtraInfo.Visible := false;
    self.lbSolveExtraInfo.Visible := false;
    self.mmSolverMessages.Visible := false;
end;

procedure TEditSolveFrame.endSolveUI;
begin
    FreeAndNil(self.taskbarProgress);

    self.pbSolveProgress.Position := 0;
    self.pbSolveProgress.Visible := false;
    self.lbSolving.visible := false;
    self.btCancelSolve.Visible := false;
    self.lbEstimatedLowSolveTime.Visible := false;
    self.lbEstimatedHighSolveTime.Visible := false;
    self.lbSliceNumber.Visible := false;
    self.lbSolveSourcePosition.visible := false;
    self.lbSolveExtraInfo.Visible := false;
    self.lbSolveExtraInfo.Visible := false;
    self.mmSolverMessages.Visible := false;
end;

end.
