object EditProbeFrm: TEditProbeFrm
  Left = 0
  Top = 0
  Width = 630
  Height = 920
  Constraints.MinHeight = 800
  TabOrder = 0
  object lblZ: TLabel
    Left = 52
    Top = 242
    Width = 5
    Height = 13
    Caption = 'z'
  end
  object lblY: TLabel
    Left = 52
    Top = 215
    Width = 6
    Height = 13
    Caption = 'y'
  end
  object lblX: TLabel
    Left = 52
    Top = 188
    Width = 6
    Height = 13
    Caption = 'x'
  end
  object lblName: TLabel
    Left = 53
    Top = 161
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblLevelAboveThreshold: TLabel
    Left = 48
    Top = 422
    Width = 125
    Height = 13
    Caption = 'Level above threshold'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object imPageTitle: TImage
    Left = 48
    Top = 18
    Width = 45
    Height = 45
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000002D0000
      002D08060000003A1AE29A00000006624B474400980098009815E12590000000
      097048597300000B1300000B1301009A9C180000000774494D4507DE03190C35
      03BC73DF52000008094944415478DAD5997B70546719C69FBD5F73DD5B36405A
      20439609625A4407A15024E4CEB4A3A21D296DC9D811F136E3D83FAC3868CB80
      D341FDA3338C56DA2234A688AD2221211A432AA114C2A55028960490DC77934D
      7693BD666FBEDFD9EC26BB49CED92A98F04ECE6CF69C3DE7FCBEE73CDFFBBDDF
      7744110A3C60214A860E854208040208CF81B6884522C86432482492E9A1D9C7
      D8D818FCB4CDB550C8E590D326A2462440FBFDFE39093C195CA1504C40334BB8
      3D9ED9E6120C8D5ACD598583F6F97C18231FCFF59093BF954A65149AA91C0C06
      679B4930A45229A7761C3AF00028CD32491CDAE57209DAE3FF91CE63D961A660
      F6D06AB551E851062D903974D9D9F71DDA3E34C40F4D19242D06ED1C19A194C7
      0F6D34E8EF19DC95F6DB387EFA2CB67FB90AFACC8CF87EDBC020EF790A851C19
      E9E951E86187033E9F9FF704738EE99E416FFAE14ED411B4D9A0C3EB3F7D01E5
      AB5672FBFBFAADBCE729950A646566A606CDDC7CC713C4EA45F3A63D7EB5C786
      7F59EDA8285C0C2DA9911CFE6008F5D73B50986B4247FB4D54567F1720C52865
      E1AB25EB71F417BB70EE6E1F1628C4E0737502F4D03083F6CD0C4DD4256FD4E1
      E99585787E4D11F41A15B7DFE9F5E337AD9771E25A0786DC5E584C7A7CA5A800
      55CBF39146F0C170182D373B71E8DC355CE9B1421309E16AE331B8BD3ED6EB30
      DFA8C7DBFBF6E074A715B5176EA0FEB90AF0F54596A3B3B352868EE04BBF3B0E
      B55CCAA9966FC8824A2625101BF7299E7427061A0C85B9828B6503A9580C9944
      CC5DE3CEB9D370F476C59598F7D9CF217B51012474BA672C88E6E737F1669044
      E8A161780594FEC2FE7791AE9071DF1990641C864109854824C670CF5DDC3EFB
      1EA730A8616926330A1E2F45984A08162E7F00FFF8E6265EA5550C3A3B2B3568
      163D94C6A37822F843415CEEB2E2D6C0305ADA3BC7C14433FA512416E1E23B35
      D1D6534864722C2B7D0212F984FFEF39347BB45BFFF41E34640FF67F3A758887
      B233B0D4ACC3F25C236CA36E743B4671B1B31F1FFCBB976AF230A492D81310E1
      D6D95370F6764755A6F3E717AD84717141C23DA2D055BCF648806649DDEBE5B7
      C786D7EB2833C8C677B0BF08B75F4A2A6A9572E4A46B50B52C1F1B2C0FE3B5D3
      1FE2ADB66B5CE3466DFD686F6D1E3F290275960E4B8B2B11218B24408F05D054
      5DC5AFB44AC90D7229424750FCC60968E5325E0BF92885E928B3FCA46C35D741
      F79C3C83A6238711F2FBE3AD2F7AF2298825D229E746A12BF9959E0C3D6867D0
      5E5EA537BE290C1D6B60301CC117F3F370ED5423CE9CBFC0CA33EED802CA1686
      7C4BDCDBC9D07FDF5629A0B40A7ADD7D808EC588B5171DEFB7C40155DA34588A
      E9F1CF906D3E3DF4A01D1E01E89283F5294347C221DC686E806FC419DD1108A2
      E6D57D3870B19D06A4E96DC8A0FF2630B8A819B45E9722346D256FA6062D262B
      745E3A07DBCDEBF445C2A6F79847D9A278FD3A7C6FED0A541F3E3129B324416F
      ABE01DC613A0070607E1F10829DD4043F3F4D06C04F48D3AA9C37911F0B8D1D9
      F67EDCC7AA8C2C1A444A20A2CEB763EDA3D45195D855D7CA0D4CC9D08DCF96F3
      2BAD56C1A0D7A7084DDBD78F9CA2FAC2433793404E9B441CBD3AF5396CB2E4A1
      EBE32BF8FDB106447C5E36DE8EB72600CBC62AA8B30D5CCBC7A804F879D563B8
      6377A2A6ED7A82AA1CF473E5FC4A4F866675AC476036EE536961A074769E068F
      7F7674E152573F065D3E48C6BCD85BBA12CB97E4232B3D0DBFAC398ABD076BE1
      7438612828C4C32B5621149C981585A895AF6D29C78F8FB5C0E199F037833EF9
      4C9980D26AAEAE4F099AD963ED813AE46668303F330D45F34D7834CF44969561
      DB8B3F83F5934F2049D3E2F38516ECFEF6362CCC3563C38E1FC11E1261C93AAA
      2F828953B98D9685584565EE0BEF36234D29BF7FD065874E267444968FCB9617
      E095DD2F714B69F1200BD5FC6A3716994DA878712FF2D797219434FF7413E0F1
      ED9BB1AFE9039CA73A9A55899F1EDA36C0BB58C3A0CB0F374EC91E21AADEBA2F
      9C81FD4E7B3453205A38456864BCFDD71A7CFFB77F408FC64037491CB25995C8
      9ED8CEF2D5D8F176635CE986ADA5BCD06C266E341A5284A6ADFCD05468768720
      75BCABC78EC4B3452C76566FC187110D7A1CAE698766D6297FBDB918AF9EBAC0
      155B1CF433A5BC1D3101DA6AB3C1EDE6877EF6CFAD18A529197B94E20408119C
      7D5DE8FEE832423E0F774CA45022A76019B2F316CE584B307B557E261F6EAAEE
      5879CB2C53FF7409BFD21A354C46636AD02C8EDFB563D5C25C1C3C7B15ADB7BA
      13F22C030B5251A44208BB2AD7E03BEFB4202B235D70AD64B13E139B572CC5CB
      F5AD54A38751BFB5845FE9C9D0FD5606ED9E596936B8903D18E69E271E47E7B0
      13B56D1F4FF91D9B6A7D6BCD235477CBF00A7532A5540ABE60BFDBFF5429361F
      F80B98EB4F6C2916505A831C53AAD0B455BED5449E9622408A7C6D85054B8C3A
      BC440A29A4890BDEAC9335FFE01BA8D8FF47CEB77C11A0AC535BFD24B6D79E84
      75D4434A6F14507A3274BF152E21E89AA678476430DB1F7B041FF50EA08D52D6
      E4F073C78A601D21089AA5F349C7A05FAE5A87A3976E704B080D02D05A069D63
      4A0D9A05AB3B6216651766053F9BDC264F6C63C7B8570F49AF1DA60B2FE57056
      16B0226AD4CFBF9E9800DDD7DF0F97CB2D7883D90EAD5603734ECE38745FBFA0
      D2732198D26673CE4495E77038679B493032333326AA3CF692E86E67D76C3309
      C643790BB89745F1B75B6C466EB70FFDAFD7BD6FA1A3B9616C8D3CE13D225BB4
      19B4DB679B6F4AE875BAE8224DF27BC45830AB389D2334170D42F45FDDE2DE04
      8392D1889A41E540ECFD612CA6403F08F11F2A9B83B65F906398000000004945
      4E44AE426082}
  end
  object lblPageTitle: TLabel
    Left = 110
    Top = 35
    Width = 87
    Height = 13
    Caption = 'Noise level probes'
  end
  object lblDepthUnderProbe: TLabel
    Left = 50
    Top = 273
    Width = 149
    Height = 13
    Caption = 'Water depth at probe location:'
  end
  object lblWeighting: TLabel
    Left = 234
    Top = 137
    Width = 78
    Height = 13
    Caption = 'Global weighting'
  end
  object lbLayerNumber: TLabel
    Left = 340
    Top = 851
    Width = 116
    Height = 13
    Caption = 'Display solution depth #'
    Visible = False
  end
  object lbDepth: TLabel
    Left = 340
    Top = 870
    Width = 29
    Height = 13
    Caption = 'Depth'
    Visible = False
  end
  object edZ: TEdit
    Left = 86
    Top = 239
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 0
    Text = '0.0'
    OnChange = EditBoxesChange
  end
  object edY: TEdit
    Left = 86
    Top = 212
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '0.0'
    OnChange = EditBoxesChange
    OnMouseDown = edMouseDown
  end
  object edX: TEdit
    Left = 86
    Top = 185
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '0.0'
    OnChange = EditBoxesChange
    OnMouseDown = edMouseDown
  end
  object btOK: TButton
    Left = 236
    Top = 237
    Width = 90
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 3
    OnClick = btOKClick
  end
  object btProbeAdd: TButton
    Left = 48
    Top = 69
    Width = 159
    Height = 25
    Caption = 'Add'
    TabOrder = 4
    OnClick = btProbeAddClick
  end
  object btProbeRemove: TButton
    Left = 235
    Top = 69
    Width = 158
    Height = 25
    Caption = 'Remove'
    TabOrder = 5
    OnClick = btProbeRemoveClick
  end
  object cbProbe: TComboBox
    Left = 48
    Top = 131
    Width = 158
    Height = 21
    DropDownCount = 100
    TabOrder = 6
    Text = 'cbProbe'
    OnChange = cbProbeChange
    OnClick = cbProbeClick
  end
  object edName: TEdit
    Left = 86
    Top = 158
    Width = 121
    Height = 21
    TabOrder = 7
    OnChange = edNameChange
  end
  object btProbeColorPick: TButton
    Left = 451
    Top = 237
    Width = 90
    Height = 25
    Caption = 'Colour'
    TabOrder = 8
    OnClick = btProbeColorPickClick
  end
  object btCancel: TButton
    Left = 344
    Top = 237
    Width = 90
    Height = 25
    Caption = 'Cancel'
    TabOrder = 9
    OnClick = btCancelClick
  end
  object sgProbeLevels: TStringGrid
    Left = 49
    Top = 292
    Width = 494
    Height = 121
    ColCount = 15
    DefaultColWidth = 60
    RowCount = 4
    ScrollBars = ssHorizontal
    TabOrder = 10
    OnDrawCell = sgProbeLevelsDrawCell
  end
  object btDataToClipboard: TButton
    Left = 428
    Top = 418
    Width = 115
    Height = 25
    Caption = 'Table to clipboard'
    TabOrder = 11
    OnClick = btDataToClipboardClick
  end
  object pcCharts: TPageControl
    Left = 49
    Top = 449
    Width = 496
    Height = 321
    ActivePage = tsSpectrum
    MultiLine = True
    TabOrder = 12
    TabPosition = tpRight
    object tsSpectrum: TTabSheet
      Caption = 'Spectrum'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object chSpectrum: TChart
        Left = 3
        Top = 3
        Width = 462
        Height = 307
        Legend.Visible = False
        MarginBottom = 0
        MarginTop = 0
        Title.Font.Color = clBlack
        Title.Text.Strings = (
          'Probe spectrum')
        BottomAxis.Grid.Visible = False
        DepthAxis.Automatic = False
        DepthAxis.AutomaticMaximum = False
        DepthAxis.AutomaticMinimum = False
        DepthAxis.Maximum = 0.740000000000022900
        DepthAxis.Minimum = -0.259999999999977400
        DepthTopAxis.Automatic = False
        DepthTopAxis.AutomaticMaximum = False
        DepthTopAxis.AutomaticMinimum = False
        DepthTopAxis.Maximum = 0.740000000000022900
        DepthTopAxis.Minimum = -0.259999999999977400
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Grid.Visible = False
        RightAxis.Automatic = False
        RightAxis.AutomaticMaximum = False
        RightAxis.AutomaticMinimum = False
        View3D = False
        Zoom.Allow = False
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object srSpectrum: TBarSeries
          Marks.Visible = False
          SeriesColor = 4470016
          Title = 'Series0'
          BarWidthPercent = 100
          Shadow.Color = 8618883
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
        end
        object srWeighting: TFastLineSeries
          SeriesColor = 10389298
          LinePen.Color = 10389298
          LinePen.Width = 3
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object tsVerticalLevel: TTabSheet
      Caption = 'Levels vs depth'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        468
        313)
      object chDepthLevels: TChart
        Left = 3
        Top = 3
        Width = 462
        Height = 278
        Legend.Visible = False
        MarginBottom = 0
        MarginTop = 0
        Title.Font.Color = clBlack
        Title.Text.Strings = (
          'Probe level vs depth')
        BottomAxis.Grid.Visible = False
        BottomAxis.Title.Caption = 'Level (dB)'
        DepthAxis.Automatic = False
        DepthAxis.AutomaticMaximum = False
        DepthAxis.AutomaticMinimum = False
        DepthAxis.Maximum = 0.740000000000022900
        DepthAxis.Minimum = -0.259999999999977400
        DepthTopAxis.Automatic = False
        DepthTopAxis.AutomaticMaximum = False
        DepthTopAxis.AutomaticMinimum = False
        DepthTopAxis.Maximum = 0.740000000000022900
        DepthTopAxis.Minimum = -0.259999999999977400
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Grid.Visible = False
        LeftAxis.Inverted = True
        LeftAxis.Title.Caption = 'Depth (m)'
        RightAxis.Automatic = False
        RightAxis.AutomaticMaximum = False
        RightAxis.AutomaticMinimum = False
        View3D = False
        Zoom.Allow = False
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object srWaterLevels: TLineSeries
          Brush.BackColor = clDefault
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loAscending
        end
        object srSeabedLevels: TLineSeries
          Brush.BackColor = clDefault
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loAscending
        end
      end
      object btGraphLevelsToClipboard: TButton
        Left = 327
        Top = 285
        Width = 130
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Levels to clipboard'
        TabOrder = 1
        OnClick = btGraphLevelsToClipboardClick
      end
    end
    object tsTimeSeries: TTabSheet
      Caption = 'Level vs time'
      Enabled = False
      ImageIndex = 2
      OnShow = tsTimeSeriesShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lbTimeSeries: TLabel
        Left = 180
        Top = 136
        Width = 117
        Height = 13
        Caption = 'Time series not available'
      end
      object chTimeSeries: TChart
        Left = 0
        Top = 0
        Width = 468
        Height = 313
        Legend.Visible = False
        MarginBottom = 0
        MarginTop = 0
        Title.Font.Color = clBlack
        Title.Text.Strings = (
          'Probe level vs time')
        BottomAxis.Grid.Visible = False
        BottomAxis.Title.Caption = 'Time (s)'
        DepthAxis.Automatic = False
        DepthAxis.AutomaticMaximum = False
        DepthAxis.AutomaticMinimum = False
        DepthAxis.Maximum = 0.740000000000022900
        DepthAxis.Minimum = -0.259999999999977400
        DepthTopAxis.Automatic = False
        DepthTopAxis.AutomaticMaximum = False
        DepthTopAxis.AutomaticMinimum = False
        DepthTopAxis.Maximum = 0.740000000000022900
        DepthTopAxis.Minimum = -0.259999999999977400
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Grid.Visible = False
        LeftAxis.Title.Caption = 'Level (dB)'
        RightAxis.Automatic = False
        RightAxis.AutomaticMaximum = False
        RightAxis.AutomaticMinimum = False
        View3D = False
        Zoom.Allow = False
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object srTimeSeries: TLineSeries
          Brush.BackColor = clDefault
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loAscending
        end
      end
    end
  end
  object weightingSelectBox: TComboBox
    Left = 235
    Top = 158
    Width = 306
    Height = 21
    DropDownCount = 100
    TabOrder = 13
    Text = 'weightingSelectBox'
    OnChange = weightingSelectBoxChange
    OnClick = weightingSelectBoxClick
  end
  object btGraphicsPrefs: TButton
    Left = 210
    Top = 783
    Width = 137
    Height = 25
    Caption = 'Display preferences'
    TabOrder = 14
    OnClick = btGraphicsPrefsClick
  end
  object rgLevelsDisplay: TRadioGroup
    Left = 45
    Top = 816
    Width = 276
    Height = 101
    Caption = 'Levels display'
    ItemIndex = 0
    Items.Strings = (
      'Max from all depths projected to surface'
      'Single depth'
      'All depths')
    TabOrder = 15
    Visible = False
    OnClick = rgLevelsDisplayClick
  end
  object btShowHideLevDisp: TButton
    Left = 49
    Top = 783
    Width = 138
    Height = 25
    Caption = 'Show depth controls'
    TabOrder = 16
    OnClick = btShowHideLevDispClick
  end
  object btDisplayIndUp: TButton
    Left = 516
    Top = 875
    Width = 20
    Height = 20
    Caption = '+'
    TabOrder = 17
    OnClick = btDisplayIndUpClick
  end
  object btDisplayIndDn: TButton
    Left = 516
    Top = 849
    Width = 20
    Height = 20
    Caption = '-'
    TabOrder = 18
    OnClick = btDisplayIndDnClick
  end
  object btDuplicateProbe: TButton
    Left = 48
    Top = 100
    Width = 159
    Height = 25
    Caption = 'Duplicate'
    TabOrder = 19
    OnClick = btDuplicateProbeClick
  end
  object btDistancesToSources: TButton
    Left = 235
    Top = 100
    Width = 158
    Height = 25
    Caption = 'Distances to sources'
    TabOrder = 20
    OnClick = btDistancesToSourcesClick
  end
end
