object FrequencyFrame: TFrequencyFrame
  Left = 0
  Top = 0
  Width = 630
  Height = 920
  TabOrder = 0
  object imPageTitle: TImage
    Left = 48
    Top = 18
    Width = 45
    Height = 45
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000002D0000
      002D0802000000B57875CD000000097048597300000B1300000B1301009A9C18
      0000000774494D4507DE0617100F2D780FAEB3000007BE4944415478DAB5987F
      5854551AC7E7DEF971E717308AC88CC30FB1CC6C295C4B5A35F5694BDA1EE559
      29B24D5625021E253584C945565B2528571215D4B684C6DD22412DB7D0D6B5AC
      27D3C7303456B14CAB350D180666068699FB73EEBDFB0E771886018C9D19DE3F
      E699B973CE3D9FF33DEF79DFF71C84E77951BFB9BFF9FC1C3B4310C4FF899783
      E338C6E5E258961FD22894C6F3F07689542A118B7D693C1CACCBE5248891FAB2
      2C0B9F62B138843C288A2A150AF81CE070310C4E9280E3A703F076F7F41CFFA2
      F16C730B349B3D233175DE83E3341A3E446B27914800C53316CC95204961C67E
      C671EC82CC758C04130902F23CC6319FD6EC0AA13072B95C26957A389C4EA7DF
      04410987C3F170D60B885A8389D8B9BF9ACAF1FC99966B2E54CA3BBA4FD5EC0A
      0F0B0B892A088AAA954A180E718167E0B8DF4B198631ECD8F7D5F7B75096D9B7
      216F56D2BDD0A0E9D29555DBAA7829F69BBBE2B717AC964A24219104A6E4E180
      A9FB5280EF7CF9F5E59CBFEE552914CFFFFED1E54B16C156723F4790771BFE55
      79F463274E1837BDF040E2DD5C282489080FF770D87B7BBD7AC01331229A9591
      2752A867EA238DAF6EC649CADB47816199455B9BDB6D32066FAADBEF242821E8
      04631A8D066688D00CD3EBC3015E535CBEFB93AB37A91EDB85BA3787CE18F67D
      D253D98A8871BF4B4C28295803DD0326804101003620AC0042D374778FDDCBD1
      69B1E66EABB23A886753E665A62D1ADA197ABEF5FEB103274F6B948AEAE27551
      91E383D4634264246020144D5BADDD3CCF0963347C7EF6B5BA63E12AC5EB8539
      7AAD76D89EADEDA6BC9DFBED4EC2F074EAE2057382DC38D113A3DC7A0087CD66
      E3385E5894F999CFB3F2B0B909D115C506086EC3F654C8E5F9A5E58D37CD52DA
      F9594D25CDB88259977E0E8AB6D86C3CC749A5D2430D1F557CF029E9B09F33EE
      924A65234D143A53143537BB0053AA8BD21F4B7B7C2113108A10C1B5D1133D1C
      56B71E1C04B694D506072F79E80EFD36C3DADB3B20269315BCBAA3F127B39263
      4EEDAF20283A603DBC1C5497C506FE71FAFC85570E36B868EACDA2355362F5BF
      F8961BAD6D596595324C5EFCCCE2F9C90F04CEA18D167B38AC3648F715B5474E
      34B54C8FD1EE5C9F3B9A0C0281C75059F3CDCDB694998905CBD3D1FFA75A80E1
      A13DDB171E07382C562B49310B57BF285186653F3A7B79DA62D77069CFCF80B5
      F66843F5A9468E709CD8BB4D4857A3318840B50D2776D57FF852F6338FCF9BA3
      D3467BD6A5B3CB42D1CC82ECF5725474AEF60D921EED62834B252FCF132BC397
      CDB96FCDCA0C6A741D6DDD3D6B77FCCDDC8B67A63C94B1284507FE0135511F87
      15FC03C7F1C8F1E370824446AD3078EBCEB7DE3EDCD8C2E2BDE70E54322C379A
      5E17BFF9AEA0CAA8522A5E5B95317DEA9D93B45A54ECD1A30BE2070C1F404422
      482A75FD26B142BD6CEEAF9F5B9AF68B0B0AB9337DFDC67692D76368FDCE5248
      5EFA495A14EDD3C3DCD915704C848E5575FFFCE8AB4B77C768B7AFCDBABD9780
      1FDCF8E9D6B2B22AF87E684BBE5EA783EE833884CC1E9835365F2A79E7039A24
      DFF853DEED37BC1C93AD2C2EBD612713344AE3CBC52445C122C4E82779FCD46C
      EE0CA69250CAB1C756199C2896A48DA8DAFC2239724CFBF6FAF71BF7D73909B2
      2C7759F2BDF708F1C3C301501DE64E3E083DA0DCFDF8F3335B0F3630347D684B
      616CCCA491D4AD397AFCF0174D13D4CA3D85B99A88080F478C5E2C7098CD6621
      CF056CE0164B0B375B5C68C23895B1A488A0A8A16DC46274418E41A60A4B9D31
      353FEB8F909206739094C96C0E460FC19AAE7CB7A9FA202695FC65C5933313A7
      FBFDAB9063EB5E2EFF4F879D73DA1BDFDEDBE3708AFAE37A6C4C0C20BA393ADC
      7A04CB017E965DB6DB825389BAC8DD1BF37D631A0C76F1F2958D35F514E3DAB2
      E289DFCE4E168E2963C20161FE93D3675E397212C6285CF2C8929447E094DA8F
      4817EDA9B966B2C48D0FDF919FE33D3BF97190269399E383E5000B53290DE57B
      CE5EBFC5B998EDAB3292EF4B844C8649A5DB8DB51F9EBF2CE2B9B2ACA5B367CE
      F0CE59E0888B050E711F474708F4100C2708C3EEEA9B563B47D31BFEB0E8E107
      671594EFB9DAD943D074EEC2392BD2527DCB1A7F8E769329C8FDE23570FCABD7
      7F28F9FB7B169C74713C043775989A63989419D336C01E710D2ADB048EF8B858
      2F4747A8F410F5A5F5B60E73F5FBC74E5E6861115481B0A57959F7DF336D684D
      E3CFD1D66E0A21876070EA9448E044E6BED7814262D8FC25704C8E8F1B438ED1
      983F476B5BBB2F87507F84EA92C3D7FCDE2C70244C8E1FC4018F5472ACF41F47
      BA7A7AA7E8A2F3972EF61E4C02BFA74206EEDB80A1E4C0E17F9F6FDEBC323DE9
      CEC91AB56A1007011CAD6DC001C36F31D6DFEAE88A8B9E00BE36488F206FCCFA
      5F0579C4DC6D871AF4FE6953FEBC221DB60F704C49982C7010ADAD6E3D3ABBED
      15F50DCDD7FF3B355607C1A7AFB2F20E1FE41A09EFE1617A3FB67538496A7ED2
      F497329F625CEC0007E4DB9F7F6E15FC03DAD51C3FF5E5956BF1DAA8AD594F53
      811E184762C9AF345EBCF6E3BEC29C98A8C830A5022497C964B1906F81038E21
      EDED26BCFFB21002116C396811F0A9F53686C9DC55234933823E20C6C489519E
      FB209EE39DB81342193B8A334B684DAD56EB745AE100E6A9D1056F059F18CB2B
      DC41A650C8755AADB4BFAE1E382B300CE3E8751034054E39A63470A45429552A
      95D27B893B8843D41758C6227C0D83D267BE4FFE07CE5971E3271AC5BF000000
      0049454E44AE426082}
  end
  object lblPageTitle: TLabel
    Left = 110
    Top = 35
    Width = 116
    Height = 13
    Caption = 'Frequencies and solvers'
  end
  object lblMasterFreqs: TLabel
    Left = 226
    Top = 96
    Width = 142
    Height = 13
    Caption = 'Master Spectrum Frequencies'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblMasterSpectrumFreqs: TLabel
    Left = 226
    Top = 119
    Width = 111
    Height = 13
    Caption = 'Master Spectrum Freqs'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbUserDefined1: TLabel
    Left = 24
    Top = 704
    Width = 68
    Height = 13
    Caption = 'User defined: '
  end
  object lbUserDefined2: TLabel
    Left = 139
    Top = 704
    Width = 66
    Height = 13
    Caption = '* log(r/r0)  + '
  end
  object lbUserDefined3: TLabel
    Left = 299
    Top = 704
    Width = 27
    Height = 13
    Caption = '* r/r0'
  end
  object rgBandwidth: TRadioGroup
    Left = 19
    Top = 96
    Width = 185
    Height = 62
    Caption = 'Assessment bandwidth'
    ItemIndex = 0
    Items.Strings = (
      'Octave'
      'Third Octave')
    TabOrder = 0
    OnClick = rgBandwidthClick
  end
  object udLowFreq: TUpDown
    Left = 226
    Top = 138
    Width = 17
    Height = 25
    Max = 10000
    Position = 5000
    TabOrder = 1
    Thousands = False
    OnClick = udLowFreqClick
  end
  object udHighFreq: TUpDown
    Left = 267
    Top = 138
    Width = 17
    Height = 25
    Max = 10000
    Position = 5000
    TabOrder = 2
    Thousands = False
    OnClick = udHighFreqClick
  end
  object btSetCurrentSolversToDefault: TButton
    Left = 28
    Top = 767
    Width = 212
    Height = 25
    Caption = 'Save solver configuration as default'
    TabOrder = 3
    Visible = False
  end
  object cbCoherent: TCheckBox
    Left = 23
    Top = 736
    Width = 277
    Height = 17
    Caption = 'Coherent calculation'
    TabOrder = 4
    Visible = False
    OnClick = cbCoherentClick
  end
  object btAdvancedSolverOptions: TButton
    Left = 19
    Top = 176
    Width = 185
    Height = 25
    Caption = 'Advanced solver options'
    TabOrder = 5
    OnClick = btAdvancedSolverOptionsClick
  end
  object edUserDefinedLogPart: TEdit
    Left = 98
    Top = 701
    Width = 35
    Height = 21
    TabOrder = 6
    Text = '15'
  end
  object edUserDefinedLinPart: TEdit
    Left = 214
    Top = 701
    Width = 79
    Height = 21
    TabOrder = 7
    Text = '0.0000'
  end
  object btUserDefined: TButton
    Left = 332
    Top = 699
    Width = 162
    Height = 25
    Caption = 'Set user defined equation'
    TabOrder = 8
    OnClick = btUserDefinedClick
  end
  object pcSolver: TPageControl
    Left = 19
    Top = 217
    Width = 510
    Height = 448
    ActivePage = tsSplitSolver
    TabOrder = 9
    object tsSingleSolver: TTabSheet
      Caption = 'Single solver'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object rgSolver: TRadioGroup
        Left = 11
        Top = 41
        Width = 471
        Height = 210
        Caption = 'Solver'
        ItemIndex = 1
        Items.Strings = (
          '20 log(r/r0)'
          '20 log(rd/r0) + 10 log(r/rd)'
          'User defined equation'
          'dBSeaPE: parabolic equation      (applicable at low frequencies)'
          'dBSeaModes: normal modes       (applicable at low frequencies)'
          
            'dBSeaRay: ray tracing                (applicable at high frequen' +
            'cies)'
          'dBSeaPE3d'
          'dBSeaRay3d'
          'ramGEO: parabolic equation       (applicable at low frequencies)'
          
            'Kraken: normal modes                (applicable at low frequenci' +
            'es)'
          
            'Bellhop: ray tracing                    (applicable at high freq' +
            'uencies)')
        TabOrder = 0
        OnClick = rgSolverClick
      end
      object cbSplitSolver_single: TCheckBox
        Left = 15
        Top = 12
        Width = 329
        Height = 17
        Caption = 'Use separate solvers for low and high frequencies'
        TabOrder = 1
        OnClick = cbSplitSolver_splitClick
      end
    end
    object tsSplitSolver: TTabSheet
      Caption = 'Split solver'
      ImageIndex = 1
      object lbCrossoverFreqHi: TLabel
        Left = 20
        Top = 108
        Width = 88
        Height = 13
        Caption = 'lbCrossoverFreqHi'
      end
      object lbCrossoverFreqLo: TLabel
        Left = 20
        Top = 89
        Width = 90
        Height = 13
        Caption = 'lbCrossoverFreqLo'
      end
      object lbCrossoverFreq: TLabel
        Left = 43
        Top = 46
        Width = 101
        Height = 13
        Caption = 'Crossover frequency'
      end
      object rgHighSolver: TRadioGroup
        Left = 254
        Top = 158
        Width = 223
        Height = 210
        Caption = 'High frequency solver'
        ItemIndex = 1
        Items.Strings = (
          '20 log(r/r0)'
          '20 log(rd/r0) + 10 log(r/rd)'
          'User defined equation'
          'dBSeaPE: parabolic equation'
          'dBSeaModes: normal modes'
          'dBSeaRay: ray tracing'
          'dBSeaPE3d'
          'dBSeaRay3d'
          'ramGEO: parabolic equation'
          'Kraken: normal modes'
          'Bellhop: ray tracing')
        TabOrder = 1
        OnClick = rgHighSolverClick
      end
      object rgLowSolver: TRadioGroup
        Left = 20
        Top = 158
        Width = 224
        Height = 210
        Caption = 'Low frequency solver'
        ItemIndex = 1
        Items.Strings = (
          '20 log(r/r0)'
          '20 log(rd/r0) + 10 log(r/rd)'
          'User defined equation'
          'dBSeaPE: parabolic equation'
          'dBSeaModes: normal modes'
          'dBSeaRay: ray tracing'
          'dBSeaPE3d'
          'dBSeaRay3d'
          'ramGEO: parabolic equation'
          'Kraken: normal modes'
          'Bellhop: ray tracing')
        TabOrder = 2
        OnClick = rgLowSolverClick
      end
      object udSplitSolverFreq: TUpDown
        Left = 20
        Top = 46
        Width = 17
        Height = 25
        Max = 10000
        Position = 5000
        TabOrder = 3
        Thousands = False
        OnClick = udSplitSolverFreqClick
      end
      object cbSplitSolver_split: TCheckBox
        Left = 15
        Top = 12
        Width = 329
        Height = 17
        Caption = 'Use separate solvers for low and high frequencies'
        TabOrder = 4
        OnClick = cbSplitSolver_splitClick
      end
      object mmLiteVersion: TMemo
        Left = 20
        Top = 43
        Width = 348
        Height = 97
        BorderStyle = bsNone
        Lines.Strings = (
          'This free version of dBSea only allows the use of the '
          'simple solvers. The full version offers a complete range of '
          'solvers designed to cover different scenarios, as well as'
          'allowing different solvers to be used in the low and high'
          'frequency ranges. '
          'Please contact your distributor for more information.')
        TabOrder = 0
      end
    end
  end
end
