﻿unit dbSeaConstants;

interface

    const
        sProgramName            = {$IF Defined(LITE)}'dBSea Basic'{$ELSE}'dBSea'{$ENDIF};
        saveFileVersion         = '8.0';
        CompressionAlgorithm    = 'SynLZ';
        sUserDB                 = 'User';
        sDefaultProjectFileExt  = '.uwa';
        sTempPathLastComponent  = 'dBSea Ltd';
        SecurityString          = '+z890RITPQrVd2mx'; //used for encrypting date value placed in registry
        sRegistryKey            = 'SOFTWARE\\dBSea Ltd\\dBSea';  //main registry key, everything goes here
        sRegistryUWADefault     = 'dBSea.projectFile'; //associate with .uwa files
        sRegistryExtKeys        = 'SOFTWARE\\Classes\\'; //create .uwa under this
        sRegistryDefaultIcon    = 'SOFTWARE\\Classes\\dBSea.projectFile\\DefaultIcon';
        sLatestVersionURL       = 'http://www.dbsea.co.uk/download'; //change once there is a definite address for it
        sLicenseCheckDBURL      = 'http://dBSea.parseapp.com/serialNumber/';
        sDownloadLatestVersionURL = 'http://www.dbsea.co.uk/download/';
        sSolveServer            = 'http://localhost:8080'; //'http://52.6.119.29:80';
        {$IFDEF DEBUG}
        defaultBathymetryFile   = 'Irish Sea Bathymetry with nans.csv';
        {$ELSE}
        defaultBathymetryFile   = 'Irish Sea Bathymetry.csv';
        {$ENDIF}
        CSVEditorEXEFile        = 'CSVEditor.exe';
        helpFile                = 'dBSea help.chm';
        ReleaseNotesFile        = 'dBSea release notes.txt';
        defaultZ0               = 100.0;
        sTickMark               : string = #$2713; //check mark unicode
        nanString               = 'NAN';

        equipTblName            = 'equiptable';
        mitigationTblName       = 'mitigationtable';
        fishTblName             = 'fishtable';
        sedimentTblName         = 'sedimenttable';
        SSPtblName              = 'SSPtable';
        waterPropsTblName       = 'waterpropstable';
        thresholdShiftTblName   = 'thresholdshifttable';
        metaTblName             = 'meta';
        currentDBVersion        = 14;
        mdaDBFile               = 'acoustLib.db';
        userDBFile              = 'userLib.db';


        attenuationtblFields    : array [0..5] of string = ('ID','Name','DataSource','Comments','Temperature','Salinity');
        thresholdShiftTblFields : array [0..11] of string = ('ID','Name','Datasource','Comments',
                                                            'PTSImpulsePeakLimit', 'PTSImpulseSELLimit', 'PTSNonImpulsePeakLimit', 'PTSNonImpulseSELLimit',
                                                            'TTSImpulsePeakLimit', 'TTSImpulseSELLimit', 'TTSNonImpulsePeakLimit', 'TTSNonImpulseSELLimit');

        updateUIMessagesIntervalMS: cardinal = 500;

        baseFreqOctave          = 16.0;
        baseFreqThirdOctave     = 12.5;

        lowLev                  : single = 0.0; //absolute low level below which we don't show results. //NB this doesn't work for diff

        iUndoMovesMax           = 16;
        iRecentFilesMaxLength   = 19;

        //security strings for accessing encoded db fields
        DBSecurityString0       = 'qAn5Fg4UJKmOP2uR';
        DBSecurityString1       = 'SqN3Ga0IOiTuQr6t';
        DBSecurityString2       = '0IzcdnRSHCO6kwLZ';
        DBSecurityString3       = 'hudxLV8mUbsABoyk';

        NULL_DATE               = -693594;

        RamBinary               = 'ramMDA3.exe';
        BellhopBinary           = 'bellhopMDA3.exe';
        KrakenBinary            = 'krakencMDA1.exe';
        FieldBinary             = 'fieldMDA2.exe';

resourcestring
  rsDSUser = 'Data source: user defined';
  rsDSMDA = 'Data source: default library';
  rsErrMDANoTable = 'Error: Default database contains no table ';
  rsUserNoTable = 'Error: User database contains no table ';


implementation

end.