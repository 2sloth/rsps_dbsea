unit SmartPointer;

interface

uses
  SysUtils;

type
  ISmart<T> = reference to function: T;

  TSmart<T: class, constructor> = class(TInterfacedObject, ISmart<T>)
  private
    FValue: T;
  public
    constructor Create; overload;
    constructor Create(AValue: T); overload;
    destructor Destroy; override;
    function Invoke: T;
  end;

implementation

{ TSmartPointer<T> }

constructor TSmart<T>.Create;
begin
  inherited;
  FValue := T.Create;
end;

constructor TSmart<T>.Create(AValue: T);
begin
  inherited Create;
  if AValue = nil then
    FValue := T.Create
  else
    FValue := AValue;
end;

destructor TSmart<T>.Destroy;
begin
  FValue.Free;
  inherited;
end;

function TSmart<T>.Invoke: T;
begin
  Result := FValue;
end;

end.
