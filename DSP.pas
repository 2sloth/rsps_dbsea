unit DSP;

interface

uses
    syncommons,
    system.sysutils, system.classes, system.math, system.types,
    vcl.dialogs,
    winapi.windows,
    baseTypes, SmartPointer, eventLogging,
    filterDef;

type

    TDSP = class(TObject)
        class procedure extendBy1Sample(var y: TDoubleDynArray);
        class function sincFilter(const m, N, len: integer): TDoubleDynArray;
        class function upsampleBy2(const v: TDoubleDynArray): TDoubleDynArray;
        class function downsampleBy2(const v: TDoubleDynArray; const antialiasFilter: TFilterDef): TDoubleDynArray;
        class function thirdOctbandFilter(const fs, centerFreq: double): TFilterDef;
        class function octbandFilter(const fs, centerFreq: double): TFilterDef;
        class function LPF(const fs, f0, Q: double): TFilterDef;
        class function HPF(const fs, f0, Q: double): TFilterDef;
        class function applyFilter(const def: TFilterDef; const x: TDoubleDynArray): TDoubleDynArray;
        class function applyFilter3Times(const def: TFilterDef; const x: TDoubleDynArray): TDoubleDynArray;
        class function combineFilters(const def1, def2: TFilterDef): TFilterDef;
        class function directConvolve(const a, b: TDoubleDynArray): TDoubleDynArray; overload;
        class function directConvolve(const a, b: TEVect): TEVect; overload;
        class function cubeFilter(const def: TFilterDef): TFilterDef;
        class function rms(const x: TDoubleDynArray): double;
    end;



implementation

{$B-}

class function TDSP.thirdOctbandFilter(const fs, centerFreq : double) : TFilterDef;
var
    lpf,hpf: TFilterDef;
begin
    if isnan(centerFreq) or isnan(fs) or (centerFreq * power(2,1/6) * 1.05 > fs / 2) then exit(nil);

    lpf := TDSP.LPF(fs, centerFreq * power(2,1/6), 0.88); //0.88 seems to work best for flat passband when 1/3 octave width
    hpf := TDSP.HPF(fs, centerFreq / power(2,1/6), 0.88);
    try
        if (lpf = nil) or (hpf = nil) then exit(nil);
        result := TDSP.combineFilters(lpf,hpf);
    finally
        lpf.free;
        hpf.free;
    end;
end;

class function TDSP.octbandFilter(const fs, centerFreq : double) : TFilterDef;
var
    lpf,hpf: TFilterDef;
begin
    if isnan(centerFreq) or isnan(fs) or (centerFreq * sqrt(2) * 1.05 > fs / 2) then exit(nil);

    lpf := TDSP.LPF(fs, centerFreq * sqrt(2), 0.77); //0.77 seems to work best for flat passband when octave width
    hpf := TDSP.HPF(fs, centerFreq / sqrt(2), 0.77);
    try
        if (lpf = nil) or (hpf = nil) then exit(nil);
        result := TDSP.combineFilters(lpf,hpf);
    finally
        lpf.free;
        hpf.free;
    end;
end;

class function TDSP.LPF(const fs, f0, Q: double): TFilterDef;
var
    w0, alpha, sinW0, cosW0: double;
begin
    if isnan(f0) or isnan(fs) or isnan(Q) or (f0 > fs / 2) then exit(nil);

    w0 := 2 * pi * f0 / fs;
    system.SineCosine(w0, sinW0, cosW0);
    alpha := sinW0/(2*Q);

    result := TFilterDef.Create(3);
    result.b[0] :=  (1 - cosW0)/2;
    result.b[1] :=   1 - cosW0;
    result.b[2] :=  (1 - cosW0)/2;
    result.a[0] :=   1 + alpha;
    result.a[1] :=  -2*cosW0;
    result.a[2] :=   1 - alpha;
end;


class function TDSP.HPF(const fs, f0, Q: double): TFilterDef;
var
    w0, alpha, sinW0, cosW0: double;
begin
    if isnan(f0) or isnan(fs) or isnan(Q) or (f0 > fs / 2) then exit(nil);

    w0 := 2 * pi * f0 / fs;
    system.SineCosine(w0, sinW0, cosW0);
    alpha := sinW0/(2*Q);

    result := TFilterDef.Create(3);
    result.b[0] :=  (1 + cosW0)/2;
    result.b[1] := -(1 + cosW0);
    result.b[2] :=  (1 + cosW0)/2;
    result.a[0] :=   1 + alpha;
    result.a[1] :=  -2*cosW0;
    result.a[2] :=   1 - alpha;
end;

class function TDSP.applyFilter(const def: TFilterDef; const x: TDoubleDynArray): TDoubleDynArray;
var
    N, m: integer;
begin
    setlength(result,length(x));

    //start of output
    for n := 0 to min(length(x),length(def.b)) - 1 do
    begin
        result[n] := def.b[0]/def.a[0]*x[n];
        for m := 1 to n do
            result[n] := result[n] + def.b[m]/def.a[0]*x[n - m] - def.a[m]/def.a[0]*result[n - m];
    end;

    //main part of output
    for n := length(def.b) to length(x) - 1 do
    begin
        result[n] := def.b[0]/def.a[0]*x[n];
        for m := 1 to length(def.b) - 1 do
            result[n] := result[n] + def.b[m]/def.a[0]*x[n - m] - def.a[m]/def.a[0]*result[n - m];
    end;
end;

class function TDSP.applyFilter3Times(const def: TFilterDef; const x: TDoubleDynArray): TDoubleDynArray;
begin
    result := TDSP.applyFilter(def,x);
    result := TDSP.applyFilter(def,result);
    result := TDSP.applyFilter(def,result);
end;

class function TDSP.combineFilters(const def1, def2: TFilterDef): TFilterDef;
begin
    if not (assigned(def1) and assigned(def2)) then exit(nil);

    result := TFilterDef.create(0);
    result.b := directConvolve(def1.b,def2.b);
    result.a := directConvolve(def1.a,def2.a);
end;

class function TDSP.directConvolve(const a, b : TDoubleDynArray) : TDoubleDynArray;
var
    i,j : integer;
begin
    //direct convolution
    setlength(result,length(a) + length(b) - 1);
    for i := 0 to length(a) - 1 do
        for j := 0 to length(b) - 1 do
            result[i + j] := result[i + j] + a[i]*b[j];
end;

class function TDSP.directConvolve(const a, b: TEVect): TEVect;
var
    i,j: integer;
begin
    //direct convolution
    setlength(result,length(a) + length(b) - 1);
    for i := 0 to length(a) - 1 do
        for j := 0 to length(b) - 1 do
            result[i + j] := result[i + j] + a[i]*b[j];
end;

class function TDSP.cubeFilter(const def: TFilterDef): TFilterDef;
var
    sqFilt: ISmart<TFilterDef>;
begin
    sqFilt := TSmart<TFilterDef>.create(TDSP.combineFilters(def,def));
    result := TDSP.combineFilters(def,sqFilt);
end;

class function TDSP.downsampleBy2(const v: TDoubleDynArray; const antialiasFilter: TFilterDef): TDoubleDynArray;
var
    i: integer;
    tmp: TDoubleDynArray;
begin
    tmp := TDSP.applyFilter(antialiasFilter, v); // antialias before downsampling
    setlength(result, length(v) div 2);
    for i := 0 to length(result) - 1 do
        result[i] := tmp[i * 2]; // take every second sample
end;

class function TDSP.upsampleBy2(const v: TDoubleDynArray): TDoubleDynArray;
const
    sincLength: integer = 31;
var
    i, lenx: integer;
    brickwallFilter, z: TDoubleDynArray;
begin
    lenx := length(v);
    setlength(z,2*lenx);
    //make vector with the samples separated by 0s
    for i := 0 to lenx - 1 do
        z[i*2] := v[i];

    brickwallFilter := sincFilter(lenx,2,sincLength);  // FFT(sinc) = rect, ie ideal lowpass filter

    z := directConvolve(brickwallFilter,z);

    //z is now too long, cut out the start and ends
    setlength(result, 2*lenx);
    for i := 0 to 2*lenx - 1 do
        result[i] := 2*z[sincLength div 2 + i];
end;

class function TDSP.sincFilter(const m, N, len: integer): TDoubleDynArray;
    function sinc(const x: double): double;
    begin
        if x = 0 then
            result := 1
        else
            result := sin(x) / x;
    end;

var
    i: integer;
begin
    setlength(result, len);
    for i := 0 to len - 1 do
        result[i] := sinc(pi * (-len / 2 + i + 1) / N) / N;
end;


class procedure TDSP.extendBy1Sample(var y: TDoubleDynArray);
var
    len: integer;
begin
    len := length(y);
    setlength(y, len + 1);
    y[len] := 2 * y[len - 1] - y[len - 2];
end;

class function TDSP.rms(const x: TDoubleDynArray): double;
var
    i: integer;
begin
    result := 0;
    for i := 0 to length(x) - 1 do
        result := result + x[i] * x[i];

    result := sqrt(result / length(x));
end;


end.
