unit dBSeaColours;

interface

uses winapi.windows, system.math, system.SysUtils,
    vcl.graphics,
    baseTypes, helperFunctions, myColor, mcklib, SmartPointer;

type

{$RTTI INHERIT}

  TColorArray = array of TColor;

    TDBSeaColours = class(TObject)
    private
    const
        {default lists of colors to use. NB these have to be the same length, due to the
        case statement in resetResultsColorList. Remember delphi colors are BGR}

        defaultColorOrder : array[0..4,0..4] of integer = ((0,-1,-1,-1,-1),
                                                            (0,4,-1,-1,-1),
                                                            (0,2,4,-1,-1),
                                                            (0,2,3,4,-1),
                                                            (0,1,2,3,4));
        alternateColorOrder : array[0..4,0..4] of integer = ((0,-1,-1,-1,-1),
                                                            (0,4,-1,-1,-1),
                                                            (0,2,4,-1,-1),
                                                            (0,2,3,4,-1),
                                                            (0,1,2,3,4));
        //rgb: 0000b2, 05009a, f10550, de5e4a, df8400, d49d67, dddd00, aafd2b, 09f207, 01bd04, 008100
        //bgr: b20000	9a0005	5005f1	4a5ede	0084df	679dd4	00dddd	2bfdaa	07f209	04bd01	008100

        //topoLandColors is trying to simulate the classic topo map look
        topoLandColors : array [0..4] of TColor = ($004400, //dark green
                                                    $009900, //light green
                                                    $00b7dd, //yellow
                                                    $13458b, //brown
                                                    $eeeeee);//off white
        simpleLandColors : array [0..1] of TColor = ($006400, $000000);
    public
    const
        defaultColors : array[0..4] of Tcolor = ($FF0000,$A300A3,$0000FF,$00FFFF,$00FF00);
        alternateColors : array[0..4] of TColor = ($0000FF,$0077FF,$00FFFF,$FFFF00,$FF0000);
        defaultCustomColors : array[0..15] of TColor = ($443500,$9e8732,$cdc574,$5B3CB0,
                                                     $3666B6,$28DE0E,$657387,$9E4E98,
                                                     $E1DD8C,$C94823,$5B0AE2,$14D8D2,
                                                     $579580,$071EE4,$8F5C70,$AC4050);

        class function darkBlue: TColor;
        class function midBlue: TColor;
        class function lightBlue: TColor;

        class function formBackground: TColor;
        class function text: TColor;
        class function GLtext: TColor;
        class function GLXSectext: TColor;
        class function GL3DBorderText: TColor;
        class function buttonBackground: TColor;
        class function headerRow: TColor;
        class function levelsColumn: TColor;
        class function greyText: TColor;
        class function white(const d: double): TColor;

        class function defaultResultsShadowZone: TColor;
        class function defaultLand: TColor;
        class procedure defaultTopoLandColors(var theColorArray: TColorArray);
        class function LandMapEdges: TColor;
        class function XSecSediment: TColor;
        class function XSecSedimentBorder: TColor;
        class function ExclusionZone: TColor;
        class function directionArrow: TColor;
        class function directionFill: TColor;

        class function defaultCustomColorsString: string;

        class procedure resetResultsColorList(aColorList: TMyColorList; listLen: integer; iSourceList: integer);
        class procedure updateResultsLevelColorsAnyList(const aColorList: TMyColorList; const listLen: integer);

        class function HSVtoRGB(H, S, V: double): TColor;
        class function randomSaturatedColor: TColor;
        class function colorMix(const a, b: TColor; const alpha: double): TColor;
        class function ColorToHtml(DColor: TColor): string; static;
        class function HtmlToColor(S: string; aDefault: TColor): TColor; static; //just green to black
    end;


implementation

class function TDBSeaColours.HtmlToColor(S: string; aDefault: TColor): TColor;
begin
    if copy(S, 1, 1) = '#' then
    begin
        S := '$' + copy(S, 6, 2) + copy(S, 4, 2) + copy(S, 2, 2);
    end
    else
        S := 'clNone';
    try
        result := StringToColor(S);
    except
        result := aDefault;
    end;
end;

class function TDBSeaColours.ColorToHtml(DColor: TColor): string;
var
    tmpRGB: TColorRef;
begin
    tmpRGB := ColorToRGB(DColor);
    result := Format('#%.2x%.2x%.2x',
      [GetRValue(tmpRGB),
      GetGValue(tmpRGB),
      GetBValue(tmpRGB)]);
end;

class function TDBSeaColours.colorMix(const a, b: TColor; const alpha: double): TColor;
var
    aR,aG,aB,bR,bG,bB: integer;

    function toByte(i: integer): byte; inline;
    begin
        result := max(0, min(255, i));
    end;
begin
    aR := saferound(getRValue(a) * (1 - alpha));
    aG := saferound(getGValue(a) * (1 - alpha));
    aB := saferound(getBValue(a) * (1 - alpha));

    bR := saferound(getRValue(b) * (alpha));
    bG := saferound(getGValue(b) * (alpha));
    bB := saferound(getBValue(b) * (alpha));

    result :=  RGB(toByte(aR + bR),toByte(aG + bG),toByte(aB + bB));
end;

class function  TDBSeaColours.darkBlue: TColor;
begin
    result := $443500;   //rgb 1 54 68, hsv 193 99 27
end;

class function  TDBSeaColours.midBlue: TColor;
begin
    result := $9e8732;  // rgb 50 135 158, hsv 193 68 62
end;

class function  TDBSeaColours.lightBlue: TColor;
begin
    result := $cdc574;  //rgb 117 195 205, hsv 187 43 80
end;

class function TDBSeaColours.formBackground: TColor;
begin
    result := $FFFFFF;
end;

class function TDBSeaColours.text: TColor;
begin
    result := $000000;
end;

class function TDBSeaColours.GLtext: TColor;
begin
    result := $FFFFFF;
end;

class function TDBSeaColours.GLXSectext: TColor;
begin
    result := $000000;
end;

class function TDBSeaColours.GL3DBorderText: TColor;
begin
    result := $000000;
end;

class function TDBSeaColours.buttonBackground: TColor;
begin
    result := $000000;
end;

class function TDBSeaColours.headerRow: TColor;
begin
    result := RGB(230, 230, 230);
end;

class function TDBSeaColours.levelsColumn: TColor;
begin
    result := RGB(245, 245, 245);
end;

class function TDBSeaColours.greyText: TColor;
begin
    result := RGB(130, 130, 130);
end;

class function TDBSeaColours.defaultResultsShadowZone: TColor;
begin
    result := RGB(100, 100, 100);
end;

class function TDBSeaColours.defaultLand: TColor;
begin
    result := simpleLandColors[0]; // RGB(0, 100, 0);
end;

class procedure TDBSeaColours.defaultTopoLandColors(var theColorArray: TColorArray);
var
    i: integer;
begin
    // nice multi colour version
    setlength(theColorArray, length(topoLandColors));
    for i := 0 to length(topoLandColors) - 1 do
        theColorArray[i] := topoLandColors[i];

    // //simple version - green to black
    // setlength(theColorArray, length(simpleLandColors));
    // for i := 0 to length(simpleLandColors) - 1 do
    // theColorArray[i] := simpleLandColors[i];
end;

class function TDBSeaColours.LandMapEdges: TColor;
begin
    result := RGB(80, 80, 80);
end;

class function TDBSeaColours.XSecSediment: TColor;
begin
    result := RGB(110, 110, 110);
end;

class function TDBSeaColours.XSecSedimentBorder: TColor;
begin
    result := RGB(max(0, getRValue(TDBSeaColours.XSecSediment) - 20),
      max(0, getGValue(TDBSeaColours.XSecSediment) - 20),
      max(0, getBValue(TDBSeaColours.XSecSediment) - 20));
end;

class function TDBSeaColours.ExclusionZone: TColor;
begin
    result := RGB(255, 0, 0);
end;

class function TDBSeaColours.directionArrow: TColor;
begin
    result := TDBSeaColours.midBlue;
end;

class function TDBSeaColours.directionFill: TColor;
begin
    result := clWhite; // RGB(240,240,240);
end;

class function TDBSeaColours.defaultCustomColorsString: string;
begin
    result := 'ColorA=443500,ColorB=9e8732,ColorC=cdc574,ColorD=5B3CB0,ColorE=3666B6,ColorF=28DE0E,ColorG=657387,ColorH=9E4E98,ColorI=E1DD8C,ColorJ=C94823,ColorK=5B0AE2,ColorL=14D8D2,ColorM=579580,ColorN=071EE4,ColorO=8F5C70,ColorP=AC4050';
end;

class function TDBSeaColours.randomSaturatedColor: TColor;
begin
    result := HSVtoRGB(Random(1000)/1000, 1.0, 1.0);
end;

class procedure TDBSeaColours.resetResultsColorList(aColorList : TMyColorList; listLen : integer; iSourceList : integer);
{get listlen colours from the default color list.
use listLen = -1 to get the default entire list. somewhat kludgey, any colorlist
may be reset as we also want to be able to reset the tempcolorlist on
the resultsColors form.}

    procedure extendColorList(aColorList: TMyColorList; targetListLength: integer);
    {for each pair of colors in the array, insert an interpolated color between them, up
    to the required total number of colors}
    var
        avCol, colA, colB: TColor;
        i: integer;
    begin
        if aColorList.Count = 0 then Exit;

        {add a single color by interpolating. repeat until we have added the required number of colors}
        i := 0;
        while aColorList.Count < targetListLength do
        begin
            colA := aColorList[i].Color;
            colB := aColorList[i + 1].Color;
            avCol := THelper.TColorMix(colA,colB,0.5);
            aColorList.Insert(i + 1,TMyColor.CreateWithColor(avCol,true)); //insert into list
            i := i + 2;

            {if we are at the end of the list, go back to the start. rinse and repeat}
            if i >= aColorList.Count - 1 then
                i := 0;
        end;
    end;

var
    i, j: integer;
    ind: integer;

    useList: array of TColor;
    useListOrder: TIntegerMatrix;
begin
    aColorList.Clear;

    if iSourceList = 1 then
    begin
        setlength(useList,length(alternateColors));
        setlength(useListOrder,length(alternateColorOrder),length(alternateColorOrder[0]));
        for i := 0 to length(useList) - 1 do
            useList[i] := alternateColors[i];
        for i := 0 to length(useListOrder) - 1 do
            for j := 0 to length(useListOrder[0]) - 1 do
                useListOrder[i,j] := alternateColorOrder[i,j];
    end
    else
    begin
        setlength(useList,length(defaultColors));
        setlength(useListOrder,length(defaultColorOrder),length(defaultColorOrder[0]));
        for i := 0 to length(useList) - 1 do
            useList[i] := defaultColors[i];
        for i := 0 to length(useListOrder) - 1 do
            for j := 0 to length(useListOrder[0]) - 1 do
                useListOrder[i,j] := defaultColorOrder[i,j];
    end;

    {can't have different length default lists, as the case expression below
    expects const bounds, so unless you have an if then switch within the
    case statement, we can't do something like
    case 1 .. length(dyn array)}

    case listlen of
        -1 : begin
            {use listlen = -1 to get the default entire list}
            for i := 0 to length(useList) - 1 do
                aColorList.Add(TMyColor.CreateWithColor(useList[i],true));
        end;
        0 : begin
            {we shouldn't get listLen = 0 but if we do then insert the first color}
            aColorList.Add(TMyColor.CreateWithColor(useList[0],true));
        end;
        1 .. (length(defaultColors) - 1) : begin
            {listlen is <= length of sourceList. pick colours out in the
            predermined order given in the array of array sourceListOrder}
            for i := 0 to listlen - 1 do
            begin
                ind := useListOrder[listlen,i];
                if checkRange(ind,0,length(useList) - 1) then
                    aColorList.Add(TMyColor.CreateWithColor(useList[useListOrder[listlen,ind]],true))
                else
                    aColorList.Add(TMyColor.CreateWithColor($00000,true));
            end;
        end;
        length(defaultColors) .. 99999 : begin
            {if we want more than the default list of colors in length, then
            generate the simple default list to start, and then use the extendColorList
            function to get as many entries as we need by interpolation}
            for i := 0 to length(useList) - 1 do
                aColorList.Add(TMyColor.CreateWithColor(useList[i],true));
            extendColorList(aColorList,listlen);
        end;
    end;
end;


class procedure TDBSeaColours.updateResultsLevelColorsAnyList(const aColorList: TMyColorList; const listLen: integer);
var
    tmpColorList: ISmart<TMyColorList>;
    myCol: TMyColor;
    i: integer;
begin
    {redo the color list. call after the limits have been changed (eg changing from 3D to XSec view)
    take a tmp copy, then use the reset list function. Copy over as many colors as needed from the
    tmp list, or all the values in the list. any remaining colors will be those from the reset function}
    tmpColorList := TSmart<TMyColorList>.create(TMyColorList.Create(true));

    for myCol in aColorList do
        tmpColorList.Add(TMyColor.CreateWithColor(myCol.color, myCol.Visible));

    {use the reset list function}
    TDBSeaColours.resetResultsColorList(aColorList,listlen,0);

    {copy the previous colors over - aColorList may be shorter or longer now}
    for i := 0 to smaller(aColorList.Count,tmpColorList.Count) - 1 do
    begin
        aColorList[i].Color := tmpColorList[i].Color;
        aColorList[i].Visible := tmpColorList[i].Visible;
    end;
end;



class function TDBSeaColours.white(const d: double): TColor;
var
    b : byte;
begin
    b := max(0,min(255,round(255.0 * d)));
    result := rgb(b,b,b);
end;


class function TDBSeaColours.HSVtoRGB(H, S, V: double): TColor;

    function RGBFP(R, G, b: double): TColor;
    const
        RGBmax = 255;
    begin
        result := RGB(round(RGBmax * R), round(RGBmax * G), round(RGBmax * b));
    end;

var
    i: integer;
    f, p, q, t: double;
begin
    Assert(InRange(H, 0.0, 1.0));
    Assert(InRange(S, 0.0, 1.0));
    Assert(InRange(V, 0.0, 1.0));

    if S = 0.0 then
    begin
        // achromatic (grey)
        result := RGBFP(V, V, V);
        Exit;
    end;

    H := H * 6.0; // sector 0 to 5
    i := floor(H);
    f := H - i; // fractional part of H
    p := V * (1.0 - S);
    q := V * (1.0 - S * f);
    t := V * (1.0 - S * (1.0 - f));
    case i of
        0:
            result := RGBFP(V, t, p);
        1:
            result := RGBFP(q, V, p);
        2:
            result := RGBFP(p, V, t);
        3:
            result := RGBFP(p, q, V);
        4:
            result := RGBFP(t, p, V);
    else
        result := RGBFP(V, p, q);
    end;
end;

end.
