unit thresholdLevels;

interface

uses
    syncommons,
    system.math,
    vcl.stdCtrls,
    SQLiteTable3, dBSeaconstants, levelConverter, smartPointer,
    eventlogging;

type
    TThresholdLevels = class(TObject)
    private
        function peakLimit(const isImpulse: boolean): double;
        function SELLimit(const isImpulse: boolean): double;
    public
        name: string;
        comments: string;
        isPTS: boolean;
        PTSImpulsePeakLimit: double;
        PTSImpulseSELLimit: double;
        PTSNonImpulsePeakLimit: double;
        PTSNonImpulseSELLimit: double;
        TTSImpulsePeakLimit: double;
        TTSImpulseSELLimit: double;
        TTSNonImpulsePeakLimit: double;
        TTSNonImpulseSELLimit: double;

        function threshold(const levType: TLevelType): double;

        class procedure thresholdsBoxInit(const cbThresholds: TComboBox; const MDASldb, userSlDb: TSQLiteDatabase);
        class procedure clearThresholdsBoxObjects(const cbThresholds: TComboBox);
    end;


implementation

function  TThresholdLevels.threshold(const levType: TLevelType): double;
begin
    case levType of
        //TODO which thresholds?
        kSPL : result := SELLimit(false);
        //kSWL : result := nan;
        kSELfreq : result := SELLimit(false);
        kSPLCrestFactor : result := peakLimit(false);
        kSELtime : result := SELLimit(true);
        kSPLpk : result := peakLimit(true);
        kSPLpkpk : result := peakLimit(true);
    else
        result := nan;
    end;
end;

function  TThresholdLevels.peakLimit(const isImpulse: boolean): double;
begin
    if isImpulse then
    begin
        if isPTS then
            result := PTSImpulsePeakLimit
        else
            result := TTSImpulsePeakLimit
    end
    else
    begin
        if isPTS then
            result := PTSNonImpulsePeakLimit
        else
            result := TTSNonImpulsePeakLimit
    end;
end;

function  TThresholdLevels.SELLimit(const isImpulse: boolean): double;
begin
    if isImpulse then
    begin
        if isPTS then
            result := PTSImpulseSELLimit
        else
            result := TTSImpulseSELLimit
    end
    else
    begin
        if isPTS then
            result := PTSNonImpulseSELLimit
        else
            result := TTSNonImpulseSELLimit
    end;
end;

class procedure TThresholdLevels.clearThresholdsBoxObjects(const cbThresholds: TComboBox);
var
    i : integer;
begin
    for i := 0 to cbthresholds.items.count - 1 do
        if assigned(cbThresholds.Items.Objects[i]) then
            TThresholdLevels(cbThresholds.Items.Objects[i]).Free;
end;

class procedure TThresholdLevels.thresholdsBoxInit(const cbThresholds: TComboBox; const MDASldb, userSlDb: TSQLiteDatabase);
    procedure addEntries(const sltb: TSqliteTable);
    var
        thresholds: TThresholdLevels;
    begin
        sltb.MoveFirst;
        while not sltb.EOF do
        begin
            //make 2 entries for each DB row
            thresholds  := TThresholdLevels.Create;
            thresholds.name := sltb.FieldAsString(sltb.FieldIndex['Name']) + ' PTS';
            thresholds.comments := sltb.FieldAsString(sltb.FieldIndex['Comments']);
            thresholds.PTSImpulsePeakLimit := sltb.FieldAsDouble(sltb.FieldIndex['PTSImpulsePeakLimit']);
            thresholds.PTSImpulseSELLimit := sltb.FieldAsDouble(sltb.FieldIndex['PTSImpulseSELLimit']);
            thresholds.PTSNonImpulsePeakLimit := nan; //sltb.FieldAsDouble(sltb.FieldIndex['PTSNonImpulsePeakLimit']);
            thresholds.PTSNonImpulseSELLimit := sltb.FieldAsDouble(sltb.FieldIndex['PTSNonImpulseSELLimit']);
            thresholds.isPTS := true;

            cbThresholds.AddItem(thresholds.name,thresholds);

            thresholds  := TThresholdLevels.Create;
            thresholds.name := sltb.FieldAsString(sltb.FieldIndex['Name']) + ' TTS';
            thresholds.comments := sltb.FieldAsString(sltb.FieldIndex['Comments']);
            thresholds.TTSImpulsePeakLimit := sltb.FieldAsDouble(sltb.FieldIndex['TTSImpulsePeakLimit']);
            thresholds.TTSImpulseSELLimit := sltb.FieldAsDouble(sltb.FieldIndex['TTSImpulseSELLimit']);
            thresholds.TTSNonImpulsePeakLimit := nan;//sltb.FieldAsDouble(sltb.FieldIndex['TTSNonImpulsePeakLimit']);
            thresholds.TTSNonImpulseSELLimit := sltb.FieldAsDouble(sltb.FieldIndex['TTSNonImpulseSELLimit']);
            thresholds.isPTS := false;

            cbThresholds.AddItem(thresholds.name,thresholds);

            sltb.Next;
        end;
    end;
var
    sltbMDA, sltbUser: ISmart<TSQLiteTable>;
    log: ISynLog;
begin
    log := TSynLogLevs.enter(self, 'thresholdsBoxInit');
    //cbThresholds.Clear;

    //if cbThresholds.Items.Count = 0 then
    begin
        eventlog.log(log, 'Open thresholds DB');

        //sort the db results by id, as the table entries have some with low ID that we want to come first
        sltbMDA := TSmart<TSQLiteTable>.create(MDASldb.GetTable(ansistring('SELECT * FROM ' + thresholdShiftTblName + ' ORDER BY ID')));
        sltbUser := TSmart<TSQLiteTable>.create(userSlDb.GetTable(ansistring('SELECT * FROM ' + thresholdShiftTblName + ' ORDER BY ID')));

        TThresholdLevels.clearThresholdsBoxObjects(cbThresholds);
        cbThresholds.Clear;
        cbThresholds.AddItem('Choose threshold',nil);

        addEntries(sltbMDA);
        addEntries(sltbUser);

        cbThresholds.ItemIndex := 0;
    end;
end;


end.
