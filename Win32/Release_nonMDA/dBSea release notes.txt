dBSea release notes

ver 1.3.0
28/08/2015

- Speed or time entry in source motion form
- Allow frequency oversampling in solve
- Export exclusion zone in ESRI format
- Added remove results from uwa file utility
- Added scale bar in GL
- Added grid display in GL
- Added more species weightings to database
- Improved radial smoothing function
- Skip solve for frequencies with no level
- Improved plotting threshold around single source
- Improved checking for new version
- dBSea will not crash if no internet connection on startup
- Forms allow resizing
- Fix minor memory leaks
- Cleanup options form
- Check for frequency bands above nyquist in time series solves
- Fix export title block bugs
- Fixed ESRI reading when short rows are present
- Improved license/HASP checking messages
- Check for all empty source spectra before solve
- Double click on cross-section guide image to re-show initial set cross section endpoints state
- UI/UX cleanup and bugfixing in CSVEditor
- Remove unused/obsolete drawing options


ver 1.2.0
23/04/2015

- Multicore solve for dBSeaModes, dBSeaPE and dBSeaRay
- UI feedback during threaded operations
- Check website for newer versions on startup
- Draggable panel dividers on dBSea main form and on CSVEditor
- Duplicate probe button
- Array tool for creating probes/sources
- Installer no longer requires uninstallation of previous version
- Fix for importing very large bathymetry files
- Controls for results contours linewidth and colour
- Added depth and range oversampling controls for dBSeaPE
- dBSeaPE fallback analytical starter if dBSeaModes fails
- Fix for displayZind issue when ldMax selected
- Added support for network Hasp keys
- Added unlicensed running mode


ver 1.1.1
23/02/2015

- Fix memory leaks due to colour assigns
- Converting user visible levels to user output level type
- Export all z layer levels as ESRI file series
- Getting correct output level type throughout all forms
- Change GL image export to account for RGBA format
- Improved entering data and setting limits on LevelLimitsDialog
- Reading back NaNs from saved bathymetry
- Simplify scenario info printing
- Fix level contours for NaN in 2D results draw
- Change installer to 64 bit install path
- Fix for mousewheel/key changing cells in scenario selector
- More checks on input SSP table data
- Bugfix for adding contributions from multiple sources
- Reducing solver memory assigns
- Fix for precision on dBSeaModes matrix determinant routine on WIN64
- Improved error messaging from solvers
- Export probe depth levels to clipboard
- Fix for errors in spectrum on duplicating source
- Fix for errors in SSP on duplicating scenario


ver 1.1.0
5/01/2015

- 64 bit version
- Added click and drag for moving source positions
- Added user defined equation solver option

ver 1.0.5
12/12/2014

- Improved regional support
- Enable comma as decimal separator
- Fix for rounding and truncating of nan and inf
- Improved lat/lng to UTM conversion
- Improved exception logging
- Added crest factor column
- Improved sizing of main windows for different screen sizes


ver 1.0.4
28/11/2014

- Implementing separate low-memory sound level storage for moving sources with arbitrarily large number of calculation positions
- Updated help files
- Changed location of acoustLib.db to user local dir
- Calculation only extends to last water distance
- More info returned on HASP/license errors
- Added meta info to database
- Fixed scrollbar behaviour in main window/frames
- Clarified 'threshold' and 'weighting' text
- Fixed formatting of stringgrids in VCL
- Better formatting of 'Level = ...' text
- Added event log and log viewer
- CSVEditor xyz file export routine
- CSVEditor can get z coordinates direct from points or from database field 
- CSVEditor messages when imported contour z coordinates differ
- Taskbar progress indicator


ver 1.0.3
11/11/2014
- Mitigation in time series calculations


ver 1.0.2
07/11/2014

- Cleaning up solver calls
- Passing save file version number
- Fix bug on opening via commandline/launching from .uwa file
- Removing unnecessary caching 
- Simplified internal type system
- Speed improvements to exclusion zone calculation
- Speed improvements to last water distance from source calculation


ver 1.0.1
27/10/2014

- SPLpk and SPLpktopk time series calculations
- CSVEditor shapefile/xyz file import
- CSVEditor ui responds to cancel
- Improved lat/lng to UTM conversion
- NOAA criteria in database
- Separate unit for speed of sound profile
- Crest factor added to source freq domain to allow NOAA non-impulsive dBpeak level output
- Option to change CSV export delimiter
- Cannot delete seabed layer 0
