unit StatefulDownsample;

interface

uses
    system.sysutils, system.classes, system.math, system.types,
    fmx.dialogs, fmx.stdctrls, fmx.forms,
    smartPointer, baseTypes, utextendedx87, helperFunctions,
    DSP, statefulbiquadfilter, FilterDef;

type
    TStatefulDownsample = class(TObject)
    private
        filter: ISmart<TStatefulBiquadFilter>;
        tmp0: TDoubleDynArray;
    public
        downsampleBy: integer;
        constructor create(const fs: double; const aDownsampleBy, inLength: integer);

        procedure downsampleByN(const x, result: TDoubleDynArray);
        class function antialiasForDownsampleBy(const fs: double; const aDownsampleBy: integer): TFilterDef;
    end;

implementation

class function TStatefulDownsample.antialiasForDownsampleBy(const fs: double; const aDownsampleBy: integer): TFilterDef;
begin
    result := TDSP.LPF(fs,fs * (1.0 / aDownsampleBy) * 0.85, 0.707);
end;

constructor TStatefulDownsample.create(const fs: double; const aDownsampleBy, inLength: integer);
begin
    inherited create;

    filter := TSmart<TStatefulBiquadFilter>.create( TStatefulBiquadFilter.create(antialiasForDownsampleBy(fs, aDownsampleBy)) );
    downsampleBy := aDownsampleBy;
    setlength(tmp0, inLength);
end;

procedure TStatefulDownsample.downsampleByN(const x, result: TDoubleDynArray);
var
    i: integer;
begin
    //tmp := filter.applyFilter3Times(x); //antialias before downsampling
    filter.applyFilterInPlace(x,0,tmp0); //antialias before downsampling
    for i := 0 to length(result) - 1 do
        result[i] := tmp0[i*downsampleBy];  //take every Nth sample
end;

end.
