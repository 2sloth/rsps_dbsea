unit MaintenanceStringParser;

interface
uses
    syncommons, eventlogging,
    system.threading, system.sysutils, system.Classes, system.StrUtils,
    xml.XMLIntf, xml.xmldom,
    vcl.forms, vcl.dialogs,
    generics.collections,
    winapi.windows, winapi.activex,
    system.UITypes, MSXML, ComObj,
    msxmldom, XMLDoc, rtti,
    HASPSecurity,
    helperFunctions,
    dBSeaConstants, passwordChecking, webGetHTTP,
    myRegistry, MCKLib, jsSpidermonkey, globalsUnit, baseTypes,
    OtlTaskControl, otlTask;

type
    TMaintenanceReturn = (kPass, kCouldNotParse, kBeforeStart, kAfterEnd, kWrongKey, kVersionNotAllowed);

    TMaintenanceStringParser = class(TObject)
    private
    {$IFDEF DEBUG}
    const
        DEBUG_KEY = '9998';
    {$ENDIF}
        class function  parse(const text: String): TDictionary<String, TValue>;
        class function  checkDates(const text: String):TMaintenanceReturn;
        class function  checkKeySerialNumber(const text: String): TMaintenanceReturn;
        class function  checkVersion(const text: string; out versionNumber: integer): TMaintenanceReturn;
        class function  checkDecryptedText(const text: String): TMaintenanceReturn;
        class function  saltPlusGUID(const salt: string): string;
        //class function  versionNumber(const text:String): integer;
    public
        class function  queryDB(const onTerminate: TOmniOnTerminatedFunction): IOmniTaskControl;
        class function  checkRegistryKeys: boolean;
        class function  maxVersionForKey(): integer;
    end;

implementation

class function  TMaintenanceStringParser.checkRegistryKeys: boolean;
var
    mdl: TMaintenanceDataList;
    md: TMaintenanceData;
begin
    result := false;

    mdl := reg.maintenanceData;
    try
        for md in mdl do
            if TMaintenanceStringParser.checkDecryptedText(js.decrypt(md.data, saltPlusGUID(md.salt))) = kPass then
                exit(true);
    finally
        mdl.free;
    end;
end;

class function TMaintenanceStringParser.saltPlusGUID(const salt: string): string;
begin
    result := salt + reg.getGUIDAlphaNumericOnly();
end;

class function TMaintenanceStringParser.queryDB(const onTerminate: TOmniOnTerminatedFunction): IOmniTaskControl;
    function generateSalt(const len: integer): string;
    begin
        result := '';
        while length(result) < len do
            result := result + Chr(ord('a') + Random(26));
    end;
var
    salt: string;
    serialNumber: string;
begin
    if TWebGetHTTP.internetConnectionExists then
        licenseDBCheckStatus := kInProgress
    else
        licenseDBCheckStatus := kNoInternet;

    salt := generateSalt(10);

    serialNumber := {$IFDEF DEBUG} DEBUG_KEY; {$ELSE} theHASPSecurity.keySerialNumber; {$ENDIF}
    if length(serialNumber) = 0 then
    begin
        licenseDBCheckStatus := kFinished;
        exit;
    end;

    //ITask(TTask.create(procedure
    result := CreateTask( procedure(const task: IOmniTask)
        function hasMatchWithExisting(const decrypted, serialNumber: string): boolean;
        var
            mdl: TMaintenanceDataList;
            md: TMaintenanceData;
        begin
            result := false;
            mdl := reg.maintenanceData;
            try
                for md in mdl do
                    if serialNumber.Equals(md.serialNumber) and
                        (AnsiCompareText(decrypted, js.decrypt(md.data, saltPlusGUID(md.salt))) = 0) then
                    exit(true);
            finally
                mdl.Free;
            end;
        end;

        function maintenanceCheckMessage(const maintenanceCheck: TMaintenanceReturn): string;
        begin
            result := '';
            case maintenanceCheck of
                kPass: ; //all good
                kCouldNotParse: result := '1260: Could not read data from license server';
                kBeforeStart: result := '1261: Current time is before start of server license period';
                kAfterEnd: result := '1262: Current time is after end of server license period';
                kWrongKey: result := '1263: server license is for wrong key';
                kVersionNotAllowed: result := '1264: Server license is not valid for this version of dBSea';
            end;
        end;
    var
        msg: string;
        text: string;
        encrypted : String;
        maintenanceCheck: TMaintenanceReturn;
    begin
        CoInitialize(nil); //need to call this in any thread that uses OLE stuff
        try
            encrypted := TWebGetHTTP.checkDB(serialNumber, saltPlusGUID(salt));

            if (encrypted.IsEmpty) or encrypted.Equals('Found no license with that serial number') then
            begin
                if not theHASPSecurity.keySerialNumber.IsEmpty then
                    showmessage('The license server returned no data for key number ' + theHASPSecurity.keySerialNumber);
                licenseDBCheckStatus := kFinished;
            end
            else
            begin
                text := js.decrypt(encrypted, saltPlusGUID(salt));
                maintenanceCheck := TMaintenanceStringParser.checkDecryptedText(text);

                if not hasMatchWithExisting(text, serialNumber) then
                begin
                    eventLog.log('New data returned from license server');
                    reg.writeMaintenanceValue(encrypted, salt, serialNumber);
                end;

                //set license check status finished, as the web query is also waits for this thread
                licenseDBCheckStatus := kFinished;
                if (maintenanceCheck <> kPass) then
                begin
                    //wait for hasp key (should already be done), so we know whether to show an error message or not
                    while not theHASPSecurity.hasPerformedInitialCheck do
                        sleep(20);

                    if not theHASPSecurity.hardwarekeyPresentForCurrentVersion() then
                    begin
                        msg := maintenanceCheckMessage(maintenanceCheck);
                        TThread.Queue(nil, procedure begin Showmessage(msg); end);
                    end;
                end;
            end;
        finally
            CoUninitialize;
        end;
    end).OnTerminated(onTerminate).run;//).start;
end;

class function TMaintenanceStringParser.checkDecryptedText(const text: String): TMaintenanceReturn;
var
    verNo: integer;
begin
    if text.IsEmpty then exit(kCouldNotParse);
    result := TMaintenanceStringParser.checkKeySerialNumber(text);
    if result <> kPass then exit;
    result := TMaintenanceStringParser.checkDates(text);
    if result <> kPass then exit;
    result := TMaintenanceStringParser.checkVersion(text,verNo);
end;

class function TMaintenanceStringParser.checkVersion(const text: string; out versionNumber: integer): TMaintenanceReturn;
var
    dict: TDictionary<String,TValue>;
    val: TValue;
begin
    versionNumber := -1;
    dict := TMaintenanceStringParser.parse(text);
    if not assigned(dict) then exit(kCouldNotParse);

    if not dict.TryGetValue('version', val) then exit(kCouldNotParse);
    versionNumber := THASPSecurity.VersionAsInteger(val.AsString);
    if versionNumber < THASPSecurity.VersionAsInteger(majorVersion(application.ExeName)) then
        exit(kVersionNotAllowed);

    result := kPass;
end;

class function TMaintenanceStringParser.checkKeySerialNumber(const text: String): TMaintenanceReturn;
var
    dict: TDictionary<String,TValue>;
    val: TValue;
begin
    dict := TMaintenanceStringParser.parse(text);
    if not assigned(dict) then exit(kCouldNotParse);

    if not dict.TryGetValue('key', val) then exit(kCouldNotParse);

    if val.AsString <> {$IFDEF DEBUG} DEBUG_KEY {$ELSE} theHASPSecurity.keySerialNumber {$ENDIF} then
        exit(kWrongKey);

    result := kPass;
end;

class function TMaintenanceStringParser.checkDates(const text: String): TMaintenanceReturn;
var
    dict: TDictionary<String,TValue>;
    val: TValue;
begin
    dict := TMaintenanceStringParser.parse(text);
    if not assigned(dict) then exit(kCouldNotParse);

    if not dict.TryGetValue('start', val) then exit(kCouldNotParse);
    if val.AsExtended > now then exit(kBeforeStart);

    if not dict.TryGetValue('end', val) then exit(kCouldNotParse);
    if val.AsExtended < now then exit(kAfterEnd);

    result := kPass;
end;

class function TMaintenanceStringParser.parse(const text: String): TDictionary<String, TValue>;
const
    parentNodeName = 'dBSeaMaintenance';
var
    xml: IXMLDOMDocument;
    LDoc: IXMLDocument;
    //childnode: IDOMNode;
    node: IXMLNode;
    i: integer;
    date: TDate;
    FmtStngs: TFormatSettings;
begin
    xml := CreateOleObject('Microsoft.XMLDOM') as IXMLDOMDocument;
    xml.async := False;
    xml.loadXML(text);
    if xml.parseError.errorCode <> 0 then
        Exit(nil);

    result := TDictionary<String,TValue>.create;

    LDoc := TXMLDocument.Create(application);
    LDoc.LoadFromXML(text);

    if ldoc.ChildNodes.First.NodeName <> parentNodeName then
        exit(nil);

    for i := 0 to ldoc.ChildNodes[0].ChildNodes.Count - 1 do
    begin
        node := ldoc.ChildNodes[0].ChildNodes[i];

        if (node.NodeName = 'key') or (node.nodename = 'version') then
            result.add(node.NodeName, TValue.FromVariant( node.NodeValue))
        else if (node.NodeName = 'start') or (node.NodeName = 'end') then
        begin
            try
                FmtStngs := TFormatSettings.Create(LOCALE_USER_DEFAULT);
                FmtStngs.DateSeparator := '/';
                FmtStngs.ShortDateFormat := 'dd/mm/yyyy';
                date := StrToDate(node.NodeValue, FmtStngs);
                result.add(node.NodeName, TValue.FromVariant(date));
            except
                on e: EconvertError do exit(nil);
            end;
        end;
    end;
end;

class function TMaintenanceStringParser.maxVersionForKey(): integer;
var
    mdl: TMaintenanceDataList;
    md: TMaintenanceData;
    text: string;
    verNo: integer;
begin
    result := 0;

    mdl := reg.maintenanceData();
    try
        for md in mdl do
        begin
            text := js.decrypt(md.data, saltPlusGUID(md.sSalt));
            if (checkDecryptedText(text) = kPass) then
            begin
                checkVersion(text, verNo);
                if verNo > result then
                    result := verNo;
            end;
        end;
    finally
        mdl.free;
    end;
end;

end.
