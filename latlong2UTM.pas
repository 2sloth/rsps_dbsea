unit latlong2UTM;

interface

uses system.math, system.sysutils, system.types,
    helperFunctions, baseTypes;

type
    TLatLong2UTM = class(TObject)
    private
    const
        a = 6378137.0; // WGS84
        f = 1 / 298.2572236; // WGS84
        k0 = 0.9996;
        drad = PI / 180;

        zoneLetters: array [0 .. 19] of char = ('C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X');

        class function internalCalc(const latDeg, lngDeg: double; const refZone: integer): TEastNorth;
    public
    const
        mPerDegreeLongAroundEquator = 40030.0 * 1.0e3 / 360;
        mPerDegreeLatAroundPoles = 40008.0 * 1.0e3 / 360;

        class function getUTMString(const en: TEastNorth): string;
        class function getLongZone(const lngDeg: double): integer; overload;
        class function getLongZone(const utmString: string): integer; overload;
        class function getLatZone(const latDeg: double): string; overload;
        class function getLatZone(const utmString: string): string; overload;
        class function latlong2UTM(const latlongDeg: TEastNorth; const refZone: integer = -1): TEastNorth;
        class function latlong2UTM_complicated(const latlongDeg: TEastNorth; const refZone: integer = -1): TEastNorth;
        class function isValidUTMZone(const s: string): boolean;
        class function UTM2LatLong(const longZone: integer; const latZone: string; const en: TEastNorth): TEastNorth;
        class function UTM2LatLong_complicated(const longZone: integer; const latZone: string; const eastNorth: TEastNorth): TEastNorth;
    end;

    TLatLngJava = class(TObject)
    private
      const
        b = 6356752.314;
        a = 6378137;
        e = 0.081819191;
        e1sq = 0.006739497;
        k0 = 0.9996;
        southernHemisphere = 'ACDEFGHJKLM';
    var
        arc,mu,ei,ca,cb,cc,cd,n0,r0,_a1,dd0,t0,Q0,lof1,lof2,lof3,_a2,phi1,fact1,fact2,fact3,fact4,zoneCM,_a3: double;

        procedure setVariables;
    public
        northing, easting: double;
        function convertUTMToLatLong(const longZone: integer; const latZone: string; const eastNorth: TEastNorth): TEastNorth;
        class function isSouthernHemisphere(const latZone: string): boolean;
    end;

implementation

class function TLatLong2UTM.latlong2UTM_complicated(const latlongDeg: TEastNorth; const refZone: integer = -1): TEastNorth;
begin
    result := TLatLong2UTM.internalCalc(latlongDeg.latitude, latlongDeg.longitude, refZone);
end;

class function TLatLong2UTM.latlong2UTM(const latlongDeg: TEastNorth; const refZone: integer = -1): TEastNorth;
var
    utmz, zcm, cosPhi: double;
begin
    // the zone normally changes every 6 degrees, and our easting values reset.
    // If we want to force reference to a certain zone, then put refZone > 1
    if refZone >= 1 then
        utmz := refZone
    else
        utmz := TLatLong2UTM.getLongZone(latlongDeg.longitude);
    zcm := 3 + 6 * (utmz - 1) - 180;

    cosPhi := cos(latlongDeg.latitude * drad);
    result.easting := cosPhi * (latlongDeg.longitude - zcm) * mPerDegreeLongAroundEquator + 5e5;

    result.northing := latlongDeg.latitude * mPerDegreeLatAroundPoles;
    if (result.northing < 0) then
        result.northing := 10000000 + result.northing;
end;

class function TLatLong2UTM.internalCalc(const latDeg, lngDeg: double; const refZone: integer): TEastNorth;
var
    b, e, N, T, C, phi, utmz, e0sq, M, zcm, AA, sinPhi, cosPhi, tanPhi, ee: double;
begin
    b := a * (1 - f); // polar axis
    ee := 1 - sqr(b / a);
    e := sqrt(ee); // eccentricity

    phi := latDeg * drad;
    // the zone normally changes every 6 degrees, and our easting values reset.
    // If we want to force reference to a certain zone, then put refZone > 1
    // NB the easting gets inaccurate as you move away from the correct zone
    if refZone > 0 then
        utmz := refZone
    else
        utmz := TLatLong2UTM.getLongZone(lngDeg);
    zcm := 3 + 6 * (utmz - 1) - 180;

    e0sq := ee / (1 - ee);
    system.SineCosine(phi,sinPhi,cosPhi);
    N := a / sqrt(1 - sqr(e * sinPhi));
    tanPhi := tan(phi);
    T := sqr(tanPhi);
    C := e0sq * sqr(cosPhi);
    AA := (lngDeg - zcm) * drad * cosPhi;
    M := phi * (1 - ee * (1 / 4 + ee * (3 / 64 + 5 * ee / 256)));
    M := M - sin(2 * phi) * (ee * (3 / 8 + ee * (3 / 32 + 45 * ee / 1024)));
    M := M + sin(4 * phi) * (sqr(ee) * (15 / 256 + ee * 45 / 1024));
    M := M - sin(6 * phi) * (sqr(ee) * ee * (35 / 3072));
    M := M * a;

    result.easting := k0 * N * AA * (1 + sqr(AA) * ((1 - T + C) / 6 + sqr(AA) * (5 - 18 * T + sqr(T) + 72 * C - 58 * e0sq) / 120));
    result.easting := result.easting + 500000; // the center of zone utmz

    result.northing := k0 * (M + N * tanPhi * (sqr(AA) * (1 / 2 + sqr(AA) * ((5 - T + 9 * C + 4 * sqr(C)) / 24 + sqr(AA) * (61 - 58 * T + sqr(T) + 600 * C - 330 * e0sq) / 720))));
    if (result.northing < 0) then
        result.northing := 10000000 + result.northing;
end;

class function TLatLong2UTM.UTM2LatLong(const longZone: integer; const latZone: string; const en: TEastNorth): TEastNorth;
var
    easting, northing, zcm, cosPhi: double;
    hemisphereSouth: boolean;
begin
    easting := en.easting;
    northing := en.northing;
    hemisphereSouth := TLatLngJava.isSouthernHemisphere(latZone);

    if hemisphereSouth then
        northing := northing - 10000000;

    result.latitude := northing / mPerDegreeLatAroundPoles;
    cosPhi := cos(result.latitude * drad);

    zcm := 3 + 6 * (longZone - 1) - 180;
    result.longitude := (easting - 5e5) / (mPerDegreeLongAroundEquator * cosPhi) + zcm;
end;

class function TLatLong2UTM.UTM2LatLong_complicated(const longZone: integer; const latZone: string; const eastNorth: TEastNorth): TEastNorth;
var
    llj: TLatLngJava;
begin
    llj := TLatLngJava.create;
    try
        result := llj.convertUTMToLatLong(longZone, latZone, eastNorth);
    finally
        llj.free;
    end;
end;

class function TLatLong2UTM.getLongZone(const lngDeg: double): integer;
begin
    result := 1 + safefloor((lngDeg + 180) / 6);
end;

class function TLatLong2UTM.getLatZone(const utmString: string): string;
var
    s: string;
begin
    s := trim(utmString);
    if (length(s) <> 2) and (length(s) <> 3) then
        exit('')
    else
        result := UpperCase(s[length(s)]);
end;

class function TLatLong2UTM.getLongZone(const utmString: string): integer;
var
    s: string;
begin
    s := trim(utmString);
    if TLatLong2UTM.isValidUTMZone(s) then
    begin
        s := trim(utmString)[1];
        if trim(utmString).length = 3 then
            s := s + trim(utmString)[2];
        if isint(s) then
            exit(strtoint(s));
    end;

    result := -1;
end;

class function TLatLong2UTM.getLatZone(const latDeg: double): string;
var
    I: integer;
begin
    if abs(latDeg) <= 80.0 then
    begin
        I := safefloor((latDeg + 80) / 8);
        if (I >= 0) and (I < 20) then
            result := zoneLetters[I];
    end
    else
        result := '?';
end;

class function TLatLong2UTM.getUTMString(const en: TEastNorth): string;
begin
    result := tostr(getLongZone(en.longitude)) + getLatZone(en.latitude);
end;

class function TLatLngJava.isSouthernHemisphere(const latZone: string): boolean;
begin
    result := southernHemisphere.indexOf(latZone) >= 0;
end;

function TLatLngJava.convertUTMToLatLong(const longZone: integer; const latZone: string; const eastNorth: TEastNorth): TEastNorth;
var
    hemisphereSouth: boolean;
begin
    easting := eastNorth.easting;
    northing := eastNorth.northing;
    hemisphereSouth := isSouthernHemisphere(latZone);
    result.latitude := 0.0;
    result.longitude := 0.0;

    if hemisphereSouth then
        northing := 10000000 - northing;
    setVariables();
    result.latitude := 180 * (phi1 - fact1 * (fact2 + fact3 + fact4)) / PI;

    if (longZone > 0) and (longZone <= 60) then
        zoneCM := 6 * longZone - 183.0
    else
        zoneCM := 3.0;

    result.longitude := zoneCM - _a3;
    if hemisphereSouth then
        result.latitude := -result.latitude;
end;

procedure TLatLngJava.setVariables();
var
    _ee, eiei, sqrtEi, sinPhi1, cosPhi1: double;
begin
    arc := northing / k0;
    _ee := sqr(e);
    mu := arc / (a * (1 - _ee / 4.0 - 3 * sqr(_ee) / 64.0 - 5 * sqr(_ee)*_ee / 256.0));

    sqrtEi := sqrt(1 - _ee);
    ei := (1 - sqrtEi) / (1 + sqrtEi);

    eiei := sqr(ei);
    ca := 3 * ei / 2 - 27 * eiei*ei / 32.0;

    cb := 21 * eiei / 16 - 55 * sqr(eiei) / 32;
    cc := 151 * eiei*ei / 96;
    cd := 1097 * sqr(eiei) / 512;
    phi1 := mu + ca * sin(2 * mu) + cb * sin(4 * mu) + cc * sin(6 * mu) + cd
      * sin(8 * mu);

    system.SineCosine(phi1, sinPhi1, cosPhi1);
    n0 := a / sqrt(1 - sqr(e * sinPhi1));

    r0 := a * (1 - _ee) / power(1 - sqr(e * sinPhi1), 3 / 2.0);
    fact1 := n0 * tan(phi1) / r0;

    _a1 := 500000 - easting;
    dd0 := _a1 / (n0 * k0);
    fact2 := sqr(dd0) / 2;

    t0 := sqr(tan(phi1));
    Q0 := e1sq * sqr(cosPhi1);
    fact3 := (5 + 3 * t0 + 10 * Q0 - 4 * Q0 * Q0 - 9 * e1sq) * power(dd0, 4) / 24;

    fact4 := (61 + 90 * t0 + 298 * Q0 + 45 * t0 * t0 - 252 * e1sq - 3 * Q0
      * Q0)
      * power(dd0, 6) / 720;

    lof1 := _a1 / (n0 * k0);
    lof2 := (1 + 2 * t0 + Q0) * power(dd0, 3) / 6.0;
    lof3 := (5 - 2 * Q0 + 28 * t0 - 3 * sqr(Q0) + 8 * e1sq + 24 * sqr(t0))
      * power(dd0, 5) / 120;
    _a2 := (lof1 - lof2 + lof3) / cosPhi1;
    _a3 := _a2 * 180 / PI;
end;

class function TLatLong2UTM.isValidUTMZone(const s: string): boolean;
    function cMatch(const c: char): boolean;
    var
        q: char;
    begin
        result := false;
        for q in zoneLetters do
            if c = q then
                exit(true);
    end;
var
    c: char;
    i: string;
begin
    result := false;
    if not ((s.Length = 2) or (s.Length = 3)) then exit();

    c := char(UpperCase(s[s.length])[1]);
    if not cMatch(c) then exit();
    if not isInt(s[1]) then exit;
    i := s[1];
    if (s.length = 3) and not isInt(s[2]) then exit;

    if s.length = 3 then
        i := i + s[2];

    if (toint(i) < 1) or (toint(i) > 60) then exit;

    result := true;
end;

end.

