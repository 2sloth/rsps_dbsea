unit HistogramCalc;

interface

uses math, types, helperfunctions, mcklib, baseTypes;

type

    TMyHistogram = class(TObject)
        class procedure calcHistogramBins(const inputData : TSingleDynArray; const binSpacing : integer; out binCenters : TSingleDynArray; out binCounts : TIntegerDynArray; const useGivenLimits : boolean = false; const aMax : single = nan; const aMin : single = nan); overload;
        class procedure calcHistogramBins(const inputData : TDoubleDynArray; const binSpacing : integer; out binCenters : TDoubleDynArray; out binCounts : TIntegerDynArray; const useGivenLimits : boolean = false; const aMax : double = nan; const aMin : double = nan); overload;
    end;

implementation

class procedure TMyHistogram.calcHistogramBins(const inputData : TSingleDynArray; const binSpacing : integer; out binCenters : TSingleDynArray; out binCounts : TIntegerDynArray; const useGivenLimits : boolean = false; const aMax : single = nan; const aMin : single = nan);
var
    doubleBinCenters : TDoubleDynArray;
begin
    TMyHistogram.calcHistogramBins(inputData.toDoubleArray,
                                   binSpacing,
                                   doubleBinCenters,
                                   binCounts,
                                   useGivenLimits,aMax,aMin);
    binCenters := doubleBinCenters.toSingleArray;
end;

class procedure TMyHistogram.calcHistogramBins(const inputData : TDoubleDynArray; const binSpacing : integer; out binCenters : TDoubleDynArray; out binCounts : TIntegerDynArray; const useGivenLimits : boolean = false; const aMax : double = nan; const aMin : double = nan);
var
    mx,mn,d : double;
    i, p : integer;
    binFeet : TDoubleDynArray;
begin
    if useGivenLimits then
    begin
        mx := aMax;
        mn := aMin;
    end
    else
    begin
        mx := nan;
        mn := nan;
        for d in inputData do
            lesserGreater(mn,mx,d);
    end;

    if isnan(mn) or isnan(mx) or isnan(binSpacing) or (binSpacing = 0) or (((ceilx(mx,binSpacing) - floorx(mn,binSpacing)) div binSpacing) = 0) then
    begin
        setlength(binCenters,0);
        setlength(binCounts,0);
        exit;
    end;

    setlength(binCounts, saferound((ceilx(mx,binSpacing) - floorx(mn,binSpacing)) div binSpacing));
    setlength(binFeet,   saferound((ceilx(mx,binSpacing) - floorx(mn,binSpacing)) div binSpacing));
    setlength(binCenters,   ((ceilx(mx,binSpacing) - floorx(mn,binSpacing)) div binSpacing));
    for I := 0 to length(binFeet) - 1 do
    begin
        binFeet[i] := floorx(mn,binSpacing) + binspacing * i;
        binCenters[i] := floorx(mn,binSpacing) + binspacing * (i + 0.5);
    end;

    {gather data into bins}
    for d in inputData do
        if not isNaN(d) then
        begin
            p := binFeet.find(d);
            if (p <> -1) then inc(binCounts[p]);
        end;
end;

end.
