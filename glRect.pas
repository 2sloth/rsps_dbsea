unit glRect;

interface

uses basetypes;

  type
      TglColor = record
          r, g, b, alpha: double;

          procedure setColor(const ar, ag, ab, aalpha: double);
      end;

      TglRect = record
          ul, ur, ll, lr: TFPoint3;
          textureUl, textureUr, textureLl, textureLr: TFPoint;
          ulColor, urColor, llColor, lrColor: TglColor;
          alpha: double;

          procedure setAllPointsZ(const z: double);
          procedure setAllPointsColor(const ar, ag, ab, aalpha: double);
      end;

implementation

procedure TglColor.setColor(const ar,ag,ab,aalpha: double);
begin
    r := ar;
    g := ag;
    b := ab;
    alpha := aalpha;
end;


procedure TglRect.setAllPointsColor(const ar,ag,ab,aalpha: double);
begin
    self.ulColor.setColor(ar,ag,ab,aalpha);
    self.urColor.setColor(ar,ag,ab,aalpha);
    self.llColor.setColor(ar,ag,ab,aalpha);
    self.lrColor.setColor(ar,ag,ab,aalpha);
end;

procedure TglRect.setAllPointsZ(const z: double);
begin
    ul.z := z;
    ur.z := z;
    ll.z := z;
    lr.z := z;
end;

end.
