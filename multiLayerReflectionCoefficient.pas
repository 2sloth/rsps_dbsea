unit multiLayerReflectionCoefficient;

interface

uses system.math, system.types, system.classes, system.Generics.collections,
    mcomplex, seafloorScheme, material, globalsUnit;

type
    TSeafloorReflectionLayer = class(TObject)
    public
        zb: double; // the local depth where this layer is valid
        rho, c, h, attenuation: double;
        k: double;
        phi, theta, Z: TComplex;

        constructor create(const material: TMaterial; const ah, azb: double);
    end;

    TSeafloorReflectionLayers = TObjectList<TSeafloorReflectionLayer>;
    //TSeafloorReflectionLayersMatrix = array of TSeafloorReflectionLayers;

    TMultiLayerReflectionCoefficient = class(TObject)
        class function  calcR(const f, theta: double; const layers: TSeafloorReflectionLayers): TComplex;
        class procedure layersFromSeafloorScheme(const scheme: TSeafloorScheme; const depth: double; const result: TSeafloorReflectionLayers);
        //class procedure removeZeroThicknessLayers(var layers: TSeafloorReflectionLayers);
        //class function layersMatrixFromSeafloorScheme(const scheme: TSeafloorScheme; const depths: TDoubleDynArray): TSeafloorReflectionLayersMatrix;
    end;

implementation


constructor TSeafloorReflectionLayer.create(const material: TMaterial; const ah, azb: double);
begin
    inherited create;

    self.rho           := material.rho;
    self.c             := material.c;
    self.attenuation   := material.attenuation;
    self.h             := ah;
    self.zb            := azb;
end;

class procedure TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(const scheme: TSeafloorScheme; const depth: double; const result: TSeafloorReflectionLayers);
var
    layer: TSeafloorLayer;
    i, ind: integer;
begin
    result.clear;// := TSeafloorReflectionLayers.create(true);

    if scheme.isReferencedFromSeafloor then
    begin
        result.add(TSeafloorReflectionLayer.create(water, depth, depth));
        for layer in scheme.layers do
        begin
            i := scheme.layers.IndexOf(layer);
            result.add(TSeafloorReflectionLayer.create(layer.material,scheme.layerThickness(i),depth));
        end;
    end
    else
    begin
        layer := scheme.layers.Last;
        if depth >= layer.depth then
        begin
            result.add(TSeafloorReflectionLayer.create(water, depth, depth));
            result.add(TSeafloorReflectionLayer.create(layer.material,scheme.layerThickness(scheme.layers.Count - 1),depth));
        end
        else
        begin
            layer := scheme.getLayerAtDepthBelowSurface(depth,depth);
            ind := scheme.layers.IndexOf(layer);
            result.add(TSeafloorReflectionLayer.create(water, depth, depth));
            result.add(TSeafloorReflectionLayer.create(layer.material,scheme.layers[ind + 1].depth - depth,depth));
            for i := ind + 1 to scheme.layers.Count - 1 do
                result.add(TSeafloorReflectionLayer.create(layer.material,scheme.layerThickness(i),depth));
        end;
    end;

    for I := result.count - 1 downto 1 do
        if (result[i].h = 0) then
            result.Delete(i);
end;

//class function TMultiLayerReflectionCoefficient.layersMatrixFromSeafloorScheme(const scheme: TSeafloorScheme; const depths: TDoubleDynArray): TSeafloorReflectionLayersMatrix;
//var
//    i: integer;
//begin
//    setlength(result, length(depths));
//    for i := 0 to length(depths) - 1 do
//        result[i] := TMultiLayerReflectionCoefficient.layersFromSeafloorScheme(scheme, depths[i]);
//end;

class function TMultiLayerReflectionCoefficient.calcR(const f, theta: double; const layers: TSeafloorReflectionLayers): TComplex;
    function RCoefficient2Layers(const l1, l2: TSeafloorReflectionLayer): TComplex;
    begin
        if (complexSin(l2.theta).discriminant = 0) or (complexSin(l1.theta).discriminant = 0) then
            result := 1 // ?
        else if (l2.phi * l2.c / complexSin(l2.theta).discriminant + l1.phi * l1.c / complexSin(l1.theta)).discriminant = 0 then
            result := 1 // ?
        else
            result := (l2.phi * l2.c / complexSin(l2.theta) - l1.phi * l1.c / complexSin(l1.theta)) /
              (l2.phi * l2.c / complexSin(l2.theta) + l1.phi * l1.c / complexSin(l1.theta));
    end;

var
    i, j, m: integer;
    R: Array of array of TComplex;
    totalReflectionAtLayerBoundary: integer;
    alpha: double;
begin
    //for R matrix, i < j always
    m := layers.count;
    if m = 0 then exit(nanComplex);
    if m = 1 then exit(nanComplex);

    setlength(R,m,m);
    totalReflectionAtLayerBoundary := -1;
    for I := 0 to m - 1 do
    begin
        layers[i].k := 2*pi*f / layers[i].c;

        if i = 0 then
            layers[i].theta := theta
        else
        begin
            //test for total internal reflection
            if totalReflectionAtLayerBoundary < 0 then
                if layers[i - 1].c < layers[i].c then //can only occur if entering higher speed layer
                    if layers[i - 1].theta.r <= arccos(layers[i - 1].c / layers[i].c) then
                        totalReflectionAtLayerBoundary := i;

            //if totalReflectionAtLayerBoundary < 0 then
            begin
                if (complexCos(layers[i - 1].theta) * layers[i].k).discriminant = 0 then
                    layers[i].theta := layers[i - 1].theta
                else
                    layers[i].theta := SaferComplexARCCOS(layers[i - 1].k / layers[i].k * complexCos(layers[i - 1].theta));
            end;
        end;

        //if totalReflectionAtLayerBoundary < 0 then
        begin
            if (complexsin(layers[i].theta)).discriminant = 0 then
                layers[i].Z := NullComplex
            else
            begin
                layers[i].Z := makeComplex(layers[i].rho * layers[i].c,0) / complexsin(layers[i].theta);
                //not sure about the attenuation part of Z - check all this
                alpha := - f / layers[i].c * (power(10,layers[i].attenuation / 20));
                layers[i].Z := layers[i].Z * complexExp(makeComplex(0,alpha));
            end;

            layers[i].phi := layers[i].k * layers[i].h * complexsin(layers[i].theta);
        end;
    end;

    if totalReflectionAtLayerBoundary = 1 then
    begin
        //total internal reflection at water/first sediment interface
        exit(RCoefficient2Layers(layers[0],layers[1]));
    end
    else
    if totalReflectionAtLayerBoundary > 1 then
    begin
        //total internal reflection occurs at some point before final layer
        for i := 0 to totalReflectionAtLayerBoundary - 2 do
        begin
            j := i + 1;
            if (layers[j].Z + layers[i].Z).discriminant = 0 then
                R[i,j] := 1
            else
                R[i,j] := (layers[j].Z - layers[i].Z) / (layers[j].Z + layers[i].Z);
        end;

        i := totalReflectionAtLayerBoundary - 1;
        j := i + 1;
        R[i,j] := RCoefficient2Layers(layers[i],layers[j]);

        for i := totalReflectionAtLayerBoundary - 2 downto 0 do
        begin
            if (1 + R[i,i + 1] * R[i+1,totalReflectionAtLayerBoundary] * ComplexEXP(makeComplex(0,2)*layers[i+1].phi)).discriminant = 0 then
                R[i,totalReflectionAtLayerBoundary] := 1
            else
                R[i,totalReflectionAtLayerBoundary] := ( R[i,i+1] + R[i+1,totalReflectionAtLayerBoundary] * ComplexEXP(makeComplex(0,2)*layers[i+1].phi) ) /
                                                       ( 1 + R[i,i + 1] * R[i+1,totalReflectionAtLayerBoundary] * ComplexEXP(makeComplex(0,2)*layers[i+1].phi) );
        end;

        result := R[0,totalReflectionAtLayerBoundary];
        //showmessage(floattostr(result.r) + '    ' + floattostr(result.i));
    end
    else
    begin
        //no total internal reflections
        for I := 0 to m - 2 do
        begin
            j := i + 1;
            if (layers[j].Z + layers[i].Z).discriminant = 0 then
                R[i,j] := 1
            else
                R[i,j] := (layers[j].Z - layers[i].Z) / (layers[j].Z + layers[i].Z);
        end;

        //NB this also works for m = 2 as the for loop never gets called
        for i := m - 3 downto 0 do
            if (1 + R[i,i + 1] * R[i+1,m-1] * ComplexEXP(makeComplex(0,2)*layers[i+1].phi)).discriminant = 0 then
                R[i,m - 1] := 1
            else
                R[i,m - 1] := ( R[i,i+1] + R[i+1,m-1] * ComplexEXP(makeComplex(0,2)*layers[i+1].phi) ) /
                              ( 1 + R[i,i + 1] * R[i+1,m-1] * ComplexEXP(makeComplex(0,2)*layers[i+1].phi) );
        result := R[0,m - 1];
    end;

    if result.modulus > 1 then
        result := result / result.modulus;
end;


end.
