unit ShapeFileReader;

interface

uses system.types, system.classes, system.SysUtils,
    shpapi129, basetypes, WrappedFPointArray, GeoOverlay;

type

    TShapeFileReader = class(TObject)
    public
        class function overlayFromShapeFile(const filename: TFilename; const fieldNumber: integer; const getZFromPoints: boolean): TGeoOverlay;
    end;

implementation


class function TShapeFileReader.overlayFromShapeFile(const filename: TFilename;
                                                        const fieldNumber: integer;
                                                        const getZFromPoints: boolean): TGeoOverlay;
var
    i, j: integer;
    hSHP: PSHPInfo;
    psShape: PSHPObject;
    nShapeType, nEntities: LongInt;
    adfMinBound, adfmaxBound: Array [0 .. 3] Of Double;
    ansiPath: AnsiString;
    Path: PAnsichar;
    bZDiffersMsgShown: boolean;
    loop: TWrappedFPointArray;
begin
    ansiPath := AnsiString(filename);
    path := @ansiPath[1];
    hShp := SHPOpen(path,'rb');
    ShpGetInfo(hShp,@nEntities,@nShapeType,@adfMinBound,@adfMaxBound);

    bZDiffersMsgShown := false;
    result := TGeoOverlay.create('');
    result.sourceFile := filename;
    for i := 0 to nEntities - 1 do
    begin
        psShape := SHPReadObject(hSHP, i);
        loop := TWrappedFPointArray.create;

        setlength(loop.points, psShape^.nVertices);
        for j := 0 to psShape^.nVertices - 1 do
        begin
            loop.points[i].x := psShape^.padfX[j];
            loop.points[i].y := psShape^.padfY[j];
        end;
        SHPDestroyObject( psShape );

        result.loops.add(loop);
    end;
    SHPClose( hSHP );
end;

end.
