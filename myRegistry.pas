unit MyRegistry;

interface
uses
    syncommons,
    system.SysUtils, system.classes, system.StrUtils, system.UITypes, system.Types,
    system.win.registry,
    vcl.graphics, vcl.forms, vcl.dialogs,
    generics.collections,
    winapi.windows,
    dbSeaConstants, helperfunctions,
    passwordChecking, eventLogging, SmartPointer, baseTypes
    {$IFNDEF HASP_DIAGNOSTIC}, dBSeaColours{$ENDIF}
    ;

type

    TRegistryLock = (kNoError,kNoPreviousDateKey,kCannotDecodeDate,kClockTurnedBack);

    TMaintenanceData = class(TObject)
    const
        sData = 'data';
        sSalt = 'salt';
        sSerialNumber = 'serialNumber';
    var
        data, salt, serialNumber: string;
    end;
    TMaintenanceDataList = TObjectList<TMaintenanceData>;

    TMyRegistry = class (TObject)
    private
    const
        sRegistryUWAKey             = 'SOFTWARE\\Classes\\dBSea.projectFile\\shell\\open\\command'; //registry key to allow opening uwa files automatically
        sDidAskUpdateDBVersion      = 'DidAskUpdateDBVersion';
        sNotifiedNewVersionKey      = 'NotifiedNewVersion';
        sNotifiedNewVersionDateKey  = 'NotifiedNewVersionDate';
        sImperialUnitsKey           = 'ImperialUnits';
        sResultsColormapSmoothKey   = 'ResultsColormapSmooth';
        sResultsAlphaKey            = 'ResultsAlpha';
        sDefaultSavePathKey         = 'DefaultSavePath';
        sTempPathKey                = 'TempPath';
        sGLBackgroundColorKey       = 'GLBackgroundColor';
        sHaveAskedAssociateUWAKey   = 'HaveAskedAssociateUWA';
        sMultiCorePEKey             = 'MultiCorePE';
        sMultiCoreModesKey          = 'sMultiCoreModes';
        sMultiCoreRayKey            = 'MultiCoreRay';
        sRecentFilesKey             = 'RecentFiles';
        GUIDKey                     = 'MachineGuid';
        sMaintenanceKey             = 'Maintenance';
        sBarredKey                  = 'GLGesperrt';    //set to $ when the program is barred
        sPreviousDateKey            = 'GLLetzteZeit';    //encrypted date of last usage, to check whether the user has reset the system clock
        sLicenseEndDateKey          = 'GLSchlussZeit';    //encrypted date when the trial period is over
        sBarredValue1               = '$';
        sBarredValue2               = '*';
        sBarredValue3               = '%';
     var
        reg: TRegistry;

        class function  countMaintenanceKeys: integer;
     public
         constructor Create;
         destructor Destroy; override;

         procedure writeRegistry(const slRecentFiles: TStringlist; const dispFeet, ResultsColormapSmooth: boolean; const ResultsAlpha: double; const backgroundColor: TColor);
         procedure readRegistry(const addRecentFileProc: TProcedureWithString;
            out dispFeet, ResultsColormapSmooth: boolean;
            out ResultsAlpha: double;
            out backgroundColor: TColor);
         procedure checkUWAFileAssociation;
         procedure associateUWAFiles;
         procedure setDidAskToUpdateDB(const dbVersion: integer);
         function  getDidAskToUpdateDB(var dBVersion: integer): boolean;
         procedure setHaveNotifiedNewVersionAvailable(const version: String; const date: TDateTime);
         procedure getHaveNotifiedNewVersionAvailable(out version: String; out date: TDateTime);
         procedure writeMaintenanceValue(const encrypted, salt, serialNumber: string);
         procedure WriteRegistryLock(const i: TRegistryLock);
         function  CheckRegistryLock(): TRegistryLock;
         procedure CheckForTrialLicense(out sLicenseOrTrialExpiryDate: String; out success: boolean);
         function  CheckPreviousOpenDate: boolean;
         class procedure writeLicenseFile(const sFileFirstLine: string);
         class function  maintenanceData: TMaintenanceDataList;
         class procedure setGUID;
         class function  getGUID(): string;
         class function  getGUIDAlphaNumericOnly: string;
    end;
var
    reg: TMyRegistry;

implementation

uses globalsUnit;

procedure TMyRegistry.writeRegistry(const slRecentFiles: TStringlist;
    const dispFeet, ResultsColormapSmooth: boolean;
    const ResultsAlpha: double;
    const backgroundColor: TColor);
var
    i : integer;
begin
    if reg.OpenKey(sRegistryKey, true) then
    begin
        reg.WriteBool(sImperialUnitsKey,dispFeet);
        reg.WriteBool(sResultsColormapSmoothKey,ResultsColormapSmooth);
        reg.WriteFloat(sResultsAlphaKey,resultsAlpha);
        reg.WriteString(sDefaultSavePathKey,DefaultSavePath);
        reg.WriteString(sTempPathKey,TempPath);
        reg.WriteString(sGLBackgroundColorKey,colortostring(BackgroundColor));
        reg.WriteBool(sHaveAskedAssociateUWAKey,haveAskedAssociateUWA);
        reg.WriteBool(sMultiCorePEKey,bMultiCorePE);
        reg.WriteBool(sMultiCoreModesKey,bMultiCoreModes);
        reg.WriteBool(sMultiCoreRayKey,bMultiCoreRay);

        for i := 0 to slRecentFiles.Count - 1 do
            reg.WriteString(sRecentFilesKey + tostr(i),slRecentFiles[i]);
    end;
    reg.CloseKey;
end;

procedure TMyRegistry.readRegistry(const addRecentFileProc: TProcedureWithString;
    out dispFeet, ResultsColormapSmooth: boolean;
    out ResultsAlpha: double;
    out backgroundColor: TColor);
var
    i : integer;
begin
    dispFeet := false;
    ResultsColormapSmooth := false;
    ResultsAlpha := 1;
    backgroundColor := {$IFNDEF HASP_DIAGNOSTIC}TDBSeaColours.darkBlue{$ELSE}0{$ENDIF};

    {try open the dBSea key. if it doesn't work, do nothing}
    if reg.OpenKey(sRegistryKey, false) then
    begin
        {must be within the correct key to call ValueExists}
        if reg.ValueExists(sResultsColormapSmoothKey) then
            resultsColormapSmooth := reg.readBool(sResultsColormapSmoothKey);
        if reg.ValueExists(sResultsAlphaKey) then
        begin
            {Setting the Alpha to too low a value would mean you can't see the results
            at all which could be confusing, so put a lower limit}
            if reg.ReadFloat(sResultsAlphaKey) < 0.15 then
                resultsAlpha := 0.15
            else
                resultsAlpha := reg.ReadFloat(sResultsAlphaKey);
        end;
        if reg.ValueExists(sImperialUnitsKey) then
            dispFeet := reg.ReadBool(sImperialUnitsKey);
        if reg.ValueExists(sDefaultSavePathKey) and directoryExists(reg.ReadString(sDefaultSavePathKey)) then
            DefaultSavePath := reg.ReadString(sDefaultSavePathKey);
        if reg.ValueExists(sTempPathKey) and directoryExists(reg.ReadString(sTempPathKey)) then
            TempPath := reg.ReadString(sTempPathKey);
        if reg.ValueExists(sGLBackgroundColorKey) then
            backgroundColor := stringtocolor(reg.ReadString(sGLBackgroundColorKey));
        if reg.ValueExists(sHaveAskedAssociateUWAKey) then
            haveAskedAssociateUWA := reg.ReadBool(sHaveAskedAssociateUWAKey);

        //multicore solves
        if reg.ValueExists(sMultiCorePEKey) then
            bMultiCorePE := reg.ReadBool(sMultiCorePEKey);
        if reg.ValueExists(sMultiCoreModesKey) then
            bMultiCoreModes := reg.ReadBool(sMultiCoreModesKey);
        if reg.ValueExists(sMultiCoreRayKey) then
            bMultiCoreRay := reg.ReadBool(sMultiCoreRayKey);


        {read in the recent files list, in reverse order to get correct order in menu}
        if assigned(addRecentFileProc) then
            for i := iRecentFilesMaxLength - 1 downto 0 do
                if reg.ValueExists(sRecentFilesKey + tostr(i)) then
                    addRecentFileProc(reg.ReadString(sRecentFilesKey + tostr(i)));
    end;
    reg.CloseKey;
end;

procedure TMyRegistry.checkUWAFileAssociation;
var
    b : boolean;
begin
    b := false;
    if reg.OpenKey(sRegistryUWAKey, true) then
    begin
        if (not reg.ValueExists('')) or (length(reg.ReadString('')) = 0) then
            b := true  //no current association
        else if ansicomparetext(reg.ReadString(''),Application.ExeName + ' %1') <> 0 then
            b := true;
    end;
    reg.CloseKey;

    if reg.OpenKey(sRegistryExtKeys + sDefaultProjectFileExt, true) then
    begin
        //check if we can associate uwa files with dBSea
        if not reg.ValueExists('') then
            b := true  //no current association
        else
        begin
            if length(reg.ReadString('')) = 0 then
                b := true  //no current association
            else if (not b) and (ansicomparetext(reg.ReadString(''),sRegistryUWADefault) <> 0) and (not haveAskedAssociateUWA) then
            begin
                //only ask if we havent asked before
                b := mrOK = MessageDlg('.uwa files (dBSea project files) are not associated with dBSea. Do you want to use dBSea to open .uwa files?',
                                 mtConfirmation ,
                                 mbOKCancel,
                                 0);
                haveAskedAssociateUWA := true;
            end;
        end;
    end;
    reg.CloseKey;

    if b then
        associateUWAFiles;
end;

constructor TMyRegistry.Create;
begin
    inherited Create;

    reg := TRegistry.Create;
    reg.RootKey := HKEY_CURRENT_USER;
end;

destructor TMyRegistry.Destroy;
begin
    reg.CloseKey;
    reg.Free;

    inherited;
end;

procedure TMyRegistry.associateUWAFiles;
begin
    if reg.OpenKey(sRegistryUWAKey, true) then
        reg.WriteString('',Application.ExeName + ' %1'); //set the command
    reg.CloseKey;

    //if reg.OpenKey(sRegistryDefaultIcon, true) then
    //    reg.WriteString('',Application.ExeName + ',3');
    //reg.CloseKey;

    if reg.OpenKey(sRegistryExtKeys + sDefaultProjectFileExt, true) then
        reg.WriteString('',sRegistryUWADefault);  //find the command from .uwa ext
    reg.CloseKey;
end;

procedure TMyRegistry.setDidAskToUpdateDB(const dbVersion: integer);
begin
    if reg.OpenKey(sRegistryKey, true) then
        reg.writeInteger(sDidAskUpdateDBVersion,dbVersion);
    reg.CloseKey;
end;

function TMyRegistry.getDidAskToUpdateDB(var dBVersion: integer): boolean;
begin
    if reg.OpenKey(sRegistryKey, false) then
    begin
        result := reg.ValueExists(sDidAskUpdateDBVersion);
        if result then
            dbVersion := reg.readInteger(sDidAskUpdateDBVersion);
    end
    else
        result := false;

    reg.CloseKey;
end;

procedure TMyRegistry.setHaveNotifiedNewVersionAvailable(const version: String; const date:TDateTime);
begin
    if reg.OpenKey(sRegistryKey, true) then
    begin
        reg.WriteString(sNotifiedNewVersionKey,version);
        reg.WriteDate(sNotifiedNewVersionDateKey,date);
    end;
    reg.CloseKey;
end;

procedure TMyRegistry.getHaveNotifiedNewVersionAvailable(out version: String; out date:TDateTime);
begin
    version := '';
    date := NULL_DATE;

    {try open the dBSea key. if it doesn't work, do nothing}
    if reg.OpenKey(sRegistryKey, false) then
    begin
        {must be within the correct key to call ValueExists}
        if reg.ValueExists(sNotifiedNewVersionKey) then
            version := reg.readString(sNotifiedNewVersionKey);
        if reg.ValueExists(sNotifiedNewVersionDateKey) then
            date := reg.readDate(sNotifiedNewVersionDateKey);
    end;
    reg.CloseKey;
end;

class procedure TMyRegistry.setGUID;
var
    newID: TGUID;
    s: string;
    aReg: ISmart<TRegistry>;
begin
    aReg := TSmart<TRegistry>.create(TRegistry.create());
    aReg.RootKey := HKEY_CURRENT_USER;
    if aReg.OpenKey(sRegistryKey, false) and (not aReg.ValueExists(GUIDKey)) and (createGUID(newID) = S_OK) then
    begin
        s := newID.ToString();
        if s[1] = '{' then
            s := rightStr(s, length(s) - 1);
        if s[length(s)] = '}' then
            s := leftStr(s, length(s) - 1);
        aReg.WriteString(GUIDKey, s);
    end;
    aReg.CloseKey;
end;

class function TMyRegistry.getGUID: string;
var
    aReg: ISmart<TRegistry>;
begin
    result := '';

    aReg := TSmart<TRegistry>.create(TRegistry.create);
    aReg.RootKey := HKEY_CURRENT_USER;
    if aReg.OpenKey(sRegistryKey, false) then
    begin
        if aReg.ValueExists(GUIDKey) then
            result := aReg.readString(GUIDKey);
    end;
    aReg.CloseKey;
end;

class function TMyRegistry.getGUIDAlphaNumericOnly: string;
var
    i: integer;
    s: string;
begin
    result := '';
    s := self.getGUID;
    for i := 1 to length(s) do
        case s[i] of
            'A'..'Z', 'a'..'z', '0'..'9' : result := result + s[i];
        end;
end;

procedure TMyRegistry.writeMaintenanceValue(const encrypted, salt, serialNumber: string);
begin
    if reg.OpenKey(sRegistryKey + '\\' + sMaintenanceKey + IntToStr(self.countMaintenanceKeys), true) then
    begin
        try
            reg.WriteString(TMaintenanceData.sData, encrypted);
            reg.WriteString(TMaintenanceData.sSalt, salt);
            reg.WriteString(TMaintenanceData.sSerialNumber, serialNumber);
        except on e: Exception do
            eventLog.log('Error on writing to registry: ' + e.ToString);
        end;
    end;
    reg.CloseKey;
end;

class function TMyRegistry.countMaintenanceKeys: integer;
var
    aReg: ISmart<TRegistry>;
begin
    aReg := TSmart<TRegistry>.create(TRegistry.Create);
    aReg.OpenKey(sRegistryKey, true);
    result := 0;
    while aReg.keyExists(sMaintenanceKey + inttostr(result)) do
        inc(result);
end;

class function  TMyRegistry.maintenanceData: TMaintenanceDataList;
var
    i: integer;
    md: TMaintenanceData;
    aReg: ISmart<TRegistry>;
begin
    result := TMaintenanceDataList.create(true);
    for i := 0 to countMaintenanceKeys - 1 do
    begin
        aReg := TSmart<TRegistry>.create(TRegistry.create);
        aReg.RootKey := HKEY_CURRENT_USER;
        if aReg.OpenKeyReadOnly(sRegistryKey + '\\' + sMaintenanceKey + inttostr(i)) then
        begin
            md := TMaintenanceData.create;
            if aReg.ValueExists(TMaintenanceData.sData) then
                md.data := aReg.ReadString(TMaintenanceData.sData);
            if aReg.ValueExists(TMaintenanceData.sSalt) then
                md.salt := aReg.ReadString(TMaintenanceData.sSalt);
            if aReg.ValueExists(TMaintenanceData.sSerialNumber) then
                md.serialNumber := aReg.ReadString(TMaintenanceData.sSerialNumber);
            result.add(md);
            areg.CloseKey;
        end;
    end;
end;

procedure TMyRegistry.WriteRegistryLock(const i: TRegistryLock);
begin
    {write a code to the registry, indicating that this program is locked and should not start}
    reg.OpenKey(sRegistryKey, true);
    case i of
    kNoPreviousDateKey: reg.WriteString(sBarredKey, sBarredValue1);
    kCannotDecodeDate: reg.WriteString(sBarredKey, sBarredValue2);
    kClockTurnedBack: reg.WriteString(sBarredKey, sBarredValue3);
    end;
    reg.closeKey;
end;

function TMyRegistry.CheckRegistryLock(): TRegistryLock;
var
    Barred: string;
begin
    reg.OpenKey(sRegistryKey, true);
    { Look at key to see if program has been barred }
    Barred := '';
    If reg.ValueExists(sBarredKey) then
        Barred := reg.ReadString(sBarredKey); { first time through barred = '' }

    if barred = sBarredValue1 then
        result := kNoPreviousDateKey
    else if barred = sBarredValue2 then
        result := kCannotDecodeDate
    else if barred = sBarredValue3 then
        result := kClockTurnedBack
    else
        result := kNoError;
    reg.closeKey;
end;

procedure TMyRegistry.CheckForTrialLicense(out sLicenseOrTrialExpiryDate: String; out success: boolean);
var
    sEncodedLicenceDate, sLicenceDate: String;
    iLicenseEndDate: Integer;
begin
    success := false;

    sEncodedLicenceDate := '';
    reg.OpenKey(sRegistryKey, false);
    If reg.ValueExists(sLicenseEndDateKey) then
        sEncodedLicenceDate := reg.ReadString(sLicenseEndDateKey);
    reg.closeKey;

    sLicenceDate := TPasswordChecking.DecodePWDEx(sEncodedLicenceDate, SecurityString);
    if isFloat(sLicenceDate) then
    begin
        iLicenseEndDate := toInt(sLicenceDate);

        {check the license end date against the current date}
        if safeRound(Date) > iLicenseEndDate then
        begin
            ShowMessage('1140: Trial license expired, contact your distributor');
            //will go to demo mode

            sLicenseOrTrialExpiryDate := 'Trial ended: ' + FormatDateTime('yyyy-mm-dd',FloatToDateTime(iLicenseEndDate));
        end
        else
        begin
            success := true; //success, we have a license end date, and are not past it
            sLicenseOrTrialExpiryDate := 'Trial ends: ' + FormatDateTime('yyyy-mm-dd',FloatToDateTime(iLicenseEndDate));
        end;
    end
    else
    begin
        if length(sEncodedLicenceDate) > 0 then
            showmessage('1150: Unable to parse stored license data');
    end;
end;

function TMyRegistry.CheckPreviousOpenDate: boolean;
var
    sPreviousTime, sPreviousTimeEncoded: String;
    PreviousDate: Integer;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'CheckPreviousOpenDate');

    { if trial expired or clock set back then it will have been barred so stop Insul }
    reg.OpenKey(sRegistryKey, false);
    { Look for prev date registry key, don't create if it doesn't exist }
    If not reg.ValueExists(sPreviousDateKey) then
    begin
        {key doesn't exist in registry. write lock and exit}
        ShowMessage('License error 104');
        WriteRegistryLock(kNoPreviousDateKey);
        Exit(false);
    end;

    sPreviousTimeEncoded := reg.ReadString(sPreviousDateKey);

    {on first install, the sPreviousDateKey field is set by the install to a date when the program was being written.}
    sPreviousTime := TPasswordChecking.DecodePWDEx(sPreviousTimeEncoded, SecurityString);{ previous time is encoded so user can't easily alter it }
    if not isFloat(sPreviousTime) then
    begin
        {if the decoded string can't be decoded into a valid date, then someone has probably messed with it. lock and exit.}
        ShowMessage('License error 103');
        WriteRegistryLock(kCannotDecodeDate);
        Exit(false);
    end
    else
        PreviousDate := toInt(sPreviousTime); //the last time the prog was opened

    if safeRound(Date) < PreviousDate then
    begin
        ShowMessage('License error 102'); { error 102 = clock turned back }
        WriteRegistryLock(kClockTurnedBack); { if todays date is less than previous time the user has turned the clock back }
        Exit(false);
    end
    else
    begin
        result := true;  //success, everything is ok
        { write todays date (encoded) as value of previous time in registry}
        reg.WriteString(sPreviousDateKey, TPasswordChecking.EncodePWDEx(IntToStr(safeRound(Date)),SecurityString, 1, 10));
    end;
    reg.closeKey;
end;

class procedure TMyRegistry.writeLicenseFile(const sFileFirstLine: string);
var
    aReg: ISmart<TRegistry>;
    log: ISynLog;
begin
    log := TSynLogLevs.Enter(self,'writeLicenseFile');

    aReg := TSmart<TRegistry>.create(TRegistry.create);
    aReg.RootKey := HKEY_CURRENT_USER;
    if aReg.OpenKey(sRegistryKey, true) then
    begin
        aReg.WriteString(sLicenseEndDateKey,sFileFirstLine);
        aReg.CloseKey;
    end;
end;

initialization
    reg := TMyRegistry.create;
    reg.setGUID; // only needed on first run

finalization
    reg.free;

end.
