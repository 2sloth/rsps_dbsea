unit WMI_Info;

interface

uses
    SysUtils,
    ActiveX,
    ComObj,
    Variants;

type
    TWMI_Info = class(TObject)
    private
        class function GetWin32_VideoControllerInfo: string;
    public
        class function info: string;

    end;

implementation

class function TWMI_Info.GetWin32_VideoControllerInfo: string;
const
    WbemUser = '';
    WbemPassword = '';
    WbemComputer = 'localhost';
    wbemFlagForwardOnly = $00000020;
var
    FSWbemLocator: OLEVariant;
    FWMIService: OLEVariant;
    FWbemObjectSet: OLEVariant;
    FWbemObject: OLEVariant;
    oEnum: IEnumvariant;
    iValue: LongWord;
begin;
    result := '';

    FSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
    FWMIService := FSWbemLocator.ConnectServer(WbemComputer, 'root\CIMV2', WbemUser, WbemPassword);
    FWbemObjectSet := FWMIService.ExecQuery('SELECT Name,PNPDeviceID,CurrentBitsPerPixel  FROM Win32_VideoController', 'WQL', wbemFlagForwardOnly);
    oEnum := IUnknown(FWbemObjectSet._NewEnum) as IEnumvariant;
    while oEnum.Next(1, FWbemObject, iValue) = 0 do
    begin
        result := result + Format('Name: %s, ID: %s, bits per pixel: %u; ', [String(FWbemObject.Name), String(FWbemObject.PNPDeviceID), uint32(FWbemObject.CurrentBitsPerPixel)]);
        FWbemObject := Unassigned;
    end;
end;

class function TWMI_Info.info: string;
begin
    result := '';
    try
        CoInitialize(nil);
        try
            result := GetWin32_VideoControllerInfo;
        finally
            CoUninitialize;
        end;
    except
        on E: EOleException do
            result := Format('EOleException %s %x', [E.Message, E.ErrorCode]);
        on E: Exception do
            result := E.Classname + ':' + E.Message;
    end;
end;

end.
